<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Calculation;
use app\models\Evaluation;
use app\models\Initiatives;
use app\models\OkrAchievement;
use app\models\OkrEvaluation;
use app\models\Status;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;


class ReportController extends Controller
{
    
    public function actionOkrAchieved(){
        $prevYear = date('Y') - 1;
        $currentYear = date('Y');

        $okrEvaluationByOkrId = Yii::$app->db->createCommand("
                                                        select 
                                                            okr_id, 
                                                            evaluation_id,
                                                            (select weight from management.okr_org oo where id = o.okr_org_id)  
                                                        from management.okr_evaluation oe 
                                                        join management.evaluation e on oe.evaluation_id = e.id 
                                                        join management.okr o on oe.okr_id = o.id
                                                        where 
                                                            e.year in ($prevYear, $currentYear) and 
                                                            oe.unit_structure_id is not null
                                                        group by okr_id, evaluation_id, o.okr_org_id  
                                                ")->queryAll();

        foreach($okrEvaluationByOkrId as $oe){

            $okrEvaluations = OkrEvaluation::find()
                                        ->where(['evaluation_id' => $oe['evaluation_id']])
                                        ->andWhere(['okr_id' => $oe['okr_id']])
                                        ->andWhere(['status_id' => [Status::OKR_ORG_E_SENT, Status::OKR_ORG_E_VERIFIED]])
                                        ->andWhere('unit_structure_id is not null')
                                        ->all();
            $achievedKrDept = 0;
            foreach($okrEvaluations as $okrEvaluation){
                if($okrEvaluation->weight && $okrEvaluation->real){

                    // if($okrEvaluation->okr->calculation->id == Calculation::MIN){
                    //     $achieve = ($okrEvaluation->okr->target / $okrEvaluation->real) * 100;
                    // } else {
                    //     $achieve = ($okrEvaluation->real / $okrEvaluation->okr->target) * 100;
                    // }

                    // real/targetq1*bobot

                    if($okrEvaluation->evaluation->period == 'Kuartal 1'){
                        $target = 'target_q1';
                    } else if($okrEvaluation->evaluation->period == 'Kuartal 2'){
                        $target = 'target_q2';
                    } else if($okrEvaluation->evaluation->period == 'Kuartal 3'){
                        $target = 'target_q3';
                    } else if($okrEvaluation->evaluation->period == 'Kuartal 4'){
                        $target = 'target_q4';
                    }
                    
                    if($okrEvaluation->okr->$target){
                        // $real = ($okrEvaluation->real * $okrEvaluation->weight) / 100;
                        $real = ($okrEvaluation->real / $okrEvaluation->okr->$target) * $okrEvaluation->weight;
                    } else {
                        $real = ($okrEvaluation->real / $okrEvaluation->okr->target) * $okrEvaluation->weight;
                    }
                    // $real = ($okrEvaluation->real*$okrEvaluation->weight)/100;

                    // $temp = ($achieve * $okrEvaluation->weight) / 100;

                    $achievedKrDept = $achievedKrDept + $real;

                    // var_dump($okrEvaluation->real);
                    // var_dump($okrEvaluation->okr->$target);
                    // var_dump($okrEvaluation->weight);
                    // var_dump($real);
                    var_dump('----');
                    // var_dump($okrEvaluation->okr_id);
                }
            }

            // $achieveKr = ($achievedKrDept * $oe['weight']) / 100;
            $achieveKr = $achievedKrDept;

            if($oe['okr_id'] == 21501){

                if($oe['evaluation_id'] == 26){
                    $achieveKr = ((8440503340 + 22714073479) / 72441365000) * 100; 
                } else if($oe['evaluation_id'] == 27){
                   $achieveKr = ((59269152809 + 14618217275) / 114833051667) * 100; 
                }

                $okrAchieved = OkrAchievement::findOne(['okr_id' => $oe['okr_id'], 'evaluation_id' => $oe['evaluation_id']]);
                if(!$okrAchieved) $okrAchieved = new OkrAchievement();
                $okrAchieved->okr_id = $oe['okr_id'];
                $okrAchieved->evaluation_id = $oe['evaluation_id'];
                $okrAchieved->achieve_value = $achieveKr;
                if($okrAchieved->save()){
                    echo $oe['okr_id'] . ' => ' . $achieveKr . PHP_EOL;
                }

            } else {
                $okrAchieved = OkrAchievement::findOne(['okr_id' => $oe['okr_id'], 'evaluation_id' => $oe['evaluation_id']]);
                if(!$okrAchieved) $okrAchieved = new OkrAchievement();
                $okrAchieved->okr_id = $oe['okr_id'];
                $okrAchieved->evaluation_id = $oe['evaluation_id'];
                $okrAchieved->achieve_value = $achieveKr;
                if($okrAchieved->save()){
                    echo $oe['okr_id'] . ' => ' . $achieveKr . PHP_EOL;
                }
            }


        }

        /* $okrAchieves = OkrAchievement::find()->alias('oa')->joinWith('okr o')->where(['o.parent_id' => $this->id])->andWhere(['o.year' => $year])->all();
        $val = 0;
        foreach($okrAchieves as $okrAchieve){
            $temp = (($okrAchieve->achieve_value?:0) * $okrAchieve->okr->org->weight) / 100;

            $val = $val + $temp;
        } */

    }

    protected function getColumnTarget($period, $type){

        $months = [];
        if($period == 'Kuartal 1'){
            if($type == 'plan'){
                array_push($months, 'month_01');
                array_push($months, 'month_02');
                array_push($months, 'month_03');
            } else if($type == 'real') {
                array_push($months, 'real_month_01');
                array_push($months, 'real_month_02');
                array_push($months, 'real_month_03');
            }
        } else if($period == 'Kuartal 2'){
            if($type == 'plan'){
                array_push($months, 'month_04');
                array_push($months, 'month_05');
                array_push($months, 'month_06');
            } else if($type == 'real') {
                array_push($months, 'real_month_04');
                array_push($months, 'real_month_05');
                array_push($months, 'real_month_06');
            }
        } else if($period == 'Kuartal 3'){
            if($type == 'plan'){
                array_push($months, 'month_07');
                array_push($months, 'month_08');
                array_push($months, 'month_09');
            } else if($type == 'real') {
                array_push($months, 'real_month_07');
                array_push($months, 'real_month_08');
                array_push($months, 'real_month_09');
            }
        } else if($period == 'Kuartal 4'){
            if($type == 'plan'){
                array_push($months, 'month_10');
                array_push($months, 'month_11');
                array_push($months, 'month_12');
            } else if($type == 'real') {
                array_push($months, 'real_month_10');
                array_push($months, 'real_month_11');
                array_push($months, 'real_month_12');
            }
        }

        return $months;
    }

}
