<?php

namespace app\commands;

use app\components\EmailNotification;
use app\models\Activities;
use app\models\Budget;
use app\models\CascadingType;
use app\models\Creator;
use app\models\Departments;
use app\models\Evaluation;
use app\models\Initiatives;
use app\models\Okr;
use app\models\OkrEvaluation;
use app\models\OkrLevel;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\Status;
use app\models\UnitStructure;
use yii\console\Controller;

class FixController extends Controller
{
    
    public function actionFixInitiativeCode($year = null){

        if(!$year) $year = date('Y');

        $initiatives = Initiatives::find()
                            ->alias('i')
                            ->joinWith(['okr o', 'position p'])
                            ->where(['o.year' => $year])
                            ->andWhere(['not', ['i.status_id'=>[Status::REJECTED,Status::DELETED]]])
                            // ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                            ->andWhere(['p.unit_structure_id' => 438])
                            ->orderBy('o.okr_code asc, i.id asc')
                            ->all();

        $no = 1;
        foreach($initiatives as $initiative){
            $prevInitiativesCode = $initiative->initiatives_code;

            $code = $initiative->okr->position->unit->initiative_code;
            $initiative->initiatives_code = $code . '-' . str_pad(($no), 2, "0", STR_PAD_LEFT);
            if($initiative->save(false)){

                $okrs = Okr::find()
                                ->where(['okr' => $initiative->workplan_name])
                                ->andWhere(['reference_no' => $prevInitiativesCode])
                                ->andWhere(['type' => 'O'])
                                ->andWhere(['year' => $year])
                                ->all();
                foreach($okrs as $okr){
                    $okr->reference_no = $initiative->initiatives_code;
                    if($okr->save(false)){
                        $childs = Okr::find()
                                        ->where(['parent_id' => $okr->id])
                                        ->andWhere(['type' => 'KR'])
                                        ->andWhere(['year' => $year])
                                        ->all();

                        foreach($childs as $child){
                            $child->reference_no = $initiative->initiatives_code;
                            if($child->save(false)){
                                echo 'done' . PHP_EOL;
                            }
                        }
                    }
                }

                echo $initiative->initiatives_code . PHP_EOL;
                $no++;
            }
        }
    }

    public function actionBudgetCode(){
        $initiatives = Initiatives::find()
                                    ->alias('i')
                                    ->joinWith('okr o')
                                    ->where(['year' => 2023])
                                    ->andWhere(['i.id' => 2195])
                                    ->orderBy('initiatives_code asc')
                                    ->all();
        
        foreach($initiatives as $initiative){
            $budgets = Budget::find()
                                ->where(['initiatives_id' => $initiative->id])
                                ->andWhere('account_id is not null')
                                ->andWhere('budget_code is null')
                                ->andWhere(['<>', 'status_id', [Status::REJECTED,Status::DELETED]])
                                ->orderBy(['id'=>SORT_ASC])->all();

            if($budgets){
                $budget_no = 1;
                foreach ($budgets as $budget) {
                    // var_dump($budget->id);
                    $account_no = preg_replace("/\s+/", "", $budget->account->daf_account_no);
                    // var_dump($account_no);
                    $budget_code = $initiative->initiatives_code .'.'. str_pad($budget_no, 2, "0", STR_PAD_LEFT) .'-'. $account_no;
                    $budget->budget_code = $budget_code;
                    // $budget->status_id = Status::APPROVED;

                    echo  $budget_code . PHP_EOL;

                    if($budget->save(false)){
                        $budget_no++;
                    }
                }
            }
        }
    }

    public function actionCopyDepartments(){

        $departments = Creator::find()
                            ->alias('c')
                            ->joinWith('dept d')
                            ->where(['d.company_id' => 1])
                            ->orderBy('c.year asc')
                            ->all();
        
        foreach($departments as $dept){

            $check = UnitStructure::findOne(['year' => $dept->year, 'unit_id' => $dept->departments_id]);

            if(!$check){
                $unit = new UnitStructure();
                $unit->year = $dept->year;
                $unit->unit_id = $dept->departments_id;
                $unit->level = (string)$dept->dept->level;
                $unit->unit_code = $dept->department_code;
                $unit->initiative_code = $dept->budget_code;
                $unit->unit_name = $dept->dept->deptname;
                if($unit->save()){
                    echo $unit->unit_name . PHP_EOL;
                } else {
                    var_dump($unit->errors);
                }
            }

        }

    }

    public function actionUpdateNameDept(){

        $units = UnitStructure::find()
                        ->where('unit_id is not null')
                        ->all();

        foreach($units as $unit){
            if($dept = Departments::findOne($unit->unit_id)){
                $dept->deptname = $unit->unit_name;
                if(!$dept->save()){
                    echo var_dump($dept->errors);
                }
            }
        }

    }

    public function actionCreateDept(){

        $units = UnitStructure::find()
                        ->where('unit_id = 4')
                        ->all();

        foreach($units as $unit){
            // if(!$dept = Departments::findOne($unit->unit_id)){
                $newUnit = new Departments();
                $newUnit->company_id = 1;
                $newUnit->branch_id = 1;
                $newUnit->deptname = $unit->unit_name;
                $newUnit->supdeptid = $unit->sup->unit_id;
                $newUnit->level = $unit->level;
                if($newUnit->save()){
                    $unit->unit_id = $newUnit->deptid;
                    $unit->save(false);
                    var_dump($unit->errors);
                }
            // }
        }

    }

    public function actionSetRefId(){
        $okrs = Okr::find()->where('ref_id is null')->all();

        foreach($okrs as $okr){
            $okr->ref_id = $okr->parent_id;
            $okr->save(false);
        }
    }

    public function actionSetOkrOrgId(){

        $okrs = Okr::find()
                        ->where(['okr_level_id' => OkrLevel::ORGANIZATION])
                        ->andWhere(['okr_org_id' => 6])
                        ->all();

        foreach($okrs as $okr){
            $okrOrgId = $okr->okr_org_id;
            $okrId = $okr->id;
            echo $okrId . PHP_EOL;

            $okrDirs = Okr::find()
                                ->where(['okr_level_id' => OkrLevel::DIRECTORATE])
                                ->andWhere('ref_id = ' . $okrId . ' or parent_id = ' . $okrId)
                                ->all();
            foreach($okrDirs as $okrDir){
                echo "Directorate" . PHP_EOL;
                $okrDir->okr_org_id = $okrOrgId;
                if($okrDir->save(false)){
                    echo '* ' . $okrDir->id . PHP_EOL;
                    $okrDepts =  Okr::find()
                    ->where(['okr_level_id' => OkrLevel::DEPARTMENT])
                    ->andWhere('ref_id = ' . $okrDir->id . ' or parent_id = ' . $okrDir->id)
                    ->all();
                    foreach($okrDepts as $okrDept){
                        echo "Departement" . PHP_EOL;
                        $okrDept->okr_org_id = $okrOrgId;
                        if($okrDept->save(false)){
                            echo '** ' . $okrDept->id . PHP_EOL;
                            $okrDivs =  Okr::find()
                                                ->where(['in', 'okr_level_id', [OkrLevel::DEPARTMENT, OkrLevel::DIVISION]])
                                                ->andWhere('ref_id = ' . $okrDept->id . ' or parent_id = ' . $okrDept->id)
                                                ->all();
                            foreach($okrDivs as $okrDiv){
                                echo "DIVISION" . PHP_EOL;
                                $okrDiv->okr_org_id = $okrOrgId;
                                if($okrDiv->save(false)){
                                    echo '*** ' . $okrDiv->id . PHP_EOL;
                                    $okrInds =  Okr::find()
                                                        ->where(['in', 'okr_level_id', [OkrLevel::DIVISION, OkrLevel::INDIVIDUAL]])
                                                        ->andWhere('ref_id = ' . $okrDiv->id . ' or parent_id = ' . $okrDiv->id)
                                                        ->all();
                                    foreach($okrInds as $okrInd){
                                        $okrInd->okr_org_id = $okrOrgId;
                                        if($okrInd->save(false)){
                                            echo '**** ' . $okrInd->id . PHP_EOL;                    
                                        } else {
                                            var_dump($okrInd->errors);die;
                                        }        
                                    }
            
                                } else {
                                    var_dump($okrDiv->errors);die;
                                }        
                            }
                        } else {
                            var_dump($okrDept->errors);die;
                        }        
                    }
                } else {
                    var_dump($okrDir->errors);die;
                }    
            }
        }

    }

    public function actionSetSupportType(){

        $okrs = Okr::find()
                        ->where(['okr_level_id' => OkrLevel::ORGANIZATION])
                        ->all();

        foreach($okrs as $okr){
            $okrId = $okr->id;

            $okrDirs = Okr::find()
                                ->where(['okr_level_id' => OkrLevel::DIRECTORATE])
                                ->andWhere('ref_id = ' . $okrId . ' or parent_id = ' . $okrId)
                                ->all();
            foreach($okrDirs as $okrDir){
                $okrDir->support_type = $okr->support_type;
                if($okrDir->save(false)){
                    $okrDepts =  Okr::find()
                                        ->where(['okr_level_id' => [OkrLevel::DIRECTORATE, OkrLevel::DEPARTMENT]])
                                        ->andWhere('ref_id = ' . $okrDir->id . ' or parent_id = ' . $okrDir->id)
                                        ->all();
                    foreach($okrDepts as $okrDept){
                        $okrDept->okr_org_id = $okrDept->support_type;
                        if($okrDept->save(false)){
                            $okrDivs =  Okr::find()
                                                ->where(['okr_level_id' => [OkrLevel::DEPARTMENT, OkrLevel::DIVISION]])
                                                ->andWhere('ref_id = ' . $okrDept->id . ' or parent_id = ' . $okrDept->id)
                                                ->all();
                            foreach($okrDivs as $okrDiv){
                                $okrDiv->okr_org_id = $okrDiv->support_type;
                                if($okrDiv->save(false)){
                                    $okrInds =  Okr::find()
                                                        ->where(['okr_level_id' => [OkrLevel::DIVISION, OkrLevel::INDIVIDUAL]])
                                                        ->andWhere('ref_id = ' . $okrDiv->id . ' or parent_id = ' . $okrDiv->id)
                                                        ->all();
                                    foreach($okrInds as $okrInd){
                                        $okrInd->okr_org_id = $okrInd->support_type;
                                        if($okrInd->save(false)){
                    
                                        }    
                                    }
            
                                }    
                            }
                        }    
                    }
                }    
            }
        }

    }

    public function actionUpdateOkrOrg(){
        $okrs = OkrEvaluation::find()->where('unit_structure_id is not null')->andWhere(['evaluation_id' => 30])->all();

        foreach($okrs as $okr){
            $okr->save();
        }
    }

    public function actionOkrCode(){
        $okrs = Okr::find()
                        ->where(['year' => 2024])
                        ->andWhere("okr_level_id != 1")
                        ->orderBy('okr_level_id asc, type desc, id asc')
                        ->all();

        foreach($okrs as $okr){

            if($okr->okr_level_id == OkrLevel::INDIVIDUAL && $okr->ref_initiatives_id){ // OKR INDIVIDU
                if($okr->type == 'O'){
                    $initiative = Initiatives::findOne($okr->ref_initiatives_id);
                    $newno = $initiative->initiatives_code;
                } else {
                    $parent = Okr::findOne($okr->parent_id);

                    $no_urut = Okr::find()->where(['parent_id' => $okr->parent_id])->andWhere(['type' => 'KR'])->count();
                    $lastNo = $no_urut + 1;
                    $newno = $parent->okr_code . '-' . str_pad($lastNo, 2, "0", STR_PAD_LEFT);
                }

            } else if($okr->position_structure_id && $position = PositionStructure::findOne($okr->position_structure_id)){ // SELAIN INDIVIDU & ORGANISASI
                $unitCode = $position->unit->unit_code;

                if($okr->cascading_type_id == CascadingType::SUPPORT){
                    $parent = Okr::findOne($okr->parent_id);
        
                    if($okr->type == 'O'){
                        if($okr->okr_level_id == OkrLevel::DIRECTORATE){
                            $newno = $unitCode . '-' . $parent->okr_code;
                        } else {
                            $no_urut = Okr::find()->where(['parent_id' => $okr->parent_id])->andWhere(['type' => 'KR'])->count();
                            $lastNo = $no_urut + 1;
                            $newno = $parent->okr_code . '-' . $lastNo;
                        }
                    } else {
                        $no_urut = Okr::find()->where(['parent_id' => $okr->parent_id])->andWhere(['type' => 'KR'])->count();
                        $lastNo = $no_urut + 1;
                        $newno = $parent->okr_code . '-' . $lastNo;
                    }
                } else {
                    $reff = Okr::findOne($okr->ref_id);
                    if($okr->okr_level_id == OkrLevel::DIRECTORATE){
                        $newno = $unitCode . '-' . $reff->okr_code;
                    } else {
                        $reffCode = explode('-', $reff->okr_code);
                        $newno = str_replace($reffCode[0], $unitCode, $reff->okr_code);
                    }
                }

            } else {
                $newno = 'no_position';
            }

            $okr->okr_code = $newno;
            if($okr->save(false)){
                echo $okr->okr_code .PHP_EOL;
            }
        }
    }

    public function actionSetIndividualOkrCode(){

        $okrs = Okr::find()
                        ->alias('o')
                        ->joinWith('position p')
                        ->where(['okr_level_id' => OkrLevel::INDIVIDUAL])
                        // ->andWhere(['type' => 'O'])
                        ->andWhere(['type' => 'KR'])
                        ->andWhere(['o.year' => 2023])
                        // ->andWhere('p.unit_structure_id = 465')
                        // ->andWhere(['position_structure_id' => 2936])
                        ->orderBy('id')
                        ->all();
        
        foreach($okrs as $okr){
            /* $parent = Okr::findOne($okr->parent_id);

            if($okr->type == 'O'){
                $no_urut = Okr::find()->where(['parent_id' => $okr->parent_id])->andWhere('okr_code is not null')->count();
                $lastNo = $no_urut + 1;
                $newno = $parent->okr_code . '-' . str_pad($lastNo, 2, "0", STR_PAD_LEFT);
            } else {
                $no_urut = Okr::find()->where(['parent_id' => $okr->parent_id])->andWhere('okr_code is not null')->count();
                $lastNo = $no_urut + 1;
                $newno = $parent->okr_code . '-' . str_pad($lastNo, 2, "0", STR_PAD_LEFT);
            } */

            $unitCode = $okr->position->unit->unit_code;
            $reffCode = explode('-', $okr->okr_code);
            $newno = str_replace($reffCode[0], $unitCode, $okr->okr_code);

            $okr->okr_code = $newno;
            if($okr->save()){
                echo "{$okr->id} => {$okr->okr_code}" . PHP_EOL;
            } else {
                var_dump($okr->errors);
            }
        }

    }

    public function actionSetStatusOkr(){
        $okrs = Okr::find()
                        ->where(['okr' => 'Persentase jumlah SDM yang memenuhi 80% pencapaian IKU individu'])
                        ->andWhere(['okr_level_id' => 5])
                        ->all();

        var_dump(count($okrs));
        foreach($okrs as $okr){
            $okr->status_id = Status::OKR_CANCEL;
            if($okr->save(false)){
                if($okr->type == 'KR'){
                    $parent = Okr::findOne($okr->parent_id);
                    if($parent){
                        $parent->status_id = Status::OKR_CANCEL;
                        $parent->save();
                    }
                }
            }
        }
    }

    public function actionSetTarget(){
        $evaluationId = 19;

        $okrEvaluations = OkrEvaluation::find()
                                            ->alias('oe')
                                            ->joinWith('okr o')
                                            ->where(['evaluation_id' => $evaluationId])
                                            ->andWhere("o.okr_level_id <> 1")
                                            // ->andWhere("oe.target is null")
                                            // ->andWhere("o.target is not null")
                                            ->all();

        foreach($okrEvaluations as $oe){
            $oe->target = $oe->okr->target;
            if($oe->save(false)){
                echo $oe->okr_id . PHP_EOL;
            }
        }
    }
    
    public function actionSetOutput(){
        $evaluationId = 20;

        $okrEvaluations = OkrEvaluation::find()
                                            ->alias('oe')
                                            ->joinWith('okr o')
                                            ->where(['evaluation_id' => $evaluationId])
                                            ->andWhere("o.okr_level_id = 1")
                                            // ->andWhere("oe.output is null")
                                            ->all();

        foreach($okrEvaluations as $oe){
            if($okrEvaluationQ1 = OkrEvaluation::find()
                                                ->alias('oe')
                                                ->joinWith('okr o')
                                                ->where(['evaluation_id' => 18])
                                                ->andWhere(['okr_id' => $oe->okr_id])->one()){
                $oe->output = $okrEvaluationQ1->output;
                if($oe->save(false)){
                    echo $oe->id . ' updated' . PHP_EOL;
                }
            }
        }
    }

    public function actionSetDueDate(){
        $activities = Activities::find()
                                ->alias('c')
                                ->joinWith('parent p')
                                ->where('c.parent_id is not null')
                                ->andWhere('c.due_date is null')
                                ->andWhere('p.due_date is not null')
                                ->all();
                                
        foreach($activities as $act){
            $act->due_date = $act->parent->due_date;
            if($act->save()){
                echo $act->id . ' => ' .$act->parent->due_date . PHP_EOL;
            }
        }
    }

    public function actionTestMail(){
        EmailNotification::test();

        return 'ok';
    }

    public function actionLevelOkr(){
        $okrs = Okr::find()->where(['year' => 2023])->all();
        $i = 1;
        foreach($okrs as $okr){
            if(in_array($okr->position->level, [9, 8])){
                $okr->okr_level_id = OkrLevel::DIRECTORATE;
            } else if($okr->position->level == 6){
                $okr->okr_level_id = OkrLevel::DEPARTMENT;
            } else if($okr->position->level == 4){
                $okr->okr_level_id = OkrLevel::DIVISION;
            } else {
                $okr->okr_level_id = OkrLevel::INDIVIDUAL;
            }

            if($okr->save(false)){
                echo "$i - {$okr->id} updated." . PHP_EOL;
            }
            $i++;
        }

        echo $i . PHP_EOL;
    }

    public function actionPositionStructure(){
        $okrs = Okr::find()->where(['year' => 2023])->andWhere(['okr_level_id' => 5])->andWhere(['type' => 'KR'])->all();

        foreach($okrs as $okr){

            if($okr->pic_id != $okr->parent->pic_id){

                $employeePositionStructure = PositionStructureEmployee::findOne(['position_structure_id' => $okr->position_structure_id]);
                $positionStructure = PositionStructureEmployee::find()
                                                            ->joinWith('position p')
                                                            ->where(['p.unit_structure_id' => $employeePositionStructure->position->unit_structure_id])
                                                            ->andWhere(['employee_id' => $okr->pic_id])
                                                            ->one();

                $parent = Okr::findOne($okr->parent_id);
                $parent->position_structure_id = $positionStructure->position_structure_id;
                $parent->year = 2023;
                if($parent->save()){
                    $okr->position_structure_id = $positionStructure->position_structure_id;
                    $okr->year = 2023;
                    $okr->save(false);
                }

    
                
                echo $okr->pic_id . PHP_EOL;
                echo $employeePositionStructure->position->unit_structure_id . PHP_EOL;
                echo $positionStructure->position_structure_id;
            }

    
        }
    }

    public function actionSetPic(){
        $okrs = Okr::find()->where(['year' => 2025])->andWhere(['okr_level_id' => [/* 2,  */3, 4]])->andWhere('pic_id is null')->all();

        foreach($okrs as $okr){
            if($positionStructureEmployee = PositionStructureEmployee::findOne(['position_structure_id' => $okr->position_structure_id])){
                $okr->pic_id = $positionStructureEmployee->employee_id;
                if($okr->save(false)){
                    echo "{$okr->okr_code} => {$okr->pic_id}" . PHP_EOL;
                }
            }
        }
    }

    public function actionGetDifferentBudget(){

        $unitId = 504;
        
        $initiatives = Initiatives::find()
                                        ->alias('i')
                                        ->joinWith(['okr.position p'])
                                        // ->where(['p.unit_structure_id' => $unitId])
                                        ->andWhere(['not in', 'i.status_id', [Status::REJECTED, Status::DELETED]])
                                        ->andWhere(['p.year' => 2024])
                                        ->all();

        $total = 0;
        foreach($initiatives as $i){
            echo $i->initiatives_code . PHP_EOL;

            $budgetTotals = 0;
            $budgets = Budget::find()->where(['initiatives_id' => $i->id])->all();
            foreach($budgets as $b){
                $budgetTotals += $b->total_amount;
            }

            if($i->total_budget > $budgetTotals){
                echo ' . . diferent' . PHP_EOL;
            } else {
                // echo $i->total_budget . ' = ' . $budgetTotals . PHP_EOL;
            }

            $total += $budgetTotals;
        }

        echo $total . PHP_EOL;
    }


    /* ============= */
    public function actionSetRefInId(){
        $okrs = Okr::find()
                        ->where(['year' => 2023])
                        ->andWhere('reference_no is not null')
                        ->all();

        $i = 0;
        foreach($okrs as $okr){
            $initiative = Initiatives::find()
                                        ->joinWith('okr o')
                                        ->where(['initiatives_code' => $okr->reference_no])
                                        ->andWhere(['year' => date('Y')])
                                        ->one();

            $okr->ref_initiatives_id = $initiative->id;
            if($okr->save()){
                echo $i . ' ' . $okr->id . PHP_EOL;
                $i++;
            }
        }
    }

    public function actionSetOkrWeight($year = null){

        if(!$year) $year = date('Y');

        $okrs = Okr::find()
                        ->where(['year' => $year])
                        ->andWhere('okr_level_id <> 1')
                        ->orderBy('okr_level_id')
                        ->all();

        $i = 0;
        foreach($okrs as $okr){
            
            /** */
            $totalValidity1 = Okr::find()
                                    ->joinWith('validity v')
                                    ->where($okr->okr_level_id == OkrLevel::INDIVIDUAL ? ['ref_initiatives_id' => $okr->ref_initiatives_id] : ['parent_id' => $okr->parent_id])
                                    ->andWhere(['year' => $year])
                                    ->andWhere(['type' => 'KR'])
                                    ->sum('value');

            if($totalValidity1) {
                $indexValidity1 = ($okr->validity->value * 100) / $totalValidity1;
            } else {
                $indexValidity1 = 0;
            }

            $totalControlabilty1 = Okr::find()
                                    ->joinWith('controll c')
                                    ->where($okr->okr_level_id == OkrLevel::INDIVIDUAL ? ['ref_initiatives_id' => $okr->ref_initiatives_id] : ['parent_id' => $okr->parent_id])
                                    ->andWhere(['year' => $year])
                                    ->andWhere(['type' => 'KR'])
                                    ->sum('value');

            if($totalControlabilty1){
                $indexControlability1 = ($okr->controll->value * 100) / $totalControlabilty1;
            } else {
                $indexControlability1 = 0;
            }

            /** WEIGHT */
            $okr->weight = ($indexValidity1 + $indexControlability1) / 2;
            /** */

            /** */
            $totalValidity2 = Okr::find()
                                    ->joinWith('validity v')
                                    ->where(['pic_id' => $okr->pic_id])
                                    ->andWhere(['position_structure_id' => $okr->position_structure_id])
                                    ->andWhere(['okr_level_id' => $okr->okr_level_id])
                                    ->andWhere(['year' => $year])
                                    ->andWhere(['type' => 'KR'])
                                    ->sum('value');

            if($totalValidity2){
                $indexValidity2 = ($okr->validity->value * 100) / $totalValidity2;
            } else {
                $indexValidity2 = 0;
            }

            $totalControlabilty2 = Okr::find()
                                    ->joinWith('controll c')
                                    ->where(['pic_id' => $okr->pic_id])
                                    ->andWhere(['position_structure_id' => $okr->position_structure_id])
                                    ->andWhere(['okr_level_id' => $okr->okr_level_id])
                                    ->andWhere(['year' => $year])
                                    ->andWhere(['type' => 'KR'])
                                    ->sum('value');

            if($totalControlabilty2){
                $indexControlability2 = ($okr->controll->value * 100) / $totalControlabilty2;
            } else {
                $indexControlability2 = 0;
            }

            /** UNIT WEIGHT */
            $okr->unit_weight = ($indexValidity2 + $indexControlability2) / 2;   
            /** */         

            if($okr->save(false)){
                echo $i . ' ' . $okr->id . PHP_EOL;
                $i++;
            }
        }
    }

    public function actionSetPosition(){
        $activities = Activities::find()
                                    ->where("position_structure_id is null")
                                    ->andWhere(['activity_category_id' => 1])
                                    ->andWhere("start_date > '2023-01-01'")
                                    ->all();

        foreach($activities as $activity){
            $post = PositionStructureEmployee::find()
                                                ->alias('pse')
                                                ->joinWith('position')
                                                ->where(['employee_id' => $activity->pic_id])
                                                ->where(['year' => 2023])
                                                ->one();
            if($post){
                $activity->position_structure_id = $post->position_structure_id;
                if($activity->save(false)){
                    echo $activity->id . PHP_EOL;
                }
            }
        }
    }


    public function actionSetTargetOkr(){
        $okrEvaluations = OkrEvaluation::find()
                                            ->where(['evaluation_id' => 22])
                                            ->andWhere("unit_structure_id is not null")
                                            ->andWhere("target is null")
                                            ->all();

        foreach($okrEvaluations as $okrEvaluation){
            $okrEvaluation->target = $okrEvaluation->okr->target_q1;
            if($okrEvaluation->save(false)){
                echo $okrEvaluation->id . PHP_EOL;
            }
        }
    }

    public function actionSetTotalAmountBudget(){

        $initiatives = Initiatives::find()
                                        ->joinWith('position p')
                                        ->where(['p.year' => 2024])
                                        // ->andWhere(['p.unit_structure_id' => 603])
                                        ->all();

        foreach($initiatives as $i){

            $budgets = Budget::find()->where(['initiatives_id' =>  $i->id])->andWhere(['not in', 'status_id', [Status::REJECTED,Status::DELETED]])->all();

            foreach($budgets as $b){
                $b->total_amount = (float) ($b->volume * ((float) $b->unit_amount) * $b->frequency);
                $b->save(false);
            }
            
            $total_budget = Budget::find()->where(['initiatives_id' =>  $i->id])->andWhere(['not in', 'status_id', [Status::REJECTED,Status::DELETED]])->sum('total_amount');

            $initiatives = Initiatives::findOne($i->id);
            $initiatives->total_budget = $total_budget;
            if($initiatives->save(false)){
                echo $i->id . ' - ' . $i->initiatives_code . ' = ' . $total_budget . PHP_EOL;
            }
        }
    }

    public function actionSetTotalBudget(){

        $initiatives = Initiatives::find()
                                        ->joinWith('position p')
                                        ->where(['p.year' => 2024])
                                        ->andWhere(['p.unit_structure_id' => 603])
                                        ->all();

        foreach($initiatives as $i){
            
            $total_budget = Budget::find()->where(['initiatives_id' =>  $i->id])->andWhere(['not in', 'status_id', [Status::REJECTED,Status::DELETED]])->sum('total_amount');

            $initiatives = Initiatives::findOne($i->id);
            $initiatives->total_budget = $total_budget;
            if($initiatives->save(false)){
                echo $i->id . ' - ' . $i->initiatives_code . ' = ' . $total_budget . PHP_EOL;
            }
        }
        
    }

    public function actionSetGrade(){
        $initiatives = Initiatives::find()->where("implementation ilike '%2024'")->andWhere("controllability_index IS NOT NULL")->andWhere("validity_index IS NOT NULL")->all();
        foreach($initiatives as $i){
            $i->grade = ($i->controllability_index + $i->validity_index) / 2;
            if($i->save(false)){
                echo $i->initiatives_code . ' updated' . PHP_EOL;
            }
        }
    }

    public function actionSetAchieveValue(){

        $evaluations = Evaluation::find()->where(['year' => [date('Y'), date('Y') - 1]])->all();

        foreach($evaluations as $evaluation){
            echo $evaluation->year . ' - ' . $evaluation->period . PHP_EOL;

            $okrEvaluations = OkrEvaluation::find()
                                                ->alias('oe')
                                                ->joinWith('okr o')
                                                ->where(['evaluation_id' => $evaluation->id])
                                                ->andWhere(['okr_level_id' => [OkrLevel::INDIVIDUAL, OkrLevel::DIVISION, OkrLevel::DEPARTMENT]])
                                                ->andWhere("unit_structure_id IS NULL")
                                                ->all();

            foreach($okrEvaluations as $oe){
                if($oe->target) $oe->achieve_value = ($oe->real / $oe->target) * 100;
                if($oe->save(false)){
                    echo $oe->id . PHP_EOL;
                    var_dump($oe->real);
                    var_dump($oe->target);
                }
            }
        }

    }

    public function actionIndividualOkr()
    {

        $okrs = Okr::find()
                        ->where(['okr_level_id' => OkrLevel::INDIVIDUAL])
                        ->andWhere(['year' => 2025])
                        ->andWhere(['type' => 'KR'])
                        ->andWhere("pic_id IS NOT NULL")
                        ->all();
                
        $i = 0;
        foreach($okrs as $okr){
            // $okr->unit_structure_id = null;
            // if($okr->save(false)){
                echo $i . ' - ' . $okr->id . PHP_EOL;
            // }
            $i++;
        }

    }
}
