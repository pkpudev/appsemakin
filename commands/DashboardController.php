<?php

namespace app\commands;

use app\models\Activity;
use app\models\EmployeeAchievement;
use app\models\Evaluation;
use app\models\Initiatives;
use app\models\InitiativesEvaluation;
use app\models\Okr;
use app\models\OkrEvaluation;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\UnitAchievement;
use yii\console\Controller;

class DashboardController extends Controller
{

    // public $evaluationId = 25;

    public function actionBeforeRun()
    {
        // $this->actionOkrEvaluationTargetUpdate();
        // $this->actionOkrEvaluationWeightUpdate();
        // $this->actionEmployeeWeight();
        // $this->actionOkrUnitWeight();
        // $this->actionOkrEvaluationWeight();
        // $this->actionInitiativesUnitWeight();
        // $this->actionInitiativesEvaluationWeight();

        $this->actionInitiativesUnitWeight();

        $evaluations = Evaluation::find()
                                    ->where(['year' => [date('Y')]])
                                    ->andWhere($id ? ['id' => $id] : '1=1')
                                    ->orderBy('id asc')
                                    ->all();

        foreach($evaluations as $evaluation){
            $this->actionOkrEvaluationWeightUpdate($evaluation->id);
            $this->actionInitiativesEvaluationWeight($evaluation->id);
        }
    }
    
    public function actionRun($id = null)
    {
        $startTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);

        $evaluations = Evaluation::find()
                                    ->where($id ? "1 = 1" : ['year' => [date('Y') - 1/* , date('Y') - 1 */]])
                                    ->andWhere($id ? ['id' => $id] : '1=1')
                                    ->orderBy('id asc')
                                    ->all();

        foreach($evaluations as $evaluation){
            echo "=============================================================================" . PHP_EOL;
            echo 'Evaluation for ' . $evaluation->year . ' period: ' . $evaluation->period . PHP_EOL;

            $this->actionOkr($evaluation->id);
            // $this->actionActivity();
            $this->actionOkrUnit($evaluation->id);
            $this->actionInitiativesUnit($evaluation->id);
        }
        
        $endTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);
        $interval = date_diff(date_create($startTime), date_create($endTime));
        echo 'Time : ' . $interval->format('%i') . ' menit' . PHP_EOL;
    }

    /* Capaian Individu */
    /* public function actionOkrEvaluationTargetUpdate()
    {
        echo "--get data target".PHP_EOL;
        $okrs = \Yii::$app->db->createCommand("SELECT id, COALESCE(target_q1, 0) as target_q1
            FROM management.okr o
            WHERE type = 'KR' AND okr_level_id <> 1")->queryAll();
        
        echo "--extracting data target".PHP_EOL;
        foreach ($okrs as $okr) {
            $okr_id = $okr['id'];
            $target_q1 = $okr['target_q1'];
            
            echo "---get okr evaluation : {$okr_id}".PHP_EOL;
            $okrEvaluation = OkrEvaluation::findOne(['okr_id'=>$okr_id]);
            if ($okrEvaluation) {
                echo "----check if target evaluation : {$okrEvaluation->id} <> target okr : {$okr_id}".PHP_EOL;
                if ($okrEvaluation->target <> $target_q1) {
                    $okrEvaluation->target = $target_q1;
                    if ($okrEvaluation->save(false)) {
                        echo "-----updated target evaluation : {$okrEvaluation->id}".PHP_EOL;
                    } else {
                        echo "-----target evaluation : {$okrEvaluation->id} not updated".PHP_EOL;
                    }
                } else {
                    echo "----target evaluation : {$okrEvaluation->id} == target okr : {$okr_id}".PHP_EOL;
                }
            } else {
                echo "---okr : {$okr_id} not have evaluation".PHP_EOL;
            }
        }
    } */

    public function actionOkrEvaluationWeightUpdate($evaluationId)
    {
        echo "--get data id, okr_id, weight, position_structure_id".PHP_EOL;
        $okrs = \Yii::$app->db->createCommand("SELECT oe.id, oe.okr_id, o.weight, o.pic_id, o.position_structure_id
            FROM management.okr_evaluation oe
            LEFT JOIN management.okr o ON o.id = oe.okr_id
            WHERE evaluation_id = {$evaluationId} AND okr_level_id = 5 AND type = 'KR' AND o.pic_id IS NOT NULL AND o.position_structure_id IS NOT NULL AND o.status_id IS NULL")->queryAll();
        
        echo "--extracting data".PHP_EOL;
        foreach ($okrs as $okr) {
            $okr_evaluation_id = $okr['id'];
            $okr_id = $okr['okr_id'];
            $weight = $okr['weight'];
            $pic_id = $okr['pic_id'];
            $position_structure_id = $okr['position_structure_id'];
            
            echo "---get total weight".PHP_EOL;
            $total_weight = \Yii::$app->db->createCommand("SELECT SUM(o.weight) as total_weight
                FROM management.okr o
                INNER JOIN management.okr_evaluation oe ON o.id = oe.okr_id
                WHERE oe.evaluation_id = {$evaluationId} AND o.pic_id = $pic_id AND o.position_structure_id = $position_structure_id AND okr_level_id = 5 AND type = 'KR' AND o.status_id IS NULL")->queryScalar();

            echo "---set calculate weight".PHP_EOL;
            if ($total_weight > 0) {
                $weight = ($weight / $total_weight) * 100;
                $weight = round($weight, 2);
            } else {
                $weight = 0;
            }
            
            echo "---check okr evaluation : {$okr_evaluation_id}".PHP_EOL;
            $okr_evaluation = OkrEvaluation::findOne($okr_evaluation_id);
            $okr_evaluation->weight = $weight;
            if ($okr_evaluation->save(false)) {
                echo "----weight okr evaluation : {$okr_evaluation_id} updated".PHP_EOL;
            }
        }
    }

    public function actionOkr($evaluationId)
    {
        echo "--get data pic_id, position_structure_id".PHP_EOL;
        $okrPositions = \Yii::$app->db->createCommand("SELECT o.pic_id, o.position_structure_id
            FROM management.okr_evaluation oe
            LEFT JOIN management.okr o ON o.id = oe.okr_id
            WHERE o.type = 'KR' AND okr_level_id <> 1 AND o.pic_id IS NOT NULL AND oe.evaluation_id = $evaluationId AND o.position_structure_id IS NOT NULL
            GROUP BY o.pic_id, o.position_structure_id
            ORDER BY o.pic_id")->queryAll();
        
        echo "--extracting data".PHP_EOL;
        foreach ($okrPositions as $okrPosition) {
            $pic_id = $okrPosition['pic_id'];
            $position_structure_id = $okrPosition['position_structure_id'];
            
            echo "---get data for pic : {$pic_id} - pos : {$position_structure_id}".PHP_EOL;

            $filterOkrEvaluation = "o.type = 'KR' AND 
                                    okr_level_id <> 1 AND 
                                    o.pic_id = $pic_id AND 
                                    o.position_structure_id = $position_structure_id AND 
                                    oe.evaluation_id = $evaluationId AND 
                                    oe.status_id IN (11,12) AND 
                                    COALESCE(oe.target::numeric, 0) <> 0";
            
            $okrEvaluationWeight = \Yii::$app->db->createCommand("SELECT SUM(COALESCE(oe.weight, 0))
                FROM management.okr_evaluation oe 
                LEFT JOIN management.okr o on o.id = oe.okr_id 
                WHERE $filterOkrEvaluation")->queryScalar() ?: 0;

            if($okrEvaluationWeight > 0){
                $okrEvaluation = \Yii::$app->db->createCommand("SELECT o.pic_id, o.position_structure_id, 
                    -- SUM((((COALESCE(oe.real::numeric, 0) / COALESCE(oe.target::numeric, 0)) * 100) * COALESCE(oe.weight, 0)) / $okrEvaluationWeight) as value
                    SUM((COALESCE(oe.achieve_value::numeric, 0) * COALESCE(oe.weight, 0)) / $okrEvaluationWeight) as value
                    FROM management.okr_evaluation oe 
                    LEFT JOIN management.okr o on o.id = oe.okr_id 
                    WHERE $filterOkrEvaluation
                    GROUP BY o.pic_id, o.position_structure_id")->queryOne();
    
                echo "---set achieve_value".PHP_EOL;
                $value = (float) $okrEvaluation['value'];
    
                $achieve_value = round($value, 2);
            } else {
                $achieve_value = 0;
            }

            // var_dump($achieve_value);

            echo "----get contribution_amount".PHP_EOL;
            $contribution_amount = round(\Yii::$app->db->createCommand("SELECT count(*) 
                FROM management.initiatives_evaluation ie 
                LEFT JOIN management.initiatives i ON i.id = ie.initiatives_id
                LEFT JOIN management.okr o ON o.id = i.okr_id
                WHERE employees_involved ilike '%$pic_id%' AND ie.evaluation_id = $evaluationId")->queryScalar(), 2);
            
            $countOkr = \Yii::$app->db->createCommand("SELECT count(*)
                FROM management.okr_evaluation oe 
                LEFT JOIN management.okr o on o.id = oe.okr_id
                WHERE o.type = 'KR' AND okr_level_id <> 1 AND o.pic_id = $pic_id AND o.position_structure_id = $position_structure_id AND oe.evaluation_id = $evaluationId")->queryScalar();

            $countDisetujui = \Yii::$app->db->createCommand("SELECT count(*)
                FROM management.okr_evaluation oe 
                LEFT JOIN management.okr o on o.id = oe.okr_id
                WHERE o.type = 'KR' AND okr_level_id <> 1 AND o.pic_id = $pic_id AND o.position_structure_id = $position_structure_id AND oe.evaluation_id = $evaluationId AND oe.status_id IN (11,12)")->queryScalar();

            echo "----set fulfillment".PHP_EOL;
            if ($countOkr > 0) {
                $fulfillment = round(($countDisetujui / $countOkr) * 100, 2);
            } else {
                $fulfillment = 0;
            }
            
            echo "----check employee achievement".PHP_EOL;
            $checkEmployeeAchievement = EmployeeAchievement::find()
            ->where(['employee_id'=>$pic_id, 'position_structure_id'=>$position_structure_id, 'evaluation_id'=>$evaluationId, 'achieve_type'=>'OKR'])
                ->one();
                
            if ($checkEmployeeAchievement) {
                $checkEmployeeAchievement->achieve_value = $achieve_value;
                $checkEmployeeAchievement->contribution_amount = $contribution_amount;
                $checkEmployeeAchievement->fulfillment = $fulfillment;
                if ($checkEmployeeAchievement->save()) {
                    echo "-----employee achievement : {$checkEmployeeAchievement->id} updated".PHP_EOL;
                }
            } else {
                $employeeAchievement = new EmployeeAchievement();
                $employeeAchievement->employee_id = $pic_id;
                $employeeAchievement->evaluation_id = $evaluationId; //$evaluation->id;
                $employeeAchievement->position_structure_id = $position_structure_id;
                $employeeAchievement->achieve_type = 'OKR';
                $employeeAchievement->achieve_value = $achieve_value;
                $employeeAchievement->contribution_amount = $contribution_amount;
                $employeeAchievement->fulfillment = $fulfillment;
                if ($employeeAchievement->save()) {
                    echo "-----new employee achievement : {$employeeAchievement->id} saved".PHP_EOL;
                }
            }
        }
    }

    public function actionEmployeeWeight()
    {
        // $year = date('Y');
        $year = 2022;

        echo "--get data pic_id, position_structure_id".PHP_EOL;
        $actPositions = \Yii::$app->db->createCommand("SELECT a.pic_id, a.position_structure_id
            FROM management.activity a
            LEFT JOIN management.initiatives i ON i.id = a.initiatives_id
            LEFT JOIN management.okr o ON o.id = i.okr_id
            WHERE o.year = $year AND a.position_structure_id IS NOT NULL
            GROUP BY a.pic_id, a.position_structure_id
            ORDER BY a.pic_id")->queryAll();
        
        echo "--extracting data".PHP_EOL;
        foreach ($actPositions as $actPosition) {
            $pic_id = $actPosition['pic_id'];
            $position_structure_id = $actPosition['position_structure_id'];
            
            echo "---get sum weight value".PHP_EOL;
            $sumWeightValue = \Yii::$app->db->createCommand("SELECT sum(a.weight_value) 
                FROM management.activity a 
                LEFT JOIN management.initiatives i ON i.id = a.initiatives_id
                LEFT JOIN management.okr o ON o.id = i.okr_id
                WHERE a.status_id NOT IN (5, 6) AND a.pic_id = $pic_id AND a.position_structure_id = $position_structure_id AND o.year=$year")->queryScalar();

            echo "---get weight value".PHP_EOL;
            $actEmployees = \Yii::$app->db->createCommand("SELECT a.id, weight_value
                FROM management.activity a 
                LEFT JOIN management.initiatives i ON i.id = a.initiatives_id
                LEFT JOIN management.okr o ON o.id = i.okr_id
                WHERE a.status_id NOT IN (5, 6) AND a.pic_id = $pic_id AND a.position_structure_id = $position_structure_id AND o.year=$year")->queryAll();

            echo "---extracting weight value".PHP_EOL;
            foreach ($actEmployees as $actEmployee) {
                $activity_id = $actEmployee['id'];
                echo "----set calculate employee weight activity : {$activity_id}".PHP_EOL;
                $employee_weight = round(($actEmployee['weight_value'] * 100) / $sumWeightValue, 2);
                
                echo "----check activity : {$activity_id}".PHP_EOL;
                $activity = Activity::findOne($activity_id);
                $activity->employee_weight = $employee_weight;
                if ($activity->save(false)) {
                    echo "-----employee weight activity : {$activity->id} updated".PHP_EOL;
                } else {
                    echo "-----employee weight activity : {$activity->id} not updated".PHP_EOL;
                }
            }
        }
    }

    public function actionActivity()
    {
        // $year = date('Y');
        $year = 2022;
        $evaluationId = $this->evaluationId;

        echo "--get data pic_id, position_structure_id".PHP_EOL;
        $actPositions = \Yii::$app->db->createCommand("SELECT a.pic_id, a.position_structure_id
            FROM management.activity a
            LEFT JOIN management.initiatives i ON i.id = a.initiatives_id
            LEFT JOIN management.okr o ON o.id = i.okr_id
            WHERE o.year = $year AND a.position_structure_id IS NOT NULL
            GROUP BY a.pic_id, a.position_structure_id
            ORDER BY a.pic_id")->queryAll();
        
        echo "--extracting data".PHP_EOL;
        foreach ($actPositions as $actPosition) {
            $pic_id = $actPosition['pic_id'];
            $position_structure_id = $actPosition['position_structure_id'];

            echo "---get data for pic : {$pic_id} - pos : {$position_structure_id}".PHP_EOL;
            echo "----get actual_value, employee_weight".PHP_EOL;    
            $actEmployees = \Yii::$app->db->createCommand("SELECT COALESCE(a.actual_value,0) as actual_value, COALESCE(a.employee_weight,0) as employee_weight
                FROM management.activity a
                LEFT JOIN management.initiatives i ON i.id = a.initiatives_id
                LEFT JOIN management.okr o ON o.id = i.okr_id
                WHERE a.status_id NOT IN (5, 6) AND a.pic_id = $pic_id AND a.position_structure_id = $position_structure_id AND o.year=$year")->queryAll();
            
            echo "----set achieve_value".PHP_EOL;
            $achieve_value = 0;
            foreach ($actEmployees as $actEmployee) {
                $actual_value = $actEmployee['actual_value'];
                $employee_weight = $actEmployee['employee_weight'];
    
                $value = ($employee_weight * $actual_value) / 100;
                $achieve_value = $achieve_value + $value;            
            }
            
            $achieve_value = round($achieve_value, 2);
            
            echo "----set fulfillment".PHP_EOL;
            $countActual = \Yii::$app->db->createCommand("SELECT count(*)
                FROM management.activity a 
                LEFT JOIN management.initiatives i ON i.id = a.initiatives_id
                LEFT JOIN management.okr o ON o.id = i.okr_id
                WHERE a.status_id NOT IN (5, 6) AND a.pic_id = $pic_id AND a.position_structure_id = $position_structure_id AND o.year=$year AND COALESCE(actual_value, 0) > 0")->queryScalar();

            $fulfillment = $countActual;

            echo "----check employee achievement".PHP_EOL;
            $checkEmployeeAchievement = EmployeeAchievement::find()
                ->where(['employee_id'=>$pic_id, 'position_structure_id'=>$position_structure_id, 'evaluation_id'=>$this->evaluationId, 'achieve_type'=>'Activity'])
                ->one();

            if ($checkEmployeeAchievement) {
                $checkEmployeeAchievement->achieve_value = $achieve_value;
                $checkEmployeeAchievement->fulfillment = $fulfillment;
                if ($checkEmployeeAchievement->save()) {
                    echo "-----employee achievement : {$checkEmployeeAchievement->id} updated".PHP_EOL;
                }
            } else {
                $employeeAchievement = new EmployeeAchievement();
                $employeeAchievement->employee_id = $pic_id;
                $employeeAchievement->evaluation_id = $evaluationId; //$evaluation->id;
                $employeeAchievement->position_structure_id = $position_structure_id;
                $employeeAchievement->achieve_type = 'Activity';
                $employeeAchievement->achieve_value = $achieve_value;
                $employeeAchievement->fulfillment = $fulfillment;
                if ($employeeAchievement->save()) {
                    echo "-----new employee achievement : {$employeeAchievement->id} saved".PHP_EOL;
                }
            }
        }
    }

    /* Capaian Unit Kerja */
    public function actionOkrUnitWeight()
    {
        // $year = date('Y');
        $year = 2023;

        echo "--get data position_structure_id".PHP_EOL;
        $positions = \Yii::$app->db->createCommand("SELECT position_structure_id
            FROM management.okr o
            WHERE o.year = $year AND okr_level_id IN (2,3,4) AND type = 'KR'
            GROUP BY position_structure_id
            ORDER BY position_structure_id")->queryAll();
        
        echo "--extracting data".PHP_EOL;
        foreach ($positions as $position) {
            $position_structure_id = $position['position_structure_id'];

            echo "---get okr_id position : {$position_structure_id}".PHP_EOL;
            $okrs = \Yii::$app->db->createCommand("SELECT id
                FROM management.okr o
                WHERE okr_level_id IN (2,3,4) AND position_structure_id = $position_structure_id AND year=$year AND type = 'KR'")->queryAll();

            foreach ($okrs as $okr) {
                $okr_id = $okr['id'];
                
                echo "----get validity_value okr : {$okr_id}".PHP_EOL;
                $validity_value = \Yii::$app->db->createCommand("SELECT v.value 
                    FROM management.okr o 
                    LEFT JOIN management.validity v ON o.validity_id = v.id
                    where o.id = $okr_id")->queryScalar();
                
                if (is_null($validity_value)) {
                    $validity_value = 0;
                }
                
                echo "----get total_validity_value position : {$position_structure_id}".PHP_EOL;
                $total_validity_value = \Yii::$app->db->createCommand("SELECT sum(v.value)
                    from management.okr o 
                    LEFT JOIN management.validity v ON o.validity_id = v.id
                    where o.position_structure_id = $position_structure_id AND okr_level_id IN (2,3,4) AND o.year=$year AND type = 'KR'")->queryScalar();

                echo "----set calculate validity_index".PHP_EOL;
                if($total_validity_value){
                    $validity_index = ($validity_value * 100) / $total_validity_value;
                    $validity_index = round($validity_index, 2);
                } else {
                    $validity_index = 0;
                }

                echo "----get controllability okr : {$okr_id}".PHP_EOL;
                $controllability_value = \Yii::$app->db->createCommand("SELECT c.value 
                    FROM management.okr o 
                    LEFT JOIN management.controllability c ON o.controllability_id = c.id
                    where o.id = $okr_id")->queryScalar();
            
                if (is_null($controllability_value)) {
                    $controllability_value = 0;
                }
                
                echo "----get total_controllability_value position : {$position_structure_id}".PHP_EOL;
                $total_controllability_value = \Yii::$app->db->createCommand("SELECT sum(c.value)
                    from management.okr o 
                    LEFT JOIN management.controllability c ON o.controllability_id = c.id
                    where o.position_structure_id = $position_structure_id AND okr_level_id IN (2,3,4) AND o.year=$year AND type = 'KR'")->queryScalar();

                echo "----set calculate controllability_index".PHP_EOL;
                if($total_controllability_value){
                    $controllability_index = ($controllability_value * 100) / $total_controllability_value;
                    $controllability_index = round($controllability_index, 2);
                } else {
                    $controllability_index = 0;
                }
                
                echo "----set calculate weight".PHP_EOL;
                $weight = ($validity_index + $controllability_index) / 2;
                $weight = round($weight, 2);
                
                echo "----check okr : {$okr_id}".PHP_EOL;
                $okr = Okr::findOne($okr_id);
                $okr->weight = $weight;
                if ($okr->save(false)) {
                    echo "-----weight okr : {$okr_id} updated".PHP_EOL;
                } else {
                    echo "-----weight okr : {$okr_id} not updated".PHP_EOL;
                }
            }
        }
    }

    public function actionOkrEvaluationWeight()
    {
        echo "--get data id, okr_id, weight, position_structure_id".PHP_EOL;
        $okrs = \Yii::$app->db->createCommand("SELECT oe.id, oe.okr_id, o.weight, o.position_structure_id, o.pic_id
            FROM management.okr_evaluation oe
            LEFT JOIN management.okr o ON o.id = oe.okr_id
            WHERE evaluation_id = {$this->evaluationId} AND okr_level_id IN (2,3,4,5) AND type = 'KR' AND o.status_id IS NULL")->queryAll();
        
        echo "--extracting data".PHP_EOL;
        foreach ($okrs as $okr) {
            $okr_evaluation_id = $okr['id'];
            $okr_id = $okr['okr_id'];
            $weight = $okr['weight'];
            $position_structure_id = $okr['position_structure_id'];
            $pic_id = $okr['pic_id'];
            
            echo "---get total weight position : {$position_structure_id}".PHP_EOL;
            $total_weight = \Yii::$app->db->createCommand("SELECT SUM(o.weight) as total_weight
                FROM management.okr o
                INNER JOIN management.okr_evaluation oe ON o.id = oe.okr_id
                WHERE oe.evaluation_id = {$this->evaluationId} AND o.position_structure_id = $position_structure_id AND okr_level_id IN (2,3,4,5) AND type = 'KR' AND oe.pic_id = $pic_id AND o.status_id IS NULL")->queryScalar();

            echo "---set calculate weight".PHP_EOL;
            if ($total_weight > 0) {
                $weight = ($weight / $total_weight) * 100;
                $weight = round($weight, 2);
            } else {
                $weight = 0;
            }
            
            echo "---check okr evaluation : {$okr_evaluation_id}".PHP_EOL;
            $okr_evaluation = OkrEvaluation::findOne($okr_evaluation_id);
            $okr_evaluation->weight = $weight;
            if ($okr_evaluation->save(false)) {
                echo "----weight okr evaluation : {$okr_evaluation_id} updated".PHP_EOL;
            }
        }
    }

    public function actionInitiativesUnitWeight()
    {
        $year = date('Y');
        // $year = 2022;
        
        echo "--get data position_structure_id".PHP_EOL;
        $positions = \Yii::$app->db->createCommand("SELECT i.position_structure_id
            FROM management.initiatives i
            LEFT JOIN management.okr o ON o.id = i.okr_id
            WHERE o.year = $year AND i.status_id NOT IN (5,6)
            GROUP BY i.position_structure_id
            ORDER BY i.position_structure_id")->queryAll();
        
        echo "--extracting data".PHP_EOL;
        foreach ($positions as $position) {
            $position_structure_id = $position['position_structure_id'];
            
            echo "---get initiatives id".PHP_EOL;
            $initiatives = \Yii::$app->db->createCommand("SELECT i.id
                FROM management.initiatives i
                LEFT JOIN management.okr o ON o.id = i.okr_id
                WHERE i.status_id NOT IN (5,6) AND i.position_structure_id = $position_structure_id AND o.year=$year AND i.id NOT IN (49,597,831,385,695)")->queryAll();

            foreach ($initiatives as $initiative) {
                $initiatives_id = $initiative['id'];
                
                echo "----get validity_value initiatives : {$initiatives_id}".PHP_EOL;
                $validity_value = \Yii::$app->db->createCommand("SELECT v.value 
                    FROM management.initiatives i 
                    LEFT JOIN management.validity v ON i.validity_id = v.id
                    where i.id = $initiatives_id")->queryScalar();
                
                if (is_null($validity_value)) {
                    $validity_value = 0;
                }
                
                echo "----get total_validity_value initiatives : {$initiatives_id}".PHP_EOL;
                $total_validity_value = \Yii::$app->db->createCommand("SELECT sum(v.value)
                    from management.initiatives i 
                    LEFT JOIN management.validity v ON i.validity_id = v.id
                    LEFT JOIN management.okr o ON i.okr_id = o.id
                    where i.position_structure_id = $position_structure_id AND i.status_id NOT IN (5,6) AND o.year=$year")->queryScalar();

                echo "----set calculate validity_index".PHP_EOL;
                $validity_index = ($validity_value * 100) / $total_validity_value;
                $validity_index = round($validity_index, 2);
                
                echo "----get controllability_value initiatives : {$initiatives_id}".PHP_EOL;
                $controllability_value = \Yii::$app->db->createCommand("SELECT c.value 
                    FROM management.initiatives i 
                    LEFT JOIN management.controllability c ON i.controllability_id = c.id
                    where i.id = $initiatives_id")->queryScalar();
            
                if (is_null($controllability_value)) {
                    $controllability_value = 0;
                }
                
                echo "----get total_controllability_value initiatives : {$initiatives_id}".PHP_EOL;
                $total_controllability_value = \Yii::$app->db->createCommand("SELECT sum(c.value)
                    from management.initiatives i 
                    LEFT JOIN management.controllability c ON i.controllability_id = c.id
                    LEFT JOIN management.okr o ON i.okr_id = o.id
                    where i.position_structure_id = $position_structure_id AND i.status_id NOT IN (5,6) AND o.year=$year")->queryScalar();

                echo "----set calculate controllability_index".PHP_EOL;
                $controllability_index = ($controllability_value * 100) / $total_controllability_value;
                $controllability_index = round($controllability_index, 2);
                
                echo "----set calculate unit_weight".PHP_EOL;
                $unit_weight = ($validity_index + $controllability_index) / 2;
                $unit_weight = round($unit_weight, 2);
                
                echo "----check initiatives : {$initiatives_id}".PHP_EOL;
                $initiatives = Initiatives::findOne($initiatives_id);
                $initiatives->unit_weight = $unit_weight;
                if ($initiatives->save(false)) {
                    echo "-----unit weight initiatives : {$initiatives_id} updated".PHP_EOL;
                }
            }
        }
    }

    public function actionInitiativesEvaluationWeight($evaluationId)
    {
        echo "--get data id, initiatives_id, unit_weight, position_structure_id".PHP_EOL;
        $evaluations = \Yii::$app->db->createCommand("SELECT ie.id, initiatives_id, i.unit_weight, i.position_structure_id
            FROM management.initiatives_evaluation ie
            LEFT JOIN management.initiatives i ON i.id = ie.initiatives_id
            WHERE evaluation_id = {$evaluationId}")->queryAll();
        
        echo "--extracting data".PHP_EOL;
        foreach ($evaluations as $evaluation) {
            $initiatives_evaluation_id = $evaluation['id'];
            $initiatives_id = $evaluation['initiatives_id'];
            $unit_weight = $evaluation['unit_weight'];
            $position_structure_id = $evaluation['position_structure_id'];
            
            echo "---get total_unit_weight position : {$position_structure_id}".PHP_EOL;
            $total_unit_weight = \Yii::$app->db->createCommand("SELECT SUM(i.unit_weight) as total_unit_weight
                FROM management.initiatives i
                INNER JOIN management.initiatives_evaluation ie ON i.id = ie.initiatives_id
                WHERE evaluation_id = {$evaluationId} AND i.position_structure_id = $position_structure_id")->queryScalar();

            echo "---set calculate weight".PHP_EOL;
            if ($total_unit_weight > 0) {
                $weight = ($unit_weight / $total_unit_weight) * 100;
                $weight = round($weight, 2);
            } else {
                $weight = 0;
            }

            echo "---check initiatives evaluation : {$initiatives_evaluation_id}".PHP_EOL;
            $initiatives_evaluation = InitiativesEvaluation::findOne($initiatives_evaluation_id);
            $initiatives_evaluation->weight = $weight;
            if ($initiatives_evaluation->save(false)) {
                echo "----weight initiatives evaluation : {$initiatives_evaluation_id} updated".PHP_EOL;
            }
        }
    }
    public function actionOkrUnit($evaluationId)
    {
        echo "--get data position_structure_id".PHP_EOL;
        $okrUnits = \Yii::$app->db->createCommand("SELECT 
                o.position_structure_id
            FROM management.okr_evaluation oe
            LEFT JOIN management.okr o on o.id = oe.okr_id
            WHERE 
                oe.evaluation_id = {$evaluationId} AND type = 'KR' AND okr_level_id IN (2,3,4)
            GROUP BY o.position_structure_id
            ORDER BY o.position_structure_id")->queryAll();
        
        echo "--extracting data".PHP_EOL;
        foreach ($okrUnits as $okrUnit) {
            $position_structure_id = $okrUnit['position_structure_id'];
            
            echo "---get value position : {$position_structure_id}".PHP_EOL;
            
            $filterOkrEvaluation = "oe.evaluation_id = {$evaluationId} AND 
                                    o.type = 'KR' AND 
                                    okr_level_id IN (2,3,4) AND 
                                    o.position_structure_id = $position_structure_id AND 
                                    oe.status_id IN (11,12) AND 
                                    COALESCE(oe.target::numeric, 0) <> 0";
            
            $okrEvaluationWeight = \Yii::$app->db->createCommand("SELECT SUM(COALESCE(oe.weight, 0))
                FROM management.okr_evaluation oe 
                LEFT JOIN management.okr o on o.id = oe.okr_id 
                WHERE $filterOkrEvaluation")->queryScalar() ?: 0;

            if ($okrEvaluationWeight > 0) {
                $okrEvaluation = \Yii::$app->db->createCommand("SELECT o.position_structure_id, 
                    -- SUM((((COALESCE(oe.real::numeric, 0) / COALESCE(oe.target::numeric, 0)) * 100) * COALESCE(oe.weight, 0)) / $okrEvaluationWeight) as value
                    SUM((COALESCE(oe.achieve_value::numeric, 0) * COALESCE(oe.weight, 0)) / $okrEvaluationWeight) as value
                    FROM management.okr_evaluation oe 
                    LEFT JOIN management.okr o on o.id = oe.okr_id 
                    WHERE $filterOkrEvaluation
                    GROUP BY o.position_structure_id")->queryOne();
    
                echo "---set achieve_value".PHP_EOL;
                $value = (float) $okrEvaluation['value'];
                
                $achieve_value = round($value, 2);
            } else {
                $achieve_value = 0;
            }

            
            $countOkr = \Yii::$app->db->createCommand("SELECT 
                    count(*)
                FROM 
                    management.okr_evaluation oe 
                LEFT JOIN management.okr o on o.id = oe.okr_id
                WHERE 
                    evaluation_id = {$evaluationId} AND 
                    o.type = 'KR' AND 
                    okr_level_id IN (2,3,4) AND 
                    o.position_structure_id = $position_structure_id")->queryScalar();

            $countDisetujui = \Yii::$app->db->createCommand("SELECT count(*)
                FROM management.okr_evaluation oe 
                LEFT JOIN management.okr o on o.id = oe.okr_id
                WHERE evaluation_id = {$evaluationId} AND o.type = 'KR' AND okr_level_id IN (2,3,4) AND o.position_structure_id = $position_structure_id AND oe.status_id IN (11,12)")->queryScalar();

            echo "---set fulfillment".PHP_EOL;
            if ($countOkr > 0) {
                $fulfillment = round(($countDisetujui / $countOkr) * 100, 2);
            } else {
                $fulfillment = 0;
            }
            
            echo "---set unit achievement position : {$position_structure_id}".PHP_EOL;
            $checkUnitAchievement = UnitAchievement::find()
                ->where(['unit_id'=>$position_structure_id, 'evaluation_id'=>$evaluationId, 'achieve_type'=>'OKR'])
                ->one();

            if ($checkUnitAchievement) {
                $checkUnitAchievement->achieve_value = $achieve_value;
                $checkUnitAchievement->fulfillment = $fulfillment;
                if ($checkUnitAchievement->save()) {
                    echo "----unit achievement {$checkUnitAchievement->id} updated".PHP_EOL;
                }
            } else {
                $unitAchievement = new UnitAchievement();
                $unitAchievement->unit_id = $position_structure_id;
                $unitAchievement->evaluation_id = $evaluationId; //$evaluation->id;
                $unitAchievement->achieve_type = 'OKR';
                $unitAchievement->achieve_value = $achieve_value;
                $unitAchievement->fulfillment = $fulfillment;
                if ($unitAchievement->save()) {
                    echo "----new unit achievement {$unitAchievement->id} saved".PHP_EOL;
                }
            }

            var_dump($achieve_value);
        }
        
    }

    public function actionInitiativesUnit($evaluationId)
    {
        $year = Evaluation::findOne($evaluationId)->year;
        
        echo "--get data position_structure_id".PHP_EOL;
        $positions = \Yii::$app->db->createCommand("SELECT i.position_structure_id
            FROM management.initiatives i
            LEFT JOIN management.okr o ON o.id = i.okr_id
            WHERE 
                o.year = $year AND 
                i.status_id NOT IN (5,6)
            GROUP BY i.position_structure_id
            ORDER BY i.position_structure_id")->queryAll();
        
        echo "--extracting data".PHP_EOL;
        foreach ($positions as $position) {
            $position_structure_id = $position['position_structure_id'];
            
            echo "---get value position : {$position_structure_id}".PHP_EOL;

            $filterInitiativesEvaluation = "evaluation_id = {$evaluationId} AND 
                                            i.status_id NOT IN (5,6) AND 
                                            i.position_structure_id = $position_structure_id AND 
                                            o.year = $year AND 
                                            ie.status_id IN (11,12) AND 
                                            COALESCE(ie.target::numeric, 0) <> 0";
            
            $initiativesEvaluationWeight = \Yii::$app->db->createCommand("SELECT SUM(COALESCE(ie.weight, 0))
                FROM management.initiatives_evaluation ie 
                LEFT JOIN management.initiatives i on i.id = ie.initiatives_id 
                LEFT JOIN management.okr o on o.id = i.okr_id 
                WHERE $filterInitiativesEvaluation")->queryScalar() ?: 0;

            $initiativesEvaluation = \Yii::$app->db->createCommand("SELECT 
                    i.position_structure_id, 
                    SUM((((COALESCE(ie.real::numeric, 0) / COALESCE(ie.target::numeric, 0)) * 100) * ie.weight) / $initiativesEvaluationWeight) as value
                FROM 
                    management.initiatives_evaluation ie 
                LEFT JOIN management.initiatives i on i.id = ie.initiatives_id 
                LEFT JOIN management.okr o on o.id = i.okr_id 
                WHERE $filterInitiativesEvaluation
                GROUP BY 
                    i.position_structure_id
                ")->queryOne();

            echo "---set achieve_value".PHP_EOL;
            $value = (float) $initiativesEvaluation['value'];
            
            $achieve_value = round($value, 2);
            
            $countInitiatives = \Yii::$app->db->createCommand("SELECT count(*)
                FROM management.initiatives_evaluation ie 
                LEFT JOIN management.initiatives i on i.id = ie.initiatives_id 
                LEFT JOIN management.okr o on o.id = i.okr_id 
                WHERE evaluation_id = {$evaluationId} AND i.status_id NOT IN (5,6) AND i.position_structure_id = $position_structure_id AND o.year = $year")->queryScalar();

            $countDisetujui = \Yii::$app->db->createCommand("SELECT count(*)
                FROM management.initiatives_evaluation ie
                LEFT JOIN management.initiatives i on i.id = ie.initiatives_id 
                LEFT JOIN management.okr o on o.id = i.okr_id 
                WHERE evaluation_id = {$evaluationId} AND i.status_id NOT IN (5,6) AND i.position_structure_id = $position_structure_id AND o.year = $year AND ie.status_id IN (11,12)")->queryScalar();

            echo "---set fulfillment".PHP_EOL;
            if ($countInitiatives > 0) {
                $fulfillment = round(($countDisetujui / $countInitiatives) * 100, 2);
            } else {
                $fulfillment = 0;
            }
            
            echo "---set unit achievement position : {$position_structure_id}".PHP_EOL;
            $checkUnitAchievement = UnitAchievement::find()
                ->where(['unit_id'=>$position_structure_id, 'evaluation_id'=>$evaluationId, 'achieve_type'=>'IN'])
                ->one();
                
            if ($checkUnitAchievement) {
                $checkUnitAchievement->achieve_value = $achieve_value;
                $checkUnitAchievement->fulfillment = $fulfillment;
                $checkUnitAchievement->save();
                if ($checkUnitAchievement->save()) {
                    echo "----unit achievement {$checkUnitAchievement->id} updated".PHP_EOL;
                }
            } else {
                $unitAchievement = new UnitAchievement();
                $unitAchievement->unit_id = $position_structure_id;
                $unitAchievement->evaluation_id = $evaluationId; //$evaluation->id;
                $unitAchievement->achieve_type = 'IN';
                $unitAchievement->achieve_value = $achieve_value;
                $unitAchievement->fulfillment = $fulfillment;
                if ($unitAchievement->save()) {
                    echo "----new unit achievement {$unitAchievement->id} saved".PHP_EOL;
                } else {
                    echo "----new unit achievement {$unitAchievement->id} not saved".PHP_EOL;
                }
            }
        }
    }
}