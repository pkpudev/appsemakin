<?php

namespace app\commands;

use app\models\Activities;
use app\models\Activity;
use app\models\ActivityEvaluation;
use app\models\ActivityEvaluationHistory;
use app\models\Evaluation;
use app\models\Initiatives;
use app\models\InitiativesEvaluation;
use app\models\InitiativesEvaluationHistory;
use app\models\Okr;
use app\models\OkrEvaluation;
use app\models\OkrEvaluationHistory;
use app\models\OkrLevel;
use app\models\OkrOrg;
use app\models\Polarization;
use app\models\Status;
use Yii;
use yii\console\Controller;

class EvaluationController extends Controller
{
    
    public function actionGenerate($id = null){
        $startTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);

        $evaluations = Evaluation::find()
                                    ->where($id ? "1=1" : ['year' => [date('Y')/*  -1 */]])
                                    ->andWhere($id ? ['id' => $id] : '1=1')
                                    ->andWhere(['use_old_pattern' => false])
                                    ->orderBy('id asc')
                                    ->all();
                                    var_dump($evaluations);

        foreach($evaluations as $evaluation){
    
            echo "=============================================================================" . PHP_EOL;
            echo 'Evaluation for ' . $evaluation->year . ' period: ' . $evaluation->period . PHP_EOL;

            // $evaluation->status_id = Status::GENERATE_PROSES;
            if($evaluation->save()){
                echo 'Evaluation status: Process' . PHP_EOL;
            }

            $periodStart = $this->getPeriodRange($evaluation->year, $evaluation->period)[0];
            $periodEnd = $this->getPeriodRange($evaluation->year, $evaluation->period)[1];

            $months = $this->getColumnTarget($evaluation->period);
            
            /** - INITIATIVES */
            $initiatives = Initiatives::find()
                                        ->alias('i')
                                        ->joinWith('okr o')
                                        // ->where("COALESCE(o." . implode(', ', $months) . ") IS NOT NULL")
                                        ->where(['i.status_id' => Status::APPROVED])
                                        // ->andWhere(['i.id' => [1649, 1653, 1654, 1754, 2384, 2389, 2392]]) //contoh
                                        ->orderBy('o.okr_level_id desc')
                                        ->all();
            foreach($initiatives as $initiative){
                foreach($months as $month){

                    $check1 = Okr::find()
                                        ->where(['ref_initiatives_id' => $initiative->id])
                                        ->andWhere("COALESCE(" . implode(', ', $months) . ") IS NOT NULL")
                                        ->one();
                    if($check1){
                        echo '* Check Initatives ' . $initiative->initiatives_code . PHP_EOL;
                        $checkInitiativesEvaluation = InitiativesEvaluation::find()
                                                                                ->where(['initiatives_id' => $initiative->id])
                                                                                ->andWhere(['evaluation_id' => $evaluation->id])
                                                                                ->andWhere('postponed_from is null')
                                                                                ->one();
                        if(!$checkInitiativesEvaluation){
                            echo '** Generating InitiativesEvaluation ' . $month . PHP_EOL;
                            $initiativesEvaluation = new InitiativesEvaluation();
                            $initiativesEvaluation->evaluation_id = $evaluation->id;
                            $initiativesEvaluation->initiatives_id = $initiative->id;
                            
                            $initiativesEvaluation->target = $initiative->okr->$month ?: 0;
                            $initiativesEvaluation->achieve_value = $this->getInitiativesAchieveValue($initiative->id, $evaluation);
                            if($initiative->polarization_id == Polarization::MAXIMIZE){
                                $initiativesEvaluation->real = $initiativesEvaluation->achieve_value * $initiativesEvaluation->target;
                            } else if($initiative->polarization_id == Polarization::MINIMIZE){
                                $initiativesEvaluation->real = $initiativesEvaluation->achieve_value ? ($initiativesEvaluation->target / $initiativesEvaluation->achieve_value) : 0;
                            } else {
                                $initiativesEvaluation->real = $initiativesEvaluation->achieve_value * $initiativesEvaluation->target;
                            }
                            
                            // $initiativesEvaluation->target = $initiative->target ? (($initiative->$month / $initiative->target) * 100) : 0;
                            // $initiativesEvaluation->real = (string) $this->getInitiativesReal($initiative->id, $evaluation);

                            $initiativesEvaluation->weight = $initiative->unit_weight;
                            $initiativesEvaluation->status_id = Status::OKR_E_APPROVE;
                            if($initiativesEvaluation->save()){
                                echo '** Done' . PHP_EOL;
                                $initiativesEvaluationHistory = new InitiativesEvaluationHistory();
                                $initiativesEvaluationHistory->initiatives_evaluation_id = $initiativesEvaluation->id;
                                $initiativesEvaluationHistory->action = 'Membuat Evaluasi Program Kerja';
                                $initiativesEvaluationHistory->created_by = $evaluation->created_by;
                                if($initiativesEvaluationHistory->save()){
                                    echo '** History created' . PHP_EOL;
                                }
                            }
                        } else {
    
                            // $initiativesEvaluation = InitiativesEvaluation::findOne($checkInitiativesEvaluation->id);
                            // if($initiativesEvaluation){
                                $checkInitiativesEvaluation->status_id = Status::OKR_E_APPROVE;
                                
                                $checkInitiativesEvaluation->target = $checkInitiativesEvaluation->initiatives->okr->$month ?: 0;
                                $checkInitiativesEvaluation->achieve_value = $this->getInitiativesAchieveValue($initiative->id, $evaluation);
                                
                                if($initiative->polarization_id == Polarization::MAXIMIZE){
                                    $checkInitiativesEvaluation->real = ($checkInitiativesEvaluation->achieve_value * $checkInitiativesEvaluation->target) / 100;
                                } else if($initiative->polarization_id == Polarization::MINIMIZE){
                                    $checkInitiativesEvaluation->real = ($checkInitiativesEvaluation->achieve_value ? ($checkInitiativesEvaluation->target / $checkInitiativesEvaluation->achieve_value) : 0) / 100;
                                } else {
                                    $checkInitiativesEvaluation->real = ($checkInitiativesEvaluation->achieve_value * $checkInitiativesEvaluation->target) / 100;
                                }
                                
                                // $checkInitiativesEvaluation->target = $initiative->target ? (($initiative->$month / $initiative->target) * 100) : 0;
                                // $checkInitiativesEvaluation->real = (string) $this->getInitiativesReal($initiative->id, $evaluation);

                                $checkInitiativesEvaluation->weight = $initiative->unit_weight;
                                $checkInitiativesEvaluation->save();
        
                                echo '** has been updated' . PHP_EOL;
                            // }
                        }
                    }
                }
            }

            /** - POSTPONED INITIATIVES */
            /* $initiativesPostpone = InitiativesEvaluation::find()->where(['postpone' => $evaluation->period])->all();
            foreach($initiativesPostpone as $initiative){
                
                foreach($months as $month){
                    echo '* Check Initatives ' . $initiative->id . ' postponed.' . PHP_EOL;
                    $checkInitiativesEvaluation = InitiativesEvaluation::find()
                                                                            ->where(['initiatives_id' => $initiative->initiatives_id])
                                                                            ->andWhere(['evaluation_id' => $evaluation->id])
                                                                            ->andWhere(['postponed_from' => $initiative->evaluation->period])
                                                                            ->one();
                    if(!$checkInitiativesEvaluation){
                        echo '** Generating InitiativesEvaluation ' . $month . PHP_EOL;
                        $initiativesEvaluation = new InitiativesEvaluation();
                        $initiativesEvaluation->evaluation_id = $evaluation->id;
                        $initiativesEvaluation->initiatives_id = $initiative->initiatives->id;
                        $initiativesEvaluation->target = 100; // $initiative->target ? (($initiative->initiatives->$month / $initiative->initiatives->target) * 100) : 0;
                        $initiativesEvaluation->real = $this->getInitiativesReal($initiative->id, $evaluation);
                        $initiativesEvaluation->weight = $initiative->initiatives->unit_weight;
                        $initiativesEvaluation->status_id = Status::OKR_E_APPROVE;
                        $initiativesEvaluation->postponed_from = $initiative->evaluation->period;
                        if($initiativesEvaluation->save()){
                            echo '** Done' . PHP_EOL;
                            $initiativesEvaluationHistory = new InitiativesEvaluationHistory();
                            $initiativesEvaluationHistory->initiatives_evaluation_id = $initiativesEvaluation->id;
                            $initiativesEvaluationHistory->action = 'Membuat Evaluasi Program Kerja';
                            $initiativesEvaluationHistory->created_by = $evaluation->created_by;
                            if($initiativesEvaluationHistory->save()){
                                echo '** History created' . PHP_EOL;
                            }
                        }
                    } else {
                        $initiativesEvaluation->status_id = Status::OKR_E_APPROVE;
                        $checkInitiativesEvaluation->target = 100; // $initiative->target ? (($initiative->initiatives->$month / $initiative->initiatives->target) * 100) : 0;
                        $checkInitiativesEvaluation->real = $this->getInitiativesReal($initiative->id, $evaluation);
                        $checkInitiativesEvaluation->weight = $initiative->unit_weight;
                        $checkInitiativesEvaluation->save();
                        echo '** has been updated' . PHP_EOL;
                    }
                }
            } */

            /** - OKR */
            $okrs = Okr::find()
                                        // ->where("COALESCE(" . implode(', ', $months) . ") IS NOT NULL")
                                        ->where('okr_level_id not in (' . OkrLevel::ORGANIZATION . ', ' . OkrLevel::DIRECTORATE . ', ' . OkrLevel::DEPARTMENT . ')')
                                        // ->andWhere(['position_structure_id' => [22, 132, 173, 176]]) //contoh
                                        ->andWhere(['type' => 'KR'])
                                        ->andWhere(['year' => $evaluation->year])
                                        ->andWhere("status_id is null")
                                        // ->andWhere(['pic_id' => [601927]])
                                        ->orderBy('okr_level_id desc')
                                        ->all();
            foreach($okrs as $okr){
                echo '* Check OKR ' . $okr->okr_code . PHP_EOL;
                $checkokrEvaluation = OkrEvaluation::find()
                                                        ->where(['okr_id' => $okr->id])
                                                        ->andWhere(['evaluation_id' => $evaluation->id])
                                                        // ->andWhere(['pic_id' => $okr->pic_id])
                                                        ->andWhere("unit_structure_id is null")
                                                        ->one();
                if(!$checkokrEvaluation){

                    $hasActivities = Activities::find()
                                                    ->where(['okr_id' => $okr->id])
                                                    ->andWhere("start_date BETWEEN '" . $periodStart . "' AND '" . $periodEnd . "'")
                                                    ->one();

                                                    
                    foreach($months as $month){
                        if($okr->$month || $hasActivities){
                            echo '** Generating okrEvaluation ' . PHP_EOL;

                            $okrEvaluation = new OkrEvaluation();
                            $okrEvaluation->evaluation_id = $evaluation->id;
                            $okrEvaluation->okr_id = $okr->id;
                            $okrEvaluation->weight = $okr->unit_weight;

                            $okrEvaluation->target = $okr->$month ? : $okr->target;
                            
                            // $okrEvaluation->target = $okr->target ? (($okr->$month / $okr->target) * 100) : 0;

                            if($evaluation->year != 2023){
                                if($okr->okr_level_id == OkrLevel::INDIVIDUAL){
                                    $okrEvaluation->achieve_value = $this->getOkrIndividualAchieveValue($okr->id, $okr->pic_id, $evaluation);
                                } else if($okr->okr_level_id == OkrLevel::DIVISION){
                                    $okrEvaluation->achieve_value = $this->getOkrUnitAchieveValue($okr->id, $okr->pic_id, $evaluation, $okr->position_structure_id);
                                } else if($okr->okr_level_id == OkrLevel::DEPARTMENT){
                                    $okrEvaluation->achieve_value = $this->getOkrDeptAchieveValue($okr->id, $okr->pic_id, $evaluation, $okr->position_structure_id);
                                }

                                if($okr->polarization_id == Polarization::MAXIMIZE){
                                    $okrEvaluation->real = ($okrEvaluation->achieve_value * $okrEvaluation->target) / 100;
                                } else if($okr->polarization_id == Polarization::MINIMIZE){
                                    $okrEvaluation->real = ($okrEvaluation->achieve_value ? ($okrEvaluation->target / $okrEvaluation->achieve_value) : 0) / 100;
                                } else {
                                    $okrEvaluation->real = ($okrEvaluation->achieve_value * $okrEvaluation->target) / 100;
                                }

                                /* if($okr->okr_level_id == OkrLevel::INDIVIDUAL){
                                    $okrEvaluation->real = $this->getOkrIndividualReal($okr->id, $okr->pic_id, $evaluation); //$okr->measure == '%' ? $this->getOkrIndividualReal($okr->id, $okr->pic_id, $evaluation) : ($this->getOkrIndividualReal($okr->id, $okr->pic_id, $evaluation) *  $okrEvaluation->target) / 100;
                                } else if($okr->okr_level_id == OkrLevel::DIVISION){
                                    $okrEvaluation->real = $this->getOkrUnitReal($okr->id, $okr->pic_id, $evaluation, $okr->position_structure_id);
                                } else if($okr->okr_level_id == OkrLevel::DEPARTMENT){
                                    $okrEvaluation->real = $this->getOkrDeptReal($okr->id, $okr->pic_id, $evaluation, $okr->position_structure_id);
                                } */
                            }

                            $okrEvaluation->pic_id = $okr->pic_id;
                            $okrEvaluation->status_id = Status::OKR_E_APPROVE;
                            if($okrEvaluation->save()){
                                echo '** Done' . PHP_EOL;
                                $okrEvaluationHistory = new OkrEvaluationHistory();
                                $okrEvaluationHistory->okr_evaluation_id = $okrEvaluation->id;
                                $okrEvaluationHistory->action = 'Membuat Evaluasi OKR';
                                $okrEvaluationHistory->created_by = $evaluation->created_by;
                                if($okrEvaluationHistory->save()){
                                    echo '** History created' . PHP_EOL;
                                }
                            } else {
                                var_dump($okrEvaluation->errors);
                            }
                        }
                    }

                    // foreach($months as $month){
                        // echo '** Generating okrEvaluation ' . $month . PHP_EOL;
                    // }
                } else {
                    echo "** {$checkokrEvaluation->id}" . PHP_EOL;
                    foreach($months as $month){
                        $checkokrEvaluation->status_id = Status::OKR_E_APPROVE;
                        $checkokrEvaluation->weight = $okr->unit_weight;
                        $checkokrEvaluation->target = $okr->$month ? : $okr->target;
                        // $checkokrEvaluation->target = $okr->target ? (($okr->$month / $okr->target) * 100) : 0;
    
                        if($evaluation->year != 2023){
                            if($okr->okr_level_id == OkrLevel::INDIVIDUAL){
                                $checkokrEvaluation->achieve_value = $this->getOkrIndividualAchieveValue($okr->id, $okr->pic_id, $evaluation);
                            } else if($okr->okr_level_id == OkrLevel::DIVISION){
                                $checkokrEvaluation->achieve_value = $this->getOkrUnitAchieveValue($okr->id, $okr->pic_id, $evaluation, $okr->position_structure_id);
                            } else if($okr->okr_level_id == OkrLevel::DEPARTMENT){
                                $checkokrEvaluation->achieve_value = $this->getOkrDeptAchieveValue($okr->id, $okr->pic_id, $evaluation, $okr->position_structure_id);
                            }

                            if($okr->polarization_id == Polarization::MAXIMIZE){
                                $checkokrEvaluation->real = ($checkokrEvaluation->achieve_value * $checkokrEvaluation->target) / 100;
                            } else if($okr->polarization_id == Polarization::MINIMIZE){
                                $checkokrEvaluation->real = ($checkokrEvaluation->achieve_value ? ($checkokrEvaluation->target / $checkokrEvaluation->achieve_value) : 0) / 100;
                            } else {
                                $checkokrEvaluation->real = ($checkokrEvaluation->achieve_value * $checkokrEvaluation->target) / 100;
                            }

                            /* if($okr->okr_level_id == OkrLevel::INDIVIDUAL){
                                $checkokrEvaluation->real = $this->getOkrIndividualReal($okr->id, $okr->pic_id, $evaluation); //$okr->measure == '%' ? $this->getOkrIndividualReal($okr->id, $okr->pic_id, $evaluation) : ($this->getOkrIndividualReal($okr->id, $okr->pic_id, $evaluation) *  $checkokrEvaluation->target) / 100;
                            } else if($okr->okr_level_id == OkrLevel::DIVISION){
                                $checkokrEvaluation->real = $this->getOkrUnitReal($okr->id, $okr->pic_id, $evaluation, $okr->position_structure_id);
                            } else if($okr->okr_level_id == OkrLevel::DEPARTMENT){
                                $checkokrEvaluation->real = $this->getOkrDeptReal($okr->id, $okr->pic_id, $evaluation, $okr->position_structure_id);
                            } */
                        }
    
                        $checkokrEvaluation->pic_id = $okr->pic_id;
                        if($checkokrEvaluation->save()){
                            // var_dump($checkokrEvaluation->target);
                            // var_dump($okr->target);
                            echo '** updated' . PHP_EOL;
                        } else {
                            var_dump($checkokrEvaluation->errors);
                        }
                    }
                }
            }
            
            /** - OKR ORGANIZATION */
            $okrOrganizations = Okr::find()
                                        ->where(['year' => $evaluation->year])
                                        ->andWhere(['okr_level_id' => OkrLevel::ORGANIZATION])
                                        ->andWhere(['type' => 'KR'])
                                        // ->andWhere(['okr_org_id' => 6])
                                        ->all();
                                        
            foreach($okrOrganizations as $okrOrganization){


                $deptOkrs = Okr::find()
                                ->where(['okr_level_id' => OkrLevel::DEPARTMENT])
                                ->andWhere(['okr_org_id' => $okrOrganization->okr_org_id])
                                ->all();
                foreach($deptOkrs as $deptOkr){
                    echo '* Check OKR Organisasi ' . $okrOrganization->okr_code . PHP_EOL;
                    $checkokrEvaluation = OkrEvaluation::find()
                                                            ->where(['okr_id' => $okrOrganization->id])
                                                            ->andWhere(['evaluation_id' => $evaluation->id])
                                                            ->andWhere(['unit_structure_id' => $deptOkr->position->unit_structure_id])
                                                            ->one();
                    if(!$checkokrEvaluation){

                        // foreach($months as $month){
                            // echo '** Generating okrEvaluation Organisasi ' . $month . PHP_EOL;

                            $okrEvaluation = new OkrEvaluation();
                            $okrEvaluation->evaluation_id = $evaluation->id;
                            $okrEvaluation->okr_id = $okrOrganization->id;
                            $okrEvaluation->pic_id = $deptOkr->pic_id;
                            $okrEvaluation->unit_structure_id = $deptOkr->position->unit_structure_id;
                            $okrEvaluation->status_id = Status::OKR_ORG_E_CREATE;
                            if($okrEvaluation->save()){
                                echo '** Done. OKR ORG = ' . $okrEvaluation->okr_id . PHP_EOL;
                                $okrEvaluationHistory = new OkrEvaluationHistory();
                                $okrEvaluationHistory->okr_evaluation_id = $okrEvaluation->id;
                                $okrEvaluationHistory->action = 'Membuat Evaluasi OKR';
                                $okrEvaluationHistory->created_by = $evaluation->created_by;
                                if($okrEvaluationHistory->save()){
                                    echo '** History created' . PHP_EOL;
                                }
                            }
                        // }
                    } else {
                        echo $checkokrEvaluation->id . PHP_EOL;
                    }
                }
            }


            $evaluation->status_id = Status::GENERATE_PROSES_2;
            
            if($evaluation->save()){
                echo 'Evaluation status: Done' . PHP_EOL;
            }          

        }



        $endTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);
        $interval = date_diff(date_create($startTime), date_create($endTime));
        echo 'Time : ' . $interval->format('%i') . ' menit' . PHP_EOL;
        
    }

    public function actionSetTarget(){

        $okrEvaluations = Yii::$app->db->createCommand("
                                                        SELECT
                                                            oe.id,
                                                            okr_id
                                                        FROM
                                                            management.okr_evaluation oe
                                                        LEFT JOIN management.okr o ON o.id = oe.okr_id
                                                        WHERE
                                                            (oe.target IS NULL or oe.target = 0) AND
                                                            o.okr_level_id = 5
                                                        ")->queryAll();
                                                        
        foreach($okrEvaluations as $okrEvaluation){
            if($okr = Okr::findOne($okrEvaluation['okr_id'])){
                if($oe = OkrEvaluation::findOne($okrEvaluation['id'])){
                    $oe->target = $okr->target_q1;
                    if($oe->save()){
                        echo "{$oe->id} => {$oe->target}" . PHP_EOL;
                    }
                }
            }
        }    

    }

    public function actionSetPic(){
        $okrs = Yii::$app->db->createCommand("
                                                        SELECT
                                                            o.id,
                                                            pse.employee_id
                                                        FROM
                                                            management.okr o
                                                        LEFT JOIN management.position_structure__employee pse ON pse.position_structure_id = o.position_structure_id
                                                        WHERE
                                                            o.pic_id IS NULL AND
                                                            o.okr_level_id in (2, 3, 4)
                                                        ")->queryAll();
                                                        
        foreach($okrs as $okr){
            if($o = Okr::findOne($okr['id'])){
                $o->pic_id = $okr['employee_id'];
                if($o->save(false)){
                    if($oe = OkrEvaluation::findOne(['okr_id' => $okr['id']])){
                        $oe->pic_id = $okr['employee_id'];
                        $oe->save();
                    }
                    echo "{$o->id} => {$o->pic_id}" . PHP_EOL;
                }
            }
        }    
    }

    public function actionDeleteOkr(){
        $okrEvaluations = OkrEvaluation::find()
                                            ->alias('oe')
                                            ->joinWith('okr o')
                                            ->where(['evaluation_id' => 19])
                                            ->andWhere(['o.status_id' => Status::OKR_CANCEL])
                                            ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                                            ->all();
        foreach($okrEvaluations as $oe){
            if($oe->delete()){
                echo '.';
            }
        }
    }

    public function actionReverseTarget(){

        $okrEvaluations = OkrEvaluation::find()
                                            ->alias('oe')
                                            ->joinWith('okr o')
                                            ->where(['evaluation_id' => 18])
                                            ->andWhere("o.okr_level_id <> " . OkrLevel::ORGANIZATION)
                                            ->andWhere("oe.target is null")
                                            ->all();
        foreach($okrEvaluations as $okrEvaluation){
            $okrEvaluation->target = $okrEvaluation->okr->target;
            if($okrEvaluation->save()){
                echo "okr evaluation {$okrEvaluation->id}" . PHP_EOL;
            }
        }
    }

    public function actionSetReal(){
        $year = date('Y');

        $okrs = Okr::find()
                        ->where(['year' => $year])
                        ->andWhere(['type' => 'KR'])
                        ->orderBy('okr_level_id desc')
                        ->all();
    }


    protected function getOkrIndividualAchieveValue($okr_id, $pic_id, $evaluation){
        
        $periodStart = $this->getPeriodRange($evaluation->year, $evaluation->period)[0];
        $periodEnd = $this->getPeriodRange($evaluation->year, $evaluation->period)[1];
        
        $achievement = Activities::find()
                                    ->alias('a')
                                    ->joinWith('okr o')
                                    ->where(['okr_id' => $okr_id])
                                    ->andWhere(['okr_level_id' => OkrLevel::INDIVIDUAL])
                                    ->andWhere("'$periodStart' <= start_date and start_date <= '$periodEnd'")
                                    ->andWhere(['a.pic_id' => $pic_id])
                                    ->andWhere(['a.status_id' => Status::ACT_DONE])
                                    ->sum('okr_value');

        return $achievement;
    }
    
    protected function getInitiativesAchieveValue($initiativesId, $evaluation){

        $periodStart = $this->getPeriodRange($evaluation->year, $evaluation->period)[0];
        $periodEnd = $this->getPeriodRange($evaluation->year, $evaluation->period)[1];

        $activities = Activities::find()
                            ->alias('a')
                            ->joinWith('okr o')
                            ->where(['year' => $evaluation->year])
                            ->andWhere(['ref_initiatives_id' => $initiativesId])
                            ->andWhere(['type' => 'KR'])
                            ->andWhere("'$periodStart' <= start_date and start_date <= '$periodEnd'")
                            ->andWhere(['a.status_id' => Status::ACT_DONE])
                            ->all();

        $real = 0;
        if($activities){
            foreach($activities as $activity){
                $real = $real + (($activity->okr_value * $activity->okr->weight)/100);
            }
        }

        return $real;
    }

    protected function getOkrUnitAchieveValue($okr_id, $pic_id, $evaluation, $position_structure_id){
        
        $periodStart = $this->getPeriodRange($evaluation->year, $evaluation->period)[0];
        $periodEnd = $this->getPeriodRange($evaluation->year, $evaluation->period)[1];
        
        $initiatives = Initiatives::find()->where(['okr_id' => $okr_id])->all();

        $achievement = 0;
        foreach($initiatives as $initiative){
            $achieveVAlueInitiatives = InitiativesEvaluation::findOne(['initiatives_id' => $initiative->id, 'evaluation_id' => $evaluation->id])->achieve_value;

            $achievement = $achievement + (($achieveVAlueInitiatives * $initiative->grade) / 100);
        }

        $okrAchievement = Activities::find()
                                        ->alias('a')
                                        ->joinWith('okr o')
                                        ->where(['okr_id' => $okr_id])
                                        ->andWhere('okr_level_id <> ' . OkrLevel::INDIVIDUAL)
                                        ->andWhere("'$periodStart' <= start_date and start_date <= '$periodEnd'")
                                        ->andWhere(['a.pic_id' => $pic_id])
                                        ->andWhere(['a.position_structure_id' => $position_structure_id])
                                        ->andWhere(['a.status_id' => Status::ACT_DONE])
                                        ->sum('okr_value');

        return $achievement + ($okrAchievement ?: 0);
    }

    protected function getOkrDeptAchieveValue($okr_id, $pic_id, $evaluation, $position_structure_id){
        $periodStart = $this->getPeriodRange($evaluation->year, $evaluation->period)[0];
        $periodEnd = $this->getPeriodRange($evaluation->year, $evaluation->period)[1];
        
        $okrs = Okr::find()
                        ->where(['parent_id' => $okr_id])
                        ->andWhere(['okr_level_id' => OkrLevel::DIVISION])
                        ->andWhere(['year' => $evaluation->year])
                        ->all();

        $objCount = 0;
        $krVal = 0;
        foreach($okrs as $okr){
            $childOkrs = Okr::find()->where(['parent_id' => $okr->id])->andWhere(['type' => 'KR'])->all();
            foreach($childOkrs as $child){
                $okrEvaluation = OkrEvaluation::findOne(['okr_id' => $child->id, 'evaluation_id' => $evaluation->id]);

                $krVal = $krVal + ($okrEvaluation->okr->weight ? (($okrEvaluation->real * $okrEvaluation->okr->weight) / 100) : 0);
            }

            // $objCount++;
        }

        $okrAchievement = Activities::find()
                                        ->alias('a')
                                        ->joinWith('okr o')
                                        ->where(['okr_id' => $okr_id])
                                        ->andWhere("'$periodStart' <= start_date and start_date <= '$periodEnd'")
                                        ->andWhere(['a.pic_id' => $pic_id])
                                        ->andWhere(['a.position_structure_id' => $position_structure_id])
                                        ->andWhere(['a.status_id' => Status::ACT_DONE])
                                        ->sum('okr_value');

        // return ($objCount ? ($krVal / $objCount) : 0) + ($okrAchievement ?: 0);
        return ($okrAchievement ?: 0) + $this->getOkrUnitAchieveValue($okr_id, $pic_id, $evaluation, $position_structure_id) + ($krVal ?: 0);
    }



    protected function getOkrIndividualReal($okr_id, $pic_id, $evaluation){
        
        $periodStart = $this->getPeriodRange($evaluation->year, $evaluation->period)[0];
        $periodEnd = $this->getPeriodRange($evaluation->year, $evaluation->period)[1];
        
        $achievement = Activities::find()
                                    ->alias('a')
                                    ->where(['okr_id' => $okr_id])
                                    ->andWhere("'$periodStart' <= start_date and start_date <= '$periodEnd'")
                                    ->andWhere(['pic_id' => $pic_id])
                                    ->andWhere(['a.status_id' => Status::ACT_DONE])
                                    ->sum('okr_value');

        return $achievement;
    }

    protected function getOkrUnitReal($okr_id, $pic_id, $evaluation, $position_structure_id){
        
        $periodStart = $this->getPeriodRange($evaluation->year, $evaluation->period)[0];
        $periodEnd = $this->getPeriodRange($evaluation->year, $evaluation->period)[1];
        
        $initiatives = Initiatives::find()->where(['okr_id' => $okr_id])->all();

        $achievement = 0;
        foreach($initiatives as $initiative){
            $realInitiatives = InitiativesEvaluation::findOne(['initiatives_id' => $initiative->id, 'evaluation_id' => $evaluation->id])->real;

            $achievement = $achievement + (($realInitiatives * $initiative->grade) / 100);
        }

        $okrAchievement = Activities::find()
                                        ->alias('a')
                                        ->where(['okr_id' => $okr_id])
                                        ->andWhere("'$periodStart' <= start_date and start_date <= '$periodEnd'")
                                        ->andWhere(['pic_id' => $pic_id])
                                        ->andWhere(['position_structure_id' => $position_structure_id])
                                        ->andWhere(['a.status_id' => Status::ACT_DONE])
                                        ->sum('okr_value');

        $okr = Okr::findOne($okr_id);
        if($okrAchievement){

            if($okr->measure == '%'){
                $okrAchievement = $okrAchievement;
            } else {
                $okrEvaluation = OkrEvaluation::findOne(['okr_id' => $okr_id, 'pic_id' => $pic_id, 'evaluation_id' => $evaluation->id]);
                if($okrEvaluation){
                    $okrAchievement = ($okrAchievement * $okrEvaluation->target) / 100;
                }
            }
        }
        


        return $achievement + ($okrAchievement ?: 0);
    }

    protected function getOkrDeptReal($okr_id, $pic_id, $evaluation, $position_structure_id){
        $periodStart = $this->getPeriodRange($evaluation->year, $evaluation->period)[0];
        $periodEnd = $this->getPeriodRange($evaluation->year, $evaluation->period)[1];
        
        $okrs = Okr::find()
                        ->where(['parent_id' => $okr_id])
                        ->andWhere(['okr_level_id' => OkrLevel::DIVISION])
                        ->andWhere(['year' => $evaluation->year])
                        ->all();

        $objCount = 0;
        $krVal = 0;
        foreach($okrs as $okr){
            $childOkrs = Okr::find()->where(['parent_id' => $okr->id])->andWhere(['type' => 'KR'])->all();
            foreach($childOkrs as $child){
                $okrEvaluation = OkrEvaluation::findOne(['okr_id' => $child->id, 'evaluation_id' => $evaluation->id]);

                $krVal = $krVal + ($okrEvaluation->okr->weight ? (($okrEvaluation->real * $okrEvaluation->okr->weight) / 100) : 0);
            }

            $objCount++;
        }

        $okrAchievement = Activities::find()
                                        ->alias('a')
                                        ->where(['okr_id' => $okr_id])
                                        ->andWhere("'$periodStart' <= start_date and start_date <= '$periodEnd'")
                                        ->andWhere(['pic_id' => $pic_id])
                                        ->andWhere(['position_structure_id' => $position_structure_id])
                                        ->andWhere(['a.status_id' => Status::ACT_DONE])
                                        ->sum('okr_value');

        return ($objCount ? ($krVal / $objCount) : 0) + ($okrAchievement ?: 0);
    }

    protected function getInitiativesReal($initiativesId, $evaluation){

        $periodStart = $this->getPeriodRange($evaluation->year, $evaluation->period)[0];
        $periodEnd = $this->getPeriodRange($evaluation->year, $evaluation->period)[1];

        $activities = Activities::find()
                            ->alias('a')
                            ->joinWith('okr o')
                            ->where(['year' => $evaluation->year])
                            ->andWhere(['ref_initiatives_id' => $initiativesId])
                            ->andWhere(['type' => 'KR'])
                            ->andWhere("'$periodStart' <= start_date and start_date <= '$periodEnd'")
                            ->andWhere(['a.status_id' => Status::ACT_DONE])
                            ->all();

        $real = 0;
        if($activities){
            foreach($activities as $activity){
                $real = $real + (($activity->okr_value * $activity->okr->weight)/100);
            }
        }

        return $real;
    }

    protected function getPeriodRange($year, $period){

        $dateRange = [];

        if($period == 'Kuartal 1'){
            $dateStart = $year . '-' . '01-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '03-01'));
        } else if($period == 'Kuartal 2'){
            $dateStart = $year . '-' . '04-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '06-01'));
        } else if($period == 'Kuartal 3'){
            $dateStart = $year . '-' . '07-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '09-01'));
        } else if($period == 'Kuartal 4'){
            $dateStart = $year . '-' . '10-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '12-01'));
        }

        array_push($dateRange, $dateStart);
        array_push($dateRange, $dateEnd);

        return $dateRange;
    }

    protected function getColumnTarget($period){

        $months = [];
        if($period == 'Kuartal 1'){
            array_push($months, 'target_q1');
        } else if($period == 'Kuartal 2'){
            array_push($months, 'target_q2');
        } else if($period == 'Kuartal 3'){
            array_push($months, 'target_q3');
        } else if($period == 'Kuartal 4'){
            array_push($months, 'target_q4');
        }

        return $months;
    }
    
}
