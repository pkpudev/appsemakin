<?php

namespace app\commands;

use app\components\CountTotalReal;
use app\models\Budget;
use app\models\Target;
use app\models\VwRKAT;
use Yii;
use yii\console\Controller;

class TotalRealController extends Controller
{
    
    public function actionCount()
    {

        $cache = \Yii::$app->cache;

        if ($cache['canRunCount'] == true) {
            var_dump('Count sedang berjalan~');
        } else {
            $startTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);
            $cache['canRunCount'] = true;

            $year       = date('Y');
            $rkat       = VwRKAT::find()
                                        ->where(['IN', 'year', [$year]])
                                        ->andWhere('budget_id not in (2504, 813)')
                                        // ->andWhere(['budget_id' => 226])
                                        ->orderBy('budget_id')->all();
            
            foreach ($rkat as $row) {
                $disbursement_total = \app\components\CountTotalReal::fromDisbursementDetail($row->budget_id); 
                $procurement_total = \app\components\CountTotalReal::fromProcurementDetail($row->budget_id);
                $selisih = \app\components\CountTotalReal::fromDisbursementDetailSelisih($row->budget_id);
                // var_dump($procurement_total);
                $total_real = $disbursement_total + $procurement_total + $selisih;
                
                if($row->total_real <> $total_real){
                    $budget = Budget::findOne($row->budget_id);
                    $budget->total_real = $total_real;
                    // var_dump($budget->total_real);
                    echo "{$budget->id}" . PHP_EOL;
                    if($budget->save(false)){
                        echo ".. saved." . PHP_EOL;
                    } else {
                        var_dump($budget->errors);
                    }
                }
            }

            $endTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);
            $interval = date_diff(date_create($startTime), date_create($endTime));
            echo 'Time : ' . $interval->format('%i') . ' menit' . PHP_EOL;
            $cache['canRunCount'] = false;
        }
    }

    public function actionResetCacheCount()
    {
        $cache = \Yii::$app->cache;
        $cache['canRunCount'] = false;

        var_dump('canRunCount reseted.');
    }

    public function actionCount1($budget_id)
    {
        $startTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);

        $year       = date('Y');
        $rkat       = VwRKAT::find()->where(['IN', 'year', [$year]])->andWhere(['budget_id' => $budget_id])->all();
        
        foreach ($rkat as $row) {
            $disbursement_total = \app\components\CountTotalReal::fromDisbursementDetail($row->budget_id); 
            $procurement_total = \app\components\CountTotalReal::fromProcurementDetail($row->budget_id);
            $total_real = $disbursement_total + $procurement_total;
            
            if($row->total_real <> $total_real){
                $budget = Budget::findOne($row->budget_id);
                $budget->total_real = $total_real;
                var_dump($budget->total_real);
                if(!$budget->save(false))
                    var_dump($budget->errors);
            }
        }

        $endTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);
        $interval = date_diff(date_create($startTime), date_create($endTime));
        echo 'Time : ' . $interval->format('%i') . ' menit' . PHP_EOL;
    }

    public function actionCountUnit($unit_id)
    {
        $startTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);

        $year       = date('Y');
        $rkat       = VwRKAT::find()->where(['IN', 'year', [$year]])->andWhere(['departments_id' => $unit_id])->all();
        
        foreach ($rkat as $row) {
            $disbursement_total = \app\components\CountTotalReal::fromDisbursementDetail($row->budget_id); 
            $procurement_total = \app\components\CountTotalReal::fromProcurementDetail($row->budget_id);
            $total_real = $disbursement_total + $procurement_total;
            
            if($row->total_real <> $total_real){
                $budget = Budget::findOne($row->budget_id);
                $budget->total_real = $total_real;
                var_dump($budget->total_real);
                if(!$budget->save(false))
                    var_dump($budget->errors);
            }
        }

        $endTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);
        $interval = date_diff(date_create($startTime), date_create($endTime));
        echo 'Time : ' . $interval->format('%i') . ' menit' . PHP_EOL;
    }

    public function actionTarget(){
        $cache = \Yii::$app->cache;

        if ($cache['canRunTarget'] == true) {
            var_dump('Target sedang berjalan~');
        } else {
            $startTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);
            $cache['canRunTarget'] = true;

            $y = date('Y');
            $m = date('m');

            $targets = Target::find()->where(['year' => $y])->all();
            foreach($targets as $target){

                echo $target->unit->unit_name . PHP_EOL;

                $column = 'real_month_';
                for($i = 1; $i <= $m; $i++){
        
                    $columnRealMonth = $column . str_pad(((int) $i), 2, "0", STR_PAD_LEFT);

                    $target->$columnRealMonth = CountTotalReal::fromDonation($target->unit->unit_id, $y, $i);
                    if($target->save()){
                        echo ' . . ' . $i . ' => ' . $target->$columnRealMonth . PHP_EOL;
                    }
        
                }
            }

            $endTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);
            $interval = date_diff(date_create($startTime), date_create($endTime));
            echo 'Time : ' . $interval->format('%i') . ' menit' . PHP_EOL;
            $cache['canRunTarget'] = false;
        }
    }

    public function actionResetCacheTarget()
    {
        $cache = \Yii::$app->cache;
        $cache['canRunTarget'] = false;

        var_dump('canRunTarget reseted.');
    }

    public function actionBudget()
    {
        $cache = \Yii::$app->cache;

        if ($cache['canRunBudget'] == true) {
            var_dump('Budget sedang berjalan~');
        } else {
            $startTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);
            $cache['canRunBudget'] = true;

            $y = date('Y');
            $m = date('m');

            $budgets = Budget::find()->where(['year' => $y])/*->andWhere(['id' => 15507])*/->orderBy('budget_code')->all();
            foreach($budgets as $budget){

                echo $budget->budget_code . PHP_EOL;
                $budgetId = $budget->id;

                $column = 'real_month_';
                for($i = 1; $i <= $m; $i++){
        
                    $columnRealMonth = $column . str_pad(((int) $i), 2, "0", STR_PAD_LEFT);
                    
                    $sum = CountTotalReal::fromDisbursement($budget->id, $y, $i);
                    /* $budget->$columnRealMonth = CountTotalReal::fromDisbursement($budget->id, $y, $i);
                    
                    if($budget->save()){
                        echo ' . . ' . $i . ' => ' . $budget->$columnRealMonth . PHP_EOL;
                    } else {
                    } */
                    
                    if($budget->$columnRealMonth <> $sum){
                        $update = Yii::$app->db->createCommand("UPDATE management.budget SET $columnRealMonth=$sum WHERE id=$budgetId")->execute();
                        
                        if($update){
                            echo ' . . ' . $i . ' => ' . $budget->$columnRealMonth . PHP_EOL;
                        }
                    }
                }
            }

            $endTime = gmdate('Y-m-d H:i:s', time() + 60 * 60 * 7);
            $interval = date_diff(date_create($startTime), date_create($endTime));
            echo 'Time : ' . $interval->format('%i') . ' menit' . PHP_EOL;
            $cache['canRunBudget'] = false;
        }
    }

    public function actionResetCacheBudget()
    {
        $cache = \Yii::$app->cache;
        $cache['canRunBudget'] = false;

        var_dump('canRunBudget reseted.');
    }
    
}
