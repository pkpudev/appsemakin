<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Activities;
use app\models\Budget;
use app\models\Initiatives;
use app\models\Okr;
use app\models\Status;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionSetApproved(){
        $initiative = Initiatives::findOne(831);
        $initiative->status_id = 4;
        $initiative->save(false);
    }

    public function actionSetActivities($target){
        $okrs = Okr::find()
                        ->where(['year' => date('Y')])
                        ->andWhere(['pic_id' => 487497])
                        ->andWhere(['type' => 'KR'])
                        ->andWhere("$target IS NOT NULL")
                        ->all();

        foreach($okrs as $okr){
            echo $okr->okr_code . PHP_EOL;
            $activities = new Activities();
            $activities->activity = $okr->okr;
            $activities->pic_id = 487497;
            $activities->activity_category_id = 1;
            $activities->position_structure_id = $okr->position_structure_id;
            $activities->okr_id = $okr->id;
            $activities->start_date = date('Y-m-d');
            $activities->due_date = date('Y-m-d');
            $activities->finish_date = date('Y-m-d');
            $activities->status_id = 20;
            $activities->progress = 100;
            if($activities->save()){
                echo '. . saved' . PHP_EOL;
            }

        }
    }

    public function actionSetNewProkerNo()
    {
        $initiatives = Initiatives::find()/* ->where(['status_id' => 4]) */->where("implementation ILIKE '%2025%'")->all();
        foreach($initiatives as $initiative){
            $okr = Okr::findOne($initiative->okr_id);
            $code = $okr->position->unit->initiative_code;
            
            $lastNo = Initiatives::find()->where("initiatives_code ILIKE '$code%'")->orderBy('initiatives_code DESC')->one();
            if($lastNo){
                $lastNo = $code . '-' . str_pad($lastNo->initiatives_code + 1, 2, "0", STR_PAD_LEFT);
            } else {
                $lastNo = $code . '-01';
            }

            $initiative->initiatives_code = $lastNo;
            $initiative->save(false);
        }
    }

    public function actionSetNewBudgetNo()
    {
        $year = date('Y');

        $initiatives = Initiatives::find()
                            ->alias('i')
                            ->joinWith(['okr o', 'position p'])
                            ->where(['o.year' => $year])
                            ->andWhere(['i.status_id' => Status::APPROVED])
                            // ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                            // ->andWhere(['p.unit_structure_id' => 744])
                            ->orderBy('p.unit_structure_id, o.okr_code asc, i.id asc')
                            ->all();

        foreach($initiatives as $initiative){
            $initiative->initiatives_code = null;
            if($initiative->save(false)){
                // echo $initiative->initiatives_code . PHP_EOL;
                $budgets = Budget::find()
                                    ->where(['initiatives_id' => $initiative->id])
                                    ->orderBy(['id'=>SORT_ASC])->all();
                if($budgets){
                    $budget_no = 1;
                    foreach ($budgets as $budget) {
                        $budget->budget_code = NULL;
                        if($budget->save(false)){
                        } else {
                            var_dump($budget->errors);die;
                        }
                    }
                }
            }
        }

        $no = 1;
        foreach($initiatives as $initiative){

            $okr = Okr::findOne($initiative->okr_id);
            $code = $okr->position->unit->initiative_code;
            
            $lastNo = Initiatives::find()->where("initiatives_code ILIKE '$code%'")->andWhere("implementation ILIKE '%2025%'")->orderBy('initiatives_code DESC')->one();
            if($lastNo){
                // var_dump($lastNo);
                $newstring = substr($lastNo->initiatives_code, -2);
                // var_dump($newstring);
                
                $lastNo = $code . '-' . date('y') . '-' . str_pad($newstring + 1, 2, "0", STR_PAD_LEFT);
            } else {
                $lastNo = $code . '-' . date('y') . '-01';
            }

            $initiative->initiatives_code = $lastNo;
            if($initiative->save(false)){
                echo $initiative->initiatives_code . PHP_EOL;
                $budgets = Budget::find()
                                    ->where(['initiatives_id' => $initiative->id])
                                    ->andWhere('account_id is not null')
                                    ->andWhere(['<>', 'status_id', [Status::REJECTED,Status::DELETED]])
                                    ->orderBy(['id'=>SORT_ASC])->all();
                if($budgets){
                    $budget_no = 1;
                    foreach ($budgets as $budget) {
                        $budget_code = $initiative->initiatives_code . '-' . str_pad($budget_no, 2, "0", STR_PAD_LEFT);
                        $budget->budget_code = $budget_code;
                        $budget->status_id = Status::APPROVED;
                        if($budget->save(false)){
                            echo '. ' . $budget_code . PHP_EOL;
                            $budget_no++;
                        } else {
                            var_dump($budget->errors);die;
                        }
                    }
                }
            }

        }
    }
}
