<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <title>MailDesign</title>
            <?php $this->head() ?>

            <!--[if mso]>
            <style>
            * {
            font-family: sans-serif !important;
            }
            </style>
            <![endif]-->

            <!--[if !mso]>
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css">
            <![endif]-->

            <style>
                html,
                body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
                }

                * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
                }

                a {
                    color: #3891cd !important;
                    text-decoration: none;
                    font-weight: bold;
                }

                a:hover {
                    color: #2e81b8 !important;
                }

                table,
                td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
                }

                table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                }

                img {
                    -ms-interpolation-mode:bicubic;
                }
        
                *[x-apple-data-detectors],
                .x-gmail-data-detectors,
                .x-gmail-data-detectors *,
                .aBn {
                    border-bottom: 0 !important;
                    cursor: default !important;
                    color: inherit !important;
                    text-decoration: none !important;
                    font-size: inherit !important;
                    font-family: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                }
        
                .a6S {
                    display: none !important;
                    opacity: 0.01 !important;
                }
        
                img.g-img + div {
                    display: none !important;
                }
            </style>

            <!--[if gte mso 9]>
            <xml>
                <o:OfficeDocumentSettings>
                    <o:AllowPNG/>
                    <o:PixelsPerInch>96</o:PixelsPerInch>
                </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->

        </head>
        <body style="background: #eee !important" bgcolor="#eee">
            <?php $this->beginBody() ?>

            <div style="font-family: \'proximaNova, sans-serif; margin: 20px auto 20px;letter-spacing: 1px;color: #404040;">
                
                <!-- SPACER -->
                <table style="margin:10px auto 0px;width:100%;max-width:600px;" cellspacing="0" cellpadding="0" border="0" align="center">	
                    <tbody>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END SPACER -->
                
                <!-- HEADER -->
                <table style="background-color:#fff;margin:10px auto 0px;width:100%;max-width:600px" cellspacing="0" cellpadding="0" border="0" bgcolor="#fff" align="center">	
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <table style="padding:10px 15px;font-size:14px;background-color:#3891cd;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                    <tbody>
                                        <tr>
                                            <td style="padding:10px" width="40%" align="right">
                                                <a href="#">
                                                    <img src="https://sigma.c27g.com/images/ico/logo.png" alt="Logo Kami" title="Logo Kami" width="100" />
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END HEADER -->
                
                <!-- CONTENT -->
                <?= $content ?>
                <!-- END CONTENT -->

                <!-- CONTACT INFO -->
                <!-- <table style="background-color:#fff;margin:3px auto;width:100%;max-width:600px;font-size:13px;font-style: italic;" cellspacing="0" cellpadding="0" border="0" bgcolor="#fff" align="center">
                    <tr>
                        <td style="padding: 20px; width: 50%" valign="top">
                            Need Help?
                            <a href="http://bit.ly/CS2HumanInitiative" style="color: #3891cd !important;">
                                <table width="100%" style="font-size: 12px;">
                                    <tr>
                                        <td width="100%">
                                            Whatsapp: +62 8119342667
                                        </td>
                                    </tr>
                                </table>
                            </a>
                            <a href="{linkUnsubcribe}" style="color: #a6a6a6 !important;font-size: 11px;font-weight: unset;">Unsubscribe from this list.</a>
                        </td>
                        <td style="padding: 20px;" valign="top" align="right">
                            Stay connected with us.
                            <div style="margin-top: 2px">
                                <a href="https://www.youtube.com/user/HumanityCalling/videos"><img src="https://sigma.c27g.com/images/ico/Sosmed01.png" width="30px" /></a>
                                <a href="https://www.facebook.com/humaninitiative.id/"><img src="https://sigma.c27g.com/images/ico/Sosmed02.png" width="30px" /></a>
                                <a href="https://www.instagram.com/humaninitiative_id/"><img src="https://sigma.c27g.com/images/ico/Sosmed03.png" width="30px" /></a>
                                <a href="https://twitter.com/hum_initiative"><img src="https://sigma.c27g.com/images/ico/Sosmed04.png" width="30px" /></a>
                            </div>
                        </td>
                    </tr>
                </table> -->
                <!-- END CONTACT INFO -->
                
                <!-- FOOTER -->
                <!-- <table style="background-color:#3891cd;margin:-15px auto;width:100%;max-width:600px;font-size:12px;margin-top: -15px;" cellspacing="0" cellpadding="0" border="0" bgcolor="#fff" align="center">
                    <tr>
                        <td style="padding: 30px; width: 50%">
                            <b>Kiprah di Indonesia dan Internasional</b><br />
                            <p align="justify">
                            20 tahun lebih berjuang atas nama kemanusiaan. Saat ini <b>PKPU Human Initiative</b> telah bertransformasi menjadi <b>Human Initiative</b> untuk mewujudkan visi menjadi organisasi kemanusiaan dunia (worldwide organization). Sejak tahun 2008, Human Initiative telah terdaftar di PBB untuk berkiprah di dunia internasional sebagai NGO dengan “Special Consultative Status with the Economic Social Council”. Pada 29 Januari 2010, Human Initiative resmi terdaftar sebagai Organisasi Sosial Nasional berdasarkan Keputusan Menteri RI No. 08/Huk/2010. Selain itu, terdaftar sebagai member of HFI (Humanitarian Forum Indonesia), Member of ICVA (International Council of Voluntary Agencies), serta NGO in EuropeAid Registered ID of the European Commission ID – 2010 – CSD – 1203198618.
                            </p>
                        </td>
                    </tr>
                </table> -->
                <!-- END FOOTER -->

                <!-- SPACER -->
                <table style="margin:10px auto 0px;width:100%;max-width:600px;" cellspacing="0" cellpadding="0" border="0" align="center">	
                    <tbody>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END SPACER -->
                
            </div>

            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>