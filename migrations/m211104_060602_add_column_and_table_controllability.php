<?php

use yii\db\Migration;

class m211104_060602_add_column_and_table_controllability extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
                
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%management.controllability}}', [
            'id' => $this->primaryKey(),
            'degree_of_controllability' => $this->string(20)->comment('Tingkat Pengendalian')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
            'value' => $this->float()->comment('Nilai'),
        ], $tableOptions);

        $this->addColumn('{{%management.okr}}', 'validity_index', $this->float()->comment('Nilai Validitas'));
        $this->addColumn('{{%management.okr}}', 'controllability_id', $this->integer()->comment('Tingkat Pengendalian'));
        $this->addColumn('{{%management.okr}}', 'controllability_index', $this->float()->comment('Nilai Pengendalian'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
