<?php

use yii\db\Migration;

class m220407_150616_create_okr_achieve extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.okr_achievement}}', [
            'id' => $this->primaryKey(),
            'okr_id' => $this->integer()->comment('OKR'),
            'evaluation_id' => $this->integer()->comment('Evaluasi'),
            'budget_plan' => $this->double()->comment('Rencana Anggaran'),
            'budget_real' => $this->double()->comment('Realisasi Anggaran'),
            'achieve_value' => $this->double()->comment('Nilai Realisasi'),
        ], $tableOptions);

        $this->addForeignKey('okr_achievement_fk_1', '{{%management.okr_achievement}}', 'evaluation_id', '{{%management.evaluation}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
