<?php

use yii\db\Migration;

class m220323_150605_create_okr_evaluation extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        
        $this->addColumn('{{%management.activity}}', 'okr_id', $this->integer()->comment('OKR'));
        $this->addColumn('{{%management.activity_mov}}', 'status', $this->string()->comment('Status'));

        /** OKR EVALUATION */
        $this->createTable('{{%management.okr_evaluation}}', [
            'id' => $this->primaryKey(),
            'evaluation_id' => $this->integer()->comment('Evaluasi'),
            'okr_id' => $this->integer()->comment('OKR'),
            'target' => $this->integer()->comment('Target'),
            'real' => $this->text()->comment('Realisasi'),
        ], $tableOptions);

        $this->addForeignKey('okr_evaluation_fk_1', '{{%management.okr_evaluation}}', 'evaluation_id', '{{%management.evaluation}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('okr_evaluation_fk_2', '{{%management.okr_evaluation}}', 'okr_id', '{{%management.okr}}', 'id', 'CASCADE', 'CASCADE');
        
        $this->createTable('{{%management.okr_evaluation_history}}', [
            'id' => $this->primaryKey(),
            'okr_evaluation_id' => $this->integer()->comment('Evaluasi OKR')->notNull(),
            'action' => $this->string(50)->notNull()->comment('Tindakan'),
            'comment' => $this->text()->comment('Komentar'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'created_stamp' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'ip' => $this->string(70)->comment('Diubah Pada'),
        ], $tableOptions);
        
        $this->addForeignKey('okr_evaluation_history_fk_1', '{{%management.okr_evaluation_history}}', 'okr_evaluation_id', '{{%management.okr_evaluation}}', 'id', 'CASCADE', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
