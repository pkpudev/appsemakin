<?php

use yii\db\Migration;

class m211001_020602_add_column_description_to_portfolio_element extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%management.portfolio_element}}', 'description', $this->string(250)->comment('Deskripsi'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
