<?php

use yii\db\Migration;

class m220325_150606_alter_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->alterColumn('{{%management.initiatives_evaluation}}', 'target', $this->double()->comment('Target'));
        $this->alterColumn('{{%management.okr_evaluation}}', 'target', $this->double()->comment('Target'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
