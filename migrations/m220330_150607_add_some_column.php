<?php

use yii\db\Migration;

class m220330_150607_add_some_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->addColumn('{{%management.initiatives_evaluation}}', 'postpone', $this->string(100)->comment('Periode'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
