<?php

use yii\db\Migration;

class m220308_150603_add_column_is_value_stream_to_unit_structure extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->addColumn('{{%management.unit_structure}}', 'is_value_stream', $this->boolean()->comment('Value Stream?'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
