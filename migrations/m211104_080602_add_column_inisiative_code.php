<?php

use yii\db\Migration;

class m211104_080602_add_column_inisiative_code extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%management.unit_structure}}', 'initiative_code', $this->string(5)->comment('Kode Proker'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
