<?php

use yii\db\Migration;

class m220329_150605_add_some_column_to_okr extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->addColumn('{{%management.okr}}', 'okr_org_id', $this->integer()->comment('OKR ORG'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
