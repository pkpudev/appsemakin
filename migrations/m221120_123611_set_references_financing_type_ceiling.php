<?php

use yii\db\Migration;

class m221120_123611_set_references_financing_type_ceiling extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey('financing_type_ceiling_fk_1', '{{%management.financing_type_ceiling}}', 'unit_structure_id', '{{%management.unit_structure}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('financing_type_ceiling_fk_2', '{{%management.financing_type_ceiling}}', 'financing_type_id', '{{%management.financing_type}}', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
