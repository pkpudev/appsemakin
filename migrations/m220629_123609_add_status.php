<?php

use yii\db\Migration;

class m220629_123609_add_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->insert('management.status', [
            'status' => 'Disembunyikan',
            'scenario' => 'okr',
            'is_active' => true
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
