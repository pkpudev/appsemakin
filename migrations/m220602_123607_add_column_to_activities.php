<?php

use yii\db\Migration;

class m220602_123607_add_column_to_activities extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->addColumn('{{%management.activities}}', 'output', $this->text()->comment('Keluaran'));
        $this->addColumn('{{%management.activities}}', 'weight', $this->float()->comment('Bobot'));
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
