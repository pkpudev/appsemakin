<?php

use yii\db\Migration;

class m211102_070602_activities extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%management.activity}}', [
            'id' => $this->primaryKey(),
            'initiatives_id' => $this->integer()->comment('initiatives')->notNull(),
            'activity_code' => $this->string(30)->comment('Kode RKAT'),
            'activity_name' => $this->text()->comment('Nama Kegiatan'),
            'start_plan' => $this->date()->comment('Tanggal Mulai'),
            'finish_plan' => $this->date()->comment('Tanggal Selesai'),
            'duration' => $this->integer()->comment('Durasi'),
            'start_actual' => $this->date()->comment('Tanggal Mulai'),
            'finish_actual' => $this->date()->comment('Tanggal Selesai'),
            'duration_actual' => $this->integer()->comment('Durasi'),
            'position_structure_id' => $this->integer()->comment('Jabatan'),
            'pic_id' => $this->integer()->comment('PIC'),
            'status_id' => $this->integer()->comment('Status'),
            'documentation' => $this->text()->comment('Catatan'),
        ], $tableOptions);
        
        $this->addForeignKey('activity_fk_1', '{{%management.activity}}', 'initiatives_id', '{{%management.initiatives}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('activity_fk_2', '{{%management.activity}}', 'position_structure_id', '{{%management.position_structure}}', 'id', 'RESTRICT', 'CASCADE');
         

        $this->createTable('{{%management.activity_history}}', [
            'id' => $this->primaryKey(),
            'activity_id' => $this->integer()->comment('Kegiatan')->notNull(),
            'action' => $this->string(50)->notNull()->comment('Tindakan'),
            'comment' => $this->text()->comment('Komentar'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'created_stamp' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'ip' => $this->string(70)->comment('Diubah Pada'),
        ], $tableOptions);
        
        $this->addForeignKey('activity_history_fk_1', '{{%management.activity_history}}', 'activity_id', '{{%management.activity}}', 'id', 'CASCADE', 'CASCADE');
        
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
