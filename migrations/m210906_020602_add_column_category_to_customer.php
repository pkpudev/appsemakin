<?php

use yii\db\Migration;

class m210906_020602_add_column_category_to_customer extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%management.customers}}', 'category', $this->integer()->comment('1 => Internal, 2 => External'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
