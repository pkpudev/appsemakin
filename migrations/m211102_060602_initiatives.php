<?php

use yii\db\Migration;

class m211102_060602_initiatives extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%management.participatory_budgeting}}', [
            'id' => $this->primaryKey(),
            'participatory_budgeting' => $this->text()->comment('Jenis Anggaran Partisipatori')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);
        
        $this->createTable('{{%management.initiatives}}', [
            'id' => $this->primaryKey(),
            'okr_id' => $this->integer()->comment('OKR')->notNull(),
            'initiatives_code' => $this->string(30)->comment('Kode RKAT'),
            'workplan_name' => $this->text()->comment('Nama Proker'),
            'initiatives' => $this->text()->comment('Deskripsi'),
            'implementation' => $this->string(100)->comment('Pelaksanaan (Bulan)'),
            'total_budget' => $this->float()->comment('Jumlah Anggaran'),
            'target' => $this->float()->comment('Target'),
            'measure' => $this->string(25)->comment('Ukuran'),
            'target_q1' => $this->double()->comment('Target Kuartal 1'),
            'target_q2' => $this->double()->comment('Target Kuartal 2'),
            'target_q3' => $this->double()->comment('Target Kuartal 3'),
            'target_q4' => $this->double()->comment('Target Kuartal 4'),
            'validity_id' => $this->integer()->comment(''),
            'validity_index' => $this->float()->comment('Index Validitas'),
            'controllability_id' => $this->integer()->comment(''),
            'controllability_index' => $this->float()->comment('Index Kendali'),
            'grade' => $this->float()->comment('Bobot'),
            'monitoring_tools' => $this->string(300)->comment('Media Monitoring'),
            'status_id' => $this->integer()->comment('Status'),
            'position_structure_id' => $this->integer()->comment('Jabatan'),
            'pic_id' => $this->integer()->comment('PIC'),
            'calculation_id' => $this->integer()->comment(''),
            'polarization_id' => $this->integer()->comment('Polarisasi'),
            'participatory_budgeting_id' => $this->integer()->comment('PIC'),
            'assumption' => $this->text()->comment('Asumsi'),
            'note' => $this->text()->comment('Catatan'),
            'metadata' => 'json'
        ], $tableOptions);
        
        $this->addForeignKey('initiatives_fk_1', '{{%management.initiatives}}', 'okr_id', '{{%management.okr}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('initiatives_fk_2', '{{%management.initiatives}}', 'validity_id', '{{%management.validity}}', 'id', 'RESTRICT', 'CASCADE');
        // $this->addForeignKey('initiatives_fk_3', '{{%management.initiatives}}', 'controllability_id', '{{%management.controllability}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('initiatives_fk_4', '{{%management.initiatives}}', 'position_structure_id', '{{%management.position_structure}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('initiatives_fk_5', '{{%management.initiatives}}', 'calculation_id', '{{%management.calculation}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('initiatives_fk_6', '{{%management.initiatives}}', 'polarization_id', '{{%management.polarization}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('initiatives_fk_7', '{{%management.initiatives}}', 'participatory_budgeting_id', '{{%management.participatory_budgeting}}', 'id', 'RESTRICT', 'CASCADE');
        

        $this->createTable('{{%management.initiatives_history}}', [
            'id' => $this->primaryKey(),
            'initiatives_id' => $this->integer()->comment('Initiatives')->notNull(),
            'action' => $this->string(50)->notNull()->comment('Tindakan'),
            'comment' => $this->text()->comment('Komentar'),
            'by' => $this->integer()->comment('Dibuat Oleh'),
            'stamp' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'ip' => $this->string(70)->comment('Diubah Pada'),
        ], $tableOptions);
        
        $this->addForeignKey('initiatives_history_fk_1', '{{%management.initiatives_history}}', 'initiatives_id', '{{%management.initiatives}}', 'id', 'CASCADE', 'CASCADE');
        
        $this->createTable('{{%management.initiatives__customers}}', [
            'id' => $this->primaryKey(),
            'initiatives_id' => $this->integer()->comment('Initiatives')->notNull(),
            'customers_id' => $this->integer()->comment('Pelanggan')->notNull(),
        ], $tableOptions);
        
        $this->addForeignKey('initiatives__customers_fk_1', '{{%management.initiatives__customers}}', 'initiatives_id', '{{%management.initiatives}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('initiatives__customers_fk_2', '{{%management.initiatives__customers}}', 'customers_id', '{{%management.customers}}', 'id', 'RESTRICT', 'CASCADE');
        
        $this->createTable('{{%management.initiatives__stakeholder}}', [
            'id' => $this->primaryKey(),
            'initiatives_id' => $this->integer()->comment('Initiatives')->notNull(),
            'stakeholder_id' => $this->integer()->comment('Pemangku Kepentingan')->notNull(),
        ], $tableOptions);
        
        $this->addForeignKey('initiatives__stakeholder_fk_1', '{{%management.initiatives__stakeholder}}', 'initiatives_id', '{{%management.initiatives}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('initiatives__stakeholder_fk_2', '{{%management.initiatives__stakeholder}}', 'stakeholder_id', '{{%management.stakeholder}}', 'id', 'RESTRICT', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
