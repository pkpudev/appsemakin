<?php

use yii\db\Migration;

class m220403_150616_add_some_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->addColumn('{{%management.okr_org_mov}}', 'note', $this->text()->comment('Catatan'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
