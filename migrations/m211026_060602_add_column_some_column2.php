<?php

use yii\db\Migration;

class m211026_060602_add_column_some_column2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        

        $this->addColumn('{{%management.okr}}', 'target_q1', $this->float()->comment('Target Kuartal 1'));
        $this->addColumn('{{%management.okr}}', 'target_q2', $this->float()->comment('Target Kuartal 2'));
        $this->addColumn('{{%management.okr}}', 'target_q3', $this->float()->comment('Target Kuartal 3'));
        $this->addColumn('{{%management.okr}}', 'target_q4', $this->float()->comment('Target Kuartal 4'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
