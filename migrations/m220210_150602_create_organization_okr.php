<?php

use yii\db\Migration;

class m220210_150602_create_organization_okr extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.portfolio_type}}', [
            'id' => $this->primaryKey(),
            'portfolio_type' => $this->string(100)->comment('Jenis Portofolio')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);
        

        $this->createTable('{{%management.okr_org}}', [
            'id' => $this->primaryKey(),
            'okr' => $this->text(250)->comment('OKR')->notNull(),
            'type' => $this->string(2)->comment('Tipe')->notNull(),
            'okr_code' => $this->string(25)->comment('Kode OKR')->notNull(),
            'parent_id' => $this->integer()->comment('Parent'),
            'perspective_id' => $this->integer()->comment('Perspektif'),
            'portfolio_type_id' => $this->integer()->comment('Jenis Portofolio'),
            'target' => $this->float()->comment('Target'),
            'measure' => $this->string(10)->comment('Satuan'),
            'start_year' => $this->integer()->comment('Mulai Periode'),
            'end_year' => $this->integer()->comment('Akhir Periode'),
            'version' => $this->integer()->comment('Versi'),
            'weight' => $this->double()->comment('Bobot'),
            'implementation_model' => $this->string(50)->comment('Model Implementasi'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->addColumn('{{%management.quality_standard_documents}}', 'validation_date', $this->date()->comment('Tanggal Pengesahan'));
        $this->addColumn('{{%management.quality_standard_documents}}', 'implementation_date', $this->date()->comment('Tanggal Pemberlakuan'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
