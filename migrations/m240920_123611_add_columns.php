<?php

use yii\db\Migration;

class m240920_123611_add_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%management.okr_evaluation}}', 'position_structure_id', $this->integer());
        $this->addColumn('{{%management.ceiling}}', 'position_structure_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
