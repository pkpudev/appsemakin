<?php

use yii\db\Migration;

class m220331_150607_add_some_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->addColumn('{{%management.initiatives_evaluation}}', 'note', $this->text()->comment('Catatan'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
