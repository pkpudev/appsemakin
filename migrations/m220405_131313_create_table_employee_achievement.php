<?php

use yii\db\Migration;

class m220405_131313_create_table_employee_achievement extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.employee_achievement}}', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->comment('Nama'),
            'year' => $this->integer()->comment('Tahun'),
            'position_structure_id' => $this->integer()->comment('Jabatan'),
            'achieve_type' => $this->string(10)->comment('Tipe'),
            'achieve_value' => $this->float()->comment('Capaian'),
            'contribution_amount' => $this->float()->comment('Jumlah Kontribusi'),
            'fulfillment' => $this->float()->comment('Pengisian'),
        ], $tableOptions);

        $this->addForeignKey('employee_achievement_fk_1', '{{%management.employee_achievement}}', 'employee_id', '{{%sdm_employee}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('employee_achievement_fk_2', '{{%management.employee_achievement}}', 'position_structure_id', '{{%management.position_structure}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }
}
