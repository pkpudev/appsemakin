<?php

use yii\db\Migration;

class m211026_060602_add_column_some_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.okr_level}}', [
            'id' => $this->primaryKey(),
            'okr_level' => $this->string(150)->comment('Level')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);

        $this->createTable('{{%management.polarization}}', [
            'id' => $this->primaryKey(),
            'polarization_type' => $this->string(150)->comment('Tipe')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);

        $this->insert('management.polarization', [
            'id' => 1,
            'polarization_type' => 'Maximize',
            'is_active' => true
        ]);

        $this->insert('management.polarization', [
            'id' => 2,
            'polarization_type' => 'Minimize',
            'is_active' => true
        ]);

        $this->insert('management.polarization', [
            'id' => 3,
            'polarization_type' => 'Stabilize',
            'is_active' => true
        ]);

        $this->createTable('{{%management.calculation}}', [
            'id' => $this->primaryKey(),
            'calculation_type' => $this->string(150)->comment('Tipe')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);

        $this->insert('management.calculation', [
            'id' => 1,
            'calculation_type' => 'Average',
            'is_active' => true
        ]);

        $this->insert('management.calculation', [
            'id' => 2,
            'calculation_type' => 'Sum',
            'is_active' => true
        ]);

        $this->insert('management.calculation', [
            'id' => 3,
            'calculation_type' => 'Last',
            'is_active' => true
        ]);

        $this->insert('management.calculation', [
            'id' => 4,
            'calculation_type' => 'Min',
            'is_active' => true
        ]);
        
        $this->createTable('{{%management.validity}}', [
            'id' => $this->primaryKey(),
            'validity_type' => $this->string(150)->comment('Tipe')->notNull(),
            'value' => $this->float()->comment('Nilai'),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);

        $this->insert('management.validity', [
            'id' => 1,
            'validity_type' => 'Exact',
            'value' => 0.5,
            'is_active' => true
        ]);

        $this->insert('management.validity', [
            'id' => 2,
            'validity_type' => 'Proxy',
            'value' => 0.300000012,
            'is_active' => true
        ]);

        $this->insert('management.validity', [
            'id' => 3,
            'validity_type' => 'Activity',
            'value' => 0.200000003,
            'is_active' => true
        ]);

        $this->addColumn('{{%management.okr}}', 'support_type', $this->integer()->comment('Support'));
        $this->addColumn('{{%management.okr}}', 'polarization_id', $this->integer()->comment('Polarization'));
        $this->addColumn('{{%management.okr}}', 'calculation_id', $this->integer()->comment('Calculation'));
        $this->addColumn('{{%management.okr}}', 'validity_id', $this->integer()->comment('Validity'));
        $this->addColumn('{{%management.okr}}', 'weight', $this->double()->comment('Weight'));

        $this->addForeignKey('okr_fk_4', '{{%management.okr}}', 'polarization_id', '{{%management.polarization}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('okr_fk_5', '{{%management.okr}}', 'calculation_id', '{{%management.calculation}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('okr_fk_6', '{{%management.okr}}', 'validity_id', '{{%management.validity}}', 'id', 'RESTRICT', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
