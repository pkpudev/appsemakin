<?php

use yii\db\Migration;

class m220610_123609_insert_new_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
                
        $this->insert('management.status', [
            'status' => 'Dibatalkan',
            'scenario' => 'activity_evaluation',
            'is_active' => true
        ]);
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
