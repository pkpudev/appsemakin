<?php

use yii\db\Migration;

class m211014_050603_okr extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.cascading_type}}', [
            'id' => $this->primaryKey(),
            'cascading_type' => $this->string(100)->comment('Tipe Cascading')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);
        

        $this->createTable('{{%management.okr}}', [
            'id' => $this->primaryKey(),
            'okr_level_id' => $this->integer()->comment('Level'),
            'position_structure_id' => $this->integer()->comment('Unit'),
            'okr' => $this->text(250)->comment('OKR')->notNull(),
            'type' => $this->string(2)->comment('Tipe')->notNull(),
            'okr_code' => $this->string(25)->comment('Kode OKR')->notNull(),
            'parent_id' => $this->integer()->comment('Parent'),
            'ref_id' => $this->integer()->comment('Ref'),
            'cascading_type_id' => $this->integer()->comment('Tipe Cascading'),
            'target' => $this->float()->comment('Target'),
            'measure' => $this->string(10)->comment('Satuan'),
            'timebound_q1' => $this->date()->comment('Kuartal 1'),
            'timebound_q2' => $this->date()->comment('Kuartal 2'),
            'timebound_q3' => $this->date()->comment('Kuartal 3'),
            'timebound_q4' => $this->date()->comment('Kuartal 4'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('okr_fk_1', '{{%management.okr}}', 'position_structure_id', '{{%management.position_structure}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('okr_fk_2', '{{%management.okr}}', 'cascading_type_id', '{{%management.cascading_type}}', 'id', 'RESTRICT', 'CASCADE');
        
        

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
