<?php

use yii\db\Migration;

class m220331_150610_add_some_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->insert('management.status', [
            'status' => 'Selesai',
            'scenario' => 'generate_evaluation',
            'is_active' => true
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
