<?php

use yii\db\Migration;

class m220804_123609_create_activities_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.activities__files}}', [
            'id' => $this->primaryKey(),
            'activities_id' => $this->integer()->comment('Kegiatan'),
            'files_id' => $this->integer()->comment('File'),
        ], $tableOptions);

        $this->addForeignKey('activities__files_fk_1', '{{%management.activities__files}}', 'activities_id', '{{%management.activities}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
