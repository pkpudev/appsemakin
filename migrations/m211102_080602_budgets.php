<?php

use yii\db\Migration;

class m211102_080602_budgets extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%management.cost_type}}', [
            'id' => $this->primaryKey(),
            'cost_type' => $this->string(100)->comment('Jenis Pembayaran')->notNull(),
            'description' => $this->text()->comment('Deskripsi'),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);

        $this->createTable('{{%management.cost_priority}}', [
            'id' => $this->primaryKey(),
            'cost_priority' => $this->string(100)->comment('Prioritas Pembayaran')->notNull(),
            'description' => $this->text()->comment('Deskripsi'),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);
        
        $this->createTable('{{%management.budget}}', [
            'id' => $this->primaryKey(),
            'activity_id' => $this->integer()->comment('activity')->notNull(),
            'budget_code' => $this->string(30)->comment('Kode RKAT'),
            'description' => $this->text()->comment('Rincian Kebutuhan'),
            'volume' => $this->integer()->comment('Volume'),
            'measure' => $this->string(25)->comment('Satuan'),
            'frequency' => $this->integer()->comment('Frekuensi'),
            'unit_amount' => $this->double()->comment('Satuan'),
            'total_amount' => $this->double()->comment('Jumlah'),
            'year' => $this->integer()->comment('Tahun'),
            'month_01' => $this->double()->comment('Januari'),
            'month_02' => $this->double()->comment('Februari'),
            'month_03' => $this->double()->comment('Maret'),
            'month_04' => $this->double()->comment('April'),
            'month_05' => $this->double()->comment('Mei'),
            'month_06' => $this->double()->comment('Juni'),
            'month_07' => $this->double()->comment('Juli'),
            'month_08' => $this->double()->comment('Agustus'),
            'month_09' => $this->double()->comment('September'),
            'month_10' => $this->double()->comment('Oktober'),
            'month_11' => $this->double()->comment('November'),
            'month_12' => $this->double()->comment('Desember'),
            'status_id' => $this->integer()->comment('Status'),
            'source_of_funds_id' => $this->integer()->comment(''),
            'cost_type_id' => $this->integer()->comment('Tipe Pembayaran'),
            'cost_priority_id' => $this->integer()->comment('Prioritas Pembayaran'),
            'account_id' => $this->integer()->comment('Akun Keuangan'),
            'note' => $this->text()->comment('Catatan'),
            'total_real' => $this->double()->comment('Total Realisasi'),
            'can_over_budget' => $this->boolean()->comment('Can Over Budget'),
        ], $tableOptions);
        
        $this->addForeignKey('budget_fk_1', '{{%management.budget}}', 'activity_id', '{{%management.activity}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('budget_fk_2', '{{%management.budget}}', 'source_of_funds_id', '{{%finance.source_of_funds}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('budget_fk_3', '{{%management.budget}}', 'cost_type_id', '{{%management.cost_type}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('budget_fk_4', '{{%management.budget}}', 'cost_priority_id', '{{%management.cost_priority}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('budget_fk_5', '{{%management.budget}}', 'account_id', '{{%finance.account}}', 'id', 'RESTRICT', 'CASCADE');
         

        $this->createTable('{{%management.budget_history}}', [
            'id' => $this->primaryKey(),
            'budget_id' => $this->integer()->comment('Anggaran')->notNull(),
            'action' => $this->string(50)->notNull()->comment('Tindakan'),
            'comment' => $this->text()->comment('Komentar'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'created_stamp' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'ip' => $this->string(70)->comment('Diubah Pada'),
        ], $tableOptions);
        
        $this->addForeignKey('budget_history_fk_1', '{{%management.budget_history}}', 'budget_id', '{{%management.budget}}', 'id', 'CASCADE', 'CASCADE');
        
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
