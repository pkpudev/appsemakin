<?php

use yii\db\Migration;

class m211202_0150602_create_target_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%management.target}}', [
            'id' => $this->primaryKey(),
            'target_name' => $this->string(150)->comment('Target')->notNull(),
            'unit_structure_id' => $this->integer()->comment('Unit'),
            'year' => $this->integer()->comment('Tahun'),
            'total_target' => $this->double()->comment('Total Target'),
            'month_01' => $this->double()->comment('Januari'),
            'month_02' => $this->double()->comment('Februari'),
            'month_03' => $this->double()->comment('Maret'),
            'month_04' => $this->double()->comment('April'),
            'month_05' => $this->double()->comment('Mei'),
            'month_06' => $this->double()->comment('Juni'),
            'month_07' => $this->double()->comment('Juli'),
            'month_08' => $this->double()->comment('Agustus'),
            'month_09' => $this->double()->comment('September'),
            'month_10' => $this->double()->comment('Oktober'),
            'month_11' => $this->double()->comment('November'),
            'month_12' => $this->double()->comment('Desember'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->addForeignKey('target_fk_1', '{{%management.target}}', 'unit_structure_id', '{{%management.unit_structure}}', 'id', 'RESTRICT', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
