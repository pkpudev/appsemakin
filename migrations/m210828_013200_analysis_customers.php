<?php

use yii\db\Migration;

class m210828_013200_analysis_customers extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.customers}}', [
            'id' => $this->primaryKey(),
            'customer' => $this->string(50)->comment('Pelanggan'),
            'description' => $this->text()->comment('Deskripsi'),
            'is_active' => $this->boolean()->comment('Aktif?')->defaultValue(true),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.customers_analysis}}', [
            'id' => $this->primaryKey(),
            'customers_id' => $this->integer()->comment('Pelanggan'),
            'needs' => $this->text()->comment('Kebutuhan'),
            'hope' => $this->text()->comment('Harapan'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('customers_analysis_fk_1', '{{%management.customers_analysis}}', 'customers_id', '{{%management.customers}}', 'id', 'CASCADE', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
