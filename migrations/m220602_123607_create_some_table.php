<?php

use yii\db\Migration;

class m220602_123607_create_some_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.activity_evaluation_value}}', [
            'id' => $this->primaryKey(),
            'evaluation' => $this->string(150)->comment('Evaluasi'),
            'evaluation_value' => $this->float()->comment('Nilai Evaluasi'),
        ], $tableOptions);
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
