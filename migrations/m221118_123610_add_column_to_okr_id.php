<?php

use yii\db\Migration;

class m221118_123610_add_column_to_okr_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->addColumn('{{%management.okr}}', 'ref_initiatives_id', $this->integer()->comment('Ref Proker'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
