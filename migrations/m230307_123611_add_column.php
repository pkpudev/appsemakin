<?php

use yii\db\Migration;

class m230307_123611_add_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%management.activities}}', 'project_id', $this->integer()->comment('Project'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
