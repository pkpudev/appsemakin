<?php

use yii\db\Migration;

class m220608_123608_create_activities_issue_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
                
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.issue_type}}', [
            'id' => $this->primaryKey(),
            'issue_type' => $this->string(250)->comment('Tipe Kendala'),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);

        $this->createTable('{{%management.activities_issue}}', [
            'id' => $this->primaryKey(),
            'activities_id' => $this->integer()->comment('Kegiatan'),
            'reporting_date' => $this->date()->comment('Tanggal Pelaporan'),
            'issue' => $this->text()->comment('Kendala'),
            'issue_type_id' => $this->integer()->comment('Jenis Kendala'),
            'issue_level' => $this->integer()->comment('Level Kendala'),
            'alternative_solution' => $this->text()->comment('Alternatif Solusi'),
            'resolution' => $this->text()->comment('Resolusi'),
            'due_date' => $this->date()->comment('Tenggat Waktu Penyelesaian'),
            'status_id' => $this->integer()->comment('Status'),
        ], $tableOptions);
        
        $this->addForeignKey('activities_issue_fk_1', '{{%management.activities_issue}}', 'activities_id', '{{%management.activities}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('activities_issue_fk_2', '{{%management.activities_issue}}', 'issue_type_id', '{{%management.issue_type}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('activities_issue_fk_3', '{{%management.activities_issue}}', 'status_id', '{{%management.status}}', 'id', 'RESTRICT', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
