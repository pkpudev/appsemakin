<?php

use yii\db\Migration;

class m220531_123606_insert_new_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->insert('management.status', [
            'status' => 'Belum Dimulai',
            'scenario' => 'activity_evaluation',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'Sedang Berlangsung',
            'scenario' => 'activity_evaluation',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'Selesai',
            'scenario' => 'activity_evaluation',
            'is_active' => true
        ]);
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
