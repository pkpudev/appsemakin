<?php

use yii\db\Migration;

class m220620_123609_add_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
           
        $this->addColumn('{{%management.initiatives_evaluation}}', 'postponed_from', $this->string(100)->comment('Ditunda Dari'));
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
