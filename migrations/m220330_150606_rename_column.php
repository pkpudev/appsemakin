<?php

use yii\db\Migration;

class m220330_150606_rename_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->renameColumn('{{%management.activity}}', 'status_evaluation_id', 'evaluation_status_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
