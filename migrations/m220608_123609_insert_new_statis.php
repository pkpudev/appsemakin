<?php

use yii\db\Migration;

class m220608_123609_insert_new_statis extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
                
        $this->insert('management.status', [
            'status' => 'Masih Berjalan',
            'scenario' => 'issue_status',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'Dialihkan',
            'scenario' => 'issue_status',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'Selesai',
            'scenario' => 'issue_status',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'Berisiko',
            'scenario' => 'issue_status',
            'is_active' => true
        ]);
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
