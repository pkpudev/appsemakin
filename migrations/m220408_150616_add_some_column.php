<?php

use yii\db\Migration;

class m220408_150616_add_some_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->addColumn('{{%management.okr_achievement}}', 'performance_analysis', $this->text()->comment('Analisis Capaian'));
        $this->addColumn('{{%management.okr_achievement}}', 'interpretation', $this->text()->comment('Interpretasi'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
