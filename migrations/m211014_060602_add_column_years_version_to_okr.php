<?php

use yii\db\Migration;

class m211014_060602_add_column_years_version_to_okr extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%management.okr}}', 'year', $this->integer()->comment('Tahun'));
        $this->addColumn('{{%management.okr}}', 'version', $this->integer()->comment('Versi'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
