<?php

use yii\db\Migration;

class m220803_123609_create_activities_for_okr_triggers_and_function extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute(
<<<FN
CREATE OR REPLACE FUNCTION management.okr_real(
    v_okr_id integer)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS \$BODY\$

DECLARE
    v_total_real        	DOUBLE PRECISION;
BEGIN

    SELECT sum(okr_value) into v_total_real FROM management.activities a1 WHERE okr_id = v_okr_id;
    
    UPDATE management.okr
    SET achievement_percentage = v_total_real
    WHERE id = v_okr_id;
  
  return 1;
END;

\$BODY\$;
FN
        );


        $this->execute(
<<<FN
CREATE OR REPLACE FUNCTION management.activities_insert2()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS \$BODY\$

BEGIN
    perform  management.okr_real(NEW.okr_id);
    return NEW;
END;

\$BODY\$;
FN
        );


        $this->execute(
<<<FN
CREATE OR REPLACE FUNCTION management.activities_update2()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS \$BODY\$

BEGIN
    perform  management.okr_real(NEW.okr_id);
    perform  management.okr_real(OLD.okr_id);
    return NEW;
END;

\$BODY\$;
FN
        );


        $this->execute(
<<<FN
CREATE OR REPLACE FUNCTION management.activities_delete2()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS \$BODY\$

BEGIN
    perform  management.okr_real(OLD.okr_id);
    return OLD;
END;

\$BODY\$;
FN
        );


        $this->execute(
<<<FN
CREATE TRIGGER activities_insert2
    AFTER INSERT
    ON management.activities
    FOR EACH ROW
    EXECUTE PROCEDURE management.activities_insert2();
FN
        );


        $this->execute(
<<<FN
CREATE TRIGGER activities_update2
    AFTER UPDATE OF okr_value
    ON management.activities
    FOR EACH ROW
    EXECUTE PROCEDURE management.activities_update2();
FN
        );


        $this->execute(
<<<FN
CREATE TRIGGER activities_delete2
    AFTER DELETE
    ON management.activities
    FOR EACH ROW
    EXECUTE PROCEDURE management.activities_delete2();
FN
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
