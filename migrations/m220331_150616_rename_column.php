<?php

use yii\db\Migration;

class m220331_150616_rename_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->renameColumn('{{%management.okr_evaluation}}', 'interpreatation', 'interpretation');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
