<?php

use yii\db\Migration;

class m240908_123609_create_okr_weight_triggers_and_function extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->execute('DROP TRIGGER IF EXISTS okr_tr_insert ON management.okr');
        $this->execute('DROP TRIGGER IF EXISTS okr_tr_update ON management.okr');
        $this->execute('DROP TRIGGER IF EXISTS okr_tr_delete ON management.okr');

        $this->execute('DROP FUNCTION IF EXISTS management.okr_insert()');
        $this->execute('DROP FUNCTION IF EXISTS management.okr_update()');
        $this->execute('DROP FUNCTION IF EXISTS management.okr_delete()');

        $this->execute(
<<<FN
CREATE OR REPLACE FUNCTION management.okr_weight_unit_weight(v_okr_id integer)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS \$BODY\$

DECLARE
    v_okr_level_id        	    INTEGER;
    v_position_structure_id	    INTEGER;
    v_ref_id        	        INTEGER;
    v_parent_id        	        INTEGER;

    v_validity_cur        	    DOUBLE PRECISION;
    v_controllability_cur       DOUBLE PRECISION;
    v_validity_val        	    DOUBLE PRECISION;
    v_controllability_val       DOUBLE PRECISION;
    v_validity_ind        	    DOUBLE PRECISION;
    v_controllability_ind       DOUBLE PRECISION;
    v_weight        	        DOUBLE PRECISION;
    
    v_validity_val2        	    DOUBLE PRECISION;
    v_controllability_val2      DOUBLE PRECISION;
    v_validity_ind2        	    DOUBLE PRECISION;
    v_controllability_ind2      DOUBLE PRECISION;
    v_unit_weight      	        DOUBLE PRECISION;

    row_data                    RECORD;
BEGIN

    SELECT 
        okr_level_id, 
        position_structure_id,
        ref_initiatives_id, 
        parent_id,
        v1.value,
        c1.value
        INTO 
        v_okr_level_id, 
        v_position_structure_id,
        v_ref_id, 
        v_parent_id,
        v_validity_cur,
        v_controllability_cur
    FROM 
        management.okr o1
    LEFT JOIN management.validity v1 ON o1.validity_id = v1.id
        LEFT JOIN management.controllability c1 ON o1.controllability_id = c1.id
    WHERE o1.id = v_okr_id;

    -- CALCULATE WEIGHT
    IF v_okr_level_id = 5 THEN
        SELECT 
            sum(value) into v_validity_val 
        FROM 
            management.okr o
        LEFT JOIN management.validity v ON o.validity_id = v.id
        WHERE 
            ref_initiatives_id = v_ref_id AND
            type = 'KR';
    ELSE
        SELECT 
            sum(value) into v_validity_val 
        FROM 
            management.okr o
        LEFT JOIN management.validity v ON o.validity_id = v.id
        WHERE 
            parent_id = v_parent_id AND
            type = 'KR';
    END IF;

    IF v_validity_val > 0 THEN
        v_validity_ind = (v_validity_cur * 100) / v_validity_val;
    ELSE
        v_validity_ind = 0;
    END IF;

    IF v_okr_level_id = 5 THEN
        SELECT 
            sum(value) into v_controllability_val 
        FROM 
            management.okr o
        LEFT JOIN management.controllability c ON o.controllability_id = c.id
        WHERE 
            ref_initiatives_id = v_ref_id AND
            type = 'KR';
    ELSE
        SELECT 
            sum(value) into v_controllability_val 
        FROM 
            management.okr o
        LEFT JOIN management.controllability c ON o.controllability_id = c.id
        WHERE 
            parent_id = v_parent_id AND
            type = 'KR';
    END IF;

    IF v_controllability_val > 0 THEN
        v_controllability_ind = (v_validity_cur * 100) / v_controllability_val;
    ELSE
        v_controllability_ind = 0;
    END IF;

    -- SET WEIGHT
    v_weight = (v_validity_ind + v_controllability_ind) / 2;


    -- CALCULATE UNIT WEIGHT
    SELECT 
        sum(value) into v_validity_val2 
    FROM 
        management.okr o
    LEFT JOIN management.validity v ON o.validity_id = v.id
    WHERE 
        position_structure_id = v_position_structure_id AND
        type = 'KR';

    IF v_validity_val2 > 0 THEN
        v_validity_ind2 = (v_validity_cur * 100) / v_validity_val2;
    ELSE
        v_validity_ind2 = 0;
    END IF;

    SELECT 
        sum(value) into v_controllability_val2 
    FROM 
        management.okr o
    LEFT JOIN management.controllability c ON o.controllability_id = c.id
    WHERE 
        position_structure_id = v_position_structure_id AND
        type = 'KR';

    IF v_controllability_val2 > 0 THEN
        v_controllability_ind2 = (v_validity_cur * 100) / v_controllability_val2;
    ELSE
        v_controllability_ind2 = 0;
    END IF;

    -- SET UNIT WEIGHT
    v_unit_weight = (v_validity_ind2 + v_controllability_ind2) / 2;

    -- UPDATE OKR
    UPDATE management.okr
    SET weight = v_weight, unit_weight = v_unit_weight
    WHERE id = v_okr_id;
  
  return 1;
END;

\$BODY\$;
FN
        );


        $this->execute(
<<<FN
CREATE FUNCTION management.okr_insert()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS \$BODY\$

BEGIN
    perform  management.okr_weight_unit_weight(NEW.parent_id);
    return NEW;
END;

\$BODY\$;
FN
        );


        $this->execute(
<<<FN
CREATE FUNCTION management.okr_update()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS \$BODY\$
DECLARE
    v_new_position_id       INTEGER;
    v_new_pic_id	        INTEGER;
    v_new_year        	    INTEGER;
    v_old_position_id       INTEGER;
    v_old_pic_id        	INTEGER;
    v_old_year        	    INTEGER;
    row_data                RECORD;
BEGIN
    v_new_position_id		:= NEW.position_structure_id;
	v_new_pic_id			:= NEW.pic_id;
	v_new_year				:= NEW.year;
	v_old_position_id		:= OLD.position_structure_id;
	v_old_pic_id			:= OLD.pic_id;
	v_old_year				:= OLD.year;
    
    -- loop for new okr 
    FOR row_data IN SELECT id from management.okr
		where position_structure_id = v_new_position_id and pic_id = v_new_pic_id and "year" = v_new_year and okr_level_id <> 1 and "type" IN ('KR') 
    LOOP
       	PERFORM management.okr_weight_unit_weight(row_data.id);
    END LOOP;
    
    -- loop for old okr 
    FOR row_data IN SELECT id from management.okr
		where position_structure_id = v_old_position_id and pic_id = v_old_pic_id and "year" = v_old_year and okr_level_id <> 1 and "type" IN ('KR')
    LOOP
       	PERFORM management.okr_weight_unit_weight(row_data.id);
    END LOOP;
    
    return NEW;
END;

\$BODY\$;
FN
        );


        $this->execute(
<<<FN
CREATE FUNCTION management.okr_delete()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS \$BODY\$
DECLARE
    v_position_id       INTEGER;
    v_pic_id	        INTEGER;
    v_year        	    INTEGER;
    row_data            RECORD;
BEGIN
    v_position_id		:= old.position_structure_id;
	v_pic_id			:= old.pic_id;
	v_year				:= old.year;
    
    FOR row_data IN SELECT id from management.okr
		where position_structure_id = v_position_id and pic_id = v_pic_id and "year" = v_year and okr_level_id <> 1 and "type" IN ('KR')
    LOOP
       	PERFORM management.okr_weight_unit_weight(row_data.id);
    END LOOP;

    return OLD;
END;

\$BODY\$;
FN
        );
   
        $this->execute(
<<<FN
CREATE TRIGGER okr_tr_insert
    AFTER INSERT
    ON management.okr FOR EACH ROW EXECUTE FUNCTION management.okr_insert()
FN
        );
   
        $this->execute(
<<<FN
CREATE TRIGGER okr_tr_update
    AFTER UPDATE
    OF pic_id,
    position_structure_id,
    year,
    validity_id,
    controllability_id,
    status_id on
    management.okr FOR EACH ROW EXECUTE FUNCTION management.okr_update()
FN
        );
   
        $this->execute(
<<<FN
CREATE TRIGGER okr_tr_delete
    AFTER DELETE
    ON management.okr FOR EACH ROW EXECUTE FUNCTION management.okr_insert()
FN
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
