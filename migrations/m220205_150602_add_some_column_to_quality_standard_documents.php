<?php

use yii\db\Migration;

class m220205_150602_add_some_column_to_quality_standard_documents extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->addColumn('{{%management.quality_standard_documents}}', 'editable_files_id', $this->integer()->comment('Editable File'));
        $this->addColumn('{{%management.quality_standard_documents}}', 'unit_structure_id', $this->integer()->comment('Unit'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
