<?php

use yii\db\Migration;

class m220331_150609_add_some_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->addColumn('{{%management.okr_org}}', 'validity_id', $this->integer()->comment('Validitas'));
        $this->addColumn('{{%management.okr_org}}', 'validity_index', $this->float()->comment(''));
        $this->addColumn('{{%management.okr_org}}', 'controllability_id', $this->integer()->comment('Tingkat Pengendalian'));
        $this->addColumn('{{%management.okr_org}}', 'controllability_index', $this->float()->comment(''));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
