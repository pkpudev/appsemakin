<?php

use yii\db\Migration;

class m220729_123609_create_activities_triggers_and_function extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute(
<<<FN
CREATE OR REPLACE FUNCTION management.progress(
    v_parent_id integer)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS \$BODY\$

DECLARE
    v_total_weight        	DOUBLE PRECISION;
    v_total_progress      	DOUBLE PRECISION;
    v_rs                  	management.activities%ROWTYPE;
BEGIN

    SELECT sum(weight) into v_total_weight FROM management.activities a1 WHERE parent_id = v_parent_id  and status_id <> 25;
    
    FOR v_rs IN
        SELECT 
            *
        FROM management.activities a2
        WHERE parent_id = v_parent_id  and status_id <> 25
    LOOP
        v_total_progress = coalesce(max(v_total_progress), 0) + coalesce(max(((v_rs.progress / 100) * ((v_rs.weight / v_total_weight) * 100))), 0);
        -- raise notice 'total: %', v_total_progress;
    END LOOP;

    UPDATE management.activities
    SET progress = v_total_progress
    WHERE id = v_parent_id;
  
  return 1;
END;

\$BODY\$;
FN
        );


        $this->execute(
<<<FN
CREATE FUNCTION management.activities_insert()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS \$BODY\$

BEGIN
    perform  management.progress(NEW.parent_id);
    return NEW;
END;

\$BODY\$;
FN
        );


        $this->execute(
<<<FN
CREATE FUNCTION management.activities_update()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS \$BODY\$

BEGIN
    perform  management.progress(NEW.parent_id);
    return NEW;
END;

\$BODY\$;
FN
        );


        $this->execute(
<<<FN
CREATE FUNCTION management.activities_delete()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS \$BODY\$

BEGIN
    perform  management.progress(OLD.parent_id);
    return OLD;
END;

\$BODY\$;
FN
        );


        $this->execute(
<<<FN
CREATE TRIGGER activities_insert
    AFTER INSERT
    ON management.activities
    FOR EACH ROW
    EXECUTE PROCEDURE management.activities_insert();
FN
        );


        $this->execute(
<<<FN
CREATE TRIGGER activities_update
    AFTER UPDATE OF progress, weight, status_id
    ON management.activities
    FOR EACH ROW
    EXECUTE PROCEDURE management.activities_update();
FN
        );


        $this->execute(
<<<FN
CREATE TRIGGER activities_delete
    AFTER DELETE
    ON management.activities
    FOR EACH ROW
    EXECUTE PROCEDURE management.activities_delete();
FN
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
