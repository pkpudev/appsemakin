<?php

use yii\db\Migration;

class m220527_123606_create_activities_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.activity_category}}', [
            'id' => $this->primaryKey(),
            'activity_category' => $this->string(150)->comment('Kategori Kegiatan'),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);

        $this->createTable('{{%management.activities}}', [
            'id' => $this->primaryKey(),
            'activity' => $this->string()->comment('Kegiatan')->notNull(),
            'description' => $this->string()->comment('Deskripsi'),
            'pic_id' => $this->integer()->comment('PIC')->notNull(),
            'activity_category_id' => $this->integer()->comment('Kategori'),
            'position_structure_id' => $this->integer()->comment('Jabatan')->notNull(),
            'okr_id' => $this->integer()->comment('Key Result')->notNull(),
            'parent_id' => $this->integer()->comment('Parent'),
            'start_date' => $this->dateTime()->comment('Dari'),
            'finish_date' => $this->dateTime()->comment('Sampai'),
            'due_date' => $this->dateTime()->comment('Jatuh Tempo'),
            'status_id' => $this->integer()->comment('Status'),
            'duration' => $this->float()->comment('Durasi'),
            'progress' => $this->float()->comment('Progres'),
            'okr_value' => $this->float()->comment('Nilai OKR'),
            'evaluation' => $this->float()->comment('Penilaian Atasan'),
            'evaluation_value' => $this->float()->comment('Nilai'),
        ], $tableOptions);

        $this->addForeignKey('activities_fk_1', '{{%management.activities}}', 'activity_category_id', '{{%management.activity_category}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('activities_fk_2', '{{%management.activities}}', 'position_structure_id', '{{%management.position_structure}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('activities_fk_3', '{{%management.activities}}', 'okr_id', '{{%management.okr}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('activities_fk_4', '{{%management.activities}}', 'status_id', '{{%management.status}}', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('{{%management.activities_history}}', [
            'id' => $this->primaryKey(),
            'activities_id' => $this->integer()->comment('Kegiatan')->notNull(),
            'action' => $this->string(50)->notNull()->comment('Tindakan'),
            'comment' => $this->text()->comment('Komentar'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'created_stamp' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'ip' => $this->string(70)->comment('IP'),
        ], $tableOptions);
        
        $this->addForeignKey('activities_history_fk_1', '{{%management.activities_history}}', 'activities_id', '{{%management.activities}}', 'id', 'CASCADE', 'CASCADE');
        
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
