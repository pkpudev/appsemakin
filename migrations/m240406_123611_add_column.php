<?php

use yii\db\Migration;

class m240406_123611_add_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%management.okr_evaluation}}', 'achieve_value', $this->double()->comment('Capaian'));
        $this->addColumn('{{%management.initiatives_evaluation}}', 'achieve_value', $this->double()->comment('Capaian'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
