<?php

use yii\db\Migration;

class m220622_123609_add_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
           
        $this->addColumn('{{%management.okr}}', 'achievement_percentage', $this->float()->comment('Persentase Capaian'));
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
