<?php

use yii\db\Migration;

class m220315_150603_add_column_pic_id_to_okr extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->addColumn('{{%management.okr}}', 'pic_id', $this->integer()->comment('PIC'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
