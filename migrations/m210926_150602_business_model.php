<?php

use yii\db\Migration;

class m210926_150602_business_model extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.portfolio}}', [
            'id' => $this->primaryKey(),
            'portfolio_name' => $this->string(250)->comment('Portofolio')->notNull(),
            'version' => $this->string(20)->comment('Versi'),
            'portfolio_date' => $this->date()->comment('Tanggal'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.portfolio_element}}', [
            'id' => $this->primaryKey(),
            'element' => $this->string(250)->comment('Elemen')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);

        $this->createTable('{{%management.portfolio_canvas}}', [
            'id' => $this->primaryKey(),
            'portfolio_id' => $this->integer()->comment('Portofolio')->notNull(),
            'element_id' => $this->integer()->comment('Elemen')->notNull(),
            'description' => $this->text()->comment('Deskripsi'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('portfolio_canvas_fk_1', '{{%management.portfolio_canvas}}', 'portfolio_id', '{{%management.portfolio}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('portfolio_canvas_fk_2', '{{%management.portfolio_canvas}}', 'element_id', '{{%management.portfolio_element}}', 'id', 'RESTRICT', 'CASCADE');
        

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
