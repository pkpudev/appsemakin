<?php

use yii\db\Migration;

class m220315_150604_create_evaluation extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        /** EVALUATION */
        $this->createTable('{{%management.evaluation}}', [
            'id' => $this->primaryKey(),
            'period' => $this->string(100)->comment('Periode')->notNull(),
            'year' => $this->integer()->comment('Tahun'),
            'start_date' => $this->date()->comment('Tanggal Mulai'),
            'end_date' => $this->date()->comment('Tanggal Akhir'),
            'status_id' => $this->integer()->comment('Status'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        /** ACTIVITY EVALUATION */
        $this->createTable('{{%management.activity_evaluation}}', [
            'id' => $this->primaryKey(),
            'evaluation_id' => $this->integer()->comment('Evaluasi'),
            'activity_id' => $this->integer()->comment('Kegiatan'),
            'actual_value' => $this->integer()->comment('Realisasi'),
            'actual_note' => $this->text()->comment('Catatan Pelaksanaan'),
        ], $tableOptions);

        $this->addForeignKey('activity_evaluation_fk_1', '{{%management.activity_evaluation}}', 'evaluation_id', '{{%management.evaluation}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('activity_evaluation_fk_2', '{{%management.activity_evaluation}}', 'activity_id', '{{%management.activity}}', 'id', 'CASCADE', 'CASCADE');
        
        $this->createTable('{{%management.activity_evaluation_history}}', [
            'id' => $this->primaryKey(),
            'activity_evaluation_id' => $this->integer()->comment('Evaluasi Kegiatan')->notNull(),
            'action' => $this->string(50)->notNull()->comment('Tindakan'),
            'comment' => $this->text()->comment('Komentar'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'created_stamp' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'ip' => $this->string(70)->comment('Diubah Pada'),
        ], $tableOptions);
        
        $this->addForeignKey('activity_evaluation_history_fk_1', '{{%management.activity_evaluation_history}}', 'activity_evaluation_id', '{{%management.activity_evaluation}}', 'id', 'CASCADE', 'CASCADE');
        
        $this->createTable('{{%management.activity_mov}}', [
            'id' => $this->primaryKey(),
            'activity_evaluation_id' => $this->integer()->comment('Evaluasi Kegiatan'),
            'mov_name' => $this->string(250)->comment('Nama Dokumen'),
            'files_id' => $this->integer()->comment('File'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->addForeignKey('activity_mov_fk_1', '{{%management.activity_mov}}', 'activity_evaluation_id', '{{%management.activity_evaluation}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%management.activity_issue}}', [
            'id' => $this->primaryKey(),
            'activity_evaluation_id' => $this->integer()->comment('Evaluasi Kegiatan'),
            'issue' => $this->text()->comment('Isu'),
            'follow_up' => $this->text()->comment('Tindak Lanjut'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->addForeignKey('activity_issue_fk_1', '{{%management.activity_issue}}', 'activity_evaluation_id', '{{%management.activity_evaluation}}', 'id', 'CASCADE', 'CASCADE');
        
        /** INITIATIVES EVALUATION */
        $this->createTable('{{%management.initiatives_evaluation}}', [
            'id' => $this->primaryKey(),
            'evaluation_id' => $this->integer()->comment('Evaluasi'),
            'initiatives_id' => $this->integer()->comment('Program Kerja'),
            'target' => $this->integer()->comment('Target'),
            'real' => $this->text()->comment('Realisasi'),
            'employees_involved' => $this->text()->comment('Karyawan Berkontribusi'),
        ], $tableOptions);

        $this->addForeignKey('initiatives_evaluation_fk_1', '{{%management.initiatives_evaluation}}', 'evaluation_id', '{{%management.evaluation}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('initiatives_evaluation_fk_2', '{{%management.initiatives_evaluation}}', 'initiatives_id', '{{%management.initiatives}}', 'id', 'CASCADE', 'CASCADE');
        
        $this->createTable('{{%management.initiatives_evaluation_history}}', [
            'id' => $this->primaryKey(),
            'initiatives_evaluation_id' => $this->integer()->comment('Evaluasi Program Kerja')->notNull(),
            'action' => $this->string(50)->notNull()->comment('Tindakan'),
            'comment' => $this->text()->comment('Komentar'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'created_stamp' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'ip' => $this->string(70)->comment('Diubah Pada'),
        ], $tableOptions);
        
        $this->addForeignKey('initiatives_evaluation_history_fk_1', '{{%management.initiatives_evaluation_history}}', 'initiatives_evaluation_id', '{{%management.initiatives_evaluation}}', 'id', 'CASCADE', 'CASCADE');
        
        $this->createTable('{{%management.initiatives_mov}}', [
            'id' => $this->primaryKey(),
            'initiatives_evaluation_id' => $this->integer()->comment('Evaluasi Program Kerja'),
            'mov_name' => $this->string(250)->comment('Nama Dokumen'),
            'files_id' => $this->integer()->comment('File'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->addForeignKey('initiatives_mov_fk_1', '{{%management.initiatives_mov}}', 'initiatives_evaluation_id', '{{%management.initiatives_evaluation}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
