<?php

use yii\db\Migration;

class m240523_123611_add_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%management.evaluation}}', 'use_old_pattern', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
