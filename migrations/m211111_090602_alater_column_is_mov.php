<?php

use yii\db\Migration;

class m211111_090602_alater_column_is_mov extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->alterColumn('{{%management.activity}}', 'is_mov', 'integer USING CAST(is_mov AS integer)');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
