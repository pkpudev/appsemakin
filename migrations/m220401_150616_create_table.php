<?php

use yii\db\Migration;

class m220401_150616_create_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.tutorial}}', [
            'id' => $this->primaryKey(),
            'title' => $this->text()->comment('Judul'),
            'link' => $this->text()->comment('Link'),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
