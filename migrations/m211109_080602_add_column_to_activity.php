<?php

use yii\db\Migration;

class m211109_080602_add_column_to_activity extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%management.activity}}', 'difficulty_level', $this->smallInteger()->comment('Tingkat Kesulitan'));
        $this->addColumn('{{%management.activity}}', 'resources_level', $this->smallInteger()->comment('Sumber Daya yg Dibutuhkan'));
        $this->addColumn('{{%management.activity}}', 'is_mov', $this->boolean()->comment('Dokumen Keluaran'));
        $this->addColumn('{{%management.activity}}', 'dissatisfaction_level', $this->smallInteger()->comment('Pengaruh terhadap ketidakpuasan'));
        $this->addColumn('{{%management.activity}}', 'weight_value', $this->float()->comment('Nilai Bobot'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
