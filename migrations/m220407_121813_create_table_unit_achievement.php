<?php

use yii\db\Migration;

class m220407_121813_create_table_unit_achievement extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.unit_achievement}}', [
            'id' => $this->primaryKey(),
            'unit_id' => $this->integer()->comment('Unit Kerja'),
            'evaluation_id' => $this->integer(),
            'achieve_type' => $this->string(10)->comment('Tipe'),
            'achieve_value' => $this->float()->comment('Capaian'),
            'fulfillment' => $this->float()->comment('Status Pengisian'),
        ], $tableOptions);

        $this->addForeignKey('unit_achievement_fk_1', '{{%management.unit_achievement}}', 'unit_id', '{{%management.unit_structure}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('unit_achievement_fk_2', '{{%management.unit_achievement}}', 'evaluation_id', '{{%management.evaluation}}', 'id', 'RESTRICT', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }
}
