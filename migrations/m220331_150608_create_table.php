<?php

use yii\db\Migration;

class m220331_150608_create_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.okr_mov_type}}', [
            'id' => $this->primaryKey(),
            'okr_id' => $this->integer()->comment('OKR'),
            'mov_name' => $this->string(2500)->comment('Alat Verifikasi'),
        ], $tableOptions);

        $this->createTable('{{%management.okr_org_mov}}', [
            'id' => $this->primaryKey(),
            'okr_evaluation_id' => $this->integer()->comment('Evaluasi OKR'),
            'okr_mov_type_id' => $this->integer()->comment('Tipe MOV'),
            'files_id' => $this->integer()->comment('File'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->addForeignKey('okr_org_mov_fk_1', '{{%management.okr_org_mov}}', 'okr_evaluation_id', '{{%management.okr_evaluation}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('okr_org_mov_fk_2', '{{%management.okr_org_mov}}', 'okr_mov_type_id', '{{%management.okr_mov_type}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
