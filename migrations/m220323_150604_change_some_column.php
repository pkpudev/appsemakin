<?php

use yii\db\Migration;

class m220323_150604_change_some_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->dropForeignKey('activity_mov_fk_1', '{{%management.activity_mov}}');
        $this->dropForeignKey('activity_issue_fk_1', '{{%management.activity_issue}}');

        $this->renameColumn('{{%management.activity_mov}}', 'activity_evaluation_id', 'activity_id');
        $this->renameColumn('{{%management.activity_issue}}', 'activity_evaluation_id', 'activity_id');

        $this->addForeignKey('activity_mov_fk_1', '{{%management.activity_mov}}', 'activity_id', '{{%management.activity}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('activity_issue_fk_1', '{{%management.activity_issue}}', 'activity_id', '{{%management.activity}}', 'id', 'CASCADE', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
