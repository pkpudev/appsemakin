<?php

use yii\db\Migration;

class m220329_150606_insert_new_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->update('management.status', ['status' => 'Sent'], ['id' => 11]);
        $this->update('management.status',['status' => 'Approved'], ['id' => 12]);
        $this->update('management.status', ['status' => 'Create', 'scenario' => 'okr_org_evaluation'], ['id' => 13]);
        
        $this->insert('management.status', [
            'status' => 'Draft',
            'scenario' => 'okr_org_evaluation',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'Sent',
            'scenario' => 'okr_org_evaluation',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'Verified',
            'scenario' => 'okr_org_evaluation',
            'is_active' => true
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
