<?php

use yii\db\Migration;

class m220315_150605_insert_new_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->insert('management.status', [
            'status' => 'Baru',
            'scenario' => 'generate_evaluation',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'Proses',
            'scenario' => 'generate_evaluation',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'Selesai',
            'scenario' => 'generate_evaluation',
            'is_active' => true
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
