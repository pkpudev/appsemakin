<?php

use yii\db\Migration;

class m211205_150602_add_some_column_to_target_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%management.target_type}}', [
            'id' => $this->primaryKey(),
            'target_type' => $this->string(150)->comment('Tipe Target')->notNull(),
            'is_active' => $this->boolean()->comment('Aktif?')->defaultValue(true),
        ], $tableOptions);


        $this->addColumn('{{%management.target}}', 'position_structure_id', $this->integer()->comment('Jabatan'));
        $this->addColumn('{{%management.target}}', 'target_type_id', $this->integer()->comment('Tipe Target'));

        $this->addForeignKey('target_fk_2', '{{%management.target}}', 'position_structure_id', '{{%management.position_structure}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('target_fk_3', '{{%management.target}}', 'target_type_id', '{{%management.target_type}}', 'id', 'RESTRICT', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
