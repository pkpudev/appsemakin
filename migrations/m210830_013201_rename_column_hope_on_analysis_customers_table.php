<?php

use yii\db\Migration;

class m210830_013201_rename_column_hope_on_analysis_customers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->renameColumn('{{%management.customers_analysis}}', 'hope', 'expectation');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
