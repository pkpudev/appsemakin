<?php

use yii\db\Migration;

class m210917_060602_stakeholder extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.stakeholder_category}}', [
            'id' => $this->primaryKey(),
            'stakeholder_category' => $this->string(150)->comment('Kategori'),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.stakeholder_influence}}', [
            'id' => $this->primaryKey(),
            'influence_grade' => $this->string(150)->comment('Tingkat Pengaruh'),
            'influence_point' => $this->integer()->comment('Nilai Pengaruh'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.stakeholder_support}}', [
            'id' => $this->primaryKey(),
            'support_grade' => $this->string(150)->comment('Tingkat Dukungan'),
            'support_point' => $this->integer()->comment('Nilai Dukungan'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.stakeholder_analysis_result}}', [
            'id' => $this->primaryKey(),
            'result_grade' => $this->string(150)->comment('Hasil'),
            'min' => $this->integer()->comment('Point Minimal'),
            'max' => $this->integer()->comment('Point Maksimal'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.stakeholder}}', [
            'id' => $this->primaryKey(),
            'stakeholder' => $this->string(150)->comment('Pemangku Kepentingan')->notNull(),
            'stakeholder_category_id' => $this->integer()->comment('Kategori')->notNull(),
            'role' => $this->text()->comment('Peran')->notNull(),
            'responsibility' => $this->text()->comment('Tanggung Jawab'),
            'relationship_owner' => $this->text()->comment('Pemilik Relasi (Unit Kerja/Jawbatan)')->notNull(),
            'expectation' => $this->text()->comment('Ekspektasi Pemangku Kepentingan Terhadap HI')->notNull(),
            'issue' => $this->text()->comment('Isu yang dihadapi (dalam membangun relasi)')->notNull(),
            'influence_id' => $this->integer()->comment('Pengaruh')->notNull(),
            'influence_point' => $this->integer()->comment('Point Pengaruh'),
            'support_id' => $this->integer()->comment('Dukungan')->notNull(),
            'support_point' => $this->integer()->comment('Point Dukungan'),
            'analysis_result_id' => $this->integer()->comment('Hasil Analisis'),
            'result_point' => $this->integer()->comment('Point Hasil Analisis'),
            'engagement' => $this->text()->comment('Strategi Engagement ')->notNull(),
        ], $tableOptions);
        
        $this->addForeignKey('stakeholder_fk_1', '{{%management.stakeholder}}', 'stakeholder_category_id', '{{%management.stakeholder_category}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('stakeholder_fk_2', '{{%management.stakeholder}}', 'influence_id', '{{%management.stakeholder_influence}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('stakeholder_fk_3', '{{%management.stakeholder}}', 'support_id', '{{%management.stakeholder_support}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('stakeholder_fk_4', '{{%management.stakeholder}}', 'analysis_result_id', '{{%management.stakeholder_analysis_result}}', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('{{%management.stakeholder_contact}}', [
            'id' => $this->primaryKey(),
            'stakeholder_id' => $this->integer()->comment('Pemangku Kepentingan')->notNull(),
            'name' => $this->string(250)->comment('Nama')->notNull(),
            'position' => $this->string(250)->comment('Jabatan'),
            'hp_no' => $this->string(15)->comment('No. HP'),
            'address' => $this->text()->comment('Alamat'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('stakeholder_contact_fk_1', '{{%management.stakeholder_contact}}', 'stakeholder_id', '{{%management.stakeholder}}', 'id', 'CASCADE', 'CASCADE');
        
        $this->createTable('{{%management.stakeholder_history}}', [
            'id' => $this->primaryKey(),
            'stakeholder_id' => $this->integer()->comment('Pemangku Kepentingan')->notNull(),
            'action' => $this->string(50)->notNull()->comment('Tindakan'),
            'comment' => $this->text()->comment('Komentar'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'created_stamp' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'ip' => $this->string(70)->comment('Diubah Pada'),
        ], $tableOptions);
        
        $this->addForeignKey('stakeholder_history_fk_1', '{{%management.stakeholder_history}}', 'stakeholder_id', '{{%management.stakeholder}}', 'id', 'CASCADE', 'CASCADE');
        

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
