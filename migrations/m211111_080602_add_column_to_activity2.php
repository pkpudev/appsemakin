<?php

use yii\db\Migration;

class m211111_080602_add_column_to_activity2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%management.activity}}', 'weight', $this->float()->comment('Bobot'));
        $this->addColumn('{{%management.activity}}', 'resources', $this->text()->comment('Sumber Daya'));
        $this->addColumn('{{%management.activity}}', 'note', $this->text()->comment('Catatan'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
