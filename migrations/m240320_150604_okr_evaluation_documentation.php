<?php

use yii\db\Migration;

class m240320_150604_okr_evaluation_documentation extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        
        $this->createTable('{{%management.okr_evaluation_documentations}}', [
            'id' => $this->primaryKey(),
            'okr_evaluation_id' => $this->integer()->comment('Evaluasi OKR'),
            'mov_name' => $this->string(250)->comment('Nama Dokumen'),
            'files_id' => $this->integer()->comment('File'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->addForeignKey('okr_evaluation_documentations_fk_1', '{{%management.okr_evaluation_documentations}}', 'okr_evaluation_id', '{{%management.okr_evaluation}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
