<?php

use yii\db\Migration;

class m221120_123610_create_financing_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.financing_type_ceiling}}', [
            'id' => $this->primaryKey(),
            'unit_structure_id' => $this->integer()->comment('Struktur Unit'),
            'financing_type_id' => $this->integer()->comment('Jenis Pembiayaan'),
            'ceiling_amount' => $this->double()->comment('Jumlah Pagu'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
