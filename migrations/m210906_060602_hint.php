<?php

use yii\db\Migration;

class m210906_060602_hint extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.attributes}}', [
            'id' => $this->primaryKey(),
            'table' => $this->string(50)->comment('Tabel'),
            'column' => $this->string(100)->comment('Kolom'),
            'feature' => $this->string(100)->comment('Fitur'),
            'label' => $this->string(150)->comment('Label'),
        ], $tableOptions);

        $this->createTable('{{%management.hint}}', [
            'id' => $this->primaryKey(),
            'attributes_id' => $this->integer()->comment('Attribute'),
            'hint' => $this->text()->comment('Hint'),
        ], $tableOptions);

        $this->addForeignKey('hint_fk_1', '{{%management.hint}}', 'attributes_id', '{{%management.attributes}}', 'id', 'CASCADE', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
