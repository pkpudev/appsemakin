<?php

use yii\db\Migration;

class m211014_060702_okr_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%management.okr_history}}', [
            'id' => $this->primaryKey(),
            'okr_id' => $this->integer()->comment('OKR')->notNull(),
            'action' => $this->string(50)->notNull()->comment('Tindakan'),
            'comment' => $this->text()->comment('Komentar'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'created_stamp' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'ip' => $this->string(70)->comment('Diubah Pada'),
        ], $tableOptions);
        
        $this->addForeignKey('okr_history_fk_1', '{{%management.okr_history}}', 'okr_id', '{{%management.okr}}', 'id', 'CASCADE', 'CASCADE');
        
        $this->createTable('{{%management.status}}', [
            'id' => $this->primaryKey(),
            'status' => $this->string(100)->comment('Status')->notNull(),
            'scenario' => $this->string(100)->comment('Scenario')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);

        $this->addColumn('{{%management.okr}}', 'status_id', $this->integer()->comment('Status'));
        $this->addForeignKey('okr_fk_3', '{{%management.okr}}', 'status_id', '{{%management.status}}', 'id', 'RESTRICT', 'CASCADE');
        
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
