<?php

use yii\db\Migration;

class m210828_013201_vision_mision extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.management}}', [
            'id' => $this->primaryKey(),
            'management_name' => $this->string(250)->comment('Manajemen'),
            'period_start' => $this->integer()->comment('Dari Periode'),
            'period_end' => $this->integer()->comment('Sampai Periode'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.vision}}', [
            'id' => $this->primaryKey(),
            'management_id' => $this->integer()->comment('Manajemen'),
            'vision' => $this->text()->comment('Visi'),
            'description' => $this->text()->comment('Deskripsi'),
            'indicator' => $this->text()->comment('Indikator'),
            'explanation' => $this->text()->comment('Penjelasan'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('vision_fk_1', '{{%management.vision}}', 'management_id', '{{%management.management}}', 'id', 'CASCADE', 'CASCADE');
        

        $this->createTable('{{%management.mission}}', [
            'id' => $this->primaryKey(),
            'vision_id' => $this->integer()->comment('Visi'),
            'mission' => $this->text()->comment('Misi'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('mission_fk_1', '{{%management.mission}}', 'vision_id', '{{%management.vision}}', 'id', 'CASCADE', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
