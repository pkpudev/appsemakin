<?php

use yii\db\Migration;

class m211105_060602_create_measure_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%management.measure}}', [
            'id' => $this->primaryKey(),
            'measure' => $this->string(20)->comment('Satuan')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
