<?php

use yii\db\Migration;

class m220602_123608_alter_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        
        $this->alterColumn('{{%management.activities}}', 'evaluation', $this->string(150)->comment('Penilaian'));
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
