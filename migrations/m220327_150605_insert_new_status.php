<?php

use yii\db\Migration;

class m220327_150605_insert_new_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->insert('management.status', [
            'status' => 'Draft',
            'scenario' => 'okr_evaluation',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'New',
            'scenario' => 'okr_evaluation',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'Sent',
            'scenario' => 'okr_evaluation',
            'is_active' => true
        ]);
        
        $this->insert('management.status', [
            'status' => 'Verified',
            'scenario' => 'okr_evaluation',
            'is_active' => true
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
