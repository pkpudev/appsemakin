<?php

use yii\db\Migration;

class m220405_150616_add_some_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->addColumn('{{%management.initiatives}}', 'unit_weight', $this->double()->comment('Bobot Unit'));
        $this->addColumn('{{%management.okr}}', 'unit_weight', $this->double()->comment('Bobot Unit'));
        
        $this->addColumn('{{%management.okr_evaluation}}', 'output_note', $this->text()->comment('Catatan'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
