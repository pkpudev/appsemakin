<?php

use yii\db\Migration;

class m220405_173809_add_column_activity extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->addColumn('{{%management.activity}}', 'employee_weight', $this->float());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
