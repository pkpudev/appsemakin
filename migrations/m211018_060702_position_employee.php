<?php

use yii\db\Migration;

class m211018_060702_position_employee extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%management.position_structure__employee}}', [
            'id' => $this->primaryKey(),
            'position_structure_id' => $this->integer()->comment('Struktur Jabatan')->notNull(),
            'employee_id' => $this->integer()->comment('Karyawan')->notNull(),
            'comment' => $this->text()->comment('Komentar'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'created_stamp' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'ip' => $this->string(70)->comment('Diubah Pada'),
        ], $tableOptions);
        
        $this->addForeignKey('position_structure__employee_fk_1', '{{%management.position_structure__employee}}', 'position_structure_id', '{{%management.position_structure}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('position_structure__employee_fk_2', '{{%management.position_structure__employee}}', 'employee_id', '{{%public.sdm_employee}}', 'id', 'CASCADE', 'CASCADE');
        
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
