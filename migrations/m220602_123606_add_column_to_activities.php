<?php

use yii\db\Migration;

class m220602_123606_add_column_to_activities extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->addColumn('{{%management.activities}}', 'supported_unit_id', $this->integer()->comment('Unit Yang Didukung'));
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
