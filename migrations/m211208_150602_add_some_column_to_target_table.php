<?php

use yii\db\Migration;

class m211208_150602_add_some_column_to_target_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->addColumn('{{%management.target}}', 'measure', $this->string(25)->comment('Satuan'));
        $this->addColumn('{{%management.target}}', 'employee_id', $this->integer()->comment('Karyawan'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
