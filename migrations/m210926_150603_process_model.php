<?php

use yii\db\Migration;

class m210926_150603_process_model extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.business_process}}', [
            'id' => $this->primaryKey(),
            'period_start' => $this->integer()->comment('Tahun Mulai')->notNull(),
            'period_end' => $this->integer()->comment('Tahun Akhir')->notNull(),
            'name' => $this->string(250)->comment('Judul')->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.business_process__files}}', [
            'id' => $this->primaryKey(),
            'business_process_id' => $this->integer()->comment('Bisnis Proses')->notNull(),
            'files_id' => $this->integer()->comment('File'),
        ], $tableOptions);

        $this->addForeignKey('business_process__files_fk_1', '{{%management.business_process__files}}', 'business_process_id', '{{%management.business_process}}', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('{{%management.document_type}}', [
            'id' => $this->primaryKey(),
            'document_type' => $this->string(250)->comment('Tipe Dokumen')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.quality_standard_documents}}', [
            'id' => $this->primaryKey(),
            'period_start' => $this->integer()->comment('Tahun Mulai')->notNull(),
            'period_end' => $this->integer()->comment('Tahun Akhir')->notNull(),
            'level' => $this->integer()->comment('Level')->notNull(),
            'document_type_id' => $this->integer()->comment('Tipe Dokumen'),
            'document_no' => $this->string(50)->comment('No. Dokumen'),
            'document_date' => $this->date()->comment('Tanggal'),
            'title' => $this->string(350)->comment('Judul'),
            'version' => $this->string(20)->comment('Versi'),
            'files_id' => $this->integer()->comment('File'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('quality_standard_documents_fk_1', '{{%management.quality_standard_documents}}', 'document_type_id', '{{%management.document_type}}', 'id', 'RESTRICT', 'CASCADE');
        

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
