<?php

use yii\db\Migration;

class m221107_123610_create_risk_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.risk_category}}', [
            'id' => $this->primaryKey(),
            'risk_category' => $this->string(150)->comment('Kategori Risiko'),
            'description' => $this->text()->comment('Deskripsi'),
            'is_active' => $this->boolean()->comment('Aktif?'),
        ], $tableOptions);

        $this->createTable('{{%management.risk}}', [
            'id' => $this->primaryKey(),
            'initiatives_id' => $this->integer()->comment('Program Kerja ID'),
            'requirements' => $this->text()->comment('Persyaratan'),
            'potential_failure' => $this->text()->comment('Potensi Kegagalan'),
            'potential_effect_of_failure' => $this->text()->comment('Potensi Efek Kegagalan'),
            'severity' => $this->float()->comment('Tingkat Keparahan'),
            'potential_causes_of_failure' => $this->text()->comment('Potensi Sebab Kegagalan'),
            'current_process_controls_prevention' => $this->text()->comment('Pencegahan Kontrol Proses Sekarang'),
            'occurance' => $this->float()->comment('Kejadian'),
            'current_process_controls_detection' => $this->text()->comment('Deteksi Kontrol Proses Saat Ini'),
            'detection' => $this->float()->comment('Deteksi'),
            'rpn' => $this->float()->comment('RPN'),
            'risk_criteria' => $this->text()->comment('Kriteria Risiko'),
            'risk_category_id' => $this->integer()->comment('Kategori Risiko'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->addForeignKey('risk_fk_1', '{{%management.risk}}', 'risk_category_id', '{{%management.risk_category}}', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('{{%management.risk_mitigation}}', [
            'id' => $this->primaryKey(),
            'risk_id' => $this->integer()->comment('Risiko'),
            'recomended_action' => $this->text()->comment('Rencana Tindak Lanjut'),
            'due_date' => $this->date()->comment('Tenggat Waktu'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->addForeignKey('risk_mitigation_fk_1', '{{%management.risk_mitigation}}', 'risk_id', '{{%management.risk}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
