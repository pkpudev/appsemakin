<?php

use yii\db\Migration;

class m220210_150603_create_perspective extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.perspective}}', [
            'id' => $this->primaryKey(),
            'perspective' => $this->string(100)->comment('Perspektif')->notNull(),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
