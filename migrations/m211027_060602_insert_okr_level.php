<?php

use yii\db\Migration;

class m211027_060602_insert_okr_level extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->insert('management.okr_level', [
            'id' => 1,
            'okr_level' => 'Organisasi',
            'is_active' => true
        ]);
        $this->insert('management.okr_level', [
            'id' => 2,
            'okr_level' => 'Direktorat',
            'is_active' => true
        ]);
        $this->insert('management.okr_level', [
            'id' => 3,
            'okr_level' => 'Departemen',
            'is_active' => true
        ]);
        $this->insert('management.okr_level', [
            'id' => 4,
            'okr_level' => 'Divisi',
            'is_active' => true
        ]);
        $this->insert('management.okr_level', [
            'id' => 5,
            'okr_level' => 'Individu',
            'is_active' => true
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
