<?php

use yii\db\Migration;

class m221217_123611_add_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%management.portfolio}}', 'period_start', $this->integer()->comment('Periode Mulai'));
        $this->addColumn('{{%management.portfolio}}', 'period_end', $this->integer()->comment('Periode Akhir'));
        $this->addColumn('{{%management.portfolio}}', 'is_active', $this->boolean()->comment('Aktif?'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
