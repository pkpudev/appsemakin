<?php

use yii\db\Migration;

class m221014_123609_add_column_to_initiatives extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->addColumn('{{%management.okr}}', 'reference_no', $this->string(50)->comment('No. Referensi'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
