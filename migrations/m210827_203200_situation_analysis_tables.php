<?php

use yii\db\Migration;

class m210827_203200_situation_analysis_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.internal_variable}}', [
            'id' => $this->primaryKey(),
            'variable' => $this->string(50)->comment('Variabel'),
            'is_active' => $this->boolean()->comment('Aktif?')->defaultValue(true),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.external_variable}}', [
            'id' => $this->primaryKey(),
            'variable' => $this->string(50)->comment('Variabel'),
            'is_active' => $this->boolean()->comment('Aktif?')->defaultValue(true),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.situation_analysis}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(350)->comment('Judul'),
            'description' => $this->text()->comment('Deskripsi'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->createTable('{{%management.strengths}}', [
            'id' => $this->primaryKey(),
            'strength' => $this->text()->comment('Kekuatan'),
            'situation_analysis_id' => $this->integer()->comment('Situasi'),
            'internal_variable_id' => $this->integer()->comment('Internal Variabel'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('strengths_fk_1', '{{%management.strengths}}', 'situation_analysis_id', '{{%management.situation_analysis}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('strengths_fk_2', '{{%management.strengths}}', 'internal_variable_id', '{{%management.internal_variable}}', 'id', 'RESTRICT', 'CASCADE');
        
        $this->createTable('{{%management.opportunities}}', [
            'id' => $this->primaryKey(),
            'opportunity' => $this->text()->comment('Peluang'),
            'situation_analysis_id' => $this->integer()->comment('Situasi'),
            'external_variable_id' => $this->integer()->comment('Eksternal Variabel'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('opportunities_fk_1', '{{%management.opportunities}}', 'situation_analysis_id', '{{%management.situation_analysis}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('opportunities_fk_2', '{{%management.opportunities}}', 'external_variable_id', '{{%management.external_variable}}', 'id', 'RESTRICT', 'CASCADE');
        
        $this->createTable('{{%management.weaknesses}}', [
            'id' => $this->primaryKey(),
            'weakness' => $this->text()->comment('Kelemahan'),
            'situation_analysis_id' => $this->integer()->comment('Situasi'),
            'internal_variable_id' => $this->integer()->comment('Internal Variabel'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('weaknesses_fk_1', '{{%management.weaknesses}}', 'situation_analysis_id', '{{%management.situation_analysis}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('weaknesses_fk_2', '{{%management.weaknesses}}', 'internal_variable_id', '{{%management.internal_variable}}', 'id', 'RESTRICT', 'CASCADE');
        
        $this->createTable('{{%management.threats}}', [
            'id' => $this->primaryKey(),
            'threat' => $this->text()->comment('Ancaman'),
            'situation_analysis_id' => $this->integer()->comment('Situasi'),
            'external_variable_id' => $this->integer()->comment('Eksternal Variabel'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('threats_fk_1', '{{%management.threats}}', 'situation_analysis_id', '{{%management.situation_analysis}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('threats_fk_2', '{{%management.threats}}', 'external_variable_id', '{{%management.external_variable}}', 'id', 'RESTRICT', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_balance');
    }
}
