<?php

use yii\db\Migration;

class m220330_150605_add_some_column_to_activity extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->addColumn('{{%management.activity}}', 'status_evaluation_id', $this->integer()->comment('Status Evaluasi'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
