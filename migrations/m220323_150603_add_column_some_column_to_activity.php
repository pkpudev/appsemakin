<?php

use yii\db\Migration;

class m220323_150603_add_column_some_column_to_activity extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->addColumn('{{%management.activity}}', 'actual_value', $this->float()->comment('Nilai Realisasi Kegiatan (%)'));
        $this->addColumn('{{%management.activity}}', 'actual_note', $this->text()->comment('Catatan Pelaksanaan'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
