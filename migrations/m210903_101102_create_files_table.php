<?php

use yii\db\Migration;

class m210903_101102_create_files_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.files}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->comment('Nama'),
            'ext' => $this->string(5)->comment('Ext'),
            'storage' => $this->smallInteger()->defaultValue(0)->comment('Storage'),
            'location' => $this->string(500)->comment('Lokasi'),
            'size' => $this->string(10)->comment('Ukuran'),
            'metadata' => $this->string(500)->comment('Metadata'),
            'created_stamp' => $this->timestamp()->defaultExpression('now()')->comment('Waktu'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'created_ip' => $this->string(70)->comment('IP'),
            'guid' => "uuid NOT NULL DEFAULT uuid_generate_v4()",
        ], $tableOptions);

        $this->createTable('{{%management.situation_analysis__files}}', [
            'id' => $this->primaryKey(),
            'situation_analysis_id' => $this->integer()->comment('Analisis Situasi'),
            'files_id' => $this->integer()->comment('Lampiran'),
        ], $tableOptions);

        $this->addForeignKey('situation_analysis__files_fk_1', '{{%management.situation_analysis__files}}', 'situation_analysis_id', '{{%management.situation_analysis}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('situation_analysis__files_fk_2', '{{%management.situation_analysis__files}}', 'files_id', '{{%management.files}}', 'id', 'CASCADE', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
