<?php

use yii\db\Migration;

class m230406_123611_add_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%management.okr}}', 'parent_weight', $this->integer()->comment('Parent Weight'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
