<?php

use yii\db\Migration;

class m220325_150607_add_column_to_evaluations extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->addColumn('{{%management.initiatives_evaluation}}', 'pic_id', $this->integer()->comment('PIC'));
        $this->addColumn('{{%management.initiatives_evaluation}}', 'weight', $this->float()->comment('Bobot'));
        $this->addColumn('{{%management.initiatives_evaluation}}', 'status_id', $this->integer()->comment('Status'));

        $this->addColumn('{{%management.okr_evaluation}}', 'unit_structure_id', $this->integer()->comment('Unit'));
        $this->addColumn('{{%management.okr_evaluation}}', 'pic_id', $this->integer()->comment('PIC'));
        $this->addColumn('{{%management.okr_evaluation}}', 'role', $this->string(50)->comment('Peran'));
        $this->addColumn('{{%management.okr_evaluation}}', 'role_value', $this->integer()->comment('Nilai Peran'));
        $this->addColumn('{{%management.okr_evaluation}}', 'function', $this->string(50)->comment('Fungsi'));
        $this->addColumn('{{%management.okr_evaluation}}', 'function_value', $this->integer()->comment('Nilai Fungsi'));
        $this->addColumn('{{%management.okr_evaluation}}', 'weight', $this->float()->comment('Bobot'));
        $this->addColumn('{{%management.okr_evaluation}}', 'output', $this->text()->comment('Keluaran'));
        $this->addColumn('{{%management.okr_evaluation}}', 'note', $this->text()->comment('Catatan'));
        $this->addColumn('{{%management.okr_evaluation}}', 'status_id', $this->integer()->comment('Status'));
        $this->addColumn('{{%management.okr_evaluation}}', 'performance_analysis', $this->text()->comment('Analisis Capaian'));
        $this->addColumn('{{%management.okr_evaluation}}', 'interpreatation', $this->text()->comment('Interpretasi'));

        $this->addColumn('{{%management.perspective}}', 'start_year', $this->integer()->comment('Tahun Mulai'));
        $this->addColumn('{{%management.perspective}}', 'end_year', $this->integer()->comment('Tahun Berakhir'));
        $this->addColumn('{{%management.perspective}}', 'weight', $this->float()->comment('Bobot'));
        $this->addColumn('{{%management.perspective}}', 'real', $this->double()->comment('Bobot'));

        $this->addColumn('{{%management.okr}}', 'real', $this->double()->comment('Bobot'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
