<?php

use yii\db\Migration;

class m220323_150606_add_column_activity_issue extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        
        $this->dropColumn('{{%management.activity_mov}}', 'status');
        $this->addColumn('{{%management.activity_issue}}', 'status', $this->string()->comment('Status'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
