<?php

use yii\db\Migration;

class m211004_050603_org_structure extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.position_structure}}', [
            'id' => $this->primaryKey(),
            'year' => $this->integer()->comment('Tahun')->notNull(),
            'position_id' => $this->integer()->comment('Jabatan')->notNull(),
            'position_name' => $this->string(250)->comment('Nama Jabatan')->notNull(),
            'position_code' => $this->string(25)->comment('Kode Jabatan'),
            'level' => $this->integer()->comment('Level'),
            'role' => $this->text()->comment('Peran'),
            'responsibility' => $this->text()->comment('Tanggung Jawab'),
            'authority' => $this->text()->comment('Wewenang'),
            'responsible_to_id' => $this->integer()->comment('Bertanggung Jawab Kepada'),
            'unit_structure_id' => $this->integer()->comment('Struktur Unit'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('position_structure_fk_1', '{{%management.position_structure}}', 'position_id', '{{%public.sdm_position}}', 'id', 'RESTRICT', 'CASCADE');
        
        $this->createTable('{{%management.unit_structure}}', [
            'id' => $this->primaryKey(),
            'year' => $this->integer()->comment('Tahun')->notNull(),
            'unit_id' => $this->integer()->comment('Unit')->notNull(),
            'unit_name' => $this->string(250)->comment('Nama Unit')->notNull(),
            'level' => $this->string(250)->comment('Level')->notNull(),
            'superior_unit_id' => $this->integer()->comment(''),
            'unit_code' => $this->string(25)->comment('Kode Unit')->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);
        
        $this->addForeignKey('unit_structure_fk_1', '{{%management.unit_structure}}', 'unit_id', '{{%attendance.departments}}', 'deptid', 'RESTRICT', 'CASCADE');
        
        

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
