<?php

use yii\db\Migration;

class m210905_020602_strategy_analysis extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%management.strategies}}', [
            'id' => $this->primaryKey(),
            'strategy' => $this->text()->comment('Strategi'),
            'situation_analysis_id' => $this->integer()->comment('Analisis Situasi'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->addForeignKey('strategies_fk_1', '{{%management.strategies}}', 'situation_analysis_id', '{{%management.situation_analysis}}', 'id', 'CASCADE', 'CASCADE');
        
        $this->createTable('{{%management.strategy_analysis}}', [
            'id' => $this->primaryKey(),
            'strategy_type' => $this->string(2)->comment('Tipe Strategi'),
            'internal_factor_id' => $this->integer()->comment('Faktor Internal'),
            'external_factor_id' => $this->integer()->comment('Faktor Eksternal'),
            'strategy_id' => $this->integer()->comment('Strategi'),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->addForeignKey('strategy_analysis_fk_1', '{{%management.strategy_analysis}}', 'strategy_id', '{{%management.strategies}}', 'id', 'CASCADE', 'CASCADE');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
