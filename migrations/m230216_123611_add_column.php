<?php

use yii\db\Migration;

class m230216_123611_add_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%management.risk_mitigation}}', 'pic_id', $this->integer()->comment('PIC'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
