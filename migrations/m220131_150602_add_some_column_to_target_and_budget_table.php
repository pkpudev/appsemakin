<?php

use yii\db\Migration;

class m220131_150602_add_some_column_to_target_and_budget_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->addColumn('{{%management.budget}}', 'real_month_01', $this->double()->defaultValue(0)->comment('Januari'));
        $this->addColumn('{{%management.budget}}', 'real_month_02', $this->double()->defaultValue(0)->comment('Februari'));
        $this->addColumn('{{%management.budget}}', 'real_month_03', $this->double()->defaultValue(0)->comment('Maret'));
        $this->addColumn('{{%management.budget}}', 'real_month_04', $this->double()->defaultValue(0)->comment('April'));
        $this->addColumn('{{%management.budget}}', 'real_month_05', $this->double()->defaultValue(0)->comment('Mei'));
        $this->addColumn('{{%management.budget}}', 'real_month_06', $this->double()->defaultValue(0)->comment('Juni'));
        $this->addColumn('{{%management.budget}}', 'real_month_07', $this->double()->defaultValue(0)->comment('Juli'));
        $this->addColumn('{{%management.budget}}', 'real_month_08', $this->double()->defaultValue(0)->comment('Agustus'));
        $this->addColumn('{{%management.budget}}', 'real_month_09', $this->double()->defaultValue(0)->comment('September'));
        $this->addColumn('{{%management.budget}}', 'real_month_10', $this->double()->defaultValue(0)->comment('Oktober'));
        $this->addColumn('{{%management.budget}}', 'real_month_11', $this->double()->defaultValue(0)->comment('November'));
        $this->addColumn('{{%management.budget}}', 'real_month_12', $this->double()->defaultValue(0)->comment('Desember'));

        $this->addColumn('{{%management.target}}', 'real_month_01', $this->double()->defaultValue(0)->comment('Januari'));
        $this->addColumn('{{%management.target}}', 'real_month_02', $this->double()->defaultValue(0)->comment('Februari'));
        $this->addColumn('{{%management.target}}', 'real_month_03', $this->double()->defaultValue(0)->comment('Maret'));
        $this->addColumn('{{%management.target}}', 'real_month_04', $this->double()->defaultValue(0)->comment('April'));
        $this->addColumn('{{%management.target}}', 'real_month_05', $this->double()->defaultValue(0)->comment('Mei'));
        $this->addColumn('{{%management.target}}', 'real_month_06', $this->double()->defaultValue(0)->comment('Juni'));
        $this->addColumn('{{%management.target}}', 'real_month_07', $this->double()->defaultValue(0)->comment('Juli'));
        $this->addColumn('{{%management.target}}', 'real_month_08', $this->double()->defaultValue(0)->comment('Agustus'));
        $this->addColumn('{{%management.target}}', 'real_month_09', $this->double()->defaultValue(0)->comment('September'));
        $this->addColumn('{{%management.target}}', 'real_month_10', $this->double()->defaultValue(0)->comment('Oktober'));
        $this->addColumn('{{%management.target}}', 'real_month_11', $this->double()->defaultValue(0)->comment('November'));
        $this->addColumn('{{%management.target}}', 'real_month_12', $this->double()->defaultValue(0)->comment('Desember'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
