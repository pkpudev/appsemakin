<?php

use yii\db\Migration;

class m210903_101101_add_link_column_to_situation_analysis extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%management.situation_analysis}}', 'ref_link', $this->text()->comment('Link Referensi'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
