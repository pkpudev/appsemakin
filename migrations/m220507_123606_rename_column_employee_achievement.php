<?php

use yii\db\Migration;

class m220507_123606_rename_column_employee_achievement extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->renameColumn('{{%management.employee_achievement}}', 'year', 'evaluation_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
