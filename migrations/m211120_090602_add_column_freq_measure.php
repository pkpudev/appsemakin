<?php

use yii\db\Migration;

class m211120_090602_add_column_freq_measure extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%management.budget}}', 'freq_measure', $this->string(25)->comment('Satuan Frekuensi'));
        $this->addColumn('{{%management.measure}}', 'used_on', $this->smallInteger()->comment('Digunakan Pada'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
