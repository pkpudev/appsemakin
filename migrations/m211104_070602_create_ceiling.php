<?php

use yii\db\Migration;

class m211104_070602_create_ceiling extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
                
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%management.ceiling}}', [
            'id' => $this->primaryKey(),
            'unit_structure_id' => $this->integer()->comment('Unit'),
            'year' => $this->integer()->comment('Tahun'),
            'ceiling_amount' => $this->double()->comment('Jumlah Plafon'),
            'is_active' => $this->boolean()->comment('Aktif?')->defaultValue(true),
            'created_at' => $this->timestamp()->defaultExpression('now()')->comment('Dibuat Pada'),
            'created_by' => $this->integer()->comment('Dibuat Oleh'),
            'updated_at' => $this->timestamp()->comment('Diubah Pada'),
            'updated_by' => $this->integer()->comment('Diubah Oleh'),
        ], $tableOptions);

        $this->insert('management.controllability', [
            'id' => 1,
            'degree_of_controllability' => 'Tinggi',
            'value' => 0.5,
            'is_active' => true
        ]);

        $this->insert('management.controllability', [
            'id' => 2,
            'degree_of_controllability' => 'Sedang',
            'value' => 0.300000012,
            'is_active' => true
        ]);

        $this->insert('management.controllability', [
            'id' => 3,
            'degree_of_controllability' => 'Rendah',
            'value' => 0.200000003,
            'is_active' => true
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
