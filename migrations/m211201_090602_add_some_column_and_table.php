<?php

use yii\db\Migration;

class m211201_090602_add_some_column_and_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%management.participatory_budgeting}}', 'description', $this->text(25)->comment('Deskripsi'));
        $this->addColumn('{{%management.budget}}', 'financing_type_id', $this->integer()->comment('Jenis Pembiayaan'));

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%management.financing_type}}', [
            'id' => $this->primaryKey(),
            'financing_type' => $this->string(510)->comment('Jenis Pembiayaan')->notNull(),
            'description' => $this->text()->comment('Deskripsi'),
            'is_active' => $this->boolean()->defaultValue(true)->comment('Aktif?'),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
