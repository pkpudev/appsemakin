<?php

use yii\db\Migration;

class m221106_123610_create_vw_okr extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->execute(
            <<<FN
            CREATE OR REPLACE VIEW management.vw_okr AS
                SELECT 
                    o."year",
                    o.id as parent_id,
                    o2.id as child_id,
                    row_number() over (order by o.id) as serial_number       
                FROM 
                    management.okr o
                    left join management.okr o2 on o.id = o2.parent_id                 
            FN
                    );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
