<?php

use yii\db\Migration;

class m221014_123610_add_column_to_budget extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->addColumn('{{%management.budget}}', 'initiatives_id', $this->integer()->comment('Program Kerja'));

        $this->addColumn('{{%management.okr}}', 'final_achieve_value', $this->double()->comment('Hasil Akhir'));

        $this->addForeignKey('budget_fk_1', '{{%management.budget}}', 'initiatives_id', '{{%management.initiatives}}', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('okr_fk_1', '{{%management.okr}}', 'reference_no', '{{%management.initiatives}}', 'initiatives_code', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }
}
