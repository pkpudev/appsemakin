<?php

namespace app\components;

use app\models\Attributes;
use app\models\Files;
use app\models\Hint;
use app\models\SituationAnalysisFiles;
use Yii;

class HistoryHelper { 

    public static function create($model, $master_column_name, $master_id, $action, $comment=null, $by=null){
            $model->$master_column_name = $master_id;
            $model->action = $action;
            if($comment) $model->comment = $comment;
            if($by) $model->created_by = $by;
            $model->save();

        return;
    }

}