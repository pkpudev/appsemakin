<?php

namespace app\components;

use app\models\ActivitiesFiles;
use app\models\ActivityMov;
use app\models\BusinessProcessFiles;
use app\models\Files;
use app\models\InitiativesMov;
use app\models\OkrEvaluationDocumentations;
use app\models\OkrOrgMov;
use app\models\SituationAnalysisFiles;
use Yii;

class FilesHelper { 

    public static function upload($files, $id, $modelName, $model = null, $column = null, $otherColumn = null){

        if ($files) {

            if($modelName == 'situation_analysis'){
                $targetDir = '/uploaded/situation_analysis/';
            } else if($modelName == 'business_process'){
                $targetDir = '/uploaded/business_process/';
            } else if($modelName == 'quality_documents'){
                $targetDir = '/uploaded/quality_documents/';
            } else if($modelName == 'activity_mov' || $modelName == 'activities_mov' || $modelName == 'initiatives_mov'){
                $targetDir = '/uploaded/mov/';
            } else if($modelName == 'individual_okr'){
                $targetDir = '/uploaded/individual_okr/';
            } else if($modelName == 'okr_mov'){
                $targetDir = '/uploaded/okr_mov/';
            } else if($modelName == 'okr_evaluation_mov'){
                $targetDir = '/uploaded/okr_mov/';
            }

            foreach ($files as $f) {
                $webroot = Yii::getAlias('@webroot');
                $filename = $id . ' - ' . str_replace('#', '-',$f->baseName) . '.' . $f->extension;
                if ($f->saveAs("{$webroot}{$targetDir}{$filename}")) {
                    $file = new Files();
                    $file->name = str_replace('#', '-',$f->baseName);
                    $file->ext = $f->extension;
                    $file->storage = 0;
                    $file->location = "{$targetDir}{$filename}";
                    $file->size = (String) $f->size;
                    $file->metadata = $f->type;
                    $file->created_by = Yii::$app->user->identity->id;
                    $file->created_ip = \app\components\UserIP::getRealIpAddr();
                    if($file->save()){
                        if($modelName == 'situation_analysis'){
                            $situationAnalysisFile = new SituationAnalysisFiles();
                            $situationAnalysisFile->situation_analysis_id = $id;
                            $situationAnalysisFile->files_id = $file->id;
                            $situationAnalysisFile->save();
                        } else if($modelName == 'business_process'){
                            $businessProcessFile = new BusinessProcessFiles();
                            $businessProcessFile->business_process_id = $id;
                            $businessProcessFile->files_id = $file->id;
                            $businessProcessFile->save();
                        } else if($modelName == 'quality_documents'){
                            $model->$column = $file->id;
                            $model->save();
                        } else if($modelName == 'activity_mov'){
                            $activityMov = new ActivityMov();
                            $activityMov->activity_id = $id;
                            $activityMov->files_id = $file->id;
                            $activityMov->mov_name = $otherColumn;
                            $activityMov->save();
                        } else if($modelName == 'activities_mov'){
                            $activitiesFiles = new ActivitiesFiles();
                            $activitiesFiles->activities_id = $id;
                            $activitiesFiles->files_id = $file->id;
                            $activitiesFiles->save();
                        } else if($modelName == 'initiatives_mov'){
                            $initiativesMov = new InitiativesMov();
                            $initiativesMov->initiatives_evaluation_id = $id;
                            $initiativesMov->files_id = $file->id;
                            $initiativesMov->mov_name = $otherColumn;
                            $initiativesMov->save();
                        } else if($modelName == 'individual_okr'){
                            return $file->location;
                        } else if($modelName == 'okr_mov'){
                            $initiativesMov = new OkrOrgMov();
                            $initiativesMov->okr_evaluation_id = $id;
                            $initiativesMov->files_id = $file->id;
                            $initiativesMov->okr_mov_type_id = $otherColumn;
                            $initiativesMov->save();
                        } else if($modelName == 'okr_evaluation_mov'){
                            $okrEvaluationDocumetations = new OkrEvaluationDocumentations();
                            $okrEvaluationDocumetations->okr_evaluation_id = $id;
                            $okrEvaluationDocumetations->files_id = $file->id;
                            $okrEvaluationDocumetations->mov_name = $otherColumn;
                            $okrEvaluationDocumetations->save();
                        }
                    } else {
                        var_dump($file->errors);die;
                    }
                }
            }
        }
    }

}