<?php

namespace app\components;

use Yii;

class CountTotalReal {

    public static function fromDisbursementDetail($budget_id)
    {
        $sql =" SELECT 
                    SUM(dd.subtotal_eqv) 
                FROM finance.disbursement_detail dd 
                LEFT JOIN finance.disbursement d ON dd.disbursement_id=d.id 
                LEFT JOIN finance.cash_bank c ON d.cash_bank_id=c.id
                WHERE 
                    dd.budget_id=".$budget_id.' AND 
                    cast(split_part(d.disbursement_no::TEXT,\'-\', 2) as integer) >= 2022 AND 
                    d.status_id not in (17,20,21)'.//17 = DITOLAK , 20 = PROSES LPJ, 21 = SUDAH LPJ
                    " AND d.disbursement_type_id not in (7,14,21,32,33)  AND 
                    (c.type <> 'Petty Cash' or c.type is null) AND 
                    (cast(metadata->>'app' AS character varying) <> 'eproc' or cast(metadata->>'app' AS character varying) is null)";
            //17 = Pengadaan Barang/Jasa Operasional, 
            //18 = Pengadaan Barang/Jasa Program, 
            //7 = Uang Muka Pengadaan Barang/Jasa Operasional, 
            //21 = Uang Muka Pengadaan Program, 
            //32 = Pembentukan Dana Proyek, 
            //33 = Penutupan Dana Proyek, 
            //14 = Pembentukan Petty Cash, 
            //16 = Penutupan Petty Cash
        $count = (double) Yii::$app->db->createCommand($sql)->queryScalar();

        return $count ?: 0;
    }

    public static function fromDisbursementDetailSelisih($budget_id)
    {
        $sql =" SELECT 1 
                FROM finance.disbursement_detail dd 
                LEFT JOIN finance.disbursement d ON dd.disbursement_id = d.id 
            WHERE 
                dd.budget_id = $budget_id AND 
                cast(split_part(d.disbursement_no::TEXT, '-', 2) as integer) >= 2022 AND 
                    d.status_id <> 17 AND 
                    d.disbursement_type_id IN (7,21) AND 
                    d.reff_id IS NOT NULL";
                    
        $isExist = Yii::$app->db->createCommand($sql)->queryScalar();

        if($isExist){
            $selisih = Yii::$app->db->createCommand("SELECT COALESCE(SUM(dd.subtotal_eqv), 0) - COALESCE(d2.total_amount_eqv, 0) AS v_total_lpj_selisih
                                                        FROM finance.disbursement_detail dd 
                                                        LEFT JOIN finance.disbursement d ON dd.disbursement_id = d.id
                                                        LEFT JOIN FINANCE.disbursement d2 ON d.reff_id = d2.id 
                                                    WHERE 
                                                        dd.budget_id = $budget_id AND 
                                                        cast(split_part(d.disbursement_no::TEXT, '-', 2) as integer) >= 2022 AND 
                                                        d.status_id NOT IN (17,20,21) AND 
                                                        d.disbursement_type_id IN (7,21) AND 
                                                        d.reff_id IS NOT NULL
                                                    GROUP BY d2.total_amount_eqv")->queryScalar();
        } else {
            $selisih = 0;
        }

        return $selisih ?: 0;
    }

    public static function fromProcurementDetail($budget_id)
    {
        $sql ='SELECT 
                    SUM(pd.total) 
                FROM eproc.procurement_detail pd 
                LEFT JOIN eproc.procurement p ON pd.procurement_id=p.id 
                WHERE 
                    pd.budget_id='.$budget_id.' AND p.request_status_id not in (7,14) AND p.created_stamp >= \'1/1/2022 00:00:00\'';
        $count = (double) Yii::$app->db->createCommand($sql)->queryScalar();

        return $count ?: 0;
    }

    public static function fromDonation($deptid, $year, $month)
    {

        if($deptid == 585){ // deptid kosong
            $filter = "dd.dept_id IS NULL";
        } else {
            $filter = "dd.dept_id = $deptid";
        }
        /* $sql =" SELECT 
                    SUM(d.total_amount_eqv) 
                FROM donation.donation d 
                WHERE 
                    d.keu_validated = 1 AND d.status_id = 1 AND
                    date_part('year', d.donation_date) = $year AND
                    date_part('month', d.donation_date) = $month AND
                    $filter"; */

        $sql =" SELECT 
                    SUM(dd.subtotal_eqv) 
                FROM donation.donation_detail dd
                LEFT JOIN donation.donation d ON d.id = dd.donation_id 
                WHERE 
                    d.keu_validated = 1 AND d.status_id = 1 AND
                    date_part('year', d.donation_date) = $year AND
                    date_part('month', d.donation_date) = $month AND
                    $filter";

        $sum = Yii::$app->db->createCommand($sql)->queryScalar();

        return $sum ?: 0;
    }

    public static function fromDisbursement($budgetId, $year, $month)
    {
        // jika $month = 12, hitung juga yg lebih dari $year (LPJ)
        /* if($month == 12){
            $filter = ...
        }else{
            $filter = "((date_part('year', d.transaction_date) = $year AND date_part('month', d.transaction_date) = $month) OR
                    (date_part('year', p.transaction_date) = $year AND date_part('month', p.transaction_date) = $month))"
        } */
        $sql1 =" SELECT 
                    SUM(dd.subtotal_eqv) 
                FROM finance.disbursement_detail dd 
                LEFT JOIN finance.disbursement d ON dd.disbursement_id=d.id 
                LEFT JOIN finance.disbursement_payment p ON dd.disbursement_payment_id=p.id
                WHERE 
                    dd.budget_id = ".$budgetId." AND 
                    d.disbursement_type_id not in (14,32,33)  AND 
                    ((date_part('year', d.transaction_date) = $year AND date_part('month', d.transaction_date) = $month) OR
                    (date_part('year', p.transaction_date) = $year AND date_part('month', p.transaction_date) = $month)) AND
                    d.status_id IN (15, 19, 20, 18)";
        $sum1 = Yii::$app->db->createCommand($sql1)->queryScalar();

        $payrollMonth = $year . str_pad(((int) $month), 2, "0", STR_PAD_LEFT);
        $sql2 =" SELECT 
                    SUM(pd.amount) 
                FROM hrm.payroll_batch_detail pd 
                LEFT JOIN hrm.payroll_batch pb ON pd.payroll_batch_id=pb.id 
                LEFT JOIN hrm.payroll_item pi ON pd.payroll_item_id=pi.id 
                WHERE 
                    pd.budget_id = ".$budgetId." AND 
                    pi.arithmetic_operation = '-' AND
                    pb.payroll_month = ".$payrollMonth;
        $sum2 = Yii::$app->db->createCommand($sql2)->queryScalar();

        $sum = $sum1 + $sum2;//var_dump($sum1);var_dump($sum2);
        return $sum ?: 0;
    }
}