<?php

namespace app\components;

class DateHelper { 

    public static function getBulanIndo($bln, $short = false){
        
        if($short){
            $bulan = array (
                1 =>   'Jan',
                2 =>   'Feb',
                3 =>   'Mar',
                4 =>   'Apr',
                5 =>   'Mei',
                6 =>   'Jun',
                7 =>   'Jul',
                8 =>   'Agu',
                9 =>   'Sep',
                10 =>   'Okt',
                11 =>   'Nov',
                12 =>   'Des'
            );
        } else {
            $bulan = array (
                1 =>   'Januari',
                2 =>   'Februari',
                3 =>   'Maret',
                4 =>   'April',
                5 =>   'Mei',
                6 =>   'Juni',
                7 =>   'Juli',
                8 =>   'Agustus',
                9 =>   'September',
                10 =>   'Oktober',
                11 =>   'November',
                12 =>   'Desember'
            );
        }
        
        return $bulan[ (int)$bln ];
    }

    public static function getTglIndo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            2 =>   'Februari',
            3 =>   'Maret',
            4 =>   'April',
            5 =>   'Mei',
            6 =>   'Juni',
            7 =>   'Juli',
            8 =>   'Agustus',
            9 =>   'September',
            10 =>   'Oktober',
            11 =>   'November',
            12 =>   'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    public static function getBulanTahun($date){
        $tahun = substr($date,0,4);
        $bulan = substr($date,4,2);

        if($bulan=='01')
            return "Januari ".$tahun;
        else if($bulan=='02')
            return "Februari ".$tahun;
        else if($bulan=='03')
            return "Maret ".$tahun;
        else if($bulan=='04')
            return "April ".$tahun;
        else if($bulan=='05')
            return "Mei ".$tahun;
        else if($bulan=='06')
            return "Juni ".$tahun;
        else if($bulan=='07')
            return "Juli ".$tahun;
        else if($bulan=='08')
            return "Agustus ".$tahun;
        else if($bulan=='09')
            return "September ".$tahun;
        else if($bulan=='10')
            return "Oktober ".$tahun;
        else if($bulan=='11')
            return "November ".$tahun;
        else if($bulan=='12')
            return "Desember ".$tahun;
        else
            return "-".$tahun;
    }

    public static function getHourMinutes($duration){
        if($duration){
            $dur = explode('.', $duration);

            $hour = $dur[0] ?: 0;
            
            if($dur[1]){
                $minutes = round(($duration - $dur[0]) * 60);
            } else {
                $minutes = 0;
            }

            return "$hour Jam $minutes Menit";
        } else {
           return "0 Jam 0 Menit";
       }
    }

}