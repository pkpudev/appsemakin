<?php
/**
 * @author 345874331@qq.com
 * @version 1.0.0
 * 
 * modified from https://github.com/jamesBan/yii2-logstash
 */

namespace app\components\log;

use yii\log\Logger;
use yii\helpers\ArrayHelper;

class JsonAccessFileTarget extends JsonFileTarget
{
    /**
     * Generates the context information to be logged.
     *
     * @return array
     */
    protected function getContextMessage()
    {
        $context = $this->context;
        if (($this->logUser === true) && ($user = \Yii::$app->get('user', false)) !== null) {
            /** @var \yii\web\User $user */
            $user = $user->identity;
            $context['userId'] = $user ? $user->id : null;
        }

        if (($request = \Yii::$app->get('request', false)) !== null) {
            /** @var \yii\web\Request $request */
            $context['ip'] = $request ? $request->userIP : null;
        }

        $serverVars = [
            'REQUEST_URI',
            'HTTP_USER_AGENT',
        ];

        foreach ($serverVars as $var) {
            $context[strtolower($var)] = isset($_SERVER[$var]) ? $_SERVER[$var] : '';
        }

        return $context;
    }
    /**
     * Convert's any type of log message to array.
     *
     * @param mixed $text Input log message.
     *
     * @return array
     */
    protected function parseText($text)
    {
        $type = gettype($text);

        switch ($type) {
            case 'array':
                return $text;
                break;
        }

        return [];
    }
    /**
     * Transform log message to assoc.
     *
     * @param array $message The log message.
     *
     * @return array
     */
    protected function prepareMessage($message)
    {
        list($text, $level, $category, $timestamp) = $message;

        $timestamp = date('c', $timestamp);
        $result = ArrayHelper::merge(
            $this->parseText($text),
            ['@timestamp' => $timestamp]
        );

        return $result;
    }
}