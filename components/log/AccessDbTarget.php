<?php // file: AppName/components/log/AccessDbTarget.php

namespace app\components\log;

use Yii;
use yii\log\LogRuntimeException;

class AccessDbTarget extends \yii\log\DbTarget
{
    public $app;

    /** @var array Add more context */
    public $context = [];

    private $_context;

    public $categories = ['yii\web\Session::open'];

    public $levels = ['info'];

    /**
     * Stores log messages to DB.
     * Starting from version 2.0.14, this method throws LogRuntimeException in case the log can not be exported.
     * @throws Exception
     * @throws LogRuntimeException
     */
    public function export()
    {
        $context = $this->getContextMessage();
        
        if (!isset($context['user_id']) || empty($context['user_id'])) {
            return;
        }

        if ($this->db->getTransaction()) {
            // create new database connection, if there is an open transaction
            // to ensure insert statement is not affected by a rollback
            $this->db = clone $this->db;
        }

        $tableName = $this->db->quoteTableName($this->logTable);
        $sql = "INSERT INTO $tableName ([[log_time]], [[app]], [[request_uri]], [[user_id]], [[user_ip]], [[user_agent]])
                VALUES (:log_time, :app, :request_uri, :user_id, :user_ip, :user_agent)";
        $command = $this->db->createCommand($sql);
        foreach ($this->messages as $message) {
            list($text, $level, $category, $timestamp) = $message;
            $timestamp = date('c', $timestamp);
            if ($command->bindValues([
                    ':log_time' => $timestamp,
                    ':app' => $this->app,
                    ':request_uri' => $context['request_uri'],
                    ':user_id' => $context['user_id'],
                    ':user_ip' => $context['user_ip'],
                    ':user_agent' => $context['http_user_agent'],
                ])->execute() > 0) {
                return;
            }
            throw new LogRuntimeException('Unable to export log through database!');
        }
    }

    /**
     * Generates the context information to be logged.
     *
     * @return array
     */
    protected function getContextMessage()
    {
        if ( ! isset($this->_context) ) {

            $context = $this->context;
            if (($user = Yii::$app->get('user', false)) !== null) {
                /** @var \yii\web\User $user */
                $user = $user->identity;
                $context['user_id'] = $user ? $user->id : null;
            }

            if (($request = Yii::$app->get('request', false)) !== null) {
                /** @var \yii\web\Request $request */
                $context['user_ip'] = $request ? $request->userIP : null;
            }

            $serverVars = [
                'REQUEST_URI',
                'HTTP_USER_AGENT',
            ];

            foreach ($serverVars as $var) {
                $context[strtolower($var)] = isset($_SERVER[$var]) ? $_SERVER[$var] : '';
            }

            $this->_context = $context;
        }

        return $this->_context;
    }
}
