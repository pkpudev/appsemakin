<?php

namespace app\components;

use app\models\Attributes;
use app\models\Files;
use app\models\Hint;
use app\models\SituationAnalysisFiles;
use Yii;

class AttributeHintHelper { 

    public static function getHint($model, $attribute){
        $tableName = $model::getTableSchema()->name;

        if($attributes = Attributes::find()->where(['table' => $tableName])->andWhere(['column' => $attribute])->one()){
            if($hint = Hint::findOne(['attributes_id' => $attributes->id])){
                $textHint = ' <span class="fa fa-question-circle" title="' . $hint->hint . '"></span>';
            } else {
                $textHint = '';
            }
            return $attributes->label . $textHint;
        } else {
            return $model->getAttributeLabel($attribute);
        }
    }

}