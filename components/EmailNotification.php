<?php

namespace app\components;

use Yii;

class EmailNotification
{

    //Mail TEST
    public static function test()
    {
        $toEmail = 'ardi.nada4@gmail.com';
        $fromEmail = 'intranet@pkpu.org';
        $fromName = 'Intranet-[Eperform]';
        $subject = 'Testing Send Mail From Eperform';

        $msg = Yii::$app->mailer->compose('test', []);
        $msg->setFrom(array($fromEmail => $fromName));
        $msg->setTo($toEmail);
        // $msg->setCc('intranet@human-initiative.org');
        $msg->setSubject($subject);
        $msg->send();
    }
}
