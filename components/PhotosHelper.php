<?php

namespace app\components;

use app\models\Branch;
use Yii;
use app\models\Donation;
use app\models\Employee;
use yii\httpclient\Client;
use app\models\SdmEmployee;
use app\models\VwEmployeeManager;

//use app\models\NotifLog;

class PhotosHelper { 

    public static function getProfilePhotosUrl($id){
        if (!$id) {
            $id = Yii::$app->user->identity->id;
        }

        if($photoUrl = Employee::findOne($id)->user_image){
            $imgUrl = 'https://hrm.c27g.com/' . $photoUrl;
        } else {
            
            $imgUrl = 'https://intranet.c27g.com/uploads/photo/' . $id . '.jpg';
            $file_headers = @get_headers($imgUrl);

            if ($file_headers[0] == 'HTTP/1.1 200 OK') {
                $imgUrl = 'https://intranet.c27g.com/uploads/photo/' . $id . '.jpg';
            } else {
                $imgUrl = '/images/ico/users.jpg';
            }
        }

        return $imgUrl;
    }

    public static function getProfilePhotosStyle($id){
        if (!$id) {
            $id = Yii::$app->user->identity->id;
        }

        $style = [];
        $imgUrl = 'https://intranet.c27g.com/uploads/photo/' . $id . '.jpg';
        $file_headers = @get_headers($imgUrl);

        $model = Employee::findOne($id);

        if(str_replace(' ', '', $model->user_image) != ''){
            // $style = ['style' => 'height: 115%;'];
        } else {
            
            $imgUrl = 'https://intranet.c27g.com/uploads/photo/' . $id . '.jpg';
            $file_headers = @get_headers($imgUrl);

            if ($file_headers[0] == 'HTTP/1.1 200 OK') {
                $style = ['style' => 'height: 115%;'];
            } else {
                $style = [];
            }
        }

        return $style;
    }

}