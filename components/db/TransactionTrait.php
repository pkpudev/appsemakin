<?php

namespace app\components\db;

trait TransactionTrait {

	public $saveJoin = false;
	public $saveCallback = false;
	public $saveJoinCallback = false;

	protected $runValidation;
	protected $callbackResults;

	public function save($runValidation = true, $attributeNames = null)
    {
    	$this->runValidation = $runValidation;
        return $this->saveTransactions($attributeNames);
    }

    protected function saveTransactions($attributeNames)
    {
        try {
            return static::getDb()->transaction(function($db) use ($attributeNames) {
                if (parent::save($this->runValidation, $attributeNames)) {
                    if ($this->saveCallback || $this->saveJoin) {
                    	return $this->saveInnerTransactions($db);
                    } else {
                    	return true;
                    }
                }
                throw new \yii\base\Exception(static::className(). ": Saving Transactions Failed!");
            });
        } catch (\Exception $e) {
            if ( ! $this->hasErrors() ) {
                $this->addError("*", $e->getMessage());
            }
            return false;
        }
    }

    protected function saveInnerTransactions($db)
    {
        return $db->transaction(function($db) {
            if ( ($this->callbackResults = $this->saveCallback())
            	&& ($joinResults = $this->saveJoin())
            ) {
                return $this->saveJoinCallback($db, $joinResults);
            }
            throw new \yii\base\Exception(static::className(). ": Saving Join Transactions Failed!");
        });
    }

	protected function saveCallback()
    {
    	if ($this->saveCallback instanceof \Closure) {
    		return call_user_func_array($this->saveCallback, [&$this]);
    	}
    	return true;
    }

    public function saveJoin()
    {
    	// replace here!
    	return true;
    }

    protected function saveJoinCallback($db, &$joinResults)
    {
    	if ($this->saveJoinCallback instanceof \Closure) {
    		return $db->transaction(function($db) use (&$joinResults) {
	    		if ( call_user_func_array($this->saveJoinCallback, [&$this, &$joinResults]) ) {
	    			return true;
	    		}
		    	throw new \yii\base\Exception(static::className(). ": Saving After Join (callback) Transactions Failed!");
	        });
    	}
    	return true;
    }
}