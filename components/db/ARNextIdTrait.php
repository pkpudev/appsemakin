<?php

namespace app\components\db;

trait ARNextIdTrait {

	private $_nextId;

	public function getNextId()
	{
		if (!isset($this->_nextId)) {
			$this->_nextId = (new \yii\db\Query())
    			->select("(last_value+1)")
    			->from($this->getTableSchema()->sequenceName)
    			->scalar();
		}
		return $this->_nextId;
	}
}