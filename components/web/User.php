<?php

namespace app\components\web;

class User extends \yii\web\User
{
	public $superuserCheck = false;
    public $superuserPermissionName = 'SuperUser';
    
	// public function can($permissionName, $params = [], $allowCaching = true)
    // {
    //     if ($this->superuserCheck && parent::can($this->superuserPermissionName, [], true))
    //         return true;
    //     return parent::can($permissionName, $params, $allowCaching);
    // }
}