<?php

namespace app\components\behavior;

use mdm\admin\components\Helper;

class User extends \yii\base\Behavior
{
	public function canAccessRoute($route, $params = [])
	{
		return Helper::checkRoute($route, $params, $this->owner);
	}

	public function getIsCreator($model, $attribute='created_by')
	{
		if (!$model->hasAttribute($attribute)) return false;
		return $this->owner->id == $model->$attribute;
	}

	public function getIsLoginUser($userId)
	{
		return $this->owner->id == $userId;
	}

	public function getIsMarketer()
	{
		return $this->owner->identity->is_marketer == 1;
	}

	public function getIsCompany($cid)
	{
		return $this->owner->identity->company_id == $cid;
	}
}
