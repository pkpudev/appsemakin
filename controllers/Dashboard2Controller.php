<?php

namespace app\controllers;

use app\models\Calculation;
use app\models\Evaluation;
use app\models\Okr;
use app\models\OkrAchievement;
use app\models\OkrEvaluation;
use app\models\search\OkrEvaluationSearch;
use app\models\search\OkrOrgSearch;
use app\models\search\OkrSearch;
use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;

class Dashboard2Controller extends Controller
{

    public function actionOkrOrg(){
        
        /* $searchModelOkrOrg = new OkrEvaluationSearch();
        $dataProviderOkrOrg = $searchModelOkrOrg->searchReport(Yii::$app->request->queryParams);
        
        return $this->render('index-organization', 
                                [
                                    'searchModelOkrOrg' => $searchModelOkrOrg,
                                    'dataProviderOkrOrg' => $dataProviderOkrOrg, 
                                ]); */

        
        $searchModel = new OkrEvaluationSearch();
        $dataProvider = $searchModel->searchReport(Yii::$app->request->queryParams, 'KR');

        return $this->render('index-organization', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOkrOrg2($year = null){

        if(!$year) $year = date('Y');
                                        
        $searchModelObjective = new OkrSearch();
        $searchModelObjective->year = $year;
        $dataProviderObjective = $searchModelObjective->search(Yii::$app->request->queryParams, 'O');

        $totalAchieve = 0;
        foreach($dataProviderObjective->getModels() as $objectiveOrganization){

            $childs = Okr::find()->where(['parent_id' => $objectiveOrganization->id])->andWhere(['okr_level_id' => 1])->all();

            $finalValue = 0;
            foreach($childs as $child){
                $achieveValues = [];

                $evaluationK1 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 1']);

                if($evaluationK1){
                    $okrAchievements = OkrEvaluation::find()
                                                        ->where(['evaluation_id' => $evaluationK1->id])
                                                        ->andWhere(['okr_id' => $child->id])
                                                        ->all();

                    $achieve = 0;
                    foreach($okrAchievements as $okrAchievement){
                        $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                    }
                    
                    if($achieve) array_push($achieveValues, $achieve?:0);
                } else {
                    // array_push($achieveValues, 0);
                }

                $evaluationK2 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 2']);

                if($evaluationK2){
                    $okrAchievements = OkrEvaluation::find()
                                                        ->where(['evaluation_id' => $evaluationK2->id])
                                                        ->andWhere(['okr_id' => $child->id])
                                                        ->all();

                    $achieve = 0;
                    foreach($okrAchievements as $okrAchievement){
                        $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                    }
                    
                    if($achieve) array_push($achieveValues, $achieve?:0);
                } else {
                    // array_push($achieveValues, 0);
                }

                $evaluationK3 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 3']);

                if($evaluationK3){
                    $okrAchievements = OkrEvaluation::find()
                                                        ->where(['evaluation_id' => $evaluationK3->id])
                                                        ->andWhere(['okr_id' => $child->id])
                                                        ->all();

                    $achieve = 0;
                    foreach($okrAchievements as $okrAchievement){
                        $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                    }
                    
                    if($achieve) array_push($achieveValues, $achieve?:0);
                } else {
                    // array_push($achieveValues, 0);
                }

                $evaluationK4 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 4']);

                if($evaluationK4){
                    $okrAchievements = OkrEvaluation::find()
                                                        ->where(['evaluation_id' => $evaluationK4->id])
                                                        ->andWhere(['okr_id' => $child->id])
                                                        ->all();

                    $achieve = 0;
                    foreach($okrAchievements as $okrAchievement){
                        $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                    }
                    
                    if($achieve) array_push($achieveValues, $achieve?:0);
                } else {
                    // array_push($achieveValues, 0);
                }

                // if($model->id == 15449) var_dump($achieveValues);
                if($child->calculation->id == Calculation::SUM){
                    $lastAchieveValue = array_sum($achieveValues);
                } else if($child->calculation->id == Calculation::AVERAGE){
                    $lastAchieveValue = $achieveValues ? array_sum($achieveValues)/count($achieveValues) : 0;
                } else if($child->calculation->id == Calculation::LAST){
                    $lastAchieveValue = end($achieveValues);
                } else if($child->calculation->id == Calculation::MIN){
                    $lastAchieveValue = min($achieveValues);
                } 

                $finalValue = $finalValue + (((($lastAchieveValue / $child->target) * 100) * $child->org->weight) / 100);
            }


            $totalAchieve = $totalAchieve + (($finalValue * $objectiveOrganization->org->weight) / 100);
            // $totalAchieve = $totalAchieve + (($objectiveOrganization->getFinalAchieveValue($year) * $objectiveOrganization->org->weight) / 100);

            
        }
        
        $searchModel = new OkrEvaluationSearch();
        $searchModel->year = $year;
        $dataProvider = $searchModel->searchReport(Yii::$app->request->queryParams, 'KR', $year);

        return $this->render('index-organization2', [
            'year' => $year,
            'searchModelObjective' => $searchModelObjective,
            'dataProviderObjective' => $dataProviderObjective,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalAchieve' => $totalAchieve,
        ]);
    }

    /** PERFORMANCE ANALYSIS */
    public function actionPerformanceAnalysis($id, $evaluation_id)
    {
        $request = Yii::$app->request;
        $model = OkrAchievement::findOne(['okr_id' => $id, 'evaluation_id' => $evaluation_id]);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Analisis Capaian OKR",
                    'content'=>$this->renderAjax('_form_performance_analysis', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $model->save();
                $msg = '<span class="text-success">Berhasil mengubah Analisis Capaian OKR</span>';

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Analisis Capaian OKR",
                    'content'=>$msg,
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Analisis Capaian OKR",
                    'content'=>$this->renderAjax('_form_performance_analysis', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** INTERPRETATION */
    public function actionInterpretation($id, $evaluation_id)
    {
        $request = Yii::$app->request;
        $model = OkrAchievement::findOne(['okr_id' => $id, 'evaluation_id' => $evaluation_id]);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Interpretasi OKR",
                    'content'=>$this->renderAjax('_form_interpretation', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $model->save();
                $msg = '<span class="text-success">Berhasil mengubah Interpretasi OKR</span>';

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Interpretasi OKR",
                    'content'=>$msg,
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Interpretasi OKR",
                    'content'=>$this->renderAjax('_form_interpretation', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }


}
