<?php

namespace app\controllers;

use app\models\Activities;
use app\models\ActivityCategory;
use app\models\EmployeeAchievement;
use app\models\Evaluation;
use app\models\PositionStructureEmployee;
use app\models\Status;
use Yii;
use yii\web\Controller;
use yii\web\Response;

/**
 * Default controller for the `api` module
 */
class ApiController extends Controller
{
    public $enableCsrfValidation = false;
    protected $token = "431f37e815752a7b2053df57cdc1a8fd";

    public function actionGetPosition($year, $employee_id, $unit_id = null){
        $request = Yii::$app->request;
        $response = [];

        if($unit_id){
            $unitFilter = " AND us.id = $unit_id";
        }

        $query = Yii::$app->db->createCommand("SELECT 
                                                us.id,
                                                us.unit_name,
                                                pse2.employee_id AS upper_id
                                            FROM 
                                                management.position_structure__employee pse
                                            INNER JOIN management.position_structure ps ON pse.position_structure_id = ps.id
                                            INNER JOIN management.unit_structure us ON ps.unit_structure_id = us.id
                                            LEFT JOIN management.position_structure__employee pse2 ON ps.responsible_to_id = pse2.position_structure_id
                                            WHERE 
                                                ps.year = $year AND
                                                pse.employee_id = $employee_id
                                                $unitFilter
                                            ORDER BY 
                                            us.unit_name ASC")->queryAll();

        

        foreach($query as $q){
            $q = [
                'id' => $q['id'],
                'unit_name' => $q['unit_name'],
                'upper_id' => $q['upper_id'],
            ];
            array_push($response, $q);
        }

        $response = [
            "result" => $response
        ];

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $response;
    }

    public function actionGetAchieves($token, $year, $period, $employeeId = null){

        $response = [];

        if($token == $this->token){
            $evaluationId = Evaluation::findOne(['year' => $year, 'period' => $period])->id;
            if($evaluationId){

                $positions = PositionStructureEmployee::find()
                                                        ->alias('pse')
                                                        ->joinWith('position ps')
                                                        ->where(['employee_id' => $employeeId])
                                                        ->andWhere(['is_active' => true])
                                                        ->andWhere(['year' => $year])
                                                        ->all();

                                                        $val = 0;
                $cnt = 0;
                $achieveVal = 0;
                foreach($positions as $p){
                    $val = 0;
                    if(in_array($p->position->level, [1, 2])/*  || !$p->position->unit_structure_id == 656 */){
                        $val = Yii::$app->db->createCommand("
                                                                SELECT 
                                                                    achieve_value
                                                                FROM 
                                                                    management.employee_achievement ea
                                                                WHERE
                                                                    evaluation_id = $evaluationId AND 
                                                                    achieve_type = 'OKR' AND 
                                                                    employee_id = $employeeId AND
                                                                    position_structure_id = {$p->position_structure_id}
                                                            ")->queryScalar();
                    } else {
                        $val = Yii::$app->db->createCommand("
                                                                SELECT 
                                                                    achieve_value
                                                                FROM 
                                                                    management.unit_achievement u
                                                                WHERE
                                                                    evaluation_id = $evaluationId AND 
                                                                    achieve_type = 'OKR' AND 
                                                                    unit_id = {$p->position_structure_id}
                                                            ")->queryScalar();
                                                            
                    }

                    // var_dump($val);
                    
                    $achieveVal = $achieveVal + $val;

                    $cnt++;
                }

                // die;

                $achieveVal = $achieveVal / $cnt;

                $item = [
                    'employee_id' => $employeeId,
                    'achieve_value' => round($achieveVal ?: 0, 2)
                ];

                array_push($response, $item);


                /* if($employeeId){
                    $filterEmployee = " AND ea.employee_id = $employeeId";
                }

                
        
                $query = Yii::$app->db->createCommand("SELECT 
                                                        ea.employee_id,
                                                        AVG(ea.achieve_value) AS achieve_value
                                                    FROM 
                                                        management.employee_achievement ea
                                                    WHERE
                                                        ea.evaluation_id = $evaluationId AND 
                                                        achieve_type = 'OKR'
                                                        $filterEmployee
                                                    GROUP BY ea.employee_id")->queryAll();
        
                foreach($query as $q){
                    $item = [
                        'employee_id' => $q['employee_id'],
                        'achieve_value' => round($q['achieve_value']?:0, 2),
                        'query' => "SELECT 
                        ea.employee_id,
                        AVG(ea.achieve_value) AS achieve_value
                    FROM 
                        management.employee_achievement ea
                    WHERE
                        ea.evaluation_id = $evaluationId AND 
                        achieve_type = 'OKR'
                        $filterEmployee
                    GROUP BY ea.employee_id"
                    ];
        
                    array_push($response, $item);
                } */
            } else {   
                $response = [
                    "error" => 'Evaluasi Tidak ditemukan!'
                ];
            }
    
            $response = [
                "result" => $response
            ];
        } else {    
            $response = [
                "error" => 'Wrong Token!'
            ];
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $response;
    }

    public function actionGetAchievesEmployee($token, $year, $period, $employeeId){

        $response = [];

        if($token == $this->token){
            $evaluationId = Evaluation::findOne(['year' => $year, 'period' => $period])->id;
            if($evaluationId){
                if($employeeId){
                    $filterEmployee = " AND ea.employee_id = $employeeId";
                }

                $positionStructureEmployees = PositionStructureEmployee::find()
                                                                            ->alias('pse')
                                                                            ->joinWith('position p')
                                                                            ->where(['employee_id' => $employeeId])
                                                                            ->andWhere(['p.year' => $year])
                                                                            ->all();
                                                                                                             
                foreach($positionStructureEmployees as $pse){
                    $item = [
                        'unit' => $pse->position->unit->unit_name,
                        'position' => $pse->position->position_name,
                        'achieve_value' => round(EmployeeAchievement::findOne(['employee_id' => $employeeId, 'evaluation_id' => $evaluationId, 'position_structure_id' => $pse->position_structure_id, 'achieve_type' => 'OKR'])->achieve_value ?: 0, 2)
                    ];
        
                    array_push($response, $item);
                }
                
        
                /* $query = Yii::$app->db->createCommand("SELECT 
                                                        us.unit_name,
                                                        ps.position_name,
                                                        ea.achieve_value
                                                    FROM 
                                                        management.employee_achievement ea
                                                    LEFT JOIN management.position_structure ps ON ea.position_structure_id = ps.id
                                                    LEFT JOIN management.unit_structure us ON ps.unit_structure_id = us.id
                                                    WHERE
                                                        ea.evaluation_id = $evaluationId AND 
                                                        achieve_type = 'OKR'
                                                        $filterEmployee
                                                    ")->queryAll();
        
                foreach($query as $q){
                    $item = [
                        'unit' => $q['unit_name'],
                        'position' => $q['position_name'],
                        'achieve_value' => round($q['achieve_value']?:0, 2)
                    ];
        
                    array_push($response, $item);
                } */
            } else {   
                $response = [
                    "error" => 'Evaluasi Tidak ditemukan!'
                ];
            }
    
            $response = [
                "result" => $response
            ];
        } else {    
            $response = [
                "error" => 'Wrong Token!'
            ];
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $response;
    }

    public function actionGetOtherValues($token, $year, $period, $employeeId, $cat){

        $response = [];

        if($token == $this->token){
            if($period == 'Kuartal 1'){
                $dateStart = $year . '-' . '01-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '03-01'));
            } else if($period == 'Kuartal 2'){
                $dateStart = $year . '-' . '04-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '06-01'));
            } else if($period == 'Kuartal 3'){
                $dateStart = $year . '-' . '07-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '09-01'));
            } else if($period == 'Kuartal 4'){
                $dateStart = $year . '-' . '10-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '12-01'));
            }

            if($cat == ActivityCategory::CROSSFUNCTION){

                $check = Activities::find()
                                        ->alias('a')
                                        ->where(['activity_category_id' => ActivityCategory::CROSSFUNCTION])
                                        ->andWhere("'$dateStart' <= start_date and start_date <= '$dateEnd'")
                                        ->andWhere(['pic_id' => $employeeId])
                                        ->andWhere(['a.status_id' => Status::ACT_DONE])
                                        ->count();

                $response = [
                                'total' => $check
                            ];

            } else if($cat == ActivityCategory::VOLUNTARY){

                $check = Activities::find()
                                        ->alias('a')
                                        ->where(['activity_category_id' => ActivityCategory::VOLUNTARY])
                                        ->andWhere("'$dateStart' <= start_date and start_date <= '$dateEnd'")
                                        ->andWhere(['pic_id' => $employeeId])
                                        ->andWhere(['a.status_id' => Status::ACT_DONE])
                                        ->count();

                $response = [
                                'total' => $check
                            ];
                
            } else if($cat == ActivityCategory::EXPERT_LOCATOR){

                $check = Activities::find()
                                        ->alias('a')
                                        ->where(['activity_category_id' => ActivityCategory::EXPERT_LOCATOR])
                                        ->andWhere("'$dateStart' <= start_date and start_date <= '$dateEnd'")
                                        ->andWhere(['pic_id' => $employeeId])
                                        ->andWhere(['a.status_id' => Status::ACT_DONE])
                                        ->count();

                $response = [
                                'total' => $check
                            ];
            } else if($cat == ActivityCategory::PKP){

                $check = Activities::find()
                                        ->alias('a')
                                        ->where(['activity_category_id' => ActivityCategory::PKP])
                                        ->andWhere("'$dateStart' <= start_date and start_date <= '$dateEnd'")
                                        ->andWhere(['pic_id' => $employeeId])
                                        ->andWhere(['a.status_id' => Status::ACT_DONE])
                                        ->count();

                $response = [
                                'total' => $check
                            ];
            }

        } else {    
            $response = [
                "error" => 'Wrong Token!'
            ];
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $response;
    }
}