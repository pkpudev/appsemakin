<?php

namespace app\controllers;

use app\components\DateHelper;
use app\models\Activity;
use app\models\Budget;
use app\models\Evaluation;
use app\models\FinancingType;
use app\models\Initiatives;
use app\models\Management;
use app\models\Mission;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\ParticipatoryBudgeting;
use app\models\Portfolio;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\search\EmployeeAchievement;
use app\models\search\InitiativesEvaluationSearch;
use app\models\search\UnitAchievement;
use app\models\search\OkrEvaluationSearch;
use app\models\search\OkrSearch;
use app\models\search\PositionStructureEmployeeSearch;
use app\models\search\PositionStructureSearch;
use app\models\search\UnitStructureSearch;
use app\models\Target;
use app\models\TargetType;
use app\models\UnitStructure;
use app\models\Vision;
use app\models\VwEmployeeManager;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class DashboardController extends Controller
{


    public function actionSummary1($yearFilter = null){
        if(!$yearFilter){
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $yearFilter = /* $lastOkr->year ?:  */date('Y');
        }


        $countObjectivesOrg = Okr::find()
                                ->where(['year' => $yearFilter])
                                ->andWhere(['type' => 'O'])
                                ->andWhere('parent_id is  null')
                                ->count();

        $countKeyResultsOrg = Okr::find()
                                ->alias('o')
                                ->joinWith('parent p')
                                ->where(['o.year' => $yearFilter])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere('p.parent_id is  null')
                                ->count();

        $countObjectives = Okr::find()
                                ->where(['year' => $yearFilter])
                                ->andWhere(['type' => 'O'])
                                ->andWhere('parent_id is not null')
                                ->count();

        $countKeyResults = Okr::find()
                                ->alias('o')
                                ->joinWith('parent p')
                                ->where(['o.year' => $yearFilter])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere('p.parent_id is not null')
                                ->count();

        $countInitiatives = Initiatives::find()
                                ->alias('i')
                                ->joinWith('okr o')
                                ->where(['o.year' => $yearFilter])
                                ->count();

        if($yearFilter < 2023){
            $countActivity = Activity::find()
                                    ->alias('a')
                                    ->joinWith(['initiative i', 'initiative.okr o'])
                                    ->where(['o.year' => $yearFilter])
                                    ->count();
        } else {            
            $countActivity = Okr::find()
                                    ->where(['year' => $yearFilter])
                                    ->andWhere('reference_no is not null')
                                    ->andWhere("type = 'KR'")
                                    ->count();
        }

        $countBudget = Budget::find()
                                ->alias('b')
                                // ->joinWith(['initiatives i', 'initiatives.okr o'])
                                ->where(['year' => $yearFilter])
                                ->count();

        $sumBudget = Budget::find()
                                ->alias('b')
                                // ->joinWith(['initiatives i', 'initiatives.okr o'])
                                ->where(['year' => $yearFilter])
                                ->sum('total_amount');

        $searchModelUnit = new UnitStructureSearch();
        if($yearFilter){
            $searchModelUnit->year = $yearFilter;
        }
        $dataProviderUnit = $searchModelUnit->search(Yii::$app->request->queryParams);

        $totalAllParticipatory = $this->getParticipatoryTotal($yearFilter);
        $participatories = ParticipatoryBudgeting::find()->where(['is_active' => true])->orderBy('id desc')->all();
        $arrParticipatory = [];
        $arrParticipatoryValue = [];
        foreach($participatories as $p){
            $totalCurrentParticipatory = $this->getParticipatoryTotal($yearFilter, $p->id);
            array_push($arrParticipatory, ['id' => $p->id, 'val' => round($totalCurrentParticipatory, 2)]);
            array_push($arrParticipatoryValue, $p->participatory_budgeting);
        }

        if($this->getParticipatoryTotal($yearFilter, 9999) > 0){
            array_push($arrParticipatory, ['id' => 9999, 'val' => round($this->getParticipatoryTotal($yearFilter, 9999), 2)]);
            array_push($arrParticipatoryValue, 'Belum Diisi');
        }

        $totalAllFinancing = $this->getTotalTarget($yearFilter);
        $financings = FinancingType::find()->where(['is_active' => true])->orderBy('id desc')->all();
        $arrFinancing = [];
        $arrFinancingValue = [];
        foreach($financings as $f){
            $totalCurrentFinancing = $this->getFinancingTotal($yearFilter, $f->id);
            array_push($arrFinancing, ['id' => $f->id, 'val' => round($totalCurrentFinancing, 2)]);
            array_push($arrFinancingValue, $f->financing_type);
        }

        if($this->getFinancingTotal($yearFilter, 999999) > 0){
            array_push($arrFinancing, ['id' => 999999, 'val' => round($this->getFinancingTotal($yearFilter, 999999), 2)]);
            array_push($arrFinancingValue, 'Belum Diisi');
        }


        return $this->render('dashboard', 
                                [
                                    'yearFilter' => $yearFilter,
                                    'countObjectivesOrg' => $countObjectivesOrg,
                                    'countKeyResultsOrg' => $countKeyResultsOrg,
                                    'countObjectives' => $countObjectives,
                                    'countKeyResults' => $countKeyResults,
                                    'countInitiatives' => $countInitiatives,
                                    'countActivity' => $countActivity,
                                    'countBudget' => $countBudget,
                                    'sumBudget' => $sumBudget,
                                    'searchModelUnit' => $searchModelUnit,
                                    'dataProviderUnit' => $dataProviderUnit,
                                    'totalAllParticipatory' => $totalAllParticipatory,
                                    'arrParticipatoryValue' => $arrParticipatoryValue,
                                    'arrParticipatory' => $arrParticipatory,
                                    'totalAllFinancing' => $totalAllFinancing,
                                    'arrFinancingValue' => $arrFinancingValue,
                                    'arrFinancing' => $arrFinancing,
                                ]);
    }
    
    public function actionSummary2($year = null, $directorateId = null, $departmentId = null, $unitId = null){

        $selectedUnit = [];
        if(!$year) $year = date('Y');
        $unitName = 'Semua Unit di tahun ' . $year;

        $isAdmin = Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD') || Yii::$app->user->can('Leader') || Yii::$app->user->can('Admin Direktorat');

        $query = UnitStructure::find()->where(['year' => $year]);
        if(!Yii::$app->user->can('Leader')) $query->andWhere(['year' => date('Y')]);

        if($directorateId || $departmentId || $unitId){
            array_push($selectedUnit, $unitId);

            if($directorateId){
                $directorate = UnitStructure::find()->where(['superior_unit_id' => $directorateId])->all();
                foreach($directorate as $dir){
                    $departements = UnitStructure::find()->where(['superior_unit_id' => $dir->id])->all();
                    foreach($departements as $dept){
                        array_push($selectedUnit, $dept->id);
                    }
                }

                $query->andWhere(['in', 'id', $selectedUnit]);

                $unitName = UnitStructure::findOne($directorateId)->unit_name;
            } else if($departmentId){
                $departements = UnitStructure::find()->where(['superior_unit_id' => $departmentId])->all();
                foreach($departements as $dept){
                    array_push($selectedUnit, $dept->id);
                }

                $query->andWhere(['in', 'id', $selectedUnit]);

                $unitName = UnitStructure::findOne($departmentId)->unit_name;
            } else {
                $query->andWhere(['id' => $unitId]);

                $unitName = UnitStructure::findOne($unitId)->unit_name;
            }
        }

        if($isAdmin){
            $listDirectorate = ArrayHelper::map(UnitStructure::find()->where(['year' => $year])->andWhere(['level' => 1])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listDepartment = ArrayHelper::map(UnitStructure::find()->where(['year' => $year])->andWhere(['level' => 2])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $selectedUnit = [];
            $employeePositions = PositionStructureEmployee::find()->alias('pse')->joinWith('position p')->where(['employee_id' => $userID])->andWhere(['p.year' => ($year ?: date('Y'))])->all();
            foreach($employeePositions as $ep){
                array_push($selectedUnit, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($selectedUnit, $gm->unit_structure_id);
                    }
                }
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $selectedUnit])->andWhere(['year' => Yii::$app->user->can('Leader') ? $year : date('Y')])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $query->andWhere(['in', 'id', $selectedUnit]);
        }

        if(!$isAdmin && $unitId) $selectedUnit = $unitId;
        if(!$isAdmin && !$unitId) $unitName = 'Semua Unit Anda di tahun ' . $year;

        $monthName = [];
        $monthlyTargetCompany = [];
        $monthlyRealisasiTargetCompany = [];
        $monthlyBudgetCompany = [];
        $monthlyRealisasiBudgetCompany = [];
        $totalTargetCompany = 0;
        $totalRealisasiTargetCompany = 0;
        $totalBudgetCompany = 0;
        $totalRealisasiBudgetCompany = 0;
        for($m = 1; $m <= 12; $m++){
            array_push($monthName, DateHelper::getBulanIndo($m, true));

            $valTarget = Target::getUnitTargetCompany($year, 'target', $selectedUnit, $m);
            $valRealisasiTarget = Target::getUnitTargetCompany($year, 'realisasi', $selectedUnit, $m);
            $valBudget = Budget::getUnitBudgetCompany($year, 'budget', $selectedUnit, $m);
            $valRealisasiBudget = Budget::getUnitBudgetCompany($year, 'realisasi', $selectedUnit, $m);

            array_push($monthlyTargetCompany, (float) $valTarget);
            array_push($monthlyRealisasiTargetCompany, (float) $valRealisasiTarget);
            array_push($monthlyBudgetCompany, (float) $valBudget);
            array_push($monthlyRealisasiBudgetCompany, (float) $valRealisasiBudget);

            $totalTargetCompany = $totalTargetCompany + $valTarget;
            $totalRealisasiTargetCompany = $totalRealisasiTargetCompany + $valRealisasiTarget;
            $totalBudgetCompany = $totalBudgetCompany + $valBudget;
            $totalRealisasiBudgetCompany = $totalRealisasiBudgetCompany + $valRealisasiBudget;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['unit_code'=>SORT_ASC]],
            'pagination' => ['pageSize' => 10]
        ]);

        return $this->render('ceiling', [
            'isAdmin' => $isAdmin,
            'unitName' => $unitName,
            'year' => $year,
            'directorateId' => $directorateId,
            'departmentId' => $departmentId,
            'unitId' => $unitId,
            'listDirectorate' => $listDirectorate,
            'listDepartment' => $listDepartment,
            'listUnit' => $listUnit,
            'dataProvider' => $dataProvider,
            'totalTargetCompany' => $totalTargetCompany,
            'totalRealisasiTargetCompany' => $totalRealisasiTargetCompany,
            'totalBudgetCompany' => $totalBudgetCompany,
            'totalRealisasiBudgetCompany' => $totalRealisasiBudgetCompany,
            'monthName' => $monthName,
            'monthlyTargetCompany' => $monthlyTargetCompany,
            'monthlyRealisasiTargetCompany' => $monthlyRealisasiTargetCompany,
            'monthlyBudgetCompany' => $monthlyBudgetCompany,
            'monthlyRealisasiBudgetCompany' => $monthlyRealisasiBudgetCompany,
        ]);        
    }

    public function actionPlan($year = null){

        if(!$year) $year = date('Y');

        $management = Management::find()->where("$year >= period_start and $year <= period_end")->one();
        $vision = Vision::findOne(['management_id' => $management->id]);
        if($vision){
            $missions = Mission::find()->where(['vision_id' => $vision->id])->all();
        }

        $portfolio = Portfolio::find()->where("$year >= period_start and $year <= period_end")->andWhere(['is_active' => true])->one();
        
        return $this->render('plan', [
            'year' => $year,
            'management' => $management,
            'vision' => $vision,
            'missions' => $missions,
            'portfolio' => $portfolio,
        ]);  
    }

    public function actionOrganizationStructure($year = null){

        if(!$year) $year = date('Y');


        return $this->render('structure', [
            'year' => $year,
        ]);  
    }

    public function actionOkrOrganization($yearFilter = null){
        if(!$yearFilter){
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $yearFilter = /* $lastOkr->year ?:  */date('Y');
        }


        $countObjectivesOrg = Okr::find()
                                ->where(['year' => $yearFilter])
                                ->andWhere(['type' => 'O'])
                                ->andWhere('parent_id is  null')
                                ->andWhere(['okr_level_id' => OkrLevel::ORGANIZATION])
                                ->count();

        $countKeyResultsOrg = Okr::find()
                                ->alias('o')
                                ->joinWith('parent p')
                                ->where(['o.year' => $yearFilter])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere('p.parent_id is  null')
                                ->andWhere(['p.okr_level_id' => OkrLevel::ORGANIZATION])
                                ->count();

        $countInitiatives = Initiatives::find()
                                ->alias('i')
                                ->joinWith('okr o')
                                ->where(['o.year' => $yearFilter])
                                ->count();

        if($yearFilter < 2023){
            $countActivity = Activity::find()
                                    ->alias('a')
                                    ->joinWith(['initiative i', 'initiative.okr o'])
                                    ->where(['o.year' => $yearFilter])
                                    ->count();
        } else {            
            $countActivity = Okr::find()
                                    ->where(['year' => $yearFilter])
                                    ->andWhere('reference_no is not null')
                                    ->andWhere("type = 'KR'")
                                    ->count();
        }

        $searchModelUnit = new UnitStructureSearch();
        if($yearFilter){
            $searchModelUnit->year = $yearFilter;
        }
        $dataProviderUnit = $searchModelUnit->search(Yii::$app->request->queryParams);


        return $this->render('okr-organization', 
                                [
                                    'yearFilter' => $yearFilter,
                                    'countObjectivesOrg' => $countObjectivesOrg,
                                    'countKeyResultsOrg' => $countKeyResultsOrg,
                                    'countInitiatives' => $countInitiatives,
                                    'countActivity' => $countActivity,
                                    'searchModelUnit' => $searchModelUnit,
                                    'dataProviderUnit' => $dataProviderUnit,
                                ]);
    }
    public function actionOkrOrganization2($yearFilter = null){
        if(!$yearFilter){
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $yearFilter = /* $lastOkr->year ?:  */date('Y');
        }


        $countObjectivesOrg = Okr::find()
                                ->where(['year' => $yearFilter])
                                ->andWhere(['type' => 'O'])
                                ->andWhere('parent_id is  null')
                                ->andWhere(['okr_level_id' => OkrLevel::ORGANIZATION])
                                ->count();

        $countKeyResultsOrg = Okr::find()
                                ->alias('o')
                                ->joinWith('parent p')
                                ->where(['o.year' => $yearFilter])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere('p.parent_id is  null')
                                ->andWhere(['p.okr_level_id' => OkrLevel::ORGANIZATION])
                                ->count();

        $countInitiatives = Initiatives::find()
                                ->alias('i')
                                ->joinWith('okr o')
                                ->where(['o.year' => $yearFilter])
                                ->count();

        if($yearFilter < 2023){
            $countActivity = Activity::find()
                                    ->alias('a')
                                    ->joinWith(['initiative i', 'initiative.okr o'])
                                    ->where(['o.year' => $yearFilter])
                                    ->count();
        } else {            
            $countActivity = Okr::find()
                                    ->where(['year' => $yearFilter])
                                    ->andWhere('reference_no is not null')
                                    ->andWhere("type = 'KR'")
                                    ->count();
        }

        $searchModel = new PositionStructureSearch();
        if($yearFilter){
            $searchModel->year = $yearFilter;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('okr-organization2', 
                                [
                                    'yearFilter' => $yearFilter,
                                    'countObjectivesOrg' => $countObjectivesOrg,
                                    'countKeyResultsOrg' => $countKeyResultsOrg,
                                    'countInitiatives' => $countInitiatives,
                                    'countActivity' => $countActivity,
                                    'searchModel' => $searchModel,
                                    'dataProvider' => $dataProvider,
                                ]);
    }

    public function actionBudget($yearFilter = null){
        if(!$yearFilter){
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $yearFilter = /* $lastOkr->year ?:  */date('Y');
        }

        $searchModelUnit = new UnitStructureSearch();
        if($yearFilter){
            $searchModelUnit->year = $yearFilter;
        }
        $dataProviderUnit = $searchModelUnit->search(Yii::$app->request->queryParams);

        $totalAllParticipatory = $this->getParticipatoryTotal($yearFilter);
        $participatories = ParticipatoryBudgeting::find()->where(['is_active' => true])->orderBy('id desc')->all();
        $arrParticipatory = [];
        $arrParticipatoryValue = [];
        foreach($participatories as $p){
            $totalCurrentParticipatory = $this->getParticipatoryTotal($yearFilter, $p->id);
            array_push($arrParticipatory, ['id' => $p->id, 'val' => round($totalCurrentParticipatory, 2)]);
            array_push($arrParticipatoryValue, $p->participatory_budgeting);
        }

        if($this->getParticipatoryTotal($yearFilter, 9999) > 0){
            array_push($arrParticipatory, ['id' => 9999, 'val' => round($this->getParticipatoryTotal($yearFilter, 9999), 2)]);
            array_push($arrParticipatoryValue, 'Belum Diisi');
        }

        $totalAllFinancing = $this->getFinancingTotal($yearFilter);
        $financings = FinancingType::find()->where(['is_active' => true])->orderBy('id desc')->all();
        $arrFinancing = [];
        $arrFinancingValue = [];
        foreach($financings as $f){
            $totalCurrentFinancing = $this->getFinancingTotal($yearFilter, $f->id);
            array_push($arrFinancing, ['id' => $f->id, 'val' => round($totalCurrentFinancing, 2)]);
            array_push($arrFinancingValue, $f->financing_type);
        }

        if($this->getFinancingTotal($yearFilter, 999999) > 0){
            array_push($arrFinancing, ['id' => 999999, 'val' => round($this->getFinancingTotal($yearFilter, 999999), 2)]);
            array_push($arrFinancingValue, 'Belum Diisi');
        }

        $totalBudgetCompany = 0;
        $totalBudgetCompanyApproved = 0;
        for($m = 1; $m <= 12; $m++){
            $valBudget = Budget::getUnitBudgetCompany($yearFilter, 'budget', null, $m);
            $valBudgetApproved = Budget::getUnitBudgetCompany($yearFilter, 'budget', null, $m, true);
            // if(Yii::$app->user->can('SuperUser')) var_dump($valBudget);

            $totalBudgetCompany = $totalBudgetCompany + $valBudget;
            $totalBudgetCompanyApproved = $totalBudgetCompanyApproved + $valBudgetApproved;
        }

        return $this->render('budget', 
                                [
                                    'yearFilter' => $yearFilter,
                                    'searchModelUnit' => $searchModelUnit,
                                    'dataProviderUnit' => $dataProviderUnit,
                                    'totalAllParticipatory' => $totalAllParticipatory,
                                    'arrParticipatoryValue' => $arrParticipatoryValue,
                                    'arrParticipatory' => $arrParticipatory,
                                    'totalAllFinancing' => $totalAllFinancing,
                                    'arrFinancingValue' => $arrFinancingValue,
                                    'arrFinancing' => $arrFinancing,
                                    'totalBudgetCompany' => $totalBudgetCompany,
                                    'totalBudgetCompanyApproved' => $totalBudgetCompanyApproved,
                                ]);
    }

    public function actionBudget2($yearFilter = null){
        if(!$yearFilter){
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $yearFilter = /* $lastOkr->year ?:  */date('Y');
        }

        $searchModel = new PositionStructureSearch();
        if($yearFilter){
            $searchModel->year = $yearFilter;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $totalAllParticipatory = $this->getParticipatoryTotal($yearFilter);
        $participatories = ParticipatoryBudgeting::find()->where(['is_active' => true])->orderBy('id desc')->all();
        $arrParticipatory = [];
        $arrParticipatoryValue = [];
        foreach($participatories as $p){
            $totalCurrentParticipatory = $this->getParticipatoryTotal($yearFilter, $p->id);
            array_push($arrParticipatory, ['id' => $p->id, 'val' => round($totalCurrentParticipatory, 2)]);
            array_push($arrParticipatoryValue, $p->participatory_budgeting);
        }

        if($this->getParticipatoryTotal($yearFilter, 9999) > 0){
            array_push($arrParticipatory, ['id' => 9999, 'val' => round($this->getParticipatoryTotal($yearFilter, 9999), 2)]);
            array_push($arrParticipatoryValue, 'Belum Diisi');
        }

        $totalAllFinancing = $this->getFinancingTotal($yearFilter);
        $financings = FinancingType::find()->where(['is_active' => true])->orderBy('id desc')->all();
        $arrFinancing = [];
        $arrFinancingValue = [];
        foreach($financings as $f){
            $totalCurrentFinancing = $this->getFinancingTotal($yearFilter, $f->id);
            array_push($arrFinancing, ['id' => $f->id, 'val' => round($totalCurrentFinancing, 2)]);
            array_push($arrFinancingValue, $f->financing_type);
        }

        if($this->getFinancingTotal($yearFilter, 999999) > 0){
            array_push($arrFinancing, ['id' => 999999, 'val' => round($this->getFinancingTotal($yearFilter, 999999), 2)]);
            array_push($arrFinancingValue, 'Belum Diisi');
        }

        $totalBudgetCompany = 0;
        $totalBudgetCompanyApproved = 0;
        for($m = 1; $m <= 12; $m++){
            $valBudget = Budget::getUnitBudgetCompany($yearFilter, 'budget', null, $m);
            $valBudgetApproved = Budget::getUnitBudgetCompany($yearFilter, 'budget', null, $m, true);
            // if(Yii::$app->user->can('SuperUser')) var_dump($valBudget);

            $totalBudgetCompany = $totalBudgetCompany + $valBudget;
            $totalBudgetCompanyApproved = $totalBudgetCompanyApproved + $valBudgetApproved;
        }

        return $this->render('budget2', 
                                [
                                    'yearFilter' => $yearFilter,
                                    'searchModel' => $searchModel,
                                    'dataProvider' => $dataProvider,
                                    'totalAllParticipatory' => $totalAllParticipatory,
                                    'arrParticipatoryValue' => $arrParticipatoryValue,
                                    'arrParticipatory' => $arrParticipatory,
                                    'totalAllFinancing' => $totalAllFinancing,
                                    'arrFinancingValue' => $arrFinancingValue,
                                    'arrFinancing' => $arrFinancing,
                                    'totalBudgetCompany' => $totalBudgetCompany,
                                    'totalBudgetCompanyApproved' => $totalBudgetCompanyApproved,
                                ]);
    }

    public function actionTarget($year = null){

        if(!$year) $year = date('Y');

        $totalCollection = Target::find()->where(['target_type_id' => [1, 16, 17, 18, 19, 20, 21, 22, 28]])->andWhere(['year' => $year])->sum('total_target');
        $totalBenefitRecipient = Target::find()->where(['target_type_id' => [29, 23, 26, 13, 4, 25, 27, 24, 15, 14]])->andWhere(['year' => $year])->sum('total_target');

        $monthName = [];
        $monthlyTargetCompany = [];
        $monthlyRealisasiTargetCompany = [];
        $collectionSegmentIndividual = [];
        $collectionSegmentInstitution = [];
        for($m = 1; $m <= 12; $m++){
            array_push($monthName, DateHelper::getBulanIndo($m, true));

            $valTarget = Target::getUnitTargetCompany($year, 'target', null, $m, [1, 20, 16, 22, 17, 21, 18, 19]);
            $valRealisasiTarget = Target::getUnitTargetCompany(($year-1), 'realisasi', null, $m, [1, 20, 16, 22, 17, 21, 18, 19]);

            array_push($monthlyTargetCompany, (float) $valTarget);
            array_push($monthlyRealisasiTargetCompany, (float) $valRealisasiTarget);

            $month = 'month_' . str_pad(((int) $m), 2, "0", STR_PAD_LEFT);
            $individualCollection = Target::find()->where(['year' => $year])->andWhere(['target_type_id' => 11])->sum($month);
            array_push($collectionSegmentIndividual, (float) $individualCollection ?: 0);

            $institutionCollection = Target::find()->where(['year' => $year])->andWhere(['target_type_id' => 12])->sum($month);
            array_push($collectionSegmentInstitution, (float) $institutionCollection ?: 0);
        }


        $searchModelUnit = new UnitStructureSearch();
        if($year){
            $searchModelUnit->year = $year;
        }
        $dataProviderUnit = $searchModelUnit->search(Yii::$app->request->queryParams);

        $targetTypeCollectionIds = [1, 16, 17, 18, 19, 20, 21, 22, 28];
        $arrayCollection = [];
        foreach($targetTypeCollectionIds as $t){
            $target = Target::find()->where(['year' => $year])->andWhere(['target_type_id' => $t])->sum('total_target');
            if($totalCollection){
                $percentage = $target / $totalCollection;
            } else {
                $percentage = 0;
            }

            array_push($arrayCollection, [
                                            'name' => TargetType::findOne($t)->target_type, 
                                            'x' => number_format($target, 2, ',', '.'),
                                            'y' => $percentage*100
                                        ]);
        }

        $benefitRecipientIds = [29, 23, 26, 13, 4, 25, 27, 24, 15, 14];
        $arrayBenefitRecipient = [];
        foreach($benefitRecipientIds as $b){
            $target = Target::find()->where(['year' => $year])->andWhere(['target_type_id' => $b])->sum('total_target');
            if($totalBenefitRecipient){
                $percentage = $target / $totalBenefitRecipient;
            } else {
                $percentage = 0;
            }

            array_push($arrayBenefitRecipient, [
                                            'name' => TargetType::findOne($b)->target_type, 
                                            'x' => number_format($target, 2, ',', '.'),
                                            'y' => $percentage*100
                                        ]);
        }

        return $this->render('target', [
            'year' => $year,
            'totalCollection' => $totalCollection,
            'totalBenefitRecipient' => $totalBenefitRecipient,
            'monthName' => $monthName,
            'monthlyTargetCompany' => $monthlyTargetCompany,
            'monthlyRealisasiTargetCompany' => $monthlyRealisasiTargetCompany,
            'searchModelUnit' => $searchModelUnit,
            'dataProviderUnit' => $dataProviderUnit,
            'arrayCollection' => $arrayCollection,
            'arrayBenefitRecipient' => $arrayBenefitRecipient,
            'collectionSegmentIndividual' => $collectionSegmentIndividual,
            'collectionSegmentInstitution' => $collectionSegmentInstitution,
        ]);  
    }




    protected function getParticipatoryTotal($year, $participatory_id = null){

        if($participatory_id){
            if($participatory_id == 9999){
                $filter = " i.participatory_budgeting_id IS NULL";
            } else {
                $filter = " i.participatory_budgeting_id = " . $participatory_id;
            }
        } else {
            $filter = " 1 = 1";
        }

        $totalAmount = Yii::$app->db->createCommand("
                        SELECT 
                            sum(i.total_budget) AS total_sum
                        FROM
                            management.initiatives i
                        INNER JOIN management.okr o ON i.okr_id = o.id
                        WHERE 
                            o.year = $year AND
                            $filter
                    ")->queryScalar();
                    

        return $totalAmount ?: 0;
    }

    protected function getFinancingTotal($year, $financing_id = null){

        if($financing_id){
            if($financing_id == 999999){
                $filter = " b.financing_type_id IS NULL";
            } else {
                $filter = " b.financing_type_id = " . $financing_id;
            }
        } else {
            $filter = " 1 = 1";
        }

        $totalAmount = Yii::$app->db->createCommand("
                        SELECT 
                            sum(b.total_amount) AS total_sum
                        FROM
                            management.budget b
                        WHERE 
                            b.year = $year AND
                            $filter
                    ")->queryScalar();
                    

        return $totalAmount ?: 0;
    }

    protected function getTotalTarget($year){

        $totalAmount = Yii::$app->db->createCommand("
                        SELECT 
                            sum(t.total_target) AS total_sum
                        FROM
                            management.target t
                        WHERE 
                            t.year = $year AND
                            t.target_type_id = 1
                    ")->queryScalar();
                    

        return $totalAmount ?: 0;
    }

    public function actionOkr()
    {
        $request = Yii::$app->request;
        $searchModel = new EmployeeAchievement();
        $dataProvider = $searchModel->search($request->queryParams, 'OKR');

        return $this->render('achievement/index-okr', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOkr2($year = null)
    {
        $request = Yii::$app->request;
        if(!$year) $year = date('Y');

        if(!Yii::$app->user->can('Admin') && !Yii::$app->user->can('Leader')) $year = date('Y');

        $searchModel = new PositionStructureEmployeeSearch();
        $searchModel->year = $year;
        $dataProvider = $searchModel->search($request->queryParams);

        return $this->render('achievement2/individual', [
            'year' => $year,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionActivity()
    {
        $request = Yii::$app->request;
        $searchModel = new EmployeeAchievement();
        $dataProvider = $searchModel->search($request->queryParams, 'Activity');

        return $this->render('achievement/index-activity', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionActivities()
    {
        $request = Yii::$app->request;
        $searchModel = new OkrSearch();
        $dataProvider = $searchModel->search($request->queryParams);

        return $this->render('achievement/index-activities', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUnit()
    {
        $request = Yii::$app->request;
        $searchModelUnit = new UnitAchievement();

        if(!$searchModelUnit->evaluation_id){
            $lastEvaluation = Evaluation::find()->orderBy('id desc')->one();
            $searchModelUnit->evaluation_id = $lastEvaluation->id;
        }

        $dataProviderUnit = $searchModelUnit->search($request->queryParams);
        $unit_id = $searchModelUnit->unit_id;
        $directorate_id = $searchModelUnit->directorate_id;
        $department_id = $searchModelUnit->department_id;
        $evaluation_id = $searchModelUnit->evaluation_id;

        $searchModelUnitOkr = new OkrEvaluationSearch();
        $searchModelUnitOkr->unit_id = $unit_id;
        $searchModelUnitOkr->directorate_id = $directorate_id;
        $searchModelUnitOkr->department_id = $department_id;
        $dataProviderUnitOkr = $searchModelUnitOkr->search($request->queryParams);

        $searchModelUnitInitiatives = new InitiativesEvaluationSearch();
        $searchModelUnitInitiatives->unit_id = $unit_id;
        $searchModelUnitInitiatives->directorate_id = $directorate_id;
        $searchModelUnitInitiatives->department_id = $department_id;
        $dataProviderUnitInitiatives = $searchModelUnitInitiatives->search($request->queryParams);

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModelUnitOkr->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $searchModelUnitOkr->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }

        $listDirectorate = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModelUnitOkr->year])->andWhere(['level' => 1])->orderBy('unit_code')->all(), 'id', function($model){
            return $model->unit_code . ' - ' . $model->unit_name;
        });

        $listDepartment = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModelUnitOkr->year])->andWhere(['level' => 2])->orderBy('unit_code')->all(), 'id', function($model){
            return $model->unit_code . ' - ' . $model->unit_name;
        });

        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one();
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one();

        return $this->render('achievement/index', [
            'searchModelUnit' => $searchModelUnit,
            'dataProviderUnit' => $dataProviderUnit,
            'unit_id' => $unit_id,
            'directorate_id' => $directorate_id,
            'department_id' => $department_id,
            'evaluation_id' => $evaluation_id,
            'searchModelUnitOkr' => $searchModelUnitOkr,
            'dataProviderUnitOkr' => $dataProviderUnitOkr,
            'searchModelUnitInitiatives' => $searchModelUnitInitiatives,
            'dataProviderUnitInitiatives' => $dataProviderUnitInitiatives,
            'listUnit' => $listUnit,
            'listDirectorate' => $listDirectorate,
            'listDepartment' => $listDepartment,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
        ]);
    }

    public function actionUnit2($year = null){
        $request = Yii::$app->request;

        if(!$year) $year = date('Y');

        if(!Yii::$app->user->can('Admin') && !Yii::$app->user->can('Leader')) $year = date('Y');

        $searchModelUnit = new UnitStructureSearch();
        $searchModelUnit->year = $year;
        $dataProviderUnit = $searchModelUnit->search($request->queryParams);

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }

        $listDirectorate = ArrayHelper::map(UnitStructure::find()->where(['year' => $year])->andWhere(['level' => 1])->orderBy('unit_code')->all(), 'id', function($model){
            return $model->unit_code . ' - ' . $model->unit_name;
        });

        $listDepartment = ArrayHelper::map(UnitStructure::find()->where(['year' => $year])->andWhere(['level' => 2])->orderBy('unit_code')->all(), 'id', function($model){
            return $model->unit_code . ' - ' . $model->unit_name;
        });

        return $this->render('achievement2/index', [
            'year' => $year,
            'listUnit' => $listUnit,
            'listDepartment' => $listDepartment,
            'listDirectorate' => $listDirectorate,
            'searchModelUnit' => $searchModelUnit,
            'dataProviderUnit' => $dataProviderUnit,
        ]);
    }
}
