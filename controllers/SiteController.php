<?php

namespace app\controllers;

use app\models\Activity;
use app\models\Application;
use app\models\Budget;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\FinancingType;
use app\models\Initiatives;
use app\models\Okr;
use app\models\ParticipatoryBudgeting;
use app\models\search\UnitStructureSearch;
use app\models\UnitStructure;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    /* public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'logout' => ['post'],
                ],
            ],
        ];
    } */

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionDashboard($yearFilter = null)
    {
        if(!$yearFilter){
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $yearFilter = $lastOkr->year ?: date('Y');
        }


        $countObjectivesOrg = Okr::find()
                                ->where(['year' => $yearFilter])
                                ->andWhere(['type' => 'O'])
                                ->andWhere('parent_id is  null')
                                ->count();

        $countKeyResultsOrg = Okr::find()
                                ->alias('o')
                                ->joinWith('parent p')
                                ->where(['o.year' => $yearFilter])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere('p.parent_id is  null')
                                ->count();

        $countObjectives = Okr::find()
                                ->where(['year' => $yearFilter])
                                ->andWhere(['type' => 'O'])
                                ->andWhere('parent_id is not null')
                                ->count();

        $countKeyResults = Okr::find()
                                ->alias('o')
                                ->joinWith('parent p')
                                ->where(['o.year' => $yearFilter])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere('p.parent_id is not null')
                                ->count();

        $countInitiatives = Initiatives::find()
                                ->alias('i')
                                ->joinWith('okr o')
                                ->where(['o.year' => $yearFilter])
                                ->count();

        $countActivity = Activity::find()
                                ->alias('a')
                                ->joinWith(['initiative i', 'initiative.okr o'])
                                ->where(['o.year' => $yearFilter])
                                ->count();

        $countBudget = Budget::find()
                                ->alias('b')
                                // ->joinWith(['initiatives i', 'initiatives.okr o'])
                                ->where(['year' => $yearFilter])
                                ->count();

        $sumBudget = Budget::find()
                                ->alias('b')
                                // ->joinWith(['initiatives i', 'initiatives.okr o'])
                                ->where(['year' => $yearFilter])
                                ->sum('total_amount');

        $searchModelUnit = new UnitStructureSearch();
        if($yearFilter){
            $searchModelUnit->year = $yearFilter;
        }
        $dataProviderUnit = $searchModelUnit->search(Yii::$app->request->queryParams);

        $totalAllParticipatory = $this->getParticipatoryTotal(2022);
        $participatories = ParticipatoryBudgeting::find()->where(['is_active' => true])->orderBy('id desc')->all();
        $arrParticipatory = [];
        $arrParticipatoryValue = [];
        foreach($participatories as $p){
            $totalCurrentParticipatory = $this->getParticipatoryTotal($yearFilter, $p->id);
            array_push($arrParticipatory, ['id' => $p->id, 'val' => round($totalCurrentParticipatory, 2)]);
            array_push($arrParticipatoryValue, $p->participatory_budgeting);
        }

        if($this->getParticipatoryTotal($yearFilter, 9999) > 0){
            array_push($arrParticipatory, ['id' => 9999, 'val' => round($this->getParticipatoryTotal($yearFilter, 9999), 2)]);
            array_push($arrParticipatoryValue, 'Belum Diisi');
        }

        $totalAllFinancing = $this->getTotalTarget(2022);
        $financings = FinancingType::find()->where(['is_active' => true])->orderBy('id desc')->all();
        $arrFinancing = [];
        $arrFinancingValue = [];
        foreach($financings as $f){
            $totalCurrentFinancing = $this->getFinancingTotal($yearFilter, $f->id);
            array_push($arrFinancing, ['id' => $f->id, 'val' => round($totalCurrentFinancing, 2)]);
            array_push($arrFinancingValue, $f->financing_type);
        }

        if($this->getFinancingTotal($yearFilter, 999999) > 0){
            array_push($arrFinancing, ['id' => 999999, 'val' => round($this->getFinancingTotal($yearFilter, 999999), 2)]);
            array_push($arrFinancingValue, 'Belum Diisi');
        }


        return $this->render('dashboard', 
                                [
                                    'yearFilter' => $yearFilter,
                                    'countObjectivesOrg' => $countObjectivesOrg,
                                    'countKeyResultsOrg' => $countKeyResultsOrg,
                                    'countObjectives' => $countObjectives,
                                    'countKeyResults' => $countKeyResults,
                                    'countInitiatives' => $countInitiatives,
                                    'countActivity' => $countActivity,
                                    'countBudget' => $countBudget,
                                    'sumBudget' => $sumBudget,
                                    'searchModelUnit' => $searchModelUnit,
                                    'dataProviderUnit' => $dataProviderUnit,
                                    'totalAllParticipatory' => $totalAllParticipatory,
                                    'arrParticipatoryValue' => $arrParticipatoryValue,
                                    'arrParticipatory' => $arrParticipatory,
                                    'totalAllFinancing' => $totalAllFinancing,
                                    'arrFinancingValue' => $arrFinancingValue,
                                    'arrFinancing' => $arrFinancing,
                                ]);
    }

    protected function getParticipatoryTotal($year, $participatory_id = null){

        if($participatory_id){
            if($participatory_id == 9999){
                $filter = " i.participatory_budgeting_id IS NULL";
            } else {
                $filter = " i.participatory_budgeting_id = " . $participatory_id;
            }
        } else {
            $filter = " 1 = 1";
        }

        $totalAmount = Yii::$app->db->createCommand("
                        SELECT 
                            sum(i.total_budget) AS total_sum
                        FROM
                            management.initiatives i
                        INNER JOIN management.okr o ON i.okr_id = o.id
                        WHERE 
                            o.year = $year AND
                            $filter
                    ")->queryScalar();
                    

        return $totalAmount ?: 0;
    }

    protected function getFinancingTotal($year, $financing_id = null){

        if($financing_id){
            if($financing_id == 999999){
                $filter = " b.financing_type_id IS NULL";
            } else {
                $filter = " b.financing_type_id = " . $financing_id;
            }
        } else {
            $filter = " 1 = 1";
        }

        $totalAmount = Yii::$app->db->createCommand("
                        SELECT 
                            sum(b.total_amount) AS total_sum
                        FROM
                            management.budget b
                        WHERE 
                            b.year = $year AND
                            $filter
                    ")->queryScalar();
                    

        return $totalAmount ?: 0;
    }

    protected function getTotalTarget($year){

        $totalAmount = Yii::$app->db->createCommand("
                        SELECT 
                            sum(t.total_target) AS total_sum
                        FROM
                            management.target t
                        WHERE 
                            t.year = $year AND
                            t.target_type_id = 1
                    ")->queryScalar();
                    

        return $totalAmount ?: 0;
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isAjax) {
            Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['']));
            return [
                'title' => 'Info',
                'content' => '<span class="text-success">Berhasil keluar dari aplikasi HRM!</span>',
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->goHome();
        }

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplicationList()
    {
        $request = Yii::$app->request;

        $applications = Application::find()->where(['is_active' => true])->andWhere("url != '' or url != '#'")->orderBy('order_no')->all();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'Daftar Aplikasi Intranet',
                    'content'=>$this->renderAjax('application-list', [
                        'applications' => $applications,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
        }else{
            return $this->redirect('index');
        }
    }
}
