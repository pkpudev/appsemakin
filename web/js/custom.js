

$(document).ready(function(){

  // MENU DROPDOWN USER
  $('.dropright').on('show.bs.dropdown', function(e){
    $(this).find('.dropdown-menu').first().stop(true, true).fadeIn(300);
  });

  $('.dropright').on('hide.bs.dropdown', function(e){
    $(this).find('.dropdown-menu').first().stop(true, true).fadeOut(200);
  });

    //Dynamic Next
   $(".btn-next").on("click", function(){
     nextStep = $("#" + $(this).parents(".slider-step").data("nextStep"));
     $(this).parents(".slider-step").attr("data-anim","hide-to--left");
     nextStep.attr("data-anim","show-from--right");
     $('html, body').animate({scrollTop: '0px'}, 500);
   });
   
   //Dynamic Back
   $(".btn-back").on("click", function(){
     backTo = $("#" + $(this).parents(".slider-step").data("backTo"));
     $(this).parents(".slider-step").attr("data-anim","hide-to--right");
     backTo.attr("data-anim","show-from--left");
     $('html, body').animate({scrollTop: '0px'}, 500);
   });

   // RESIZE for SHOW .close-layer
  $(window).resize(function() {
    
    var previewwidth = $(window).width();
    if(previewwidth > 768){
      $(".close-layer").hide();
    }
  });

  $('[rel="tooltip"]').on('click', function () {
    $(this).tooltip('hide')
  });

});



$( "#tog-cont" ).click(function() {
  var previewwidth = $(window).width();
  if(previewwidth < 768){
    $(".close-layer").show();
  }

  $("html, body").animate({ scrollTop: 0 }, "slow");
});

$( ".close-layer" ).click(function() {
  $(".close-layer").hide()
  usrProf();
});


  
$("#menu-btn, #tog-cont").click(function(){
  usrProf();
});

function usrProf(){
  if($("body").hasClass("sidebar-collapse")){
    $("#usr").fadeIn(300).show();
  } else {
    $("#usr").fadeOut(200).hide();
  }
}

var TRange=null;

function findString (str) {
 if (parseInt(navigator.appVersion)<4) return;
 var strFound;
 if (window.find) {

  // CODE FOR BROWSERS THAT SUPPORT window.find

  strFound=self.find(str);
  if (!strFound) {
   strFound=self.find(str,0,1);
   while (self.find(str,0,1)) continue;
  }
 }
 else if (navigator.appName.indexOf("Microsoft")!=-1) {

  // EXPLORER-SPECIFIC CODE

  if (TRange!=null) {
   TRange.collapse(false);
   strFound=TRange.findText(str);
   if (strFound) TRange.select();
  }
  if (TRange==null || strFound==0) {
   TRange=self.document.body.createTextRange();
   strFound=TRange.findText(str);
   if (strFound) TRange.select();
  }
 }
 else if (navigator.appName=="Opera") {
  alert ("Opera browsers not supported, sorry...")
  return;
 }
 if (!strFound) alert ("String '"+str+"' not found!")
 return;
}

//Modal Animate
/* $('#ajaxCrudModal').on('show.bs.modal', function (e) {
  $('.modal .modal-dialog').attr('class', 'modal-dialog bounceIn animated');
}) */

/* $('#ajaxCrudModal').on('hide.bs.modal', function (e) {
  $('.modal .modal-dialog').attr('class', 'modal-dialog bounceOut animated');
}) */


 

//theme
var theme = localStorage['semakin-theme'];
if (theme == 'dark') {
    $("body").addClass("dark-mode");
    $('#theme').prop('checked', true);
    $('#theme2').prop('checked', true);
} else {
    $("body").removeClass("dark-mode");
    $('#theme').prop('checked', false);
    $('#theme2').prop('checked', false);
}

jQuery('#theme').change(function() {
    if ($(this).prop('checked')) {
        $("body").addClass("dark-mode"); //checked
        localStorage['semakin-theme'] = "dark";
    }
    else {
        $("body").removeClass("dark-mode"); //not checked
        sessionStorage.removeItem('theme');
        localStorage['semakin-theme'] = "";
    }
});

jQuery('#theme2').change(function() {
    if ($(this).prop('checked')) {
        $("body").addClass("dark-mode"); //checked
        localStorage['semakin-theme'] = "dark";
    }
    else {
        $("body").removeClass("dark-mode"); //not checked
        sessionStorage.removeItem('theme');
        localStorage['semakin-theme'] = "";
    }
});