<?php

use app\models\Activity;
use app\models\OkrOrgMov;
use app\models\PositionStructureEmployee;
use app\models\Status;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'attribute' => 'perspective_id',
        'label' => 'Perspektif',
        'value' => function($model){
            return $model->okr->org->perspective_id ? $model->okr->org->perspective->perspective: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\Perspective::find()->orderBy('id')->asArray()->all(), 'id', 'perspective'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
        'group' => true,
    ],
    [
        'attribute' => 'perspective_id',
        'label' => 'Bobot Perspektif',
        'value' => function($model){
            return $model->okr->org->perspective_id ? $model->okr->org->perspective->weight  . '%': '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 1,
        'visible' => Yii::$app->user->can('Admin')
    ],
    [
        'attribute' => 'parent_okr_code',
        'label' => 'Objective',
        'format' => 'raw',
        'value' => function($model){
            return '<b>' . $model->okr->parent->okr_code . '</b><br />' . $model->okr->parent->okr;
        },
        'group' => true,
        'subGroupOf' => 1,
        'width' => '250px'
    ],
    [
        'attribute' => 'okr_code',
        'label' => 'Key Result',
        'format' => 'raw',
        'value' => function($model){
            return '<b>' . $model->okr->okr_code . '</b><br />' . $model->okr->okr;
        },
        'group' => true,
        'subGroupOf' => 3,
        'width' => '250px'
    ],
    [
        'attribute' => 'weight',
        'label' => 'Bobot Key Result',
        'value' => function($model){
            return $model->okr->org->weight ? $model->okr->org->weight . '%': '';
        },
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
        'visible' => Yii::$app->user->can('Admin')
    ],
    [
        'label' => 'Rencana Anggaran',
        'value' => function($model){
            return $model->achieved->budget_plan ? number_format($model->achieved->budget_plan, 2, ',', '.') . '%': '-';
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Realisasi Anggaran',
        'value' => function($model){
            return $model->achieved->budget_real ? number_format($model->achieved->budget_real, 2, ',', '.') . '%': '-';
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'attribute' => 'portofolio_type',
        'label' => 'Jenis Portofolio',
        'value' => function($model){
            return $model->okr->org->portfolio_type_id ? $model->okr->org->portfolio->portfolio_type: '';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\PortfolioType::find()->orderBy('id')->asArray()->all(), 'id', 'portfolio_type'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
        'group' => true,
        'subGroupOf' => 4,
    ],
    [
        'attribute' => 'target',
        'label' => 'Target',
        'value' => function($model){
            return $model->okr->target ? $model->okr->target . $model->okr->measure: '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
    ],
    /* [
        'attribute' => 'target',
        'label' => 'Kuartal 1',
        'value' => function($model){
            return $model->okr->target_q1 ? $model->okr->target_q1 . $model->okr->measure: '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
    ],
    [
        'attribute' => 'target',
        'label' => 'Kuartal 2',
        'value' => function($model){
            return $model->okr->target_q2 ? $model->okr->target_q2 . $model->okr->measure: '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
    ],
    [
        'attribute' => 'target',
        'label' => 'Kuartal 3',
        'value' => function($model){
            return $model->okr->target_q3 ? $model->okr->target_q3 . $model->okr->measure: '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
    ],
    [
        'attribute' => 'target',
        'label' => 'Kuartal 4',
        'value' => function($model){
            return $model->okr->target_q4 ? $model->okr->target_q4 . $model->okr->measure: '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
    ], */
    [
        'label' => 'Departemen',
        'attribute' => 'unit_structure_id',
        'format' => 'raw',
        'value' => function($model){
            return $model->unit_structure_id ? $model->unit->unit_name : '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(\app\models\UnitStructure::find()->where(['year' => date('Y')])->andWhere(['level' => 2])->orderBy('unit_name')->asArray()->all(), 'id', 'unit_name'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
        'width' => '150px'
    ],
    [
        'attribute' => 'weight',
        'label' => 'Bobot Kontribusi',
        'value' => function($model){
            return $model->weight ? number_format($model->weight, 2, ',', '.') . '%': '-';
        },
        'hAlign' => 'center',
        'visible' => Yii::$app->user->can('Admin')
    ],
    [
        'label' => 'Nilai Realisasi Key Result',
        'value' => function($model){
            return $model->achieved ? number_format($model->achieved->achieve_value, 2, ',', '.') . '%': '-';
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Keluaran Yang Dihasilkan',
        'value' => function($model){
            return $model->output ?: '-';
        }
    ],
    [
        'label' => 'Catatan KR',
        'value' => function($model){
            return $model->note ?: '-';
        }
    ],
    /* [
        'label' => 'Analisis Capaian',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->achieved->performance_analysis ?: '-') .
                            '</td>
                            <td width="10px">' .
                            (Yii::$app->user->can('Admin') ?  Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['performance-analysis', 'id' => $model->okr_id, 'evaluation_id' => $model->evaluation_id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Analisis Capaian',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
    ], */
    [
        'label' => 'Interpretasi',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->achieved->interpretation ?: '-') .
                            '</td>
                            <td width="10px">' .
                            (Yii::$app->user->can('Admin') ?  Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['interpretation', 'id' => $model->okr_id, 'evaluation_id' => $model->evaluation_id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Interpretasi',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
    ],
];
