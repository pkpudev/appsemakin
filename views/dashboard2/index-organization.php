<?php

use app\models\Evaluation;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$this->title = 'Laporan OKR Organisasi';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="cost-type-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => require(__DIR__.'/_columns_organization.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Daftar Laporan OKR Organisasi',
                'before'=>'<div class="row">' .
                            '<div class="col-md-2">' .
                                    Select2::widget([
                                        'model' => $searchModel,
                                        'attribute' => 'evaluation_id',
                                        'pjaxContainerId' => 'crud-datatable-pjax',
                                        'options' => [
                                            'id' => 'divisi-filter',
                                            'class' => 'form-control additional-filter',
                                            'placeholder' => '-- Filter Unit Kerja --',
                                        ],
                                        'data' => ArrayHelper::map(Evaluation::find()->orderBy('id')->all(), 'id', function($model){
                                            return $model->year . ' - ' . $model->period;
                                        }),
                                    ]) .
                            '</div>' .
                            '</div>',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>