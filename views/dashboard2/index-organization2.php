<?php

use app\models\Calculation;
use app\models\Evaluation;
use app\models\Okr;
use app\models\OkrEvaluation;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\Progress;
use yii\helpers\ArrayHelper;

$this->title = 'Laporan OKR Organisasi';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>
    .box-dashboard {
        border: 1px solid var(--background);
        border-radius: 5px;
        padding: 5px; 
        text-align: center; 
        margin: 5px auto;
    }
</style>

<div class="cost-type-index">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-default">
                <div class="box-body">
                    <label style="color: var(--borderColor);font-size: 17px;">Filter Tahun</label>
                    <?= DatePicker::widget([
                        'name' => 'year-filter',
                        'value' => $year,
                        'options' => [
                            'placeholder' => 'Filter Tahun',
                            'onchange' => 'changeYear($(this).val())',
                        ],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'startView'=>'year',
                            'minViewMode'=>'years',
                            'format' => 'yyyy',
                            'allowClear' => false
                        ]
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="box box-default">
                <div class="box-body">
                    <div class="box-dashboard">
                        <h4 style="color: var(--borderColor);font-size: 24px;font-weight: bold;">Total Capaian</h4>
                        <h2 style="font-size: 80px;margin-top: -15px;"><?= round($totalAchieve, 2); ?> %</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div id="ajaxCrudDatatable">
        
    <?=GridView::widget([
            'id'=>'crud-datatable-objective',
            'dataProvider' => $dataProviderObjective,
            'filterModel' => $searchModelObjective,
            'filterSelector' => '.additional-filter',
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'width' => '50px',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    // uncomment below and comment detail if you need to render via ajax
                    // 'detailUrl' => Url::to(['/site/book-details']),
                    'detail' => function ($model, $key, $index, $column) {
                        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
                    },
                    'headerOptions' => ['class' => 'kartik-sheet-style'], 
                    'expandOneOnly' => true
                ],
                // [
                //     'attribute' => 'id',
                //     'visible' => Yii::$app->user->can('SuperUser')
                // ],
                [
                    'attribute' => 'okr_code',
                ],
                [
                    'attribute' => 'okr',
                    'label' => 'Objective'
                ],
                [
                    'label' => 'Capaian Kuartal 1',
                    'format' => 'raw',
                    'value' => function($model){
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $percentage = $model->getObjAchieveValue($year, 'Kuartal 1');
                        // if(Yii::$app->user->can('SuperUser')) var_dump($model->id);

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    }, 
                    'width' => '200px'
                ],
                [
                    'label' => 'Capaian Kuartal 2',
                    'format' => 'raw',
                    'value' => function($model){
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $percentage = $model->getObjAchieveValue($year, 'Kuartal 2');

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    }, 
                    'width' => '200px'
                ],
                [
                    'label' => 'Capaian Kuartal 3',
                    'format' => 'raw',
                    'value' => function($model){
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $percentage = $model->getObjAchieveValue($year, 'Kuartal 3');
                        // if(Yii::$app->user->can('SuperUser')) var_dump($model->id);

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    }, 
                    'width' => '200px'
                ],
                [
                    'label' => 'Capaian Kuartal 4',
                    'format' => 'raw',
                    'value' => function($model){
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $percentage = $model->getObjAchieveValue($year, 'Kuartal 4');

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    }, 
                    'width' => '200px'
                ],
                [
                    'label' => 'Hasil Akhir',
                    'format' => 'raw',
                    'value' => function($model){
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');

                        $childs = Okr::find()->where(['parent_id' => $model->id])->andWhere(['okr_level_id' => 1])->all();
                        
                        $finalValue = 0;
                        foreach($childs as $child){
                            $achieveValues = [];

                            $evaluationK1 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 1']);

                            if($evaluationK1){
                                $okrAchievements = OkrEvaluation::find()
                                                                    ->where(['evaluation_id' => $evaluationK1->id])
                                                                    ->andWhere(['okr_id' => $child->id])
                                                                    ->all();

                                $achieve = 0;
                                foreach($okrAchievements as $okrAchievement){
                                    $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                                }
                                
                                if($achieve) array_push($achieveValues, $achieve?:0);
                            } else {
                                // array_push($achieveValues, 0);
                            }

                            $evaluationK2 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 2']);

                            if($evaluationK2){
                                $okrAchievements = OkrEvaluation::find()
                                                                    ->where(['evaluation_id' => $evaluationK2->id])
                                                                    ->andWhere(['okr_id' => $child->id])
                                                                    ->all();

                                $achieve = 0;
                                foreach($okrAchievements as $okrAchievement){
                                    $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                                }
                                
                                if($achieve) array_push($achieveValues, $achieve?:0);
                            } else {
                                // array_push($achieveValues, 0);
                            }

                            $evaluationK3 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 3']);

                            if($evaluationK3){
                                $okrAchievements = OkrEvaluation::find()
                                                                    ->where(['evaluation_id' => $evaluationK3->id])
                                                                    ->andWhere(['okr_id' => $child->id])
                                                                    ->all();

                                $achieve = 0;
                                foreach($okrAchievements as $okrAchievement){
                                    $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                                }
                                
                                if($achieve) array_push($achieveValues, $achieve?:0);
                            } else {
                                // array_push($achieveValues, 0);
                            }

                            $evaluationK4 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 4']);

                            if($evaluationK4){
                                $okrAchievements = OkrEvaluation::find()
                                                                    ->where(['evaluation_id' => $evaluationK4->id])
                                                                    ->andWhere(['okr_id' => $child->id])
                                                                    ->all();

                                $achieve = 0;
                                foreach($okrAchievements as $okrAchievement){
                                    $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                                }
                                
                                if($achieve) array_push($achieveValues, $achieve?:0);
                            } else {
                                // array_push($achieveValues, 0);
                            }

                            // if(Yii::$app->user->id ==487497 && $model->okr_code == '01') echo json_encode($achieveValues);
                            if($child->calculation->id == Calculation::SUM){
                                $lastAchieveValue = array_sum($achieveValues);
                            } else if($child->calculation->id == Calculation::AVERAGE){
                                $lastAchieveValue = $achieveValues ? array_sum($achieveValues)/count($achieveValues) : 0;
                            } else if($child->calculation->id == Calculation::LAST){
                                $lastAchieveValue = end($achieveValues);
                            } else if($child->calculation->id == Calculation::MIN){
                                $lastAchieveValue = min($achieveValues);
                            } 

                            // if(Yii::$app->user->id ==487497 && $model->okr_code == '01') echo (((($lastAchieveValue / $child->target) * 100) * $child->org->weight) / 100);
                            // if(Yii::$app->user->id ==487497 && $model->okr_code == '01') echo $child->calculation->id . ' - ' . $lastAchieveValue . ' - ' . $child->target . '<br />';
                            // if(Yii::$app->user->id ==487497 && $model->okr_code == '01') echo json_encode($child->target);
                            // if(Yii::$app->user->id ==487497 && $model->okr_code == '01') var_dump($child->org->weight);
                            $finalValue = $finalValue + (((($lastAchieveValue / $child->target) * 100) * $child->org->weight) / 100);
                        }

                        // $percentage = $model->getFinalAchieveValue($year);

                        return Progress::widget([
                            'percent' => $finalValue,
                            'label' => $finalValue < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($finalValue, 2) . '%</span>' :round($finalValue, 2).'%',
                        ]);
                    }, 
                    'width' => '200px'
                ]
            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Capaian Objective Organisasi',
                'before'=>'',
                'after'=>'',
            ]
        ])?>

        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => require(__DIR__.'/_columns_organization2.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Daftar Laporan OKR Organisasi',
                'before'=>'',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<script>
    function changeYear(val) {
        var year = val;

        window.location.replace("/dashboard2/okr-org2?year=" + year);
    }
</script>