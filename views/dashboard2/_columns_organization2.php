<?php

use app\models\Activity;
use app\models\Calculation;
use app\models\Evaluation;
use app\models\OkrEvaluation;
use app\models\OkrOrgMov;
use app\models\PositionStructureEmployee;
use app\models\Status;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'attribute' => 'perspective_id',
        'label' => 'Perspektif',
        'value' => function($model){
            return $model->okr->org->perspective_id ? $model->okr->org->perspective->perspective: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\Perspective::find()->orderBy('id')->asArray()->all(), 'id', 'perspective'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
        'group' => true,
    ],
    [
        'attribute' => 'perspective_id',
        'label' => 'Bobot Perspektif',
        'value' => function($model){
            return $model->okr->org->perspective_id ? $model->okr->org->perspective->weight  . '%': '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 1,
        'visible' => Yii::$app->user->can('Admin')
    ],
    [
        'attribute' => 'parent_okr_code',
        'label' => 'Objective',
        'format' => 'raw',
        'value' => function($model){
            return '<b>' . $model->okr->parent->okr_code . '</b><br />' . $model->okr->parent->okr;
        },
        'group' => true,
        'subGroupOf' => 1,
        'width' => '250px'
    ],
    [
        'attribute' => 'okr_code',
        'label' => 'Key Result',
        'format' => 'raw',
        'value' => function($model){
            return '<b>' . $model->okr->okr_code . '</b><br />' . $model->okr->okr;
        },
        'group' => true,
        'subGroupOf' => 3,
        'width' => '250px'
    ],
    [
        'attribute' => 'weight',
        'label' => 'Bobot Key Result',
        'value' => function($model){
            return $model->okr->org->weight ? $model->okr->org->weight . '%': '';
        },
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
        'visible' => Yii::$app->user->can('Admin')
    ],
    /* [
        'label' => 'Rencana Anggaran',
        'value' => function($model){
            return $model->achieved->budget_plan ? number_format($model->achieved->budget_plan, 2, ',', '.') . '%': '-';
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Realisasi Anggaran',
        'value' => function($model){
            return $model->achieved->budget_real ? number_format($model->achieved->budget_real, 2, ',', '.') . '%': '-';
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ], */
    [
        'attribute' => 'portofolio_type',
        'label' => 'Jenis Portofolio',
        'value' => function($model){
            return $model->okr->org->portfolio_type_id ? $model->okr->org->portfolio->portfolio_type: '';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\PortfolioType::find()->orderBy('id')->asArray()->all(), 'id', 'portfolio_type'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
        'group' => true,
        'subGroupOf' => 4,
    ],
    [
        'attribute' => 'target',
        'label' => 'Target',
        'value' => function($model){
            return $model->okr->target ? number_format($model->okr->target, 2, ',', '.') . ' ' . $model->okr->measure: '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
    ],
    /* [
        'attribute' => 'target',
        'label' => 'Kuartal 1',
        'value' => function($model){
            return $model->okr->target_q1 ? $model->okr->target_q1 . $model->okr->measure: '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
    ],
    [
        'attribute' => 'target',
        'label' => 'Kuartal 2',
        'value' => function($model){
            return $model->okr->target_q2 ? $model->okr->target_q2 . $model->okr->measure: '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
    ],
    [
        'attribute' => 'target',
        'label' => 'Kuartal 3',
        'value' => function($model){
            return $model->okr->target_q3 ? $model->okr->target_q3 . $model->okr->measure: '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
    ],
    [
        'attribute' => 'target',
        'label' => 'Kuartal 4',
        'value' => function($model){
            return $model->okr->target_q4 ? $model->okr->target_q4 . $model->okr->measure: '-';
        },
        'filter' => false,
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 4,
    ], */
    [
        'label' => 'Departemen',
        'attribute' => 'unit_structure_id',
        'format' => 'raw',
        'value' => function($model){
            return $model->unit_structure_id ? $model->unit->unit_name : '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(\app\models\UnitStructure::find()->where(['year' => (Yii::$app->getRequest()->getQueryParam('year') ?: date('Y'))])->andWhere(['level' => 2])->orderBy('unit_name')->asArray()->all(), 'id', 'unit_name'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
        'width' => '150px',
        'group' => true,
        'subGroupOf' => 4,
    ],
    /* [
        'attribute' => 'weight',
        'label' => 'Bobot Kontribusi',
        'value' => function($model){
            $okrEvaluation = OkrEvaluation::findOne(['okr_id' => $model->okr_id, 'unit_structure_id' => $model->unit_structure_id]);
            return $model->weight ? number_format($okrEvaluation->weight, 2, ',', '.') . '%': '-';
        },
        'hAlign' => 'center',
        'group' => true,
        'subGroupOf' => 8,
        'visible' => Yii::$app->user->can('Admin')
    ], */
    [
        'label' => 'Target K1',
        'value' => function($model){
            return $model->okr->target_q1 ? number_format($model->okr->target_q1, 2, ',', '.') . ' ' . $model->okr->measure: '-';
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Realisasi K1',
        'value' => function($model){
            if($model->okr_id == 21501){
                $realisasi = (8440503340 + 22714073479);
                return number_format($realisasi, 2, ',', '.')  . ' ' . $model->okr->measure;
            } else {
                $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                // if(Yii::$app->user->can('SuperUser')) var_dump($model->okr_id);
                return number_format($model->getAchievedByPeriod($year, 'Kuartal 1', $model->unit_structure_id), 2, ',', '.') . ' ' . $model->okr->measure;
            }
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Target K2',
        'value' => function($model){
            return $model->okr->target_q2 ? number_format($model->okr->target_q2, 2, ',', '.') . ' ' . $model->okr->measure: '-';
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Realisasi K2',
        'value' => function($model){
            if($model->okr_id == 21501){
                $realisasi = (59269152809 + 14618217275);
                return number_format($realisasi, 2, ',', '.')  . ' ' . $model->okr->measure;
            } else {
                $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                return number_format($model->getAchievedByPeriod($year, 'Kuartal 2', $model->unit_structure_id), 2, ',', '.') . ' ' . $model->okr->measure;
            }
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Target K3',
        'value' => function($model){
            return $model->okr->target_q3 ? number_format($model->okr->target_q3, 2, ',', '.') . ' ' . $model->okr->measure: '-';
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Realisasi K3',
        'value' => function($model){
            $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
            return number_format($model->getAchievedByPeriod($year, 'Kuartal 3', $model->unit_structure_id), 2, ',', '.') . ' ' . $model->okr->measure;
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Target K4',
        'value' => function($model){
            return $model->okr->target_q4 ? number_format($model->okr->target_q4, 2, ',', '.') . ' ' . $model->okr->measure: '-';
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Realisasi K4',
        'value' => function($model){
            $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
            return number_format($model->getAchievedByPeriod($year, 'Kuartal 4', $model->unit_structure_id), 2, ',', '.') . ' ' . $model->okr->measure;
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Hasil Akhir',
        'value' => function($model){
            if($model->okr_id == 21501){
                $realisasi = (8440503340 + 22714073479) + (59269152809 + 14618217275);
                return number_format($realisasi, 2, ',', '.')  . ' ' . $model->okr->measure;
            } else {
                // var_dump($model->okr_id);
                $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                // return $model->getLastAchievedByPeriod($year, $model->unit_structure_id);
                $achieveValues = [];

                $evaluationK1 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 1']);
                if($evaluationK1){
                    array_push($achieveValues, $model->getAchievedByPeriod($year, 'Kuartal 1', $model->unit_structure_id));
                }

                $evaluationK2 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 2']);
                if($evaluationK2){
                    array_push($achieveValues, $model->getAchievedByPeriod($year, 'Kuartal 2', $model->unit_structure_id));
                }
                
                $evaluationK3 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 3']);
                if($evaluationK3){
                    array_push($achieveValues, $model->getAchievedByPeriod($year, 'Kuartal 3', $model->unit_structure_id));
                }
                
                $evaluationK4 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 4']);
                if($evaluationK4){
                    array_push($achieveValues, $model->getAchievedByPeriod($year, 'Kuartal 4', $model->unit_structure_id));
                }

                
                if($model->okr->calculation->id == Calculation::SUM){
                    $lastAchieveValue = array_sum($achieveValues);
                } else if($model->okr->calculation->id == Calculation::AVERAGE){
                    $lastAchieveValue = $achieveValues ? array_sum($achieveValues)/count($achieveValues) : 0;
                } else if($model->okr->calculation->id == Calculation::LAST){
                    $lastAchieveValue = end($achieveValues);
                } else if($model->okr->calculation->id == Calculation::MIN){
                    $lastAchieveValue = min($achieveValues);
                }

                // var_dump($achieveValues);

                // $lastAchieveValue = (float) str_replace('%', '', $lastAchieveValue);
                return is_numeric($lastAchieveValue) ? number_format($lastAchieveValue, 2, ',', '.') . ' ' . $model->okr->measure . ' (' . ($model->okr->calculation->calculation_type ?: '-') . ')' : $lastAchieveValue . ' (' . ($model->okr->calculation->calculation_type ?: '-') . ')' ;
            }
        },
        'group' => true,
        'subGroupOf' => 4,
        'hAlign' => 'center',
    ],
    [
        'label' => 'Keluaran Yang Dihasilkan',
        'value' => function($model){
            return $model->output ?: '-';
        },
    ],
    /* [
        'label' => 'Catatan KR',
        'value' => function($model){
            return $model->note ?: '-';
        }
    ], */
    /* [
        'label' => 'Analisis Capaian',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->achieved->performance_analysis ?: '-') .
                            '</td>
                            <td width="10px">' .
                            (Yii::$app->user->can('Admin') ?  Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['performance-analysis', 'id' => $model->okr_id, 'evaluation_id' => $model->evaluation_id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Analisis Capaian',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
    ], */
    /* [
        'label' => 'Interpretasi',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->achieved->interpretation ?: '-') .
                            '</td>
                            <td width="10px">' .
                            (Yii::$app->user->can('Admin') ?  Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['interpretation', 'id' => $model->okr_id, 'evaluation_id' => $model->evaluation_id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Interpretasi',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
    ], */
];
