<?php

use kartik\editors\Summernote;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-evaluation-form">

    <?php $form = ActiveForm::begin(); ?>
  
	<?= $form->field($model, 'interpretation')->dropDownList([
																'Istimewa' => 'Istimewa', 
																'Sangat Baik' => 'Sangat Baik',
																'Baik' => 'Baik',
																'Cukup' => 'Cukup',
																'Kurang' => 'Kurang',
																'Buruk' => 'Buruk',
																], ['class' => 'form-control']) ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<script>
$('#ajaxCrudModal').on('shown.bs.modal', function() {
  $('#activityevaluation-actual_note').summernote();
})
</script>