<?php

use app\models\Calculation;
use app\models\Evaluation;
use app\models\Okr;
use app\models\OkrEvaluation;
use app\models\OkrOrg;
use kartik\grid\GridView;
use yii\bootstrap\Progress;
use yii\data\ActiveDataProvider;

?>
<div>
    <div class="box box-default">
        <div class="box-body">
            <?= GridView::widget([
                'id'=>'crud-datatable-objective',
                'dataProvider' => new ActiveDataProvider(['query' => Okr::find()->where(['parent_id' => $model->id])->andWhere(['type' => 'KR'])->orderBy('okr_code')]),
                'filterSelector' => '.additional-filter',
                'pjax'=>true,
                'summary' => false,
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'width' => '30px',
                    ],
                    [
                        'attribute' => 'id',
                        'visible' => Yii::$app->user->can('SuperUser')
                    ],
                    [
                        'attribute' => 'okr_code',
                    ],
                    [
                        'attribute' => 'okr',
                        'label' => 'Key Result'
                    ],
                    [
                        'label' => 'Bobot',
                        'value' => function($model){
                            // var_dump($model->id);
                            return $model->org->weight;
                        }
                    ],
                    [
                        'label' => 'Capaian Kuartal 1',
                        'format' => 'raw',
                        'value' => function($model){
                            // if(Yii::$app->user->can('SuperUser')) var_dump($model->id);
                            $percentage = $model->getKrAchieveValue($model->year, 'Kuartal 1');

                            // return Progress::widget([
                            //     'percent' => $percentage,
                            //     'label' => round($percentage, 2).'%',
                            // ])/* .$real.'/'.$target.' '.$measure */;
                            return round($percentage, 2);
                        }, 
                        'width' => '200px'
                    ],
                    [
                        'label' => 'Capaian Kuartal 2',
                        'format' => 'raw',
                        'value' => function($model){
                            // if(Yii::$app->user->can('SuperUser')) var_dump($model->id);
                            $percentage = $model->getKrAchieveValue($model->year, 'Kuartal 2');

                            // return Progress::widget([
                            //     'percent' => $percentage,
                            //     'label' => round($percentage, 2).'%',
                            // ])/* .$real.'/'.$target.' '.$measure */;
                            return round($percentage, 2);
                        }, 
                        'width' => '200px'
                    ],
                    [
                        'label' => 'Capaian Kuartal 3',
                        'format' => 'raw',
                        'value' => function($model){
                            $percentage = $model->getKrAchieveValue($model->year, 'Kuartal 3');

                            // return Progress::widget([
                            //     'percent' => $percentage,
                            //     'label' => round($percentage, 2).'%',
                            // ])/* .$real.'/'.$target.' '.$measure */;
                            return round($percentage, 2);
                        }, 
                        'width' => '200px'
                    ],
                    [
                        'label' => 'Capaian Kuartal 4',
                        'format' => 'raw',
                        'value' => function($model){
                            $percentage = $model->getKrAchieveValue($model->year, 'Kuartal 4');

                            // return Progress::widget([
                            //     'percent' => $percentage,
                            //     'label' => round($percentage, 2).'%',
                            // ])/* .$real.'/'.$target.' '.$measure */;
                            return round($percentage, 2);
                        }, 
                        'width' => '200px'
                    ],
                    [
                        'label' => 'Hasil Akhir',
                        'format' => 'raw',
                        'value' => function($model){

                            if($model->id == 21501){

                                //              REALISASI K1                REALISASI K2
                                $realisasi = (8440503340 + 22714073479) + (59269152809 + 14618217275);
                                //        TARGET K1     TARGET K2    
                                // $target = 72441365000 + 114833051667;
                                $target = 325105000000;
                                
                                // TOTAL REALISASI / TARGET * 100
                                return round((($realisasi / $target) * 100), 0) . '%';
                            } else {
                                $achieveValues = [];
    
                                $evaluationK1 = Evaluation::findOne(['year' => $model->year, 'period' => 'Kuartal 1']);
    
                                if($evaluationK1){
                                    $okrAchievements = OkrEvaluation::find()
                                                                        ->where(['evaluation_id' => $evaluationK1->id])
                                                                        ->andWhere(['okr_id' => $model->id])
                                                                        ->all();
    
                                    $achieve = 0;
                                    foreach($okrAchievements as $okrAchievement){
                                        $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                                    }
                                    
                                    if($achieve) array_push($achieveValues, $achieve?:0);
                                } else {
                                    // array_push($achieveValues, 0);
                                }
    
                                $evaluationK2 = Evaluation::findOne(['year' => $model->year, 'period' => 'Kuartal 2']);
    
                                if($evaluationK2){
                                    $okrAchievements = OkrEvaluation::find()
                                                                        ->where(['evaluation_id' => $evaluationK2->id])
                                                                        ->andWhere(['okr_id' => $model->id])
                                                                        ->all();
    
                                    $achieve = 0;
                                    foreach($okrAchievements as $okrAchievement){
                                        $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                                    }
                                    
                                    if($achieve) array_push($achieveValues, $achieve?:0);
                                } else {
                                    // array_push($achieveValues, 0);
                                }
    
                                $evaluationK3 = Evaluation::findOne(['year' => $model->year, 'period' => 'Kuartal 3']);
    
                                if($evaluationK3){
                                    $okrAchievements = OkrEvaluation::find()
                                                                        ->where(['evaluation_id' => $evaluationK3->id])
                                                                        ->andWhere(['okr_id' => $model->id])
                                                                        ->all();
    
                                    $achieve = 0;
                                    foreach($okrAchievements as $okrAchievement){
                                        $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                                    }
                                    
                                    if($achieve) array_push($achieveValues, $achieve?:0);
                                } else {
                                    // array_push($achieveValues, 0);
                                }
    
                                $evaluationK4 = Evaluation::findOne(['year' => $model->year, 'period' => 'Kuartal 4']);
    
                                if($evaluationK4){
                                    $okrAchievements = OkrEvaluation::find()
                                                                        ->where(['evaluation_id' => $evaluationK4->id])
                                                                        ->andWhere(['okr_id' => $model->id])
                                                                        ->all();
    
                                    $achieve = 0;
                                    foreach($okrAchievements as $okrAchievement){
                                        $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
                                    }
                                    
                                    if($achieve) array_push($achieveValues, $achieve?:0);
                                } else {
                                    // array_push($achieveValues, 0);
                                }
    
                                if($model->id == 21487) var_dump($achieveValues);
                                if($model->calculation->id == Calculation::SUM){
                                    $calculate = 'Sum';
                                    $lastAchieveValue = array_sum($achieveValues);
                                } else if($model->calculation->id == Calculation::AVERAGE){
                                    $calculate = 'Avg';
                                    $lastAchieveValue = $achieveValues ? array_sum($achieveValues)/count($achieveValues) : 0;
                                } else if($model->calculation->id == Calculation::LAST){
                                    $lastAchieveValue = end($achieveValues);
                                    $calculate = 'Last';
                                } else if($model->calculation->id == Calculation::MIN){
                                    $lastAchieveValue = min($achieveValues);
                                    $calculate = 'Min';
                                } 
    
                                return round(($lastAchieveValue / $model->target) * 100, 2) . '% (' . $calculate . ')';
                            }


                            // var_dump($model->id);

                            // $percentage = $model->getLastAchievedByPeriod($model->year);

                            // return Progress::widget([
                            //     'percent' => $percentage,
                            //     'label' => round($percentage, 2).'%',
                            // ])/* .$real.'/'.$target.' '.$measure */;
                            // return $percentage;
                        }, 
                        'width' => '200px'
                    ]
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => false
            ])?>
        </div>
    </div>
</div>