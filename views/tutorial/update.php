<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tutorial */
?>
<div class="tutorial-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
