<?php

use app\models\Activity;
use app\models\Budget;
use app\models\Ceiling;
use app\models\Initiatives;
use app\models\Okr;
use dosamigos\highcharts\HighCharts;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\JsExpression;

$this->title = 'Dashboard';
?>
<style>
    .box-dashboard {
        border: 1px solid var(--background);
        border-radius: 5px;
        padding: 5px; text-align: center; margin: 1px auto;
    }
</style>
<div class="dashboard-page">

    <div class="box box-default">
        <div class="box-body">

            <div class="row">
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <label style="color: var(--borderColor);font-size: 17px;">Filter Tahun</label>
                        <?= DatePicker::widget([
                            'name' => 'year-filter',
                            'value' => $yearFilter,
                            'options' => [
                                'placeholder' => 'Filter Tahun',
                                'onchange' => 'changeYear($(this).val())',
                            ],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'startView'=>'year',
                                'minViewMode'=>'years',
                                'format' => 'yyyy',
                                'allowClear' => false
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
            
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;"><?= $countObjectivesOrg; ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;">Objectives Organisasi</b>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;"><?= $countKeyResultsOrg; ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;">Key Result Organisasi</b>
                    </div>
                </div>
            </div>
            
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;"><?= $countObjectives; ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;">Total Objectives</b>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;"><?= $countKeyResults; ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;">Total Key Result</b>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;"><?= $countInitiatives; ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;">Program Kerja</b>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;"><?= $countActivity; ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;">Kegiatan</b>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;"><?= $countBudget; ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;">Item Anggaran</b>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;">Rp <?= number_format($sumBudget, 0, ",", "."); ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;">Total Anggaran</b>
                    </div>
                </div>
            </div>

            <!-- // OKR SUMMARY -->
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-12">
                    <div class="box-dashboard" style="text-align: left;">
                        <?= GridView::widget([
                            'id'=>'okr-summary',
                            'dataProvider' => $dataProviderUnit,
                            'filterModel' => $searchModelUnit,
                            'layout' => '{items}{pager}',
                            'pjax'=>true,
                            'hover'=>true,
                            'export' => [
                                'label' => 'Export Excel atau PDF',
                                'header' => '',
                                'options' => [
                                    'class' => 'btn btn-primary'
                                ], 
                                'menuOptions' => [ 
                                        'style' => 'background: var(--background); width: 187px;'
                                ]
                            ],
                            'exportConfig' => [
                                GridView::PDF => true,
                                GridView::EXCEL => true,
                            ],
                            'emptyText' => 'Tidak ada data Unit.',
                            'tableOptions' => [
                                'class' => 'table table-responsive table-striped table-bordered'
                            ],
                            'showPageSummary' => true,
                            'columns' => [
                                [
                                    'class' => 'kartik\grid\SerialColumn',
                                    'width' => '30px',
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'attribute' => 'unit_code',
                                    'value' => function($model){
                                        return $model->unit_code ?: '-';
                                    }
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'attribute' => 'unit_name',
                                    'value' => function($model){
                                        return $model->unit_name ?: '-';
                                    },
                                    'pageSummary' => 'Total',
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Jumlah Objective',
                                    'value' => function($model){
                                        $count = Okr::find()->alias('o')->joinWith('position p')->where(['p.unit_structure_id' => $model->id])->andWhere(['o.type' => 'O'])->count();
                                        return number_format($count ?: 0, 0, ",", ".");
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Jumlah Key Result',
                                    'value' => function($model){
                                        $count = Okr::find()->alias('o')->joinWith('position p')->where(['p.unit_structure_id' => $model->id])->andWhere(['o.type' => 'KR'])->count();
                                        return number_format($count ?: 0, 0, ",", ".");
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ]
                            ],
                            'toolbar'=> [
                                    ['content'=>
                                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                                        '{toggleData}'
                                    ],
                                    ['content'=>
                                        '{export}'
                                    ],
                                ],
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'panel' => [
                                    'type' => 'primary',
                                    'heading' => '<i class="fa fa-list-alt"></i> OKR Summary',
                                    'before' => '',
                                    'after'=>'',
                                ]
                        ]) ?>
                    </div>
                </div>
            </div>

            <!-- // INITIATIVE SUMMARY -->
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-12">
                    <div class="box-dashboard" style="text-align: left;">
                        <?= GridView::widget([
                            'id'=>'initiatives-summary',
                            'dataProvider' => $dataProviderUnit,
                            'filterModel' => $searchModelUnit,
                            'layout' => '{items}{pager}',
                            'pjax'=>true,
                            'hover'=>true,
                            'export' => [
                                'label' => 'Export Excel atau PDF',
                                'header' => '',
                                'options' => [
                                    'class' => 'btn btn-primary'
                                ], 
                                'menuOptions' => [ 
                                        'style' => 'background: var(--background); width: 187px;'
                                ]
                            ],
                            'exportConfig' => [
                                GridView::PDF => true,
                                GridView::EXCEL => true,
                            ],
                            'emptyText' => 'Tidak ada data Unit.',
                            'tableOptions' => [
                                'class' => 'table table-responsive table-striped table-bordered'
                            ],
                            'showPageSummary' => true,
                            'columns' => [
                                [
                                    'class' => 'kartik\grid\SerialColumn',
                                    'width' => '30px',
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'attribute' => 'unit_code',
                                    'value' => function($model){
                                        return $model->unit_code ?: '-';
                                    }
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'attribute' => 'unit_name',
                                    'value' => function($model){
                                        return $model->unit_name ?: '-';
                                    },
                                    'pageSummary' => 'Total',
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Jumlah Proker',
                                    'value' => function($model){
                                        $count = Initiatives::find()
                                                                ->alias('i')
                                                                ->joinWith('position p')
                                                                ->where(['p.unit_structure_id' => $model->id])
                                                                ->count();
                                        return number_format($count ?: 0, 0, ",", ".");
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Jumlah Kegiatan',
                                    'value' => function($model){
                                        $count = Activity::find()
                                                                ->alias('a')
                                                                ->joinWith('position p')
                                                                ->where(['p.unit_structure_id' => $model->id])
                                                                ->count();
                                        return number_format($count ?: 0, 0, ",", ".");
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Jumlah Item Anggaran',
                                    'value' => function($model){
                                        $count = Budget::find()
                                                            ->alias('b')
                                                            ->joinWith(['activity a', 'activity.position p'])
                                                            ->where(['p.unit_structure_id' => $model->id])
                                                            ->count();
                                        return number_format($count ?: 0, 0, ",", ".");
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Pagu Anggaran',
                                    'format' => 'currency',
                                    'value' => function($model){
                                        $ceiling = Ceiling::findOne(['unit_structure_id' => $model->id])->ceiling_amount;
                                        return $ceiling ?: 0;
                                        // return number_format($ceiling ?: 0, 0, ",", ".");
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Total Anggaran',
                                    'format' => 'currency',
                                    'value' => function($model){
                                        $sum = Budget::find()
                                                            ->alias('b')
                                                            ->joinWith(['activity a', 'activity.position p'])
                                                            ->where(['p.unit_structure_id' => $model->id])
                                                            ->sum('total_amount');
                                        return $sum ?: 0;
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ],
                            ],
                            'toolbar'=> [
                                    ['content'=>
                                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                                        '{toggleData}'
                                    ],
                                    ['content'=>
                                        '{export}'
                                    ],
                                ],
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'panel' => [
                                    'type' => 'primary',
                                    'heading' => '<i class="fa fa-list-alt"></i> Initiatives Summary',
                                    'before' => '',
                                    'after'=>'',
                                ]
                        ]) ?>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="col-md-6">
                    <div class="box-dashboard" style="text-align: left;">
                        <b style="color: var(--borderColor);font-size: 17px;">Komposisi Anggaran Partisipatori</b><br />
                        
                        <?php 
                            $valPar = [];
                            foreach($arrParticipatory as $ap){
                                array_push($valPar, (float) $ap['val']);
                            }; 
                        ?>

                        <?= HighCharts::widget([
                            'clientOptions' => [
                                'chart' => [
                                    'height' => '400px',
                                    'type' => ['bar']
                                ],
                                'title' => [
                                    'text' => "Anggaran Partisipatori {$yearFilter}"
                                ],
                                'xAxis' => [
                                    'categories' => $arrParticipatoryValue
                                ],
                                'yAxis' => [
                                    'title' => [
                                        'text' => 'Persentase'
                                    ],
                                    'tickInterval' => 10000000,
                                    'labels' => [
                                        'format' => '{text}' // The $ is literally a dollar unit
                                    ],
                                ],
                                'credits' => [
                                    'enabled' => false
                                ],
                                'series' => [
                                    [
                                        'name' => 'Jenis Anggaran Participatory', 
                                        'data' => $valPar,
                                    ]
                                ],
                                'colors' => ['rgb(111, 68, 137)'],
                                'tooltip' => [
                                    'formatter' => new JsExpression('function() {
                                        var total = ' . $totalAllParticipatory . ';
                                        var percent = parseFloat((this.y / total) * 100).toFixed(2);
                                        tooltip = this.x + ": " + percent + "% (Rp " + this.y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ")";
                                        return tooltip;
                                    }')
                                ],
                                'plotOptions' => [
                                    'bar' => [
                                        'dataLabels' => [
                                            'enabled' => true,
                                            'formatter' => new JsExpression('function() {
                                                var total = ' . $totalAllParticipatory . ';
                                                var percent = parseFloat((this.y / total) * 100).toFixed(2);
                                                persentase = percent + "%";
                                                return persentase;
                                            }')
                                        ]
                                    ]
                                ],
                            ]
                        ]); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-dashboard" style="text-align: left;">
                        <b style="color: var(--borderColor);font-size: 17px;">Jenis Pembiayaan</b><br />
                        
                        <?php 
                            $valFin = [];
                            foreach($arrFinancing as $af){
                                array_push($valFin, $af['val']);
                            }; 
                        ?>

                        <?= HighCharts::widget([
                            'clientOptions' => [
                                'chart' => [
                                    'height' => '400px',
                                    'type' => ['bar']
                                ],
                                'title' => [
                                    'text' => "Jenis Pembiayaan {$yearFilter}"
                                ],
                                'xAxis' => [
                                    'categories' => $arrFinancingValue
                                ],
                                'yAxis' => [
                                    'title' => [
                                        'text' => 'Persentase'
                                    ],
                                    'tickInterval' => 10000000,
                                    'labels' => [
                                        'format' => '{text}' // The $ is literally a dollar unit
                                    ],
                                ],
                                'credits' => [
                                    'enabled' => false
                                ],
                                'series' => [
                                    [
                                        'name' => 'Jenis Pembiayaan', 
                                        'data' => $valFin,
                                    ]
                                ],
                                'colors' => ['rgb(111, 68, 137)'],
                                'tooltip' => [
                                    'formatter' => new JsExpression('function() {
                                        var total = ' . $totalAllFinancing . ';
                                        var percent = parseFloat((this.y / total) * 100).toFixed(2);
                                        tooltip = this.x + ": " + percent + "% (Rp " + this.y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ")";
                                        return tooltip;
                                    }')
                                ],
                                'plotOptions' => [
                                    'bar' => [
                                        'dataLabels' => [
                                            'enabled' => true,
                                            'formatter' => new JsExpression('function() {
                                                var total = ' . $totalAllFinancing . ';
                                                var percent = parseFloat((this.y / total) * 100).toFixed(2);
                                                persentase = percent + "%";
                                                return persentase;
                                            }')
                                        ]
                                    ]
                                ],
                                
                                /* 'legend' => [
                                    'layout' => 'vertical',
                                    'align' => 'right',
                                    'verticalAlign' => 'top',
                                    'x' => -40,
                                    'y' => 80,
                                    'floating' => true,
                                    'borderWidth' => 1,
                                    'backgroundColor' =>
                                        '#FFFFFF',
                                    'shadow' => true
                                ], */
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    function changeYear(val) {
        var year = val;

        window.location.replace("/site/dashboard?yearFilter=" + year);
    }
</script>