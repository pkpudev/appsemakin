<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this common\components\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model common\models\LoginForm */

$this->title = 'SEMAKIN - Login';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='fa fa-user form-control-feedback''></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='fa fa-lock form-control-feedback''></span>"
];
?>

<div class="row">
    <!-- <div class="col-md-8"></div> -->
    <div class="col-md-12">
        <div class="box box-warning login-box" style="background: var(--backgroundGradient) !important;">
            <div class="box-body text-center">

                <div class="user-icon">
                    <span class="fa fa-users"></span>
                </div>

                <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

                    <?= $form
                        ->field($model, 'username', $fieldOptions1)
                        ->label(false)
                        ->textInput(['placeholder' => $model->getAttributeLabel('username'), 'style' => 'border-radius: 5px; color: #e08e0b;']) ?>

                    <?= $form
                        ->field($model, 'password', $fieldOptions2)
                        ->label(false)
                        ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'style' => 'border-radius: 5px; color: #e08e0b;']) ?>


                    <?= Html::submitButton(
                        '<i class="fa fa-sign-in"></i>&nbsp;&nbsp;Masuk',
                        [
                            'class' => 'btn btn-warning btn-block btn-flat',
                            'name' => 'login-button',
                            'style' => 'border-radius: 5px;'
                        ]
                    ) ?>

                <?php ActiveForm::end(); ?>

                <hr />
                
                <b>Our Page: </b><br />
                <a href="https://human-initiative.org/" target="_blank" title="Home"><span class="fa fa-home"></span></a>
                <a href="https://www.facebook.com/humaninitiative.id/" target="_blank" title="Facebook"><span class="fa fa-facebook"></span></a>
                <a href="https://www.instagram.com/humaninitiative_id/" target="_blank" title="Instagram"><span class="fa fa-instagram"></span></a>
                <a href="https://twitter.com/hum_initiative" target="_blank" title="Twitter"><span class="fa fa-twitter"></span></a>
                <a href="https://www.youtube.com/user/HumanityCalling/videos" target="_blank" title="Youtube"><span class="fa fa-youtube"></span></a>
                
                <hr />

                <a href="#"><span class="fa fa-users"></span> SemakinApp</a>

            </div>
        </div>
    </div>
</div>

