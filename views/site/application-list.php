<?php

use yii\helpers\Html;

?>
<style>
    .application-item {
        width: 25%;
        float: left;
        padding: 5px;
    }

    .item {
        padding: 15px;
        background: #eee;
        margin: 5px;
        border: 1px solid #999;
    }

    .info-box-content {
        padding: 15px 10px;
        margin-left: 50px;
    }

    .info-box-icon {
        height: 50px;
        width: 50px;
        line-height: 49px;-color: rgba(0, 0, 0, 0.2);
        background: var(--background);
        color: #fff;
        font-size: 25px;
    }

    .info-box {
        min-height: 50px;
        border: 1px solid var(--background);
    }

    .info-box a {
        color:  var(--background);
    }

    @media (max-width: 640px) {
        .application-item {
            width: 100%;
        }
    }

    .dark-mode .info-box a {
        color:  #fff;
    }
</style>
<div class="application-list" style="display: inline-block;">

    <?php foreach($applications as $application): ?>
        <div class="application-item">
            <div class="info-box">
                <span class="info-box-icon">
                    <i class="fa <?= $application->icon ?: 'fa-file'; ?>"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">
                        <strong>
                            <?= Html::a($application->application_name, $application->url, ['target' => '_blank']); ?>
                        </strong>
                    </span>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

</div>