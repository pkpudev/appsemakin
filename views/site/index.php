<?php

/* @var $this yii\web\View */

use app\components\DateHelper;
use app\models\Evaluation;
use app\models\Status;
use app\models\VwEmployeeManager;
use yii\helpers\Html;

$this->title = 'Homepage';
?>

<style>
    .box-custom {
        border: 1px solid #fff;
        border-radius: 5px;
        padding: 5px; text-align: center; margin: 1px auto;
    }
</style>

<div class="site-index">

    <div class="box box-primary">
        <div class="box-body">
            <div class="jumbotron text-center bg-transparent">
                <h2><span class="fa fa-cube"></span><br />Selamat Datang!</h2>

                <p class="lead">Aplikasi ini sedang dalam proses pengembangan, jadi ada beberapa <i>bug</i> yang mungkin ditemukan.<br />
                Silahkan hubungi Tim IT jika menemukan <i>bug</i> dalam aplikasi ini.</p>
            </div>
        </div>
    </div>

    <?php 
        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one() || Yii::$app->user->can('Admin');
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one() || Yii::$app->user->can('Admin');

        $evaluation = Evaluation::findOne(['status_id' => Status::GENERATE_PROSES_2]);

        $startDate = $evaluation->start_date;
        $finishDate = $evaluation->end_date;
        $currentDate = date('Y-m-d');
        
        if((strtotime($currentDate) >= strtotime($startDate)) && (strtotime($currentDate) <= strtotime($finishDate))){
            $canFillEvaluation = true;
        } else {
            $canFillEvaluation = false;
        }

        if($evaluation && $canFillEvaluation):
    ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default" style="background-color: var(--background) !important; color: #fff !important;">
                    <div class="box-body">
                        <h4>Terdapat Evaluasi Kinerja Tahun <?= $evaluation->year; ?> Periode <?= $evaluation->period; ?> yang bisa kamu isi sampai dengan <?= DateHelper::getTglIndo($evaluation->end_date); ?>, silahkan klik link dibawah ini untuk mulai mengisinya.</h4>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="box-custom">
                                    <?= Html::a('Kegiatan', ['/evaluation/activity/index'], ['style' => 'color: #fff !important; font-weight: bold; font-size: 18px;']); ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="box-custom">
                                    <?= Html::a('OKR Individu', ['/evaluation/okr/individual'], ['style' => 'color: #fff !important; font-weight: bold; font-size: 18px;']); ?>
                                </div>
                            </div>
                            <?php if($isManager || $isGeneralManager): ?>
                            <div class="col-md-3">
                                <div class="box-custom">
                                    <?= Html::a('OKR Unit', ['/evaluation/okr/unit'], ['style' => 'color: #fff !important; font-weight: bold; font-size: 18px;']); ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="box-custom">
                                    <?= Html::a('Program Kerja', ['/evaluation/initiatives/index'], ['style' => 'color: #fff !important; font-weight: bold; font-size: 18px;']); ?>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php endif; ?>
    
</div>
