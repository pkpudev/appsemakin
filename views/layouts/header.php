<?php

use yii\bootstrap\Nav;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$logoMini = Html::tag('span', 'HI', ['class' => 'logo-mini']);
$logoLg = Html::tag('span', 'Front End', ['class' => 'logo-lg']); ?>

<header class="main-header" id="myHeader">
    <nav class="navbar navbar-default">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">MegaMenu</a>
    </div>


    <div class="collapse navbar-collapse js-navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="dropdown mega-dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Payroll <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>

            <ul class="dropdown-menu mega-dropdown-menu row">
                <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header"><span class="fa fa-gift"></span>Klaim</li>
                        <li><a href="#">Input Klaim</a></li>
                        <li><a href="#">Daftar Klaim</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Review Klaim</a></li>
                        <li><a href="#">Approval Klaim</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Input Plafon Klaim</a></li>
                        <li><a href="#">Daftar Plafon Klaim</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Laporan Klaim</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Input Batch Klaim</a></li>
                        <li><a href="#">Daftar Batch Klaim</a></li>
                    </ul>
                </li>
                <li class="col-sm-3">
                    <ul>
                        <li class="dropdown-header">BPJS</li>
                        <li><a href="#">Input BPJS</a></li>
                        <li><a href="#">Daftar BPJS</a></li>
                        <li class="dropdown-header">Koperasi</li>
                        <li><a href="#">Input Koperasi</a></li>
                        <li><a href="#">Daftar Koperasi</a></li>
                        <li class="dropdown-header">Rekening Karyawan</li>
                        <li><a href="#">Input Rekening Karyawan</a></li>
                        <li><a href="#">Daftar Rekening Karyawan</a></li>
                    </ul>
                </li>
            </ul>

            </li>
        </ul>

    </div>
    <!-- /.nav-collapse -->
  </nav>
</header>