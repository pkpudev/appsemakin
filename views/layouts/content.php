<?php

use app\components\PhotosHelper;
use app\models\DonationStatus;
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;
use yii\helpers\Html;
// use yii\httpclient\Client;

?>
<style>
.button {
  position: relative;
  width: 90%;
  max-width: 800px;
  margin: -10px auto;
  padding: 10px 35px;
  outline: none;
  text-decoration: none;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  text-transform: uppercase;
  background-color: var(--success);
  border: 1px solid var(--success);
  border-radius: 10px;
  color: #FFF;
  font-weight: 400;
  font-family: inherit;
  z-index: 0;
  overflow: hidden;
  transition: all 0.3s cubic-bezier(0.02, 0.01, 0.47, 1);
}

.button span {
  color: #164ca7;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0.7px;
}

.button:hover span {
  animation: storm1261 0.7s ease-in-out both;
  animation-delay: 0.06s;
}

@keyframes storm1261 {
  0% {
    transform: translate3d(0, 0, 0) translateZ(0);
  }

  25% {
    transform: translate3d(4px, 0, 0) translateZ(0);
  }

  50% {
    transform: translate3d(-3px, 0, 0) translateZ(0);
  }

  75% {
    transform: translate3d(2px, 0, 0) translateZ(0);
  }

  100% {
    transform: translate3d(0, 0, 0) translateZ(0);
  }
}

.btn-shine {
  border: 1px solid;
  overflow: hidden;
  position: relative;
}

.btn-shine span {
  z-index: 20;
}

.btn-shine:after {
  background: #FFF;
  content: "";
  height: 355px;
  left: -295px;
  opacity: 0.4;
  position: absolute;
  top: -150px;
  transform: rotate(35deg);
  transition: all 1550ms cubic-bezier(0.19, 1, 0.22, 1);
  width: 250px;
  z-index: -10;
}

.btn-shine:hover:after {
  left: 120%;
  transition: all 1550ms cubic-bezier(0.19, 1, 0.22, 1);
}
</style>
<div class="content-wrapper">
    <section class="content-header">

        <table width="100%">
            <tr>
                <td width="60px">
                    <a href="#" class="sidebar-toggle" title="Buka/Tutup Menu" data-toggle="offcanvas" role="button" id="tog-cont" style="color: rgba(0, 0, 0, 0.6) !important; padding: 4px 7px !important; font-size: 17px;">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                </td>
                <td>
                    <div class="navbar-custom-menu pull-left" style="white-space: nowrap;">

                        <div class="tool-box pull-right">
                            <label class="switch" title="Hidupkan/Matikan Dark-Mode">
                                <input type="checkbox" id="theme">
                                <span class="slider round
                                <?= (!Yii::$app->user->can('Admin CRM') && 
                                !Yii::$app->user->can('Admin CRM Kantor') &&  
                                !Yii::$app->user->can('Admin Keuangan')) ? 'canOnlySwitch' : ''; ?>"></span>
                            </label>

                            <?php if(Yii::$app->user->can('Admin CRM') || Yii::$app->user->can('Admin CRM Kantor') ||  Yii::$app->user->can('Admin Keuangan')): ?>
                            <div class="pull-right dropdown notification">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="fa fa-bell"></span>
                                </a>
                                
                                <ul class="dropdown-menu" style="margin-top: 4px;margin-right: -10px;">
                                    <?php if(Yii::$app->user->can('Admin CRM') || Yii::$app->user->can('Admin CRM Kantor')): ?>
                                    <li>
                                        <?php/*  Html::a("Terdapat ". Yii::$app->aggregator->donasiBelumValidCrm . " donasi belum valid CRM.", 
                                            ['/donation/donation/index', 'DonationSearch[status_id]' => DonationStatus::BELUM_VALID, 'DonationSearch[keu_validated]' => DonationStatus::VALID], []); */ ?>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <?php endif; ?>
                        </div>

                        <?php if (isset($this->blocks['content-header'])) { ?>
                        <h3 style="margin-top: 15px;"><?= $this->blocks['content-header'] ?></h3>
                        <?php } else { ?>
                        <h3 style="margin-top: 15px;">
                            <?php
                                if ($this->title !== null) {
                                    echo \yii\helpers\Html::encode($this->title);
                                } else {
                                    echo \yii\helpers\Inflector::camel2words(
                                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                                    );
                                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                                } ?>
                        </h3>
                        <?php } ?>
                    </div>
                </td>
            </tr>
        </table>


        <div class="pull-right dropdown user-box" style="padding: 10px 45px 10px 10px;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <?= Yii::$app->user->identity->full_name; ?>
                
                <div class="cropcircle" style="width: 30px; height: 30px;  position: absolute; top: 5px; right: 5px; background-position: center center; background-repeat: no-repeat; background-size: cover; background-image: url('<?= PhotosHelper::getProfilePhotosUrl(Yii::$app->user->id); ?>')">
                </div>
            </a>

            <ul class="dropdown-menu">
                <?php if(!Yii::$app->user->isGuest){ ?>
                    <li class="user-header">
                        <a href="/employee/profile/" class="" style="padding-top: 150px;">
                        <center>
                        <div class="cropcircle" style="width: 150px; height: 150px; position: absolute; top: 15px; right: 85px; background-position: center center; background-repeat: no-repeat; background-size: cover; background-image: url('<?= PhotosHelper::getProfilePhotosUrl(Yii::$app->user->id); ?>')">
                        </div>
                            

                            <p style="margin-top: 10px;font-size: 18px;margin-bottom: -5px;">
                                Pengaturan Akun
                                <?php //!Yii::$app->user->isGuest ? Yii::$app->user->identity->name : "Guest"?>
                                <!-- <?php // $position = \app\models\VwPosition::find()->where(['employee_id'=>Yii::$app->user->id])->one(); ?>
                                <small><?php // ($position)?$position->position_name.' - '.$position->department:' - '?></small> -->
                            </p>
                        </center>
                        </a>
                    </li>
                    <li>
                        <?= Html::a('Sign out', ['/site/logout'], ['data-method' => 'post', 'class' => '']) ?>
                    </li>
                        
                <?php } else { ?>
                    <?= Html::a(
                        'Login',
                        ['/site/login'],
                        ['data-method' => 'post', 'class' => 'btn btn-warning btn-flat', 'style' => 'color: #fff;']
                    ) ?>
                <?php } ?>
            </ul>
        </div>
        
    </section>

    <section class="content">
        
        <?php
            $userId = Yii::$app->user->id;
            $key = Yii::$app->db->createCommand("SELECT key FROM intranet.application a where application_name = 'HRM'")->queryScalar();
            $linkApp = getenv('YII_ENV') == 'dev' ? 'intranetx' : 'intranet';

            $url = "https://$linkApp.c27g.com/api/announcement/active";
            $body = [
                'user_id' => $userId
            ];

            // $client = new Client();
            // $result = $client->createRequest()
            //     ->setMethod('POST')
            //     ->setUrl($url)
            //     ->addHeaders([
            //         'App-Key' => $key
            //     ])
            //     ->setData($body)
            //     ->send();
            
            // $response = $result->data;

            if(/* $response['status'] == 200 ||  */false):
        ?>
            <div id="popup-internal" style="position: fixed; top: 0; right: 0; width: 100%; height: 100%; z-index: 10000; background: rgba(0, 0, 0, 0.5);">
                <div style="display: block; margin: 20px auto; width: 100%; max-width: 700px; height: fit-content; border-radius: 100px 0px; border:none; background: #2387c7 !important; overflow: hidden;">
                    <center>
                        <h3 style="color: #fff;"><?= $response['data']['title'] ?></h3>
                    </center>
                    <?php if ($image = $response['data']['image']): ?>
                    <center>
                        <?= Html::a(Html::img($image, ['width'=>'60%']), $image, ['target'=>'_blank', 'data-pjax'=>0]) ?>
                        <br>
                    </center>
                    <?php endif; ?>
                    <center>
                        <p style="font-size: 12pt; color: #fff;">
                            <?= nl2br($response['data']['description']) ?>
                        </p>
                    </center>
                </div>
                <?= Html::button('<i class="fa fa-times"></i>', ['id' => 'close-popup', 'class' => 'btn btn-default btn-sm', 'style'=>'margin: -15px auto; padding: 10px 13px; outline: none; text-decoration: none; display: flex; justify-content: center; align-items: center; text-transform: uppercase; border-radius: 30px; font-weight: 400; font-family: inherit; z-index: 0; overflow: hidden; transition: all 0.3s cubic-bezier(0.02, 0.01, 0.47, 1); color: #FFF !important;', 'data-dismiss' => 'modal', 'data-popup-id' => $response['data']['id']]) ?>
            </div>
        <?php endif; ?>

        <?= Alert::widget() ?>
        <?= $content ?>

        <div class="btn-scr">
            <button id="btnScr">
                <span class="fa fa-caret-up"></span>
            </button>
        </div>

    </section>
</div>

<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class='control-sidebar-bg'></div>

<?php
$urlViewed = "https://$linkApp.c27g.com/api/announcement/save-viewer ";
$script = <<< JS

    $("#close-popup").click(function(){
      document.getElementById("popup-internal").style.display = "none";
      popUpId = $(this).data('popup-id');

      var formData = {
        user_id: '$userId',
        announcement_id: popUpId
      };

      $.ajax({
        type: "POST",
        url: "$urlViewed",
        headers: {"App-Key": "$key"},
        data: formData,
        dataType: "json",
        encode: true,
      }).done(function (data) {
        // console.log(data);
      });
    });

    $(document).scroll(function(){
        if ($(this).scrollTop() > 50 ) {
            $('.btn-scr:hidden').stop(true, true).fadeIn();
        } else {
            $('.btn-scr').stop(true, true).fadeOut();
        }
    });

    $("#btnScr").click(function(){
        $('html, body').animate({ scrollTop: 0 }, 'slow', function () {
            // alert("reached top");
        });

        return false;
    });

JS;
$this->registerJs($script);
?>