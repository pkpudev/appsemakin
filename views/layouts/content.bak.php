<?php

use app\components\PhotosHelper;
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;

CrudAsset::register($this);
?>
<div class="content-wrapper">
    <section class="content-header">

        <table width="100%">
            <tr>
                <td width="60px">
                    <a href="#" class="sidebar-toggle" title="Buka/Tutup Menu" data-toggle="offcanvas" role="button" id="tog-cont" style="color: rgba(0, 0, 0, 0.6) !important; padding: 4px 7px !important; font-size: 17px;">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                </td>
                <td>
                    <div class="navbar-custom-menu pull-left" style="white-space: nowrap;">

                        <div class="tool-box pull-right">
                            <label class="switch" title="Hidupkan/Matikan Dark-Mode">
                                <input type="checkbox" id="theme">
                                <span class="slider round
                                <?= (!Yii::$app->user->can('Admin CRM') && 
                                !Yii::$app->user->can('Admin CRM Cabang') &&  
                                !Yii::$app->user->can('Admin Keuangan')) ? 'canOnlySwitch' : ''; ?>"></span>
                            </label>

                            <?php if(Yii::$app->user->can('Admin CRM') || Yii::$app->user->can('Admin CRM Cabang') ||  Yii::$app->user->can('Admin Keuangan')): ?>
                            <div class="pull-right dropdown notification">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="fa fa-bell"></span>
                                </a>
                                
                                <ul class="dropdown-menu" style="margin-top: 4px;margin-right: -10px;">
                                    <?php if(Yii::$app->user->can('Admin CRM') || Yii::$app->user->can('Admin CRM Cabang')): ?>
                                    <li>
                                        <?php /*  Html::a("Terdapat ". Yii::$app->aggregator->donasiBelumValidCrm . " donasi belum valid CRM.", 
                                            ['/donation/donation/index', 'DonationSearch[status_id]' => DonationStatus::BELUM_VALID, 'DonationSearch[keu_validated]' => DonationStatus::VALID], []); */ ?>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <?php endif; ?>
                        </div>

                        <?php if (isset($this->blocks['content-header'])) { ?>
                        <h3 style="margin-top: 15px;"><?= $this->blocks['content-header'] ?></h3>
                        <?php } else { ?>
                        <h3 style="margin-top: 15px;">
                            <?php
                                if ($this->title !== null) {
                                    echo \yii\helpers\Html::encode($this->title);
                                } else {
                                    echo \yii\helpers\Inflector::camel2words(
                                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                                    );
                                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                                } ?>
                        </h3>
                        <?php } ?>
                    </div>
                </td>
            </tr>
        </table>


        <div class="pull-right dropdown user-box" style="padding: 10px 45px 10px 10px;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <?= !Yii::$app->user->isGuest ? Yii::$app->user->identity->full_name : 'Anda belum Login'; ?>
                
                <div class="cropcircle" style="width: 30px; height: 30px;  position: absolute; top: 5px; right: 5px; background-position: center center; background-repeat: no-repeat; background-size: cover; background-image: url('<?= PhotosHelper::getProfilePhotosUrl(Yii::$app->user->id); ?>')">
                </div>
            </a>

            <ul class="dropdown-menu">
                <?php if(!Yii::$app->user->isGuest){ ?>
                    <li class="user-header">
                        <a href="https://hrm.c27g.com/employee/profile/" class="" style="padding-top: 150px;">
                        <center>
                        <div class="cropcircle" style="width: 150px; height: 150px; position: absolute; top: 15px; right: 85px; background-position: center center; background-repeat: no-repeat; background-size: cover; background-image: url('<?= PhotosHelper::getProfilePhotosUrl(Yii::$app->user->id); ?>')">
                        </div>
                            

                            <p style="margin-top: 10px;font-size: 18px;margin-bottom: -5px;">
                                Pengaturan Akun
                                <?php //!Yii::$app->user->isGuest ? Yii::$app->user->identity->name : "Guest"?>
                                <!-- <?php // $position = \app\models\VwPosition::find()->where(['employee_id'=>Yii::$app->user->id])->one(); ?>
                                <small><?php // ($position)?$position->position_name.' - '.$position->department:' - '?></small> -->
                            </p>
                        </center>
                        </a>
                    </li>
                    <li>
                        <?= Html::a('Sign out', ['/site/logout'], ['data-method' => 'post', 'class' => '']) ?>
                    </li>
                        
                <?php } else { ?>
                    <?= Html::a(
                        'Login',
                        ['/site/login'],
                        ['data-method' => 'post', 'class' => 'btn btn-success btn-flat', 'style' => 'width: 100%;']
                    ) ?>
                <?php } ?>
            </ul>
        </div>
        
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>

        <div class="btn-scr">
            <button id="btnScr">
                <span class="fa fa-caret-up"></span>
            </button>
        </div>

    </section>
</div>

<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class='control-sidebar-bg'></div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<?php
$script = <<< JS
    /* $("body").niceScroll({
        cursorwidth:12,
        cursoropacitymin:0.4,
        cursorcolor:'#6e8cb6',
        cursorborder:'none',
        cursorborderradius:4,
        autohidemode:'leave'
    });
    $(".main-sidebar").niceScroll({
        cursorwidth:12,
        cursoropacitymin:0.4,
        cursorcolor:'#6e8cb6',
        cursorborder:'none',
        cursorborderradius:4,
        autohidemode:'leave'
    }); */

    $(document).scroll(function(){
        if ($(this).scrollTop() > 50 ) {
            $('.btn-scr:hidden').stop(true, true).fadeIn();
        } else {
            $('.btn-scr').stop(true, true).fadeOut();
        }
    });

    $("#btnScr").click(function(){
        $('html, body').animate({ scrollTop: 0 }, 'slow', function () {
            // alert("reached top");
        });

        return false;
    });

JS;
$this->registerJs($script);
?>