<?php

use app\models\Management;
use app\models\VwEmployeeManager;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use yii\helpers\Html;


$isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one();

$lastManagement = Management::find()->orderBy('id desc')->one();
?>

<aside class="main-sidebar" style="padding-top: 0;">

    <section class="sidebar">
        <a href="/" class="welcome-link logo">
            <span class="logo-mini"><span class="fa fa-cube"></span></span>
            <span class="logo-lg"><span class="fa fa-cube"></span>&nbsp;&nbsp;E - P E R F O R M</span>
        </a>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree',  'data-widget' => "tree"],
                'items' => [
                    [
                        'label' => 'Aplikasi Intranet',
                        'icon' => 'ellipsis-h',
                        'url' => '/site/application-list',
                        'template'=>'<a href="{url}" role="modal-remote">{icon} {label}</a>'
                    ],
                    [
                        'label' => 'Dashboard',
                        'icon' => 'dashboard',
                        'url' => '#',
                        'items' => [
                            // ['label' => 'Perencanaan', 'icon' => 'list', 'url' => ['/dashboard/summary1']],
                            [
                                'label' => 'Perencanaan',
                                'icon' => 'list',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Strategic Plan', 'icon' => 'list', 'url' => ['/dashboard/plan']],
                                    ['label' => 'Struktur Organisasi', 'icon' => 'list', 'url' => ['/dashboard/organization-structure']],
                                    ['label' => 'OKR Organisasi', 'icon' => 'list', 'url' => ['/dashboard/okr-organization2']],
                                    ['label' => 'Target Penghim. & PM', 'icon' => 'list', 'url' => ['/dashboard/target']],
                                    ['label' => 'Anggaran', 'icon' => 'list', 'url' => ['/dashboard/budget2']],
                                ],
                            ],
                            [
                                'label' => 'Pencapaian Kinerja',
                                'icon' => 'list',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Anggaran', 'icon' => 'list', 'url' => ['/dashboard/summary2']],
                                    ['label' => 'OKR Individu', 'icon' => 'list', 'url' => ['/dashboard/okr2']],
                                    // ['label' => 'Kegiatan Kerja', 'icon' => 'list', 'url' => ['/dashboard/activity']],
                                    ['label' => 'Unit Kerja', 'icon' => 'list', 'url' => ['/dashboard/unit2']],
                                    ['label' => 'OKR Organisasi', 'icon' => 'list', 'url' => ['/dashboard2/okr-org2']],
                                ],
                            ],
                        ],
                    ],
                    [
                        'label' => 'Konteks Organisasi',
                        'icon' => 'sitemap',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Analisis Situasi',
                                'icon' => 'spinner',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Analisis Situasi', 'icon' => 'list', 'url' => ['/analysis/situtation-analysis/index']],
                                    [
                                        'label' => 'Setting Variabel',
                                        'icon' => 'gear',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Variabel Internal', 'icon' => 'list', 'url' => ['/analysis/situtation-analysis/internal-variable']],
                                            ['label' => 'Variabel Eksternal', 'icon' => 'list', 'url' => ['/analysis/situtation-analysis/external-variable']],
                                        ],
                                        'visible' => Yii::$app->user->can('Admin'),
                                    ],
                                ],
                            ],
                            [
                                'label' => 'Analisis Strategi',
                                'icon' => 'map-o',
                                'url' => ['/analysis/strategy-analysis/index'],
                            ],
                            [
                                'label' => 'Analisis Pelanggan',
                                'icon' => 'users',
                                'url' => ['/analysis/customer-analysis/index'],
                            ],
                            [
                                'label' => 'Analisis Stakeholder',
                                'icon' => 'blind',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Pemangku Kepentingan', 'icon' => 'list', 'url' => ['/analysis/stakeholder/index']],
                                    [
                                        'label' => 'Setting',
                                        'icon' => 'gear',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Kategori', 'icon' => 'list', 'url' => ['/setting/stakeholder-category/index']],
                                            ['label' => 'Pengaruh', 'icon' => 'list', 'url' => ['/setting/stakeholder-influence/index']],
                                            ['label' => 'Dukungan', 'icon' => 'list', 'url' => ['/setting/stakeholder-support/index']],
                                            ['label' => 'Hasil Analisis', 'icon' => 'list', 'url' => ['/setting/stakeholder-analysis-result/index']],
                                        ],
                                        'visible' => Yii::$app->user->can('Admin'),
                                    ],
                                ]
                            ],
                            [
                                'label' => 'Proses Bisnis & Dok. Mutu',
                                'icon' => 'file-text-o',
                                'url' => ['/businessprocess/business-process/index'],
                            ],
                        ],
                        'visible' => Yii::$app->user->can('Admin'),
                    ], 
                    [
                        'label' => 'Kepemimpinan',
                        'icon' => 'file',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Model Bisnis',
                                'icon' => 'file',
                                'url' => ['/analysis/portfolio/index'],
                            ],
                            [
                                'label' => 'Struktur Organisasi',
                                'icon' => 'sitemap',
                                'url' => ['/leadership/structure/index'],
                                'items' => [
                                    ['label' => 'Unit', 'icon' => 'bank', 'url' => ['/leadership/structure/unit']],
                                    ['label' => 'Jabatan', 'icon' => 'users', 'url' => ['/leadership/structure/index']],
                                ],
                                'visible' => Yii::$app->user->can('Admin') || Yii::$app->user->can('Observer')
                            ],
                        ],
                        'visible' => Yii::$app->user->can('Admin'),
                    ],
                    /* [
                        'label' => 'Perencanaan V1 (2022)',
                        'icon' => 'circle-o-notch',
                        'url' => '#',
                        'items' => [[
                            'label' => 'Rencana Kerja Tahunan',
                            'icon' =>'file',
                            'url' => '#',
                            'items'=>[
                                [
                                    'label' => 'OKR',
                                    'icon' =>'file',
                                    'url' => '#',
                                    'items' => [
                                        [
                                            'label' => 'OKR Unit',
                                            'icon' =>'file',
                                            'url' => ['/strategic/okr/index'],
                                        ],
                                        // [
                                        //     'label' => 'OKR (List)',
                                        //     'icon' =>'file',
                                        //     'url' => ['/strategic/okr/index'],
                                        // ],
                                        [
                                            'label' => 'OKR Individu',
                                            'icon' =>'file',
                                            'url' => ['/strategic/okr/individual'],
                                        ],
                                    ]
                                ],
                                ['label' => '', 'options' => ['class' => 'divider']],
                                [
                                    'label' => 'Pagu Anggaran',
                                    'icon' =>'file',
                                    'url' => ['/strategic/ceiling/index'],
                                    'visible' => Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')
                                ],
                                [
                                    'label' => 'Target',
                                    'icon' =>'file',
                                    'url' => ['/strategic/target/index'],
                                    'visible' => Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')
                                ],
                                ['label' => '', 'options' => ['class' => 'divider']],
                                [
                                    'label' => 'Program Kerja',
                                    'icon' =>'file',
                                    'url' => ['/strategic/initiatives/index'],
                                ],
                                [
                                    'label' => 'Kegiatan Kerja',
                                    'icon' =>'file',
                                    'url' => ['/strategic/activity/index'],
                                ],
                                [
                                    'label' => 'Anggaran Kerja',
                                    'icon' =>'file',
                                    'url' => ['/strategic/budget/index'],
                                ],
                                ['label' => '', 'options' => ['class' => 'divider']],
                                [
                                    'label' => 'Approval RKAT',
                                    'icon' =>'file',
                                    'url' => ['/strategic/initiatives/approval-all'],
                                    'visible' => Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')
                                ],
                                // [
                                //     'label' => 'Rincian Pelaksanaan Kerja',
                                //     'icon' =>'file',
                                //     'url' => '#',
                                // ],
                            ]
                        ],
                        ]
                    ], */
                    [
                        'label' => 'Perencanaan',
                        'icon' => 'circle-o-notch',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Rencana Strategis',
                                'icon' => 'code-fork',
                                'url' => '#',
                                'items' => [
                                    [
                                        'label' => 'Visi Misi', 
                                        'icon' => 'binoculars', 
                                        'url' => [Yii::$app->user->can('Admin') ? '/strategic/management/index' : ($lastManagement ? '/strategic/management/view?id=' . $lastManagement->id : '#')]
                                    ],
                                    [
                                        'label' => 'OKR Organisasi',
                                        'icon' =>'file',
                                        'url' => ['/strategic/okr/organization'],
                                    ],
                                ]
                            ],
                            [
                                'label' => 'Rencana Kerja Tahunan',
                                'icon' =>'file',
                                'url' => '#',
                                'items'=>[
                                    [
                                        'label' => 'OKR',
                                        'icon' =>'file',
                                        'url' => '#',
                                        'items' => [
                                            [
                                                'label' => 'OKR Unit',
                                                'icon' =>'file',
                                                'url' => ['/strategic/okr/index'],
                                            ],
                                            // [
                                            //     'label' => 'OKR (List)',
                                            //     'icon' =>'file',
                                            //     'url' => ['/strategic/okr/index'],
                                            // ],
                                            [
                                                'label' => 'OKR Individu',
                                                'icon' =>'file',
                                                'url' => ['/strategic/okr/individual'],
                                            ],
                                        ]
                                    ],
                                    ['label' => '', 'options' => ['class' => 'divider']],
                                    [
                                        'label' => 'Pagu Anggaran',
                                        'icon' =>'file',
                                        'url' => ['/strategic/ceiling/index'],
                                        'visible' => Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')
                                    ],
                                    [
                                        'label' => 'Pagu Pembiayaan',
                                        'icon' =>'file',
                                        'url' => ['/strategic/financing-type-ceiling/index'],
                                        'visible' => Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')
                                    ],
                                    [
                                        'label' => 'Target',
                                        'icon' =>'file',
                                        'url' => ['/strategic/target/index'],
                                        'visible' => Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')
                                    ],
                                    ['label' => '', 'options' => ['class' => 'divider']],
                                    [
                                        'label' => 'Program Kerja',
                                        'icon' =>'file',
                                        'url' => ['/strategic/initiatives2/index'],
                                    ],
                                    [
                                        'label' => 'Anggaran Kerja',
                                        'icon' =>'file',
                                        'url' => ['/strategic/budget/index2'],
                                    ],
                                    ['label' => '', 'options' => ['class' => 'divider']],
                                    [
                                        'label' => 'Analisis Risiko',
                                        'icon' =>'list',
                                        'url' => ['/strategic/risk/index'],
                                        // 'visible' => Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD') || Yii::$app->user->can('Risk Management Officer')
                                    ],
                                    ['label' => '', 'options' => ['class' => 'divider']],
                                    [
                                        'label' => 'Approval RKAT',
                                        'icon' =>'file',
                                        'url' => ['/strategic/initiatives/approval-all'],
                                        'visible' => Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')
                                    ],
                                ]
                            ],
                        ],
                    ],
                    [
                        'label' => 'Evaluasi',
                        'icon' => 'balance-scale',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Kegiatan Kerja', 'icon' => 'list', 'url' => ['/evaluation/activity']],
                            ['label' => 'Program Kerja', 'icon' => 'list', 'url' => ['/evaluation/initiatives']],
                            ['label' => 'OKR Individu', 'icon' => 'list', 'url' => ['/evaluation/okr/individual']],
                            ['label' => 'OKR Unit', 'icon' => 'list', 'url' => ['/evaluation/okr/unit']],
                            ['label' => 'OKR Organisasi (Admin)', 'icon' => 'list', 'url' => ['/evaluation/okr/organization-admin'], 'visible' => Yii::$app->user->can('Admin')],
                            ['label' => 'OKR Organisasi', 'icon' => 'list', 'url' => ['/evaluation/okr/organization'], 'visible' => Yii::$app->user->can('Leader') || Yii::$app->user->can('Admin') || $isGeneralManager],
                            ['label' => '', 'options' => ['class' => 'divider']],
                            ['label' => 'Progres Evaluasi', 'icon' => 'list', 'url' => ['/evaluation/monitoring/index'], 'visible' => Yii::$app->user->can('Leader') || Yii::$app->user->can('Admin') || $isGeneralManager],
                            ['label' => '', 'options' => ['class' => 'divider']],
                            ['label' => 'Setting Evaluasi', 'icon' => 'list', 'url' => ['/evaluation/evaluation'], 'visible' => Yii::$app->user->can('Admin')],
                        ],
                        // 'visible' => Yii::$app->user->can('Admin')
                    ],
                    [
                        'label' => 'Monitoring',
                        'icon' =>'file',
                        'url' => '#',
                        'items'=>[
                            ['label' => 'Kegiatan Kerja', 'icon' => 'list', 'url' => ['/evaluation/activities/index']],
                            ['label' => 'Daftar Kendala/Isu', 'icon' => 'list', 'url' => ['/evaluation/activities/issues']],
                            ['label' => 'Penilaian Kegiatan', 'icon' => 'list', 'url' => ['/evaluation/activities/review']],
                            ['label' => '', 'options' => ['class' => 'divider']],
                            ['label' => 'Jam Kerja', 'icon' => 'list', 'url' => ['/evaluation/activities/hour']],
                            ['label' => 'Ringkasan Monitoring', 'icon' => 'list', 'url' => ['/dashboard/activities']],
                        ],
                        // 'visible' => Yii::$app->user->can('Admin')
                    ],
                    ['label' => 'Tutorial', 'icon' => 'files-o', 'url' => ['/tutorial/index']],
                    /* [
                        'label' => 'Pengorganisasian',
                        'icon' => 'gg',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Sumber Daya',
                                'icon' => 'first-order',
                                'url' => '#',
                                'items' => [
                                    [
                                        'label' => 'HR Apps', 
                                        'icon' => 'file', 
                                        'url' =>'https://hrm.c27g.com/',
                                       'template' => '<a href="{url}" target="_blank">{icon} {label}</a>'
                                    ],
                                    [
                                        'label' => 'Procurement Apps', 
                                        'icon' => 'file', 
                                        'url' =>'https://eproc.c27g.com/',
                                       'template' => '<a href="{url}" target="_blank">{icon} {label}</a>'
                                    ],
                                    [
                                        'label' => 'Finance Apps', 
                                        'icon' => 'file', 
                                        'url' =>'https://finance.c27g.com/',
                                       'template' => '<a href="{url}" target="_blank">{icon} {label}</a>'
                                    ],
                                ]
                            ],
                            [
                                'label' => 'Kompetensi & Pengembangan Sumber Daya', 
                                'icon' => 'file', 
                                'url' =>'https://hiinstitute.or.id/',
                               'template' => '<a href="{url}" target="_blank">{icon} {label}</a>'
                            ],
                            [
                                'label' => 'Komunikasi', 
                                'icon' => 'file', 
                                'url' =>'#',
                               'template' => '<a href="{url}" target="_blank">{icon} {label}</a>'
                            ],
                            [
                                'label' => 'Informasi terdokumentasi', 
                                'icon' => 'file', 
                                'url' =>'https://pusdatin.c27g.com/',
                               'template' => '<a href="{url}" target="_blank">{icon} {label}</a>'
                            ],
                        ]
                    ],
                    [
                        'label' => 'Pelaksanaan',
                        'icon' => 'file',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Penugasan',
                                'icon' => 'file',
                                'url' => '#',
                            ],
                            [
                                'label' => 'Laporan Pelaksanaan Tugas', 
                                'icon' => 'file', 
                                'url' =>'#',
                            ],
                            [
                                'label' => 'Laporan Unit Kerja', 
                                'icon' => 'file', 
                                'url' =>'#',
                            ],
                        ]
                    ],
                    [
                        'label' => 'Penilaian',
                        'icon' => 'file',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Pemantauan, Pengukuran & Evaluasi',
                                'icon' => 'file',
                                'url' => '#',
                            ],
                            [
                                'label' => 'Audit Internal', 
                                'icon' => 'file', 
                                'url' =>'#',
                            ],
                            [
                                'label' => 'Audit Eksternal', 
                                'icon' => 'file', 
                                'url' =>'#',
                            ],
                        ]
                    ], */
                    [
                        'label' => 'Pengaturan',
                        'icon' => 'wrench',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Anggaran Partisipatori', 'icon' => 'list', 'url' => ['/setting/participatory-budgeting/index'], 'visible' => Yii::$app->user->can('SuperUser')],
                            ['label' => 'Attribute', 'icon' => 'list', 'url' => ['/setting/attributes/index'], 'visible' => Yii::$app->user->can('SuperUser')],
                            ['label' => 'Elemen Model Bisnis', 'icon' => 'list', 'url' => ['/setting/portfolio-element/index']],
                            ['label' => 'Hint', 'icon' => 'list', 'url' => ['/setting/hint/index']],
                            ['label' => 'Jenis Biaya', 'icon' => 'list', 'url' => ['/setting/cost-type/index']],
                            ['label' => 'Jenis Kendala', 'icon' => 'list', 'url' => ['/setting/issue-type/index']],
                            ['label' => 'Jenis Pembiayaan', 'icon' => 'list', 'url' => ['/setting/financing-type/index']],
                            ['label' => 'Kategori Kegiatan', 'icon' => 'list', 'url' => ['/setting/activity-category/index']],
                            ['label' => 'Kategori Risiko', 'icon' => 'list', 'url' => ['/setting/risk-category/index']],
                            ['label' => 'Penilaian Kegiatan', 'icon' => 'list', 'url' => ['/setting/activity-evaluation-value/index']],
                            ['label' => 'Perspektif', 'icon' => 'list', 'url' => ['/setting/perspective/index']],
                            ['label' => 'Prioritas Pembiayaan', 'icon' => 'list', 'url' => ['/setting/cost-priority/index']],
                            ['label' => 'Satuan', 'icon' => 'list', 'url' => ['/setting/measure/index']],
                            ['label' => 'Sumber Dana', 'icon' => 'list', 'url' => ['/setting/source-of-fund/index']],
                            ['label' => 'Tipe Cascading', 'icon' => 'list', 'url' => ['/setting/cascading-type/index']],
                            ['label' => 'Tipe Dokumen', 'icon' => 'list', 'url' => ['/setting/document-type/index']],
                            ['label' => 'Tipe Portofolio', 'icon' => 'list', 'url' => ['/setting/portfolio-type/index']],
                            ['label' => 'Tipe Target', 'icon' => 'list', 'url' => ['/setting/target-type/index']],
                        ],
                        'visible' => Yii::$app->user->can('Admin'),
                    ],
                    [
                        'label' => 'Superuser Tools',
                        'icon' => 'gears',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'], 'linkOptions' => ['target' => '_blank']],
                            [
                                'label' => 'Otorisasi',
                                'icon' => 'key',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Assignment', 'icon' => 'users', 'url' => ['/admin/assignment']],
                                    ['label' => 'Role', 'icon' => 'user', 'url' => ['/admin/role']],
                                    ['label' => 'Permission', 'icon' => 'tasks', 'url' => ['/admin/permission']],
                                    ['label' => 'Route', 'icon' => 'link', 'url' => ['/admin/route']],
                                    ['label' => 'Rule', 'icon' => 'map-o', 'url' => ['/admin/rule']],
                                ],
                            ],
                        ],
                        'visible' => Yii::$app->user->can('SuperUser'),
                    ],
                    [
                        'label' => 'Sign Out',
                        'icon' => 'power-off',
                        'url' => '/site/logout',
                        'options' => ['id' => 'logout-menu'],
                        'template'=>'<a href="{url}" data-method="post">{icon} {label}</a>'
                    ]
                ],
            ]
        ) ?>


        <div href="/" class="welcome-link logo" style="position: absolute;bottom: 0;">
            <span class="logo-lg" style="font-size: 14px;color: #fff !important;">Copyright &copy; 2021-<?= date('Y'); ?> by IT DEV and <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</span>
        </div>

    </section>

</aside>