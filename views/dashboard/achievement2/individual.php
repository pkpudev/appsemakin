<?php

use app\models\Calculation;
use app\models\Employee;
use app\models\EmployeeAchievement;
use app\models\Evaluation;
use app\models\Okr;
use app\models\OkrEvaluation;
use app\models\PositionStructure;
use app\models\UnitAchievement;
use app\models\UnitStructure;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\bootstrap\Progress;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Individu';
?>
<div class="row">
    <div class="col-md-3">
        <div class="box box-default">
            <div class="box-body">
                <div class="box-dashboard">
                    <center><b style="color: var(--borderColor);font-size: 17px;">Filter</b></center>
                    <table width="100%">
                        <tr>
                            <td width="150px">
                                <?= 
                                    DatePicker::widget([
                                        'model' => $searchModel,
                                        'attribute' => 'year',
                                        'pjaxContainerId' => 'crud-datatable-pjax',
                                        'options' => [
                                            'id' => 'year-filter',
                                            'placeholder' => 'Filter Tahun',
                                            'class' => 'form-control additional-filter',
                                            'onchange' => 'changeFilterYear($(this).val())',
                                        ],
                                        'pluginOptions' => [
                                            'autoclose'=>true,
                                            'startView'=>'year',
                                            'minViewMode'=>'years',
                                            'format' => 'yyyy',
                                            'allowClear' => false
                                        ]
                                    ])
                                ?>
                            </td>
                            
                            <td style="padding-left: 5px;">
                                <?= Select2::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'unit_structure_id',
                                    'options' => [
                                        'id' => 'unit-filter',
                                        'placeholder' => '-- Filter Unit --',
                                        'onchange' => 'changeFilterUnit($(this).val())',
                                        // 'multiple' => true
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                    'data' => ArrayHelper::map(UnitStructure::find()->where(['year' => $year])->orderBy('unit_code')->andWhere("unit_id <> 329")->all(), 'id', function($model){
                                                    return $model->unit_code . ' - ' . $model->unit_name;
                                                }),
                                    ])
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box box-default">
    <div class="box-body">
        
    <?=GridView::widget([
            'id'=>'okr-crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'attribute'=>'employee_id',
                    'value'=>function ($model) {
                        return $model->employee->full_name;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(Employee::find()->where(['and', ['user_status'=>1], ['is_employee'=>1], ['<>', 'employee_type_id', 4]])->orderBy('full_name')->all(), 'id', 'full_name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>''],
                    'group'=>true,
                    'width' => '280px',
                ],
                [
                    'attribute'=>'unit_structure_id',
                    'label' => 'Unit',
                    'value'=>function ($model) {
                        return $model->position->unit->unit_name;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(UnitStructure::find()->where(['year'=>$year])->orderBy('unit_name')->all(), 'id', 'unit_name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>''],
                    'group'=>true,
                    'width' => '280px',
                ],
                [
                    'attribute'=>'position_structure_id',
                    'value'=>function ($model) {
                        return $model->position->position_name . (Yii::$app->user->can('SuperUser') ? ' (' . $model->position_structure_id . ')' : '');
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(PositionStructure::find()->where(['year'=>$year])->orderBy('position_name')->all(), 'id', 'position_name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>''],
                    'width' => '280px',
                ],
                [
                    'label'=>'Kuartal 1',
                    'filter'=>false,
                    'value'=>function ($model) {
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $evaluation = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 1']);
                        
                        if(in_array($model->position->level, [1, 2, null]) || $model->position->unit->unit_id == 277){
                            $achieve = EmployeeAchievement::findOne(['evaluation_id' => $evaluation->id, 'position_structure_id' => $model->position_structure_id, 'employee_id' => $model->employee_id, 'achieve_type' => 'OKR']);
                            $percentage = $achieve->achieve_value ?: 0;
                        } else {
                            $achieve = UnitAchievement::findOne(['evaluation_id' => $evaluation->id, 'unit_id' => $model->position_structure_id, 'achieve_type' => 'OKR']);
                            $percentage = $achieve->achieve_value ?: 0;
                        }

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    },
                    'format'=>'raw',
                ],
                [
                    'label'=>'Kuartal 2',
                    'filter'=>false,
                    'value'=>function ($model) {
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $evaluation = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 2']);

                        if(in_array($model->position->level, [1, 2, null]) || $model->position->unit->unit_id == 277){
                            $achieve = EmployeeAchievement::findOne(['evaluation_id' => $evaluation->id, 'position_structure_id' => $model->position_structure_id, 'employee_id' => $model->employee_id, 'achieve_type' => 'OKR']);
                            $percentage = $achieve->achieve_value ?: 0;
                        } else {
                            $achieve = UnitAchievement::findOne(['evaluation_id' => $evaluation->id, 'unit_id' => $model->position_structure_id, 'achieve_type' => 'OKR']);
                            $percentage = $achieve->achieve_value ?: 0;
                        }

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    },
                    'format'=>'raw',
                ],
                [
                    'label'=>'Kuartal 3',
                    'filter'=>false,
                    'value'=>function ($model) {
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $evaluation = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 3']);

                        if(in_array($model->position->level, [1, 2, null]) || $model->position->unit->unit_id == 277){
                            $achieve = EmployeeAchievement::findOne(['evaluation_id' => $evaluation->id, 'position_structure_id' => $model->position_structure_id, 'employee_id' => $model->employee_id, 'achieve_type' => 'OKR']);
                            $percentage = $achieve->achieve_value ?: 0;
                        } else {
                            $achieve = UnitAchievement::findOne(['evaluation_id' => $evaluation->id, 'unit_id' => $model->position_structure_id, 'achieve_type' => 'OKR']);
                            $percentage = $achieve->achieve_value ?: 0;
                        }

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    },
                    'format'=>'raw',
                ],
                [
                    'label'=>'Kuartal 4',
                    'filter'=>false,
                    'value'=>function ($model) {
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $evaluation = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 4']);

                        if(in_array($model->position->level, [1, 2, null]) || $model->position->unit->unit_id == 277){
                            $achieve = EmployeeAchievement::findOne(['evaluation_id' => $evaluation->id, 'position_structure_id' => $model->position_structure_id, 'employee_id' => $model->employee_id, 'achieve_type' => 'OKR']);
                            $percentage = $achieve->achieve_value ?: 0;
                        } else {
                            $achieve = UnitAchievement::findOne(['evaluation_id' => $evaluation->id, 'unit_id' => $model->position_structure_id, 'achieve_type' => 'OKR']);
                            $percentage = $achieve->achieve_value ?: 0;
                        }

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    },
                    'format'=>'raw',
                ],
                /* [
                    'label' => 'Hasil Akhir',
                    'format' => 'raw',
                    'value' => function($model){
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');

                        $okrs = Okr::find()
                                        ->where(['position_structure_id' => $model->position_structure_id])
                                        ->andWhere(['pic_id' => $model->employee_id])
                                        ->andWhere(['type' => 'KR'])
                                        ->andWhere(['year' => $year])
                                        ->all();
                        
                        $val = 0;
                        foreach($okrs as $okr){
                            $achieveValues = [];

                            $evaluation1 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 1']);
                            $evaluation2 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 2']);
                            $evaluation3 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 3']);
                            $evaluation4 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 4']);

                            $okrEvaluation1 = OkrEvaluation::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation1->id]);
                            $okrEvaluation2 = OkrEvaluation::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation2->id]);
                            $okrEvaluation3 = OkrEvaluation::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation3->id]);
                            $okrEvaluation4 = OkrEvaluation::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation4->id]);

                            $weight = $okr->unit_weight;

                            if($okrEvaluation1) array_push($achieveValues, ($weight && $okrEvaluation1->target ? ((($okrEvaluation1->real / $okrEvaluation1->target)?:0) * $weight) * 100 / 100 : 0));
                            if($okrEvaluation2) array_push($achieveValues, ($weight && $okrEvaluation2->target ? ((($okrEvaluation2->real / $okrEvaluation2->target)?:0) * $weight) * 100 / 100 : 0));
                            if($okrEvaluation3) array_push($achieveValues, ($weight && $okrEvaluation3->target ? ((($okrEvaluation3->real / $okrEvaluation3->target)?:0) * $weight) * 100 / 100 : 0));
                            if($okrEvaluation4) array_push($achieveValues, ($weight && $okrEvaluation4->target ? ((($okrEvaluation4->real / $okrEvaluation4->target)?:0) * $weight) * 100 / 100 : 0));

                            if($okr->calculation->id == Calculation::SUM){
                                $lastAchieveValue = array_sum($achieveValues);
                            } else if($okr->calculation->id == Calculation::AVERAGE){
                                $lastAchieveValue = $achieveValues ? array_sum($achieveValues)/count($achieveValues) : 0;
                            } else if($okr->calculation->id == Calculation::LAST){
                                $lastAchieveValue = end($achieveValues);
                            } else if($okr->calculation->id == Calculation::MIN){
                                $lastAchieveValue = $achieveValues ? min($achieveValues) : 0;
                            }

                            var_dump($okr->id . ' = ' . $okrEvaluation4->real);

                            // if($okr->id == 21470) {
                            //     $filterOkrEvaluation = "o.type = 'KR' AND 
                            //         okr_level_id <> 1 AND 
                            //         o.pic_id = {$model->employee_id} AND 
                            //         o.position_structure_id = {$model->position_structure_id} AND 
                            //         oe.evaluation_id = {$evaluation2->id} AND 
                            //         oe.status_id IN (11,12) AND 
                            //         COALESCE(oe.target::numeric, 0) <> 0";
            
                            //     $okrEvaluationWeight = \Yii::$app->db->createCommand("SELECT SUM(COALESCE(oe.weight, 0))
                            //         FROM management.okr_evaluation oe 
                            //         LEFT JOIN management.okr o on o.id = oe.okr_id 
                            //         WHERE $filterOkrEvaluation")->queryScalar() ?: 0;
                                    
                            //     var_dump($okrEvaluation2->real);
                            //     var_dump($weight);
                            //     var_dump($okrEvaluationWeight);
                            //     var_dump(($okrEvaluation2->real * $weight) / $okrEvaluationWeight);
                            //     var_dump(json_encode($achieveValues));
                            // }
                            // var_dump($lastAchieveValue);
                            // var_dump($okr->calculation->calculation_type);
                            $val = $val + $lastAchieveValue;
                            $achieveValues = [];
                        }
                            
                        return Progress::widget([
                            'percent' => round($val, 2),
                            'label' => round($val, 2).'%',
                        ]);
                    }
                ] */
            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ]
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> OKR',
                'before'=>'',
                'after'=>'',
            ]
        ])?>
    </div>
</div>


<script>
    function changeFilterYear(val) {
        window.location.replace("/dashboard/okr2?year=" + val);
    }
    function changeFilterUnit(val) {
        var year = $("#year-filter").val();
        window.location.replace("/dashboard/okr2?year=" + year + "&PositionStructureEmployeeSearch[unit_structure_id]=" + val);
    }
</script>