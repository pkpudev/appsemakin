<?php

use app\models\Calculation;
use app\models\Evaluation;
use app\models\Okr;
use app\models\OkrEvaluation;
use app\models\PositionStructure;
use app\models\UnitAchievement;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\bootstrap\Progress;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Unit Kerja';
?>
<div class="row">
    <div class="col-md-3">
        <div class="box box-default">
            <div class="box-body">
                <div class="box-dashboard">
                    <center><b style="color: var(--borderColor);font-size: 17px;">Filter</b></center>
                    <table width="100%">
                        <tr>
                            <td width="150px">
                                <?= DatePicker::widget([
                                        'model' => $searchModelUnit,
                                        'attribute' => 'year',
                                        'pjaxContainerId' => 'crud-datatable-pjax',
                                        'options' => [
                                            'id' => 'year-filter',
                                            'placeholder' => 'Filter Tahun',
                                            'class' => 'form-control additional-filter',
                                            'onchange' => 'changeFilterYear($(this).val())',
                                        ],
                                        'pluginOptions' => [
                                            'autoclose'=>true,
                                            'startView'=>'year',
                                            'minViewMode'=>'years',
                                            'format' => 'yyyy',
                                            'allowClear' => false
                                        ]
                                    ])
                                ?>
                            </td>
                            <td style="padding-left: 5px;">
                                <?= Select2::widget([
                                    'model' => $searchModelUnit,
                                    'attribute' => 'id',
                                    'options' => [
                                        'id' => 'unit-filter',
                                        'placeholder' => '-- Filter Unit --',
                                        'onchange' => 'changeFilterUnit($(this).val())',
                                        // 'multiple' => true
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                    'data' => ArrayHelper::map(UnitStructure::find()->where(['year' => $year])->all(), 'id', function($model){
                                                    return $model->unit_code . ' - ' . $model->unit_name;
                                                }),
                                    ])
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box box-default">
    <div class="box-body">
        
    <?=GridView::widget([
            'id'=>'unit-crud-datatable',
            'dataProvider' => $dataProviderUnit,
            'filterModel' => $searchModelUnit,
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'attribute'=>'unit_name',
                    'value'=>function ($model) {
                        return $model->unit_code . ' - ' . $model->unit_name;
                        // return $model->position->unit->unit_code . ' - ' . $model->position->unit->unit_name;
                    },
                    'filter'=>false,
                    'width' => '280px',
                ],
                [
                    'label' => 'Kuartal 1',
                    'format' => 'raw',
                    'value' => function($model){
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $evaluation = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 1']);

                        $position = PositionStructure::find()->where(['unit_structure_id' => $model->id])->orderBy('level desc')->one();

                        $achieve = UnitAchievement::find()
                            ->where(['unit_id'=>$position->id, 'evaluation_id'=>$evaluation->id, 'achieve_type'=>'OKR'])
                            ->one();
                        $percentage = $achieve->achieve_value ?: 0;

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    }
                ],
                [
                    'label' => 'Kuartal 2',
                    'format' => 'raw',
                    'value' => function($model){
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $evaluation = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 2']);

                        $position = PositionStructure::find()->where(['unit_structure_id' => $model->id])->orderBy('level desc')->one();

                        $achieve = UnitAchievement::find()
                            ->where(['unit_id'=>$position->id, 'evaluation_id'=>$evaluation->id, 'achieve_type'=>'OKR'])
                            ->one();
                        $percentage = $achieve->achieve_value ?: 0;

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    }
                ],
                [
                    'label' => 'Kuartal 3',
                    'format' => 'raw',
                    'value' => function($model){
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $evaluation = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 3']);

                        $position = PositionStructure::find()->where(['unit_structure_id' => $model->id])->orderBy('level desc')->one();

                        $achieve = UnitAchievement::find()
                            ->where(['unit_id'=>$position->id, 'evaluation_id'=>$evaluation->id, 'achieve_type'=>'OKR'])
                            ->one();
                        $percentage = $achieve->achieve_value ?: 0;

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    }
                ],
                [
                    'label' => 'Kuartal 4',
                    'format' => 'raw',
                    'value' => function($model){
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $evaluation = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 4']);

                        $position = PositionStructure::find()->where(['unit_structure_id' => $model->id])->orderBy('level desc')->one();

                        $achieve = UnitAchievement::find()
                            ->where(['unit_id'=>$position->id, 'evaluation_id'=>$evaluation->id, 'achieve_type'=>'OKR'])
                            ->one();
                        $percentage = $achieve->achieve_value ?: 0;

                        return Progress::widget([
                            'percent' => $percentage,
                            'label' => $percentage < 10 ? '<span class="text text-danger" style="font-weight: bold;">' . round($percentage, 2) . '%</span>' :round($percentage, 2).'%',
                        ]);
                    }
                ],
                [
                    'label' => 'Hasil Akhir',
                    'format' => 'raw',
                    'value' => function($model){
                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                        $position = PositionStructure::find()->where(['unit_structure_id' => $model->id])->orderBy('level desc')->one();

                        $okrs = Okr::find()
                                        ->where(['position_structure_id' => $position->id])
                                        ->all();
                        
                        $val = 0;
                        foreach($okrs as $okr){
                            $achieveValues = [];

                            $evaluation1 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 1']);
                            $evaluation2 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 2']);
                            $evaluation3 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 3']);
                            $evaluation4 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 4']);

                            $okrEvaluation1 = OkrEvaluation::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation1->id]);
                            $okrEvaluation2 = OkrEvaluation::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation2->id]);
                            $okrEvaluation3 = OkrEvaluation::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation3->id]);
                            $okrEvaluation4 = OkrEvaluation::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation4->id]);

                            $weight = $okr->unit_weight;

                            if($okrEvaluation1) array_push($achieveValues, ($weight ? (($okrEvaluation1->real?:0) * $weight) / 100 : 0));
                            if($okrEvaluation2) array_push($achieveValues, ($weight ? (($okrEvaluation2->real?:0) * $weight) / 100 : 0));
                            if($okrEvaluation3) array_push($achieveValues, ($weight ? (($okrEvaluation3->real?:0) * $weight) / 100 : 0));
                            if($okrEvaluation4) array_push($achieveValues, ($weight ? (($okrEvaluation4->real?:0) * $weight) / 100 : 0));

                            if($okr->calculation->id == Calculation::SUM){
                                $lastAchieveValue = array_sum($achieveValues);
                            } else if($okr->calculation->id == Calculation::AVERAGE){
                                $lastAchieveValue = $achieveValues ? array_sum($achieveValues)/count($achieveValues) : 0;
                            } else if($okr->calculation->id == Calculation::LAST){
                                $lastAchieveValue = end($achieveValues);
                            } else if($okr->calculation->id == Calculation::MIN){
                                $lastAchieveValue = $achieveValues ? min($achieveValues) : 0;
                            }

                            $val = $val + $lastAchieveValue;
                        }
                            
                        return Progress::widget([
                            'percent' => round($val, 2),
                            'label' => round($val, 2).'%',
                        ]);
                    }
                ]
            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ]
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Unit Kerja',
                'before'=>'',
                'after'=>'',
            ]
        ])?>
    </div>
</div>

<?php
    $isManagerOrGm = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => [4, 6]])->andWhere(['is_active' => true])->one();
    if(Yii::$app->user->can('Admin') || Yii::$app->user->can('Leader') || $isManagerOrGm): 
?>
    <div class="box box-default">
        <div class="box-body">
            <?php
                
                $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                $evaluations = Evaluation::find()->where(['year' => $year])->orderBy('id')->all();

                $val = [];
                $i = 1;
                echo '<div class="row">';
                foreach($evaluations as $evaluation){

                    $countUnit = UnitStructure::find()->where(['year' => date('Y')])->andWhere("level != '1'")->andWhere("unit_id <> 329")->count();
                    $achieveAll = UnitAchievement::find()->joinWith(['position', 'position.unit u'])->where(['evaluation_id'=>$evaluation->id])->andWhere(['achieve_type'=>'OKR'])->andWhere("u.level != '1'")->andWhere("u.unit_id <> 329")->count();
                    $achieveA = UnitAchievement::find()->joinWith(['position', 'position.unit u'])->where(['evaluation_id'=>$evaluation->id])->andWhere(['achieve_type'=>'OKR'])->andWhere("achieve_value > 79")->andWhere(['achieve_type'=>'OKR'])->andWhere("u.level != '1'")->andWhere("u.unit_id <> 329")->count();
                    $achieveB = UnitAchievement::find()->joinWith(['position', 'position.unit u'])->where(['evaluation_id'=>$evaluation->id])->andWhere(['achieve_type'=>'OKR'])->andWhere("achieve_value > 59 and achieve_value <= 79")->andWhere(['achieve_type'=>'OKR'])->andWhere("u.level != '1'")->andWhere("u.unit_id <> 329")->count();
                    $achieveC = UnitAchievement::find()->joinWith(['position', 'position.unit u'])->where(['evaluation_id'=>$evaluation->id])->andWhere(['achieve_type'=>'OKR'])->andWhere("achieve_value > 39 and achieve_value <= 59")->andWhere(['achieve_type'=>'OKR'])->andWhere("u.level != '1'")->andWhere("u.unit_id <> 329")->count();
                    $achieveD = UnitAchievement::find()->joinWith(['position', 'position.unit u'])->where(['evaluation_id'=>$evaluation->id])->andWhere(['achieve_type'=>'OKR'])->andWhere("achieve_value > 19 and achieve_value <= 39")->andWhere(['achieve_type'=>'OKR'])->andWhere("u.level != '1'")->andWhere("u.unit_id <> 329")->count();
                    $achieveE = UnitAchievement::find()->joinWith(['position', 'position.unit u'])->where(['evaluation_id'=>$evaluation->id])->andWhere(['achieve_type'=>'OKR'])->andWhere("achieve_value <= 19")->andWhere(['achieve_type'=>'OKR'])->andWhere("u.level != '1'")->andWhere("u.unit_id <> 329")->count();
                    
                    array_push($val, ['name' => 'Sangat Baik', 'y' => $achieveA ?: 0]);
                    array_push($val, ['name' => 'Baik', 'y' => $achieveB ?: 0]);
                    array_push($val, ['name' => 'Cukup', 'y' => $achieveC ?: 0]);
                    array_push($val, ['name' => 'Kurang', 'y' => $achieveD ?: 0]);

                    if($countUnit != $achieveAll){
                        array_push($val, ['name' => 'Sangat Kurang', 'y' => $achieveE ? $achieveE + ($countUnit - $achieveAll): 0]);
                    } else {
                        array_push($val, ['name' => 'Sangat Kurang', 'y' => $achieveE?: 0]);
                    }

                    // var_dump($achieveAll . ' ' . ($achieveA + $achieveB + $achieveC + $achieveD + $achieveE + 2));
                    // var_dump($countUnit);

                    if($achieveAll > 0){
                        echo '<div class="col-md-6">';
                        // echo '<div style="border: 1px solid var(--borderColor); margin: 4px;">';
                        echo \dosamigos\highcharts\HighCharts::widget([
                            'clientOptions' => [
                                'chart' => [
                                        'type' => 'pie'
                                ],
                                'credits' => [
                                    'enabled' => false
                                ],
                                'title' => [
                                    'text' => 'Kuartal ' . $i, 
                                    'style' => ['color' => 'var(--borderColor)', 'font-weight' => 'bold', 'font-size' => '17px']
                                ],
                                'plotOptions' => [
                                    'series' => [
                                        'dataLabels' => [
                                            'enabled' => true,
                                            'format' => '{point.name}: {point.y} ({point.percentage:.1f}%)'
                                        ]
                                    ]
                                ],
                                'tooltip' => [
                                    'headerFormat' => '<span style="font-size:11px">{point.name}</span><br>',
                                    'pointFormat' => '{point.name}: {point.y} ({point.percentage:.1f}%)'
                                ],
                                'series' => [
                                    [
                                        'colorByPoint' => true,
                                        'data' => $val
                                    ],
                                ],
                            ]
                        ]);
                        // echo '</div>';
                        echo '</div>';
                    }

                    $i++;
                    $val = [];
                }
                echo '</div>'
            ?>
        </div>
    </div>
<?php endif; ?>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<script>
    function changeFilterYear(val) {
        window.location.replace("/dashboard/unit2?year=" + val);
    }
    function changeFilterUnit(val) {
        var year = $("#year-filter").val();
        window.location.replace("/dashboard/unit2?year=" + year + "&UnitStructureSearch[id]=" + val);
    }
</script>