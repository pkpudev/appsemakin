<?php

use app\models\PortfolioCanvas;
use app\models\PortfolioElement;
use kartik\date\DatePicker;
use yii\widgets\Pjax;

$this->title = 'Strategic Plan ' . $year;

?>
<style>
    .box-dashboard {
        border: 1px solid var(--background);
        border-radius: 5px;
        padding: 5px; 
        text-align: center; 
        margin: 5px auto;
    }

    .place-mission {
        width: 700px;
        margin: auto;
    }

    p {
        border-top: 2px dashed;
        border-color: var(--borderColor) !important;
        margin:0; padding: 30px;
    }

    p:nth-child(even) {
        border-left: 2px dashed;
        border-top-left-radius: 30px;
        border-bottom-left-radius: 30px;
        margin-right: 30px; 
        padding-right: 0;
    }

    p:nth-child(odd) {
        border-right: 2px dashed;
        border-top-right-radius: 30px;
        border-bottom-right-radius: 30px;
        margin-left: 30px; 
        padding-left: 0;
    }

    p:first-child {
        border-top: 0;
        border-top-right-radius:0;
        border-top-left-radius:0;
    }

    p:last-child {
        border-bottom-right-radius:0;
        border-bottom-left-radius:0;
    }
</style>
<div class="plan">
    
    <div class="box box-default">
        <div class="box-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-6" style="padding-left: 0 !important; padding-right: 0 !important;">
                        <div class="box-dashboard">
                            <center><b style="color: var(--borderColor);font-size: 17px;">Filter</b></center>
                            <table width="100%">
                                <tr>
                                    <td width="100%">
                                        <?= DatePicker::widget([
                                            'name' => 'year-filter',
                                            'value' => $year,
                                            'options' => [
                                                'id' => 'year',
                                                'placeholder' => 'Filter Tahun',
                                                'onchange' => 'changeFilterYear($(this).val())',
                                            ],
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'startView'=>'year',
                                                'minViewMode'=>'years',
                                                'format' => 'yyyy',
                                                'allowClear' => false
                                            ]
                                        ]); ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box-dashboard">
                        <center><b style="color: var(--borderColor);font-size: 17px;">VISI</b></center>
                        <h3><?= $vision ? $vision->vision : '-'; ?></h3>

                        <center><b style="color: var(--borderColor);font-size: 17px;">MISI</b></center>
                        <?php
                            if($missions){
                                $i = 1;
                                echo "<div class='place-mission'>";
                                foreach($missions as $mission){
                                    echo '<p>' . $mission->mission . '</p>';

                                    $i++;
                                }
                                echo "</div>";
                            } else {
                                echo '<center><b>-</b></center>';
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box-dashboard" style="text-align: left;">
                        <center><b style="color: var(--borderColor);font-size: 17px;">MODEL BISNIS</b></center>

                        <?php if($portfolio) : ?>
                        <center><b style="color: var(--borderColor);font-size: 17px;"><?= $portfolio->portfolio_name; ?></b></center>
                            <table width="100%" class="table table-bordered" style="margin-top: 10px; margin-bottom: 0;">
                                <tr>
                                    <td width="172px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::VALUE_STREAMS)->element; ?></b><br />
                                        <i><?= PortfolioElement::findOne(PortfolioElement::VALUE_STREAMS)->description ?: '-'; ?></i>
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::VALUE_STREAMS)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::VALUE_STREAMS])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                    <td width="172px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::SOLUSTIONS)->element; ?></b><br />
                                        <i><?= PortfolioElement::findOne(PortfolioElement::SOLUSTIONS)->description ?: '-'; ?></i>
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::SOLUSTIONS)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::SOLUSTIONS])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                    <td width="172px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::CUSTOMERS)->element; ?></b><br />
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::CUSTOMERS)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::CUSTOMERS])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                    <td width="172px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::CHANNELS)->element; ?></b><br />
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::CHANNELS)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::CHANNELS])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                    <td width="172px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::CUSTOMER_RELATIONSHIPS)->element; ?></b><br />
                                        <i><?= PortfolioElement::findOne(PortfolioElement::CUSTOMER_RELATIONSHIPS)->description ?: '-'; ?></i>
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::CUSTOMER_RELATIONSHIPS)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::CUSTOMER_RELATIONSHIPS])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                    <td width="172px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::BUDGET)->element; ?></b><br />
                                        <i><?= PortfolioElement::findOne(PortfolioElement::BUDGET)->description ?: '-'; ?></i>
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::BUDGET)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::BUDGET])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                    <td width="172px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::KPI_REVENUE)->element; ?></b><br />
                                        <i><?= PortfolioElement::findOne(PortfolioElement::KPI_REVENUE)->description ?: '-'; ?></i>
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::KPI_REVENUE)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::KPI_REVENUE])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" class="table table-bordered" style="margin-top: -1px; margin-bottom: 0;">
                                <tr>
                                    <td width="401px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::KEY_PARTNERS)->element; ?></b><br />
                                        <i><?= PortfolioElement::findOne(PortfolioElement::KEY_PARTNERS)->description ?: '-'; ?></i>
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::KEY_PARTNERS)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::KEY_PARTNERS])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                    <td width="401px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::KEY_ACTIVITIES)->element; ?></b><br />
                                        <i><?= PortfolioElement::findOne(PortfolioElement::KEY_ACTIVITIES)->description ?: '-'; ?></i>
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::KEY_ACTIVITIES)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::KEY_ACTIVITIES])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                    <td width="401px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::KEY_RESOURCES)->element; ?></b><br />
                                        <i><?= PortfolioElement::findOne(PortfolioElement::KEY_RESOURCES)->description ?: '-'; ?></i>
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::KEY_RESOURCES)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::KEY_RESOURCES])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" class="table table-bordered" style="margin-top: -1px;">
                                <tr>
                                    <td width="602px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::COST_STRUCTURE)->element; ?></b><br />
                                        <i><?= PortfolioElement::findOne(PortfolioElement::COST_STRUCTURE)->description ?: '-'; ?></i>
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::COST_STRUCTURE)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::COST_STRUCTURE])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                    <td width="602px">
                                        <b style="font-size: 13px; color: var(--borderColor);"><?= PortfolioElement::findOne(PortfolioElement::REVENUE_STREAMS)->element; ?></b><br />
                                        <i><?= PortfolioElement::findOne(PortfolioElement::REVENUE_STREAMS)->description ?: '-'; ?></i>
                                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::REVENUE_STREAMS)->element)))  . '-pjax']) ?>
                                            <div style="font-size: 12px;">
                                                <?php
                                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::REVENUE_STREAMS])->andWhere(['portfolio_id' => $portfolio->id])->orderBy('id asc')->all();
                                                    foreach($canvas as $cnv){
                                                        echo '&#8226; ' . $cnv->description . '<br />';
                                                    }
                                                ?>
                                            </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                </tr>
                            </table>
                        <?php else: ?>
                            <center><b>-</b></center>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<script>
    function changeFilterYear(val) {
        var year = val;

        window.location.replace("/dashboard/plan?year=" + year);
    }
</script>