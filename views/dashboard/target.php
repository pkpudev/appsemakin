<?php

use app\models\Target;
use Beta\Microsoft\Graph\Model\Currency;
use dosamigos\highcharts\HighCharts;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Target Penghimpunan & Penerima Manfaat ' . $year;

?>
<style>
    .box-dashboard {
        border: 1px solid var(--background);
        border-radius: 5px;
        padding: 5px;  
        margin: 5px auto;
    }
</style>

<div class="plan">
    
    <div class="box box-default">
        <div class="box-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="box-dashboard">
                        <center><b style="color: var(--borderColor);font-size: 17px;">Filter</b></center>
                        <table width="100%">
                            <tr>
                                <td width="100%">
                                    <?= DatePicker::widget([
                                        'name' => 'year-filter',
                                        'value' => $year,
                                        'options' => [
                                            'id' => 'year',
                                            'placeholder' => 'Filter Tahun',
                                            'onchange' => 'changeFilterYear($(this).val())',
                                        ],
                                        'pluginOptions' => [
                                            'autoclose'=>true,
                                            'startView'=>'year',
                                            'minViewMode'=>'years',
                                            'format' => 'yyyy',
                                            'allowClear' => false
                                        ]
                                    ]); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-dashboard">
                        <center><b style="color: var(--borderColor);font-size: 17px;">Target Total Penghimpunan</b></center>
                        <h3 style="margin-top: 0px; margin-bottom: 8px; text-align: center;"><?= number_format($totalCollection, 0, ',', '.'); ?></h3>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-dashboard">
                        <center><b style="color: var(--borderColor);font-size: 17px;">Target Total Penerima Manfaat</b></center>
                        <h3 style="margin-top: 0px; margin-bottom: 8px; text-align: center;"><?= number_format($totalBenefitRecipient, 0, ',', '.'); ?></h3>
    
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box-dashboard">
                        <b style="color: var(--borderColor);font-size: 17px;">Target Penghimpunan per Bulan</b><br />
                        <i>Target dibanding dengan Donasi Tahun Lalu</i>
                        
                        <?= HighCharts::widget([
                                'clientOptions' => [
                                    'title' => [
                                        'text' => '', 
                                        'style' => ['color' => 'var(--borderColor)', 'font-weight' => 'bold', 'font-size' => '17px']
                                    ],
                                    'xAxis' => [
                                        'categories' => $monthName
                                    ],
                                    'credits' => [
                                        'enabled' => false
                                    ],
                                    'series' => [
                                        [
                                            'name' => 'Target PHP ' . $year, 
                                            'data' => $monthlyTargetCompany,
                                        ],
                                        [
                                            'name' => 'Realisasi PHP ' . ($year-1), 
                                            'data' => $monthlyRealisasiTargetCompany,
                                        ],
                                    ],
                                    'colors' => ['rgb(255, 102, 102)', 'rgb(102, 102, 255)', 'rgb(77, 255, 121)', 'rgb(255, 210, 77)'],
                                ]
                            ]); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box-dashboard">
                        <?= GridView::widget([
                            'id'=>'okr-summary',
                            'dataProvider' => $dataProviderUnit,
                            'filterModel' => $searchModelUnit,
                            'layout' => '{items}{pager}',
                            'pjax'=>true,
                            'hover'=>true,
                            'export' => [
                                'label' => 'Export Excel atau PDF',
                                'header' => '',
                                'options' => [
                                    'class' => 'btn btn-primary'
                                ], 
                                'menuOptions' => [ 
                                        'style' => 'background: var(--background); width: 187px;'
                                ]
                            ],
                            'exportConfig' => [
                                GridView::PDF => true,
                                GridView::EXCEL => true,
                            ],
                            'emptyText' => 'Tidak ada data Unit.',
                            'tableOptions' => [
                                'class' => 'table table-responsive table-striped table-bordered'
                            ],
                            'showPageSummary' => true,
                            'columns' => [
                                [
                                    'class' => 'kartik\grid\SerialColumn',
                                    'width' => '30px',
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'attribute' => 'unit_code',
                                    'value' => function($model){
                                        return $model->unit_code ?: '-';
                                    }
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'attribute' => 'unit_name',
                                    'value' => function($model){
                                        return $model->unit_name ?: '-';
                                    },
                                    'pageSummary' => 'Total',
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Target',
                                    'format' => 'Currency',
                                    'value' => function($model){
                                        $year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');
                                        $sum = Target::find()
                                                        ->where(['target_type_id' => [1, 11, 12, 16, 17, 18, 19, 20, 21, 22]])
                                                        ->andWhere(['year' => $year])
                                                        ->andWhere(['unit_structure_id' => $model->id])
                                                        ->sum('total_target');
                                        // return number_format($sum ?: 0, 0, ",", ".");
                                        return $sum ?: 0;
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Total Donasi Tahun Lalu',
                                    'format' => 'Currency',
                                    'value' => function($model){
                                        $year = (Yii::$app->getRequest()->getQueryParam('year') ?: date('Y'))-1;
                                        $sum = Yii::$app->db->createCommand("
                                                                SELECT
                                                                    real_month_01 + real_month_02 + real_month_03 + real_month_04 + real_month_05 + real_month_06 + real_month_07 + real_month_08 + real_month_09 + real_month_10 + real_month_11 + real_month_12 as sum
                                                                from 
                                                                    management.target t
                                                                left join management.unit_structure us on t.unit_structure_id = us.id
                                                                where 
                                                                    t.year = $year and
                                                                    target_type_id in (1, 11, 12, 16, 17, 18, 19, 20, 21, 22) and
                                                                    us.unit_id = {$model->unit_id}
                                                                ")->queryOne();
                                    
                                        // return number_format($sum['sum'] ?: 0, 0, ",", ".");
                                        return $sum['sum'] ?: 0;
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ],
                            ],
                            'toolbar'=> [
                                    ['content'=>
                                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                                        '{toggleData}'
                                    ],
                                    ['content'=>
                                        '{export}'
                                    ],
                                ],
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'panel' => [
                                    'type' => 'primary',
                                    'heading' => 'Tabel Target Penghimpunan per Kantor/Unit',
                                    'before' => '',
                                    'after'=>'',
                                ]
                        ]) ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box-dashboard">
                        <?= \dosamigos\highcharts\HighCharts::widget([
                                'clientOptions' => [
                                    'chart' => [
                                            'type' => 'column'
                                    ],
                                    'credits' => [
                                        'enabled' => false
                                    ],
                                    'title' => [
                                        'text' => 'Target Penghimpunan per Segment', 
                                        'style' => ['color' => 'var(--borderColor)', 'font-weight' => 'bold', 'font-size' => '17px']
                                    ],
                                    'xAxis' => [
                                        'categories' => $monthName
                                    ],
                                    'yAxis' => [
                                        'min' => 0,
                                        'title' => [
                                            'text' => 'Target'
                                        ],
                                    ],
                                    'tooltip' => [
                                        'pointFormat' => '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                                        'shared' => true
                                    ],
                                    'plotOptions' => [
                                        'column' => [
                                            'stacking' => 'percent',
                                            'dataLabels' =>  [
                                                'format' => '{point.percentage:.0f}%',
                                                'enabled' =>  true
                                            ]
                                        ]
                                    ],
                                    'series' => [
                                        [
                                            'name' => 'Individu',
                                            'data' => $collectionSegmentIndividual
                                        ], 
                                        [
                                            'name' => 'Institusi',
                                            'data' => $collectionSegmentInstitution
                                        ], 
                                    ],
                                    'colors' => ['rgb(87, 86, 93)', 'rgb(51, 153, 255)'],
                                ]
                            ]);
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="box-dashboard">
                        <?= \dosamigos\highcharts\HighCharts::widget([
                                'clientOptions' => [
                                    'chart' => [
                                            'type' => 'pie'
                                    ],
                                    'credits' => [
                                        'enabled' => false
                                    ],
                                    'title' => [
                                        'text' => 'Target Penghimpunan per Value Stream', 
                                        'style' => ['color' => 'var(--borderColor)', 'font-weight' => 'bold', 'font-size' => '17px']
                                    ],
                                    'plotOptions' => [
                                        'series' => [
                                            'dataLabels' => [
                                                'enabled' => true,
                                                'format' => '{point.name}: {point.x} ({point.y:.1f}%)'
                                            ]
                                        ]
                                    ],
                                    'tooltip' => [
                                        'headerFormat' => '<span style="font-size:11px">{point.name}</span><br>',
                                        'pointFormat' => '{point.name}: {point.x} ({point.y:.1f}%)'
                                    ],
                                    'series' => [
                                        [
                                            'colorByPoint' => true,
                                            'data' => $arrayCollection
                                        ],
                                    ],
                                ]
                            ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-dashboard">
                        <?= \dosamigos\highcharts\HighCharts::widget([
                                'clientOptions' => [
                                    'chart' => [
                                            'type' => 'pie'
                                    ],
                                    'credits' => [
                                        'enabled' => false
                                    ],
                                    'title' => [
                                        'text' => 'Target Penerima Manfaat per Value Stream', 
                                        'style' => ['color' => 'var(--borderColor)', 'font-weight' => 'bold', 'font-size' => '17px']
                                    ],
                                    'plotOptions' => [
                                        'series' => [
                                            'dataLabels' => [
                                                'enabled' => true,
                                                'format' => '{point.name}: {point.x} ({point.y:.1f}%)'
                                            ]
                                        ]
                                    ],
                                    'tooltip' => [
                                        'headerFormat' => '<span style="font-size:11px">{point.name}</span><br>',
                                        'pointFormat' => '{point.name}: {point.x} ({point.y:.1f}%)'
                                    ],
                                    'series' => [
                                        [
                                            'colorByPoint' => true,
                                            'data' => $arrayBenefitRecipient
                                        ],
                                    ],
                                ]
                            ]);
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function changeFilterYear(val) {
        var year = val;

        window.location.replace("/dashboard/target?year=" + year);
    }
</script>