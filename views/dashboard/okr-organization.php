<?php

use app\models\Activity;
use app\models\Budget;
use app\models\Ceiling;
use app\models\Initiatives;
use app\models\Okr;
use dosamigos\highcharts\HighCharts;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\JsExpression;

$this->title = 'OKR Organisasi';
?>
<style>
    .box-dashboard {
        border: 1px solid var(--background);
        border-radius: 5px;
        padding: 5px; text-align: center; margin: 1px auto;
    }

    .target-link {
        font-size: 17px;
        height: 75px;
        display: inline-block;
        width: 100%;
        background: var(--background);
        color: #fff !important;
        padding-top: 26px;
        border: 1px solid var(--background);
        border-radius: 5px;
        text-align: center; margin: 1px auto;
    }

    .target-link:hover {
        color: var(--background) !important;
        background: #fff;
    }
</style>
<div class="dashboard-page">

    <div class="box box-default">
        <div class="box-body">

            <div class="row">
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <label style="color: var(--borderColor);font-size: 17px;">Filter Tahun</label>
                        <?= DatePicker::widget([
                            'name' => 'year-filter',
                            'value' => $yearFilter,
                            'options' => [
                                'placeholder' => 'Filter Tahun',
                                'onchange' => 'changeYear($(this).val())',
                            ],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'startView'=>'year',
                                'minViewMode'=>'years',
                                'format' => 'yyyy',
                                'allowClear' => false
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
            
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;"><?= $countObjectivesOrg; ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;">Objectives Organisasi</b>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;"><?= $countKeyResultsOrg; ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;">Key Result Organisasi</b>
                    </div>
                </div>
                <div class="col-md-3">
                    <div style="padding: 0;">
                        <b><?= Html::a('Lihat Detail OKR Organisasi', ['/strategic/okr/organization?OkrOrgSearch[start_year]=' . $yearFilter], ['class' => 'target-link', 'target' => '_blank']); ?></b>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 10px;">
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;"><?= $countInitiatives; ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;">Program Kerja</b>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-dashboard">
                        <h3 style="margin-top: 5px;"><?= $countActivity; ?></h3>
                        <b style="color: var(--borderColor);font-size: 17px;"> <?= $yearFilter < 2023 ? 'Kegiatan' : 'Indikator Keberhasilan'; ?></b>
                    </div>
                </div>
            </div>

            <!-- // OKR SUMMARY -->
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-12">
                    <div class="box-dashboard" style="text-align: left;">
                        <?= GridView::widget([
                            'id'=>'okr-summary',
                            'dataProvider' => $dataProviderUnit,
                            'filterModel' => $searchModelUnit,
                            'layout' => '{items}{pager}',
                            'pjax'=>true,
                            'hover'=>true,
                            'export' => [
                                'label' => 'Export Excel atau PDF',
                                'header' => '',
                                'options' => [
                                    'class' => 'btn btn-primary'
                                ], 
                                'menuOptions' => [ 
                                        'style' => 'background: var(--background); width: 187px;'
                                ]
                            ],
                            'exportConfig' => [
                                GridView::PDF => true,
                                GridView::EXCEL => true,
                            ],
                            'emptyText' => 'Tidak ada data Unit.',
                            'tableOptions' => [
                                'class' => 'table table-responsive table-striped table-bordered'
                            ],
                            'showPageSummary' => true,
                            'columns' => [
                                [
                                    'class' => 'kartik\grid\SerialColumn',
                                    'width' => '30px',
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'attribute' => 'unit_code',
                                    'value' => function($model){
                                        return $model->unit_code ?: '-';
                                    }
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'attribute' => 'unit_name',
                                    'value' => function($model){
                                        return $model->unit_name ?: '-';
                                    },
                                    'pageSummary' => 'Total',
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Jumlah Objective',
                                    'value' => function($model){
                                        $year = Yii::$app->getRequest()->getQueryParam('yearFilter') ?: date('Y');
                                        $count = Okr::find()
                                                        ->alias('o')
                                                        ->joinWith('position p')
                                                        ->where(['p.unit_structure_id' => $model->id])
                                                        ->andWhere(['o.type' => 'O'])
                                                        ->andWhere(['o.year' => $year])
                                                        ->andWhere('o.okr_level_id <> 5')
                                                        ->count();
                                        return number_format($count ?: 0, 0, ",", ".");
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Jumlah Key Result',
                                    'value' => function($model){
                                        $year = Yii::$app->getRequest()->getQueryParam('yearFilter') ?: date('Y');
                                        $count = Okr::find()
                                                        ->alias('o')
                                                        ->joinWith('position p')
                                                        ->where(['p.unit_structure_id' => $model->id])
                                                        ->andWhere(['o.type' => 'KR'])
                                                        ->andWhere(['o.year' => $year])
                                                        ->andWhere('o.okr_level_id <> 5')
                                                        ->count();
                                        return number_format($count ?: 0, 0, ",", ".");
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ],
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'label' => 'Jumlah Proker',
                                    'value' => function($model){
                                        $year = Yii::$app->getRequest()->getQueryParam('yearFilter') ?: date('Y');
                                        $count = Initiatives::find()
                                                                ->alias('i')
                                                                ->joinWith('position p')
                                                                ->where(['p.unit_structure_id' => $model->id])
                                                                ->andWhere(['p.year' => $year])
                                                                ->count();
                                        return number_format($count ?: 0, 0, ",", ".");
                                    },
                                    'hAlign' => 'right',
                                    'pageSummary' => true,
                                ],
                            ],
                            'toolbar'=> [
                                    ['content'=>
                                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                                        '{toggleData}'
                                    ],
                                    ['content'=>
                                        '{export}'
                                    ],
                                ],
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'panel' => [
                                    'type' => 'primary',
                                    'heading' => '<i class="fa fa-list-alt"></i> OKR Summary',
                                    'before' => '',
                                    'after'=>'',
                                ]
                        ]) ?>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

</div>
<script>
    function changeYear(val) {
        var year = val;

        window.location.replace("/dashboard/okr-organization?yearFilter=" + year);
    }
</script>