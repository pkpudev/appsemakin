<?php

use app\models\Budget;
use app\models\FinancingType;
use app\models\FinancingTypeCeiling;

$financingTypes = FinancingType::find()->orderBy('id')->all();
?>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default" style="background: #fff !important;">
                <div class="box-body">
                    <label>Jenis Pembiayaan Pagu Anggaran</label>
                    <table class="kv-grid-table table table-bordered table-striped table-condensed kv-table-wrap">
                        <thead>
                            <tr>
                                <?php foreach($financingTypes as $ft): ?>
                                    <th style="text-align: end;"><?= str_replace('Biaya', 'Pagu', $ft->financing_type); ?></th>
                                    <th style="text-align: end;">Anggaran <?= str_replace('Biaya', '', $ft->financing_type); ?></th>
                                <?php endforeach; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php foreach($financingTypes as $ft): ?>
                                    <?php 
                                        $pagu = 0;
                                        $anggaran = 0;
                                        $pagu = FinancingTypeCeiling::findOne(['unit_structure_id' => $model->unit_structure_id, 'financing_type_id' => $ft->id])->ceiling_amount ?: 0;
                                        $anggaran = Budget::find()
                                                                ->alias('b')
                                                                ->joinWith(['initiatives i', 'initiatives.okr o', 'initiatives.okr.position p'])
                                                                ->where(['p.unit_structure_id' => $model->unit_structure_id])
                                                                ->andWhere(['p.id' => $model->id])
                                                                ->andWhere(['b.financing_type_id' => $ft->id])
                                                                ->sum('total_amount'); 
                                    ?>
                                    <td align="right">Rp. <?= number_format(($pagu ?: 0), 0, ',', '.'); ?></td>
                                    <td align="right" style="<?= $anggaran > $pagu ? 'background: #e16151; color: #fff;' : ''; ?>">Rp. <?= number_format(($anggaran ?: 0), 0, ',', '.'); ?></td>
                                <?php endforeach; ?>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>