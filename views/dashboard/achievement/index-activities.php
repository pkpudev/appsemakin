<?php

use app\models\Activities;
use app\models\Employee;
use app\models\PositionStructure;
use app\models\Status;
use app\models\UnitStructure;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\bootstrap\Progress;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Ringkasan Monitoring';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="activities-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'activities-crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'unit_id',
                    'filter' => false,
                    'format' => 'html',
                    'value' => function ($model, $key, $index, $widget) {
                        return '<b>' . $model->position->unit->unit_code . ' ' . $model->position->unit->unit_name . '</b>';
                    },
                    'group' => true,
                    'groupedRow' => true,                    // move grouped column to a single grouped row
                    'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
                    'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
                ],
                /* [
                    'class'=>'\kartik\grid\DataColumn',
                    // 'attribute' => 'parent_okr',
                    'label' => 'Objective Unit',
                    'format' => 'raw',
                    'value' => function($model){
                        return '<b>' . $model->parent->parent->parent->parent->okr_code . '</b><br />' . $model->parent->parent->parent->parent->okr;
                    },
                    'width' => '200px',
                    'group' => true,
                    // 'subGroupOf' => 1
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    // 'attribute' => 'okr',
                    'label' => 'Key Result Unit',
                    'width' => '200px',
                    'format' => 'raw',
                    'value' => function($model){
                        return '<b>' . $model->parent->parent->okr_code . '</b><br />' . $model->parent->parent->okr;
                    },
                    'group' => true,
                ], */
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'label' => 'Nama',
                    'attribute'=>'pic_id',
                    'value'=>function ($model) {
                        return $model->pic->full_name;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(Employee::find()->where(['and', ['user_status'=>1], ['is_employee'=>1], ['<>', 'employee_type_id', 4]])->orderBy('full_name')->all(), 'id', 'full_name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>''],
                    'width' => '150px',
                    'group'=>true,
                    'subGroupOf' => 1
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute' => 'parent_okr',
                    'label' => 'Objective',
                    'format' => 'raw',
                    'value' => function($model){
                        return '<b>' . $model->parent->okr_code . '</b><br />' . $model->parent->okr;
                    },
                    'width' => '200px',
                    'group' => true,
                    'subGroupOf' => 1
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute' => 'okr',
                    'label' => 'Key Result',
                    'width' => '200px',
                    'format' => 'raw',
                    'value' => function($model){
                        return '<b>' . $model->okr_code . '</b><br />' . $model->okr;
                    }
                ],
                [  
                    'class'=>'\kartik\grid\DataColumn',
                    'label' => 'Jumlah Kegiatan',
                    'value' => function($model){
                        $cnt = Activities::find()->where(['okr_id' => $model->id])->andWhere(['parent_id' => null])->count();
                        return $cnt ?: 0;
                    },
                    'hAlign' => 'center',
                    // 'headerOptions' => ['style' => 'background-color: var(--info); color: #fff;'],
                    // 'contentOptions' => ['style' => 'background-color: var(--info); color: #fff;'],
                    'width' => '120px'
                ],
                [  
                    'class'=>'\kartik\grid\DataColumn',
                    'label' => 'Belum Dimulai',
                    'value' => function($model){
                        $cnt = Activities::find()->where(['okr_id' => $model->id])->andWhere(['parent_id' => null])->andWhere(['status_id' => Status::ACT_NEW])->count();
                        return $cnt ?: 0;
                    },
                    'hAlign' => 'center',
                    // 'headerOptions' => ['style' => 'background-color: var(--default); color: #fff;'],
                    // 'contentOptions' => ['style' => 'background-color: var(--default); color: #fff;'],
                    'width' => '120px'
                ],
                [  
                    'class'=>'\kartik\grid\DataColumn',
                    'label' => 'Sedang Berlangsung',
                    'value' => function($model){
                        $cnt = Activities::find()->where(['okr_id' => $model->id])->andWhere(['parent_id' => null])->andWhere(['status_id' => Status::ACT_ON_PROGRESS])->count();
                        return $cnt ?: 0;
                    },
                    'hAlign' => 'center',
                    // 'headerOptions' => ['style' => 'background-color: var(--warning); color: #fff;'],
                    // 'contentOptions' => ['style' => 'background-color: var(--warning); color: #fff;'],
                    'width' => '120px'
                ],
                [  
                    'class'=>'\kartik\grid\DataColumn',
                    'label' => 'Selesai',
                    'value' => function($model){
                        $cnt = Activities::find()->where(['okr_id' => $model->id])->andWhere(['parent_id' => null])->andWhere(['status_id' => Status::ACT_DONE])->count();
                        return $cnt ?: 0;
                    },
                    'hAlign' => 'center',
                    // 'headerOptions' => ['style' => 'background-color: var(--success); color: #fff;'],
                    // 'contentOptions' => ['style' => 'background-color: var(--success); color: #fff;'],
                    'width' => '120px'
                ],
                [  
                    'class'=>'\kartik\grid\DataColumn',
                    'label' => 'Dibatalkan',
                    'value' => function($model){
                        $cnt = Activities::find()->where(['okr_id' => $model->id])->andWhere(['parent_id' => null])->andWhere(['status_id' => Status::ACT_CANCEL])->count();
                        return $cnt ?: 0;
                    },
                    'hAlign' => 'center',
                    // 'headerOptions' => ['style' => 'background-color: var(--danger); color: #fff;'],
                    // 'contentOptions' => ['style' => 'background-color: var(--danger); color: #fff;'],
                    'width' => '120px'
                ],
                /* [
                    'class'=>'\kartik\grid\DataColumn',
                    'label' => 'Nilai OKR',
                    'attribute' => 'achievement_percentage',
                    'value' => function($model){
                        return $model->achievement_percentage ? round($model->achievement_percentage, 2) . '%' : '-';
                    },
                    'hAlign' => 'center',
                    'width' => '120px'
                ], */
            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ]
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Kegiatan Kerja',
                'before'=>
                '<table><tr>' .
                    '<td width="150px" style="padding-right: 2px;">' .
                        DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'year',
                            'pjaxContainerId' => 'activities-crud-datatable-pjax',
                            'options' => [
                                'id' => 'year-filter',
                                'class' => 'form-control additional-filter',
                                'placeholder'=>'-- Filter Tahun --',
                            ],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'startView'=>'year',
                                'minViewMode'=>'years',
                                'format' => 'yyyy',
                                'allowClear' => false
                            ]
                        ]) .
                    '</td>' .
                    '<td width="300px">' . Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'unit_id',
                        'pjaxContainerId' => 'activities-crud-datatable-pjax',
                        'options' => [
                            'id' => 'unit_id-filter',
                            'class' => 'form-control additional-filter',
                            'placeholder'=>'-- Filter Unit --',
                        ],
                        'pluginOptions' => ['allowClear' => true],
                        'data' => ArrayHelper::map(UnitStructure::find()->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                            return $model->unit_code . ' - ' . $model->unit_name;
                        }),
                    ]) . '</td>'.
                '</tr></table>',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>