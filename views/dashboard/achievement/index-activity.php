<?php
use app\models\Employee;
use app\models\PositionStructure;
use app\models\UnitStructure;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\Progress;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Capaian Kegiatan Kerja';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="activity-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'activity-crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'attribute'=>'employee_id',
                    'value'=>function ($model) {
                        return $model->employee->full_name;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(Employee::find()->where(['and', ['user_status'=>1], ['is_employee'=>1], ['<>', 'employee_type_id', 4]])->orderBy('full_name')->all(), 'id', 'full_name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['id'=>'employee_id_2', 'placeholder'=>''],
                    'group'=>true,
                    'width' => '280px',
                ],
                [
                    'attribute'=>'unit_structure_id',
                    'label' => 'Unit',
                    'value'=>function ($model) {
                        return $model->positionStructure->unit->unit_name;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(UnitStructure::find()->where(['year'=>date('Y')])->orderBy('unit_name')->all(), 'id', 'unit_name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['id'=>'unit_structure_id_2', 'placeholder'=>''],
                    'group'=>true,
                    'width' => '280px',
                ],
                [
                    'attribute'=>'position_structure_id',
                    'value'=>function ($model) {
                        return $model->positionStructure->position_name;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(PositionStructure::find()->orderBy('position_name')->all(), 'id', 'position_name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['id'=>'position_structure_id_2', 'placeholder'=>''],
                    'group'=>true,
                    'width' => '280px',
                ],
                [
                    'attribute'=>'achieve_value',
                    'filter'=>false,
                    'value'=>function ($model) {
                        return Progress::widget([
                            'percent' => $model->achieve_value,
                            'label' => $model->achieve_value.'%',
                        ]);
                    },
                    'format'=>'raw',
                    'width' => '280px',
                ],
                [
                    'attribute'=>'fulfillment',
                    'filter'=>false,
                    'value'=>function ($model) {
                        return $model->fulfillment.' Kegiatan';
                    },
                    'width' => '120px',
                ],
            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ]
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Kegiatan Kerja',
                'before'=>'',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>