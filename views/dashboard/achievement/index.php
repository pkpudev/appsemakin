<?php

use app\models\Evaluation;
use app\models\UnitStructure;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

$this->title = 'Capaian Unit Kerja';
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .progress-bar {
        color: #f00 !important;
        }
</style>

<div class="row">
    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-body">
                <div class="box-dashboard">
                    <center><b style="color: var(--borderColor);font-size: 17px;">Filter</b></center>
                    <table width="100%">
                        <tr>
                            <td width="150px">
                                <?= Select2::widget([
                                    'name' => 'evaluation_id-filter',
                                    'value' => $evaluation_id,
                                    'options' => [
                                        'id' => 'evaluation_id-filter',
                                        'placeholder' => '-- Filter Periode --',
                                        'onchange' => 'changeFilterEvaluation($(this).val())',
                                    ],
                                    'pluginOptions' => ['allowClear' => false],
                                    'data' => ArrayHelper::map(Evaluation::find()->orderBy('year asc, period asc')->all(), 'id', function($model){
                                        return $model->year . ' - ' . $model->period;
                                    }),
                                    ])
                                ?>
                            </td>
                            <td style="padding-left: 5px;">
                                <?= Select2::widget([
                                        'name' => 'directorate-filter',
                                        'value' => $directorate_id,
                                        'pjaxContainerId' => 'crud-datatable-pjax',
                                        'options' => [
                                            'id' => 'directorate-filter',
                                            'placeholder' => '-- Filter Directorate --',
                                            'onchange' => 'changeFilterDirectorate($(this).val())',
                                        ],
                                        'pluginOptions' => ['allowClear' => true],
                                        'data' => $listDirectorate,
                                    ])
                                ?>
                            </td>
                            <td style="padding-left: 5px;">
                                <?= Select2::widget([
                                        'name' => 'department-filter',
                                        'value' => $department_id,
                                        'pjaxContainerId' => 'crud-datatable-pjax',
                                        'options' => [
                                            'id' => 'department-filter',
                                            'placeholder' => '-- Filter Department --',
                                            'onchange' => 'changeFilterDepartment($(this).val())',
                                        ],
                                        'pluginOptions' => ['allowClear' => true],
                                        'data' => $listDepartment,
                                    ])
                                ?>
                            </td>
                            <td style="padding-left: 5px;">
                                <?= Select2::widget([
                                    'name' => 'unit-filter',
                                    'value' => $unit_id,
                                    'options' => [
                                        'id' => 'unit-filter',
                                        'placeholder' => '-- Filter Unit --',
                                        'onchange' => 'changeFilterUnit($(this).val())',
                                        // 'multiple' => true
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                    'data' => $listUnit,
                                    ])
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->render('index-unit', [
    'searchModel' => $searchModelUnit,
    'dataProvider' => $dataProviderUnit,
]) ?>

<div class="row">
    <div class="col-md-6">
        <?= $this->render('index-unit-okr', [
            'searchModel' => $searchModelUnitOkr,
            'dataProvider' => $dataProviderUnitOkr,
        ]) ?>
    </div>
    <div class="col-md-6">
        <?= $this->render('index-unit-initiatives', [
            'searchModel' => $searchModelUnitInitiatives,
            'dataProvider' => $dataProviderUnitInitiatives,
        ]) ?>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<script>
    function changeFilterUnit(val) {
        var unitId = val;
        var evaluationId = $("#evaluation_id-filter").val();
        if(evaluationId === 'undefined') evaluationId = '';

        window.location.replace("/dashboard/unit?UnitAchievement[unit_id]=" + unitId + "&OkrEvaluationSearch[unit_id]=" + unitId + "&InitiativesEvaluationSearch[unit_id]=" + unitId +
                                "&UnitAchievement[evaluation_id]=" + evaluationId + "&OkrEvaluationSearch[evaluation_id]=" + evaluationId + "&InitiativesEvaluationSearch[evaluation_id]=" + evaluationId);
    }
    function changeFilterDirectorate(val) {

        window.location.replace("/dashboard/unit?UnitAchievement[directorate_id]=" + val);
    }
    function changeFilterDepartment(val) {
        var evaluationId = $("#evaluation_id-filter").val();
        window.location.replace("/dashboard/unit?UnitAchievement[unit_id]=" + val + "&OkrEvaluationSearch[unit_id]=" + val + "&InitiativesEvaluationSearch[unit_id]=" + val +
                                "&UnitAchievement[evaluation_id]=" + evaluationId + "&OkrEvaluationSearch[evaluation_id]=" + evaluationId + "&InitiativesEvaluationSearch[evaluation_id]=" + evaluationId);
    }
    function changeFilterEvaluation(val) {
        var evaluationId = val;
        var unitId = $("#unit-filter").val();
        if(unitId === 'undefined') unitId = '';

        window.location.replace("/dashboard/unit?UnitAchievement[unit_id]=" + unitId + "&OkrEvaluationSearch[unit_id]=" + unitId + "&InitiativesEvaluationSearch[unit_id]=" + unitId +
                                "&UnitAchievement[evaluation_id]=" + evaluationId + "&OkrEvaluationSearch[evaluation_id]=" + evaluationId + "&InitiativesEvaluationSearch[evaluation_id]=" + evaluationId);
    }
</script>