<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'attribute'=>'employee_id',
        'value'=>function ($model) {
            return $model->employee->full_name;
        },
        'group'=>true
    ],
    [
        'attribute'=>'position_structure_id',
        'value'=>function ($model) {
            return $model->positionStructure->position_name;
        },
        'group'=>true
    ],
    [
        'attribute'=>'achieve_value',
        'filter'=>false
    ],
    [
        'attribute'=>'contribution_amount',
        'filter'=>false
    ],
    [
        'attribute'=>'fulfillment',
        'filter'=>false
    ],
];
