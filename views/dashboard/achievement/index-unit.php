<?php

use app\models\Evaluation;
use app\models\UnitStructure;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\bootstrap\Progress;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\UnitAchievement;

CrudAsset::register($this);

?>
<div class="unit-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'unit-crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                /* [
                    'label' => 'Periode',
                    'attribute' => 'evaluation_id',
                    'value'=>function ($model) {
                        return $model->evaluation->year . ' (' . $model->evaluation->period . ')';
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(Evaluation::find()->orderBy('id')->all(), 'id', function($model){
                        return $model->year . ' (' . $model->period . ')';
                    }),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>''],
                    'group'=>true,
                    'width' => '150px',
                ], */
                // [
                //     // 'attribute'=>'unit_id',
                //     'value'=>function ($model) {
                //         return $model->id;
                //     },
                // ],
                [
                    'attribute'=>'unit_id',
                    'value'=>function ($model) {
                        return $model->position->unit->unit_code . ' - ' . $model->position->unit->unit_name;
                    },
                    'filter'=>false,
                    'width' => '280px',
                ],
                [
                    'attribute'=>'achieve_value',
                    'label'=>'Capaian Key Result',
                    'filter'=>false,
                    'value'=>function ($model) {
                        return Progress::widget([
                            'percent' => $model->achieve_value,
                            'label' => $model->achieve_value.'%',
                        ]);
                    },
                    'format'=>'raw',
                    'width' => '280px',
                ],
                [
                    'attribute'=>'fulfillment',
                    'label'=>'Pengisian Key Result',
                    'filter'=>false,
                    'value'=>function ($model) {
                        return $model->fulfillment.'%';
                    },
                    'width' => '120px',
                ],
                [
                    'attribute'=>'achieve_value_initiatives',
                    'label'=>'Capaian Program Kerja',
                    'filter'=>false,
                    'value'=>function ($model) {
                        $achieve_initiatives = UnitAchievement::find()
                            ->where(['unit_id'=>$model->unit_id, 'evaluation_id'=>$model->evaluation_id, 'achieve_type'=>'IN'])
                            ->one();
                            
                        return Progress::widget([
                            'percent' => $achieve_initiatives->achieve_value,
                            'label' => $achieve_initiatives->achieve_value.'%',
                        ]);
                    },
                    'format'=>'raw',
                    'width' => '280px',
                ],
                [
                    'attribute'=>'fulfillment_initiatives',
                    'label'=>'Pengisian Program Kerja',
                    'filter'=>false,
                    'value'=>function ($model) {
                        $achieve_initiatives = UnitAchievement::find()
                            ->where(['unit_id'=>$model->unit_id, 'evaluation_id'=>$model->evaluation_id, 'achieve_type'=>'IN'])
                            ->one();
                        return $achieve_initiatives->fulfillment.'%';
                    },
                    'width' => '120px',
                ],
            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ]
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Unit Kerja',
                'before'=>'',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>