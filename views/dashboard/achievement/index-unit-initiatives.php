<?php
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Progress;
use yii\helpers\Html;

CrudAsset::register($this);

?>
<div class="initiatives-unit-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'initiatives-unit-crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'unit_id',
                    'filter' => false,
                    'format' => 'html',
                    'value' => function ($model, $key, $index, $widget) {
                        return $model->initiatives->okr->position->unit->unit_name;
                    },
                    'group' => true,
                    'groupedRow' => true,
                    'groupOddCssClass' => 'kv-grouped-row',
                    'groupEvenCssClass' => 'kv-grouped-row',
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'okr',
                    'label' => 'Objectives',
                    'value' => function($model){
                        return $model->initiatives->okr->okr;
                    },
                    // 'width' => '150px',
                    'group' => true,
                    'subGroupOf' => 1
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'label' => 'Program Kerja',
                    'attribute' => 'workplan_name',
                    'value' => function ($model) {
                        return ($model->initiatives) ? $model->initiatives->workplan_name : '-';
                    },
                    // 'width' => '150px',
                ],
                [
                    'label' => 'Capaian',
                    'value'=>function ($model) {
                        $target = $model->target ?: 0;
                        $real = $model->real ?: 0;
                        $real_percent = ($target > 0) ? round(($real / $target) * 100, 2) : 0;
                        $measure = $model->initiatives->measure;
                        return Progress::widget([
                            'percent' => $real_percent,
                            'label' => $real_percent.'%',
                        ]).$real.'/'.$target.' '.$measure;
                    },
                    'format'=>'raw',
                    'width' => '250px',
                ]
            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ]
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Program Kerja',
                'before'=>'',
                'after'=>'',
            ]
        ])?>
    </div>
</div>