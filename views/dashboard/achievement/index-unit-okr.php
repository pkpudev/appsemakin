<?php

use app\models\Evaluation;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\bootstrap\Progress;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

CrudAsset::register($this);

?>
<div class="okr-unit-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'okr-unit-crud-datatable',
            'filterSelector' => '.additional-filter',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute' => 'parent_id',
                    'label' => 'Unit',
                    'value' => function($model){
                        return $model->okr->parent->position->unit->unit_name ?: 'Organisasi';
                    },
                    'group' => true,
                    'groupedRow' => true,
                    'groupOddCssClass' => 'kv-grouped-row',
                    'groupEvenCssClass' => 'kv-grouped-row',
                    'filter' => false,
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute' => 'parent_okr',
                    'label' => 'Objective',
                    'value' => function($model){
                        return $model->okr->parent->okr;
                    },
                    // 'width' => '150px',
                    'group' => true,
                    'subGroupOf' => 1,
                    'filter' => false,
                ],
                [
                    'attribute' => 'okr',
                    'label' => 'Key Result',
                    'value' => function($model){
                        return $model->okr->okr;
                    },
                    // 'width' => '150px',
                    'filter' => false,
                ],
                [
                    'label' => 'Capaian',
                    'value'=>function ($model) {
                        $target = $model->target ?: 0;
                        $real = $model->real ?: 0;
                        $real_percent = ($target > 0) ? round(($real / $target) * 100, 2) : 0;
                        $measure = $model->okr->measure;
                        return Progress::widget([
                            'percent' => $real_percent,
                            'label' => $real_percent.'%',
                        ]).$real.'/'.$target.'% ' . ($measure == '%' ? '' : '(' . ((($target * $model->okr->target) / 100) . ' ' .$measure) . ')');
                    },
                    'format'=>'raw',
                    'width' => '250px',
                ]
            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ]
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> OKR',
                'before'=>'',
                'after'=>'',
            ]
        ])?>
    </div>
</div>