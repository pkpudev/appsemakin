<?php

$this->title = 'Struktur Organisasi';
?>
<style>
img {
    border: 1px solid var(--background);
    border-radius: 5px;
    padding: 5px; 
    margin: 5px auto;
}
</style>
<div class="structure">
    
    <div class="box box-default">
        <div class="box-body">
            <img id="img" src="/images/structure.jpg" width="100%" />
        </div>
    </div>

</div>
<?php
$script = <<< JS

    $("#img").ezPlus({
        // zoomType: 'lens',
        // lensShape: 'round',
        // lensSize: 400
        zoomWindowOffsetX: -600,
        zoomWindowOffsetY: 200,
    });
        
JS;

$this->registerJs($script);
?>