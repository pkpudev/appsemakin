<?php

use app\components\DateHelper;
use app\models\Budget;
use app\models\Target;

$arr= [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
        'headerOptions' => ['class' => 'stick-relative zero-col'],
        'contentOptions' => ['class' => 'stick-relative zero-col']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_code',
        'headerOptions' => ['class' => 'stick-relative first-col'],
        'contentOptions' => ['class' => 'stick-relative first-col']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_name',
        // 'noWrap' => true,
        'headerOptions' => ['class' => 'stick-relative second-col'],
        'contentOptions' => ['class' => 'stick-relative second-col'],
        'width' => '400px'
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px; background: #eee;">
                        <tr>
                            <td colspan="6" align="center" style="background: #eee;">
                                Total
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTargetYear($model->id, 'target');
            $realisasiTarget = Target::getUnitTargetYear($model->id, 'realisasi');
            $budget = Budget::getUnitBudgetYear($model->id, 'budget');
            $realisasiBudget = Budget::getUnitBudgetYear($model->id, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }

            return "<table width='100%' style='background: #eee;'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'headerOptions' => ['style' => 'padding: 0;'],
        'contentOptions' => ['style' => 'background: #eee;']
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px;">
                        <tr>
                            <td colspan="6" align="center">
                            ' . DateHelper::getBulanIndo(1) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 1, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 1, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 1, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 1, 'realisasi');
            
            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }

            return "<table width='100%'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'pageSummary' => $this->params['totalBudgetJan']
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px; background: #eee;">
                        <tr>
                            <td colspan="6" align="center" style="background: #eee;">
                            ' . DateHelper::getBulanIndo(2) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 2, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 2, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 2, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 2, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }

            return "<table width='100%' style='background: #eee;'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'headerOptions' => ['style' => 'padding: 0;'],
        'contentOptions' => ['style' => 'background: #eee;'],
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px;">
                        <tr>
                            <td colspan="6" align="center">
                            ' . DateHelper::getBulanIndo(3) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 3, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 3, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 3, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 3, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }

            return "<table width='100%'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px; background: #eee;">
                        <tr>
                            <td colspan="6" align="center" style="background: #eee;">
                            ' . DateHelper::getBulanIndo(4) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 4, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 4, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 4, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 4, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }

            return "<table width='100%' style='background: #eee;'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'headerOptions' => ['style' => 'padding: 0;'],
        'contentOptions' => ['style' => 'background: #eee;'],
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px;">
                        <tr>
                            <td colspan="6" align="center">
                            ' . DateHelper::getBulanIndo(5) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 5, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 5, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 5, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 5, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }

            return "<table width='100%'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'headerOptions' => ['style' => 'padding: 0;'],
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px; background: #eee;">
                        <tr>
                            <td colspan="6" align="center" style="background: #eee;">
                            ' . DateHelper::getBulanIndo(6) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 6, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 6, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 6, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 6, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }

            return "<table width='100%' style='background: #eee;'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'headerOptions' => ['style' => 'padding: 0;'],
        'contentOptions' => ['style' => 'background: #eee;'],
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px;">
                        <tr>
                            <td colspan="6" align="center">
                            ' . DateHelper::getBulanIndo(7) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 7, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 7, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 7, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 7, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }


            return "<table width='100%'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'headerOptions' => ['style' => 'padding: 0;'],
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px; background: #eee;">
                        <tr>
                            <td colspan="6" align="center" style="background: #eee;">
                            ' . DateHelper::getBulanIndo(8) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 8, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 8, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 8, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 8, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }

            return "<table width='100%' style='background: #eee;'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'headerOptions' => ['style' => 'padding: 0;'],
        'contentOptions' => ['style' => 'background: #eee;'],
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px;">
                        <tr>
                            <td colspan="6" align="center">
                            ' . DateHelper::getBulanIndo(9) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 9, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 9, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 9, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 9, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }

            return "<table width='100%'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'headerOptions' => ['style' => 'padding: 0;'],
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px; background: #eee;">
                        <tr>
                            <td colspan="6" align="center" style="background: #eee;">
                            ' . DateHelper::getBulanIndo(10) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 10, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 10, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 10, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 10, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }

            return "<table width='100%' style='background: #eee;'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'headerOptions' => ['style' => 'padding: 0;'],
        'contentOptions' => ['style' => 'background: #eee;'],
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px;">
                        <tr>
                            <td colspan="6" align="center">
                            ' . DateHelper::getBulanIndo(11) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 11, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 11, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 11, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 11, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }
            
            return "<table width='100%'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'headerOptions' => ['style' => 'padding: 0;'],
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important; width: 700px; background: #eee;">
                        <tr>
                            <td colspan="6" align="center" style="background: #eee;">
                            ' . DateHelper::getBulanIndo(12) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right">Target PHP</td>
                            <td width="20%" align="right">Realisasi PHP</td>
                            <td width="10%" align="right">%</td>
                            <td width="20%" align="right">Rencana Anggaran</td>
                            <td width="20%" align="right">Realisasi Biaya</td>
                            <td width="10%" align="right">%</td>
                        </tr>
                    </table>',
        'format' => 'raw',
        'hiddenFromExport' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 12, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 12, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 12, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 12, 'realisasi');

            if($target > $realisasiTarget){
                $coloredTarget = "style='color: var(--danger);'";
            }

            if($budget < $realisasiBudget){
                $coloredBudget = "style='color: var(--danger);'";
            }

            return "<table width='100%' style='background: #eee;'>
                        <tr>
                            <td width='20%' align='right'>" . number_format($target, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredTarget . ">" . 
                                number_format($realisasiTarget, 0, ',', '.') . 
                            "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($target > 0 ? number_format(($realisasiTarget/$target)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                            <td width='20%' align='right'>" . number_format($budget, 0, ',', '.') . "</td>
                            <td width='20%' align='right' " . $coloredBudget . ">" . 
                                number_format($realisasiBudget, 0, ',', '.') . 
                                "</td>" .
                            "<td width='10%' align='right' " . $coloredTarget . ">" . 
                                ($budget > 0 ? number_format(($realisasiBudget/$budget)*100, 2, ',', '.') : '0') . "%" .
                            "</td>
                        </tr>
                    </table>";
        },
        'headerOptions' => ['style' => 'padding: 0;'],
        'contentOptions' => ['style' => 'background: #eee;'],
    ],

    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(1) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 1, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 1, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 1, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 1, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(2) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 2, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 2, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 2, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 2, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(3) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 3, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 3, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 3, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 3, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(4) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 4, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 4, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 4, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 4, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(5) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 5, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 5, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 5, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 5, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(6) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 6, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 6, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 6, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 6, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(7) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 7, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 7, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 7, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 7, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(8) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 8, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 8, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 8, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 8, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(9) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 9, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 9, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 9, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 9, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(10) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 10, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 10, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 10, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 10, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(11) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 11, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 11, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 11, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 11, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
    [
        'header' => '<table class="kv-grid-table table table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
                        <tr>
                            <td colspan="4" align="center">
                            ' . DateHelper::getBulanIndo(12) . '
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" align="center">Target PHP</td>
                            <td width="25%" align="center">Realisasi PHP</td>
                            <td width="25%" align="center">Rencana Anggaran</td>
                            <td width="25%" align="center">Realisasi Biaya</td>
                        </tr>
                    </table>',
        'headerOptions' => ['style' => 'padding: 0;'],
        'format' => 'raw',
        'hidden' => true,
        'value' => function($model){
            $target = Target::getUnitTarget($model->id, 12, 'target');
            $realisasiTarget = Target::getUnitTarget($model->id, 12, 'realisasi');
            $budget = Budget::getUnitBudget($model->id, 12, 'budget');
            $realisasiBudget = Budget::getUnitBudget($model->id, 12, 'realisasi');

            return "<table width='100%'>
                        <tr>
                            <td width='25%' align='right'>" . $target . "</td>
                            <td width='25%' align='right'>" . $realisasiTarget . "</td>
                            <td width='25%' align='right'>" . $budget . "</td>
                            <td width='25%' align='right'>" . $realisasiBudget . "</td>
                        </tr>
                    </table>";
        }
    ],
];

return $arr;
