<?php

use app\components\DateHelper;
use dosamigos\highcharts\HighCharts;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\bootstrap\Tabs;
use yii\helpers\Html;

$this->title = 'Dashboard Anggaran';


?>
<style>
    .box-dashboard {
        border: 1px solid var(--background);
        border-radius: 5px;
        padding: 5px; 
        text-align: center; 
        margin: 5px auto;
    }
    .stick-relative {
        position: -webkit-sticky;
        position: sticky;
        background: #fff;
    }
    .zero-col {
        width: 30px;
        min-width: 30px;
        max-width: 30px;
        left: 0px;
        border: 1px solid #f4f4f4;
    }
    .first-col {
        width: 100px;
        min-width: 100px;
        max-width: 100px;
        left: 30px;
        border: 1px solid #f4f4f4;
    }
    .second-col {
        width: 400px;
        min-width: 400px;
        max-width: 400px;
        left: 130px;
        border: 1px solid #f4f4f4;
    }
    .tab-content {
        height: <?= $isAdmin ? '246px' : '202px'; ?> !important;
        overflow-y: auto;
        overflow-x: hidden;
    }
    .tab-pane{
        min-height: auto !important;
    }
    .nav > li > a {
        padding: 5px 15px !important;
    }
</style>
<div class="ceiling">
    
    <div class="box box-default">
        <div class="box-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-12" style="padding-left: 0 !important; padding-right: 0 !important;">
                        <div class="box-dashboard">
                            <center><b style="color: var(--borderColor);font-size: 17px;">Filter</b></center>
                            <table width="100%"><tr>
                                <td width="30%">
                                    <?= DatePicker::widget([
                                        'name' => 'year-filter',
                                        'value' => $year,
                                        'options' => [
                                            'id' => 'year-filter',
                                            'placeholder' => 'Filter Tahun',
                                            'onchange' => 'changeFilterYear($(this).val())',
                                        ],
                                        'pluginOptions' => [
                                            'autoclose'=>true,
                                            'startView'=>'year',
                                            'minViewMode'=>'years',
                                            'format' => 'yyyy',
                                            'allowClear' => false
                                        ]
                                    ]); ?>
                                </td>
                                <td style="padding-left: 10px;">
                                    <?= Select2::widget([
                                            'name' => 'unit-filter',
                                            'value' => $unitId,
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'id' => 'unit-filter',
                                                'placeholder' => '-- Filter Unit --',
                                                'onchange' => 'changeFilterUnit($(this).val())',
                                            ],
                                            'pluginOptions' => ['allowClear' => true],
                                            'data' => $listUnit,
                                        ])
                                    ?>
                                </td>
                            </tr></table>
                            <table width="100%" style="margin-top: 10px; <?= $isAdmin ? '' : 'display: none;';?>"><tr>
                                <td width="50%">
                                    <?= Select2::widget([
                                            'name' => 'directorate-filter',
                                            'value' => $directorateId,
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'id' => 'directorate-filter',
                                                'placeholder' => '-- Filter Directorate --',
                                                'onchange' => 'changeFilterDirectorate($(this).val())',
                                            ],
                                            'pluginOptions' => ['allowClear' => true],
                                            'data' => $listDirectorate,
                                        ])
                                    ?>
                                </td>
                                <td style="padding-left: 10px;">
                                    <?= Select2::widget([
                                            'name' => 'department-filter',
                                            'value' => $departmentId,
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'id' => 'department-filter',
                                                'placeholder' => '-- Filter Department --',
                                                'onchange' => 'changeFilterDepartment($(this).val())',
                                            ],
                                            'pluginOptions' => ['allowClear' => true],
                                            'data' => $listDepartment,
                                        ])
                                    ?>
                                </td>
                            </tr></table>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding-left: 0 !important; padding-right: 5px !important;">
                        <div class="box-dashboard">
                            <b style="color: var(--borderColor);font-size: 17px;">Total Target PHP</b>
                            <h3 style="margin-top: 5px;">Rp. <?= number_format($totalTargetCompany, 0, ',', '.'); ?></h3>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding-left: 5px !important; padding-right: 0 !important;">
                        <div class="box-dashboard">
                            <b style="color: var(--borderColor);font-size: 17px;">Total Realisasi PHP</b>
                            <h3 style="margin-top: 5px;">
                                Rp. <?= number_format($totalRealisasiTargetCompany, 0, ',', '.'); ?>
                                <?php if($totalTargetCompany): ?>
                                (<?= number_format(($totalRealisasiTargetCompany/$totalTargetCompany)*100, 2, ',', '.').'%'; ?>)
                                <?php endif; ?>
                            </h3>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding-left: 0 !important; padding-right: 5px !important;">
                        <div class="box-dashboard">
                            <b style="color: var(--borderColor);font-size: 17px;">Total Rencana Anggaran</b>
                            <h3 style="margin-top: 5px;">Rp. <?= number_format($totalBudgetCompany, 0, ',', '.'); ?></h3>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding-left: 5px !important; padding-right: 0 !important;">
                        <div class="box-dashboard">
                            <b style="color: var(--borderColor);font-size: 17px;">Total Realisasi Biaya</b>
                            <h3 style="margin-top: 5px;">
                                Rp. <?= number_format($totalRealisasiBudgetCompany, 0, ',', '.'); ?>
                                <?php if($totalBudgetCompany): ?>
                                (<?= number_format(($totalRealisasiBudgetCompany/$totalBudgetCompany)*100, 2, ',', '.').'%'; ?>)
                                <?php endif; ?>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" style="padding-left: 0px;">
                    <div class="box-dashboard">
                        <?php $this->beginBlock('grafik'); ?>
                            <?= HighCharts::widget([
                                'clientOptions' => [
                                    'chart' => [
                                        'height' => $isAdmin ? '245px' : '195px',
                                    ],
                                    'title' => [
                                        'text' => $unitName, 
                                        'style' => ['color' => 'var(--borderColor)', 'font-weight' => 'bold', 'font-size' => '17px']
                                    ],
                                    'xAxis' => [
                                        'categories' => $monthName
                                    ],
                                    'credits' => [
                                        'enabled' => false
                                    ],
                                    'series' => [
                                        [
                                            'name' => 'Target PHP', 
                                            'data' => $monthlyTargetCompany,
                                        ],
                                        [
                                            'name' => 'Realisasi PHP', 
                                            'data' => $monthlyRealisasiTargetCompany,
                                        ],
                                        [
                                            'name' => 'Rencana Anggaran', 
                                            'data' => $monthlyBudgetCompany,
                                        ],
                                        [
                                            'name' => 'Realisasi Biaya', 
                                            'data' => $monthlyRealisasiBudgetCompany,
                                        ]
                                    ],
                                    'colors' => ['rgb(255, 102, 102)', 'rgb(102, 102, 255)', 'rgb(77, 255, 121)', 'rgb(255, 210, 77)'],
                                ]
                            ]); ?>
                        <?php $this->endBlock(); ?>

                        <?php $this->beginBlock('table'); ?>
                            <table class="kv-grid-table table table-hover table-bordered table-striped table-condensed">
                                <tr>
                                    <th style="text-align: center;">Bulan</th>
                                    <th style="text-align: center;">Target PHP</th>
                                    <th style="text-align: center;">Realisasi PHP</th>
                                    <th style="text-align: center;">Rencana Anggaran</th>
                                    <th style="text-align: center;">Realisasi Biaya</th>
                                </tr>
                                <?php 
                                    for($m = 0; $m < 12; $m++){ 
                                ?>
                                <tr>
                                    <td width="125px" align="left"><?= DateHelper::getBulanIndo($m+1); ?></td>
                                    <td width="125px" align="right"><?= number_format($monthlyTargetCompany[$m], 0, ',', '.'); ?></td>
                                    <td width="125px" align="right" <?= ($monthlyTargetCompany[$m] > $monthlyRealisasiTargetCompany[$m]) ? "style='color: var(--danger);'" : ''; ?>>
                                        <?= number_format($monthlyRealisasiTargetCompany[$m], 0, ',', '.'); ?>
                                        <?php if($monthlyTargetCompany[$m]): ?>
                                         (<?= number_format(($monthlyRealisasiTargetCompany[$m]/$monthlyTargetCompany[$m])*100, 2, ',', '.'); ?>%)
                                         <?php endif; ?>
                                    </td>
                                    <td width="125px" align="right"><?= number_format($monthlyBudgetCompany[$m], 0, ',', '.'); ?></td>
                                    <td width="125px" align="right" <?= ($monthlyBudgetCompany[$m] < $monthlyRealisasiBudgetCompany[$m]) ? "style='color: var(--danger);'" : ''; ?>>
                                        <?= number_format($monthlyRealisasiBudgetCompany[$m], 0, ',', '.'); ?>
                                        <?php if($monthlyBudgetCompany[$m]): ?>
                                        (<?= number_format(($monthlyRealisasiBudgetCompany[$m]/$monthlyBudgetCompany[$m])*100, 2, ',', '.'); ?>%)
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </table>
                        <?php $this->endBlock(); ?>
                        <?= Tabs::widget([
                            'id' => 'grafik-table-tabs',
                            'encodeLabels' => false,
                            'items' => [
                                [
                                    'label' => "<span class='fa fa-line-chart'></span>&nbsp;Grafik",
                                    'content' => $this->blocks['grafik'],
                                ],
                                [
                                    'label' => "<span class='fa fa-table'></span>&nbsp;Tabel",
                                    'content' => $this->blocks['table'],
                                ],
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>

            <div class="row ceiling-list">
                <div class="col-md-12">
                    <div class="box-dashboard" style="text-align: left;">
                        <?= GridView::widget([
                            'id'=>'ceiling',
                            'dataProvider' => $dataProvider,
                            // 'showPageSummary' => true,
                            'responsiveWrap' => false,
                            'pjax'=>true,
                            'hover' => true,
                            'export' => [
                                'label' => 'Export Excel atau PDF',
                                'header' => '',
                                'options' => [
                                    'class' => 'btn btn-primary'
                                ], 
                                'menuOptions' => [ 
                                        'style' => 'background: var(--background); width: 187px;'
                                ]
                            ],
                            'exportConfig' => [
                                GridView::PDF => true,
                                GridView::EXCEL => true,
                            ],
                            'columns' => require(__DIR__.'/_columns.php'),
                            'toolbar'=> [
                                ['content'=>
                                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                                    '{toggleData}'
                                ],
                                ['content'=>
                                    '{export}'
                                ],
                            ],
                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'panel' => [
                                'type' => 'primary',
                                'heading' => 'Penyajian Pagu Anggaran',
                                'before'=>'',
                                'after'=>'',
                            ]
                        ])?>
                        <b style="color: var(--borderColor);font-size: 17px;">Keterangan: </b><br />
                        1. Realisasi PHP: Data diambil dari Sigma secara otomatis.<br /> 
                        2. Realisasi Biaya: Data diambil dari Disbursement secara otomatis.<br /> 
                        3. Data akan update setiap jam.<br /> 
                        4. Data pada kolom Realisasi PHP yang berwarna merah artinya realisasi PHP belum mencapai target.<br /> 
                        5. Data pada kolom Realisasi Biaya yang berwarna merah artinya realisasi biaya sudah melebihi anggaran.<br /> 
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</div>
<script>
    function changeFilterYear(val) {
        var year = val;
        var unitId = $("#unit-filter").val();

        window.location.replace("/dashboard/summary2?year=" + year + "&unitId=" + unitId);
    }
    function changeFilterDirectorate(val) {
        var directorateId = val;
        var year = $("#year-filter").val();

        window.location.replace("/dashboard/summary2?year=" + year + "&directorateId=" + directorateId);
    }
    function changeFilterDepartment(val) {
        var departmentId = val;
        var year = $("#year-filter").val();

        window.location.replace("/dashboard/summary2?year=" + year + "&departmentId=" + departmentId);
    }
    function changeFilterUnit(val) {
        var unitId = val;
        var year = $("#year-filter").val();

        window.location.replace("/dashboard/summary2?year=" + year + "&unitId=" + unitId);
    }
</script>