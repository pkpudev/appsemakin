<?php

namespace app\models;

use Yii;
use \app\models\base\Hint as BaseHint;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.hint".
 */
class Hint extends BaseHint
{

    public function getAtr()
    {
        return $this->hasOne(\app\models\Attributes::className(), ['id' => 'attributes_id']);
    }
}
