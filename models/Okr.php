<?php

namespace app\models;

use Yii;
use \app\models\base\Okr as BaseOkr;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.okr".
 */
class Okr extends BaseOkr
{

    public $krs;
    public $kr_targets;
    public $kr_measures;
    public $timebounds_q1;
    public $timebounds_q2;
    public $timebounds_q3;
    public $timebounds_q4;
    public $targets_q1;
    public $targets_q2;
    public $targets_q3;
    public $targets_q4;
    public $unit_structure_id;
    public $files;
    public $initiatives_id;

    const SCENARIO_INPUT_KR = 'input-kr';
    const SCENARIO_INPUT_IK = 'input-ik';

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [
                    [
                        'krs', 'kr_targets', 'kr_measures', 
                        'timebounds_q1', 'timebounds_q2', 'timebounds_q3', 'timebounds_q4',
                        'targets_q1', 'targets_q2', 'targets_q3', 'targets_q4',
                        'unit_structure_id', 'files', 'initiatives_id'
                    ], 'safe'
                ],
                [
                    [
                        'support_type', 'polarization_id', 'calculation_id', 'validity_id', 'controllability_id', 'measure', 
                        // 'timebound_q1', 'timebound_q2', 'timebound_q3', 'timebound_q4',
                        // 'target_q1', 'target_q2', 'target_q3', 'target_q4'
                    ], 'required', 'on' => self::SCENARIO_INPUT_KR
                ],
                [
                    ['position_structure_id'], 'required', 'on' => self::SCENARIO_INPUT_IK
                ]
            ]
        );
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_INPUT_KR => [
                'support_type', 'polarization_id', 'calculation_id', 'validity_id', 'controllability_id', 'measure', 
                'timebound_q1', 'timebound_q2', 'timebound_q3', 'timebound_q4',
                'target_q1', 'target_q2', 'target_q3', 'target_q4'
            ],
        ]);
    }

    public function beforeValidate()
    {
        if (!parent::beforeValidate()) return false;

        if(!$this->okr_code) $this->okr_code = $this->generateCode();    
        
        return true;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($this->type == 'KR' && $modelParent = Okr::findOne($this->parent_id)){
                $this->okr_level_id = $modelParent->okr_level_id;
                $this->cascading_type_id = $modelParent->cascading_type_id;
                $this->year = $modelParent->year;
                if($this->okr_level_id == OkrLevel::INDIVIDUAL){
                    $modelParent->position_structure_id = $this->position_structure_id;
                    $modelParent->save(false);
                } else {
                    $this->position_structure_id = $modelParent->position_structure_id;
                    $this->pic_id = $modelParent->pic_id;
                }
            }

            if(!$this->okr_org_id){
                if($this->cascading_type_id == CascadingType::SUPPORT){
                    if($parent = self::findOne($this->parent_id)){
                        $this->okr_org_id = $parent->okr_org_id;
                    }
                } else {
                    if($reff = self::findOne($this->ref_id)){
                        $this->okr_org_id = $reff->okr_org_id;
                    }
                }
            }

            if ($this->isNewRecord) {
                $this->version = 1;      
                if(!$this->okr_code) $this->okr_code = $this->generateCode();          
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }
    
    public function afterSave($insert, $changedAttributes)
    {


        if($this->type == 'KR'){
            $initiatives = Initiatives::find()->where(['okr_id' => $this->id])->all();
            foreach($initiatives as $initiative){
                // $initiative->okr = $this->workplan_name;
                // $initiative->save(false);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    protected function generateCode(){
        if($this->okr_level_id == OkrLevel::ORGANIZATION){
            if($this->type == 'O'){
                $countObjective = self::find()->where(['year' => $this->year])->count();
                $newno = str_pad($countObjective + 1, 2, "0", STR_PAD_LEFT);
            } else {
                $parent = self::findOne($this->parent_id);
                $countKr = self::find()->where(['parent_id' => $this->parent_id])->count();
                $newno = $parent->okr_code . '-' . str_pad($countKr + 1, 2, "0", STR_PAD_LEFT);
            }

        } else {

            if(($this->type == 'O' && $this->okr_level_id == OkrLevel::INDIVIDUAL)){
                $parent = self::findOne($this->parent_id);
                
                $no_urut = self::find()->where(['parent_id' => $this->parent_id])->count();
                $lastNo = $no_urut + 1;
                $newno = $parent->okr_code . '-' . str_pad($lastNo, 2, "0", STR_PAD_LEFT);
            } else if(($this->position_structure_id && $position = PositionStructure::findOne($this->position_structure_id))){
                $unitCode = $position->unit->unit_code;

                if($this->cascading_type_id == CascadingType::SUPPORT){
                    $parent = self::findOne($this->parent_id);
        
                    if($this->type == 'O'){
                        if($this->okr_level_id == OkrLevel::DIRECTORATE){
                            $newno = $unitCode . '-' . $parent->okr_code;
                        } else {
                            $no_urut = self::find()->where(['parent_id' => $this->parent_id])->andWhere(['type' => 'KR'])->count();
                            $lastNo = $no_urut + 1;
                            $newno = $parent->okr_code . '-' . str_pad($lastNo, 2, "0", STR_PAD_LEFT);
                        }
                    } else {
                        $no_urut = self::find()->where(['parent_id' => $this->parent_id])->andWhere(['type' => 'KR'])->count();
                        $lastNo = $no_urut + 1;
                        $newno = $parent->okr_code . '-' . str_pad($lastNo, 2, "0", STR_PAD_LEFT);
                    }
                } else {
                    $reff = self::findOne($this->ref_id);
                    if($this->okr_level_id == OkrLevel::DIRECTORATE){
                        $newno = $unitCode . '-' . $reff->okr_code;
                    } else {
                        $reffCode = explode('-', $reff->okr_code);
                        $newno = str_replace($reffCode[0], $unitCode, $reff->okr_code);
                    }
                }

            } else {
                $newno = 'no_position';
            }
        }
        
        return $newno;
    }

    public function getKeyResult($id)
    {

        $krs = self::find()->where(['parent_id' => $id])->andWhere(['type' => 'KR'])->all();

        if (!empty($krs)) {
            $html = '';

            foreach($krs as $kr){
                $html .= '<b>' . $kr->okr_code . '</b><br />' . 
                            $kr->okr . '<br />';

            }

            return $html;
        } else {
            return "-";
        }
    }

    public function getKeyResultExcel($id)
    {

        $krs = self::find()->where(['parent_id' => $id])->andWhere(['type' => 'KR'])->all();

        if (!empty($krs)) {
            $html = '';

            foreach($krs as $kr){
                $html .= $kr->okr_code . ":\n" . 
                            $kr->okr . "\n";

            }

            return $html;
        } else {
            return "-";
        }
    }

    public function getObjAchieveValue($year, $period){
        $evaluation = Evaluation::findOne(['year' => $year, 'period' => $period]);

        $okrAchieves = OkrAchievement::find()->alias('oa')->joinWith('okr o')->where(['o.parent_id' => $this->id])->andWhere(['evaluation_id' => $evaluation->id])->andWhere(['okr_level_id' => 1])->all();
        $val = 0;
        $i = 0;
        foreach($okrAchieves as $okrAchieve){
            $val = $val + ($okrAchieve->achieve_value?:0);
            
            $i++;
        }

        return $val/ ($i ?: 1);

    }

    public function getKrAchieveValue($year, $period){
        $evaluation = Evaluation::findOne(['year' => $year, 'period' => $period]);

        $okrAchieve = OkrAchievement::find()->alias('oa')->joinWith('okr o')->where(['o.id' => $this->id])->andWhere(['evaluation_id' => $evaluation->id])->one();

        return $okrAchieve->achieve_value?:0;

    }

    /* public function getFinalAchieveValue($year){

        $okrAchieves = OkrAchievement::find()->alias('oa')->joinWith('okr o')->where(['o.parent_id' => $this->id])->andWhere(['o.year' => $year])->all();
        $val = 0;
        
        $achieveValues = [];
        foreach($okrAchieves as $okrAchieve){
            // $temp = (($okrAchieve->achieve_value?:0) * $okrAchieve->okr->org->weight) / 100;

            // $val = $val + $temp;
            // var_dump($okrAchieve->achieve_value);
            array_push($achieveValues, $okrAchieve->achieve_value?:0);
        }
        
        // return $val;
        if($this->calculation->id == Calculation::SUM){
            $lastAchieveValue = array_sum($achieveValues);
        } else if($this->calculation->id == Calculation::AVERAGE){
            $lastAchieveValue = $achieveValues ? array_sum($achieveValues)/count($achieveValues) : 0;
        } else if($this->calculation->id == Calculation::LAST){
            $lastAchieveValue = end($achieveValues);
        } else if($this->calculation->id == Calculation::MIN){
            $lastAchieveValue = min($achieveValues);
        }

        // var_dump(json_encode($achieveValues));

        return is_numeric($lastAchieveValue) ? number_format($lastAchieveValue, 2, ',', '.') . '% (' . ($this->calculation->calculation_type ?: '-') . ')' : '-';
    } */

    public function getFinalAchieveValue($year){

        $okrs = Okr::find()->where(['parent_id' => $this->id])->andWhere(['year' => $year])->andWhere(['type' => 'KR'])->all();
        $val = 0;
        
        $achieveValues = [];
        foreach($okrs as $okr){
            
            $evaluation1 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 1']);
            $evaluation2 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 2']);
            $evaluation3 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 3']);
            $evaluation4 = Evaluation::findOne(['year' => $year, 'period' => 'Kuartal 4']);

            $okrAchievement1 = OkrAchievement::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation1->id]);
            $okrAchievement2 = OkrAchievement::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation2->id]);
            $okrAchievement3 = OkrAchievement::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation3->id]);
            $okrAchievement4 = OkrAchievement::findOne(['okr_id' => $okr->id, 'evaluation_id' => $evaluation4->id]);

            $weight = $okr->org->weight;

            if($okrAchievement1) array_push($achieveValues, ($weight ? (($okrAchievement1->achieve_value?:0) * $weight) / 100 : 0));
            if($okrAchievement2) array_push($achieveValues, ($weight ? (($okrAchievement2->achieve_value?:0) * $weight) / 100 : 0));
            if($okrAchievement3) array_push($achieveValues, ($weight ? (($okrAchievement3->achieve_value?:0) * $weight) / 100 : 0));
            if($okrAchievement4) array_push($achieveValues, ($weight ? (($okrAchievement4->achieve_value?:0) * $weight) / 100 : 0));

            
            if($okr->calculation->id == Calculation::SUM){
                $lastAchieveValue = array_sum($achieveValues);
            } else if($okr->calculation->id == Calculation::AVERAGE){
                $lastAchieveValue = $achieveValues ? array_sum($achieveValues)/count($achieveValues) : 0;
            } else if($okr->calculation->id == Calculation::LAST){
                $lastAchieveValue = end($achieveValues);
            } else if($okr->calculation->id == Calculation::MIN){
                $lastAchieveValue = min($achieveValues);
            }

            /* if($this->id == 15456){
                var_dump($lastAchieveValue);
            } */

            $val = $val + $lastAchieveValue;
        }
        
        return $val;
    }

    public function getLastAchievedByPeriod($year){
        $evaluations = Evaluation::find()->where(['year' => $year])->orderBy('id')->all();

        if($evaluations){

            $achieveValues = [];
            foreach($evaluations as $evaluation){
                $okrAchievement = OkrAchievement::findOne(['evaluation_id' => $evaluation->id, 'okr_id' => $this->id]);
                if($okrAchievement) array_push($achieveValues, $okrAchievement->achieve_value?:0);
            }

            if($this->calculation->id == Calculation::SUM){
                $lastAchieveValue = array_sum($achieveValues);
            } else if($this->calculation->id == Calculation::AVERAGE){
                $lastAchieveValue = $achieveValues ? array_sum($achieveValues)/count($achieveValues) : 0;
            } else if($this->calculation->id == Calculation::LAST){
                $lastAchieveValue = end($achieveValues);
            } else if($this->calculation->id == Calculation::MIN){
                $lastAchieveValue = min($achieveValues);
            }

            // var_dump(json_encode($achieveValues));

            return is_numeric($lastAchieveValue) ? number_format($lastAchieveValue, 2, ',', '.') . '% (' . ($this->calculation->calculation_type ?: '-') . ')' : '-';
        } else {
            return '-';
        }
    }

    public function getLevel()
    {
        return $this->hasOne(\app\models\OkrLevel::className(), ['id' => 'okr_level_id']);
    }

    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    public function getRef()
    {
        return $this->hasOne(self::className(), ['id' => 'ref_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(\app\models\PositionStructure::className(), ['id' => 'position_structure_id']);
    }

    public function getCascadingType()
    {
        return $this->hasOne(\app\models\CascadingType::className(), ['id' => 'cascading_type_id']);
    }

    public function getSupport()
    {
        $support = [1 => 'Epic', 2 => 'Enabler'];
        return $support[$this->support_type];
    }

    public function getPolarization()
    {
        return $this->hasOne(\app\models\Polarization::className(), ['id' => 'polarization_id']);
    }

    public function getCalculation()
    {
        return $this->hasOne(\app\models\Calculation::className(), ['id' => 'calculation_id']);
    }

    public function getValidity()
    {
        return $this->hasOne(\app\models\Validity::className(), ['id' => 'validity_id']);
    }

    public function getControll()
    {
        return $this->hasOne(\app\models\Controllability::className(), ['id' => 'controllability_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(\app\models\Status::className(), ['id' => 'status_id']);
    }

    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
    
    public function getInitiatives()
    {
        return $this->hasMany(\app\models\Initiatives::className(), ['okr_id' => 'id']);
    }
    
    public function getInitiatives2()
    {
        return $this->hasMany(\app\models\Initiatives::className(), ['initiatives_code' => 'reference_no']);
    }
    
    public function getPic()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'pic_id']);
    }
    
    public function getOrg()
    {
        return $this->hasOne(\app\models\OkrOrg::className(), ['id' => 'okr_org_id']);
    }

    public function getMovs()
    {
        return $this->hasMany(\app\models\OkrMovType::className(), ['okr_id' => 'id']);
    }
}
