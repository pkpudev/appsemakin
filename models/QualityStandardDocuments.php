<?php

namespace app\models;

use Yii;
use \app\models\base\QualityStandardDocuments as BaseQualityStandardDocuments;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.quality_standard_documents".
 */
class QualityStandardDocuments extends BaseQualityStandardDocuments
{

    public $files;
    public $links;
    public $storages;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['files', 'links', 'storages'], 'safe']
            ]
        );
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }

    public function getUnit()
    {
        return $this->hasOne(\app\models\UnitStructure::className(), ['id' => 'unit_structure_id']);
    }

    public function getType()
    {
        return $this->hasOne(\app\models\DocumentType::className(), ['id' => 'document_type_id']);
    }

    public function getFile()
    {
        return $this->hasOne(\app\models\Files::className(), ['id' => 'files_id']);
    }

    public function getEditableFile()
    {
        return $this->hasOne(\app\models\Files::className(), ['id' => 'editable_files_id']);
    }
    
    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
