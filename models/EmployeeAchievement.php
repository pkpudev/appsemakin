<?php

namespace app\models;

use Yii;
use \app\models\base\EmployeeAchievement as BaseEmployeeAchievement;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.employee_achievement".
 */
class EmployeeAchievement extends BaseEmployeeAchievement
{
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    public function getPositionStructure()
    {
        return $this->hasOne(PositionStructure::className(), ['id' => 'position_structure_id']);
    }

    public function getEvaluation()
    {
        return $this->hasOne(\app\models\Evaluation::className(), ['id' => 'evaluation_id']);
    }
}
