<?php

namespace app\models;

use Yii;
use \app\models\base\OkrLevel as BaseOkrLevel;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.okr_level".
 */
class OkrLevel extends BaseOkrLevel
{

    const ORGANIZATION      = 1;
    const DIRECTORATE       = 2;
    const DEPARTMENT        = 3;
    const DIVISION          = 4;
    const INDIVIDUAL        = 5;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
