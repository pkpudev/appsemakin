<?php

namespace app\models;

use Yii;
use \app\models\base\PortfolioElement as BasePortfolioElement;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.portfolio_element".
 */
class PortfolioElement extends BasePortfolioElement
{

    const VALUE_STREAMS             = 1;  
	const SOLUSTIONS                = 2; 
	const CUSTOMERS                 = 3; 
	const CHANNELS                  = 4; 
	const CUSTOMER_RELATIONSHIPS    = 5;  
	const BUDGET                    = 6; 
	const KPI_REVENUE               = 7; 
	const KEY_PARTNERS              = 8;  
	const KEY_ACTIVITIES            = 9;  
	const KEY_RESOURCES             = 10;  
	const COST_STRUCTURE            = 11;  
	const REVENUE_STREAMS           = 12;  

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
