<?php

namespace app\models;

use Yii;
use \app\models\base\Initiatives as BaseInitiatives;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.initiatives".
 */
class Initiatives extends BaseInitiatives
{

    const SCENARIO_IMPLEMENTATION_VALUE_UPDATE = 'implementation_value_update';

    public $year;
    public $comment;
    public $objectives;
    public $involvement_dept_ids;
    protected $creator;
    public $unit_structure_id;
    public $oldStatusId;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[static::SCENARIO_IMPLEMENTATION_VALUE_UPDATE] = ['implementation'];
        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [[/* 'target', 'measure',  */'workplan_name'/* , 'objectives', 'involvement_dept_ids' */, 'polarization_id', 'calculation_id', 'validity_id', 'controllability_id'/* , 'assumption' */], 'required'];
        $rules[] = [['comment'], 'string'];
        $rules[] = [['unit_structure_id'], 'integer'];
        $rules[] = [['objectives', 'involvement_dept_ids'], 'safe'];

        return $rules;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'okr_id' => 'Objectives',
            'initiatives_code' => 'Kode Proker',
            'workplan_name' => 'Nama Proker',
            'initiatives' => 'Deskripsi',
            'implementation' => 'Pelaksanaan (Bulan)',
            'total_budget' => 'Jumlah Anggaran',
            'target' => 'Target',
            'measure' => 'Ukuran',
            'target_q1' => 'Target Kuartal 1',
            'target_q2' => 'Target Kuartal 2',
            'target_q3' => 'Target Kuartal 3',
            'target_q4' => 'Target Kuartal 4',
            'validity_id' => 'Nilai Validitas',
            'validity_index' => 'Index Validitas',
            'controllability_id' => 'Tingkat Pengendalian',
            'controllability_index' => 'Index Kendali',
            'grade' => 'Bobot',
            'monitoring_tools' => 'Alat Verifikasi',
            'status_id' => 'Status',
            'position_structure_id' => 'Penanggung Jawab',
            'pic_id' => 'PIC',
            'calculation_id' => 'Calculation',
            'polarization_id' => 'Polarisasi',
            'participatory_budgeting_id' => 'Jenis Anggaran Partisipatori',
            'assumption' => 'Potensi Risiko',
            'note' => 'Catatan',
            'metadata' => 'Metadata',
            'unit_structure_id' => 'Unit',
        ];
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->oldStatusId = $this->status_id;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->status_id = \app\models\Status::DRAFT;
                if (empty($this->initiatives_code))
                    $this->initiatives_code = $this->genInitiativesCode();
            }

            if($this->okr_id && $okr = Okr::findOne($this->okr_id)){
                $this->position_structure_id = $okr->position_structure_id;

                if($employeePosition = PositionStructureEmployee::findOne(['position_structure_id' => $this->position_structure_id])){
                    $this->pic_id = $employeePosition->employee_id;
                }   
            }
            
            $this->workplan_name = ucfirst($this->workplan_name);
            $this->initiatives = ucfirst($this->initiatives);

            return true;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {

        if($this->oldStatusId != Status::APPROVED && $this->status_id == Status::APPROVED){
            //set up budget_code on table bsc.budget
            /* 
            $activity = Activity::find()
                                ->where(['initiatives_id'=>$this->id])
                                ->andWhere(['<>', 'status_id', [Status::REJECTED,Status::DELETED]])
                                ->orderBy(['start_plan'=>SORT_ASC, 'id'=>SORT_ASC])
                                ->all();
            if($activity){
                $activity_no = 1;
                foreach ($activity as $row) {
                    // var_dump($row->id);
                    $row->activity_code = $this->initiatives_code.'.'.str_pad($activity_no, 2, "0", STR_PAD_LEFT);
                    $row->status_id = Status::APPROVED;
                    if($row->save(false)){

                        $budgets = Budget::find()
                            ->where(['activity_id'=>$row->id])
                            ->andWhere('account_id is not null')
                            ->andWhere(['<>', 'status_id', [Status::REJECTED,Status::DELETED]])
                            ->orderBy(['id'=>SORT_ASC])->all();
                        if($budgets){
                            $budget_no = 1;
                            foreach ($budgets as $budget) {
                                // var_dump($budget->id);
                                $account_no = preg_replace("/\s+/", "", $budget->account->daf_account_no);
                                // var_dump($account_no);
                                $budget_code = $row->activity_code .'.'. str_pad($budget_no, 2, "0", STR_PAD_LEFT) .'-'. $account_no;
                                $budget->budget_code = $budget_code;
                                $budget->status_id = Status::APPROVED;
                                if($budget->save(false)){
                                    $budget_no++;
                                } else {
                                }
                            }
                        }

                        $activity_no++;
                    }
                    // var_dump($account_no);die;
                }
            } */

            $budgets = Budget::find()
                                ->where(['initiatives_id' => $this->id])
                                ->andWhere('account_id is not null')
                                ->andWhere(['<>', 'status_id', [Status::REJECTED,Status::DELETED]])
                                ->orderBy(['id'=>SORT_ASC])->all();
            if($budgets){
                $budget_no = 1;
                foreach ($budgets as $budget) {
                    // var_dump($budget->id);
                    $account_no = preg_replace("/\s+/", "", $budget->account->daf_account_no);
                    // var_dump($account_no);
                    // inisial_unit - 2 digit tahun -  kode proker . no urut  
                    // $budget_code = $this->initiatives_code .'.'. str_pad($budget_no, 2, "0", STR_PAD_LEFT);
                    // $budget_code = $this->initiatives_code .'.'. str_pad($budget_no, 2, "0", STR_PAD_LEFT) .'-'. $account_no;
                    $budget_code = $this->initiatives_code . '-' . str_pad($budget_no, 2, "0", STR_PAD_LEFT);
                    $budget->budget_code = $budget_code;
                    $budget->status_id = Status::APPROVED;
                    if($budget->save(false)){
                        $budget_no++;
                    } else {
                        var_dump($budget->errors);die;
                    }
                }
            }
        }

        $okrs = Okr::find()->where(['ref_initiatives_id' => $this->id])->andWhere(['type' => 'O'])->all();
        foreach($okrs as $okr){
            $okr->okr = $this->workplan_name;
            $okr->save(false);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function genInitiativesCode()
    {
        // kode IKU
        $okr = Okr::findOne($this->okr_id);
        $code = $okr->position->unit->initiative_code;
        $year = $okr->year;
        // // no urut
        // $count = self::find()
        //             ->alias('i')
        //             ->joinWith(['okr o', 'okr.position op'])
        //             ->where(['o.year' => $okr->year])
        //             ->andWhere(['op.unit_structure_id' => $okr->position->unit_structure_id])
        //             ->count();
                    
        // $next_no = str_pad(((int) $count) + 1, 2, "0", STR_PAD_LEFT);

        //inisial unit-2 digit tahun.no urut

        // return "$code-$next_no";

        $lastNo = Initiatives::find()->where("initiatives_code ILIKE '$code%'")->andWhere("implementation ILIKE '%$year%'")->orderBy('initiatives_code DESC')->one();
        if($lastNo){
            // var_dump($lastNo);
            $newstring = substr($lastNo->initiatives_code, -2);
            // var_dump($newstring);
            
            $lastNo = $code . '-' . date('y') . '-' . str_pad($newstring + 1, 2, "0", STR_PAD_LEFT);
        } else {
            $lastNo = $code . '-' . date('y') . '-01';
        }
        return $lastNo;
    }

    public function renewImplementationValue()
    {
        return true;

        $monthsName = [];
        $filledMonths = [];
        $finishedInit = false;
        foreach ($this->budgets as $budget) {
            for ($i = 1; $i <= 12; $i++) {
                $attribute = "month_" . str_pad($i, 2, "0", STR_PAD_LEFT);
                if (!$finishedInit) {
                    $monthsName[$i] = $budget->getAttributeLabel($attribute);
                    $filledMonths[$i] = false;

                    if ($i == 12) {
                        $finishedInit = true;
                    }
                }
                if (!empty($budget->$attribute)) {
                    $filledMonths[$i] = true;
                }
            }
        }
        $result = null;
        $lastMonth = null;
        for ($i = 1; $i <= 12; $i++) {
            if ($filledMonths[$i] === true) {
                //echo "i = $i terisi<br />";
                if (empty($result)) {
                    $result = $monthsName[$i];
                } elseif ($i > 1 && $filledMonths[$i - 1] === false) {
                    $result .= ', ' . $monthsName[$i];
                } elseif ($i == 12) {
                    if ($filledMonths[$i - 1] === true) {
                        if ($filledMonths[$i - 2] === true) {
                            $result .= ' s/d ' . $monthsName[$i];
                        } else {
                            $result .= ', ' . $monthsName[$i];
                        }
                    }
                } else {
                    $lastMonth = $monthsName[$i];
                }
                //echo "hasil: $result<br /><br />";
            } else {
                //echo "i = $i tidak terisi<br />";
                if ($lastMonth && $i > 2 && $filledMonths[$i - 2] === true) {
                    if ($i > 3 && $filledMonths[$i - 3] === true) {
                        $result .= ' s/d ' . $lastMonth;
                    } else {
                        $result .= ', ' . $lastMonth;
                    }
                }
                $lastMonth = null;
                //echo "hasil: $result<br /><br />";
            }
        }
        $this->implementation = $result;

        $oldScenario = $this->getScenario();
        $this->setScenario(static::SCENARIO_IMPLEMENTATION_VALUE_UPDATE);
        $saved = $this->save();
        $this->setScenario($oldScenario);

        return $saved;
    }

    public function getQuarterGrade()
    {
        $grade = $this->grade;
        $count = 0;
        if ($this->target_q1 > 0)
            $count++;
        if ($this->target_q2 > 0)
            $count++;
        if ($this->target_q3 > 0)
            $count++;
        if ($this->target_q4 > 0)
            $count++;
        $quarter_grade = round($grade / $count, 2);

        return $quarter_grade;
    }

    public function getQuarterBudget($quarter)
    {
        $total_budget = 0;
        if ($this->has_budget == true) {
            $budget = Budget::find()->where(['strategic_initiatives_id' => $this->id])->all();
            foreach ($budget as $row) {
                if ($quarter == 1)
                    $total_budget = $row->month_01 + $row->month_02 + $row->month_03;
                if ($quarter == 2)
                    $total_budget = $row->month_04 + $row->month_05 + $row->month_06;
                if ($quarter == 3)
                    $total_budget = $row->month_07 + $row->month_08 + $row->month_09;
                if ($quarter == 4)
                    $total_budget = $row->month_10 + $row->month_11 + $row->month_12;
            }
        }

        return $total_budget;
    }

    public static function getTotalBudget($unitId){
        $totalBudget = self::find()
                            ->alias('i')
                            ->joinWith(['okr o', 'okr.position p'])
                            ->where(['p.unit_structure_id' => $unitId])
                            ->andWhere(['not in', 'i.status_id', [Status::REJECTED, Status::DELETED]])
                            ->sum('total_budget');

        return $totalBudget;
    }

    public function getCustomerList()
    {

        $customers = InitiativesCustomers::find()->where(['initiatives_id' => $this->id])->all();

        if (!empty($customers)) {
            $html = "<ul style='padding-left: 10px;'>";
            foreach ($customers as $cust) {
                $html .= "<li>{$cust->customer->customer}</li>";
            }
            $html .= "</ul>";

            return $html;
        } else {
            return "-";
        }
    }

    public function getStakeholderList()
    {

        $stakeholders = InitiativesStakeholder::find()->where(['initiatives_id' => $this->id])->all();

        if (!empty($stakeholders)) {
            $html = "<ul style='padding-left: 10px;'>";
            foreach ($stakeholders as $stk) {
                $html .= "<li>{$stk->stakeholder->stakeholder}</li>";
            }
            $html .= "</ul>";

            return $html;
        } else {
            return "-";
        }
    }

    public function getOkr()
    {
        return $this->hasOne(\app\models\Okr::className(), ['id' => 'okr_id']);
    }

    public function getStartActivityDate()
    {
        $startDate = Activity::find()->where(['strategic_initiatives_id' => $this->id])->orderBy(['id' => SORT_ASC])->one();

        return $startDate->start_plan;
    }

    public function getEndActivityDate()
    {
        $endDate = Activity::find()->where(['strategic_initiatives_id' => $this->id])->orderBy(['id' => SORT_DESC])->one();

        return $endDate->finish_plan;
    }

    public function getBudgets()
    {
        $activities = Activity::find()->where(['initiatives_id' => $this->id])->all();
        $idActivities = [];
        foreach($activities as $a){
            array_push($idActivities, $a->id);
        }

        $dataProvider = Budget::find()->where(['in', 'activity_id', $idActivities])->orderBy('budget_code');

        return $dataProvider;
    }

    public function getActivities()
    {
        return $this->hasMany(\app\models\Activity::className(), ['initiatives_id' => 'id']);
    }

    public function getStatus()
    {
        return $this->hasOne(\app\models\Status::className(), ['id' => 'status_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(\app\models\PositionStructure::className(), ['id' => 'position_structure_id']);
    }
    
    public function getParticipatory()
    {
        return $this->hasOne(\app\models\ParticipatoryBudgeting::className(), ['id' => 'participatory_budgeting_id']);
    }
    
    public function getValidity()
    {
        return $this->hasOne(\app\models\Validity::className(), ['id' => 'validity_id']);
    }

    public function getControllability()
    {
        return $this->hasOne(\app\models\Controllability::className(), ['id' => 'controllability_id']);
    }

    public function getPolarization()
    {
        return $this->hasOne(\app\models\Polarization::className(), ['id' => 'polarization_id']);
    }

    public function getCalculation()
    {
        return $this->hasOne(\app\models\Calculation::className(), ['id' => 'calculation_id']);
    }

    public function getInitiativesHistories()
    {
        return $this->hasMany(\app\models\InitiativesHistory::className(), ['initiatives_id' => 'id']);
    }
}
