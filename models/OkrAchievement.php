<?php

namespace app\models;

use Yii;
use \app\models\base\OkrAchievement as BaseOkrAchievement;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.okr_achievement".
 */
class OkrAchievement extends BaseOkrAchievement
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    /** RELATIONS */
    public function getEvaluation()
    {
        return $this->hasOne(\app\models\Evaluation::className(), ['id' => 'evaluation_id']);
    }

    public function getOkr()
    {
        return $this->hasOne(\app\models\Okr::className(), ['id' => 'okr_id']);
    }
}
