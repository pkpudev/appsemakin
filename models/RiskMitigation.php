<?php

namespace app\models;

use Yii;
use \app\models\base\RiskMitigation as BaseRiskMitigation;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.risk_mitigation".
 */
class RiskMitigation extends BaseRiskMitigation
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    public function getRisk()
    {
        return $this->hasOne(\app\models\Risk::className(), ['id' => 'risk_id']);
    }

    public function getPic()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'pic_id']);
    }
}
