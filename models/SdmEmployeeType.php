<?php

namespace app\models;

use Yii;
use \app\models\base\SdmEmployeeType as BaseSdmEmployeeType;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sdm_employee_type".
 */
class SdmEmployeeType extends BaseSdmEmployeeType
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
