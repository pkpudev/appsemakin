<?php

namespace app\models\search;

use app\models\Evaluation;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InitiativesEvaluation;
use app\models\Okr;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;

/**
 * InitiativesEvaluationSearch represents the model behind the search form about `app\models\InitiativesEvaluation`.
 */
class InitiativesEvaluationSearch extends InitiativesEvaluation
{
    public $year;
    public $unit_id;
    public $unit_code;
    public $unit_name;
    public $initiatives_code;
    public $directorate_id;
    public $department_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'evaluation_id', 'initiatives_id', 'target', 'year', 'unit_id', 'status_id', 'directorate_id', 'department_id'], 'integer'],
            [['real', 'employees_involved', 'initiatives_code', 'unit_code', 'unit_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InitiativesEvaluation::find()
                                            ->alias('ie')
                                            ->joinWith(['initiatives i', 'initiatives.okr o', 'initiatives.okr.position p']);;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['initiatives_code'=>SORT_ASC]],
        ]);

        $dataProvider->sort->attributes['initiatives_code'] = [
            'asc' => ['i.initiatives_code' => SORT_ASC],
            'desc' => ['i.initiatives_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $userID = Yii::$app->user->id;
        if(!isset($this->year) or empty($this->year)) {
            $lastOkr = Okr::find()->where("year is not null")->orderBy('year desc')->one();
            $this->year = $lastOkr->year ?: date('Y');
        }

        if(!$this->evaluation_id){
            $currentDate= strtotime(date('Y-m-d'));
                        
            if (($currentDate >= strtotime("{$this->year}-01-01")) && ($currentDate <= strtotime(date("Y-m-t", strtotime( $this->year . '-' . '04-01'))))){
                $period = 'Kuartal 1';
            } else if (($currentDate >= strtotime("{$this->year}-04-01")) && ($currentDate <= strtotime(date("Y-m-t", strtotime( $this->year . '-' . '07-01'))))){
                $period = 'Kuartal 2';
            } else if (($currentDate >= strtotime("{$this->year}-07-01")) && ($currentDate <= strtotime(date("Y-m-t", strtotime( $this->year . '-' . '10-01'))))){
                $period = 'Kuartal 3';
            } else if (($currentDate >= strtotime("{$this->year}-10-01")) && ($currentDate <= strtotime(date("Y-m-t", strtotime( $this->year . '-' . '12-01'))))){
                $period = 'Kuartal 4';
            }

            $evaluation = Evaluation::findOne(['year' =>  date('Y'), 'period' => $period]);
            $this->evaluation_id = $evaluation->id;
        }

        if($this->directorate_id){
            $unitIds = [];
            $depts = UnitStructure::find()->where(['superior_unit_id' => $this->directorate_id])->all();
            foreach($depts as $dept){
                array_push($unitIds, $dept->id);

                $units = UnitStructure::find()->where(['superior_unit_id' => $dept->id])->all();
                foreach($units as $unit){
                    array_push($unitIds, $unit->id);
                }
            }
            
            $query->andWhere(['in', 'p.unit_structure_id', $unitIds]);
        }

        if($this->department_id){
            $unitIds = [];
            $units = UnitStructure::find()->where(['superior_unit_id' => $this->department_id])->all();
            foreach($units as $unit){
                array_push($unitIds, $unit->id);
            }
            
            $query->andWhere(['in', 'p.unit_structure_id', $unitIds]);
        }

        if(!Yii::$app->user->can('SuperUser')) {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin') && !Yii::$app->user->can('Admin Direktorat')){

                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 4 || $ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }
                
                $query->andWhere(['in', 'p.unit_structure_id', $dept]);
            } else if(Yii::$app->user->can('Admin Direktorat')){
                $userId = Yii::$app->user->id;
                $dept = [];
    
                $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $this->year])->all();

                foreach($vwEmployeeManagers as $vwEmployeeManager){
                    $directorates = UnitStructure::find()->where(['year' => $this->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                    foreach($directorates as $directorate){
                        array_push($dept, $directorate->id);
                        
                        $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                        foreach($divisions as $division){
                            array_push($dept, $division->id);
                        }
                    }
                }

                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }

                $query->andWhere(['in', 'p.unit_structure_id', $dept]);
            }
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'evaluation_id' => $this->evaluation_id,
            'initiatives_id' => $this->initiatives_id,
            'target' => $this->target,
            'p.unit_structure_id' => $this->unit_id,
            'ie.status_id' => $this->status_id,
            'i.initiatives_code' => $this->initiatives_code,
        ]);

        $query->andFilterWhere(['like', 'real', $this->real])
            ->andFilterWhere(['like', 'employees_involved', $this->employees_involved]);

        return $dataProvider;
    }

    public function searchMonitoring($params, $directorateId = null, $departmentId = null)
    {
        $query = InitiativesEvaluation::find()
                                            ->select('u.unit_code, u.unit_name, p.unit_structure_id')
                                            ->alias('ie')
                                            ->joinWith(['initiatives i', 'initiatives.okr o', 'initiatives.okr.position p', 'initiatives.okr.position.unit u']);;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['unit_code'=>SORT_ASC]],
            'pagination' => ['pageSize' => 10]
        ]);

        $dataProvider->sort->attributes['unit_code'] = [
            'asc' => ['u.unit_code' => SORT_ASC],
            'desc' => ['u.unit_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($directorateId){
            $unitIds = [];
            $depts = UnitStructure::find()->where(['superior_unit_id' => $directorateId])->all();
            foreach($depts as $dept){
                array_push($unitIds, $dept->id);

                $units = UnitStructure::find()->where(['superior_unit_id' => $dept->id])->all();
                foreach($units as $unit){
                    array_push($unitIds, $unit->id);
                }
            }
            
            $query->andWhere(['in', 'p.unit_structure_id', $unitIds]);
        }

        if($departmentId){
            $unitIds = [];
            $units = UnitStructure::find()->where(['superior_unit_id' => $departmentId])->all();
            foreach($units as $unit){
                array_push($unitIds, $unit->id);
            }
            
            $query->andWhere(['in', 'p.unit_structure_id', $unitIds]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'evaluation_id' => $this->evaluation_id,
            'initiatives_id' => $this->initiatives_id,
            'target' => $this->target,
            'p.unit_structure_id' => $this->unit_id,
            'ie.status_id' => $this->status_id,
            'u.year' => $this->year
        ]);

        $query->andFilterWhere(['ilike', 'real', $this->real])
            ->andFilterWhere(['ilike', 'employees_involved', $this->employees_involved])
            ->andFilterWhere(['ilike', 'u.unit_code', $this->unit_code])
            ->andFilterWhere(['ilike', 'u.unit_name', $this->unit_name]);

        $query->groupBy('u.unit_code, u.unit_name, p.unit_structure_id');

        return $dataProvider;
    }
}
