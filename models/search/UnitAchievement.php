<?php

namespace app\models\search;

use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UnitAchievement as UnitAchievementModel;
use app\models\UnitStructure;

/**
 * UnitAchievement represents the model behind the search form about `app\models\UnitAchievement`.
 */
class UnitAchievement extends UnitAchievementModel
{
    public $year;
    public $achieve_value_initiatives;
    public $fulfillment_initiatives;
    public $directorate_id;
    public $department_id;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'unit_id', 'evaluation_id', 'directorate_id', 'department_id', 'year'], 'integer'],
            [['achieve_type'], 'safe'],
            [['achieve_value', 'fulfillment', 'achieve_value_initiatives', 'fulfillment_initiatives'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UnitAchievementModel::find()
            ->joinWith(['position p', 'position.unit u'])
            ->orderBy('u.unit_code');

// var_dump($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!Yii::$app->user->can('SuperUser')) {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin')){

                $userID = Yii::$app->user->id;
                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }
                
                $query->andWhere(['in', 'p.unit_structure_id', $dept]);
            }
        }

        if($this->directorate_id){
            $unitIds = [];
            $depts = UnitStructure::find()->where(['superior_unit_id' => $this->directorate_id])->all();
            foreach($depts as $dept){
                array_push($unitIds, $dept->id);

                $units = UnitStructure::find()->where(['superior_unit_id' => $dept->id])->all();
                foreach($units as $unit){
                    array_push($unitIds, $unit->id);
                }
            }
            
            $query->andWhere(['in', 'p.unit_structure_id', $unitIds]);
        }

        if($this->department_id){
            $unitIds = [];
            $units = UnitStructure::find()->where(['superior_unit_id' => $this->department_id])->all();
            foreach($units as $unit){
                array_push($unitIds, $unit->id);
            }
            
            $query->andWhere(['in', 'p.unit_structure_id', $unitIds]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'p.unit_structure_id' => $this->unit_id,
            'p.year' => $this->year,
            'evaluation_id' => $this->evaluation_id,
            'achieve_value' => $this->achieve_value,
            'fulfillment' => $this->fulfillment,
            'achieve_type' => 'OKR',
        ]);

        // if($this->unit_id)
        // $query->andWhere(['in', 'p.unit_structure_id', $this->unit_id]);
        // $query->andWhere(['in', 'p.unit_structure_id', explode(',', $this->unit_id)]);

        // var_dump($query->createCommand()->getRawSql());
        // die;
        // var_dump('asd');

        return $dataProvider;
    }
}
