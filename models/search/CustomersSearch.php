<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customers;

/**
 * CustomersSearch represents the model behind the search form about `app\models\Customers`.
 */
class CustomersSearch extends Customers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category'], 'integer'],
            [['customer', 'description', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customers::find()
            ->alias('c')
            ->joinWith('createdBy0 cb')
            ->joinWith('updatedBy0 ub');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'c.id' => $this->id,
            'c.is_active' => $this->is_active,
            'c.created_at' => $this->created_at,
            'c.updated_at' => $this->updated_at,
            'c.category' => $this->category,
        ]);

        $query->andFilterWhere(['ilike', 'c.customer', $this->customer])
            ->andFilterWhere(['ilike', 'c.description', $this->description])
            ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
            ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by]);

        return $dataProvider;
    }
}
