<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\Status;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use app\models\VwOkr;

/**
 * OkrSearch represents the model behind the search form about `app\models\Okr`.
 */
class OkrSearch extends Okr
{

    public $id_objective;
    public $objective_code;
    public $objective_desc;
    public $key_result_code;
    public $key_result_desc;
    public $unit_id;
    
    //untuk model Initiatives
    public $initiative_id;
    public $total_budget;
    public $status_id;
    public $workplan_name;
    public $initiatives_code;
    public $initiatives;
    public $i_position_structure_id;
    public $i_implementation;
    public $i_target;
    public $i_measure;
    public $i_note;
    public $i_tools;
    public $i_validity_id;
    public $i_controllability_id;
    public $i_grade;
    public $participatory_budgeting_id;
    public $i_participatory_budgeting;
    public $pic_id;
    public $i_q1;
    public $i_q2;
    public $i_q3;
    public $i_q4;
    public $is_completed;
    public $pic_name;

    public $child_of;
    public $unit_name;
    public $parent_okr_code;
    public $parent_okr;
    public $child_id;
    public $child_okr_code;
    public $child_okr;
    public $child_weight;
    public $child_measure;
    public $child_target_q1;
    public $child_timebound_q1;
    public $child_target_q2;
    public $child_timebound_q2;
    public $child_target_q3;
    public $child_timebound_q3;
    public $child_target_q4;
    public $child_timebound_q4;
    public $child_polarization_id;
    public $child_calculation_id;
    public $child_validity_id;
    public $child_controllability_id;

     /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['child_of', 'id', 'okr_level_id', 'unit_id', 'position_structure_id', 'parent_id', 'ref_id', 'cascading_type_id', 'year', 'version', 'pic_id', 'id_objective', 'pic_name'], 'integer'],
            [['okr', 'type', 'okr_code', 'measure', 'timebound_q1', 'timebound_q2', 'timebound_q3', 'timebound_q4', 'created_at', 'updated_at', 'objective_code', 
            'objective_desc', 'key_result_code', 'key_result_desc', 'initiative_id', 'total_budget', 'status_id', 'workplan_name', 'initiatives_code', 
            'initiatives', 'i_position_structure_id', 'i_implementation', 'i_target', 'i_measure', 'i_note', 'i_tools', 'i_validity_id', 'i_controllability_id', 
            'participatory_budgeting_id', 'i_grade', 'i_q1', 'i_q2', 'i_q3', 'i_q4', 'parent_okr_code', 'parent_okr', 'created_by', 'updated_by',
            'i_participatory_budgeting', 'child_okr_code', 'child_okr'], 'safe'],
            [['target'], 'number'],
            [['is_completed'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type = 'KR', $level = null)
    {
        $query = Okr::find()
                    ->alias('o')
                    ->joinWith('parent pr')
                    ->joinWith('position ps')
                    ->joinWith('createdBy0 cb')
                    ->joinWith('updatedBy0 ub')
                    ->joinWith('pic p')
                    ->where(['o.type' => $type])
                    ->andWhere("o.status_id is null");
                    // ->andWhere("o.status_id <> " . Status::OKR_CANCEL);

        if(Yii::$app->controller->id == 'dashboard'){
            $sort = ['pic_name' => SORT_ASC, 'parent_okr_code' => SORT_ASC, 'current_okr_code' => SORT_ASC];
        } else {
            $sort = ['ref_initiatives_id' => SORT_ASC, 'parent_okr_code' => SORT_ASC, 'current_okr_code' => SORT_ASC];
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => $sort]
        ]);

        $dataProvider->sort->attributes['parent_okr_code'] = [
            'asc' => ['pr.okr_code' => SORT_ASC],
            'desc' => ['pr.okr_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['current_okr_code'] = [
            'asc' => ['o.okr_code' => SORT_ASC],
            'desc' => ['o.okr_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['pic_name'] = [
            'asc' => ['p.full_name' => SORT_ASC],
            'desc' => ['p.full_name' => SORT_DESC],
        ];

        $this->load($params);

        // if(!isset($this->year) or empty($this->year)) {
            if(Yii::$app->user->can('Admin') || Yii::$app->user->can('Leader')){
                if(!isset($this->year) or empty($this->year)){
                    $lastOkr = Okr::find()->where("year is not null")->orderBy('year desc')->one();
                    $this->year = $lastOkr->year ?: date('Y');
                }
            } else {
                $this->year = date('Y');
            }
        // }


        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        if(Yii::$app->controller->action->id == 'individual' || $level == 'individual'){
            $query->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL]);
        } else if(Yii::$app->controller->id == 'dashboard'){
            $query->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL]);
        } else if(Yii::$app->controller->id == 'dashboard2' && Yii::$app->controller->action->id == 'okr-org2'){
            $query->andWhere(['o.okr_level_id' => OkrLevel::ORGANIZATION]);
        } else {
            $query->andWhere('o.okr_level_id <> ' . OkrLevel::INDIVIDUAL);
        }

        if($this->is_completed == 1){
            $query->andWhere('o.polarization_id is not null and o.calculation_id is not null and o.validity_id is not null and o.controllability_id is not null');
        } else if($this->is_completed == 2){
            $query->andWhere('o.polarization_id is null or o.calculation_id is null or o.validity_id is null or o.controllability_id is null');
        }

        if(!Yii::$app->user->can('SuperUser') && Yii::$app->controller->id != 'dashboard2') {
            
            if(!Yii::$app->user->can('BOD') && !Yii::$app->user->can('Admin')  && !Yii::$app->user->can('Observer') && !Yii::$app->user->can('Admin Direktorat')){

                $userID = Yii::$app->user->id;
                $isLeader = false;
                
                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }

                    if($ep->position->level == 2 or $ep->position->level == 4) $isLeader = true;
                }

                // var_dump(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && ($isSuperVisor && Yii::$app->user->identity->branch_id != 1));
                
                if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                    $maxYear = date('Y');
    
                    if($this->year > $maxYear){
                        $year = '0000';
                    } else {
                        $year = $this->year;
                    }
                } else {
                    $year = $this->year;
                }

                $query->andWhere(['o.year' => $year]);

                $query->andWhere(['in', 'ps.unit_structure_id', $dept]);
            } else if(Yii::$app->user->can('Admin Direktorat')){
                $userId = Yii::$app->user->id;
                $dept = [];
    
                $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $this->year])->all();

                foreach($vwEmployeeManagers as $vwEmployeeManager){
                    $directorates = UnitStructure::find()->where(['year' => $this->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                    foreach($directorates as $directorate){
                        array_push($dept, $directorate->id);
                        
                        $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                        foreach($divisions as $division){
                            array_push($dept, $division->id);
                        }
                    }
                }
    

                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }

                $query->andWhere(['in', 'ps.unit_structure_id', $dept]);
            } 
        }

        $query->andFilterWhere([
            'o.id' => $this->id,
            'o.okr_level_id' => $this->okr_level_id,
            'o.position_structure_id' => $this->position_structure_id,
            'o.parent_id' => $this->parent_id,
            'o.ref_id' => $this->ref_id,
            'o.cascading_type_id' => $this->cascading_type_id,
            'o.target' => $this->target,
            'o.timebound_q1' => $this->timebound_q1,
            'o.timebound_q2' => $this->timebound_q2,
            'o.timebound_q3' => $this->timebound_q3,
            'o.timebound_q4' => $this->timebound_q4,
            'o.created_at' => $this->created_at,
            'o.updated_at' => $this->updated_at,
            'o.year' => $this->year,
            'o.version' => $this->version,
            'ps.unit_structure_id' => $this->unit_id,
            'o.pic_id' => $this->pic_id,
            // 'o.pic_id' => $this->pic_name
        ]);

        $query->andFilterWhere(['ilike', 'o.okr', $this->okr])
            ->andFilterWhere(['ilike', 'type', $this->type])
            ->andFilterWhere(['ilike', 'o.okr_code', $this->okr_code])
            ->andFilterWhere(['ilike', 'pr.okr_code', $this->parent_okr_code])
            ->andFilterWhere(['ilike', 'pr.okr', $this->parent_okr])
            ->andFilterWhere(['ilike', 'measure', $this->measure])
            ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
            ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by]);

        // var_dump($query->createCommand()->getRawSql());

        return $dataProvider;
    }

    public function search2($params)
    {
        $query = self::find()
                    ->alias('o')
                    ->joinWith(['child ch', 'position ps', 'position.unit u', 'createdBy0 cb', 'updatedBy0 ub', 'pic p'])
                    ->select("
                                u.unit_name,
                                o.id as id, 
                                o.okr_code, 
                                o.okr, 
                                o.okr_level_id, 
                                ch.okr_code as child_okr_code, 
                                ch.okr as child_okr, 
                                ch.id as child_id, 
                                ch.weight as child_weight,
                                ch.measure as child_measure,
                                ch.target_q1 as child_target_q1,
                                ch.timebound_q1 as child_ttimebound_q1,
                                ch.target_q2 as child_target_q2,
                                ch.timebound_q2 as child_ttimebound_q2,
                                ch.target_q3 as child_target_q3,
                                ch.timebound_q3 as child_ttimebound_q3,
                                ch.target_q4 as child_target_q4,
                                ch.timebound_q4 as child_timebound_q4,
                                ch.polarization_id as child_polarization_id,
                                ch.calculation_id as child_calculation_id,
                                ch.validity_id as child_validity_id,
                                ch.controllability_id as child_controllability_id,
                            ")
                    ->where(['o.type' => 'O'])
                    // ->andWhere("ch.type <> 'O'")
                    ->andWhere("o.status_id is null");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['unit_code' => SORT_ASC, 'okr_code' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['child_okr_code'] = [
            'asc' => ['ch.okr_code' => SORT_ASC],
            'desc' => ['ch.okr_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['current_okr_code'] = [
            'asc' => ['o.okr_code' => SORT_ASC],
            'desc' => ['o.okr_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['pic_name'] = [
            'asc' => ['p.full_name' => SORT_ASC],
            'desc' => ['p.full_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['unit_name'] = [
            'asc' => ['u.unit_name' => SORT_ASC],
            'desc' => ['u.unit_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['unit_code'] = [
            'asc' => ['u.unit_code' => SORT_ASC],
            'desc' => ['u.unit_code' => SORT_DESC],
        ];

        $this->load($params);

        if(!isset($this->year) or empty($this->year)) {
            if(Yii::$app->user->can('Admin') || Yii::$app->user->can('Leader')){
                $lastOkr = Okr::find()->where("year is not null")->orderBy('year desc')->one();
                $this->year = $lastOkr->year ?: date('Y');
            } else {
                $this->year = date('Y');
            }
        }

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        if(Yii::$app->controller->action->id == 'individual'){
            $query->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL]);
        } else if(Yii::$app->controller->id == 'dashboard'){
            $query->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL]);
        } else if(Yii::$app->controller->id == 'dashboard2' && Yii::$app->controller->action->id == 'okr-org2'){
            $query->andWhere(['o.okr_level_id' => OkrLevel::ORGANIZATION]);
        } else {
            $query->andWhere('o.okr_level_id <> ' . OkrLevel::INDIVIDUAL);
        }

        if($this->is_completed == 1){
            $query->andWhere('ch.polarization_id is not null and ch.calculation_id is not null and ch.validity_id is not null and ch.controllability_id is not null');
        } else if($this->is_completed == 2){
            $query->andWhere('ch.polarization_id is null or ch.calculation_id is null or ch.validity_id is null or ch.controllability_id is null');
        }

        if(!Yii::$app->user->can('SuperUser') && Yii::$app->controller->id != 'dashboard2') {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin')){

                $userID = Yii::$app->user->id;
                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }
                
                $query->andWhere(['in', 'ps.unit_structure_id', $dept]);
            }
        }

        $query->andFilterWhere([
            'o.id' => $this->id,
            'o.okr_level_id' => $this->okr_level_id,
            'o.position_structure_id' => $this->position_structure_id,
            'o.parent_id' => $this->parent_id,
            'o.ref_id' => $this->ref_id,
            'o.cascading_type_id' => $this->cascading_type_id,
            'o.target' => $this->target,
            'o.timebound_q1' => $this->timebound_q1,
            'o.timebound_q2' => $this->timebound_q2,
            'o.timebound_q3' => $this->timebound_q3,
            'o.timebound_q4' => $this->timebound_q4,
            'o.created_at' => $this->created_at,
            'o.updated_at' => $this->updated_at,
            'o.year' => $this->year,
            'o.version' => $this->version,
            'ps.unit_structure_id' => $this->unit_id,
            'o.pic_id' => $this->pic_id,
            // 'o.pic_id' => $this->pic_name
        ]);

        $query->andFilterWhere(['ilike', 'o.okr', $this->okr])
            ->andFilterWhere(['ilike', 'type', $this->type])
            ->andFilterWhere(['ilike', 'o.okr_code', $this->okr_code])
            ->andFilterWhere(['ilike', 'ch.okr_code', $this->child_okr_code])
            ->andFilterWhere(['ilike', 'ch.okr', $this->child_okr])
            ->andFilterWhere(['ilike', 'measure', $this->measure])
            ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
            ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by]);

        // var_dump($query->createCommand()->getRawSql());
        
        return $dataProvider;
    }

    public function searchVwOkr($params)
    {
        $query = VwOkr::find()
                    ->alias('vw')
                    ->joinWith(['parent p', 'child ch', 'parent.position ps', 'parent.position.unit u'])
                    ->where(['p.type' => 'O'])
                    ->andWhere("(ch.type='KR' or child_id is null)");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['unit_code' => SORT_ASC, 'child_okr_code' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['unit_code'] = [
            'asc' => ['u.unit_code' => SORT_ASC],
            'desc' => ['u.unit_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['child_okr_code'] = [
            'asc' => ['ch.okr_code' => SORT_ASC],
            'desc' => ['ch.okr_code' => SORT_DESC],
        ];

        $this->load($params);

        if(!isset($this->year) or empty($this->year)) {
            if(Yii::$app->user->can('Admin') || Yii::$app->user->can('Leader')){
                $lastOkr = Okr::find()->where("year is not null")->orderBy('year desc')->one();
                $this->year = $lastOkr->year ?: date('Y');
            } else {
                $this->year = date('Y');
            }
        }

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     $query->where('0=1');
        //     return $dataProvider;
        // }

        $query->andWhere('p.okr_level_id <> ' . OkrLevel::INDIVIDUAL);

        if(!Yii::$app->user->can('SuperUser')) {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin') and !Yii::$app->user->can('Admin Direktorat') && !Yii::$app->user->can('Observer')){

                $userID = Yii::$app->user->id;
                $isSuperVisor = false;

                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                    
                        $units = Yii::$app->db->createCommand("SELECT id FROM management.unit_structure WHERE superior_unit_id = {$ep->position->unit_structure_id}")->queryColumn();
                        foreach($units as $unit){
                            array_push($dept, $unit);
                        }
                        
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            
                        // if($gm->unit_structure_id == 671) var_dump($gm->position_name);
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }

                    if($ep->position->level == 2 || $ep->position->level == 4) $isSuperVisor = true;
                }

                if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && ($isSuperVisor)){
                        $year = $this->year;
                } else {
                    $maxYear = date('Y');
                    if($this->year > $maxYear){
                        $year = $maxYear;
                    } else {
                        $year = $this->year;
                    }
                }

                $query->andWhere(['vw.year' => $year]);

                $query->andWhere(['in', 'ps.unit_structure_id', $dept]);
            } else if(Yii::$app->user->can('Admin Direktorat')){
                $userId = Yii::$app->user->id;
                $dept = [];
    
                $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $this->year])->all();

                foreach($vwEmployeeManagers as $vwEmployeeManager){
                    $directorates = UnitStructure::find()->where(['year' => $this->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                    foreach($directorates as $directorate){
                        array_push($dept, $directorate->id);
                        
                        $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                        foreach($divisions as $division){
                            array_push($dept, $division->id);
                        }
                    }
                }

                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }

                $query->andWhere(['in', 'ps.unit_structure_id', $dept]);
            } 
        }

        if($this->child_of){
            // LEFT JOIN "management"."okr" "p2" ON "p2"."parent_id" = "p"."id" 
            $query->leftJoin('management.okr p2', 'p2.id = p.parent_id');
            $query->andWhere(['p2.position_structure_id' => $this->child_of]);
        }

        $query->andFilterWhere([
            'vw.year' => $this->year,
            'p.okr_level_id' => $this->okr_level_id,
            'ps.unit_structure_id' => $this->unit_id,
            'ps.id' => $this->position_structure_id,
        ]);

        $query->andFilterWhere(['ilike', 'p.okr', $this->okr])
            ->andFilterWhere(['ilike', 'p.okr_code', $this->okr_code])
            ->andFilterWhere(['ilike', 'ch.okr_code', $this->child_okr_code])
            ->andFilterWhere(['ilike', 'ch.okr', $this->child_okr]);

        // var_dump($query->createCommand()->getRawSql());
        
        return $dataProvider;
    }

    public function searchForInitiatives($params)
    {
        $query = self::find()->alias('o')
            ->joinWith(['position p', 'initiatives i', 'initiatives.participatory pb'])
            ->select("o.okr_code, o.okr, o.id as id_objective, p.unit_structure_id AS unit_id, o.okr AS objective, i.id AS initiative_id, i.*, pb.participatory_budgeting AS participatory_budgeting");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['okr_code'=>SORT_ASC]],
        ]);

        $this->load($params);

        $userID = Yii::$app->user->id;
        $companyID = Yii::$app->user->identity->company_id;

        if(!isset($this->year) or empty($this->year)) {
            if(Yii::$app->user->can('Admin') || Yii::$app->user->can('Leader')){
                $lastOkr = Okr::find()->where("year is not null")->orderBy('year desc')->one();
                $this->year = $lastOkr->year ?: date('Y');
            } else {
                $this->year = date('Y');
            }
        }
        
        if(!Yii::$app->user->can('SuperUser')) {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin') and !Yii::$app->user->can('Admin Direktorat') and !Yii::$app->user->can('Observer')){
                $userID = Yii::$app->user->id;
                $isSuperVisor = false;
                
                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }

                    if($ep->position->level == 2 || $ep->position->level == 4) $isSuperVisor = true;
                }
                
                if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && ($isSuperVisor)){
                    $year = $this->year;
                } else {
                    $maxYear = date('Y');
                    if($this->year > $maxYear){
                        $year = $maxYear;
                    } else {
                        $year = $this->year;
                    }
                }

                $query->andWhere(['o.year' => $year]);

                $query->andWhere(['in', 'p.unit_structure_id', $dept]);
            }  else if(Yii::$app->user->can('Admin Direktorat')){
                $userId = Yii::$app->user->id;
                $dept = [];
    
                $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $this->year])->all();
    
                foreach($vwEmployeeManagers as $vwEmployeeManager){
                    $directorates = UnitStructure::find()->where(['year' => $this->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                    foreach($directorates as $directorate){
                        array_push($dept, $directorate->id);
                        
                        $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                        foreach($divisions as $division){
                            array_push($dept, $division->id);
                        }
                    }
                }
                
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }

                $query->andWhere(['in', 'p.unit_structure_id', $dept]);
            }
        } else {
            $query->andWhere(['o.year' => $this->year]);
        }
        
        if($this->year == 2022){
            $query->andWhere(['o.type' => 'O']);
        } else {
            $query->andWhere(['o.type' => 'KR']);
        }

        $query->andWhere(['not in', 'o.okr_level_id', [OkrLevel::ORGANIZATION, OkrLevel::INDIVIDUAL]]);
        // $query->andWhere(['o.status_id' => \app\models\Status::APPROVED]);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        if(Yii::$app->controller->action->id == 'approval-all'){
            $query->andWhere(['i.status_id' => Status::SEND]);
        }

        if($this->participatory_budgeting_id){
            if($this->participatory_budgeting_id == 999999){
                $query->andWhere('i.participatory_budgeting_id IS NULL');
            } else {
                $query->andFilterWhere(['i.participatory_budgeting_id' => $this->participatory_budgeting_id]);
            }
        }
        
        $query->andFilterWhere([
                // 'k.status_id' => \app\models\Status::APPROVED,
                'p.id' => $this->position_structure_id,
                'p.unit_structure_id' => $this->unit_id,
                'i.total_budget' => $this->total_budget,
                'i.target' => $this->i_target,
                'i.validity_id' => $this->validity_id,
                'i.controllability_id' => $this->controllability_id,
                'i.status_id' => $this->status_id,
                'i.pic_id' => $this->pic_id,
                'i.position_structure_id' => $this->i_position_structure_id
            ]);

        $query->andFilterWhere(['ilike', 'o.okr_code', $this->okr_code])
            ->andFilterWhere(['ilike', 'o.okr', $this->okr])
            ->andFilterWhere(['ilike', 'i.workplan_name', $this->workplan_name])
            ->andFilterWhere(['ilike', 'i.implementation', $this->i_implementation])
            ->andFilterWhere(['ilike', 'i.initiatives_code', $this->initiatives_code])
            ->andFilterWhere(['ilike', 'i.initiatives', $this->initiatives])
            ->andFilterWhere(['ilike', 'i.measure', $this->i_measure])
            ->andFilterWhere(['ilike', 'i.grade', $this->i_grade])
            ->andFilterWhere(['ilike', 'i.monitoring_tools', $this->i_tools])
            ->andFilterWhere(['ilike', 'i.target_q1', $this->i_q1])
            ->andFilterWhere(['ilike', 'i.target_q2', $this->i_q2])
            ->andFilterWhere(['ilike', 'i.target_q3', $this->i_q3])
            ->andFilterWhere(['ilike', 'i.target_q4', $this->i_q4])
            ->andFilterWhere(['ilike', 'pb.participatory_budgeting', $this->i_participatory_budgeting])
            ->andFilterWhere(['ilike', 'i.note', $this->i_note]);

        $query->orderBy(['o.okr_code'=>SORT_ASC,'p.unit_structure_id'=>SORT_ASC,'i.id'=>SORT_ASC]);

        // var_dump($query->createCommand()->getRawSql());
        
        return $dataProvider;
    }

    public function getInitiative()
    {
        return $this->hasOne(\app\models\Initiatives::className(), ['id' => 'initiative_id']);
    }

    public function getChild()
    {
        return $this->hasOne(\app\models\Okr::className(), ['parent_id' => 'id']);
    }

    public function getUnit()
    {
        return $this->hasOne(\app\models\UnitStructure::className(), ['id' => 'unit_id']);
    }
}
