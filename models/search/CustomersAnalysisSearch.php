<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomersAnalysis;

/**
 * CustomersAnalysisSearch represents the model behind the search form about `app\models\CustomersAnalysis`.
 */
class CustomersAnalysisSearch extends CustomersAnalysis
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['needs', 'expectation', 'created_at', 'updated_at', 'customers_id', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomersAnalysis::find()
                    ->alias('cs')
                    ->joinWith('customers c')
                    ->joinWith('createdBy0 cb')
                    ->joinWith('updatedBy0 ub');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['customers_id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'cs.id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'needs', $this->needs])
            ->andFilterWhere(['ilike', 'expectation', $this->expectation])
            ->andFilterWhere(['ilike', 'c.customer', $this->customers_id])
            ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
            ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by]);

        return $dataProvider;
    }
}
