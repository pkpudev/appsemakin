<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RiskCategory;

/**
 * RiskCategorySearch represents the model behind the search form about `app\models\RiskCategory`.
 */
class RiskCategorySearch extends RiskCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'risk_category', 'description'], 'integer'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RiskCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'risk_category' => $this->risk_category,
            'description' => $this->description,
            'is_active' => $this->is_active,
        ]);

        return $dataProvider;
    }
}
