<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StrategyAnalysis;

/**
 * StrategyAnalysisSearch represents the model behind the search form about `app\models\StrategyAnalysis`.
 */
class StrategyAnalysisSearch extends StrategyAnalysis
{

    public $situation_analysis_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'internal_factor_id', 'external_factor_id', 'strategy_id', 'created_by', 'updated_by'], 'integer'],
            [['strategy_type', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StrategyAnalysis::find()
                    ->alias('stra')
                    ->joinWith('strategy s');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['strategy' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['strategy'] = [
            'asc' => ['s.strategy' => SORT_ASC],
            'desc' => ['s.strategy' => SORT_DESC],
        ];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'stra.id' => $this->id,
            'stra.internal_factor_id' => $this->internal_factor_id,
            'stra.external_factor_id' => $this->external_factor_id,
            'stra.strategy_id' => $this->strategy_id,
            'stra.created_at' => $this->created_at,
            'stra.created_by' => $this->created_by,
            'stra.updated_at' => $this->updated_at,
            'stra.updated_by' => $this->updated_by,
            's.situation_analysis_id' => $this->situation_analysis_id,
        ]);

        $query->andFilterWhere(['like', 'stra.strategy_type', $this->strategy_type]);

        return $dataProvider;
    }
}
