<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PositionStructure;

/**
 * PositionStructureSearch represents the model behind the search form about `app\models\PositionStructure`.
 */
class PositionStructureSearch extends PositionStructure
{

    public $f_directorate;
    public $f_department;
    public $f_division;
    public $f_squad;
    public $ucode;
    public $uname;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'position_id', 'level', 'created_by', 'updated_by'], 'integer'],
            [['position_name', 'position_code', 'role', 'responsibility', 'authority', 
            'created_at', 'updated_at', 'responsible_to_id', 'unit_structure_id',
            'f_directorate', 'f_department', 'f_division', 'f_squad', 'ucode', 'uname'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PositionStructure::find()
                    ->alias('ps')
                    ->joinWith('resp r')
                    ->joinWith('unit u')
                    ->where("ps.id NOT IN (4390, 4391, 4392, 4393, 4474, 4394, 4395, 4396, 4397, 4398, 3851, 3852, 4597, 3816, 3873, 4026, 4085, 4095)");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['ucode' => SORT_ASC, 'position_name' => SORT_ASC]],
        ]);

        $dataProvider->sort->attributes['pcode'] = [
            'asc' => ['ps.position_code' => SORT_ASC],
            'desc' => ['ps.position_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ucode'] = [
            'asc' => ['u.unit_code' => SORT_ASC],
            'desc' => ['u.unit_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['uname'] = [
            'asc' => ['u.unit_name' => SORT_ASC],
            'desc' => ['u.unit_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if($this->f_directorate){
            $query->andWhere(['u.level' => 1]);
            $query->andWhere(['ilike', 'u.unit_name', $this->f_directorate]);
        }

        if($this->f_department){
            $query->andWhere(['u.level' => 2]);
            $query->andWhere(['ilike', 'u.unit_name', $this->f_department]);
        }

        if($this->f_division){
            $query->andWhere(['u.level' => 3]);
            $query->andWhere(['ilike', 'u.unit_name', $this->f_division]);
        }

        if($this->f_squad){
            $query->andWhere(['u.level' => 5]);
            $query->andWhere(['ilike', 'u.unit_name', $this->f_squad]);
        }

        if(Yii::$app->controller->action->id == 'budget2' and $this->year > 2024){
            $query->andWhere('ps.level >= 4');
            $query->andWhere("EXISTS (SELECT 1 FROM management.ceiling WHERE position_structure_id = ps.id and year = {$this->year})");
        } else if(Yii::$app->controller->action->id == 'okr-organization2' and $this->year > 2024){
            $query->andWhere('ps.level >= 4');
        }

        $query->andFilterWhere([
            'ps.id' => $this->id,
            'ps.year' => $this->year,
            'ps.position_id' => $this->position_id,
            'ps.level' => $this->level,
            // 'ps.unit_structure_id' => $this->unit_structure_id,
            'ps.created_at' => $this->created_at,
            'ps.created_by' => $this->created_by,
            'ps.updated_at' => $this->updated_at,
            'ps.updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'ps.position_name', $this->position_name])
            ->andFilterWhere(['ilike', 'ps.position_code', $this->position_code])
            ->andFilterWhere(['ilike', 'ps.role', $this->role])
            ->andFilterWhere(['ilike', 'ps.responsibility', $this->responsibility])
            ->andFilterWhere(['ilike', 'r.position_name', $this->responsible_to_id])
            ->andFilterWhere(['ilike', 'ps.authority', $this->authority])
            ->andFilterWhere(['ilike', 'u.unit_code', $this->ucode])
            ->andFilterWhere(['ilike', 'u.unit_name', $this->uname]);

        return $dataProvider;
    }
}
