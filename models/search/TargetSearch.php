<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Target;

/**
 * TargetSearch represents the model behind the search form about `app\models\Target`.
 */
class TargetSearch extends Target
{
    public $unit_code;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'target_type_id'], 'integer'],
            [['target_name', 'created_at', 'updated_at', 'created_by', 'updated_by', 'unit_structure_id', 'measure', 'unit_code'], 'safe'],
            [['total_target', 'month_01', 'month_02', 'month_03', 'month_04', 'month_05', 'month_06', 'month_07', 'month_08', 'month_09', 'month_10', 'month_11', 'month_12'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Target::find()
                            ->alias('t')
                            ->joinWith('unit u')
                            ->joinWith('createdBy0 cb')
                            ->joinWith('updatedBy0 ub');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['unit_code' => SORT_ASC]],
        ]);

        $dataProvider->sort->attributes['unit_code'] = [
            'asc' => ['u.unit_code' => SORT_ASC],
            'desc' => ['u.unit_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            't.id' => $this->id,
            't.year' => $this->year,
            't.total_target' => $this->total_target,
            't.month_01' => $this->month_01,
            't.month_02' => $this->month_02,
            't.month_03' => $this->month_03,
            't.month_04' => $this->month_04,
            't.month_05' => $this->month_05,
            't.month_06' => $this->month_06,
            't.month_07' => $this->month_07,
            't.month_08' => $this->month_08,
            't.month_09' => $this->month_09,
            't.month_10' => $this->month_10,
            't.month_11' => $this->month_11,
            't.month_12' => $this->month_12,
            't.created_at' => $this->created_at,
            't.updated_at' => $this->updated_at,
            't.target_type_id' => $this->target_type_id,
        ]);

        $query->andFilterWhere(['ilike', 't.target_name', $this->target_name])
        ->andFilterWhere(['ilike', 't.measure', $this->measure])
        ->andFilterWhere(['ilike', 'u.unit_name', $this->unit_structure_id])
        ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
        ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by])
        ->andFilterWhere(['ilike', 'u.unit_code', $this->unit_code]);

        return $dataProvider;
    }
}
