<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Threats;

/**
 * ThreatsSearch represents the model behind the search form about `app\models\Threats`.
 */
class ThreatsSearch extends Threats
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'situation_analysis_id', 'external_variable_id', 'created_by', 'updated_by'], 'integer'],
            [['threat', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Threats::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'situation_analysis_id' => $this->situation_analysis_id,
            'external_variable_id' => $this->external_variable_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'threat', $this->threat]);

        return $dataProvider;
    }
}
