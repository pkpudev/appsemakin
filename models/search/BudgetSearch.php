<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Budget;
use app\models\Okr;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\Status;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;

/**
 * BudgetSearch represents the model behind the search form about `app\models\Budget`.
 */
class BudgetSearch extends Budget
{
    public $year;
    public $unit_id;
    public $unit_name;
    public $unit_code;
    public $budget_status;
    public $budget_measure;
    public $workplan_name;
    public $i_code;
    public $i_status_id;
    public $position_structure_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_structure_id', 'id', 'activity_id', 'volume', 'frequency', 'year', 'status_id', 'source_of_funds_id', 'cost_type_id', 'cost_priority_id', 'account_id', 'financing_type_id', 'initiatives_id', 'unit_id'], 'integer'],
            [['budget_code', 'description', 'measure', 'note', 'freq_measure', 'unit_name', 'unit_code', 'i_code', 'i_status_id'], 'safe'],
            [['unit_amount', 'total_amount', 'month_01', 'month_02', 'month_03', 'month_04', 'month_05', 'month_06', 'month_07', 'month_08', 'month_09', 'month_10', 'month_11', 'month_12', 'total_real', 'real_month_01', 'real_month_02', 'real_month_03', 'real_month_04', 'real_month_05', 'real_month_06', 'real_month_07', 'real_month_08', 'real_month_09', 'real_month_10', 'real_month_11', 'real_month_12'], 'number'],
            [['can_over_budget'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->alias('b')
            ->joinWith(['initiatives i', 'initiatives.okr o', 'initiatives.okr.position p', 'initiatives.okr.position.unit u'])
            ->select("u.unit_code, u.unit_name, i.workplan_name, i.initiatives_code, i.status_id as i_status_id, b.id as budget_id, b.measure as budget_measure, b.status_id as budget_status, b.frequency, b.*, o.position_structure_id");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['i_code' => SORT_ASC]]
        ]);


        $dataProvider->sort->attributes['i_code'] = [
            'asc' => ['i.initiatives_code' => SORT_ASC],
            'desc' => ['i.initiatives_code' => SORT_DESC],
        ];

        $this->load($params);

        $userID = Yii::$app->user->id;
        $companyID = Yii::$app->user->identity->company_id;
        if(!isset($this->year) or empty($this->year)) {
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $this->year = $lastOkr->year ?: date('Y');
        }
        
        if(!Yii::$app->user->can('SuperUser')) {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin') and !Yii::$app->user->can('Keuangan') and !Yii::$app->user->can('Admin Direktorat') ){

                $isSuperVisor = false;
                
                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }

                    if(($ep->position->level == 2 && Yii::$app->user->identity->branch_id != 1) || $ep->position->level == 4) $isSuperVisor = true;
                }
                
                if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) || (($isSuperVisor && Yii::$app->user->identity->branch_id != 1) || Yii::$app->user->can('Leader'))){
                    
                    $maxYear = date('Y', strtotime(Yii::$app->params['ttdKontrakDate'])) + 1;
    
                    if($this->year > $maxYear){
                        $year = $maxYear;
                    } else {
                        $year = $this->year;
                    }
                    // var_dump('asd');
                } else {
                    $maxYear = date('Y');
    
                    if($this->year > $maxYear){
                        $year = $maxYear;
                    } else {
                        $year = $this->year;
                    }
                }

                $query->andWhere(['o.year' => $year]);
                
                $query->andWhere(['in', 'p.unit_structure_id', $dept]);

            } else if(Yii::$app->user->can('Admin Direktorat')){
                $userId = Yii::$app->user->id;
                $dept = [];

                $vwEmployeeManager = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $this->year])->one();
                array_push($dept, $vwEmployeeManager->directorate_id);

                $directorates = UnitStructure::find()->where(['year' => $this->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                foreach($directorates as $directorate){
                    array_push($dept, $directorate->id);
                    
                    $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                    foreach($divisions as $division){
                        array_push($dept, $division->id);
                    }
                }
                
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }
                
                $query->andWhere(['in', 'p.unit_structure_id', $dept]);
            }
        }
        
        $query->andWhere(['o.year' => $this->year]);
        
        $query->andWhere(['not in', 'i.status_id', [Status::REJECTED, Status::DELETED]]);
        // $bsc = Bsc::find()->where(['company_id'=>Yii::$app->user->identity->company_id, 'year'=>$this->year])->one();
        // $this->lock = $bsc->lock;

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        if($this->financing_type_id){
            if($this->financing_type_id == 999999){
                $query->andWhere('b.financing_type_id IS NULL');
            } else {
                $query->andFilterWhere(['b.financing_type_id' => $this->financing_type_id]);
            }
        }

        $query->andFilterWhere([
            'o.position_structure_id' => $this->position_structure_id,
            'u.id' => $this->unit_id,
            'b.volume' => $this->volume,
            'b.unit_amount' => $this->unit_amount,
            'b.frequency' => $this->frequency,
            'b.total_amount' => $this->total_amount,
            'b.year' => $this->year,
            'b.month_01' => $this->month_01,
            'b.month_02' => $this->month_02,
            'b.month_03' => $this->month_03,
            'b.month_04' => $this->month_04,
            'b.month_05' => $this->month_05,
            'b.month_06' => $this->month_06,
            'b.month_07' => $this->month_07,
            'b.month_08' => $this->month_08,
            'b.month_09' => $this->month_09,
            'b.month_10' => $this->month_10,
            'b.month_11' => $this->month_11,
            'b.month_12' => $this->month_12,
            'b.status_id' => $this->budget_status,
            'b.source_of_funds_id' => $this->source_of_funds_id,
            'b.cost_type_id' => $this->cost_type_id,
            'b.cost_priority_id' => $this->cost_priority_id,
            'b.account_id' => $this->account_id,
        ]);

        $query->andFilterWhere(['ilike', 'b.description', $this->description])
            ->andFilterWhere(['ilike', 'b.measure', $this->budget_measure])
            ->andFilterWhere(['ilike', 'b.budget_code', $this->budget_code])
            ->andFilterWhere(['ilike', 'i.workplan_name', $this->workplan_name]);

        $query->orderBy(['u.unit_code'=>SORT_ASC,'i.initiatives_code'=>SORT_ASC,'b.id'=>SORT_ASC]);

        // var_dump($query->createCommand()->getRawSql());
        
        return $dataProvider;
    }

    

    public function getAccountDafNo($account_id)
    {
        $account = \app\models\Account::findOne($account_id);
        if($account)
            $result=$account->daf_account_no;
        else
            $result='';

        return $result;
    }

    public function getAccountName($account_id)
    {
        $account = \app\models\Account::findOne($account_id);
        if($account)
            $result=$account->account_name;
        else
            $result='';

        return $result;
    }

    protected function getPeriodRange($year, $period){

        $dateRange = [];

        if($period == 'Kuartal 1'){
            $dateStart = $year . '-' . '01-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '04-' . date("t", strtotime($year . '-04-01'))));
        } else if($period == 'Kuartal 2'){
            $dateStart = $year . '-' . '04-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '06-' . date("t", strtotime($year . '-06-01'))));
        } else if($period == 'Kuartal 3'){
            $dateStart = $year . '-' . '07-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '09-' . date("t", strtotime($year . '-09-01'))));
        } else if($period == 'Kuartal 4'){
            $dateStart = $year . '-' . '10-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '12-' . date("t", strtotime($year . '-12-01'))));
        }

        array_push($dateRange, $dateStart);
        array_push($dateRange, $dateEnd);

        return $dateRange;
    }
}
