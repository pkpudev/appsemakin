<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StakeholderSupport;

/**
 * StakeholderSupportSearch represents the model behind the search form about `app\models\StakeholderSupport`.
 */
class StakeholderSupportSearch extends StakeholderSupport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'support_point'], 'integer'],
            [['support_grade', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StakeholderSupport::find()
                    ->alias('ss')
                    ->joinWith('createdBy0 cb')
                    ->joinWith('updatedBy0 ub');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ss.id' => $this->id,
            'ss.support_point' => $this->support_point,
            'ss.created_at' => $this->created_at,
            'ss.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'ss.support_grade', $this->support_grade])
                ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
                ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by]);

        return $dataProvider;
    }
}
