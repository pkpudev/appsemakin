<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Initiatives;
use app\models\Bsc;
use app\models\Okr;
use app\models\VwDepartments;

/**
 * InitiativesSearch represents the model behind the search form about `app\models\Initiatives`.
 */
class InitiativesSearch extends Initiatives
{
    public $kpi;
    public $kode_kpi;
    public $departments_id;
    public $i_budget_code;
    public $lock;
    // activity model
    public $activity_name;
    //untuk model budget
    public $strategic_initiatives_id;
    public $budget_id;
    public $description;
    public $volume;
    public $budget_measure;
    public $frequency;
    public $unit_amount;
    public $total_amount;
    public $year;
    public $month_01;
    public $month_02;
    public $month_03;
    public $month_04;
    public $month_05;
    public $month_06;
    public $month_07;
    public $month_08;
    public $month_09;
    public $month_10;
    public $month_11;
    public $month_12;
    public $budget_status;
    public $source_of_funds_id;
    public $cost_type_id;
    public $cost_priority_id;
    public $account_id;

    private $_initiativeSource = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'strategic_initiatives_id', 'budget_id', 'target', 'validity_id', 'controllability_id', 'status_id', 'pic_id', 'volume', 'budget_status', 'grade', 'year', 'frequency', 'source_of_funds_id', 'cost_type_id', 'cost_priority_id', 'account_id'], 'integer'],
            [['workplan_name', 'implementation', 'initiatives_code', 'initiatives', 'measure', 'monitoring_tools', 'note', 'budget_code', 'metadata', 'kpi', 'kode_kpi', 'departments_id', 'description', 'budget_measure', 'has_budget', 'i_budget_code', 'year', 'activity_name'], 'safe'],
            [['total_budget', 'unit_amount', 'total_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchForApproval($params)
    {
        $query = self::find()->alias('i')
            ->joinWith(['kpiDistribution kd']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['ilike', 'kd.kpi_distribution_code', $this->kode_kpi])
            ->andFilterWhere(['kd.departments_id' => $this->departments_id]);

        $query->andFilterWhere([
                'i.id' => $this->id,
                'i.kpi_distribution_id' => $this->kpi_distribution_id,
                'i.total_budget' => $this->total_budget,
                'i.target' => $this->target,
                'i.grade' => $this->grade,
                'i.validity_id' => $this->validity_id,
                'i.controllability_id' => $this->controllability_id,
                'i.status_id' => \app\models\Status::SEND,
                'i.pic_id' => $this->pic_id,
            ]);

        $query->andFilterWhere(['ilike', 'i.workplan_name', $this->workplan_name])
            ->andFilterWhere(['ilike', 'i.implementation', $this->implementation])
            ->andFilterWhere(['ilike', 'i.initiatives_code', $this->initiatives_code])
            ->andFilterWhere(['ilike', 'i.initiatives', $this->initiatives])
            ->andFilterWhere(['ilike', 'i.measure', $this->measure])
            ->andFilterWhere(['ilike', 'i.monitoring_tools', $this->monitoring_tools])
            ->andFilterWhere(['ilike', 'i.pic_text', $this->pic_text])
            ->andFilterWhere(['ilike', 'i.note', $this->note])
            ->andFilterWhere(['ilike', 'i.budget_code', $this->budget_code])
            ->andFilterWhere(['ilike', 'i.metadata', $this->metadata]);

        $query->joinWith(['kpiDistribution.kpi k'])
            ->andFilterWhere(['k.status_id' => \app\models\Status::APPROVED])
            ->andFilterWhere(['ilike', 'k.description', $this->kpi]);

        $userID = Yii::$app->user->id;
        if(!Yii::$app->user->can('SuperUser')) {
            //untuk sementara BSC Admin bisa muncul 1 company
	        if(Yii::$app->user->can('BSC Admin')){
	        	$query->joinWith(['kpiDistribution.vwDepartment d'])
	                ->andFilterWhere(['d.company_id' => Yii::$app->user->identity->company_id]);
	        }
            elseif(Yii::$app->user->can('BOD')){
                $directorate = \app\models\VwEmployeeManager::find()->where(['employee_id'=>$userID])->one();
                $query->joinWith(['kpiDistribution.vwDepartment d'])
                    ->andFilterWhere(['d.directorate_id' => $directorate->directorate_id])
                    ->andFilterWhere(['<>', 'kd.departments_id', $directorate->division_id]);
            }
            else{ //CEO
                $directorate = \app\models\VwEmployeeManager::find()->where(['employee_id'=>$userID])->one();
                $query->joinWith(['kpiDistribution.vwDepartment d'])
                    ->andFilterWhere(['d.supdeptid' => $directorate->division_id]);
            }
        }
        
        $query->orderBy('kd.departments_id');

        return $dataProvider;
    }

    public function searchForApproval2($params)
    {
        $query = self::find()->alias('i')
            ->joinWith(['okr o', 'position p']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $userID = Yii::$app->user->id;

        if(!isset($this->year) or empty($this->year)) {
            $lastOkr = Okr::find()->where("year is not null")->orderBy('year desc')->one();
            $this->year = $lastOkr->year ?: date('Y');
        }
        
        $query->andWhere(['o.year' => $this->year]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['ilike', 'o.okr_code', $this->kode_kpi])
            ->andFilterWhere(['p.unit_structure_id' => $this->departments_id]);

        $query->andFilterWhere([
                'i.id' => $this->id,
                'i.total_budget' => $this->total_budget,
                'i.target' => $this->target,
                'i.grade' => $this->grade,
                'i.validity_id' => $this->validity_id,
                'i.controllability_id' => $this->controllability_id,
                'i.status_id' => \app\models\Status::SEND,
                'i.pic_id' => $this->pic_id,
            ]);

        $query->andFilterWhere(['ilike', 'i.workplan_name', $this->workplan_name])
            ->andFilterWhere(['ilike', 'i.implementation', $this->implementation])
            ->andFilterWhere(['ilike', 'i.initiatives_code', $this->initiatives_code])
            ->andFilterWhere(['ilike', 'i.initiatives', $this->initiatives])
            ->andFilterWhere(['ilike', 'i.measure', $this->measure])
            ->andFilterWhere(['ilike', 'i.monitoring_tools', $this->monitoring_tools])
            ->andFilterWhere(['ilike', 'i.note', $this->note])
            // ->andFilterWhere(['ilike', 'i.budget_code', $this->budget_code])
            ->andFilterWhere(['ilike', 'i.metadata', $this->metadata]);

        // $query->joinWith(['kpiDistribution.kpi k'])
        //     ->andFilterWhere(['k.status_id' => \app\models\Status::APPROVED])
        //     ->andFilterWhere(['ilike', 'k.description', $this->kpi]);

        /* if(!Yii::$app->user->can('SuperUser')) {
            //untuk sementara BSC Admin bisa muncul 1 company
            if(Yii::$app->user->can('BSC Admin')){
                $query->andFilterWhere(['d.company_id' => Yii::$app->user->identity->company_id]);
            }
            elseif(Yii::$app->user->can('BOD')){
                $directorate = \app\models\VwEmployeeManager::find()->where(['employee_id'=>$userID])->one();
                $query->andFilterWhere(['d.directorate_id' => $directorate->directorate_id])
                    ->andFilterWhere(['<>', 'kd.departments_id', $directorate->division_id]);
            }
            else{ //CEO
                $directorate = \app\models\VwEmployeeManager::find()->where(['employee_id'=>$userID])->one();
                $query->andFilterWhere(['d.supdeptid' => $directorate->division_id]);
            }
        } */
        
        // $query->orderBy(['d.department_code'=>SORT_ASC,'i.initiatives_code'=>SORT_ASC]);

        return $dataProvider;
    }

    public function searchForBudget($params)
    {
        $query = self::find()->alias('i')
            ->joinWith(['activities a','activities.budgets b','kpiDistribution kd','kpiDistribution.vwDepartment d'])
            ->select("i.workplan_name, i.initiatives_code, i.kpi_distribution_id, a.activity_name, i.budget_code as i_budget_code, b.id as budget_id, b.measure as budget_measure, b.status_id as budget_status, b.frequency, b.*, kd.departments_id, i.participatory_budgeting_id");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort'=> ['defaultOrder' => ['initiatives_code'=>SORT_ASC]],
        ]);

        $this->load($params);
        // var_dump($params['InitiativeSearch']['year']);

        $this->year = $params['InitiativeSearch']['year'];

		if(!Yii::$app->user->can('RKAT Maker') || Yii::$app->user->can('BSC Admin')){
        $this->departments_id = $params['InitiativeSearch']['departments_id'];
        }

        $this->workplan_name = $params['InitiativeSearch']['workplan_name'];
        $this->activity_name = $params['InitiativeSearch']['activity_name'];
        $this->account_id = $params['InitiativeSearch']['account_id'];
        $this->volume = $params['InitiativeSearch']['volume'];
        $this->unit_amount = $params['InitiativeSearch']['unit_amount'];
        $this->total_amount = $params['InitiativeSearch']['total_amount'];
        $this->source_of_funds_id = $params['InitiativeSearch']['source_of_funds_id'];
        $this->cost_type_id = $params['InitiativeSearch']['cost_type_id'];
        $this->cost_priority_id = $params['InitiativeSearch']['cost_priority_id'];
        // var_dump($this->year);
        // var_dump($this->departments_id);

        $userID = Yii::$app->user->id;
        $companyID = Yii::$app->user->identity->company_id;
        if(!isset($this->year) or empty($this->year)) {
            $this->year = Bsc::getCurrentYear();
        }
        
        if(!Yii::$app->user->can('SuperUser')) {
            if(Yii::$app->user->can('RKAT Maker') and !Yii::$app->user->can('BOD') and !Yii::$app->user->can('BSC Admin')){
                $creator = \app\models\Creator::find()->where(['or', ['employee_id'=>$userID, 'year'=>$this->year], ['admin_id'=>$userID, 'year'=>$this->year]])->all();
                $dept = [];
                foreach ($creator as $row) {
                    array_push($dept, $row->departments_id);
                }
                // $this->departments_id = $creator->departments_id;
                $query->andWhere(['in', 'kd.departments_id', $dept]);
            }else{
                $query->andFilterWhere(['d.company_id' => $companyID]);
            }
        }

        $query->joinWith(['kpiDistribution.kpi.objective.bsc bsc']);
        $query->andWhere(['bsc.year' => $this->year]);
        $bsc = Bsc::find()->where(['company_id'=>Yii::$app->user->identity->company_id, 'year'=>$this->year])->one();
        $this->lock = $bsc->lock;

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'kd.departments_id' => $this->departments_id,
            'b.volume' => $this->volume,
            'b.unit_amount' => $this->unit_amount,
            'b.frequency' => $this->frequency,
            'b.total_amount' => $this->total_amount,
            'b.year' => $this->year,
            'b.month_01' => $this->month_01,
            'b.month_02' => $this->month_02,
            'b.month_03' => $this->month_03,
            'b.month_04' => $this->month_04,
            'b.month_05' => $this->month_05,
            'b.month_06' => $this->month_06,
            'b.month_07' => $this->month_07,
            'b.month_08' => $this->month_08,
            'b.month_09' => $this->month_09,
            'b.month_10' => $this->month_10,
            'b.month_11' => $this->month_11,
            'b.month_12' => $this->month_12,
            'b.status_id' => $this->budget_status,
            'b.source_of_funds_id' => $this->source_of_funds_id,
            'b.cost_type_id' => $this->cost_type_id,
            'b.cost_priority_id' => $this->cost_priority_id,
            'b.account_id' => $this->account_id,
            // 'i.has_budget' => 1,
        ]);

        $query->andFilterWhere(['ilike', 'b.description', $this->description])
            ->andFilterWhere(['ilike', 'b.measure', $this->budget_measure])
            // ->andFilterWhere(['ilike', 'b.source', $this->source])
            // ->andFilterWhere(['ilike', 'b.beneficiary', $this->beneficiary])
            ->andFilterWhere(['ilike', 'i.budget_code', $this->i_budget_code])
            ->andFilterWhere(['ilike', 'a.activity_name', $this->activity_name])
            ->andFilterWhere(['ilike', 'i.workplan_name', $this->workplan_name]);

        $query->orderBy(['d.department_code'=>SORT_ASC,'i.initiatives_code'=>SORT_ASC,'b.id'=>SORT_ASC]);

        return $dataProvider;
    }

    public function searchForDistribution($params)
    {
        $query = self::find()->alias('i')
            ->joinWith(['kpiDistribution kd','kpiDistribution.vwDepartment d'])
            ->select("i.workplan_name, i.initiatives_code, i.kpi_distribution_id, i.budget_code as i_budget_code, i.total_budget, kd.departments_id");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort'=> ['defaultOrder' => ['initiatives_code'=>SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'i.has_budget' => 1,
        ]);

        $query->andFilterWhere(['ilike', 'i.budget_code', $this->i_budget_code])
            ->andFilterWhere(['ilike', 'i.workplan_name', $this->workplan_name]);

        $userID = Yii::$app->user->id;
        $companyID = Yii::$app->user->identity->company_id;
        $query->joinWith(['kpiDistribution kd']);
        if(!Yii::$app->user->can('SuperUser')) {
            if(Yii::$app->user->can('RKAT Maker') and !Yii::$app->user->can('BOD')){
                $creator = \app\models\Creator::find()->where(['employee_id'=>$userID])->one();
                $query->andFilterWhere(['kd.departments_id' => $creator->departments_id]);
            }else{
                $query->andFilterWhere(['d.company_id' => $companyID, 'kd.departments_id'=>$this->departments_id]);
            }
        }
        $query->orderBy(['d.department_code'=>SORT_ASC,'i.initiatives_code'=>SORT_ASC]);

        return $dataProvider;
    }

    public function getBudget()
    {
        return $this->hasOne(\app\models\Budget::className(), ['id' => 'budget_id']);
    }

    public function getBudgetStatus()
    {
        return $this->hasOne(\app\models\Status::className(), ['id' => 'budget_status']);
    }

    /* public function getKpiDistribution()
    {
        return $this->hasOne(\app\models\KpiDistribution::className(), ['id' => 'kpi_distribution_id']);
    } */

    private function getInitiativeSource($strategicInitiativeId)
    {
        if (!isset($this->_initiativeSource[$strategicInitiativeId])) {
            $this->_initiativeSource[$strategicInitiativeId] = \app\models\InitiativesSource::find()->where(['strategic_initiatives_id'=>$strategicInitiativeId])->all();
        }
        return $this->_initiativeSource[$strategicInitiativeId];
    }

    public function getDirectorate()
    {
        $model = VwDepartments::find()->where([
            'deptid' => $this->departments_id,
            'year' => $this->year,
        ])->one();
        return @$model->directorate->deptname;
    }

    public function getFundSource()
    {
        $source = $this->getInitiativeSource($this->strategic_initiatives_id);
        $list = null;
        $separator=', ';
        foreach ($source as $row) {
            if($list)
                $list .= $separator;
            $list .= $row->sourceOfFunds->source_of_funds;
        }

        return $list;
    }

    public function getAsnaf()
    {
        $source = $this->getInitiativeSource($this->strategic_initiatives_id);
        $list = null;
        $separator=', ';
        foreach ($source as $row) {
            if($list)
                $list .= $separator;
            $list .= $row->getAsnafList($row->id);
        }

        return $list;
    }

    public function getSourceAsnafList($strategicInitiativeId)
    {
        $source = $this->getInitiativeSource($strategicInitiativeId);
        $count = count($source);
        
        $list = $count ? '<ul>' : null;

        foreach ($source as $row) {
            $list .= '<li><b>'.$row->sourceOfFunds->source_of_funds.'</b><br />('.$row->getAsnafList($row->id).')</li>';
        }

        if ($count) {
            $list .= '</ul>';
        }

        return $list;
    }

    public function getSourceofFundsAmount($initiatives_id, $source_id)
    {
        $source = \app\models\InitiativesSource::find()->where(['strategic_initiatives_id'=>$initiatives_id, 'source_of_funds_id'=>$source_id])->one();
        if($source)
            $amount=$source->amount;
        else
            $amount=0;

        return $amount;
    }
}
