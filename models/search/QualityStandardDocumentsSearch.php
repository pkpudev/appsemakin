<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QualityStandardDocuments;

/**
 * QualityStandardDocumentsSearch represents the model behind the search form about `app\models\QualityStandardDocuments`.
 */
class QualityStandardDocumentsSearch extends QualityStandardDocuments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'period_start', 'period_end', 'level', 'document_type_id', 'created_by', 'updated_by', 'files_id', 'unit_structure_id'], 'integer'],
            [['document_no', 'document_date', 'title', 'version', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QualityStandardDocuments::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['level' => SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $qd = QualityStandardDocuments::find();
        if($this->period_start && $this->period_end){
            $query->andWhere('period_start >= ' . $this->period_start . ' and period_end <= ' . $this->period_end);
        } else {
            $lastQd = QualityStandardDocuments::find()->orderBy('id desc')->one();
            $filterYearFrom = $lastQd->period_start;
            $filterYearTo = $lastQd->period_end;

            $query->andWhere(['period_start' => $filterYearFrom]);
            $query->andWhere(['period_end' => $filterYearTo]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            // 'period_start' => $this->period_start,
            // 'period_end' => $this->period_end,
            'level' => $this->level,
            'document_type_id' => $this->document_type_id,
            'document_date' => $this->document_date,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'files_id' => $this->files_id,
            'unit_structure_id' => $this->unit_structure_id,
        ]);

        $query->andFilterWhere(['ilike', 'document_no', $this->document_no])
            ->andFilterWhere(['ilike', 'title', $this->title])
            ->andFilterWhere(['ilike', 'version', $this->version]);

        return $dataProvider;
    }
}
