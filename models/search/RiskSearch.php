<?php

namespace app\models\search;

use app\models\Okr;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Risk;
use app\models\RiskMitigation;

/**
 * RiskSearch represents the model behind the search form about `app\models\Risk`.
 */
class RiskSearch extends Risk
{

    public $year; 
    public $unit_id; 
    public $initiatives_code; 
    public $initiatives_name; 
    public $recomended_action; 
    public $due_date; 

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'initiatives_id', 'risk_category_id', 'created_by', 'updated_by', 'unit_id'], 'integer'],
            [['requirements', 'potential_failure', 'potential_effect_of_failure', 'potential_causes_of_failure', 
            'current_process_controls_prevention', 'current_process_controls_detection', 'risk_criteria', 'created_at', 
            'updated_at', 'year', 'initiatives_code', 'initiatives_name', 'recomended_action', 'due_date'], 'safe'],
            [['severity', 'occurance', 'detection', 'rpn'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Risk::find()
                            ->alias('r')
                            ->joinWith(['initiatives i', 'initiatives.okr o', 'initiatives.okr.position ps']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['okr_code' => SORT_ASC, 'initiatives_code' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['initiatives_code'] = [
            'asc' => ['i.initiatives_code' => SORT_ASC],
            'desc' => ['i.initiatives_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['okr_code'] = [
            'asc' => ['o.okr_code' => SORT_ASC],
            'desc' => ['o.okr_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!isset($this->year) or empty($this->year)) {
            $lastOkr = Okr::find()->where("year is not null")->orderBy('year desc')->one();
            $this->year = $lastOkr->year ?: date('Y');
        }

        $query->andFilterWhere([
            'r.id' => $this->id,
            'r.initiatives_id' => $this->initiatives_id,
            'r.severity' => $this->severity,
            'r.occurance' => $this->occurance,
            'r.detection' => $this->detection,
            'r.rpn' => $this->rpn,
            'r.risk_category_id' => $this->risk_category_id,
            'r.created_at' => $this->created_at,
            'r.created_by' => $this->created_by,
            'r.updated_at' => $this->updated_at,
            'r.updated_by' => $this->updated_by,
            'r.updated_by' => $this->updated_by,
            'o.year' => $this->year,
            'ps.unit_structure_id' => $this->unit_id,
        ]);

        $query->andFilterWhere(['like', 'requirements', $this->requirements])
            ->andFilterWhere(['like', 'potential_failure', $this->potential_failure])
            ->andFilterWhere(['like', 'potential_effect_of_failure', $this->potential_effect_of_failure])
            ->andFilterWhere(['like', 'potential_causes_of_failure', $this->potential_causes_of_failure])
            ->andFilterWhere(['like', 'current_process_controls_prevention', $this->current_process_controls_prevention])
            ->andFilterWhere(['like', 'current_process_controls_detection', $this->current_process_controls_detection])
            ->andFilterWhere(['ilike', 'initiatives_code', $this->initiatives_code])
            ->andFilterWhere(['ilike', 'workplan_name', $this->initiatives_name])
            ->andFilterWhere(['like', 'risk_criteria', $this->risk_criteria]);

        return $dataProvider;
    }

    
    public function searchRisk($params)
    {
        $query = RiskMitigation::find()
                            ->alias('rm')
                            ->joinWith(['risk r', 'risk.initiatives i', 'risk.initiatives.okr o', 'risk.initiatives.okr.position ps']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['okr_code' => SORT_ASC, 'initiatives_code' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['initiatives_code'] = [
            'asc' => ['i.initiatives_code' => SORT_ASC],
            'desc' => ['i.initiatives_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['okr_code'] = [
            'asc' => ['o.okr_code' => SORT_ASC],
            'desc' => ['o.okr_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!Yii::$app->user->can('SuperUser')) {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin') and !Yii::$app->user->can('Risk Management Officer')){

                $userID = Yii::$app->user->id;
                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }
                
                $query->andWhere(['in', 'ps.unit_structure_id', $dept]);
            }
        }

        if(!isset($this->year) or empty($this->year)) {
            $lastOkr = Okr::find()->where("year is not null")->orderBy('year desc')->one();
            $this->year = $lastOkr->year ?: date('Y');
        }

        $query->andFilterWhere([
            'r.id' => $this->id,
            'r.initiatives_id' => $this->initiatives_id,
            'r.severity' => $this->severity,
            'r.occurance' => $this->occurance,
            'r.detection' => $this->detection,
            'r.rpn' => $this->rpn,
            'r.risk_category_id' => $this->risk_category_id,
            'r.created_at' => $this->created_at,
            'r.created_by' => $this->created_by,
            'r.updated_at' => $this->updated_at,
            'r.updated_by' => $this->updated_by,
            'r.updated_by' => $this->updated_by,
            'o.year' => $this->year,
            'ps.unit_structure_id' => $this->unit_id,
            'rm.due_date' => $this->due_date,
        ]);

        $query->andFilterWhere(['like', 'requirements', $this->requirements])
            ->andFilterWhere(['like', 'potential_failure', $this->potential_failure])
            ->andFilterWhere(['like', 'potential_effect_of_failure', $this->potential_effect_of_failure])
            ->andFilterWhere(['like', 'potential_causes_of_failure', $this->potential_causes_of_failure])
            ->andFilterWhere(['like', 'current_process_controls_prevention', $this->current_process_controls_prevention])
            ->andFilterWhere(['like', 'current_process_controls_detection', $this->current_process_controls_detection])
            ->andFilterWhere(['ilike', 'initiatives_code', $this->initiatives_code])
            ->andFilterWhere(['ilike', 'workplan_name', $this->initiatives_name])
            ->andFilterWhere(['ilike', 'recomended_action', $this->recomended_action])
            ->andFilterWhere(['like', 'risk_criteria', $this->risk_criteria]);

        // var_dump($query->createCommand()->getRawSql());

        return $dataProvider;
    }
}
