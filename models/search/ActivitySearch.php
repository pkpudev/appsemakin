<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Activity;
use app\models\Bsc;
use app\models\Okr;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\Status;

/**
 * ActivitySearch represents the model behind the search form about `app\models\Activity`.
 */
class ActivitySearch extends Activity
{
    /**
     * @inheritdoc
     */
    public $lock;
    public $year;
    public $unit_id;
    public $unit_name;
    public $unit_code;
    // untuk initiative
    public $i_code;
    public $i_status_id;
    public $workplan_name;
    //untuk model budget
    public $budget_id;
    public $description;
    public $volume;
    public $budget_measure;
    public $frequency;
    public $unit_amount;
    public $total_amount;
    public $month_01;
    public $month_02;
    public $month_03;
    public $month_04;
    public $month_05;
    public $month_06;
    public $month_07;
    public $month_08;
    public $month_09;
    public $month_10;
    public $month_11;
    public $month_12;
    public $budget_status;
    public $budget_code;
    public $source_of_funds_id;
    public $cost_type_id;
    public $cost_priority_id;
    public $account_id;
    public $position_structure_id;
    public $financing_type_id;
    public $freq_measure;
    public $period;

    public function rules()
    {
        return [
            [['id', 'initiatives_id', 'duration', 'pic_id', 'status_id', 'position_structure_id', 'year', 'volume', 'i_status_id', 'budget_status', 'year', 'frequency', 'source_of_funds_id', 'cost_type_id', 'cost_priority_id', 'account_id', 'financing_type_id'], 'integer'],
            [['period', 'activity_name', 'start_plan', 'finish_plan', 'start_actual', 'finish_actual', 'documentation', 'unit_id', 'i_budget_code', 'budget_code', 'workplan_name', 'unit_name', 'unit_code', 'freq_measure'], 'safe'],
            [['unit_amount', 'total_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Activity::find()
            ->alias('a')
            ->joinWith(['initiative i', 'initiative.okr o', 'initiative.position p', 'initiative.position.unit u']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['activity_code'=>SORT_ASC]],
        ]);

        $this->load($params);

        $userID = Yii::$app->user->id;
        $companyID = Yii::$app->user->identity->company_id;
        if(!isset($this->year) or empty($this->year)) {
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $this->year = $lastOkr->year ?: date('Y');
        }

        if(!Yii::$app->user->can('SuperUser')) {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin')){

                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }
                
                $query->andWhere(['in', 'p.unit_structure_id', $dept]);
            }
        }

        $query->andWhere(['o.year' => $this->year]);
        $query->andWhere(['not in', 'i.status_id', [Status::REJECTED, Status::DELETED]]);

        if($this->period){
            $periodStart = $this->getPeriodRange($this->year, $this->period)[0];
            $periodEnd = $this->getPeriodRange($this->year, $this->period)[1];

            
            // $query->andWhere("a.start_plan >= '{$periodStart}'");
            // $query->andWhere("a.start_plan < '{$periodEnd}'");

            $query->andWhere("(
                (cast('{$periodStart}' as timestamp) >= start_plan AND cast('{$periodStart}' as timestamp) <= finish_plan) OR 
                (cast('{$periodEnd}' as timestamp) >= start_plan AND cast('{$periodEnd}' as timestamp) <= finish_plan) OR
                (start_plan >= '{$periodStart}' AND start_plan <= '{$periodEnd}') OR 
                (finish_plan >= '{$periodStart}' AND finish_plan <= '{$periodEnd}') OR 
                (cast('{$periodStart}' as timestamp) <=start_plan AND cast('{$periodEnd}' as timestamp) >= finish_plan)
            )");

        }
        
        // $bsc = Bsc::find()->where(['company_id'=>Yii::$app->user->identity->company_id, 'year'=>$this->year])->one();
        // $this->lock = $bsc->lock;

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'initiatives_id' => $this->initiatives_id,
            'start_plan' => $this->start_plan,
            'finish_plan' => $this->finish_plan,
            'duration' => $this->duration,
            'start_actual' => $this->start_actual,
            'finish_actual' => $this->finish_actual,
            'a.pic_id' => $this->pic_id,
            'i.position_structure_id' => $this->position_structure_id,
            'status_id' => $this->status_id,
            'o.year' => $this->year,
            'p.unit_structure_id' => $this->unit_id,
        ]);

        $query->andFilterWhere(['ilike', 'activity_name', $this->activity_name]);
        $query->andFilterWhere(['ilike', 'documentation', $this->documentation]);

        $query->orderBy(['u.unit_code'=>SORT_ASC,'i.initiatives_code'=>SORT_ASC]);
        
        // var_dump($query->createCommand()->getRawSql());
        return $dataProvider;
    }

    public function searchForBudget($params)
    {
        $query = self::find()->alias('a')
            ->joinWith(['initiative i', 'budgets b', 'initiative.okr o', 'initiative.okr.position p', 'initiative.okr.position.unit u'])
            ->select("u.unit_code, u.unit_name, i.workplan_name, i.initiatives_code, i.status_id as i_status_id, a.activity_name, b.id as budget_id, b.measure as budget_measure, b.status_id as budget_status, b.frequency, b.*, o.position_structure_id");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['i_code' => SORT_ASC]]
        ]);


        $dataProvider->sort->attributes['i_code'] = [
            'asc' => ['i.initiatives_code' => SORT_ASC],
            'desc' => ['i.initiatives_code' => SORT_DESC],
        ];

        $this->load($params);

        $userID = Yii::$app->user->id;
        $companyID = Yii::$app->user->identity->company_id;
        if(!isset($this->year) or empty($this->year)) {
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $this->year = $lastOkr->year ?: date('Y');
        }
        
        if(!Yii::$app->user->can('SuperUser')) {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin') && !Yii::$app->user->can('Keuangan')){

                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }
                
                $query->andWhere(['in', 'p.unit_structure_id', $dept]);
            }
        }
        
        $query->andWhere(['o.year' => $this->year]);
        $query->andWhere(['not in', 'i.status_id', [Status::REJECTED, Status::DELETED]]);
        // $bsc = Bsc::find()->where(['company_id'=>Yii::$app->user->identity->company_id, 'year'=>$this->year])->one();
        // $this->lock = $bsc->lock;

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->financing_type_id){
            if($this->financing_type_id == 999999){
                $query->andWhere('b.financing_type_id IS NULL');
            } else {
                $query->andFilterWhere(['b.financing_type_id' => $this->financing_type_id]);
            }
        }

        $query->andFilterWhere([
            'u.id' => $this->unit_id,
            'b.volume' => $this->volume,
            'b.unit_amount' => $this->unit_amount,
            'b.frequency' => $this->frequency,
            'b.total_amount' => $this->total_amount,
            'b.year' => $this->year,
            'b.month_01' => $this->month_01,
            'b.month_02' => $this->month_02,
            'b.month_03' => $this->month_03,
            'b.month_04' => $this->month_04,
            'b.month_05' => $this->month_05,
            'b.month_06' => $this->month_06,
            'b.month_07' => $this->month_07,
            'b.month_08' => $this->month_08,
            'b.month_09' => $this->month_09,
            'b.month_10' => $this->month_10,
            'b.month_11' => $this->month_11,
            'b.month_12' => $this->month_12,
            'b.status_id' => $this->budget_status,
            'b.source_of_funds_id' => $this->source_of_funds_id,
            'b.cost_type_id' => $this->cost_type_id,
            'b.cost_priority_id' => $this->cost_priority_id,
            'b.account_id' => $this->account_id,
        ]);

        $query->andFilterWhere(['ilike', 'b.description', $this->description])
            ->andFilterWhere(['ilike', 'b.measure', $this->budget_measure])
            ->andFilterWhere(['ilike', 'b.budget_code', $this->budget_code])
            ->andFilterWhere(['ilike', 'a.activity_name', $this->activity_name])
            ->andFilterWhere(['ilike', 'i.workplan_name', $this->workplan_name]);

        $query->orderBy(['u.unit_code'=>SORT_ASC,'i.initiatives_code'=>SORT_ASC,'a.activity_code'=>SORT_ASC,'b.id'=>SORT_ASC]);
        
        return $dataProvider;
    }

    public function getSourceOfFunds($source_id)
    {
        $source = \app\models\SourceOfFunds::findOne($source_id);
        if($source)
            $result=$source->source_of_funds;
        else
            $result='';

        return $result;
    }

    public function getCostType($cost_type_id)
    {
        $costType = \app\models\CostType::findOne($cost_type_id);
        if($costType)
            $result=$costType->cost_type;
        else
            $result='';

        return $result;
    }

    public function getCostPriority($cost_priority_id)
    {
        $costPriority = \app\models\CostPriority::findOne($cost_priority_id);
        if($costPriority)
            $result=$costPriority->cost_priority;
        else
            $result='';

        return $result;
    }

    public function getFinancingType($financing_type_id)
    {
        $financingType = \app\models\FinancingType::findOne($financing_type_id);
        if($financingType)
            $result=$financingType->financing_type;
        else
            $result='';

        return $result;
    }

    public function getAccount($account_id)
    {
        $account = \app\models\Account::findOne($account_id);
        if($account)
            $result=$account->daf_account_no . ' ' . $account->account_name;
        else
            $result='';

        return $result;
    }

    public function getAccountDafNo($account_id)
    {
        $account = \app\models\Account::findOne($account_id);
        if($account)
            $result=$account->daf_account_no;
        else
            $result='';

        return $result;
    }

    public function getAccountName($account_id)
    {
        $account = \app\models\Account::findOne($account_id);
        if($account)
            $result=$account->account_name;
        else
            $result='';

        return $result;
    }

    protected function getPeriodRange($year, $period){

        $dateRange = [];

        if($period == 'Kuartal 1'){
            $dateStart = $year . '-' . '01-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '04-' . date("t", strtotime($year . '-04-01'))));
        } else if($period == 'Kuartal 2'){
            $dateStart = $year . '-' . '04-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '06-' . date("t", strtotime($year . '-06-01'))));
        } else if($period == 'Kuartal 3'){
            $dateStart = $year . '-' . '07-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '09-' . date("t", strtotime($year . '-09-01'))));
        } else if($period == 'Kuartal 4'){
            $dateStart = $year . '-' . '10-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '12-' . date("t", strtotime($year . '-12-01'))));
        }

        array_push($dateRange, $dateStart);
        array_push($dateRange, $dateEnd);

        return $dateRange;
    }
}
