<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmployeeAchievement as EmployeeAchievementModel;
use app\models\PositionStructure;

/**
 * EmployeeAchievement represents the model behind the search form about `app\models\EmployeeAchievement`.
 */
class EmployeeAchievement extends EmployeeAchievementModel
{
    public $unit_structure_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'employee_id', 'evaluation_id', 'position_structure_id', 'unit_structure_id'], 'integer'],
            [['achieve_type'], 'safe'],
            [['achieve_value', 'contribution_amount', 'fulfillment'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type)
    {
        $query = EmployeeAchievementModel::find()
            ->alias('ea')
            ->joinWith(['positionStructure ps', 'employee e'])
            ->orderBy('ps.unit_structure_id, e.full_name');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if(!Yii::$app->user->can('SuperUser')) {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin')){

                $userID = Yii::$app->user->id;
                $dept = [];
                $employeePositions = PositionStructureEmployeeSearch::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }
                
                $query->andWhere(['in', 'ps.unit_structure_id', $dept]);
            }
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'evaluation_id' => $this->evaluation_id,
            'position_structure_id' => $this->position_structure_id,
            'achieve_value' => $this->achieve_value,
            'contribution_amount' => $this->contribution_amount,
            'fulfillment' => $this->fulfillment,
            'achieve_type' => $type,
            'ps.unit_structure_id' => $this->unit_structure_id,
        ]);

        return $dataProvider;
    }
}
