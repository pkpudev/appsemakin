<?php

namespace app\models\search;

use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;

/**
 * UnitStructureSearch represents the model behind the search form about `app\models\UnitStructure`.
 */
class UnitStructureSearch extends UnitStructure
{

    public $period;
    public $department_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'unit_id', 'created_by', 'updated_by', 'department_id'], 'integer'],
            [['unit_name', 'level', 'unit_code', 'created_at', 'updated_at', 'superior_unit_id', 'initiative_code', 'period'], 'safe'],
            [['is_value_stream'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UnitStructure::find()
                    ->alias('us')
                    ->joinWith('sup s');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['ucode' => SORT_ASC]],
            'pagination' => [
                'pageSize' => Yii::$app->controller->action->id == 'dashboard' ? 100 : 20,
            ],
        ]);

        $dataProvider->sort->attributes['ucode'] = [
            'asc' => ['us.unit_code' => SORT_ASC],
            'desc' => ['us.unit_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!$this->year) $this->year = date('Y');

        if(!Yii::$app->user->can('SuperUser')) {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin') and !Yii::$app->user->can('Admin Direktorat')){

                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }

                if(!Yii::$app->user->can('Leader')) $query->andWhere(['us.year' => date('Y')]);
                
                $query->andWhere(['in', 'us.id', $dept]);
            }  else if(Yii::$app->user->can('Admin Direktorat')){
                $userId = Yii::$app->user->id;
                $dept = [];
    
                $vwEmployeeManager = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $this->year])->one();
    
                $directorates = UnitStructure::find()->where(['year' => $this->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                foreach($directorates as $directorate){
                    array_push($dept, $directorate->id);
                    
                    $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                    foreach($divisions as $division){
                        array_push($dept, $division->id);
                    }
                }

                $query->andWhere(['in', 'us.id', $dept]);
            } 
        }

        if(Yii::$app->controller->id == 'dashboard' && Yii::$app->controller->action->id == 'target'){
            $query->andWhere("
                                EXISTS (
                                        SELECT 
                                            1 
                                        FROM management.target 
                                        where 
                                            unit_structure_id = us.id AND
                                            year = {$this->year} AND
                                            total_target > 0
                                        )
                            ");
                            

            if($this->year > 2022){
                $prevYear = $this->year - 1;
                $query->andWhere("
                    EXISTS (
                        SELECT 
                            1 
                        FROM management.target t
                        left join management.unit_structure us2 on t.unit_structure_id = us2.id 
                        where 
                            us2.unit_id  = us.unit_id and 
                            t.year = {$prevYear} AND
                            total_target > 0
                        )
                ");
            }
        } else if (Yii::$app->controller->id == 'dashboard' && Yii::$app->controller->action->id == 'unit2'){
            $query->andWhere("us.unit_id <> 329");
            $query->andWhere("us.level != '1'");
        } else if (Yii::$app->controller->id == 'activities' && Yii::$app->controller->action->id == 'abk'){
            $query->andWhere("us.unit_id <> 329");
            $query->andWhere("us.level not in ('1', '2')");
        }

        if($this->department_id){
            $query->andWhere(['us.superior_unit_id' => $this->department_id]);
        }

        $query->andFilterWhere([
            'us.id' => $this->id,
            'us.year' => $this->year,
            'us.unit_id' => $this->unit_id,
            'us.created_at' => $this->created_at,
            'us.created_by' => $this->created_by,
            'us.updated_at' => $this->updated_at,
            'us.updated_by' => $this->updated_by,
            'us.is_value_stream' => $this->is_value_stream,
        ]);

        if(is_numeric($this->superior_unit_id)){
            $query->andFilterWhere(['s.year' => $this->superior_unit_id]);
        } else {
            $query->andFilterWhere(['ilike', 'us.unit_name', $this->superior_unit_id]);
        }

        $query->andFilterWhere(['ilike', 'us.unit_name', $this->unit_name])
            ->andFilterWhere(['ilike', 'us.level', $this->level])
            ->andFilterWhere(['ilike', 'us.unit_code', $this->unit_code])
            ->andFilterWhere(['ilike', 'us.initiative_code', $this->initiative_code]);

            // var_dump($query->createCommand()->getRawSql());
            
        return $dataProvider;
    }
}
