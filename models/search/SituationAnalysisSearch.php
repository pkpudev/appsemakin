<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SituationAnalysis;

/**
 * SituationAnalysisSearch represents the model behind the search form about `app\models\SituationAnalysis`.
 */
class SituationAnalysisSearch extends SituationAnalysis
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'description', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SituationAnalysis::find()
                    ->alias('sa')
                    ->joinWith('createdBy0 cb')
                    ->joinWith('updatedBy0 ub');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sa.id' => $this->id,
            'sa.created_at' => $this->created_at,
            'sa.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'sa.title', $this->title])
        ->andFilterWhere(['ilike', 'sa.description', $this->description])
        ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
        ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by]);

        return $dataProvider;
    }
}
