<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StakeholderAnalysisResult;

/**
 * StakeholderAnalysisResultSearch represents the model behind the search form about `app\models\StakeholderAnalysisResult`.
 */
class StakeholderAnalysisResultSearch extends StakeholderAnalysisResult
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'min', 'max'], 'integer'],
            [['result_grade', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StakeholderAnalysisResult::find()
                    ->alias('sar')
                    ->joinWith('createdBy0 cb')
                    ->joinWith('updatedBy0 ub');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sar.id' => $this->id,
            'sar.min' => $this->min,
            'sar.max' => $this->max,
            'sar.created_at' => $this->created_at,
            'sar.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'result_grade', $this->result_grade])
                ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
                ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by]);

        return $dataProvider;
    }
}
