<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Employee;
use Yii;

/**
 * EmployeeSearch represents the model behind the search form of `app\models\Employee`.
 */
class EmployeeSearch extends Employee
{

    public $role;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_status', 'phone_id', 'branch_id', 'is_new_task', 'parent_alias_id', 'company_id', 'expired_key', 'deleted_by', 'location_id', 'is_donor', 'country_id', 'fax_id', 'bank_account_id', 'marketer_id', 'donor_type_id', 'is_valid', 'phone_mark', 'mail_mark', 'created_by', 'updated_by', 'is_new', 'has_idcard', 'remainder_date', 'merged_to_id', 'donor_level_id', 'donor_class_id', 'donor_company_id', 'sex', 'is_employee', 'position_id', 'is_marketer', 'employee_type_id', 'marital_status', 'branch_id_financial', 'is_external_orphan_admin', 'workplace_id', 'is_qurban_reguler'], 'integer'],
            [['user_name', 'passwd', 'email', 'full_name', '_history', 'register_date', 'guid', 'activkey', 'auth_key', 'deleted_stamp', 'donor_no', 'address', 'postal_code', 'npwp_no', 'npwz_no', 'text_searchable', 'last_login', 'donor_no_old', 'website', 'updated_validity', 'note', 'alias_name', 'created_stamp', 'updated_stamp', 'remainder_message', 'address_full', 'phone_no_full', 'blood_type', 'data_source', 'birth_place', 'birth_date', 'starting_date', 'ending_date', 'permanent_address', 'current_phone_no', 'permanent_phone_no', 'identity_no', 'user_image', 'nip', 'job', 'private_email', 'religion', 'role'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employee::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->filterRole($query, $this->role);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_status' => $this->user_status,
            'phone_id' => $this->phone_id,
            'register_date' => $this->register_date,
            'branch_id' => $this->branch_id,
            'is_new_task' => $this->is_new_task,
            'parent_alias_id' => $this->parent_alias_id,
            'company_id' => $this->company_id,
            'expired_key' => $this->expired_key,
            'deleted_by' => $this->deleted_by,
            'deleted_stamp' => $this->deleted_stamp,
            'location_id' => $this->location_id,
            'is_donor' => $this->is_donor,
            'country_id' => $this->country_id,
            'fax_id' => $this->fax_id,
            'bank_account_id' => $this->bank_account_id,
            'marketer_id' => $this->marketer_id,
            'donor_type_id' => $this->donor_type_id,
            'last_login' => $this->last_login,
            'is_valid' => $this->is_valid,
            'phone_mark' => $this->phone_mark,
            'mail_mark' => $this->mail_mark,
            'updated_validity' => $this->updated_validity,
            'created_by' => $this->created_by,
            'created_stamp' => $this->created_stamp,
            'updated_by' => $this->updated_by,
            'updated_stamp' => $this->updated_stamp,
            'is_new' => $this->is_new,
            'has_idcard' => $this->has_idcard,
            'remainder_date' => $this->remainder_date,
            'merged_to_id' => $this->merged_to_id,
            'donor_level_id' => $this->donor_level_id,
            'donor_class_id' => $this->donor_class_id,
            'donor_company_id' => $this->donor_company_id,
            'birth_date' => $this->birth_date,
            'sex' => $this->sex,
            'starting_date' => $this->starting_date,
            'ending_date' => $this->ending_date,
            'is_employee' => $this->is_employee,
            'position_id' => $this->position_id,
            'is_marketer' => $this->is_marketer,
            'employee_type_id' => $this->employee_type_id,
            'marital_status' => $this->marital_status,
            'branch_id_financial' => $this->branch_id_financial,
            'is_external_orphan_admin' => $this->is_external_orphan_admin,
            'workplace_id' => $this->workplace_id,
            'is_qurban_reguler' => $this->is_qurban_reguler,
        ]);

        $query->andFilterWhere(['ilike', 'user_name', $this->user_name])
            ->andFilterWhere(['ilike', 'passwd', $this->passwd])
            ->andFilterWhere(['ilike', 'email', $this->email])
            ->andFilterWhere(['ilike', 'full_name', $this->full_name])
            ->andFilterWhere(['ilike', '_history', $this->_history])
            ->andFilterWhere(['ilike', 'guid', $this->guid])
            ->andFilterWhere(['ilike', 'activkey', $this->activkey])
            ->andFilterWhere(['ilike', 'auth_key', $this->auth_key])
            ->andFilterWhere(['ilike', 'donor_no', $this->donor_no])
            ->andFilterWhere(['ilike', 'address', $this->address])
            ->andFilterWhere(['ilike', 'postal_code', $this->postal_code])
            ->andFilterWhere(['ilike', 'npwp_no', $this->npwp_no])
            ->andFilterWhere(['ilike', 'npwz_no', $this->npwz_no])
            ->andFilterWhere(['ilike', 'text_searchable', $this->text_searchable])
            ->andFilterWhere(['ilike', 'donor_no_old', $this->donor_no_old])
            ->andFilterWhere(['ilike', 'website', $this->website])
            ->andFilterWhere(['ilike', 'note', $this->note])
            ->andFilterWhere(['ilike', 'alias_name', $this->alias_name])
            ->andFilterWhere(['ilike', 'remainder_message', $this->remainder_message])
            ->andFilterWhere(['ilike', 'address_full', $this->address_full])
            ->andFilterWhere(['ilike', 'phone_no_full', $this->phone_no_full])
            ->andFilterWhere(['ilike', 'blood_type', $this->blood_type])
            ->andFilterWhere(['ilike', 'data_source', $this->data_source])
            ->andFilterWhere(['ilike', 'birth_place', $this->birth_place])
            ->andFilterWhere(['ilike', 'permanent_address', $this->permanent_address])
            ->andFilterWhere(['ilike', 'current_phone_no', $this->current_phone_no])
            ->andFilterWhere(['ilike', 'permanent_phone_no', $this->permanent_phone_no])
            ->andFilterWhere(['ilike', 'identity_no', $this->identity_no])
            ->andFilterWhere(['ilike', 'user_image', $this->user_image])
            ->andFilterWhere(['ilike', 'nip', $this->nip])
            ->andFilterWhere(['ilike', 'job', $this->job])
            ->andFilterWhere(['ilike', 'private_email', $this->private_email])
            ->andFilterWhere(['ilike', 'religion', $this->religion]);

        return $dataProvider;
    }

    protected function filterRole(&$query, $role)
    {
          if (empty($role)) {
                return;
          }

          $userIds = Yii::$app->getAuthManager()->getUserIdsByRole($role);
          if (empty($userIds)) {
                $query->where('0=1');
          } else {
                $query->andWhere(['in', 'id', $userIds]);
          }
    }
}
