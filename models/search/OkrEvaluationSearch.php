<?php

namespace app\models\search;

use app\models\Evaluation;
use app\models\Okr;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OkrEvaluation;
use app\models\OkrLevel;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;

/**
 * OkrEvaluationSearch represents the model behind the search form about `app\models\OkrEvaluation`.
 */
class OkrEvaluationSearch extends OkrEvaluation
{
    public $year;
    public $unit_id;
    public $unit_code;
    public $unit_name;
    public $parent_okr_code;
    public $parent_okr;
    public $okr_code;
    public $okr;
    public $pic_id;
    public $okr_level_id;
    public $position_name;
    public $employee_name;
    public $position_structure_id;
    public $perspective_id;
    public $portofolio_type;
    public $directorate_id;
    public $department_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['portofolio_type', 'perspective_id', 'id', 'evaluation_id', 'okr_id', 'target', 'year', 'unit_id', 'pic_id', 'status_id', 'okr_level_id', 'directorate_id', 'department_id'], 'integer'],
            [['real', 'okr_code', 'okr', 'parent_okr_code', 'parent_okr', 'unit_code', 'unit_name', 'position_name', 'employee_name', 'unit_structure_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['okr o', 'okr.parent p', 'okr.position op'])
                                ->where(['o.type' => 'KR']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['parent_okr_code' => SORT_ASC, 'okr_code' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['parent_okr_code'] = [
            'asc' => ['p.okr_code' => SORT_ASC],
            'desc' => ['p.okr_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['okr_code'] = [
            'asc' => ['o.okr_code' => SORT_ASC],
            'desc' => ['o.okr_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        
        if(!isset($this->year) or empty($this->year)) {
            $lastEvaluation = Evaluation::find()->orderBy('year desc')->one();
            $this->year = $lastEvaluation->year ?: date('Y');
        }
        
        if(!$this->evaluation_id){
            $currentDate= strtotime(date('Y-m-d'));
                        
            if (($currentDate >= strtotime("{$this->year}-01-01")) && ($currentDate <= strtotime(date("Y-m-t", strtotime( $this->year . '-' . '04-01'))))){
                $period = 'Kuartal 1';
            } else if (($currentDate >= strtotime("{$this->year}-04-01")) && ($currentDate <= strtotime(date("Y-m-t", strtotime( $this->year . '-' . '07-01'))))){
                $period = 'Kuartal 2';
            } else if (($currentDate >= strtotime("{$this->year}-07-01")) && ($currentDate <= strtotime(date("Y-m-t", strtotime( $this->year . '-' . '10-01'))))){
                $period = 'Kuartal 3';
            } else if (($currentDate >= strtotime("{$this->year}-10-01")) && ($currentDate <= strtotime(date("Y-m-t", strtotime( $this->year . '-' . '12-01'))))){
                $period = 'Kuartal 4';
            }

            $evaluation = Evaluation::findOne(['year' =>  date('Y'), 'period' => $period]);
            $this->evaluation_id = $evaluation->id;
        }
        
        if(!Yii::$app->user->can('SuperUser') && Yii::$app->controller->action->id != 'organization') {
            $userID = Yii::$app->user->id;
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin') && !Yii::$app->user->can('Admin Direktorat')) {
                $dept = [];
                $isManager = false;
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 4 || $ep->position->level == 6){
                        $isManager = true;
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }
                
                if($isManager){
                    $query->andWhere('op.unit_structure_id in (' . implode(', ', $dept) . ') or o.pic_id = ' . $userID);
                    // $query->andWhere('o.position_structure_id in (' . implode(', ', $postId) . ') or o.pic_id = ' . $userID);
                } else {
                    $query->andWhere(['o.pic_id' => $userID]);
                }

            } else if(Yii::$app->user->can('Admin Direktorat')){
                $userId = Yii::$app->user->id;
                $dept = [];
    
                $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $this->year])->all();

                foreach($vwEmployeeManagers as $vwEmployeeManager){
                    $directorates = UnitStructure::find()->where(['year' => $this->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                    foreach($directorates as $directorate){
                        array_push($dept, $directorate->id);
                        
                        $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                        foreach($divisions as $division){
                            array_push($dept, $division->id);
                        }
                    }
                }

                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }

                $query->andWhere(['in', 'op.unit_structure_id', $dept]);
            } 
        }

        if($this->directorate_id){
            $unitIds = [];
            $depts = UnitStructure::find()->where(['superior_unit_id' => $this->directorate_id])->all();
            foreach($depts as $dept){
                array_push($unitIds, $dept->id);

                $units = UnitStructure::find()->where(['superior_unit_id' => $dept->id])->all();
                foreach($units as $unit){
                    array_push($unitIds, $unit->id);
                }
            }
            
            $query->andWhere(['in', 'op.unit_structure_id', $unitIds]);
        }

        if($this->department_id){
            $unitIds = [];
            $units = UnitStructure::find()->where(['superior_unit_id' => $this->department_id])->all();
            foreach($units as $unit){
                array_push($unitIds, $unit->id);
            }
            
            $query->andWhere(['in', 'op.unit_structure_id', $unitIds]);
        }

        if(Yii::$app->controller->action->id == 'individual'){
            $query->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL]);
        } else if(Yii::$app->controller->action->id == 'unit'){
            $query->andWhere('o.okr_level_id not in (' . OkrLevel::INDIVIDUAL . ', ' . OkrLevel::ORGANIZATION . ')');
        } else if(Yii::$app->controller->action->id == 'organization' || Yii::$app->controller->action->id == 'organization-admin' || Yii::$app->controller->action->id == 'okr-org2'){
            $query->andWhere(['o.okr_level_id' => OkrLevel::ORGANIZATION]);
        }

        $query->andFilterWhere([
            'oe.id' => $this->id,
            'evaluation_id' => $this->evaluation_id,
            'okr_id' => $this->okr_id,
            'oe.target' => $this->target,
            'op.unit_structure_id' => $this->unit_id,
            'o.pic_id' => $this->pic_id,
            'oe.status_id' => $this->status_id,
            'o.okr_level_id' => $this->okr_level_id,
            'oe.unit_structure_id' => $this->unit_structure_id,
        ]);

        $query->andFilterWhere(['ilike', 'real', $this->real]);
        $query->andFilterWhere(['ilike', 'p.okr_code', $this->parent_okr_code]);
        $query->andFilterWhere(['ilike', 'p.okr', $this->parent_okr]);
        $query->andFilterWhere(['ilike', 'o.okr_code', $this->okr_code]);
        $query->andFilterWhere(['ilike', 'o.okr', $this->okr]);

        // var_dump($query->createCommand()->getRawSql());
                
        return $dataProvider;
    }

    
    public function searchMonitoring($params, $type, $directorateId = null, $departmentId = null)
    {

        if($type == 'unit'){
            $select = 'op.unit_structure_id, u.unit_code, u.unit_name';
            $sort = ['unit_code' => SORT_ASC];
        } else if($type == 'individu'){
            $select = 'o.position_structure_id, u.unit_code, u.unit_name, op.position_name, e.full_name, oe.pic_id';
            $sort = ['unit_code' => SORT_ASC];
            // $sort = ['parent_okr_code' => SORT_ASC, 'okr_code' => SORT_ASC];
        }

        $query = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['okr o', 'okr.parent p', 'okr.position op', 'okr.position.unit u', 'pic e'])
                                ->select($select)
                                ->where(['o.type' => 'KR']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => $sort],
            'pagination' => ['pageSize' => 10]
        ]);

        $dataProvider->sort->attributes['parent_okr_code'] = [
            'asc' => ['p.okr_code' => SORT_ASC],
            'desc' => ['p.okr_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['okr_code'] = [
            'asc' => ['o.okr_code' => SORT_ASC],
            'desc' => ['o.okr_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['unit_code'] = [
            'asc' => ['u.unit_code' => SORT_ASC],
            'desc' => ['u.unit_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        
        if($directorateId){
            $unitIds = [];
            $depts = UnitStructure::find()->where(['superior_unit_id' => $directorateId])->all();
            foreach($depts as $dept){
                array_push($unitIds, $dept->id);

                $units = UnitStructure::find()->where(['superior_unit_id' => $dept->id])->all();
                foreach($units as $unit){
                    array_push($unitIds, $unit->id);
                }
            }
            
            $query->andWhere(['in', 'op.unit_structure_id', $unitIds]);
        }

        if($departmentId){
            $unitIds = [];
            $units = UnitStructure::find()->where(['superior_unit_id' => $departmentId])->all();
            foreach($units as $unit){
                array_push($unitIds, $unit->id);
            }
            
            $query->andWhere(['in', 'op.unit_structure_id', $unitIds]);
        }

        if($type == 'individu'){
            $query->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL]);
            $query->andWhere(['e.employee_type_id' => [1, 2]]);
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'evaluation_id' => $this->evaluation_id,
            'okr_id' => $this->okr_id,
            'oe.target' => $this->target,
            'op.unit_structure_id' => $this->unit_id,
            'o.pic_id' => $this->pic_id,
            'oe.status_id' => $this->status_id,
            'o.okr_level_id' => $this->okr_level_id,
            'u.year' => $this->year
        ]);

        $query->andFilterWhere(['ilike', 'real', $this->real]);
        $query->andFilterWhere(['ilike', 'p.okr_code', $this->parent_okr_code]);
        $query->andFilterWhere(['ilike', 'p.okr', $this->parent_okr]);
        $query->andFilterWhere(['ilike', 'o.okr_code', $this->okr_code]);
        $query->andFilterWhere(['ilike', 'o.okr', $this->okr]);
        $query->andFilterWhere(['ilike', 'u.unit_code', $this->unit_code]);
        $query->andFilterWhere(['ilike', 'u.unit_name', $this->unit_name]);
        $query->andFilterWhere(['ilike', 'op.position_name', $this->position_name]);
        $query->andFilterWhere(['ilike', 'e.full_name', $this->employee_name]);

        $query->groupBy($select);
        // var_dump($query->createCommand()->getRawSql());
        return $dataProvider;
    }

    
    public function searchReport($params, $type, $year = null)
    {
        $query = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['okr o', 'okr.org org', 'okr.parent p', 'okr.position op'])
                                ->where(['o.type' => $type])
                                ->andWhere('oe.unit_structure_id is not null');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['okr_code' => SORT_ASC, 'unit_structure_id' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['parent_okr_code'] = [
            'asc' => ['p.okr_code' => SORT_ASC],
            'desc' => ['p.okr_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['okr_code'] = [
            'asc' => ['o.okr_code' => SORT_ASC],
            'desc' => ['o.okr_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if(!isset($this->year) or empty($this->year)) {
            $lastOkr = Okr::find()->where("year is not null")->orderBy('year desc')->one();
            $this->year = $lastOkr->year ?: date('Y');
        }

        if(Yii::$app->controller->action->id == 'okr-org'){
            $lastEval = Evaluation::find()->orderBy('id asc')->one();
            $this->evaluation_id = $lastEval->id;
        } else if(!isset($this->evaluation_id) or empty($this->evaluation_id)) {
            // $lastEval = Evaluation::find()->orderBy('id desc')->one();
            // $this->evaluation_id = $lastEval->id ?: date('Y');
        }

        if($year){
            $query->andWhere(['o.year' => $year]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'evaluation_id' => $this->evaluation_id,
            'okr_id' => $this->okr_id,
            'target' => $this->target,
            'op.unit_structure_id' => $this->unit_id,
            'o.pic_id' => $this->pic_id,
            'oe.status_id' => $this->status_id,
            'o.okr_level_id' => $this->okr_level_id,
            'oe.unit_structure_id' => $this->unit_structure_id,
            'org.perspective_id' => $this->perspective_id,
            'org.portfolio_type_id' => $this->portofolio_type,
            'oe.evaluation_id' => $this->evaluation_id,
        ]);

        $query->andFilterWhere(['ilike', 'real', $this->real]);
        $query->andFilterWhere(['ilike', 'p.okr_code', $this->parent_okr_code]);
        $query->andFilterWhere(['ilike', 'p.okr', $this->parent_okr]);
        $query->andFilterWhere(['ilike', 'o.okr_code', $this->okr_code]);
        $query->andFilterWhere(['ilike', 'o.okr', $this->okr]);
        // var_dump($query->createCommand()->getRawSql());
        return $dataProvider;
    }
}
