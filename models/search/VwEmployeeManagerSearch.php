<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VwEmployeeManager;

/**
 * VwEmployeeManagerSearch represents the model behind the search form about `app\models\VwEmployeeManager`.
 */
class VwEmployeeManagerSearch extends VwEmployeeManager
{

    public $period;
    public $from_date;
    public $to_date;
    public $unit_code;
    public $unit_name;
    public $position_name;
    public $employee_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'upper_id', 'branch_id', 'position_id', 'unit_id', 'division_id', 'level', 'gm_id', 'director_id', 'user_status', 'year'], 'integer'],
            [['directorate_id', 'unit_code', 'unit_name', 'position_name', 'employee_name', 'from_date', 'to_date', 'period'], 'safe'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VwEmployeeManager::find()->alias('vw')->joinWith(['unit u', 'position p', 'employee e']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['unit_code' => SORT_ASC, 'employee_name' => SORT_ASC]]
        ]);
        
        $dataProvider->sort->attributes['unit_code'] = [
            'asc' => ['u.unit_code' => SORT_ASC],
            'desc' => ['u.unit_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['unit_name'] = [
            'asc' => ['u.unit_name' => SORT_ASC],
            'desc' => ['u.unit_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['position_name'] = [
            'asc' => ['p.position_name' => SORT_ASC],
            'desc' => ['p.position_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['employee_name'] = [
            'asc' => ['e.full_name' => SORT_ASC],
            'desc' => ['e.full_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!$this->year) $this->year = date('Y');

        // if(!$this->from_date && !$this->to_date){
        //     $this->to_date = date('Y-m-d');
        //     $this->from_date = date('Y-m-d', strtotime('-1 week'));
        // }

        // $query->andWhere(['vw.year' =>  date('Y', strtotime($this->from_date))]);

        $query->andFilterWhere([
            'employee_id' => $this->employee_id,
            'upper_id' => $this->upper_id,
            'branch_id' => $this->branch_id,
            'position_id' => $this->position_id,
            'unit_id' => $this->unit_id,
            'division_id' => $this->division_id,
            'level' => $this->level,
            'gm_id' => $this->gm_id,
            'director_id' => $this->director_id,
            'user_status' => $this->user_status,
            'vw.year' => $this->year,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['ilike', 'directorate_id', $this->directorate_id]);
        $query->andFilterWhere(['ilike', 'u.unit_name', $this->unit_name]);
        $query->andFilterWhere(['ilike', 'p.position_name', $this->position_name]);
        $query->andFilterWhere(['ilike', 'e.full_name', $this->employee_name]);

        // var_dump($query->createCommand()->getRawSql());

        return $dataProvider;
    }
}
