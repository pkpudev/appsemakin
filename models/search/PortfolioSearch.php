<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Portfolio;

/**
 * PortfolioSearch represents the model behind the search form about `app\models\Portfolio`.
 */
class PortfolioSearch extends Portfolio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['portfolio_name', 'version', 'portfolio_date', 'created_at', 'updated_at', 'created_by', 'updated_by', 'is_active'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Portfolio::find()
                            ->alias('p')
                            ->joinWith('createdBy0 cb')
                            ->joinWith('updatedBy0 ub');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'p.id' => $this->id,
            'p.portfolio_date' => $this->portfolio_date,
            'p.created_at' => $this->created_at,
            // 'p.created_by' => $this->created_by,
            'p.updated_at' => $this->updated_at,
            'p.is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['ilike', 'portfolio_name', $this->portfolio_name])
            ->andFilterWhere(['ilike', 'version', $this->version])
            ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
            ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by]);

        return $dataProvider;
    }
}
