<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Activities;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\Status;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;

/**
 * ActivitiesSearch represents the model behind the search form about `app\models\Activities`.
 */
class ActivitiesSearch extends Activities
{

    public $year;
    public $unit_structure_id;
    public $directorate_id;
    public $departement_id;
    public $picked_date;
    public $okr_text;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pic_id', 'activity_category_id', 'position_structure_id', 'okr_id', 'parent_id', 'status_id', 'year', 'unit_structure_id', 'directorate_id', 'departement_id', 'project_id'], 'integer'],
            [['activity', 'description', 'start_date', 'finish_date', 'due_date', 'picked_date', 'output', 'okr_text'], 'safe'],
            [['duration', 'progress', 'okr_value', 'evaluation', 'evaluation_value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $status = null)
    {
        $query = Activities::find()->alias('a')->joinWith('okr o')->where('a.parent_id is null');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort'=> ['defaultOrder' => ['due_date' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->year){
            $query->andWhere(["date_part('year', a.start_date)" => $this->year]);
        } else {
            $this->year = date('Y');
        }

        if(Yii::$app->controller->action->id == 'review'){
            $query->andWhere(['a.status_id' => Status::ACT_DONE]);
            $query->andWhere("a.evaluation is null");

            $unitIds = VwEmployeeManager::find()
                                                    ->select('unit_id')
                                                    ->where(['employee_id' => Yii::$app->user->id])
                                                    ->andWhere(['year' => $this->year])
                                                    ->column();

            $picIds = VwEmployeeManager::find()->select('employee_id')->where(['unit_id' => $unitIds])->column();

            $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->andWhere(['year' => $this->year])->one();
            if($isGeneralManager){
                $picIds2 = VwEmployeeManager::find()->select('employee_id')->where(['gm_id' =>  Yii::$app->user->id])->column();
                $picIds = array_merge($picIds, $picIds2);
            }
            

            $currentDate = date('Y-m-d');
            // $query->andWhere(['a.pic_id' => Yii::$app->user->id]); 
            $query->andWhere(['a.pic_id' => $picIds]); 
            // $query->andWhere("a.start_date <= '$currentDate' and a.due_date >= '$currentDate'");    
            
            // $query->andWhere([''])

        } else if(Yii::$app->controller->action->id == 'index'){

            $unitIds = VwEmployeeManager::find()
                                                    ->select('unit_id')
                                                    ->where(['employee_id' => Yii::$app->user->id])
                                                    ->andWhere(['year' => $this->year])
                                                    ->column();

            $picIds = VwEmployeeManager::find()->select('employee_id')->where(['unit_id' => $unitIds])->column();

            $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->andWhere(['year' => $this->year])->one();
            if($isGeneralManager){
                $picIds2 = VwEmployeeManager::find()->select('employee_id')->where(['gm_id' =>  Yii::$app->user->id])->column();
                $picIds = array_merge($picIds, $picIds2);
            }
            

            $currentDate = date('Y-m-d');
            // $query->andWhere(['a.pic_id' => Yii::$app->user->id]); 
            $query->andWhere(['a.pic_id' => $picIds]); 
            $query->andWhere("a.start_date <= '$currentDate' and a.due_date >= '$currentDate'");           
        } else if(Yii::$app->controller->action->id == 'all'){
            if(Yii::$app->user->can('Admin')){

            } else {
                $userID = Yii::$app->user->id;
                $isGm = false;
                $isManager = false;
                
                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }

                        $isGm = true;
                    } else if($ep->position->level == 4){
                        $vwEmployeeManagers = VwEmployeeManager::find()
                                                            ->where(['upper_id' => Yii::$app->user->id])
                                                            ->andWhere(['year' => $this->year])
                                                            ->all();
            
                        $unitIds = [];
                        $picIds = [];
                        foreach($vwEmployeeManagers as $vwEmployeeManager){
                            array_push($dept, $vwEmployeeManager->unit_id);
                        }

                        $isManager = true;
                    }
                }

                if($isGm || $isManager){
                    $picIds = VwEmployeeManager::find()->select('employee_id')->where(['unit_id' => $dept])->column();
    
                    $query->andWhere(['in', 'a.pic_id', $picIds]);         
                } else {
                    $query->andWhere(['a.pic_id' => Yii::$app->user->id]);         
                }
            }
        } else if(Yii::$app->controller->action->id == 'lower'){
            $vwEmployeeManagers = VwEmployeeManager::find()
                                                ->where(['upper_id' => Yii::$app->user->id])
                                                ->andWhere(['year' => $this->year])
                                                ->all();

            $unitIds = [];
            $picIds = [];
            foreach($vwEmployeeManagers as $vwEmployeeManager){
                array_push($unitIds, $vwEmployeeManager->unit_id);
                array_push($picIds, $vwEmployeeManager->employee_id);
            }

            $query->andWhere(['in', 'a.pic_id', $picIds]);
            
        } else if(Yii::$app->controller->action->id == 'kanban'){

            if(Yii::$app->user->can('Admin')){

            } else {

                /* if(Yii::$app->controller->action->id == 'lower'){
                    $unitIds = [];
                    $picIds = [];
                    array_push($picIds, Yii::$app->user->id);
                    
                    $vwEmployeeManagers = VwEmployeeManager::find()
                                                    ->where(['upper_id' => Yii::$app->user->id])
                                                    ->andWhere(['year' => $this->year])
                                                    ->all();
    
                    foreach($vwEmployeeManagers as $vwEmployeeManager){
                        array_push($unitIds, $vwEmployeeManager->unit_id);
                        array_push($picIds, $vwEmployeeManager->employee_id);
                    }
                    $query->andWhere(['not in', 'a.pic_id', Yii::$app->user->id]);
                } else { */
                    $userID = Yii::$app->user->id;
                    $isGm = false;
                    $isManager = false;
                    
                    $dept = [];
                    $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
                    foreach($employeePositions as $ep){
                        array_push($dept, $ep->position->unit_structure_id);

                        if($ep->position->level == 6){
                            $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                            foreach($gms as $gm){
                                array_push($dept, $gm->unit_structure_id);
                            }

                            $isGm = true;
                        } else if($ep->position->level == 4){
                            $vwEmployeeManagers = VwEmployeeManager::find()
                                                                ->where(['upper_id' => Yii::$app->user->id])
                                                                ->andWhere(['year' => $this->year])
                                                                ->all();
                
                            $unitIds = [];
                            $picIds = [];
                            foreach($vwEmployeeManagers as $vwEmployeeManager){
                                array_push($dept, $vwEmployeeManager->unit_id);
                            }

                            $isManager = true;
                        }
                    }

                    if($isGm || $isManager){
                        $picIds = VwEmployeeManager::find()->select('employee_id')->where(['unit_id' => $dept])->column();
        
                        $query->andWhere(['in', 'a.pic_id', $picIds]);         
                    } else {
                        // $query->andWhere(['a.pic_id' => Yii::$app->user->id]);         
                    }
                // }          
            }
        }

        if($this->directorate_id || $this->departement_id || $this->unit_structure_id){
            $selectedUnit = [];

            if($this->directorate_id){
                array_push($selectedUnit, $this->directorate_id);
                $directorate = UnitStructure::find()->where(['superior_unit_id' => $this->directorate_id])->all();
                foreach($directorate as $dir){
                    $departements = UnitStructure::find()->where(['superior_unit_id' => $dir->id])->all();
                    foreach($departements as $dept){
                        array_push($selectedUnit, $dept->id);
                    }
                }
            } else if($this->departement_id){
                array_push($selectedUnit, $this->departement_id);
                $departements = UnitStructure::find()->where(['superior_unit_id' => $this->departement_id])->all();
                foreach($departements as $dept){
                    array_push($selectedUnit, $dept->id);
                }
            } else {
                array_push($selectedUnit, $this->unit_structure_id);
            }

            
            $positionIds = PositionStructure::find()->select('id')->where(['unit_structure_id' => $selectedUnit])->column();
            
            $query->andWhere(['in', 'a.position_structure_id', $positionIds]);
        }

        if($this->picked_date){
            if($this->picked_date == 'Invalid date - Invalid date'){
                $this->picked_date = null;
            } else {
                list($picked_date_from, $picked_date_to) = explode(' - ', $this->picked_date);
                $query->andWhere("
                                    (cast('{$picked_date_from}' as timestamp) >= a.start_date AND cast('{$picked_date_from}' as timestamp) <= a.due_date) OR 
                                    (cast('{$picked_date_to}' as timestamp) >= a.start_date AND cast('{$picked_date_to}' as timestamp) <= a.due_date) OR
                                    (a.start_date >= '{$picked_date_from}' AND a.start_date <= '{$picked_date_to}') OR 
                                    (a.due_date >= '{$picked_date_from}' AND a.due_date <= '{$picked_date_to}') OR 
                                    (cast('{$picked_date_from}' as timestamp) <= a.start_date AND cast('{$picked_date_to}' as timestamp) >= a.due_date)
                                "); 

                $this->picked_date = $this->picked_date;
            }
        }

        if($status == 'belum_dimulai'){
            $query->andWhere(['a.status_id' => Status::ACT_NEW]);
        } else if($status == 'sedang_berlangsung'){
            $query->andWhere(['a.status_id' => Status::ACT_ON_PROGRESS]);
        } else if($status == 'selesai'){
            $query->andWhere(['a.status_id' => Status::ACT_DONE]);
        }

        if($this->okr_text){
            $okrText = $this->okr_text;
            $query->andWhere("o.okr_code ilike '%$okrText%' or o.okr ilike '%$okrText%'");
        }

        $query->andFilterWhere([
            'a.id' => $this->id,
            'a.pic_id' => $this->pic_id,
            'a.activity_category_id' => $this->activity_category_id,
            'a.position_structure_id' => $this->position_structure_id,
            'okr_id' => $this->okr_id,
            'a.parent_id' => $this->parent_id,
            'a.start_date' => $this->start_date,
            'a.finish_date' => $this->finish_date,
            'a.due_date' => $this->due_date,
            'a.status_id' => $this->status_id,
            'a.duration' => $this->duration,
            'a.progress' => $this->progress,
            'a.okr_value' => $this->okr_value,
            'a.evaluation' => $this->evaluation,
            'a.evaluation_value' => $this->evaluation_value,
            'a.project_id' => $this->project_id,
        ]);

        $query->andFilterWhere(['ilike', 'a.activity', $this->activity])
            ->andFilterWhere(['ilike', 'a.description', $this->description])
            ->andFilterWhere(['ilike', 'a.output', $this->output]);

        // var_dump($query->createCommand()->getRawSql());

        return $dataProvider;
    }
}
