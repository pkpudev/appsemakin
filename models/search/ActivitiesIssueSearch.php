<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ActivitiesIssue;
use app\models\Okr;
use app\models\PositionStructure;

/**
 * ActivitiesIssueSearch represents the model behind the search form about `app\models\ActivitiesIssue`.
 */
class ActivitiesIssueSearch extends ActivitiesIssue
{

    public $full_name;
    public $activity;
    public $okr;
    public $unit_structure_id;
    public $year;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'activities_id', 'issue_type_id', 'issue_level', 'status_id', 'unit_structure_id', 'year'], 'integer'],
            [['reporting_date', 'issue', 'alternative_solution', 'resolution', 'due_date', 'full_name', 'activity', 'okr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ActivitiesIssue::find()
                                    ->alias('ai')
                                    ->joinWith(['activities a', 'activities.pic p', 'activities.okr o']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['full_name'=>SORT_ASC]],
        ]);

        $dataProvider->sort->attributes['full_name'] = [
            'asc' => ['p.full_name' => SORT_ASC],
            'desc' => ['p.full_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if(!isset($this->year) or empty($this->year)) {
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $this->year = $lastOkr->year ?: date('Y');
        }

        
        if(/* $this->directorate_id || $this->departement_id ||  */$this->unit_structure_id){
            $selectedUnit = [];

            /* if($this->directorate_id){
                $directorate = UnitStructure::find()->where(['superior_unit_id' => $this->directorate_id])->all();
                foreach($directorate as $dir){
                    $departements = UnitStructure::find()->where(['superior_unit_id' => $dir->id])->all();
                    foreach($departements as $dept){
                        array_push($selectedUnit, $dept->id);
                    }
                }
            } else if($this->departement_id){
                $departements = UnitStructure::find()->where(['superior_unit_id' => $this->departement_id])->all();
                foreach($departements as $dept){
                    array_push($selectedUnit, $dept->id);
                }
            } else { */
                array_push($selectedUnit, $this->unit_structure_id);
            // }

            
            $positionIds = PositionStructure::find()->select('id')->where(['unit_structure_id' => $selectedUnit])->column();
            
            $query->andWhere(['in', 'a.position_structure_id', $positionIds]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'activities_id' => $this->activities_id,
            'reporting_date' => $this->reporting_date,
            'issue_type_id' => $this->issue_type_id,
            'issue_level' => $this->issue_level,
            'due_date' => $this->due_date,
            'status_id' => $this->status_id,
            'o.year' => $this->year,
        ]);

        if($this->okr){
            $query->andWhere("o.okr_code ilike '%{$this->okr}%' or o.okr ilike '%{$this->okr}%'");
        }

        $query->andFilterWhere(['ilike', 'issue', $this->issue])
            ->andFilterWhere(['ilike', 'alternative_solution', $this->alternative_solution])
            ->andFilterWhere(['ilike', 'a.activity', $this->activity])
            ->andFilterWhere(['ilike', 'resolution', $this->resolution]);
            
        return $dataProvider;
    }
}
