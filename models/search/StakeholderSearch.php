<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Stakeholder;

/**
 * StakeholderSearch represents the model behind the search form about `app\models\Stakeholder`.
 */
class StakeholderSearch extends Stakeholder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'stakeholder_category_id', 'influence_id', 'influence_point', 'support_id', 'support_point', 'analysis_result_id', 'result_point'], 'integer'],
            [['stakeholder', 'role', 'responsibility', 'relationship_owner', 'expectation', 'issue', 'engagement'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stakeholder::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'stakeholder_category_id' => $this->stakeholder_category_id,
            'influence_id' => $this->influence_id,
            'influence_point' => $this->influence_point,
            'support_id' => $this->support_id,
            'support_point' => $this->support_point,
            'analysis_result_id' => $this->analysis_result_id,
            'result_point' => $this->result_point,
        ]);

        $query->andFilterWhere(['ilike', 'stakeholder', $this->stakeholder])
            ->andFilterWhere(['ilike', 'role', $this->role])
            ->andFilterWhere(['ilike', 'responsibility', $this->responsibility])
            ->andFilterWhere(['ilike', 'relationship_owner', $this->relationship_owner])
            ->andFilterWhere(['ilike', 'expectation', $this->expectation])
            ->andFilterWhere(['ilike', 'issue', $this->issue])
            ->andFilterWhere(['ilike', 'engagement', $this->engagement]);

        return $dataProvider;
    }
}
