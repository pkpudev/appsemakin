<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ActivityEvaluation;
use app\models\Okr;

/**
 * ActivityEvaluationSearch represents the model behind the search form about `app\models\ActivityEvaluation`.
 */
class ActivityEvaluationSearch extends ActivityEvaluation
{

    public $year;
    public $unit_id;
    public $initiatives_code;
    public $initiatives;
    public $activity_code;
    public $activity_name;
    public $pic_id;
    public $resources;
    public $period;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'evaluation_id', 'activity_id', 'actual_value', 'year', 'unit_id', 'pic_id'], 'integer'],
            [['actual_note', 'initiatives_code', 'initiatives', 'activity_code', 'activity_name', 'resources', 'period'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ActivityEvaluation::find()
                        ->alias('ae')
                        ->joinWith(['activity a', 'activity.initiative i', 'activity.initiative.okr o', 'activity.initiative.okr.position p', 'evaluation e']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['activity_code'=>SORT_ASC]],
        ]);

        $dataProvider->sort->attributes['activity_code'] = [
            'asc' => ['a.activity_code' => SORT_ASC],
            'desc' => ['a.activity_code' => SORT_DESC],
        ];
        
        $this->load($params);

        if(!Yii::$app->user->can('Admin')){
            $this->pic_id = Yii::$app->user->id;
        }

        if(!isset($this->year) or empty($this->year)) {
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $this->year = $lastOkr->year ?: date('Y');
        }
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'evaluation_id' => $this->evaluation_id,
            'activity_id' => $this->activity_id,
            'actual_value' => $this->actual_value,
            'o.year' => $this->year,
            'p.unit_structure_id' => $this->unit_id,
            'a.pic_id' => $this->pic_id,
            'e.year' => $this->year,
            'e.period' => $this->period,
        ]);

        $query->andFilterWhere(['ilike', 'actual_note', $this->actual_note]);
        $query->andFilterWhere(['ilike', 'i.initiatives_code', $this->initiatives_code]);
        $query->andFilterWhere(['ilike', 'i.initiatives', $this->initiatives]);
        $query->andFilterWhere(['ilike', 'a.activity_code', $this->activity_code]);
        $query->andFilterWhere(['ilike', 'a.activity_name', $this->activity_name]);
        $query->andFilterWhere(['ilike', 'a.resources', $this->resources]);

        return $dataProvider;
    }
}
