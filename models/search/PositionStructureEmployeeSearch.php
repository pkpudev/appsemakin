<?php

namespace app\models\search;

use app\models\PositionStructure;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PositionStructureEmployee;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;

/**
 * PositionStructureEmployeeSearch represents the model behind the search form about `app\models\PositionStructureEmployee`.
 */
class PositionStructureEmployeeSearch extends PositionStructureEmployee
{
    public $unit_id;
    public $level;
    public $branch_id;
    public $employee_type_id;
    public $year;
    public $unit_structure_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'level', 'branch_id', 'employee_type_id', 'year', 'unit_structure_id'], 'integer'],
            [['comment', 'created_stamp', 'ip', 'position_structure_id', 'employee_id', 'unit_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PositionStructureEmployee::find()
                    ->alias('pse')
                    ->joinWith(['employee e', 'position p'])
                    ->where(['e.employee_type_id' => [1, 2]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['employee_id' => SORT_ASC]]
        ]);
        
        $dataProvider->sort->attributes['employee_id'] = [
            'asc' => ['e.full_name' => SORT_ASC],
            'desc' => ['e.full_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->year){
            $query->andWhere(['p.year' => $this->year]);
        }

        if(!Yii::$app->user->can('SuperUser')) {
            if(!Yii::$app->user->can('BOD') and !Yii::$app->user->can('Admin') and !Yii::$app->user->can('Admin Direktorat')){

                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);

                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }
                
                if(!Yii::$app->user->can('Leader')) $query->andWhere(['p.year' => date('Y')]);
                $query->andWhere(['in', 'p.unit_structure_id', $dept]);
            }  else if(Yii::$app->user->can('Admin Direktorat')){
                $userId = Yii::$app->user->id;
                $dept = [];
    
                $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $this->year])->all();

                $d = [];
                foreach($vwEmployeeManagers as $vw){
                    array_push($d, $vw->directorate_id);
                }
    
                $directorates = UnitStructure::find()->where(['year' => $this->year])->andWhere(['superior_unit_id' => $d])->orderBy('unit_code')->all();
                foreach($directorates as $directorate){
                    array_push($dept, $directorate->id);
                    
                    $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                    foreach($divisions as $division){
                        array_push($dept, $division->id);
                    }
                }

                $query->andWhere(['in', 'p.unit_structure_id', $dept]);
            } 
        }

        if(Yii::$app->controller->action->id == 'okr2'){
            $query->andWhere(['pse.is_active' => true]);
        }

        if($this->unit_structure_id){
            $query->andWhere(['p.unit_structure_id' => $this->unit_structure_id]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'p.unit_structure_id' => $this->unit_id,
            // 'position_structure_id' => $this->position_structure_id,
            'employee_id' => $this->employee_id,
            'created_by' => $this->created_by,
            'created_stamp' => $this->created_stamp,
            'p.level' => $this->level,
            'e.branch_id' => $this->branch_id,
            'e.employee_type_id' => $this->employee_type_id,
            // 'p.unit_structure_id' => $this->unit_structure_id,
        ]);

        $query->andFilterWhere(['ilike', 'comment', $this->comment])
            ->andFilterWhere(['ilike', 'p.position_name', $this->position_structure_id])
            // ->andFilterWhere(['ilike', 'e.full_name', $this->employee_id])
            ->andFilterWhere(['ilike', 'ip', $this->ip]);
            
        return $dataProvider;
    }
}
