<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ceiling;

/**
 * CeilingSearch represents the model behind the search form about `app\models\Ceiling`.
 */
class CeilingSearch extends Ceiling
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year'], 'integer'],
            [['ceiling_amount', 'created_at', 'updated_at', 'unit_structure_id', 'created_by', 'updated_by', 'position_structure_id'], 'safe'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ceiling::find()
                    ->alias('c')
                    ->joinWith('unit u')
                    ->joinWith('position p')
                    ->joinWith('createdBy0 cb')
                    ->joinWith('updatedBy0 ub');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['year' => SORT_DESC, 'ucode' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['ucode'] = [
            'asc' => ['u.unit_code' => SORT_ASC],
            'desc' => ['u.unit_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['uname'] = [
            'asc' => ['u.unit_name' => SORT_ASC],
            'desc' => ['u.unit_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'c.id' => $this->id,
            'c.year' => $this->year,
            'c.is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'u.unit_name', $this->unit_structure_id]);
        $query->andFilterWhere(['ilike', 'ceiling_amount', $this->ceiling_amount])
                ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
                ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by]);

        return $dataProvider;
    }
}
