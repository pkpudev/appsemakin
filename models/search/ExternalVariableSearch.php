<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ExternalVariable;

/**
 * ExternalVariableSearch represents the model behind the search form about `app\models\ExternalVariable`.
 */
class ExternalVariableSearch extends ExternalVariable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['variable', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ExternalVariable::find()
            ->alias('ev')
            ->joinWith('createdBy0 cb')
            ->joinWith('updatedBy0 ub');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ev.id' => $this->id,
            'ev.is_active' => $this->is_active,
            'ev.created_at' => $this->created_at,
            'ev.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'variable', $this->variable])
        ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
        ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by]);

        return $dataProvider;
    }
}
