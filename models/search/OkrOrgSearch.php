<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OkrOrg;

/**
 * OkrOrgSearch represents the model behind the search form about `app\models\OkrOrg`.
 */
class OkrOrgSearch extends OkrOrg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'perspective_id', 'portfolio_type_id', 'start_year', 'end_year', 'version', 'created_by', 'updated_by'], 'integer'],
            [['okr', 'type', 'okr_code', 'measure', 'implementation_model', 'created_at', 'updated_at'], 'safe'],
            [['target', 'weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type = 'KR')
    {
        $query = OkrOrg::find()
                        ->alias('o')
                        ->joinWith('parent pr')
                        ->joinWith('createdBy0 cb')
                        ->joinWith('updatedBy0 ub')
                        ->joinWith('perspective p')
                        ->where(['o.type' => $type]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [/* 'perspective_id' => SORT_ASC, 'parent_okr_code' => SORT_ASC,  */'current_okr_code' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['parent_okr_code'] = [
            'asc' => ['pr.okr_code' => SORT_ASC],
            'desc' => ['pr.okr_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['current_okr_code'] = [
            'asc' => ['o.okr_code' => SORT_ASC],
            'desc' => ['o.okr_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'perspective_id' => $this->perspective_id,
            'o.portfolio_type_id' => $this->portfolio_type_id,
            'target' => $this->target,
            'o.start_year' => $this->start_year,
            'o.end_year' => $this->end_year,
            'version' => $this->version,
            'weight' => $this->weight,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'o.okr', $this->okr])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'o.okr_code', $this->okr_code])
            ->andFilterWhere(['like', 'measure', $this->measure])
            ->andFilterWhere(['like', 'implementation_model', $this->implementation_model]);

            // var_dump($query->createCommand()->getRawSql());

        return $dataProvider;
    }
}
