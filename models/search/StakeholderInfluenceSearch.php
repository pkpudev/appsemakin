<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StakeholderInfluence;

/**
 * StakeholderInfluenceSearch represents the model behind the search form about `app\models\StakeholderInfluence`.
 */
class StakeholderInfluenceSearch extends StakeholderInfluence
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'influence_point'], 'integer'],
            [['influence_grade', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StakeholderInfluence::find()
                    ->alias('si')
                    ->joinWith('createdBy0 cb')
                    ->joinWith('updatedBy0 ub');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'si.id' => $this->id,
            'si.influence_point' => $this->influence_point,
            'si.created_at' => $this->created_at,
            'si.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'si.influence_grade', $this->influence_grade])
                ->andFilterWhere(['ilike', 'cb.full_name', $this->created_by])
                ->andFilterWhere(['ilike', 'ub.full_name', $this->updated_by]);

        return $dataProvider;
    }
}
