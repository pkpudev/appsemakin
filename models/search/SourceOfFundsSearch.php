<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SourceOfFunds;

/**
 * SourceOfFundsSearch represents the model behind the search form about `app\models\SourceOfFunds`.
 */
class SourceOfFundsSearch extends SourceOfFunds
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'type'], 'integer'],
            [['source_of_funds'], 'safe'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SourceOfFunds::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_active' => $this->is_active,
            'company_id' => $this->company_id,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['ilike', 'source_of_funds', $this->source_of_funds]);

        return $dataProvider;
    }
}
