<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinancingTypeCeiling;
use app\models\Okr;

/**
 * FinancingTypeCeilingSearch represents the model behind the search form about `app\models\FinancingTypeCeiling`.
 */
class FinancingTypeCeilingSearch extends FinancingTypeCeiling
{

    public $year;
    public $unit_code;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'unit_structure_id', 'financing_type_id', 'created_by', 'updated_by', 'year'], 'integer'],
            [['ceiling_amount'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinancingTypeCeiling::find()
                                            ->alias('ftc')
                                            ->joinWith(['unit u', 'createdBy0 c']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['unit_code' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['unit_code'] = [
            'asc' => ['u.unit_code' => SORT_ASC],
            'desc' => ['u.unit_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!isset($this->year) or empty($this->year)) {
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $this->year = $lastOkr->year ?: date('Y');
        }

        $query->andFilterWhere([
            'ftc.id' => $this->id,
            'ftc.unit_structure_id' => $this->unit_structure_id,
            'ftc.financing_type_id' => $this->financing_type_id,
            'ftc.ceiling_amount' => $this->ceiling_amount,
            'ftc.created_at' => $this->created_at,
            'ftc.created_by' => $this->created_by,
            'ftc.updated_at' => $this->updated_at,
            'ftc.updated_by' => $this->updated_by,
            'ftc.updated_by' => $this->updated_by,
            'u.year' => $this->year,
        ]);

        return $dataProvider;
    }
}
