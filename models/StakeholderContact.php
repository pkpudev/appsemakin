<?php

namespace app\models;

use Yii;
use \app\models\base\StakeholderContact as BaseStakeholderContact;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.stakeholder_contact".
 */
class StakeholderContact extends BaseStakeholderContact
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }


    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
