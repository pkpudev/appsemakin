<?php

namespace app\models;

use Yii;
use \app\models\base\ActivitiesIssue as BaseActivitiesIssue;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.activities_issue".
 */
class ActivitiesIssue extends BaseActivitiesIssue
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    
    //RELATION
    public function getActivities()
    {
        return $this->hasOne(Activities::className(), ['id' => 'activities_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
    
    public function getType()
    {
        return $this->hasOne(IssueType::className(), ['id' => 'issue_type_id']);
    }
}
