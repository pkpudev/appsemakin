<?php

namespace app\models;

use Yii;
use \app\models\base\ParticipatoryBudgeting as BaseParticipatoryBudgeting;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.participatory_budgeting".
 */
class ParticipatoryBudgeting extends BaseParticipatoryBudgeting
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
