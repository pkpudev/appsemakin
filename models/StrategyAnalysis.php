<?php

namespace app\models;

use Yii;
use \app\models\base\StrategyAnalysis as BaseStrategyAnalysis;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.strategy_analysis".
 */
class StrategyAnalysis extends BaseStrategyAnalysis
{

    const StrategyType = [
        'SO' => 'Strength - Opportunity',
        'WO' => 'Weakness - Opportunity',
        'ST' => 'Strength - Threat',
        'WT' => 'Weakness - Threat',
    ];

    public $strategyText;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['strategyText'], 'safe']
            ]
        );
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }
    
    public function getStrategy()
    {
        return $this->hasOne(\app\models\Strategies::className(), ['id' => 'strategy_id']);
    }

    public function getSituation()
    {
        return $this->hasOne(\app\models\SituationAnalysis::className(), ['id' => 'situation_analysis_id'])->via('strategy');
    }
    
    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
