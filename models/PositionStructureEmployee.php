<?php

namespace app\models;

use Yii;
use \app\models\base\PositionStructureEmployee as BasePositionStructureEmployee;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.position_structure__employee".
 */
class PositionStructureEmployee extends BasePositionStructureEmployee
{

    public $q1;
    public $q2;
    public $q3;
    public $q4;

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{             
            }

            return true;
        }
    }

    public function getPosition()
    {
        return $this->hasOne(PositionStructure::className(), ['id' => 'position_structure_id']);
    }

    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }
    

    /* public function getPriority()
    {
        return $this->hasOne(PositionPriority::className(), ['id' => 'position_priority_id']);
    }

    public function getControlability()
    {
        return $this->hasOne(PositionControlability::className(), ['id' => 'position_controlabilty_id']);
    } */
    
    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
