<?php

namespace app\models;

use Yii;
use \app\models\base\Files as BaseFiles;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.files".
 */
class Files extends BaseFiles
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
