<?php

namespace app\models;

use Yii;
use \app\models\base\Risk as BaseRisk;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.risk".
 */
class Risk extends BaseRisk
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            $this->rpn = $this->severity * $this->occurance/*  * $this->detection */;
            $this->risk_criteria = $this->riskValue();

            return true;
        }
    }

    protected function riskValue(){

        if(in_array($this->severity, [9, 10])){
            return 'Critical Risk';
        } else if(
                    $this->severity == 1 || 
                    ($this->occurance > 1 && $this->occurance <= 8 && $this->severity == 1) || 
                    ($this->occurance == 2 && $this->severity <= 4) || 
                    ($this->occurance == 3 && $this->severity <= 3) ||
                    ($this->occurance == 4 && $this->severity <= 2)
                ){
            return 'Low Risk';
        } else if(
                    ($this->severity >= 5 && $this->severity <= 8 && $this->occurance >= 4) ||
                    ($this->severity == 4 && $this->occurance >= 7) || 
                    ($this->severity == 3 && $this->occurance >= 9)
                ){
            return 'High Risk';
        } else {
            return 'Medium Risk';
        }

    }

    public function getInitiatives()
    {
        return $this->hasOne(\app\models\Initiatives::className(), ['id' => 'initiatives_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(\app\models\RiskCategory::className(), ['id' => 'risk_category_id']);
    }
    
}
