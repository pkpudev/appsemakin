<?php

namespace app\models;

use Yii;
use \app\models\base\Budget as BaseBudget;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.budget".
 */
class Budget extends BaseBudget
{
    public $budget_available;
    public $old_total_amount;
    // public $initiatives_id;
    public $currentScenario;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['initiatives_id'], 'required'],
            ]
        );
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->setTotalAmount();
        $this->setOldTotalAmount();
    }

    public function setTotalAmount()
    {
        $this->total_amount = (float) ($this->volume * ((float) $this->unit_amount) * $this->frequency);
    }

    public function setOldTotalAmount()
    {
        $this->old_total_amount = $this->total_amount;
    }

    public function beforeValidate()
    {
        if (!parent::beforeValidate())
            return false;

        if (!is_numeric($this->volume)) {
            $this->volume = 0;
        }
        if (!is_numeric($this->unit_amount)) {
            $this->unit_amount = 0;
        }
        if (!is_numeric($this->frequency)) {
            $this->frequency = 1;
        }
        $this->setTotalAmount();
        
        return true;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->status_id = \app\models\Status::DRAFT;
                // $activity = Activity::findOne($this->activity_id);
                $this->year = $this->initiatives->okr->year;
            } else {
                
            }

            $this->total_amount = (float) ($this->volume * ((float) $this->unit_amount) * $this->frequency);
            $this->description = ucfirst($this->description);

            return true;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {

        // if((!$insert) && ($this->activity_id <> $changedAttributes['activity_id']))
        // if((!$insert) && ($this->activity_id <> $changedAttributes['initatives_id']))
        // {
        //     //update old activity_id => initiative total budget
        //     // if($activity = Activity::findOne($changedAttributes['activity_id'])){
        //     if($this->initiatives_id){
        //         if($initiatives = Initiatives::findOne($this->initiatives_id)){
        //             $initiatives->total_budget = $this->getInitiativesTotalBudget($initiatives->id);
        //             $initiatives->save(false); 
        //         }

        //     }
        // }
        
        // if(($insert) || ($changedAttributes['total_amount'])){
            //update new activity_id => initiative total budget
            // $activity = Activity::findOne($this->activity_id);
        // }
        $initiatives = Initiatives::findOne($this->initiatives_id);
        $initiatives->total_budget = $this->getInitiativesTotalBudget($initiatives->id);
        $initiatives->save(false);
        

        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        // $activity = Activity::findOne($this->activity_id);
        $initiatives = Initiatives::findOne($this->initiatives_id);
        $initiatives->total_budget = $this->getInitiativesTotalBudget($initiatives->id);
        $initiatives->save(false);

        parent::afterSave($insert, $changedAttributes);
    }

    public function getInitiativesTotalBudget($initiatives_id)
    {
        // $activities = Activity::find()->where(['initiatives_id'=>$initiatives_id])->andWhere(['not in', 'status_id', [Status::REJECTED,Status::DELETED]])->all();
        // $idActivities = [];
        // foreach($activities as $a){
        //     array_push($idActivities, $a->id);
        // }

        // $total_budget = Budget::find()->where(['in', 'activity_id', $idActivities])->andWhere(['not in', 'status_id', [Status::REJECTED,Status::DELETED]])->sum('total_amount');
        $total_budget = Budget::find()->where(['initiatives_id' =>  $initiatives_id])->andWhere(['not in', 'status_id', [Status::REJECTED,Status::DELETED]])->sum('total_amount');

        return $total_budget;
    }

    public static function getUnitBudget($unit_id, $month, $column){

        if($column == 'budget'){
            $monthColumn = 'month_' . str_pad(((int) $month), 2, "0", STR_PAD_LEFT);
        } else {
            $monthColumn = 'real_month_' . str_pad(((int) $month), 2, "0", STR_PAD_LEFT);
        }

        $positions = PositionStructure::find()->where(['unit_structure_id' => $unit_id])->all();

        $positionId = [];
        foreach($positions as $position){
            array_push($positionId, $position->id);

            $year = $position->year;
        }

        $budget = self::find()
                            ->alias('b')
                            ->joinWith($year == 2022 ? 'activity a' : 'initiatives')
                            ->where(['in', 'position_structure_id', $positionId])
                            ->sum($monthColumn);

        return $budget ?: 0;
    }

    public static function getUnitBudgetYear($unit_id, $column){

        if($column == 'budget'){
            $monthColumnName = 'month_';
        } else {
            $monthColumnName = 'real_month_';
        }

        $positions = PositionStructure::find()->where(['unit_structure_id' => $unit_id])->all();

        $positionId = [];
        foreach($positions as $position){
            array_push($positionId, $position->id);

            $year = $position->year;
        }

        $budget = 0;
        for($m = 1; $m <= 12; $m++){

            $monthColumn = $monthColumnName . str_pad(((int) $m), 2, "0", STR_PAD_LEFT);
            $budgetValue = self::find()
                                    ->alias('b')
                                    ->joinWith($year == 2022 ? 'activity a' : 'initiatives')
                                    ->where(['in', 'position_structure_id', $positionId])
                                    ->sum($monthColumn);

            $budget = $budget + $budgetValue;
        }

        return $budget;
    }

    public static function getUnitBudgetCompany($year, $column, $dept = null, $m, $approved = false){

        if($column == 'budget'){
            $monthColumn = 'month_' . str_pad(((int) $m), 2, "0", STR_PAD_LEFT);
        } else {
            $monthColumn = 'real_month_' . str_pad(((int) $m), 2, "0", STR_PAD_LEFT);
        }

        if($dept){
            $positions = PositionStructure::find()->where(['in', 'unit_structure_id', $dept])->all();
    
            $positionId = [];
            foreach($positions as $position){
                array_push($positionId, $position->id);
                $year = $position->year;
            }
        }

        if($dept){
            $budgetValue = self::find()
                    ->alias('b')
                    ->joinWith($year == 2022 ? 'activity a' : 'initiatives')
                    ->where(['in', 'position_structure_id', $positionId])
                    ->sum($monthColumn);
        } else {
            if($year == 2022){
                $budgetValue = self::find()
                        ->where(['year' => $year])
                        ->sum($monthColumn);
            } else {
                $budgetValue = self::find()
                        ->alias('b')
                        ->joinWith(['initiatives i', 'initiatives.position p', 'initiatives.okr o', 'initiatives.position.unit us'])
                        ->where(['o.year' => $year])
                        ->andWhere(['b.year' => $year])
                        ->andWhere(['us.year' => $year])
                        ->andWhere($approved ? ['i.status_id' => Status::APPROVED] : "i.status_id not in (5, 6)")
                        ->sum($monthColumn);
            }
        }
            

        return $budgetValue ?: 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(\app\models\Activity::className(), ['id' => 'activity_id']);
    }
    public function getInitiatives()
    {
        return $this->hasOne(\app\models\Initiatives::className(), ['id' => 'initiatives_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(\app\models\Status::className(), ['id' => 'status_id']);
    }

    public function getSourceOfFunds()
    {
        return $this->hasOne(\app\models\SourceOfFunds::className(), ['id' => 'source_of_funds_id']);
    }

    public function getCostType()
    {
        return $this->hasOne(\app\models\CostType::className(), ['id' => 'cost_type_id']);
    }

    public function getCostPriority()
    {
        return $this->hasOne(\app\models\CostPriority::className(), ['id' => 'cost_priority_id']);
    }

    public function getFinancingType()
    {
        return $this->hasOne(\app\models\FinancingType::className(), ['id' => 'financing_type_id']);
    }

    public function getAccount()
    {
        return $this->hasOne(\app\models\Account::className(), ['id' => 'account_id']);
    }

    public function getBudgetHistories()
    {
        return $this->hasMany(\app\models\BudgetHistory::className(), ['budget_id' => 'id']);
    }
}
