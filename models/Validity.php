<?php

namespace app\models;

use Yii;
use \app\models\base\Validity as BaseValidity;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.validity".
 */
class Validity extends BaseValidity
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
