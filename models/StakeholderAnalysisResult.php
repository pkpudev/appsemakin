<?php

namespace app\models;

use Yii;
use \app\models\base\StakeholderAnalysisResult as BaseStakeholderAnalysisResult;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.stakeholder_analysis_result".
 */
class StakeholderAnalysisResult extends BaseStakeholderAnalysisResult
{

    const MONITOR   = 1;
    const INFORMED  = 2;
    const SATISFIED = 3;
    const CLOSELY   = 4;

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }


    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
