<?php

namespace app\models;

use Yii;
use \app\models\base\SituationAnalysisFiles as BaseSituationAnalysisFiles;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.situation_analysis__files".
 */
class SituationAnalysisFiles extends BaseSituationAnalysisFiles
{

    public $files;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['files'], 'safe']
            ]
        );
    }

    public function getFile()
    {
        return $this->hasOne(\app\models\Files::className(), ['id' => 'files_id']);
    }
}
