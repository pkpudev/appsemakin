<?php

namespace app\models;

use Yii;
use \app\models\base\Polarization as BasePolarization;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.polarization".
 */
class Polarization extends BasePolarization
{

    const MAXIMIZE      = 1;
    const MINIMIZE      = 2;
    const STABILIZE     = 3;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
