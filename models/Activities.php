<?php

namespace app\models;

use Yii;
use \app\models\base\Activities as BaseActivities;
use DateTime;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.activities".
 */
class Activities extends BaseActivities
{

    const ISSUE_LEVEL = [1 => 'Strategis', 2 => 'Operasional', 3 => 'Teknis'];
    const WEIGHT_TEXT = [15 => 'Mudah', 35 => 'Sedang', 50 => 'Berat'];

    public $hour, $minutes, $links, $storages, $files, $year;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
                [['hour', 'minutes', 'files', 'links', 'storages'], 'safe'],
                [['due_date'], 'checkDate'],
            ]
        );
    }

    public function checkDate($attribute){
        $date1 = new DateTime($this->start_date);
        $date2 = new DateTime($this->due_date);

        $interval = $date1->diff($date2);

        $month1 = date('n', strtotime($this->start_date));
        $month2 = date('n', strtotime($this->due_date));

        if(
            ($month1 == 3 && $month2 == 4) || 
            ($month1 == 6 && $month2 == 7) || 
            ($month1 == 9 && $month2 == 10) || 
            ($month1 == 12 && $month2 == 1)
        ){
            $this->addError($attribute, 'Tanggal Mulai & Tanggal Target Selesai tidak bisa lintas kuartal');
        } else if($interval->days >= 14){
            $this->addError($attribute, 'Maksimal 14 hari jaraknya antara Tanggal Mulai & Tanggal Target Selesai');
        }
    }

    public function afterFind()
    {
        parent::afterFind();
        // var_dump($this->position_structure_id);

        
        // $this->position_structure_id = explode('-', $this->position_structure_id)[0];

       if($this->duration){
            $duration = explode('.', $this->duration);

            $this->hour = $duration[0] ?: 0;
            
            if($duration[1]){
                $this->minutes = round(($this->duration - $duration[0]) * 60);
            } else {
                $this->minutes = 0;
            }
       }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->status_id = Status::ACT_NEW;
                if(!$this->pic_id) $this->pic_id = Yii::$app->user->id;
            } else {
                $child = self::findOne(['parent_id' => $this->id]);
            }

            if($this->evaluation){
                $ev = ActivityEvaluationValue::find()->where(['evaluation' => $this->evaluation])->one();
                if($ev){
                    $this->evaluation_value = $ev->evaluation_value;
                }
            }

            $this->position_structure_id = explode('-', $this->position_structure_id)[0];

            if(($this->hour || $this->minutes) && !$child) $this->duration = $this->getDuration();

            if($this->parent_id || !$child){
                if($this->progress > 0 && $this->progress < 100) {
                    $this->status_id  =Status::ACT_ON_PROGRESS;
                } else if($this->progress == 0) {
                    $this->status_id  =Status::ACT_NEW;
                } else if($this->progress == 100) {
                    $this->status_id  =Status::ACT_DONE;
                }
            }

            if($this->status_id == Status::ACT_DONE){
                $this->finish_date = date('Y-m-d');
            } else {
                $this->finish_date = null;
            }

            return true;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(($insert) || ($this->hour != $changedAttributes['hour'] || $this->minutes != $changedAttributes['minutes'])){
            if($this->parent_id){
                $totalDuration = Yii::$app->db->createCommand("
                                                SELECT 
                                                    SUM(duration) 
                                                FROM 
                                                    management.activities
                                                WHERE
                                                    parent_id = {$this->parent_id}
                                            ")->queryScalar();

                $parent = self::findOne($this->parent_id);
                $parent->duration = $totalDuration;
                $parent->save(false);
            }
        }

        /* if(($insert) || ($this->weight != $changedAttributes['weight']) || ($this->status_id != $changedAttributes['status_id'])){
            if($this->parent_id){
                $cnt = Activities::find()->where(['parent_id' => $this->parent_id])->andWhere(['status_id' => Status::ACT_DONE])->count();
                $totalWeight = Yii::$app->db->createCommand("
                                                SELECT 
                                                    SUM(weight) 
                                                FROM 
                                                    management.activities
                                                WHERE
                                                    parent_id = {$this->parent_id}
                                            ")->queryScalar();

                // $child = Activities::find()->where(['parent_id' => $this->parent_id])->


                $parent = self::findOne($this->parent_id);
                if($cnt > 0){
                    $parent->progress = ($this->weight/$totalWeight)*100;
                } else {
                    $parent->progress = null;
                }
                $parent->save(false);
            }
        } */

        // if($this->parent_id){
        // if($this->status_id != $changedAttributes['status_id']){
            if($this->parent_id){
                $statusNew = self::find()->where(['parent_id' => $this->parent_id])->andWhere(['status_id' => Status::ACT_NEW])->one();
                $statusProgress = self::find()->where(['parent_id' => $this->parent_id])->andWhere(['status_id' => Status::ACT_ON_PROGRESS])->one();
                $statusDone = self::find()->where(['parent_id' => $this->parent_id])->andWhere(['status_id' => Status::ACT_DONE])->one();
                $statusCancel = self::find()->where(['parent_id' => $this->parent_id])->andWhere(['status_id' => Status::ACT_CANCEL])->one();

                $parent = self::findOne($this->parent_id);

                if(!$statusNew && !$statusProgress){
                    $parent->status_id = Status::ACT_DONE;
                } else if(($statusProgress && $statusDone) || ($statusNew && $statusDone) || $statusProgress){
                    $parent->status_id = Status::ACT_ON_PROGRESS;
                } else if(!$statusNew && !$statusProgress && !$statusDone && $statusCancel){
                    $parent->status_id = Status::ACT_CANCEL;
                } else  {
                    $parent->status_id = Status::ACT_NEW;
                }
                $parent->save(false);
            }
        // }
        
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        if($this->parent_id){
            $totalDuration = Yii::$app->db->createCommand("
                                            SELECT 
                                                SUM(duration) 
                                            FROM 
                                                management.activities
                                            WHERE
                                                parent_id = {$this->parent_id}
                                        ")->queryScalar();

            $statusNew = self::find()->where(['parent_id' => $this->parent_id])->andWhere(['status_id' => Status::ACT_NEW])->one();
            $statusProgress = self::find()->where(['parent_id' => $this->parent_id])->andWhere(['status_id' => Status::ACT_ON_PROGRESS])->one();
            $statusDone = self::find()->where(['parent_id' => $this->parent_id])->andWhere(['status_id' => Status::ACT_DONE])->one();
            $statusCancel = self::find()->where(['parent_id' => $this->parent_id])->andWhere(['status_id' => Status::ACT_CANCEL])->one();

            $parent = self::findOne($this->parent_id);

            if(!$statusNew && !$statusProgress){
                $status = Status::ACT_DONE;
            } else if(($statusProgress && $statusDone) || ($statusNew && $statusDone) || $statusProgress){
                $status = Status::ACT_ON_PROGRESS;
            } else if(!$statusNew && !$statusProgress && !$statusDone && $statusCancel){
                $status = Status::ACT_CANCEL;
            } else  {
                $status = Status::ACT_NEW;
            }
                                        
            $parent = self::findOne($this->parent_id);
            $parent->duration = $totalDuration;
            $parent->status_id = $status;
            $parent->save(false);
        }
    }

    protected function getDuration(){
        $hour = $this->hour;
        $minutes = $this->minutes ? ($this->minutes / 60) : 0;

        return round($hour + $minutes, 2);
    }

    //RELATION
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    public function getPic()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'pic_id']);
    }

    public function getPositionStructure()
    {
        return $this->hasOne(\app\models\PositionStructure::className(), ['id' => 'position_structure_id']);
    }

    public function getSupportedUnit()
    {
        return $this->hasOne(\app\models\UnitStructure::className(), ['id' => 'supported_unit_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(\app\models\ActivityCategory::className(), ['id' => 'activity_category_id']);
    }

    public function getOkr()
    {
        return $this->hasOne(\app\models\Okr::className(), ['id' => 'okr_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(\app\models\Status::className(), ['id' => 'status_id']);
    }
}
