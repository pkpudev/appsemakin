<?php

namespace app\models;

use Yii;
use \app\models\base\InitiativesStakeholder as BaseInitiativesStakeholder;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.initiatives__stakeholder".
 */
class InitiativesStakeholder extends BaseInitiativesStakeholder
{

    
    public function getStakeholder()
    {
        return $this->hasOne(\app\models\Stakeholder::className(), ['id' => 'stakeholder_id']);
    }
}
