<?php

namespace app\models;

use Yii;
use \app\models\base\TargetType as BaseTargetType;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.target_type".
 */
class TargetType extends BaseTargetType
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
