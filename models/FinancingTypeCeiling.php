<?php

namespace app\models;

use Yii;
use \app\models\base\FinancingTypeCeiling as BaseFinancingTypeCeiling;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.financing_type_ceiling".
 */
class FinancingTypeCeiling extends BaseFinancingTypeCeiling
{

    public $year;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['year'], 'safe']
            ]
        );
    }

    public function getUnit()
    {
        return $this->hasOne(\app\models\UnitStructure::className(), ['id' => 'unit_structure_id']);
    }

    public function getFinancingType()
    {
        return $this->hasOne(\app\models\FinancingType::className(), ['id' => 'financing_type_id']);
    }

    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
