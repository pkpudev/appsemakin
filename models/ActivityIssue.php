<?php

namespace app\models;

use Yii;
use \app\models\base\ActivityIssue as BaseActivityIssue;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.activity_issue".
 */
class ActivityIssue extends BaseActivityIssue
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    
    public function getActivities()
    {
        return $this->hasOne(\app\models\Activities::className(), ['id' => 'activities_id']);
    }
}
