<?php

namespace app\models;

use Yii;
use \app\models\base\SdmPositionLevel as BaseSdmPositionLevel;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "public.sdm_position_level".
 */
class SdmPositionLevel extends BaseSdmPositionLevel
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
