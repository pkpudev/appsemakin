<?php

namespace app\models;

use Yii;
use \app\models\base\Weaknesses as BaseWeaknesses;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.weaknesses".
 */
class Weaknesses extends BaseWeaknesses
{

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            $this->weakness = ucfirst($this->weakness);

            return true;
        }
    }


    public function getVariable()
    {
        return $this->hasOne(\app\models\InternalVariable::className(), ['id' => 'internal_variable_id']);
    }
    
    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
