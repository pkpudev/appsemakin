<?php

namespace app\models;

use Yii;
use \app\models\base\OkrEvaluationHistory as BaseOkrEvaluationHistory;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.okr_evaluation_history".
 */
class OkrEvaluationHistory extends BaseOkrEvaluationHistory
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
