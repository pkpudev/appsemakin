<?php

namespace app\models;

use Yii;
use \app\models\base\Customers as BaseCustomers;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.customers".
 */
class Customers extends BaseCustomers
{

    const INTERNAL  = 1; 
    const EXTERNAL  = 2; 

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }
    
    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
