<?php

namespace app\models;

use Yii;
use \app\models\base\ActivityCategory as BaseActivityCategory;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.activity_category".
 */
class ActivityCategory extends BaseActivityCategory
{

    const REGULER           = 1;
    const CROSSFUNCTION     = 2;
    const VOLUNTARY         = 3;
    const EXPERT_LOCATOR    = 4;
    const PROJECT           = 5;
    const PKP               = 6;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
