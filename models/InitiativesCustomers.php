<?php

namespace app\models;

use Yii;
use \app\models\base\InitiativesCustomers as BaseInitiativesCustomers;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.initiatives__customers".
 */
class InitiativesCustomers extends BaseInitiativesCustomers
{

    
    public function getCustomer()
    {
        return $this->hasOne(\app\models\Customers::className(), ['id' => 'customers_id']);
    }
}
