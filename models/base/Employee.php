<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base-model class for table "public.sdm_employee".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $passwd
 * @property string $email
 * @property string $full_name
 * @property integer $user_status
 * @property string $_history
 * @property integer $phone_id
 * @property string $register_date
 * @property integer $branch_id
 * @property string $guid
 * @property integer $is_new_task
 * @property string $activkey
 * @property integer $parent_alias_id
 * @property integer $company_id
 * @property string $auth_key
 * @property integer $expired_key
 * @property integer $deleted_by
 * @property string $deleted_stamp
 * @property string $donor_no
 * @property string $address
 * @property string $postal_code
 * @property integer $location_id
 * @property string $npwp_no
 * @property string $npwz_no
 * @property integer $is_donor
 * @property integer $country_id
 * @property integer $fax_id
 * @property integer $bank_account_id
 * @property integer $marketer_id
 * @property integer $donor_type_id
 * @property string $text_searchable
 * @property string $last_login
 * @property integer $is_valid
 * @property string $donor_no_old
 * @property string $website
 * @property integer $phone_mark
 * @property integer $mail_mark
 * @property string $updated_validity
 * @property string $note
 * @property string $alias_name
 * @property string $created_stamp
 * @property string $updated_stamp
 * @property integer $is_new
 * @property integer $has_idcard
 * @property integer $remainder_date
 * @property string $remainder_message
 * @property string $address_full
 * @property string $phone_no_full
 * @property string $blood_type
 * @property string $data_source
 * @property integer $merged_to_id
 * @property integer $donor_level_id
 * @property integer $donor_class_id
 * @property integer $donor_company_id
 * @property string $birth_place
 * @property string $birth_date
 * @property integer $sex
 * @property string $starting_date
 * @property string $ending_date
 * @property integer $is_employee
 * @property integer $position_id
 * @property integer $is_marketer
 * @property integer $employee_type_id
 * @property string $permanent_address
 * @property string $current_phone_no
 * @property string $permanent_phone_no
 * @property string $identity_no
 * @property integer $marital_status
 * @property string $user_image
 * @property integer $branch_id_financial
 * @property integer $is_external_orphan_admin
 * @property string $nip
 * @property string $job
 * @property string $private_email
 * @property integer $workplace_id
 * @property string $religion
 * @property integer $is_qurban_reguler
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $aliasModel
 */
abstract class Employee extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'public.sdm_employee';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_status', 'phone_id', 'branch_id', 'is_new_task', 'parent_alias_id', 'company_id', 'expired_key', 'deleted_by', 'location_id', 'is_donor', 'country_id', 'fax_id', 'bank_account_id', 'marketer_id', 'donor_type_id', 'is_valid', 'phone_mark', 'mail_mark', 'is_new', 'has_idcard', 'remainder_date', 'merged_to_id', 'donor_level_id', 'donor_class_id', 'donor_company_id', 'sex', 'is_employee', 'position_id', 'is_marketer', 'employee_type_id', 'marital_status', 'branch_id_financial', 'is_external_orphan_admin', 'workplace_id', 'is_qurban_reguler'], 'default', 'value' => null],
            [['user_status', 'phone_id', 'branch_id', 'is_new_task', 'parent_alias_id', 'company_id', 'expired_key', 'deleted_by', 'location_id', 'is_donor', 'country_id', 'fax_id', 'bank_account_id', 'marketer_id', 'donor_type_id', 'is_valid', 'phone_mark', 'mail_mark', 'is_new', 'has_idcard', 'remainder_date', 'merged_to_id', 'donor_level_id', 'donor_class_id', 'donor_company_id', 'sex', 'is_employee', 'position_id', 'is_marketer', 'employee_type_id', 'marital_status', 'branch_id_financial', 'is_external_orphan_admin', 'workplace_id', 'is_qurban_reguler'], 'integer'],
            [['_history', 'address', 'text_searchable', 'note', 'address_full', 'phone_no_full', 'permanent_address'], 'string'],
            [['register_date', 'deleted_stamp', 'last_login', 'updated_validity', 'created_stamp', 'updated_stamp', 'birth_date', 'starting_date', 'ending_date'], 'safe'],
            [['user_name', 'guid', 'activkey', 'auth_key', 'birth_place'], 'string', 'max' => 40],
            [['passwd', 'email', 'data_source', 'user_image', 'private_email'], 'string', 'max' => 100],
            [['full_name'], 'string', 'max' => 150],
            [['donor_no'], 'string', 'max' => 20],
            [['postal_code'], 'string', 'max' => 10],
            [['npwp_no', 'npwz_no'], 'string', 'max' => 24],
            [['donor_no_old', 'current_phone_no', 'permanent_phone_no'], 'string', 'max' => 15],
            [['website', 'religion'], 'string', 'max' => 50],
            [['alias_name', 'identity_no'], 'string', 'max' => 30],
            [['remainder_message'], 'string', 'max' => 160],
            [['blood_type'], 'string', 'max' => 2],
            [['nip'], 'string', 'max' => 12],
            [['job'], 'string', 'max' => 255],
            [['donor_no'], 'unique'],
            [['email'], 'unique'],
            [['guid'], 'unique'],
            [['nip'], 'unique'],
            [['user_name'], 'unique'],
            [['branch_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Branch::className(), 'targetAttribute' => ['branch_id' => 'id']],
            [['branch_id_financial'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Branch::className(), 'targetAttribute' => ['branch_id_financial' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            // [['employee_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\SdmEmployeeType::className(), 'targetAttribute' => ['employee_type_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_name' => 'User Name',
            'passwd' => 'Passwd',
            'email' => 'Email',
            'full_name' => 'Full Name',
            'user_status' => 'User Status',
            '_history' => 'History',
            'phone_id' => 'Phone ID',
            'register_date' => 'Register Date',
            'branch_id' => 'Branch ID',
            'guid' => 'Guid',
            'is_new_task' => 'Is New Task',
            'activkey' => 'Activkey',
            'parent_alias_id' => 'Parent Alias ID',
            'company_id' => 'Company ID',
            'auth_key' => 'Auth Key',
            'expired_key' => 'Expired Key',
            'deleted_by' => 'Deleted By',
            'deleted_stamp' => 'Deleted Stamp',
            'donor_no' => 'Donor No',
            'address' => 'Address',
            'postal_code' => 'Postal Code',
            'location_id' => 'Location ID',
            'npwp_no' => 'Npwp No',
            'npwz_no' => 'Npwz No',
            'is_donor' => 'Is Donor',
            'country_id' => 'Country ID',
            'fax_id' => 'Fax ID',
            'bank_account_id' => 'Bank Account ID',
            'marketer_id' => 'Marketer ID',
            'donor_type_id' => 'Donor Type ID',
            'text_searchable' => 'Text Searchable',
            'last_login' => 'Last Login',
            'is_valid' => 'Is Valid',
            'donor_no_old' => 'Donor No Old',
            'website' => 'Website',
            'phone_mark' => 'Phone Mark',
            'mail_mark' => 'Mail Mark',
            'updated_validity' => 'Updated Validity',
            'note' => 'Note',
            'alias_name' => 'Alias Name',
            'created_by' => 'Created By',
            'created_stamp' => 'Created Stamp',
            'updated_by' => 'Updated By',
            'updated_stamp' => 'Updated Stamp',
            'is_new' => 'Is New',
            'has_idcard' => 'Has Idcard',
            'remainder_date' => 'Remainder Date',
            'remainder_message' => 'Remainder Message',
            'address_full' => 'Address Full',
            'phone_no_full' => 'Phone No Full',
            'blood_type' => 'Blood Type',
            'data_source' => 'Data Source',
            'merged_to_id' => 'Merged To ID',
            'donor_level_id' => 'Donor Level ID',
            'donor_class_id' => 'Donor Class ID',
            'donor_company_id' => 'Donor Company ID',
            'birth_place' => 'Birth Place',
            'birth_date' => 'Birth Date',
            'sex' => 'Sex',
            'starting_date' => 'Starting Date',
            'ending_date' => 'Ending Date',
            'is_employee' => 'Is Employee',
            'position_id' => 'Position ID',
            'is_marketer' => 'Is Marketer',
            'employee_type_id' => 'Employee Type ID',
            'permanent_address' => 'Permanent Address',
            'current_phone_no' => 'Current Phone No',
            'permanent_phone_no' => 'Permanent Phone No',
            'identity_no' => 'Identity No',
            'marital_status' => 'Marital Status',
            'user_image' => 'User Image',
            'branch_id_financial' => 'Branch Id Financial',
            'is_external_orphan_admin' => 'dipakai DATABASEYATIM',
            'nip' => 'Nip',
            'job' => 'Job',
            'private_email' => 'Level',
            'workplace_id' => 'Tempat Bekerja',
            'religion' => 'Religion',
            'is_qurban_reguler' => 'Is Qurban Reguler',
        ];
    }




}
