<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base-model class for table "management.portfolio_canvas".
 *
 * @property integer $id
 * @property integer $portfolio_id
 * @property integer $element_id
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $aliasModel
 */
abstract class PortfolioCanvas extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'management.portfolio_canvas';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['portfolio_id', 'element_id'], 'required'],
            [['portfolio_id', 'element_id'], 'default', 'value' => null],
            [['portfolio_id', 'element_id'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['portfolio_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Portfolio::className(), 'targetAttribute' => ['portfolio_id' => 'id']],
            [['element_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\PortfolioElement::className(), 'targetAttribute' => ['element_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'portfolio_id' => 'Portofolio',
            'element_id' => 'Elemen',
            'description' => 'Deskripsi',
            'created_at' => 'Dibuat Pada',
            'created_by' => 'Dibuat Oleh',
            'updated_at' => 'Diubah Pada',
            'updated_by' => 'Diubah Oleh',
        ];
    }




}
