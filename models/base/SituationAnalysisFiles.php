<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "management.situation_analysis__files".
 *
 * @property integer $id
 * @property integer $situation_analysis_id
 * @property integer $files_id
 * @property string $aliasModel
 */
abstract class SituationAnalysisFiles extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'management.situation_analysis__files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['situation_analysis_id', 'files_id'], 'default', 'value' => null],
            [['situation_analysis_id', 'files_id'], 'integer'],
            [['files_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Files::className(), 'targetAttribute' => ['files_id' => 'id']],
            [['situation_analysis_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\SituationAnalysis::className(), 'targetAttribute' => ['situation_analysis_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'situation_analysis_id' => 'Analisis Situasi',
            'files_id' => 'Lampiran',
        ];
    }




}
