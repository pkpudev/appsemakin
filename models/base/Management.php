<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base-model class for table "management.management".
 *
 * @property integer $id
 * @property string $management_name
 * @property integer $period_start
 * @property integer $period_end
 * @property string $created_at
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $created_by
 * @property string $aliasModel
 */
abstract class Management extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'management.management';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period_start', 'period_end', 'updated_by'], 'default', 'value' => null],
            [['period_start', 'period_end', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['management_name'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'management_name' => 'Kepengurusan',
            'period_start' => 'Dari Periode',
            'period_end' => 'Sampai Periode',
            'created_at' => 'Dibuat Pada',
            'created_by' => 'Dibuat Oleh',
            'updated_at' => 'Diubah Pada',
            'updated_by' => 'Diubah Oleh',
        ];
    }




}
