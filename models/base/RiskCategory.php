<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "management.risk_category".
 *
 * @property integer $id
 * @property integer $risk_category
 * @property integer $description
 * @property boolean $is_active
 * @property string $aliasModel
 */
abstract class RiskCategory extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'management.risk_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['risk_category', 'description'], 'default', 'value' => null],
            [['risk_category', 'description'], 'safe'],
            [['is_active'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'risk_category' => 'Kategori Risiko',
            'description' => 'Deskripsi',
            'is_active' => 'Aktif?',
        ];
    }




}
