<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base-model class for table "management.unit_structure".
 *
 * @property integer $id
 * @property integer $year
 * @property integer $unit_id
 * @property string $unit_name
 * @property string $level
 * @property integer $superior_unit_id
 * @property string $unit_code
 * @property string $created_at
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $created_by
 * @property integer $is_value_stream
 * @property string $aliasModel
 */
abstract class UnitStructure extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'management.unit_structure';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'unit_id', 'unit_name', 'level', 'unit_code'], 'required'],
            [['year', 'unit_id', 'superior_unit_id', 'updated_by'], 'default', 'value' => null],
            [['year', 'unit_id', 'superior_unit_id', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['is_value_stream'], 'boolean'],
            [['unit_name', 'level'], 'string', 'max' => 250],
            [['unit_code', 'initiative_code'], 'string', 'max' => 25],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Departments::className(), 'targetAttribute' => ['unit_id' => 'deptid']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'year' => 'Tahun',
            'unit_id' => 'Nama Unit Sebelumnya',
            'unit_name' => 'Nama Unit',
            'level' => 'Level',
            'superior_unit_id' => 'Unit Atasan',
            'unit_code' => 'Kode Unit',
            'created_at' => 'Dibuat Pada',
            'created_by' => 'Dibuat Oleh',
            'updated_at' => 'Diubah Pada',
            'updated_by' => 'Diubah Oleh',
            'initiative_code' => 'Inisial Unit',
            'is_value_stream' => 'Value Stream?',
        ];
    }




}
