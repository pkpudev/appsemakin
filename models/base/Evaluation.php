<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base-model class for table "management.evaluation".
 *
 * @property integer $id
 * @property string $period
 * @property integer $year
 * @property string $start_date
 * @property string $end_date
 * @property integer $status_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property boolean $use_old_pattern
 * @property string $aliasModel
 */
abstract class Evaluation extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'management.evaluation';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period'], 'required'],
            [['use_old_pattern'], 'boolean'],
            [['year', 'status_id'], 'default', 'value' => null],
            [['year', 'status_id'], 'integer'],
            [['start_date', 'end_date', 'created_at', 'updated_at'], 'safe'],
            [['period'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'period' => 'Periode',
            'year' => 'Tahun',
            'start_date' => 'Tanggal Mulai',
            'end_date' => 'Tanggal Akhir',
            'status_id' => 'Status',
            'created_at' => 'Dibuat Pada',
            'created_by' => 'Dibuat Oleh',
            'updated_at' => 'Diubah Pada',
            'updated_by' => 'Diubah Oleh',
        ];
    }




}
