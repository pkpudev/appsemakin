<?php

namespace app\models;

use Yii;
use \app\models\base\Calculation as BaseCalculation;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.calculation".
 */
class Calculation extends BaseCalculation
{

    const AVERAGE       = 1;
    const SUM           = 2;
    const LAST          = 3;
    const MIN           = 4;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
