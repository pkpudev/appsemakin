<?php

namespace app\models;

use Yii;
use \app\models\base\OkrOrg as BaseOkrOrg;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.okr_org".
 */
class OkrOrg extends BaseOkrOrg
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->version = 1;      
                if($this->isNewRecord && !$this->okr_code) $this->okr_code = $this->generateCode();          
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }

    protected function generateCode(){
        if($this->type == 'O'){
            $countObjective = self::find()->where(['start_year' => $this->start_year])->count();
            $newno = str_pad($countObjective + 1, 2, "0", STR_PAD_LEFT);
        } else {
            $parent = self::findOne($this->parent_id);
            $countKr = self::find()->where(['parent_id' => $this->parent_id])->count();
            $newno = $parent->okr_code . '-' . str_pad($countKr + 1, 2, "0", STR_PAD_LEFT);
        }

        return $newno;
    }

    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    public function getPerspective()
    {
        return $this->hasOne(Perspective::className(), ['id' => 'perspective_id']);
    }

    public function getPortfolio()
    {
        return $this->hasOne(PortfolioType::className(), ['id' => 'portfolio_type_id']);
    }

    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
