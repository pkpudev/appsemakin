<?php

namespace app\models;

use Yii;
use \app\models\base\Vision as BaseVision;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.vision".
 */
class Vision extends BaseVision
{

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }


    public function getManagement()
    {
        return $this->hasOne(\app\models\Management::className(), ['id' => 'management_id']);
    }

    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
