<?php

namespace app\models;

use Yii;
use \app\models\base\Employee as BaseEmployee;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "public.sdm_employee".
 */
class Employee extends BaseEmployee
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    public function getBranch()
    {
        return $this->hasOne(\app\models\Branch::className(), ['id' => 'branch_id']);
    }

    public function getType()
    {
        return $this->hasOne(\app\models\SdmEmployeeType::className(), ['id' => 'employee_type_id']);
    }
}
