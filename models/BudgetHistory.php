<?php

namespace app\models;

use Yii;
use \app\models\base\BudgetHistory as BaseBudgetHistory;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.budget_history".
 */
class BudgetHistory extends BaseBudgetHistory
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    public function getBudget()
    {
        return $this->hasOne(\app\models\Budget::className(), ['id' => 'budget_id']);
    }

    public function getBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'by']);
    }
}
