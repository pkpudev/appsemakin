<?php

namespace app\models;

use Yii;
use \app\models\base\PositionStructure as BasePositionStructure;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.position_structure".
 */
class PositionStructure extends BasePositionStructure
{

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }

    public function getDirectorateId(){
        if($this->unit->level == 1){
            return $this->unit->id;
        } else if($this->unit->level == 2){
            return $this->unit->sup->id;
        } else if($this->unit->level == 3){
            return $this->unit->sup->sup->id;
        } else if($this->unit->level == 5){
            return $this->unit->sup->sup->id;
        }
    }

    public function getDirectorate(){
        if($this->unit->level == 1){
            return $this->unit->unit_name;
        } else if($this->unit->level == 2){
            return $this->unit->sup->unit_name;
        } else if($this->unit->level == 3){
            return $this->unit->sup->sup->unit_name;
        } else if($this->unit->level == 5){
            return $this->unit->sup->sup->unit_name;
        }
    }

    public function getDepartment(){
        if($this->unit->level == 1){
            return '-';
        } else if($this->unit->level == 2){
            return $this->unit->unit_name;
        } else if($this->unit->level == 3){
            return $this->unit->sup->unit_name;
        } else if($this->unit->level == 5){
            return $this->unit->sup->unit_name;
        }
    }

    public function getDivision(){
        if($this->unit->level == 1){
            return '-';
        } else if($this->unit->level == 2){
            return '-';
        } else if($this->unit->level == 3){
            return $this->unit->unit_name;
        } else if($this->unit->level == 5){
            return '-';
        }
    }

    public function getSquad(){
        if($this->unit->level == 1){
            return '-';
        } else if($this->unit->level == 2){
            return '-';
        } else if($this->unit->level == 3){
            return '-';
        } else if($this->unit->level == 5){
            return $this->unit->unit_name;
        }
    }

    public function getLevel0()
    {
        return $this->hasOne(SdmPositionLevel::className(), ['id' => 'level']);
    }

    public function getResp()
    {
        return $this->hasOne(self::className(), ['id' => 'responsible_to_id']);
    }

    public function getUnit()
    {
        return $this->hasOne(UnitStructure::className(), ['id' => 'unit_structure_id']);
    }
    
    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
