<?php

namespace app\models;

use Yii;
use \app\models\base\Tutorial as BaseTutorial;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.tutorial".
 */
class Tutorial extends BaseTutorial
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
