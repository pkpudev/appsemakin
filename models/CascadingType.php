<?php

namespace app\models;

use Yii;
use \app\models\base\CascadingType as BaseCascadingType;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.cascading_type".
 */
class CascadingType extends BaseCascadingType
{

    const FULL      = 1;
    const PARTIAL   = 2;
    const SUPPORT   = 3;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
