<?php

namespace app\models;

use Yii;
use \app\models\base\ActivityMov as BaseActivityMov;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.activity_mov".
 */
class ActivityMov extends BaseActivityMov
{

    public $files;
    public $links;
    public $storages;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['files', 'links', 'storages'], 'safe']
            ]
        );
    }

    public function getActivity()
    {
        return $this->hasOne(\app\models\Activity::className(), ['id' => 'activity_id']);
    }
    
    public function getFile()
    {
        return $this->hasOne(\app\models\Files::className(), ['id' => 'files_id']);
    }
}
