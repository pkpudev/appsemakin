<?php

namespace app\models;

use Yii;
use \app\models\base\ActivityEvaluation as BaseActivityEvaluation;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.activity_evaluation".
 */
class ActivityEvaluation extends BaseActivityEvaluation
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    /** RELATIONS */
    public function getEvaluation()
    {
        return $this->hasOne(\app\models\Evaluation::className(), ['id' => 'evaluation_id']);
    }

    public function getActivity()
    {
        return $this->hasOne(\app\models\Activity::className(), ['id' => 'activity_id']);
    }

}
