<?php

namespace app\models;

use Yii;
use \app\models\base\Activity as BaseActivity;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.activity".
 */
class Activity extends BaseActivity
{
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['activity_name', 'start_plan', 'finish_plan', 'pic_id', 'documentation', 'difficulty_level', 'resources_level', 'is_mov', 'dissatisfaction_level', 'resources'], 'required'];

        return $rules;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'initiatives_id' => 'Program Kerja',
            'activity_code' => 'Kode Kegiatan',
            'activity_name' => 'Kegiatan',
            'start_plan' => 'Tanggal Mulai',
            'finish_plan' => 'Tanggal Selesai',
            'duration' => 'Durasi',
            'start_actual' => 'Tanggal Mulai',
            'finish_actual' => 'Tanggal Selesai',
            'duration_actual' => 'Durasi',
            'position_structure_id' => 'Jabatan',
            'pic_id' => 'Ditugaskan Kepada',
            'status_id' => 'Status',
            'documentation' => 'Alat Verifikasi',
            'difficulty_level' => 'Tingkat Kesulitan / Kendali',
            'resources_level' => 'Sumber Daya yg Dibutuhkan',
            'is_mov' => 'Dokumen Keluaran/Mov',
            'dissatisfaction_level' => 'Pengaruh terhadap ketidakpuasan pelanggan',
            'weight_value' => 'Nilai Bobot',
            'weight' => 'Bobot',
            'resources' => 'Sumber Daya',
            'note' => 'Catatan',
            'actual_value' => 'Nilai Realisasi Kegiatan (%)',
            'actual_note' => 'Catatan Realisasi',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->status_id = \app\models\Status::DRAFT;
                if (empty($this->activity_code))
                    $this->activity_code = $this->genActivityCode();

                    if(!$this->evaluation_status_id) $this->evaluation_status_id = Status::OKR_E_DRAFT;
            }

            $this->weight_value = $this->calculateWeightValue();
            $this->activity_name = ucfirst($this->activity_name);

            $unitStructureId = Initiatives::findOne($this->initiatives_id)->okr->position->unit_structure_id;
            $pse = PositionStructureEmployee::find()->joinWith(['position p'])->where(['p.unit_structure_id' => $unitStructureId, 'employee_id'=>$this->pic_id])->one();
            $this->position_structure_id = $pse->position_structure_id;
            
            return true;
        }
    }

    public function genActivityCode()
    {
        // kode Initiatives
        $initiatives = Initiatives::findOne($this->initiatives_id);
        $code = $initiatives->initiatives_code;
        // no urut
        $count = self::find()->where(['initiatives_id' => $this->initiatives_id])->count();
                    
        $next_no = str_pad(((int) $count) + 1, 2, "0", STR_PAD_LEFT);

        return "$code.$next_no";
    }

    public function calculateWeightValue()
    {
        return $weight_value = $this->difficulty_level + $this->resources_level + $this->is_mov + $this->dissatisfaction_level;
    }

    public function getInitiative()
    {
        return $this->hasOne(\app\models\Initiatives::className(), ['id' => 'initiatives_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(\app\models\PositionStructure::className(), ['id' => 'position_structure_id']);
    }

    public function getPic()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'pic_id']);
    }

    public function getOkr()
    {
        return $this->hasOne(\app\models\Okr::className(), ['id' => 'okr_id']);
    }

    public function getBudgets()
    {
        return $this->hasMany(\app\models\Budget::className(), ['activity_id' => 'id']);
    }

    public function getActivityHistories()
    {
        return $this->hasMany(\app\models\ActivityHistory::className(), ['activity_id' => 'id']);
    }
}
