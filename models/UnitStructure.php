<?php

namespace app\models;

use Yii;
use \app\models\base\UnitStructure as BaseUnitStructure;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.unit_structure".
 */
class UnitStructure extends BaseUnitStructure
{

    public $branch_id;
    public $clone_from;
    public $clone_to;
    public $countPerson;
    public $fte;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['branch_id', 'clone_from', 'clone_to'], 'integer']
            ]
        );
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }

    public function getDirectorateName(){
        if($this->level == 1){
            return $this->unit_name;
        } else if($this->level == 2){
            return $this->sup->unit_name;
        } else if($this->level == 3){
            return $this->sup->sup->unit_name;
        } else if($this->level == 5){
            return $this->sup->sup->unit_name;
        }
    }

    public function getDirectorateCode(){
        if($this->level == 1){
            return $this->unit_code;
        } else if($this->level == 2){
            return $this->sup->unit_code;
        } else if($this->level == 3){
            return $this->sup->sup->unit_code;
        } else if($this->level == 5){
            return $this->sup->sup->unit_code;
        }
    }

    public function getDepartmentName(){
        if($this->level == 1){
            return '-';
        } else if($this->level == 2){
            return $this->unit_name;
        } else if($this->level == 3){
            return $this->sup->unit_name;
        } else if($this->level == 5){
            return $this->sup->unit_name;
        }
    }

    public function getDepartmentCode(){
        if($this->level == 1){
            return '-';
        } else if($this->level == 2){
            return $this->unit_code;
        } else if($this->level == 3){
            return $this->sup->unit_code;
        } else if($this->level == 5){
            return $this->sup->unit_code;
        }
    }

    public function getSup()
    {
        return $this->hasOne(self::className(), ['id' => 'superior_unit_id']);
    }

    public function getDept()
    {
        return $this->hasOne(Departments::className(), ['deptid' => 'unit_id']);
    }
    
    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
