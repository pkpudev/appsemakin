<?php

namespace app\models;

use Yii;
use \app\models\base\InitiativesHistory as BaseInitiativesHistory;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.initiatives_history".
 */
class InitiativesHistory extends BaseInitiativesHistory
{

    public function getInitiatives()
    {
        return $this->hasOne(\app\models\Initiatives::className(), ['id' => 'initiatives_id']);
    }

    public function getBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'by']);
    }
}
