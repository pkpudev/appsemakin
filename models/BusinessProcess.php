<?php

namespace app\models;

use Yii;
use \app\models\base\BusinessProcess as BaseBusinessProcess;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.business_process".
 */
class BusinessProcess extends BaseBusinessProcess
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
