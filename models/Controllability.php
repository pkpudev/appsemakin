<?php

namespace app\models;

use Yii;
use \app\models\base\Controllability as BaseControllability;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.controllability".
 */
class Controllability extends BaseControllability
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
