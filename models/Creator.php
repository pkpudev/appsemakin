<?php

namespace app\models;

use Yii;
use \app\models\base\Creator as BaseCreator;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bsc.creator".
 */
class Creator extends BaseCreator
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }


    public function getDept()
    {
        return $this->hasOne(\app\models\Departments::className(), ['deptid' => 'departments_id']);
    }
}
