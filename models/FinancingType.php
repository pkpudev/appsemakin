<?php

namespace app\models;

use Yii;
use \app\models\base\FinancingType as BaseFinancingType;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.financing_type".
 */
class FinancingType extends BaseFinancingType
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
