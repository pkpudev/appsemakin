<?php

namespace app\models;

use Yii;
use \app\models\base\ActivitiesFiles as BaseActivitiesFiles;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.activities__files".
 */
class ActivitiesFiles extends BaseActivitiesFiles
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
    
    public function getFile()
    {
        return $this->hasOne(\app\models\Files::className(), ['id' => 'files_id']);
    }
}
