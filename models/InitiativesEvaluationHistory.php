<?php

namespace app\models;

use Yii;
use \app\models\base\InitiativesEvaluationHistory as BaseInitiativesEvaluationHistory;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.initiatives_evaluation_history".
 */
class InitiativesEvaluationHistory extends BaseInitiativesEvaluationHistory
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
