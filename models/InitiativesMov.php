<?php

namespace app\models;

use Yii;
use \app\models\base\InitiativesMov as BaseInitiativesMov;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.initiatives_mov".
 */
class InitiativesMov extends BaseInitiativesMov
{

    public $files;
    public $links;
    public $storages;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['files', 'links', 'storages'], 'safe']
            ]
        );
    }

    public function getInitiativesEvaluation()
    {
        return $this->hasOne(\app\models\InitiativesEvaluation::className(), ['id' => 'initiatives_evaluation_id']);
    }
    
    public function getFile()
    {
        return $this->hasOne(\app\models\Files::className(), ['id' => 'files_id']);
    }
}
