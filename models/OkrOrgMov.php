<?php

namespace app\models;

use Yii;
use \app\models\base\OkrOrgMov as BaseOkrOrgMov;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.okr_org_mov".
 */
class OkrOrgMov extends BaseOkrOrgMov
{
    
    public $files;
    public $links;
    public $storages;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['files', 'links', 'storages'], 'safe']
            ]
        );
    }

    public function getMovType()
    {
        return $this->hasOne(\app\models\OkrMovType::className(), ['id' => 'okr_mov_type_id']);
    }

    public function getFiles0()
    {
        return $this->hasOne(\app\models\Files::className(), ['id' => 'files_id']);
    }
}
