<?php

namespace app\models;

use Yii;
use \app\models\base\SdmPosition as BaseSdmPosition;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "public.sdm_position".
 */
class SdmPosition extends BaseSdmPosition
{

    public function getLevel0()
    {
        return $this->hasOne(\app\models\SdmPositionLevel::className(), ['id' => 'level']);
    }

    public function getDepartments()
    {
        return $this->hasOne(\app\models\Departments::className(), ['deptid' => 'division_id']);
    }
}
