<?php

namespace app\models;

use Yii;
use \app\models\base\PortfolioCanvas as BasePortfolioCanvas;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.portfolio_canvas".
 */
class PortfolioCanvas extends BasePortfolioCanvas
{

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }


    public function getPortfolio()
    {
        return $this->hasOne(\app\models\Portfolio::className(), ['id' => 'portfolio_id']);
    }

    public function getElement()
    {
        return $this->hasOne(\app\models\PortfolioElement::className(), ['id' => 'element_id']);
    }
    
    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
