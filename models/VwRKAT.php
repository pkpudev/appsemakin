<?php

namespace app\models;

use Yii;
use \app\models\base\VwRKAT as BaseVwRKAT;

/**
 * This is the model class for table "bsc.vw_rkat".
 */
class VwRKAT extends BaseVwRKAT
{
    const APPROVED = 4;
    
    public static function primaryKey()
    {
        return ['budget_id'];
    }

    public function attributeLabels()
    {
        return [
            'year' => 'Tahun Anggaran',
            'departments_id' => 'Divisi',
            'budget_code' => 'Kode Anggaran',
            'workplan_name' => 'Nama Proker',
            'account_name' => 'Nama Akun',
            'activity_name' => 'Aktivitas',
        ];
    }

    public function getText()
    {
        return $this->budget_code . ' - ' . $this->workplan_name;
    }

    public function getFormatted_balance()
    {
        return $this->getCurrency('balance');
    }

    public function getCurrency($attribute)
    {
        if (!$this->hasAttribute($attribute)) {
            return;
        }
        return 'Rp '.number_format($this->$attribute);
    }

    public function getSubtotal()
    {
        return $this->getDisbursementDetails()->sum('subtotal_eqv');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['deptid' => 'departments_id']);
    }
}