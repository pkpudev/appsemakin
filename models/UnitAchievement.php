<?php

namespace app\models;

use Yii;
use \app\models\base\UnitAchievement as BaseUnitAchievement;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.unit_achievement".
 */
class UnitAchievement extends BaseUnitAchievement
{
    public function getPosition()
    {
        return $this->hasOne(PositionStructure::className(), ['id' => 'unit_id']);
    }

    public function getEvaluation()
    {
        return $this->hasOne(\app\models\Evaluation::className(), ['id' => 'evaluation_id']);
    }
}
