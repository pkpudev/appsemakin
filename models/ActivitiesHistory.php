<?php

namespace app\models;

use Yii;
use \app\models\base\ActivitiesHistory as BaseActivitiesHistory;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.activities_history".
 */
class ActivitiesHistory extends BaseActivitiesHistory
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    public static function createHistory(Activities $activities, $action, $comment)
    {
        // Insert History
        $history = new static;
        $history->activities_id = $activities->id;
        $history->action = $action;
        $history->comment = $comment;
        return $history->save();
    }

    public function beforeValidate()
    {
        if (!parent::beforeValidate()) return false;

        $this->created_stamp = new \yii\db\Expression('NOW()');
        if (Yii::$app->hasProperty('user'))
            $this->created_by = Yii::$app->user->id;
            $this->ip = \app\components\UserIP::getRealIpAddr();

        return true;
    }


    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }
}
