<?php

namespace app\models;

use Yii;
use \app\models\base\VwOkr as BaseVwOkr;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.vw_okr".
 */
class VwOkr extends BaseVwOkr
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    public static function primaryKey()
    {
        return ['serial_number'];
    }

    public function getParent()
    {
        return $this->hasOne(Okr::className(), ['id' => 'parent_id']);
    }

    public function getChild()
    {
        return $this->hasOne(Okr::className(), ['id' => 'child_id']);
    }
}
