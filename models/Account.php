<?php

namespace app\models;

use Yii;
use \app\models\base\Account as BaseAccount;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "finance.account".
 */
class Account extends BaseAccount
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
