<?php

namespace app\models;

use Yii;
use \app\models\base\PortfolioType as BasePortfolioType;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.portfolio_type".
 */
class PortfolioType extends BasePortfolioType
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}