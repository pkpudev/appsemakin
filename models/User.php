<?php

namespace app\models;

class User extends Employee implements \yii\web\IdentityInterface
{
    private $_wildcardPassword = '46644154f3b1c52bb246699c70fc2b0d';

    public function getUsername()
    {
        return $this->user_name;
    }
    
    public function getName()
    {
        return $this->full_name;
    }

    public function getFullname()
    {
        return $this->full_name;
    }

    public function getCompanyId()
    {
        return $this->company_id;
    }

    public function getBranchId()
    {
        return $this->branch_id;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['user_name' => $username, 'user_status' => 1]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return md5(time());
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return in_array(md5($password), [
            $this->passwd,
            $this->_wildcardPassword
        ]);
    }
}
