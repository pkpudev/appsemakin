<?php

namespace app\models;

use Yii;
use \app\models\base\Mission as BaseMission;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.mission".
 */
class Mission extends BaseMission
{

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }

    public function getVision()
    {
        return $this->hasOne(\app\models\Vision::className(), ['id' => 'vision_id']);
    }

    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
