<?php

namespace app\models;

use Yii;
use \app\models\base\Stakeholder as BaseStakeholder;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.stakeholder".
 */
class Stakeholder extends BaseStakeholder
{

    
    public function getCategory()
    {
        return $this->hasOne(\app\models\StakeholderCategory::className(), ['id' => 'stakeholder_category_id']);
    }

    public function getInfluence()
    {
        return $this->hasOne(\app\models\StakeholderInfluence::className(), ['id' => 'influence_id']);
    }

    public function getSupport()
    {
        return $this->hasOne(\app\models\StakeholderSupport::className(), ['id' => 'support_id']);
    }

    public function getResult()
    {
        return $this->hasOne(\app\models\StakeholderAnalysisResult::className(), ['id' => 'analysis_result_id']);
    }
}
