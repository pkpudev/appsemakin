<?php

namespace app\models;

use Yii;
use \app\models\base\Ceiling as BaseCeiling;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.ceiling".
 */
class Ceiling extends BaseCeiling
{

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }

    public static function getPlafon($deptid, $position_structure_id, $year)
    {
        $plafon = self::find()->where(['unit_structure_id'=>$deptid, 'position_structure_id' => $position_structure_id, 'year'=>$year])->one();
        $ceiling = $plafon->ceiling_amount;
        if(!$ceiling)
            $ceiling = 0;

        return $ceiling;
    }

    public static function getTotalBudget($deptid, $position_structure_id, $year)
    {
        $total_budget = Initiatives::find()->alias('i')
            ->joinWith(['okr o', 'okr.position p'])
            ->where(['p.unit_structure_id'=>$deptid, 'o.position_structure_id' => $position_structure_id, 'o.year'=>$year])
            ->andWhere(['not in', 'i.status_id', [Status::REJECTED, Status::DELETED]])
            ->sum('i.total_budget');
        if(!$total_budget)
            $total_budget = 0;

        return $total_budget;
    }

    public static function getPlafonBalance($deptid, $position_structure_id, $year)
    {
        $plafon = self::getPlafon($deptid, $position_structure_id, $year);
        $total_budget = self::getTotalBudget($deptid, $position_structure_id, $year);
        $balance = $plafon - $total_budget;

        return $balance;
    }
    
    public function getUnit()
    {
        return $this->hasOne(\app\models\UnitStructure::className(), ['id' => 'unit_structure_id']);
    }
    
    public function getPosition()
    {
        return $this->hasOne(\app\models\PositionStructure::className(), ['id' => 'position_structure_id']);
    }

    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
