<?php

namespace app\models;

use Yii;
use \app\models\base\OkrEvaluation as BaseOkrEvaluation;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.okr_evaluation".
 */
class OkrEvaluation extends BaseOkrEvaluation
{

    public $item_functions;
    public $unit_code;
    public $unit_name;
    public $position_name;
    public $full_name;
    public $position_structure_id;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [
                    [
                        'item_functions'
                    ], 'safe']
            ]
        );
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            
            if($this->role == 'Utama'){
                $this->role_value = 2; //UTAMA
            } else if($this->role == 'Support'){
                $this->role_value = 1; //SUPPORT
            }

            if($this->function){
                $this->function_value = count(explode('; ', $this->function));
            } else {
                $this->function_value = null;
            }

            if(!$this->isNewRecord && $this->unit_structure_id && $this->function_value && $this->role_value) $this->weight = $this->getBobot();
            

            return true;
        }
    }

    protected function getBobot(){

        $currentOkr = Okr::findOne($this->okr_id);

        $totalRoleValue = self::find()
                                ->where(['okr_id' => $currentOkr->id])
                                ->andWhere(['evaluation_id' => $this->evaluation_id])
                                ->sum('role_value');
        $totalFunctionValue = self::find()
                                ->where(['okr_id' => $currentOkr->id])
                                ->andWhere(['evaluation_id' => $this->evaluation_id])
                                ->sum('function_value');


        if($totalRoleValue && $totalFunctionValue){
            $bobot = (($this->role_value/$totalRoleValue) + ($this->function_value/$totalFunctionValue)) / 2 * 100; 
        } else {
            $bobot = 100;
        }
        
        return $bobot ?: 0;
    }

    public function getAchievedByPeriod($year, $period, $unitId){
        $evaluation = Evaluation::findOne(['year' => $year, 'period' => $period]);

        if($evaluation){
            // $okrAchievement = OkrEvaluation::findOne(['evaluation_id' => $evaluation->id, 'okr_id' => $this->okr_id, 'unit_structure_id' => $unitId]);
            $okrAchievements = OkrEvaluation::find()
                                                ->where(['evaluation_id' => $evaluation->id])
                                                ->andWhere(['okr_id' => $this->okr_id])
                                                ->all();

            $achieve = 0;
            foreach($okrAchievements as $okrAchievement){
                $achieve = $achieve + (($okrAchievement->real * $okrAchievement->weight)/100);
            }

            // return $okrAchievement->real ? number_format($okrAchievement->real, 2, ',', '.') . '%' : '-';
            
            // return $okrAchievement->achieved->achieve_value ? number_format($okrAchievement->achieved->achieve_value, 2, ',', '.') . '%' : '-';
            return $achieve ?: 0;
        } else {
            return 0;
        }
    }

    public function getLastAchievedByPeriod($year, $unitId){
        $evaluations = Evaluation::find()->where(['year' => $year])->orderBy('id')->all();

        if($evaluations){

            $achieveValues = [];
            foreach($evaluations as $evaluation){
                $okrAchievement = OkrEvaluation::findOne(['evaluation_id' => $evaluation->id, 'okr_id' => $this->okr_id, 'unit_structure_id' => $unitId]);

                // array_push($achieveValues, $okrAchievement->real ?: 0);
                array_push($achieveValues, $okrAchievement->achieved->achieve_value ?: 0);
            }

            if($this->okr->calculation->id == Calculation::SUM){
                $lastAchieveValue = array_sum($achieveValues);
            } else if($this->okr->calculation->id == Calculation::AVERAGE){
                $lastAchieveValue = $achieveValues ? array_sum($achieveValues)/count($achieveValues) : 0;
            } else if($this->okr->calculation->id == Calculation::LAST){
                $lastAchieveValue = end($achieveValues);
            } else if($this->okr->calculation->id == Calculation::MIN){
                $lastAchieveValue = min($achieveValues);
            }

            return is_numeric($lastAchieveValue) ? number_format($lastAchieveValue, 2, ',', '.') . '% (' . ($this->okr->calculation->calculation_type ?: '-') . ')' : '-';
        } else {
            return '-';
        }
    }

    /** RELATIONS */
    public function getEvaluation()
    {
        return $this->hasOne(\app\models\Evaluation::className(), ['id' => 'evaluation_id']);
    }

    public function getOkr()
    {
        return $this->hasOne(\app\models\Okr::className(), ['id' => 'okr_id']);
    }

    public function getOkrOrg()
    {
        return $this->hasOne(\app\models\OkrOrg::className(), ['id' => 'okr_id']);
    }

    public function getAchieved()
    {
        return $this->hasOne(\app\models\OkrAchievement::className(), ['okr_id' => 'okr_id', 'evaluation_id' => 'evaluation_id']);
    }

    public function getPic()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'pic_id']);
    }

    public function getUnit()
    {
        return $this->hasOne(\app\models\UnitStructure::className(), ['id' => 'unit_structure_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(\app\models\Status::className(), ['id' => 'status_id']);
    }

    public function getMovs()
    {
        return $this->hasMany(\app\models\OkrOrgMov::className(), ['okr_evaluation_id' => 'id']);
    }
}
