<?php

namespace app\models;

use Yii;
use \app\models\base\ActivityHistory as BaseActivityHistory;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.activity_history".
 */
class ActivityHistory extends BaseActivityHistory
{

    public static function createHistory($id, $action, $comment){
        $history=new ActivityHistory;
        $history->by = Yii::$app->user->identity->id;
        $history->ip = Yii::$app->request->userIP;
        $history->activity_id = $id;
        $history->action = $action;
        $history->comment = $comment;
        $history->save();
    }

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    public function getBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'by']);
    }
}
