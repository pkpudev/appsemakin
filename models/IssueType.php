<?php

namespace app\models;

use Yii;
use \app\models\base\IssueType as BaseIssueType;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.issue_type".
 */
class IssueType extends BaseIssueType
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
