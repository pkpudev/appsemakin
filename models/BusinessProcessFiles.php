<?php

namespace app\models;

use Yii;
use \app\models\base\BusinessProcessFiles as BaseBusinessProcessFiles;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.business_process__files".
 */
class BusinessProcessFiles extends BaseBusinessProcessFiles
{

    public $files;
    public $links;
    public $storages;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['files', 'links', 'storages'], 'safe']
            ]
        );
    }

    
    public function getBusinessProcess()
    {
        return $this->hasOne(\app\models\BusinessProcess::className(), ['id' => 'business_process_id']);
    }
    
    public function getFile()
    {
        return $this->hasOne(\app\models\Files::className(), ['id' => 'files_id']);
    }
}
