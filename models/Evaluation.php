<?php

namespace app\models;

use Yii;
use \app\models\base\Evaluation as BaseEvaluation;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.evaluation".
 */
class Evaluation extends BaseEvaluation
{

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->status_id = Status::GENERATE_BARU;
            }else{
                if(Yii::$app->hasProperty('user'))
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
            }

            return true;
        }
    }

    public function isCanEdit(){
        $startDate = $this->start_date;
        $finishDate = $this->end_date;
        $currentDate = date('Y-m-d');
        
        // if(date('Y') != 2023){
        //     return true;
        // } else {
            return true;
        // }
    }

    public function getStatus()
    {
        return $this->hasOne(\app\models\Status::className(), ['id' => 'status_id']);
    }

    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
