<?php

namespace app\models;

use Yii;
use \app\models\base\CostType as BaseCostType;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.cost_type".
 */
class CostType extends BaseCostType
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
