<?php

namespace app\models;

use Yii;
use \app\models\base\RiskCategory as BaseRiskCategory;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.risk_category".
 */
class RiskCategory extends BaseRiskCategory
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
