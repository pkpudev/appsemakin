<?php

namespace app\models;

use Yii;
use \app\models\base\Target as BaseTarget;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.target".
 */
class Target extends BaseTarget
{

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                
            }else{
                if(Yii::$app->hasProperty('user')){
                    $this->updated_by = Yii::$app->user->id;
                    $this->updated_at = new \yii\db\Expression('NOW()');                
                }
            }
            
            $this->total_target = $this->month_01 + $this->month_02 + $this->month_03 + $this->month_04 + $this->month_05 + $this->month_06 + $this->month_07 + $this->month_08 + $this->month_09 + $this->month_10 + $this->month_11 + $this->month_12;

            return true;
        }
    }


    public static function getUnitTarget($unit_id, $month, $column){

        if($column == 'target'){
            $monthColumn = 'month_' . str_pad(((int) $month), 2, "0", STR_PAD_LEFT);
        } else {
            $monthColumn = 'real_month_' . str_pad(((int) $month), 2, "0", STR_PAD_LEFT);
        }

        $target = self::findOne(['unit_structure_id' => $unit_id])->$monthColumn;

        return $target ?: 0;
    }

    public static function getUnitTargetYear($unit_id, $column){

        if($column == 'target'){
            $monthColumnName = 'month_';
        } else {
            $monthColumnName = 'real_month_';
        }

        $target = 0;
        for($m = 1; $m <= 12; $m++){
            $monthColumn = $monthColumnName . str_pad(((int) $m), 2, "0", STR_PAD_LEFT);
            $targetValue = self::findOne(['unit_structure_id' => $unit_id])->$monthColumn;

            $target = $target + $targetValue;
        }

        return $target;

    }

    public static function getUnitTargetCompany($year, $column, $dept = null, $m, $targetTypeIds = null){

        if($column == 'target'){
            $monthColumn = 'month_' . str_pad(((int) $m), 2, "0", STR_PAD_LEFT);
        } else {
            $monthColumn = 'real_month_' . str_pad(((int) $m), 2, "0", STR_PAD_LEFT);
        }

        if($dept){
            $targetValue = self::find()
                                    ->where(['year' => $year])
                                    ->andWhere(['in', 'unit_structure_id', $dept])
                                    // ->andWhere($targetTypeIds ? ['target_type_id' => $targetTypeIds] : '1=1')
                                    ->sum($monthColumn);
        } else {
            $targetValue = self::find()
                                    ->where(['year' => $year])
                                    // ->andWhere($targetTypeIds ? ['target_type_id' => $targetTypeIds] : '1=1')
                                    ->sum($monthColumn);
        }


        return $targetValue ?: 0;

    }

    public function getUnit()
    {
        return $this->hasOne(\app\models\UnitStructure::className(), ['id' => 'unit_structure_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(\app\models\PositionStructure::className(), ['id' => 'position_structure_id']);
    }

    public function getEmployee()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'employee_id']);
    }

    public function getType()
    {
        return $this->hasOne(\app\models\TargetType::className(), ['id' => 'target_type_id']);
    }

    public function getCreatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }
}
