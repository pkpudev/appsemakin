<?php

namespace app\models;

use Yii;
use \app\models\base\Status as BaseStatus;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.status".
 */
class Status extends BaseStatus
{

    const SCENARIO_INITIATIVES         = 'Default';
    const SCENARIO_GENERATE         = 'generate_evaluation';
    const SCENARIO_OKR_EVALUATION   = 'okr_evaluation';
    const SCENARIO_OKR_ORG_EVALUATION   = 'okr_org_evaluation';
    const SCENARIO_ACTIVITY_EVALUATION   = 'activity_evaluation';
    const SCENARIO_ISSUE   = 'issue_status';
    const SCENARIO_OKR   = 'okr';

    
	const DRAFT 	            = 1;
	const SEND 		            = 2;
	const REVISION 	            = 3;
	const APPROVED 	            = 4;
    const REJECTED              = 5;
	const DELETED           	= 6;
	const GENERATE_BARU 	    = 7;
	const GENERATE_PROSES 	    = 8;
	const GENERATE_PROSES_2	    = 9;
	const OKR_E_DRAFT 	        = 10;
	const OKR_E_SENT 	        = 11;
	const OKR_E_APPROVE         = 12;
	const OKR_ORG_E_CREATE 	    = 13;
	const OKR_ORG_E_DRAFT 	    = 14;
	const OKR_ORG_E_SENT 	    = 15;
	const OKR_ORG_E_VERIFIED    = 16;
	const GENERATE_SELESAI      = 17;
	const ACT_NEW               = 18;
	const ACT_ON_PROGRESS       = 19;
	const ACT_DONE              = 20;
	const ACT_CANCEL            = 25;
	const ISSUE_RUNNING         = 21;
	const ISSUE_DIVERTED        = 22;
	const ISSUE_DONE            = 23;
	const ISSUE_RISK            = 24;
	const OKR_CANCEL            = 26;

	public function getStatusname($id)
    {
        $status = $this->status;
        $labels = array(
            1   => 'info',
            2   => 'primary',
            3   => 'warning',
            4   => 'success',
            5   => 'danger',
            6   => 'danger',
            7   => 'info',
            8   => 'warning',
            9   => 'primary',
            10  => 'info',
            11  => 'warning',
            12  => 'success',
            13  => 'info',
            14  => 'primary',
            15  => 'warning',
            16  => 'success',
            17  =>'success',
            18  =>'default',
            19  =>'warning',
            20  =>'success',
            21  =>'warning',
            22  =>'info',
            23  =>'success',
            24  =>'danger',
            25  =>'danger',
            26  =>'danger',
        );

        $name = '<span class="label label-'.$labels[$id].'">'.$status.'</span>';
    
        return $name;
    }
}
