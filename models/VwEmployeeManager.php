<?php

namespace app\models;

use Yii;
use \app\models\base\VwEmployeeManager as BaseVwEmployeeManager;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "hrm.vw_employee_manager".
 */
class VwEmployeeManager extends BaseVwEmployeeManager
{

    public $id;
    public $duration;
    public $fte;
    
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    
    public static function primaryKey()
    {
        return ["id"];
    }

    public function getEmployee()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'employee_id']);
    }

    public function getUnit()
    {
        return $this->hasOne(\app\models\UnitStructure::className(), ['id' => 'unit_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(\app\models\PositionStructure::className(), ['id' => 'position_id']);
    }
}
