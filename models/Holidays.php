<?php

namespace app\models;

use Yii;
use \app\models\base\Holidays as BaseHolidays;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "hrm.holidays".
 */
class Holidays extends BaseHolidays
{

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_stamp = new \yii\db\Expression('NOW()');
                $this->created_by = Yii::$app->user->id;
            } else {
                $this->updated_stamp = new \yii\db\Expression('NOW()');
                $this->updated_by = Yii::$app->user->id;
            }


            return true;
        }
    }


    //RELATION
    public function getBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy0()
    {
        return $this->hasOne(\app\models\Employee::className(), ['id' => 'updated_by']);
    }

    // public function getDivision()
    // {
    //     return $this->hasOne(\app\models\Department::className(), ['deptid' => 'deptid']);
    // }

}
