<?php

namespace app\models;

use Yii;
use \app\models\base\ActivityEvaluationHistory as BaseActivityEvaluationHistory;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.activity_evaluation_history".
 */
class ActivityEvaluationHistory extends BaseActivityEvaluationHistory
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
