<?php

namespace app\models;

use Yii;
use \app\models\base\SourceOfFunds as BaseSourceOfFunds;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "finance.source_of_funds".
 */
class SourceOfFunds extends BaseSourceOfFunds
{


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->company_id = 1;
            }

            return true;
        }
    }

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
