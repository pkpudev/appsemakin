<?php

namespace app\models;

use Yii;
use \app\models\base\InitiativesEvaluation as BaseInitiativesEvaluation;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "management.initiatives_evaluation".
 */
class InitiativesEvaluation extends BaseInitiativesEvaluation
{

    public $unit_code;
    public $unit_name;
    public $unit_structure_id;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    /** RELATIONS */
    public function getEvaluation()
    {
        return $this->hasOne(\app\models\Evaluation::className(), ['id' => 'evaluation_id']);
    }

    public function getInitiatives()
    {
        return $this->hasOne(\app\models\Initiatives::className(), ['id' => 'initiatives_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(\app\models\Status::className(), ['id' => 'status_id']);
    }
}
