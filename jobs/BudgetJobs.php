<?php

namespace app\jobs;

use app\models\Budget;
use Yii2RabbitMQ\LogPrintJob;
use app\models\PhpDonor;
use app\models\VwRKAT;

class BudgetJobs extends LogPrintJob
{    
    public function execute($queue)
    {
        parent::execute($queue);

        $budgetId = $this->data['budget_id'];

        $rkat       = VwRKAT::find()
                                    ->where(['year' => date('Y')])
                                    ->andWhere(['budget_id' => $budgetId])
                                    ->one();
        
        $disbursement_total = \app\components\CountTotalReal::fromDisbursementDetail($budgetId); 
        $procurement_total = \app\components\CountTotalReal::fromProcurementDetail($budgetId);
        $selisih = \app\components\CountTotalReal::fromDisbursementDetailSelisih($budgetId);
        
        $total_real = $disbursement_total + $procurement_total + $selisih;
        
        if($rkat->total_real <> $total_real){
            $budget = Budget::findOne($budgetId);
            $budget->total_real = $total_real;
            
            // echo "{$budget->id}" . PHP_EOL;
            if($budget->save(false)){
                // echo ".. saved." . PHP_EOL;
            } else {
                // var_dump($budget->errors);
            }
        }
        
        // $model = 
        // TODO coding here!
        // (string) $this->routingKey
        // (array) $this->data
    }
}
