<?php

return [
    'admin' => [
        'class' => 'mdm\admin\Module',
        'controllerMap' => [
            'assignment' => [
                'class' => 'app\controllers\rbac\AssignmentController',
            ],
            'role' => [
                'class' => 'app\controllers\rbac\RoleController',
            ],
        ],
    ],
    'analysis' => [
        'class' => 'app\modules\analysis\AnalysisModule',
    ],
    'api2' => [
        'class' => 'app\modules\api2\Api2Module',
    ],
    'businessprocess' => [
        'class' => 'app\modules\businessprocess\BusinessProcessModule',
    ],
    'evaluation' => [
        'class' => 'app\modules\evaluation\EvaluationModule',
    ],
    'gridview' =>  [
        'class' => '\kartik\grid\Module'
    ],
    'leadership' => [
        'class' => 'app\modules\leadership\LeadershipModule',
    ],
    'strategic' => [
        'class' => 'app\modules\strategic\StrategicModule',
    ],
    'setting' => [
        'class' => 'app\modules\setting\SettingModule',
    ],
    
];
