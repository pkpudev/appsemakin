<?php

$db = require(__DIR__ . '/db.php');
$dblog = require(__DIR__ . '/dblog.php');
$mailer = require __DIR__ . '/mail.php';
$user = require(__DIR__ . '/user.php');

return [
    'assetManager' => [
        'bundles' => [
            'dmstr\web\AdminLteAsset' => [
                'skin' => 'skin-red-light',
            ],
        ],
    ],
    'authManager' => [
        'class' => 'yii\rbac\DbManager',
        'defaultRoles' => ['Employee'],
        'itemTable' => 'management.auth_item',
        'itemChildTable' => 'management.auth_item_child',
        'assignmentTable' => 'management.auth_assignment',
        'ruleTable' => 'management.auth_rule',
    ],
    'cache' => [
        'class' => 'yii\caching\FileCache',
    ],
    'db' => $db,
    'dblog' => $dblog,
    'errorHandler' => [
        'errorAction' => 'site/error',
    ],
    'formatter' => [
        'dateFormat' => 'dd.MM.yyyy',
        'decimalSeparator' => ',',
        'thousandSeparator' => '.',
        'currencyCode' => 'Rp ',
        // 'numberFormatterOptions' => [
        //     NumberFormatter::MIN_FRACTION_DIGITS => 0,
        //     NumberFormatter::MAX_FRACTION_DIGITS => 0,
        // ],
    ],
    'i18n' => [
        'translations' => [
            '*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/messages', // if advanced application, set @frontend/messages
                'sourceLanguage' => 'id',
                'fileMap' => [
                    //'main' => 'main.php',
                ],
            ],
        ],
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'app\components\log\AccessDbTarget',
                'db' => 'dblog',
                'logTable' => 'public.user_log',
                'app' => getenv('APP_LOG_NAME'),
            ],
            [
                'class' => 'app\components\log\JsonAccessFileTarget',
                'categories' => ['yii\web\Session::open'],
                'levels' => ['info'],
                'logFile' => '@runtime/logs/access_in_json.log',
            ],
            [
                'class' => 'app\components\log\JsonFileTarget',
                'levels' => ['error', 'warning'],
                'except' => [
                    'yii\web\HttpException:404',
                    'yii\web\HttpException:403',
                    'yii\debug\Module::checkAccess',
                    'yii\web\HttpException:400',
                    'yii\base\UserException',
                    'yii\web\HttpException:401',
                    'yii\web\User::getIdentityAndDurationFromCookie',
                    'yii\web\ForbiddenHttpException:403', //403
                    'yii\web\NotFoundHttpException:404', //404
                    'yii\web\MethodNotAllowedHttpException:405', //405
                ],
                'logFile' => '@runtime/logs/error_in_json.log',
            ],
        ],
    ],
    'mailer' => $mailer,
    'request' => [
        'cookieValidationKey' => 'i25NBG41M8-k44OJZ_RGQEMhQdkjOGe10',
        'parsers' => [
            'application/json' => 'yii\web\JsonParser',
        ],
        'trustedHosts' => [
                            '0.0.0.0/0',                
                        ],                
        'secureHeaders' => [
            'X-Forwarded-For',                
            'X-Forwarded-Host',                
            'X-Forwarded-Proto',                
            'X-Forwarded-Port',                
            'X-Real-IP',                
            'CF-Connecting-IP',                
        ],                
        'ipHeaders' => [
            'CF-Connecting-IP',                
            'X-Real-IP',                
        ],
    ],
    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [],
    ],
    'user' => $user,
    'view' => [
        'theme' => [
            'pathMap' => [
                '@app/views' => 'app\components\View'
            ],
        ],
    ],
];
