<?php


$components = require(__DIR__ . '/components.php');
$modules = require(__DIR__ . '/modules.php');
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name' => 'eperform',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => $components,
    'params' => $params,
    'modules' => $modules,
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            // '*'
            'site/login',
            'site/logout',
            'api/get-achieves',
            'api/get-achieves-employee',
            'api/get-other-values',
            'api2/*',
        ],
    ],
    'container' => [
        'definitions' => [
            'wbraganca\dynamicform\DynamicFormAsset' => [
                'sourcePath' => '@app/assets',
                'js' => [YII_DEBUG ? 'js/yii2-dynamic-form.js' : 'js/yii2-dynamic-form.min.js'],
            ]
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

return $config;
