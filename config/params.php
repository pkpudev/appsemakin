<?php

return [
    'bsVersion' => '3.x',
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',

    'ttdKontrakDate' => '2025-12-31'
];
