<?php

if (getenv('YII_ENV') == 'dev') {
    return [
        'class' => 'app\components\web\User',
        'identityClass' => 'app\models\User',
        'enableAutoLogin' => true,
        'superuserCheck' => true,
        'superuserPermissionName' => 'SuperUser',
    ];
} else {
    return [
        'class' => 'pkpudev\simplesaml\WebUser',
        'identityClass' => 'app\models\User',
        'autoloaderPath'=>'/home/samlsp/simplesamlphp-1.18.8/lib/_autoload.php',
        // 'authSource'=>'c27g-sp',
        'authSource'=>'c27g-sp',
        'attributesConfig'=>array(
            'id'=>'id',
            'username'=>'user_name',
            'name'=>'full_name',
            'fullname'=>'full_name',
            'email'=>'email',
            'branchId'=>'branch_id',
            'type'=>'type',
            'companyId'=>'company_id',
        ),
        'superuserCheck' => true,
        'superuserPermissionName' => 'superuserAccess',
    ];
}