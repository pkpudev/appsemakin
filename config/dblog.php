<?php // file: AppName/config/dblog.php

return [
    'class' => 'yii\db\Connection',
    'dsn' => sprintf(
        'pgsql:host=%s;port=%s;dbname=%s',
        getenv('DBLOG_HOST'),
        getenv('DBLOG_PORT'),
        getenv('DBLOG_NAME')
    ),
    'username' => getenv('DBLOG_USER'),
    'password' => getenv('DBLOG_PASS'),
    'charset' => 'utf8',
];