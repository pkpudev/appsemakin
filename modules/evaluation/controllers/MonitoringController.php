<?php

namespace app\modules\evaluation\controllers;

use app\models\Evaluation;
use app\models\OkrEvaluation;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\search\InitiativesEvaluationSearch;
use app\models\search\OkrEvaluationSearch;
use app\models\UnitStructure;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;


class MonitoringController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($id = null, $directorateId = null, $departmentId = null)
    {
        
        $evaluation = Evaluation::findOne($id);

        if(!$id){
            $evaluation = Evaluation::find()->orderBy('id desc')->one();
        }
        
        $year = $evaluation->year;
        $period = $evaluation->period;

        $query = UnitStructure::find()->where(['year' => $year]);
                
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['unit_code'=>SORT_ASC]],
            // 'pagination' => ['pageSize' => 10]
        ]);

        
        $listDirectorate = ArrayHelper::map(UnitStructure::find()->where(['year' => $year])->andWhere(['level' => 1])->orderBy('unit_code')->all(), 'id', function($model){
            return $model->unit_code . ' - ' . $model->unit_name;
        });

        $listDepartment = ArrayHelper::map(UnitStructure::find()->where(['year' => $year])->andWhere(['level' => 2])->orderBy('unit_code')->all(), 'id', function($model){
            return $model->unit_code . ' - ' . $model->unit_name;
        });

        $searchModelInitiatives = new InitiativesEvaluationSearch();
        $searchModelInitiatives->year = $year;
        $dataProviderInitiatives = $searchModelInitiatives->searchMonitoring(Yii::$app->request->queryParams, $directorateId, $departmentId);

        $searchModelOkrUnit = new OkrEvaluationSearch();
        $searchModelOkrUnit->year = $year;
        $dataProviderOkrUnit = $searchModelOkrUnit->searchMonitoring(Yii::$app->request->queryParams, 'unit', $directorateId, $departmentId);

        $searchModelOkrIndividual = new OkrEvaluationSearch();
        $searchModelOkrIndividual->year = $year;
        $dataProviderOkrIndividual = $searchModelOkrIndividual->searchMonitoring(Yii::$app->request->queryParams, 'individu', $directorateId, $departmentId);

        return $this->render('index', 
                                [
                                    'year' => $year,
                                    'period' => $period,
                                    'evaluation' => $evaluation,
                                    'dataProvider' => $dataProvider,
                                    'searchModelInitiatives' => $searchModelInitiatives,
                                    'dataProviderInitiatives' => $dataProviderInitiatives,
                                    'searchModelOkrUnit' => $searchModelOkrUnit,
                                    'dataProviderOkrUnit' => $dataProviderOkrUnit,
                                    'searchModelOkrIndividual' => $searchModelOkrIndividual,
                                    'dataProviderOkrIndividual' => $dataProviderOkrIndividual,
                                    'listDirectorate' => $listDirectorate,
                                    'listDepartment' => $listDepartment,
                                    'directorateId' => $directorateId,
                                    'departmentId' => $departmentId,
                                ]);
    }
}
