<?php

namespace app\modules\evaluation\controllers;

use app\components\FilesHelper;
use app\models\Activity;
use Yii;
use app\models\ActivityEvaluation;
use app\models\ActivityEvaluationHistory;
use app\models\ActivityIssue;
use app\models\ActivityMov;
use app\models\Evaluation;
use app\models\Files;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\search\ActivityEvaluationSearch;
use app\models\search\ActivitySearch;
use app\models\Status;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * ActivityController implements the CRUD actions for ActivityEvaluation model.
 */
class ActivityController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    public function actionEvaluation(){

        $evaluations = Evaluation::find()->where(['status_id' => Status::GENERATE_SELESAI])->all();
        
        return $this->render('evaluation', [
            'evaluations' => $evaluations,
        ]);

    }

    /**
     * Lists all ActivityEvaluation models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new ActivityEvaluationSearch();
        $searchModel = new ActivitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $isLeader = false;
            
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }

                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }
        
        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one();
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
            'isManager' => $isManager,
        ]);
    }


    /**
     * Displays a single ActivityEvaluation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "ActivityEvaluation #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new ActivityEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new ActivityEvaluation();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new ActivityEvaluation",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new ActivityEvaluation",
                    'content'=>'<span class="text-success">Create ActivityEvaluation success</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Create new ActivityEvaluation",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing ActivityEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update ActivityEvaluation #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "ActivityEvaluation #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                 return [
                    'title'=> "Update ActivityEvaluation #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing ActivityEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing ActivityEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /** ACTUAL VALUE */
    public function actionActualValue($id)
    {
        $request = Yii::$app->request;
        $model = Activity::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Nilai Realisasi Kegiatan",
                    'content'=>$this->renderAjax('_form_actual_value', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                if($model->actual_value == 100){
                    $checkMov = ActivityMov::findOne(['activity_id' => $id]);
                    if(!$checkMov){
                        $msg = '<span class="text-danger">Harus melampirkan minimal 1 dokumen terlebih dahulu sebagai Alat Verifikasi</span>';
                    } else {
                        $model->save();
                        $msg = '<span class="text-success">Berhasil mengubah Nilai Realisasi Kegiatan</span>';
                    }
                } else {
                    $model->save();
                    $msg = '<span class="text-success">Berhasil mengubah Nilai Realisasi Kegiatan</span>';
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Nilai Realisasi Kegiatan",
                    'content'=>$msg,
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Nilai Realisasi Kegiatan",
                    'content'=>$this->renderAjax('_form_actual_value', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** ACTUAL VALUE */
    public function actionActualNote($id)
    {
        $request = Yii::$app->request;
        $model = Activity::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Catatan Realisasi Kegiatan",
                    'content'=>$this->renderAjax('_form_actual_note', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Catatan Realisasi Kegiatan",
                    'content'=>'<span class="text-success">Berhasil mengubah Catatan Realisasi Kegiatan</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Catatan Realisasi Kegiatan",
                    'content'=>$this->renderAjax('_form_actual_note', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** SET OKR */
    public function actionSetOkr($id)
    {
        $request = Yii::$app->request;
        $model = Activity::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah OKR",
                    'content'=>$this->renderAjax('_form_okr', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save(false)){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah OKR",
                    'content'=>'<span class="text-success">Berhasil mengubah OKR</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah OKR",
                    'content'=>$this->renderAjax('_form_okr', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** SEND */
    public function actionSend(){
        $request = Yii::$app->request;
        $model = new Activity();
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Kirim Kegiatan",
                    'content'=>$this->renderAjax('_form_send', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            } else if($model->load($request->post())){

                $activityEvaluations = Activity::find()
                                                ->where(['pic_id' => Yii::$app->user->id])
                                                ->andWhere(['position_structure_id' => $model->position_structure_id])
                                                ->andWhere(['evaluation_status_id' => Status::OKR_E_DRAFT])
                                                ->andWhere('actual_value is not null')
                                                ->all();

                foreach($activityEvaluations as $activityEvaluation){
                    $activityEvaluation->status_id = Status::OKR_E_SENT;
                    if($activityEvaluation->save()){
                        $history = new ActivityEvaluationHistory();
                        $history->activity_id = $activityEvaluation->id;
                        $history->action = 'Mengirim Kegiatan';
                        $history->save();
                    }
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Kirim Kegiatan",
                    'content'=>'<span class="text-success">Berhasil mengirim Kegiatan</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Kirim Kegiatan",
                    'content'=>$this->renderAjax('_form_send', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** APPROVE */
    public function actionApprove(){
        $request = Yii::$app->request;
        $model = new Activity();
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Approve Kegiatan Staf",
                    'content'=>$this->renderAjax('_form_approve', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            } else if($model->load($request->post())){

                $acativityEvaluations = Activity::find()
                                                ->where(['pic_id' => $model->pic_id])
                                                ->andWhere(['position_structure_id' => $model->position_structure_id])
                                                ->andWhere(['evaluation_status_id' => Status::OKR_E_SENT])
                                                ->all();

                foreach($acativityEvaluations as $acativityEvaluation){
                    $acativityEvaluation->status_id = Status::OKR_E_APPROVE;
                    if($acativityEvaluation->save()){
                        $history = new ActivityEvaluationHistory();
                        $history->activity_id = $acativityEvaluation->id;
                        $history->action = 'Mengirim Kegiatan';
                        $history->save();
                    }
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Approve Kegiatan Staf",
                    'content'=>'<span class="text-success">Berhasil meng-approve Kegiatan Staf</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Approve Kegiatan Staf",
                    'content'=>$this->renderAjax('_form_approve', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** DOCUMENTATIONS */
    public function actionViewDocumentations($id)
    {
        $request = Yii::$app->request;
        $dataProvider = new ActiveDataProvider([
            'query' => ActivityMov::find()->where(['activity_id' => $id])
        ]);


        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=>'Alat Verifikasi',
                    'content'=>$this->renderAjax('_view_documentations', [
                        'dataProvider' => $dataProvider,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Alat Verifikasi', ['create-documentations','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->redirect('index');
        }
    }

    public function actionCreateDocumentations($id)
    {
        $request = Yii::$app->request;
        $model = new ActivityMov();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){
                if($model->storages == 0){
                    $model->files = UploadedFile::getInstances($model, 'files');
                    FilesHelper::upload($model->files, $id, 'activity_mov', null, null, $model->mov_name);
                } else {
                    $file = new Files();
                    $file->name = $model->links;
                    $file->storage = 1;
                    $file->location = $model->links;
                    $file->created_by = Yii::$app->user->identity->id;
                    $file->created_ip = \app\components\UserIP::getRealIpAddr();
                    if($file->save()){
                        $activityMov = new ActivityMov();
                        $activityMov->activity_id = $id;
                        $activityMov->files_id = $file->id;
                        $activityMov->mov_name = $model->mov_name;
                        $activityMov->save();
                    } else {
                        var_dump($file->errors);die;
                    }

                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>'<span class="text-success">Create ActivityEvaluation success</span>',
                    'footer'=> Html::a('Kembali',['view-documentations', 'id' => $id],['class'=>'btn btn-default pull-left','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionUpdateDocumentations($id)
    {
        $request = Yii::$app->request;
        $model = ActivityMov::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>'<span class="text-success">Berhasil mengubah Alat Verifikasi</span>',
                    'footer'=> Html::a('Kembali',['view-documentations', 'id' => $model->activity_id],['class'=>'btn btn-default pull-left','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionDeleteDocumentations($id)
    {
        $request = Yii::$app->request;
       ActivityMov::findOne($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /** ISSUE */
    public function actionViewIssue($id)
    {
        $request = Yii::$app->request;
        $dataProvider = new ActiveDataProvider([
            'query' => ActivityIssue::find()->where(['activity_id' => $id])
        ]);


        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=>'Kendala',
                    'content'=>$this->renderAjax('_view_issue', [
                        'dataProvider' => $dataProvider,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Kendala', ['create-issue','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->redirect('index');
        }
    }

    public function actionCreateIssue($id)
    {
        $request = Yii::$app->request;
        $model = new ActivityIssue();
        $model->activity_id = $id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Kendala",
                    'content'=>$this->renderAjax('_form_issue', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Kendala",
                    'content'=>'<span class="text-success">Berhasil menambah Kendala</span>',
                    'footer'=> Html::a('Kembali',['view-issue', 'id' => $id],['class'=>'btn btn-default pull-left','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tambah Kendala",
                    'content'=>$this->renderAjax('_form_issue', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionUpdateIssue($id)
    {
        $request = Yii::$app->request;
        $model = ActivityIssue::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Kendala",
                    'content'=>$this->renderAjax('_form_issue', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Kendala",
                    'content'=>'<span class="text-success">Berhasil mengubah Kendala</span>',
                    'footer'=> Html::a('Kembali',['view-issue', 'id' => $model->activity_id],['class'=>'btn btn-default pull-left','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Ubah Kendala",
                    'content'=>$this->renderAjax('_form_issue', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionDeleteIssue($id)
    {
        $request = Yii::$app->request;
       ActivityIssue::findOne($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    public function actionUpdatePic($id)
    {
        $request = Yii::$app->request;
        $model = Activity::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah PIC",
                    'content'=>$this->renderAjax('_form_pic', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save(false)){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah PIC",
                    'content'=>'<span class="text-success">Berhasil mengubah PIC</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah PIC",
                    'content'=>$this->renderAjax('_form_pic', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }
    


    /**
     * Finds the ActivityEvaluation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ActivityEvaluation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ActivityEvaluation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
