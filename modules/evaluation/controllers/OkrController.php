<?php

namespace app\modules\evaluation\controllers;

use app\components\FilesHelper;
use app\models\Activities;
use app\models\Evaluation;
use app\models\Files;
use app\models\Okr;
use Yii;
use app\models\OkrEvaluation;
use app\models\OkrEvaluationDocumentations;
use app\models\OkrEvaluationHistory;
use app\models\OkrLevel;
use app\models\OkrOrgMov;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\search\OkrEvaluationSearch;
use app\models\Status;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * OkrController implements the CRUD actions for OkrEvaluation model.
 */
class OkrController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                    'set-draft-organization' => ['post'],
                    'set-sent-organization' => ['post'],
                    'set-verified-organization' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all OkrEvaluation models.
     * @return mixed
     */
    public function actionIndividual()
    {
        $searchModel = new OkrEvaluationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        
        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];
            
            $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->all();
            
            foreach($vwEmployeeManagers as $vwEmployeeManager){
                $directorates = UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                foreach($directorates as $directorate){
                    array_push($dept, $directorate->id);
                    
                    $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                    foreach($divisions as $division){
                        array_push($dept, $division->id);
    
                        $division2s = UnitStructure::find()->where(['superior_unit_id' => $division->id])->orderBy('unit_code')->all();
                        foreach($division2s as $division2){
                            array_push($dept, $division2->id);
                        }
                    }
                }
            }


            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'id', $dept])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $isLeader = false;

            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }

                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }

        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one();
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
        ]);
    }

    public function actionUnit()
    {
        $searchModel = new OkrEvaluationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
  
        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];
            
            $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->all();
            
            foreach($vwEmployeeManagers as $vwEmployeeManager){
                $directorates = UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                foreach($directorates as $directorate){
                    array_push($dept, $directorate->id);
                    
                    $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                    foreach($divisions as $division){
                        array_push($dept, $division->id);
    
                        $division2s = UnitStructure::find()->where(['superior_unit_id' => $division->id])->orderBy('unit_code')->all();
                        foreach($division2s as $division2){
                            array_push($dept, $division2->id);
                        }
                    }
                }
            }


            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'id', $dept])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $isLeader = false;

            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }

                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }
        
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one() || Yii::$app->user->can('Admin');
        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one()|| Yii::$app->user->can('Admin');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
        ]);
    }

    public function actionOrganizationAdmin()
    {
        $lastEvaluation = OkrEvaluation::find()->orderBy('id desc')->one();
        $year = $lastEvaluation->evaluation->year;

        $searchModel = new OkrEvaluationSearch();
        $searchModel->year = $year;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $year = Evaluation::findOne($searchModel->evaluation_id)->year;

        return $this->render('index-organization-adm', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'year' => $year,
        ]);
    }

    public function actionOrganization()
    {
        $lastEvaluation = OkrEvaluation::find()->orderBy('id desc')->one();
        $year = $lastEvaluation->evaluation->year;

        $searchModel = new OkrEvaluationSearch();
        $searchModel->year = $year;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $year = Evaluation::findOne($searchModel->evaluation_id)->year;
        
        return $this->render('index-organization', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'year' => $year,
        ]);
    }

    /** SET ROLE */
    public function actionSetRole($id)
    {
        $request = Yii::$app->request;
        $model = OkrEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Peran",
                    'content'=>$this->renderAjax('_form_role', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save(false)){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Peran",
                    'content'=>'<span class="text-success">Berhasil mengubah Peran</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Peran",
                    'content'=>$this->renderAjax('_form_role', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** SET FUNCTION */
    public function actionSetFunction($id)
    {
        $request = Yii::$app->request;
        $model = OkrEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Fungsi",
                    'content'=>$this->renderAjax('_form_function', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) /* && $model->save(false) */){

                if($model->item_functions){
                    $model->function = implode('; ', $model->item_functions);
                } else {
                    $model->function = null;
                }
                $model->save(false);

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Fungsi",
                    'content'=>'<span class="text-success">Berhasil mengubah Fungsi</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Fungsi",
                    'content'=>$this->renderAjax('_form_function', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    
    /** SET FUNCTION */
    public function actionSetTarget($id)
    {
        $request = Yii::$app->request;
        $model = OkrEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Target",
                    'content'=>$this->renderAjax('_form_target', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save(false)){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Target",
                    'content'=>'<span class="text-success">Berhasil mengubah Target</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Target",
                    'content'=>$this->renderAjax('_form_target', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** SET DRAFT ORGANIZATION */
    public function actionSetDraftOrganization()
    {
        $request = Yii::$app->request;
        $okrOrgs = OkrEvaluation::find()
                                    ->alias('oe')
                                    ->joinWith('evaluation e')
                                    ->where(['e.status_id' => Status::GENERATE_PROSES_2])
                                    ->andWhere(['oe.status_id' => Status::OKR_ORG_E_CREATE])
                                    ->all();
        foreach($okrOrgs as $okrOrg){
            $okrOrg->status_id = Status::OKR_ORG_E_DRAFT;
            if($okrOrg->save()){
                $okrEvaluationHistory = new OkrEvaluationHistory();
                $okrEvaluationHistory->okr_evaluation_id = $okrOrg->id;
                $okrEvaluationHistory->action = 'Menjadikan Draf Evaluasi OKR';
                $okrEvaluationHistory->save();
            }
        }
        
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "OKR Organization" ,
                'content'=>'<span class="text-success">Berhasil menjadikan status OKR Organisasi menjadi Draf</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /** SET SENT ORGANIZATION */
    public function actionSetSentOrganization($id)
    {
        $request = Yii::$app->request;
        $okrOrg = OkrEvaluation::findOne($id);
        $okrOrg->status_id = Status::OKR_ORG_E_SENT;
        if($okrOrg->save()){
            $okrEvaluationHistory = new OkrEvaluationHistory();
            $okrEvaluationHistory->okr_evaluation_id = $okrOrg->id;
            $okrEvaluationHistory->action = 'Menjadikan Dikirim Evaluasi OKR';
            $okrEvaluationHistory->save();
        }
        
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "OKR Organization" ,
                'content'=>'<span class="text-success">Berhasil menjadikan status OKR Organisasi menjadi Dikirim</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }
    
    /** SET VERIFIED ORGANIZATION */
    public function actionSetVerifiedOrganization()
    {
        $request = Yii::$app->request;
        $okrOrgs = OkrEvaluation::find()
                                    ->alias('oe')
                                    ->joinWith('evaluation e')
                                    ->where(['e.status_id' => Status::GENERATE_PROSES_2])
                                    ->andWhere(['oe.status_id' => Status::OKR_ORG_E_SENT])
                                    ->all();
        foreach($okrOrgs as $okrOrg){
            $okrOrg->status_id = Status::OKR_ORG_E_VERIFIED;
            if($okrOrg->save()){
                $okrEvaluationHistory = new OkrEvaluationHistory();
                $okrEvaluationHistory->okr_evaluation_id = $okrOrg->id;
                $okrEvaluationHistory->action = 'Menjadikan Diverifikasi Evaluasi OKR';
                $okrEvaluationHistory->save();
            }
        }
        
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "OKR Organization" ,
                'content'=>'<span class="text-success">Berhasil menjadikan status OKR Organisasi menjadi Draf</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /** SEND */
    public function actionSend($type){
        $request = Yii::$app->request;
        $model = new Okr();

        if($type == 'individual'){
            $text = 'Individu';
            $view = '_form_send';
        } else if($type == 'unit'){
            $text = 'Unit';
            $view = '_form_send-unit';
        }
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Kirim OKR " . $text,
                    'content'=>$this->renderAjax($view, [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            } else if($model->load($request->post())){

                if($type == 'individual'){
                    $okrEvaluations = OkrEvaluation::find()
                                                    ->alias('oe')
                                                    ->joinWith(['okr o', 'evaluation e'])
                                                    ->where(['o.pic_id' => Yii::$app->user->id])
                                                    ->andWhere(['o.position_structure_id' => $model->position_structure_id])
                                                    ->andWhere('o.okr_level_id = ' . OkrLevel::INDIVIDUAL)
                                                    ->andWhere(['e.status_id' => Status::GENERATE_PROSES_2])
                                                    ->all();
                                                    
                    foreach($okrEvaluations as $okrEvaluation){
                        $okrEvaluation->status_id = Status::OKR_E_SENT;
                        if($okrEvaluation->save()){
                            $history = new OkrEvaluationHistory();
                            $history->okr_evaluation_id = $okrEvaluation->id;
                            $history->action = 'Mengirim OKR Individu';
                            $history->save();
                        }
                    }
                } else {
                    $unitId = $model->unit_structure_id;
                    $positionId = [];
                    $positions = PositionStructure::find()->where(['unit_structure_id' => $unitId])->all();
                    foreach($positions as $position){
                        array_push($positionId, $position->id);
                    }
    
                    $okrEvaluations = OkrEvaluation::find()
                                                    ->alias('oe')
                                                    ->joinWith(['okr o', 'evaluation e'])
                                                    ->where(['o.position_structure_id' => $positionId])
                                                    ->andWhere(['oe.status_id' => Status::OKR_E_DRAFT])
                                                    ->andWhere(['e.status_id' => Status::GENERATE_PROSES_2])
                                                    ->andWhere('o.okr_level_id <> ' . OkrLevel::INDIVIDUAL)
                                                    ->all();
    
                    foreach($okrEvaluations as $okrEvaluation){
                        $okrEvaluation->status_id = Status::OKR_E_SENT;
                        if($okrEvaluation->save()){
                            $history = new OkrEvaluationHistory();
                            $history->okr_evaluation_id = $okrEvaluation->id;
                            $history->action = 'Mengirim OKR Unit';
                            $history->save();
                        }
                    }
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Kirim OKR " . $text,
                    'content'=>'<span class="text-success">Berhasil mengirim OKR ' . $text . '</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Kirim OKR " . $text,
                    'content'=>$this->renderAjax($view, [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** APPROVE */
    public function actionApprove($type){
        $request = Yii::$app->request;
        $model = new Okr();

        
        if($type == 'individual'){
            $text = 'Staf';
            $view = '_form_approve';
        } else if($type == 'unit'){
            $text = 'Unit';
            $view = '_form_approve-unit';
        }
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Setujui OKR " . $text,
                    'content'=>$this->renderAjax($view, [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            } else if($model->load($request->post())){

                if($type == 'individual'){
                    $okrEvaluations = OkrEvaluation::find()
                                                    ->alias('oe')
                                                    ->joinWith('okr o')
                                                    ->where(['oe.pic_id' => $model->pic_id])
                                                    ->andWhere(['o.position_structure_id' => $model->position_structure_id])
                                                    ->andWhere(['oe.status_id' => Status::OKR_E_SENT])
                                                    ->andWhere(['o.okr_level_id' => 5])
                                                    ->all();
    
                    foreach($okrEvaluations as $okrEvaluation){
                        $okrEvaluation->status_id = Status::OKR_E_APPROVE;
                        if($okrEvaluation->save()){
                            $history = new OkrEvaluationHistory();
                            $history->okr_evaluation_id = $okrEvaluation->id;
                            $history->action = 'Mengaprove OKR';
                            $history->save();
                        }
                    }
                } else {
                    $unitId = $model->unit_structure_id;
                    $positionId = [];
                    $positions = PositionStructure::find()->where(['unit_structure_id' => $unitId])->all();
                    foreach($positions as $position){
                        array_push($positionId, $position->id);
                    }
    
                    $okrEvaluations = OkrEvaluation::find()
                                                    ->alias('oe')
                                                    ->joinWith('okr o')
                                                    ->where(['o.position_structure_id' => $positionId])
                                                    ->andWhere(['oe.status_id' => Status::OKR_E_SENT])
                                                    ->andWhere(['o.okr_level_id' => [2,3,4]])
                                                    ->all();
    
                    foreach($okrEvaluations as $okrEvaluation){
                        $okrEvaluation->status_id = Status::OKR_E_APPROVE;
                        if($okrEvaluation->save()){
                            $history = new OkrEvaluationHistory();
                            $history->okr_evaluation_id = $okrEvaluation->id;
                            $history->action = 'Mengaprove OKR Unit';
                            $history->save();
                        }
                    }
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Approve OKR " . $text,
                    'content'=>'<span class="text-success">Berhasil menyetujui OKR ' . $text . '</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Approve OKR " . $text,
                    'content'=>$this->renderAjax($view, [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }


    /** DOCUMENTATIONS */
    public function actionViewDocumentations($id, $unitId=null)
    {
        $request = Yii::$app->request;
        $model = OkrEvaluation::findOne($id);
        // $model = OkrEvaluation::findOne(['okr_id' => $id, 'unit_structure_id' => $unitId]);
        $dataProvider = new ActiveDataProvider([
            'query' => OkrOrgMov::find()->where(['okr_evaluation_id' => $model->id])
        ]);

        
        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=>'Alat Verifikasi',
                    'content'=>$this->renderAjax('_view_documentations', [
                        'dataProvider' => $dataProvider,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Alat Verifikasi', ['create-documentations','id'=>$id/* , 'unitId' => $unitId */],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->redirect('index');
        }
    }

    public function actionCreateDocumentations($id/* , $unitId */)
    {
        $request = Yii::$app->request;
        $model = new OkrOrgMov();
        $modelEvaluation = OkrEvaluation::findOne($id);
        // $modelEvaluation = OkrEvaluation::findOne(['okr_id' => $id, 'unit_structure_id' => $unitId]);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                        'modelEvaluation' => $modelEvaluation,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){
                if($model->storages == 0){
                    $model->files = UploadedFile::getInstances($model, 'files');
                    FilesHelper::upload($model->files, $modelEvaluation->id, 'okr_mov', null, null, $model->okr_mov_type_id);
                } else {
                    $file = new Files();
                    $file->name = $model->links;
                    $file->storage = 1;
                    $file->location = $model->links;
                    $file->created_by = Yii::$app->user->identity->id;
                    $file->created_ip = \app\components\UserIP::getRealIpAddr();
                    if($file->save()){
                        $okrOrgMov = new OkrOrgMov();
                        $okrOrgMov->okr_evaluation_id = $modelEvaluation->id;;
                        $okrOrgMov->files_id = $file->id;
                        $okrOrgMov->okr_mov_type_id = $model->okr_mov_type_id;
                        $okrOrgMov->note = $model->note;
                        $okrOrgMov->save();
                    }

                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>'<span class="text-success">Berhasil menambah Dokumen</span>',
                    'footer'=> Html::a('Kembali',['view-documentations', 'id' => $id/* , 'unitId' => $unitId */],['class'=>'btn btn-default pull-left','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                        'modelEvaluation' => $modelEvaluation,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    
    public function actionDeleteDocumentations($id)
    {
        $request = Yii::$app->request;
       OkrOrgMov::findOne($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Displays a single OkrEvaluation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "OkrEvaluation #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new OkrEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new OkrEvaluation();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new OkrEvaluation",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new OkrEvaluation",
                    'content'=>'<span class="text-success">Create OkrEvaluation success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Create new OkrEvaluation",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing OkrEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update OkrEvaluation #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "OkrEvaluation #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                 return [
                    'title'=> "Update OkrEvaluation #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing OkrEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing OkrEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }
    
    /**Output */
    public function actionOutput($id)
    {
        $request = Yii::$app->request;
        $model = OkrEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Keluaran OKR",
                    'content'=>$this->renderAjax('_form_output', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $model->save();
                $msg = '<span class="text-success">Berhasil mengubah Keluaran OKR</span>';

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Keluaran OKR",
                    'content'=>$msg,
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Keluaran OKR",
                    'content'=>$this->renderAjax('_form_output', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }
    
    /**REAL */
    public function actionReal($id)
    {
        $request = Yii::$app->request;
        $model = OkrEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Nilai Realisasi OKR",
                    'content'=>$this->renderAjax('_form_real', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                // if($model->real == 100){
                //     $checkMov = InitiativesMov::findOne(['initiatives_evaluation_id' => $id]);
                //     if(!$checkMov){
                //         $msg = '<span class="text-danger">Harus melampirkan minimal 1 dokumen terlebih dahulu sebagai Alat Verifikasi</span>';
                //     } else {
                //         $model->save();
                //         $msg = '<span class="text-success">Berhasil mengubah Nilai Realisasi OKR</span>';
                //     }
                // } else {
                    $model->save();
                    $msg = '<span class="text-success">Berhasil mengubah Nilai Realisasi OKR</span>';
                // }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Nilai Realisasi OKR",
                    'content'=>$msg,
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Nilai Realisasi OKR",
                    'content'=>$this->renderAjax('_form_real', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }
    
    /** NOTE */
    public function actionNote($id)
    {
        $request = Yii::$app->request;
        $model = OkrEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Catatan Realisasi OKR",
                    'content'=>$this->renderAjax('_form_note', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $model->save();
                $msg = '<span class="text-success">Berhasil mengubah Catatan Realisasi OKR</span>';

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Catatan Realisasi OKR",
                    'content'=>$msg,
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Catatan Realisasi OKR",
                    'content'=>$this->renderAjax('_form_note', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }
    
    /** OUTPUT NOTE */
    public function actionOutputNote($id)
    {
        $request = Yii::$app->request;
        $model = OkrEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Catatan Keluaran OKR",
                    'content'=>$this->renderAjax('_form_output_note', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $model->save();
                $msg = '<span class="text-success">Berhasil mengubah Catatan Keluaran OKR</span>';

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Catatan Keluaran OKR",
                    'content'=>$msg,
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Catatan Keluaran OKR",
                    'content'=>$this->renderAjax('_form_output_note', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** PERFORMANCE ANALYSIS */
    public function actionPerformanceAnalysis($id)
    {
        $request = Yii::$app->request;
        $model = OkrEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Analisis Capaian OKR",
                    'content'=>$this->renderAjax('_form_performance_analysis', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $model->save();
                $msg = '<span class="text-success">Berhasil mengubah Analisis Capaian OKR</span>';

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Analisis Capaian OKR",
                    'content'=>$msg,
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Analisis Capaian OKR",
                    'content'=>$this->renderAjax('_form_performance_analysis', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** INTERPRETATION */
    public function actionInterpretation($id)
    {
        $request = Yii::$app->request;
        $model = OkrEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Interpretasi OKR",
                    'content'=>$this->renderAjax('_form_interpretation', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $model->save();
                $msg = '<span class="text-success">Berhasil mengubah Interpretasi OKR</span>';

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Interpretasi OKR",
                    'content'=>$msg,
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Interpretasi OKR",
                    'content'=>$this->renderAjax('_form_interpretation', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** DOCUMENTATIONS */
    public function actionViewDocumentations2($id)
    {
        $request = Yii::$app->request;
        $dataProvider = new ActiveDataProvider([
            'query' => OkrEvaluationDocumentations::find()->where(['okr_evaluation_id' => $id])
        ]);


        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=>'Alat Verifikasi',
                    'content'=>$this->renderAjax('_view_documentations2', [
                        'dataProvider' => $dataProvider,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Alat Verifikasi', ['create-documentations2','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->redirect('index');
        }
    }

    /** VIEW ACTIVITIES */
    public function actionViewActivities($okr_id, $evaluation_id, $okr_evaluation_id)
    {
        $request = Yii::$app->request;
        $evaluation = Evaluation::findOne($evaluation_id);

        $okrEvaluation = OkrEvaluation::findOne($okr_evaluation_id);
        
        $periodStart = $this->getPeriodRange($evaluation->year, $evaluation->period)[0];
        $periodEnd = $this->getPeriodRange($evaluation->year, $evaluation->period)[1];

        $dataProvider = new ActiveDataProvider([
            'query' => Activities::find()->where(['okr_id' => $okr_id])
                            ->andWhere("'$periodStart' <= start_date and start_date <= '$periodEnd'")
                            ->andWhere(['status_id' => Status::ACT_DONE])
                            ->andWhere("parent_id is NULL")
        ]);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Daftar Kegiatan",
                'content'=>$this->renderAjax('_view_activities', [
                    'okrEvaluation' => $okrEvaluation,
                    'dataProvider' => $dataProvider,
                ]),
            'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

            ];
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionCreateDocumentations2($id)
    {
        $request = Yii::$app->request;
        $model = new OkrEvaluationDocumentations();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations2', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){
                if($model->storages == 0){
                    $model->files = UploadedFile::getInstances($model, 'files');
                    FilesHelper::upload($model->files, $id, 'okr_evaluation_mov', null, null, $model->mov_name);
                } else {
                    $file = new Files();
                    $file->name = $model->links;
                    $file->storage = 1;
                    $file->location = $model->links;
                    $file->created_by = Yii::$app->user->identity->id;
                    $file->created_ip = \app\components\UserIP::getRealIpAddr();
                    if($file->save()){
                        $okrEvaluationDocumentations = new OkrEvaluationDocumentations();
                        $okrEvaluationDocumentations->okr_evaluation_id = $id;
                        $okrEvaluationDocumentations->files_id = $file->id;
                        $okrEvaluationDocumentations->mov_name = $model->mov_name;
                        if($okrEvaluationDocumentations->save()){

                        } else {

                            var_dump($okrEvaluationDocumentations->errors);die;
                        }
                    } else {
                        var_dump($file->errors);die;
                    }

                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>'<span class="text-success">Berhasil menambahkan Alat Verifikasi</span>',
                    'footer'=> Html::a('Kembali',['view-documentations2', 'id' => $id],['class'=>'btn btn-default pull-left','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations2', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionUpdateDocumentations2($id)
    {
        $request = Yii::$app->request;
        $model = OkrEvaluationDocumentations::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations2', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>'<span class="text-success">Berhasil mengubah Alat Verifikasi</span>',
                    'footer'=> Html::a('Kembali',['view-documentations2', 'id' => $model->okr_evaluation_id],['class'=>'btn btn-default pull-left','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations2', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionDeleteDocumentations2($id)
    {
        $request = Yii::$app->request;
       OkrEvaluationDocumentations::findOne($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the OkrEvaluation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OkrEvaluation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OkrEvaluation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function getPeriodRange($year, $period){

        $dateRange = [];

        if($period == 'Kuartal 1'){
            $dateStart = $year . '-' . '01-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '03-01'));
        } else if($period == 'Kuartal 2'){
            $dateStart = $year . '-' . '04-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '06-01'));
        } else if($period == 'Kuartal 3'){
            $dateStart = $year . '-' . '07-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '09-01'));
        } else if($period == 'Kuartal 4'){
            $dateStart = $year . '-' . '10-01';
            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '12-01'));
        }

        array_push($dateRange, $dateStart);
        array_push($dateRange, $dateEnd);

        return $dateRange;
    }
}
