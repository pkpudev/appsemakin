<?php

namespace app\modules\evaluation\controllers;

use app\components\FilesHelper;
use Yii;
use app\models\Activities;
use app\models\ActivitiesFiles;
use app\models\ActivitiesHistory;
use app\models\ActivitiesIssue;
use app\models\Employee;
use app\models\Files;
use app\models\Holidays;
use app\models\Okr;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\search\ActivitiesIssueSearch;
use app\models\search\ActivitiesSearch;
use app\models\search\UnitStructureSearch;
use app\models\search\VwEmployeeManagerSearch;
use app\models\Status;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * ActivitiesController implements the CRUD actions for Activities model.
 */
class ActivitiesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $searchModel = new ActivitiesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $currentDate = date('Y-m-d');

        $cntAllAct = Activities::find()->where(['pic_id' => Yii::$app->user->id])->andWhere("start_date <= '$currentDate' and due_date >= '$currentDate'")->andWhere('parent_id is null')->count();
        $cntDoneAct = Activities::find()->where(['pic_id' => Yii::$app->user->id])->andWhere("start_date <= '$currentDate' and due_date >= '$currentDate'")->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_DONE])->count();
        $cntProgressAct = Activities::find()->where(['pic_id' => Yii::$app->user->id])->andWhere("start_date <= '$currentDate' and due_date >= '$currentDate'")->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_ON_PROGRESS])->count();
        $cntNewAct = Activities::find()->where(['pic_id' => Yii::$app->user->id])->andWhere("start_date <= '$currentDate' and due_date >= '$currentDate'")->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_NEW])->count();
        $cntCancelAct = Activities::find()->where(['pic_id' => Yii::$app->user->id])->andWhere("start_date <= '$currentDate' and due_date >= '$currentDate'")->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_CANCEL])->count();

        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        $isAdmin = Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD');
        
        if($isAdmin){
            $listDirectorate = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['level' => '1'])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
            $listDepartment = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['level' => '2'])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere("level != '1' and level != '2'")->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $userID = Yii::$app->user->id;
            
            $listOkr = ArrayHelper::map(Okr::find()->where(['pic_id' => $userID])->andWhere(['type' => 'KR'])->andWhere(['year' => $searchModel->year])->all(), 'id', function($model){
                return $model->okr_code . ' - ' . $model->okr;
            }, function($model){
                return $model->position->unit->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $post = [];
            $isLeader = false;

            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                array_push($post, $ep->position_structure_id);

                
                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['id' => $dept])->andWhere("level != '1'")->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listOkr = ArrayHelper::map(Okr::find()->where(['pic_id' => $userID])->andWhere(['position_structure_id' => $post])->andWhere(['type' => 'KR'])->andWhere(['year' => $year])->all(), 'id', function($model){
                return $model->okr_code . ' - ' . $model->okr;
            }, function($model){
                return $model->position->unit->unit_name;
            });
        }

        return $this->render('index', [
            'isAdmin' => $isAdmin,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cntAllAct' => $cntAllAct,
            'cntDoneAct' => $cntDoneAct,
            'cntProgressAct' => $cntProgressAct,
            'cntNewAct' => $cntNewAct,
            'cntCancelAct' => $cntCancelAct,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
            'listUnit' => $listUnit,
            'listDirectorate' => $listDirectorate,
            'listDepartment' => $listDepartment,
            'listOkr' => $listOkr,
        ]);
    }

    /**
     * Lists all Activities models.
     * @return mixed
     */
    /* public function actionIndex()
    {

        $vwEmployeeManagers = VwEmployeeManager::find()
                                                ->where(['employee_id' => Yii::$app->user->id])
                                                ->all();

        $unitIds = [];
        foreach($vwEmployeeManagers as $vwEmployeeManager){
            array_push($unitIds, $vwEmployeeManager->unit_id);
        }

        $positionIds = PositionStructure::find()->select('id')->where(['unit_structure_id' => $unitIds])->column();

        $activities = new ActiveDataProvider([
            'query' => Activities::find()->where('parent_id is null')
        ]);

        $cntAllAct = Activities::find()->where(['pic_id' => Yii::$app->user->id])->andWhere('parent_id is null')->count();
        $cntDoneAct = Activities::find()->where(['pic_id' => Yii::$app->user->id])->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_DONE])->count();
        $cntProgressAct = Activities::find()->where(['pic_id' => Yii::$app->user->id])->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_ON_PROGRESS])->count();
        $cntNewAct = Activities::find()->where(['pic_id' => Yii::$app->user->id])->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_NEW])->count();
        $cntCancelAct = Activities::find()->where(['pic_id' => Yii::$app->user->id])->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_CANCEL])->count();

        
        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();


        return $this->render('index', [
            'activities' => $activities,
            'cntAllAct' => $cntAllAct,
            'cntDoneAct' => $cntDoneAct,
            'cntProgressAct' => $cntProgressAct,
            'cntNewAct' => $cntNewAct,
            'cntCancelAct' => $cntCancelAct,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
            'vwEmployeeManagers' => $vwEmployeeManagers,
        ]);
    } */

    public function actionHeaderActivity($id){
        $model = Activities::findOne($id);

        $this->layout = false;
        return $this->render('_head_act', [
            'model' => $model,
        ]);
    }

    public function actionDetailActivity($id){
        $model = Activities::findOne($id);

        $this->layout = false;
        return $this->render('_detail_act', [
            'model' => $model,
        ]);
    }

    /* public function actionSubActivity($id){
        $model = Activities::findOne($id);

        $this->layout = false;
        return $this->render('_sub_act', [
            'model' => $model,
        ]);
    } */

    public function actionAll()
    {

        $searchModel = new ActivitiesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->can('Admin')){
            $filterCount = "1 = 1";
        } else {
            $filterCount = ['pic_id' => Yii::$app->user->id];
        }

        $cntAllAct = Activities::find()->where($filterCount)->andWhere('parent_id is null')->count();
        $cntDoneAct = Activities::find()->where($filterCount)->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_DONE])->count();
        $cntProgressAct = Activities::find()->where($filterCount)->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_ON_PROGRESS])->count();
        $cntNewAct = Activities::find()->where($filterCount)->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_NEW])->count();
        $cntCancelAct = Activities::find()->where($filterCount)->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_CANCEL])->count();

        
        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        $isAdmin = Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD');

        if($isAdmin){
            $listDirectorate = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['level' => '1'])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
            $listDepartment = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['level' => '2'])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere("level != '1' and level != '2'")->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $userID = $searchModel->pic_id ?: Yii::$app->user->id;
            
            $listOkr = ArrayHelper::map(Okr::find()->where(['pic_id' => $userID])->andWhere(['type' => 'KR'])->andWhere(['year' => $searchModel->year])->all(), 'id', function($model){
                return $model->okr_code . ' - ' . $model->okr;
            }, function($model){
                return $model->position->unit->unit_name;
            });
        } else {
            $userID = $searchModel->pic_id ?: Yii::$app->user->id;
            $dept = [];
            $post = [];
            $isLeader = false;

            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                array_push($post, $ep->position_structure_id);
                
                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }
            
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere("level != '1'")->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listOkr = ArrayHelper::map(Okr::find()->where(['pic_id' => $userID])->andWhere(['position_structure_id' => $post])->andWhere(['type' => 'KR'])->andWhere(['year' => $year])->all(), 'id', function($model){
                return $model->okr_code . ' - ' . $model->okr;
            }, function($model){
                return $model->position->unit->unit_name;
            });
        }
        
        if($searchModel->unit_structure_id){
            $picId = [];
            $vwEmployeeManagers = VwEmployeeManager::find()->where(['unit_id' => $searchModel->unit_structure_id])->all();
            foreach($vwEmployeeManagers as $vwEmployeeManager){
                array_push($picId, $vwEmployeeManager->employee_id);
            }

            $listEmployee = ArrayHelper::map(Employee::find()->andWhere(['id' => $picId])->orderBy('full_name')->all(), 'id', 'full_name');
        } else {
            $listEmployee = ArrayHelper::map(Employee::find()->where(['is_employee' => 1])->andWhere(['company_id' => 1])->andWhere(['user_status' => 1])->andWhere(['employee_type_id' => [1,2,5]])->orderBy('full_name')->all(), 'id', 'full_name');
        }

        return $this->render('index', [
            'isAdmin' => $isAdmin,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cntAllAct' => $cntAllAct,
            'cntDoneAct' => $cntDoneAct,
            'cntProgressAct' => $cntProgressAct,
            'cntNewAct' => $cntNewAct,
            'cntCancelAct' => $cntCancelAct,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
            'listUnit' => $listUnit,
            'listDirectorate' => $listDirectorate,
            'listDepartment' => $listDepartment,
            'listEmployee' => $listEmployee,
            'listOkr' => $listOkr,
        ]);
    }

    public function actionLower()
    {

        $searchModel = new ActivitiesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $vwEmployeeManagers = VwEmployeeManager::find()
                                                ->where(['employee_id' => /* Yii::$app->user->id */199833])
                                                ->andWhere(['level' => 4])
                                                ->all();

        $unitIds = [];
        foreach($vwEmployeeManagers as $vwEmployeeManager){
            array_push($unitIds, $vwEmployeeManager->unit_id);
        }

        $positionIds = PositionStructure::find()->select('id')->where(['unit_structure_id' => $unitIds])->column();

        $cntAllAct = Activities::find()->where(['position_structure_id' => $positionIds])/* ->andWhere("pic_id != " . Yii::$app->user->id) */->andWhere('parent_id is null')->count();
        $cntDoneAct = Activities::find()->where(['position_structure_id' => $positionIds])/* ->andWhere("pic_id != " . Yii::$app->user->id) */->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_DONE])->count();
        $cntProgressAct = Activities::find()->where(['position_structure_id' => $positionIds])/* ->andWhere("pic_id != " . Yii::$app->user->id) */->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_ON_PROGRESS])->count();
        $cntNewAct = Activities::find()->where(['position_structure_id' => $positionIds])/* ->andWhere("pic_id != " . Yii::$app->user->id) */->andWhere('parent_id is null')->andWhere(['status_id' => Status::ACT_NEW])->count();
        $cntCancelAct = Activities::find()->where(['position_structure_id' => $positionIds])->andWhere(['status_id' => Status::ACT_CANCEL])->count();


        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        $isAdmin = Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD');

        if($isAdmin){
            $listDirectorate = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['level' => '1'])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
            $listDepartment = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['level' => '2'])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere("level != '1' and level != '2'")->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $userID = $searchModel->pic_id ?: Yii::$app->user->id;
            
            $listOkr = ArrayHelper::map(Okr::find()->where(['pic_id' => $userID])->andWhere(['type' => 'KR'])->andWhere(['year' => $searchModel->year])->all(), 'id', function($model){
                return $model->okr_code . ' - ' . $model->okr;
            }, function($model){
                return $model->position->unit->unit_name;
            });
        } else {
            $userID = $searchModel->pic_id ?: Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere("level != '1'")->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listOkr = ArrayHelper::map(Okr::find()->where(['pic_id' => $userID])->andWhere(['position_structure_id' => $post])->andWhere(['type' => 'KR'])->andWhere(['year' => $year])->all(), 'id', function($model){
                return $model->okr_code . ' - ' . $model->okr;
            }, function($model){
                return $model->position->unit->unit_name;
            });
        }
        
        if($searchModel->unit_structure_id){
            $picId = [];
            $vwEmployeeManagers = VwEmployeeManager::find()->where(['unit_id' => $searchModel->unit_structure_id])->all();
            foreach($vwEmployeeManagers as $vwEmployeeManager){
                array_push($picId, $vwEmployeeManager->employee_id);
            }

            $listEmployee = ArrayHelper::map(Employee::find()->andWhere(['id' => $picId])->orderBy('full_name')->all(), 'id', 'full_name');
        } else {
            $listEmployee = ArrayHelper::map(Employee::find()->where(['is_employee' => 1])->andWhere(['company_id' => 1])->andWhere(['user_status' => 1])->andWhere(['employee_type_id' => [1,2,5]])->orderBy('full_name')->all(), 'id', 'full_name');
        }

        return $this->render('index', [
            'isAdmin' => $isAdmin,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'vwEmployeeManagers' => $vwEmployeeManagers,
            'cntAllAct' => $cntAllAct,
            'cntDoneAct' => $cntDoneAct,
            'cntCancelAct' => $cntCancelAct,
            'cntProgressAct' => $cntProgressAct,
            'cntNewAct' => $cntNewAct,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
            'listUnit' => $listUnit,
            'listDirectorate' => $listDirectorate,
            'listDepartment' => $listDepartment,
            'listEmployee' => $listEmployee,
            'listOkr' => $listOkr,
        ]);
    }

    public function actionReview(){
        $searchModel = new ActivitiesSearch();

        if(!Yii::$app->user->can('Admin') && !Yii::$app->user->can('BOD')){
            $searchModel->year = date('Y');
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        
        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'isManager' => $isManager,
        ]);
    }

    public function actionKanban(){
        $searchModel = new ActivitiesSearch();
        $dataProviderBelumDimulai = $searchModel->search(Yii::$app->request->queryParams, 'belum_dimulai');
        $dataProviderSedangBerlangsung = $searchModel->search(Yii::$app->request->queryParams, 'sedang_berlangsung');
        $dataProviderSelesai = $searchModel->search(Yii::$app->request->queryParams, 'selesai');

        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        $isAdmin = Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD');
        
        if($isAdmin){
            $listDirectorate = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['level' => '1'])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
            $listDepartment = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['level' => '2'])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere("level != '1' and level != '2'")->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
            
            $userID = $searchModel->pic_id ?: Yii::$app->user->id;

            $listOkr = ArrayHelper::map(Okr::find()->where(['pic_id' => $userID])->andWhere(['type' => 'KR'])->andWhere(['year' => $searchModel->year])->all(), 'id', function($model){
                return $model->okr_code . ' - ' . $model->okr;
            }, function($model){
                return $model->position->unit->unit_name;
            });
        } else {
            $userID = $searchModel->pic_id ?: Yii::$app->user->id;
            $dept = [];
            $post = [];
            $isLeader = false;
            
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                array_push($post, $ep->position_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
                
                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere("level != '1'")->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listOkr = ArrayHelper::map(Okr::find()->where(['pic_id' => $userID])->andWhere(['position_structure_id' => $post])->andWhere(['type' => 'KR'])->andWhere(['year' => $year])->all(), 'id', function($model){
                return $model->okr_code . ' - ' . $model->okr;
            }, function($model){
                return $model->position->unit->unit_name;
            });
        }

        $userID = Yii::$app->user->id;
        $dept = [];
        $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
        foreach($employeePositions as $ep){
            array_push($dept, $ep->position->unit_structure_id);
        }
        
        
        if($searchModel->unit_structure_id || $dept){
            $picId = [];
            $vwEmployeeManagers = VwEmployeeManager::find()->where(['unit_id' => ($searchModel->unit_structure_id ?: $dept)])->andWhere(['year' => $searchModel->year])->all();
            foreach($vwEmployeeManagers as $vwEmployeeManager){
                array_push($picId, $vwEmployeeManager->employee_id);
            }

            $listEmployee = ArrayHelper::map(Employee::find()->andWhere(['id' => $picId])->orderBy('full_name')->all(), 'id', 'full_name');
        } else {
            $listEmployee = ArrayHelper::map(Employee::find()->where(['is_employee' => 1])->andWhere(['company_id' => 1])->andWhere(['user_status' => 1])->andWhere(['employee_type_id' => [1,2,5]])->orderBy('full_name')->all(), 'id', 'full_name');
        }

        

        return $this->render('kanban', [
            'isAdmin' => $isAdmin,
            'searchModel' => $searchModel,
            'listUnit' => $listUnit,
            'listDirectorate' => $listDirectorate,
            'listDepartment' => $listDepartment,
            'listEmployee' => $listEmployee,
            'dataProviderBelumDimulai' => $dataProviderBelumDimulai,
            'dataProviderSedangBerlangsung' => $dataProviderSedangBerlangsung,
            'dataProviderSelesai' => $dataProviderSelesai,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
            'listOkr' => $listOkr,
        ]);
    }

    public function actionDetailKanban($id){
        $model = Activities::findOne($id);

        $this->layout = false;
        return $this->render('kanban/_detail_item', [
            'model' => $model,
        ]);
    }

    public function actionHour(){
        $searchModel = new VwEmployeeManagerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('hour', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAbk(){
        $searchModel = new UnitStructureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('abk', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCalendar(){
        $searchModel = new ActivitiesSearch();
        $dataProviderBelumDimulai = $searchModel->search(Yii::$app->request->queryParams, 'belum_dimulai');
        $dataProviderSedangBerlangsung = $searchModel->search(Yii::$app->request->queryParams, 'sedang_berlangsung');
        $dataProviderSelesai = $searchModel->search(Yii::$app->request->queryParams, 'selesai');

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                /* if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                } */
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }

        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();

        if($searchModel->unit_structure_id){
            $picId = [];
            $vwEmployeeManagers = VwEmployeeManager::find()->where(['unit_id' => $searchModel->unit_structure_id])->all();
            foreach($vwEmployeeManagers as $vwEmployeeManager){
                array_push($picId, $vwEmployeeManager->employee_id);
            }

            $listEmployee = ArrayHelper::map(Employee::find()->andWhere(['id' => $picId])->orderBy('full_name')->all(), 'id', 'full_name');
        } else {
            $listEmployee = ArrayHelper::map(Employee::find()->where(['is_employee' => 1])->andWhere(['company_id' => 1])->andWhere(['user_status' => 1])->andWhere(['employee_type_id' => [1,2,5]])->orderBy('full_name')->all(), 'id', 'full_name');
        }

        

        return $this->render('calendar', [
            'searchModel' => $searchModel,
            'listUnit' => $listUnit,
            'listEmployee' => $listEmployee,
            'dataProviderBelumDimulai' => $dataProviderBelumDimulai,
            'dataProviderSedangBerlangsung' => $dataProviderSedangBerlangsung,
            'dataProviderSelesai' => $dataProviderSelesai,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
        ]);
    }


    /**
     * Displays a single Activities model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $page = 'index', array $ActivitiesSearch = null)
    {
        $request = Yii::$app->request;

        $model = $this->findModel($id);

        $dataProvideChild = new ActiveDataProvider([
            'query' => Activities::find()->where(['parent_id' => $id])->orderBy('id')
        ]);

        $dataProvideHistory = new ActiveDataProvider([
            'query' => ActivitiesHistory::find()->where(['activities_id' => $id])->orderBy('created_stamp desc')
        ]);

        $dataProviderFile = new ActiveDataProvider([
            'query' => ActivitiesFiles::find()->where(['activities_id' => $id])->orderBy('id asc')
        ]);

        $urlParams = '';
        if($ActivitiesSearch){
            foreach($ActivitiesSearch as $key => $val){
                $urlParams .= '&ActivitiesSearch[' . $key . ']=' . $val;
            }
        }

        $hasChild = Activities::find()->where(['parent_id' => $model->id])->one();
        
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> $model->parent_id ? 'Lihat Sub Kegiatan' : 'Lihat Kegiatan Utama',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                        'dataProvideChild' => $dataProvideChild,
                        'dataProvideHistory' => $dataProvideHistory,
                        'dataProviderFile' => $dataProviderFile,
                        'page' => $page,
                        'urlParams' => $urlParams,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            '<div class="btn-group">' .
                                ($hasChild ? '' : Html::a('<span class="fa fa-dot-circle-o"></span>&nbsp;&nbsp;Ubah Progress', ['update-status', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['title' => 'Ubah Progress', 'class' => 'btn btn-default', 'role' => 'modal-remote'])) .
                                Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-warning','role'=>'modal-remote']) .
                            '</div>'
                ];
        }else{
            return $this->redirect('index');
        }
    }

    /**
     * Creates a new Activities model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($parent_id = null, $page = 'index', array $ActivitiesSearch = null)
    {
        $request = Yii::$app->request;
        $model = new Activities();
        $modelParent = Activities::findOne($parent_id);
                
        if($parent_id){
            $model->parent_id = $parent_id;
            $model->activity_category_id = $modelParent->activity_category_id;
            $model->pic_id = $modelParent->pic_id;
            $model->position_structure_id = $modelParent->position_structure_id;
            $model->supported_unit_id = $modelParent->supported_unit_id;
            $model->okr_id = $modelParent->okr_id;
            $model->start_date = $modelParent->start_date;
            $model->finish_date = $modelParent->finish_date;
            $model->due_date = $modelParent->due_date;

        }
        
        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => [4, 5]])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();

        $urlParams = '';
        if($ActivitiesSearch){
            foreach($ActivitiesSearch as $key => $val){
                $urlParams .= '&ActivitiesSearch[' . $key . ']=' . $val;
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> $parent_id ? 'Tambah Sub Rencana Kegiatan' : 'Tambah Rencana Kegiatan Utama',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                        'isManager' => $isManager,
                        'isGeneralManager' => $isGeneralManager,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){
                
                $isSave = false;
                if($model->save()){
                    $msg = "<span class='text-success'>Berhasil menambah " . ($parent_id ? 'Sub ' : '') . "Kegiatan</span>";
                    
                    ActivitiesHistory::createHistory($model, ($parent_id ? 'Membuat sub kegiatan.' : 'Membuat kegiatan baru.'), '');
                    $isSave = true;
                } else {
                    $keys = array_keys($model->errors);
                    $msg = "<span class='text-danger'>Terjadi Kesalahan!<br />Error: " . str_replace(['["', '"]'], "", json_encode($model->errors[$keys[0]])) . "</span>";
                    $isSave = false;
                }

                
                if($page == 'kanban'){
                    if($parent_id){
                        return [
                            'title'=> 'Tambah Kegiatan',
                            'content'=>'<span class="text-success">' . $msg . '</span>' . $this->getScriptRefreshDivOnKanban($model->parent_id ?: $model->id),
                            'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a($model->parent_id ? 'Tambah Sub Kegiatan Lagi' : 'Tambah Sub Kegiatan', ['create', 'parent_id' => $model->parent_id ?: $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['class' => 'btn btn-success', 'role' => 'modal-remote'])
                        ];
                    } else {

                        if($isSave)
                        Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['kanban?' . $urlParams]));
                        
                        return [
                            'title'=> 'Tambah Kegiatan',
                            'content'=>'<span class="text-success">' . $msg . '</span>',
                            'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                        ];
                    }
                } else {
                    return [
                        'forceReload'=> $parent_id ? "" : '#activities-list-pjax',
                        'title'=> 'Tambah Kegiatan',
                        'content'=>'<span class="text-success">' . $msg . '</span>' . $this->getScriptRefreshDiv($model->parent_id ?: $model->id),
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
                }

            }else{
                return [
                    'title'=> 'Tambah Kegiatan',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Activities model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $page = 'index', array $ActivitiesSearch = null)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $modelParent = Activities::findOne($model->parent_id);
        if($modelParent) $parent_id = $model->parent_id;

        $urlParams = '';
        if($ActivitiesSearch){
            foreach($ActivitiesSearch as $key => $val){
                $urlParams .= '&ActivitiesSearch[' . $key . ']=' . $val;
            }
        }

        
        $isManager = VwEmployeeManager::find()->where(['employee_id' => $model->pic_id])->andWhere(['upper_id' => Yii::$app->user->id])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();
        // $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => $model->pic_id])->andWhere(['upper_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->andWhere(['year' => date('Y')])->one();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> $modelParent ? 'Ubah Sub Kegiatan' : 'Ubah Kegiatan Utama',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                        'isManager' => $isManager,
                        // 'isGeneralManager' => $isGeneralManager,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                
                ActivitiesHistory::createHistory($model, ($parent_id ? 'Mengubah sub kegiatan.' : 'Mengubah Kegiatan Utama.'), '');

                if($page == 'kanban'){
                    // Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['kanban?' . $urlParams]));
                    return [
                        'title'=> 'Tambah Kegiatan',
                        'content'=>'<span class="text-success">Berhasil mengubah Kegiatan</span>' . $this->getScriptRefreshDivOnKanban($model->parent_id ?: $model->id),
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
                } else {
                    return [
                        'forceReload '=> $parent_id ? '' : '#activities-list-pjax',
                        'title'=>  'Ubah Kegiatan',
                        'content' => "<span class='text-success'>Berhasil mengubah Kegiatan" . $this->getScriptRefreshDiv($model->parent_id ?: $model->id),
                        'footer' => Html::button('Tutup', ['id' => 'refresh-activity', 'class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
    
                    ];
                }

            }else{
                 return [
                    'title'=> 'Ubah Kegiatan',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect([$page]);
        }
    }

    public function actionUpdateStatus($id, $page = 'index', array $ActivitiesSearch = null)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $urlParams = '';
        if($ActivitiesSearch){
            foreach($ActivitiesSearch as $key => $val){
                $urlParams .= '&ActivitiesSearch[' . $key . ']=' . $val;
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Status Kegiatan',
                    'content'=>$this->renderAjax('_form-update-status', [
                                    'model' => $model,
                                ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save(false)){
                
                ActivitiesHistory::createHistory($model, ($model->parent_id ? 'Mengubah status sub kegiatan.' : 'Membuat status kegiatan utama.'), '');
                
                if($page == 'kanban'){
                    Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['kanban?' . $urlParams]));
                }

                return [
                    'forceReload '=> $model->parent_id ? '' : '#activities-list-pjax',
                    'title'=>  'Ubah Status Kegiatan',
                    'content' => "<span class='text-success'>Berhasil mengubah Status Kegiatan" . $this->getScriptRefreshDiv($model->parent_id ?: $model->id),
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            }else{
                 return [
                    'title'=> 'Ubah Status Kegiatan',
                    'content'=>$this->renderAjax('_form-update-status', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['kanban']);
        }
    }

    public function actionUpdateEvaluation($id, $page = 'index', array $ActivitiesSearch = null)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $urlParams = '';
        if($ActivitiesSearch){
            foreach($ActivitiesSearch as $key => $val){
                $urlParams .= '&ActivitiesSearch[' . $key . ']=' . $val;
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Kontribusi Kegiatan',
                    'content'=>$this->renderAjax('_form-update-evaluation', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save(false)){

                if($page == 'kanban'){
                    // Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['kanban?' . $urlParams]));
                    return [
                        'title'=> 'Kontribusi Terhadap Capaian OKR',
                        'content'=>'<span class="text-success">Berhasil mengubah Kontribusi Terhadap Capaian OKR</span>' . $this->getScriptRefreshDivOnKanban($model->parent_id ?: $model->id),
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
                } else {
                    return [
                        'forceReload '=> '#activities-list-pjax',
                        'title'=>  'Kontribusi Terhadap Capaian OKR',
                        'content' => "<span class='text-success'>Berhasil mengubah Kontribusi Terhadap Capaian OKR" . $this->getScriptRefreshDiv($model->parent_id ?: $model->id),
                        'footer' => Html::button('Tutup', ['id' => 'refresh-activity', 'class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
    
                    ];
                }
            }else{
                 return [
                    'title'=> 'Ubah Kontribusi Kegiatan',
                    'content'=>$this->renderAjax('_form-update-evaluation', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionUpdateOutput($id, $page = 'index', array $ActivitiesSearch = null)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $urlParams = '';
        if($ActivitiesSearch){
            foreach($ActivitiesSearch as $key => $val){
                $urlParams .= '&ActivitiesSearch[' . $key . ']=' . $val;
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Keluaran Hasil Kegiatan',
                    'content'=>$this->renderAjax('_form-update-output', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save(false)){

                if($model->storages == 0){
                    $model->files = UploadedFile::getInstances($model, 'files');
                    FilesHelper::upload($model->files, $id, 'activities_mov');
                } else {

                    $file = new Files();
                    $file->name = $model->links;
                    $file->storage = 1;
                    $file->location = $model->links;
                    $file->created_by = Yii::$app->user->identity->id;
                    $file->created_ip = \app\components\UserIP::getRealIpAddr();
                    if($file->save()){
                        $activitiesFiles = new ActivitiesFiles();
                        $activitiesFiles->activities_id = $id;
                        $activitiesFiles->files_id = $file->id;
                        $activitiesFiles->save();
                    }

                }
                
                ActivitiesHistory::createHistory($model, 'Mengubah keluaran kegiatan.', '');
                

                if($page == 'kanban'){
                    // Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['kanban?' . $urlParams]));
                    return [
                        'title'=> 'Keluaran Hasil Kegiatan',
                        'content'=>'<span class="text-success">Berhasil mengubah Keluaran Hasil Kegiatan</span>' . $this->getScriptRefreshDivOnKanban($model->parent_id ?: $model->id),
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
                } else {
                    return [
                        'forceReload '=> '#activities-list-pjax',
                        'title'=>  'Keluaran Hasil Kegiatan',
                        'content' => "<span class='text-success'>Berhasil mengubah Keluaran Hasil Kegiatan" . $this->getScriptRefreshDiv($model->parent_id ?: $model->id),
                        'footer' => Html::button('Tutup', ['id' => 'refresh-activity', 'class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
    
                    ];
                }
            }else{
                 return [
                    'title'=> 'Ubah Keluaran Kegiatan',
                    'content'=>$this->renderAjax('_form-update-output', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionIssues()
    {
        $searchModel = new ActivitiesIssueSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];

            $vwEmployeeManager = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->one();

            $directorates = UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
            foreach($directorates as $directorate){
                array_push($dept, $directorate->id);
                
                $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                foreach($divisions as $division){
                    array_push($dept, $division->id);
                }
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'id', $dept])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $isLeader = false;

            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }

                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }

        return $this->render('issue', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
        ]);
    }

    public function actionIssue($activity_id)
    {
        $request = Yii::$app->request;

        $dataProvider = new ActiveDataProvider([
            'query' => ActivitiesIssue::find()->where(['activities_id' => $activity_id])
        ]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'Kendala Kegiatan',
                    'content'=>$this->renderAjax('issue-list', [
                        'dataProvider' => $dataProvider
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Kendala',['create-issue','activity_id'=>$activity_id],['class'=>'btn btn-success','role'=>'modal-remote'])
                ];
        }else{
            return $this->redirect('index');
        }
    }

    public function actionCreateIssue($activity_id)
    {
        $request = Yii::$app->request;
        $model = new ActivitiesIssue();
        $model->activities_id = $activity_id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah Kendala',
                    'content'=>$this->renderAjax('_form_issue', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){

                ActivitiesHistory::createHistory($model->activities, 'Menambah kendala pada kegiatan.', '');
                
                $dataProvider = new ActiveDataProvider([
                    'query' => ActivitiesIssue::find()->where(['activities_id' => $activity_id])
                ]);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title'=> 'Kendala Kegiatan',
                    'content'=>$this->renderAjax('issue-list', [
                        'dataProvider' => $dataProvider
                    ]) . $this->getScriptRefreshDiv($model->activities->parent_id ?: $model->activities->id),
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Kendala',['create-issue','activity_id'=>$activity_id],['class'=>'btn btn-success','role'=>'modal-remote'])
                ];
            }else{
                 return [
                    'title'=> 'Tambah Kendala',
                    'content'=>$this->renderAjax('_form_issue', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionUpdateIssue($id, $page)
    {
        $request = Yii::$app->request;
        $model = ActivitiesIssue::findOne($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Kendala',
                    'content'=>$this->renderAjax('_form_issue', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){

                if($page == 'issues'){
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'forceReload'=> "#crud-datatable-pjax",
                        'title'=> 'Kendala Kegiatan',
                        'content'=>$this->renderAjax('view-issue', [
                            'model' => $model
                        ]),
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                    Html::a('Ubah',['update-issue', 'id'=>$id, 'page' => $page],['class'=>'btn btn-success','role'=>'modal-remote'])
                    ];
                } else {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'title'=> 'Kendala Kegiatan',
                        'content'=>$this->renderAjax('view-issue', [
                            'model' => $model
                        ]) . $this->getScriptRefreshDiv($model->activities->parent_id ?: $model->activities->id),
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                    Html::a('Ubah',['update-issue', 'id'=>$id, 'page' => $page],['class'=>'btn btn-success','role'=>'modal-remote'])
                    ];
                }
                
            }else{
                 return [
                    'title'=> 'Ubah Kendala',
                    'content'=>$this->renderAjax('_form_issue', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionViewIssue($id, $page)
    {
        $request = Yii::$app->request;

        $model = ActivitiesIssue::findOne($id);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'Lihat Kendala Kegiatan',
                    'content'=>$this->renderAjax('view-issue', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Ubah',['update-issue','id'=>$id, 'page' => $page],['class'=>'btn btn-warning','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view-issue', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeleteIssue($id, $page)
    {
        $request = Yii::$app->request;
        $model = ActivitiesIssue::findOne($id);
        $modelActivitiesId = $model->activities_id;
        $model->delete();

        ActivitiesHistory::createHistory(Activities::findOne($modelActivitiesId), 'Menghapus kendala.', '');
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($page == 'issues'){
                return [
                    'forceReload'=> "#crud-datatable-pjax",
                    'title'=>  'Info',
                    'content' => "<span class='text-success'>Berhasil menghapus Kendala Kegiatan</span>",
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            } else {

                $dataProvider = new ActiveDataProvider([
                    'query' => ActivitiesIssue::find()->where(['activities_id' => $modelActivitiesId])
                ]);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    // 'forceReload'=> "#sub-act-{$modelActivitiesId}-pjax",
                    'title'=> 'Kendala Kegiatan',
                    'content'=>$this->renderAjax('issue-list', [
                        'dataProvider' => $dataProvider
                    ]) . $this->getScriptRefreshDiv($model->activities->parent_id ?: $model->activities->id),
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Kendala',['create-issue','activity_id'=>$modelActivitiesId],['class'=>'btn btn-success','role'=>'modal-remote'])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete an existing Activities model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $page = 'index', array $ActivitiesSearch = null)
    {
        $request = Yii::$app->request;
        Activities::deleteAll(['parent_id' => $id]);
        $model = $this->findModel($id);
        $modelParentId = $model->parent_id;
        $model->delete();

        if($modelParentId){
            ActivitiesHistory::createHistory(Activities::findOne($modelParentId), 'Menghapus sub kegiatan.', '');
        }

        $urlParams = '';
        if($ActivitiesSearch){
            foreach($ActivitiesSearch as $key => $val){
                $urlParams .= '&ActivitiesSearch[' . $key . ']=' . $val;
            }
        }
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($page == 'kanban'){
                if($modelParentId){
                    return [
                        'title'=> 'Tambah Kegiatan',
                        'content'=>'<span class="text-success">Berhasil menghapus Kegiatan</span>' . $this->getScriptRefreshDivOnKanban($model->parent_id ?: $model->id),
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
                } else {
                    Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['kanban?' . $urlParams]));
                    
                    return [
                        'title'=> 'Tambah Kegiatan',
                        'content'=>'<span class="text-success">Berhasil menghapus Kegiatan</span>',
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
                }
            } else {
                return [
                        // 'forceClose'=>true,
                        'forceReload' => $modelParentId ? /* "#sub-act-{$modelParentId}-pjax" */'' : '#activities-list-pjax',
                        'title'=>  'Info',
                        'content' => "<span class='text-success'>Berhasil menghapus Kegiatan</span>" . $this->getScriptRefreshDiv($model->parent_id ?: $model->id),
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
            }

        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionCancel($id, $page = 'index', array $ActivitiesSearch = null)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $child = Activities::find()->where(['parent_id' => $id])->all();
        $modelParentId = $model->parent_id;
        $model->delete();

        if($modelParentId){
            ActivitiesHistory::createHistory(Activities::findOne($modelParentId), 'Menghapus sub kegiatan.', '');
        }

        $urlParams = '';
        if($ActivitiesSearch){
            foreach($ActivitiesSearch as $key => $val){
                $urlParams .= '&ActivitiesSearch[' . $key . ']=' . $val;
            }
        }
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($page == 'kanban'){
                if($modelParentId){
                    return [
                        'title'=> 'Tambah Kegiatan',
                        'content'=>'<span class="text-success">Berhasil menghapus Kegiatan</span>' . $this->getScriptRefreshDivOnKanban($model->parent_id ?: $model->id),
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
                } else {
                    Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['kanban?' . $urlParams]));
                    
                    return [
                        'title'=> 'Tambah Kegiatan',
                        'content'=>'<span class="text-success">Berhasil menghapus Kegiatan</span>',
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
                }
            } else {
                return [
                        // 'forceClose'=>true,
                        'forceReload' => $modelParentId ? /* "#sub-act-{$modelParentId}-pjax" */'' : '#activities-list-pjax',
                        'title'=>  'Info',
                        'content' => "<span class='text-success'>Berhasil menghapus Kegiatan</span>" . $this->getScriptRefreshDiv($model->parent_id ?: $model->id),
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
            }

        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }


     /**
     * Delete multiple existing Activities model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkReview()
    {
        $request = Yii::$app->request;
        $evaluation = $request->post('evaluation');

        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->evaluation = $evaluation;
            if($model->save(false)){
                ActivitiesHistory::createHistory($model, 'Review Kegiatan.', '');
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    public function actionMonitoring()
    {
        return $this->render('monitoring');
    }

    public function actionGetPositions() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $employeeId = $_POST['depdrop_parents'];

            $out = [];
            $pses = PositionStructureEmployee::find()
                                                ->alias('pse')
                                                ->joinWith('position p')
                                                ->where(['pse.employee_id' => $employeeId])
                                                ->andWhere(['pse.is_active' => true])
                                                ->andWhere(['p.year' => [date('Y')-1, date('Y')]])
                                                ->all();
            foreach($pses as $pse){
                array_push($out, ['id' => $pse->position_structure_id . '-' . $pse->employee_id, 'name' => $pse->position->year . ' - ' . $pse->position->position_name]);
            }
        }
        return ['output'=>$out, 'selected'=>''];
    }

    public function actionGetOkrs() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parent = $_POST['depdrop_parents'];
            
            $positionStructureId = explode('-', $parent[0])[0];
            $employeeId = explode('-', $parent[0])[1];

            $level = PositionStructure::findOne($positionStructureId)->level;

            $out = [];
            $okrs = Okr::find()
                            ->where(['position_structure_id' => $positionStructureId])
                            ->andWhere(in_array($level, [1, 2]) ? ['pic_id' => $employeeId] : '1=1')
                            ->andWhere(['year' => [date('Y')-1, date('Y')]])
                            ->andWhere(['type' => 'KR'])
                            ->orderBy('okr_code')
                            ->all();
            foreach($okrs as $okr){
                array_push($out, ['id' => $okr->id, 'name' => $okr->okr_code . ' - ' . $okr->okr . ($okr->okr_level_id == 5 ? ' (Staf/Supervisor)' : ' (Unit)')]);
                // $out[$okr->parent->okr_code . ' - ' . $okr->parent->okr][$okr->parent->okr_code . ' - ' . $okr->parent->okr] = ['id' => $okr->id, 'name' => $okr->okr_code . ' - ' . $okr->okr];
            }
        }
        return ['output'=>$out, 'selected'=>''];
    }

    public function actionGetCalendar(){
        $items = [];

        $activities = Activities::find()
                                    ->where("start_date is not null")
                                    ->andWhere("due_date is not null")
                                    ->andWhere(['pic_id' => Yii::$app->user->id])
                                    ->all();

        foreach($activities as $act){

            if($act->start_date == $act->due_date){
                $toDate = $act->start_date;
            } else {
                $toDate = date('Y-m-d', strtotime("+1 day", strtotime($act->due_date)));
            }

            $items[] = [
                'id' => $act->id,
                'title' => $act->pic->full_name . '(' . $act->activity . ')',
                'start' => $act->start_date,
                'end' => $toDate,
                // 'url' => Url::to(['attendance/leave/view', 'id' => $act->id]),
                "backgroundColor" => "var(--background)",
                "borderColor" => "#fff !important",
                "textColor" => "#fff !important"
            ];
        } 

        
        $holidays = Holidays::find()->all();
        foreach($holidays as $holiday){
            $items[] = [
                'id' => $holiday->id,
                'title' => $holiday->holiday_name,
                'start' => $holiday->holiday_date,
                "backgroundColor" => "var(--danger)",
                "borderColor" => "#fff !important",
                "textColor" => "#fff !important"
            ];
        }

        return json_encode($items);        
    }

    /**
     * Finds the Activities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Activities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Activities::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getScriptRefreshDiv($parent_id){
        return '<script>
                    $(document).ready(function () {
                        var parentId = ' . $parent_id . ';
                        
                        var refreshHeaderAct = function() {
                            $.ajax({
                                url: "/evaluation/activities/header-activity?id=" + parentId,
                                cache: false,
                                success: function(html) {
                                    $("#header-act-"+ parentId).html(html);
                                }
                            });
                        };
                        refreshHeaderAct();

                        var refreshDetailAct = function() {
                            $.ajax({
                                url: "/evaluation/activities/detail-activity?id=" + parentId,
                                cache: false,
                                success: function(html) {
                                    $("#detail-act-"+ parentId).html(html);
                                }
                            });
                        };
                        refreshDetailAct();
                    });
                </script>';
    }

    protected function getScriptRefreshDivOnKanban($parent_id){
// // var_dump($parent_id);die;
//         return "<script>alert('".$parent_id."');</script>";
        
        return "<script>
                    $(document).ready(function () {
                        var modelId = " . $parent_id . ";
                        
                        var refreshAct = function() {
                            $.ajax({
                                async: true,
                                url: '/evaluation/activities/detail-kanban?id=' + modelId,
                                cache: false,
                                success: function(html) {
                                    $('#act-'+ modelId).html(html);
                                }
                            });
                        };
                        refreshAct();
                    });
                </script>";
    }
}
