<?php

namespace app\modules\evaluation\controllers;

use app\components\FilesHelper;
use app\models\Activity;
use app\models\Files;
use app\models\Initiatives;
use Yii;
use app\models\InitiativesEvaluation;
use app\models\InitiativesEvaluationHistory;
use app\models\InitiativesMov;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\search\InitiativesEvaluationSearch;
use app\models\Status;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * InitiativesController implements the CRUD actions for InitiativesEvaluation model.
 */
class InitiativesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all InitiativesEvaluation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InitiativesEvaluationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];
            
            $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->all();
            
            foreach($vwEmployeeManagers as $vwEmployeeManager){
                $directorates = UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                foreach($directorates as $directorate){
                    array_push($dept, $directorate->id);
                    
                    $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                    foreach($divisions as $division){
                        array_push($dept, $division->id);
    
                        $division2s = UnitStructure::find()->where(['superior_unit_id' => $division->id])->orderBy('unit_code')->all();
                        foreach($division2s as $division2){
                            array_push($dept, $division2->id);
                        }
                    }
                }
            }


            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'id', $dept])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $isLeader = false;

            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }

                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }
        
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one() || Yii::$app->user->can('Admin');
        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one()|| Yii::$app->user->can('Admin');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
            'isGeneralManager' => $isGeneralManager,
            'isManager' => $isManager,
        ]);
    }


    /**
     * Displays a single ActivityEvaluation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $dataProvider = new ActiveDataProvider([
            'query' => Activity::find()->where(['initiatives_id' => $model->initiatives_id]),
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'Lihat Evaluasi Program Kerja',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                        'dataProvider' => $dataProvider,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
        }else{
            return $this->render('view', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Creates a new InitiativesEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new InitiativesEvaluation();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new InitiativesEvaluation",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new InitiativesEvaluation",
                    'content'=>'<span class="text-success">Create InitiativesEvaluation success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Create new InitiativesEvaluation",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing InitiativesEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update InitiativesEvaluation #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "InitiativesEvaluation #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                 return [
                    'title'=> "Update InitiativesEvaluation #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing InitiativesEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing InitiativesEvaluation model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }
    
    /** ACTUAL VALUE */
    public function actionReal($id)
    {
        $request = Yii::$app->request;
        $model = InitiativesEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Nilai Realisasi Program Kerja",
                    'content'=>$this->renderAjax('_form_real', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $check = $model->real/$model->target;
                if($check >= 1){
                    $checkMov = InitiativesMov::findOne(['initiatives_evaluation_id' => $id]);
                    if(!$checkMov){
                        $msg = '<span class="text-danger">Harus melampirkan minimal 1 dokumen terlebih dahulu sebagai Alat Verifikasi</span>';
                        $button = Html::a('Tambah Alat Verifikasi', ['view-documentations', 'id' => $id], ['class' => 'btn btn-warning', 'role' => 'modal-remote']);
                    } else {
                        $model->save();
                        $msg = '<span class="text-success">Berhasil mengubah Nilai Realisasi Program Kerja</span>';
                    }
                } else {
                    $model->save();
                    $msg = '<span class="text-success">Berhasil mengubah Nilai Realisasi Program Kerja</span>';
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Nilai Realisasi Program Kerja",
                    'content'=>$msg,
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) . $button
                ];
            }else{
                return [
                    'title'=> "Ubah Nilai Realisasi Program Kerja",
                    'content'=>$this->renderAjax('_form_real', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** CONTRIBUTE */
    public function actionContribute($id)
    {
        $request = Yii::$app->request;
        $model = InitiativesEvaluation::findOne($id);
        $currentEmployee = $model->employees_involved;
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Karyawan Berkontribusi",
                    'content'=>$this->renderAjax('_form_contribute_employee', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                if($currentEmployee){
                    if(strpos($currentEmployee, $model->employees_involved) !== false){
                        $msg = '<span class="text-danger"> Karyawan Sudah Berkontribusi</span>'  ;
                    } else {
                        $model->employees_involved = $currentEmployee . ', ' . $model->employees_involved;
                        $model->save();
                        $msg = '<span class="text-success">Berhasil menambah Karyawan Berkontribusi</span>';
                    }
                } else {
                    $model->save();
                    $msg = '<span class="text-success">Berhasil menambah Karyawan Berkontribusi</span>';
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Karyawan Berkontribusi",
                    'content'=>$msg,
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Tambah Karyawan Berkontribusi",
                    'content'=>$this->renderAjax('_form_contribute_employee', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionResetContribute($id)
    {
        $request = Yii::$app->request;
        $model = InitiativesEvaluation::findOne($id);
        $model->employees_involved = null;
        $model->save();
        

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }
    
    /** POSTPONE */
    public function actionPostpone($id)
    {
        $request = Yii::$app->request;
        $model = InitiativesEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tunda Penilaian Program Kerja",
                    'content'=>$this->renderAjax('_form_postpone', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tunda Penilaian Program Kerja",
                    'content'=>'<span class="text-success">Berhasil mengnunda Penilaian Program Kerja</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Tunda Penilaian Program Kerja",
                    'content'=>$this->renderAjax('_form_postpone', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** SEND */
    public function actionSend(){
        $request = Yii::$app->request;
        $model = new Initiatives();
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Kirim Program Kerja",
                    'content'=>$this->renderAjax('_form_send', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            } else if($model->load($request->post())){

                $unitId = $model->unit_structure_id;
                $positionId = [];
                $positions = PositionStructure::find()->where(['unit_structure_id' => $unitId])->all();
                foreach($positions as $position){
                    array_push($positionId, $position->id);
                }

                $initiativesEvaluations = InitiativesEvaluation::find()
                                                ->alias('ie')
                                                ->joinWith(['initiatives i', 'evaluation e'])
                                                ->where(['i.position_structure_id' => $positionId])
                                                ->andWhere(['e.status_id' => Status::GENERATE_PROSES_2])
                                                ->all();

                foreach($initiativesEvaluations as $initiativesEvaluation){
                    $initiativesEvaluation->status_id = Status::OKR_E_SENT;
                    if($initiativesEvaluation->save()){
                        $history = new InitiativesEvaluationHistory();
                        $history->initiatives_evaluation_id = $initiativesEvaluation->id;
                        $history->action = 'Mengirim Program Kerja';
                        $history->save();
                    }
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Kirim Program Kerja",
                    'content'=>'<span class="text-success">Berhasil mengirim Program Kerja</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Kirim Program Kerja",
                    'content'=>$this->renderAjax('_form_send', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** APPROVED */
    public function actionApproved(){
        $request = Yii::$app->request;
        $model = new Initiatives();
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Setujui Program Kerja",
                    'content'=>$this->renderAjax('_form_approve', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            } else if($model->load($request->post())){

                $unitId = $model->unit_structure_id;
                $positionId = [];
                $positions = PositionStructure::find()->where(['unit_structure_id' => $unitId])->all();
                foreach($positions as $position){
                    array_push($positionId, $position->id);
                }

                $initiativesEvaluations = InitiativesEvaluation::find()
                                                ->alias('ie')
                                                ->joinWith(['initiatives i', 'evaluation e'])
                                                ->where(['i.position_structure_id' => $positionId])
                                                ->andWhere(['ie.status_id' => Status::OKR_E_SENT])
                                                ->andWhere(['e.status_id' => Status::GENERATE_PROSES_2])
                                                ->all();

                foreach($initiativesEvaluations as $initiativesEvaluation){
                    $initiativesEvaluation->status_id = Status::OKR_E_APPROVE;
                    if($initiativesEvaluation->save()){
                        $history = new InitiativesEvaluationHistory();
                        $history->initiatives_evaluation_id = $initiativesEvaluation->id;
                        $history->action = 'Meng-approve Program Kerja';
                        $history->save();
                    }
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Setujui Program Kerja",
                    'content'=>'<span class="text-success">Berhasil menyetujui Program Kerja</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Setujui Program Kerja",
                    'content'=>$this->renderAjax('_form_approve', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /** DOCUMENTATIONS */
    public function actionViewDocumentations($id)
    {
        $request = Yii::$app->request;
        
        $modelInitiativesEvaluation = InitiativesEvaluation::findOne($id);

        $dataProvider = new ActiveDataProvider([
            'query' => InitiativesMov::find()->where(['initiatives_evaluation_id' => $id])
        ]);

        
        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=>'Alat Verifikasi',
                    'content'=>$this->renderAjax('_view_documentations', [
                        'dataProvider' => $dataProvider,
                        'modelInitiativesEvaluation' => $modelInitiativesEvaluation,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            ($modelInitiativesEvaluation->evaluation->isCanEdit() ?
                            Html::a('Tambah Alat Verifikasi', ['create-documentations','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']) : '')
                ];
        }else{
            return $this->redirect('index');
        }
    }

    

    /** NOTE */
    public function actionNote($id)
    {
        $request = Yii::$app->request;
        $model = InitiativesEvaluation::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Catatan Program Kerja",
                    'content'=>$this->renderAjax('_form_note', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Catatan Program Kerja",
                    'content'=>'<span class="text-success">Berhasil mengubah Catatan Program Kerja</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }else{
                return [
                    'title'=> "Ubah Catatan Program Kerja",
                    'content'=>$this->renderAjax('_form_note', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionCreateDocumentations($id)
    {
        $request = Yii::$app->request;
        $model = new InitiativesMov();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){
                if($model->storages == 0){
                    $model->files = UploadedFile::getInstances($model, 'files');
                    FilesHelper::upload($model->files, $id, 'initiatives_mov', null, null, $model->mov_name);
                } else {
                    $file = new Files();
                    $file->name = $model->links;
                    $file->storage = 1;
                    $file->location = $model->links;
                    $file->created_by = Yii::$app->user->identity->id;
                    $file->created_ip = \app\components\UserIP::getRealIpAddr();
                    if($file->save()){
                        $InitiativesMov = new InitiativesMov();
                        $InitiativesMov->initiatives_evaluation_id = $id;
                        $InitiativesMov->files_id = $file->id;
                        $InitiativesMov->mov_name = $model->mov_name;
                        $InitiativesMov->save();
                    }

                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>'<span class="text-success">Berhasil menambah Dokumen</span>',
                    'footer'=> Html::a('Kembali',['view-documentations', 'id' => $id],['class'=>'btn btn-default pull-left','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionUpdateDocumentations($id)
    {
        $request = Yii::$app->request;
        $model = InitiativesMov::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>'<span class="text-success">Berhasil mengubah Alat Verifikasi</span>',
                    'footer'=> Html::a('Kembali',['view-documentations', 'id' => $model->initiatives_evaluation_id],['class'=>'btn btn-default pull-left','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionDeleteDocumentations($id)
    {
        $request = Yii::$app->request;
       InitiativesMov::findOne($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the InitiativesEvaluation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InitiativesEvaluation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InitiativesEvaluation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
