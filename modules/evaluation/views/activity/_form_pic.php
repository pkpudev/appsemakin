<?php

use app\models\Employee;
use app\models\PositionStructureEmployee;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
/* @var $form yii\widgets\ActiveForm */
$unitId = $model->position->unit_structure_id;
$idEmployee = PositionStructureEmployee::find()
											->alias('pse')
											->joinWith('position p')
											->select(['pse.employee_id'])
											->where(['p.unit_structure_id' => $unitId])
											->andWhere(['pse.is_active' => true])
											->column();

?>

<div class="activity-evaluation-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'pic_id')->widget(Select2::classname(), [
		'data' => ArrayHelper::map(Employee::find()->where(['id' => $idEmployee])->all(), 'id', 'full_name'),
		'theme' => Select2::THEME_BOOTSTRAP,
		'initValueText' => $model->pic->full_name,
		'options' => [
						'prompt' => '-- Pilih Karyawan --',
					],
		'pluginOptions' => [
			'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
			]
	]); ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
