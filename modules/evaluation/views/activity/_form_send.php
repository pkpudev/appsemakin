<?php

use app\models\PositionStructureEmployee;
use app\models\UnitStructure;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
/* @var $form yii\widgets\ActiveForm */

$userID = Yii::$app->user->id;
$listPosition = ArrayHelper::map(PositionStructureEmployee::find()->where(['employee_id' => $userID])->all(), 'position_structure_id', function($model){
    return $model->position->position_name;
});
?>

<div class="activity-evaluation-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'position_structure_id')->dropDownList($listPosition, ['class' => 'form-control']) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
