<?php

use kartik\editors\Summernote;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-evaluation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'issue')->textInput() ?>
	
    <?= $form->field($model, 'status')->dropDownList(['Sudah Ditangani' => 'Sudah Ditangani', 'Belum Ditangani' => 'Belum Ditangani'], ['class' => 'form-control']) ?>

    <?= $form->field($model, 'follow_up')->textarea(['rows' => 3]) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<script>
$('#ajaxCrudModal').on('shown.bs.modal', function() {
  $('#activityevaluation-actual_note').summernote();
})
</script>