<?php

use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\Url;

?>
<div class="vw-documentations">
    <?= GridView::widget([
            'id' => 'documentations',
            'dataProvider' => $dataProvider,
            'layout' => '{items}{pager}',
            'pjax' => true,
            'emptyText' => 'Tidak ada data Dokumentasi.',
            'tableOptions' => [
                'class' => 'table table-responsive table-striped table-bordered'
            ],
            'columns' =>  [
                [
                    'class' => 'kartik\grid\SerialColumn'
                ],
                [
                    'attribute' => 'mov_name',
                    'value' => function ($model) {
                        return ($model->mov_name) ?: '-';
                    },
                ],
                [
                    'attribute' => 'files_id',
                    'format' => 'raw',
                    'value' => function($model){
                        return $model->files_id ? Html::a($model->file->name, $model->file->storage === 0 ? [$model->file->location] : $model->file->location, ['target' => '_blank', 'data-pjax' => 0]) : '-';
                    }
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'urlCreator' => function($action, $model, $key, $index) { 
                            return Url::to([$action,'id'=>$key]);
                    },   
                    'template' => '{update}&nbsp;&nbsp;{delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    ['update-documentations', 'id' => $model->id], 
                                    [
                                        'title' => 'Ubah Dokumentasi ini',
                                        'role' => 'modal-remote',
                                        'style' => 'color: var(--warning) !important'
                                    ]
                                );
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="fa fa-trash"></span>',
                                ['delete-documentations', 'id' => $model->id], 
                                [
                                    'title' => 'Hapus Dokumentasi ini',
                                    'role'=>'modal-remote',
                                    'data-confirm'=>false, 
                                    'data-method'=>false,
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Konfirmasi',
                                    'data-confirm-message'=>'Yakin ingin menghapus Dokumentasi ini?',
                                    'style' => 'color: var(--danger) !important'
                                ]
                            );
                        },
                    ],
                    'width' => '90px'
                ],
            ],
        ]) ?>
</div>