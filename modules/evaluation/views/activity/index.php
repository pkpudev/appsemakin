<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\select2\Select2;

$this->title = 'Evaluasi Kegiatan Kerja';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="cost-type-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                /* ['content'=>
                    Html::a('<i class="glyphicon glyphicon-send"></i> Kirim', ['send'],
                    ['role'=>'modal-remote', 'class'=>'btn btn-warning', 'title'=>'Reset Grid']) .
                    ($isManager ? Html::a('<i class="fa fa-check"></i> Approve', ['approve'],
                    ['role'=>'modal-remote', 'class'=>'btn btn-success', 'title'=>'Reset Grid']) : '')
                ], */
                [
                    'content' => Html::a(
                        '<i class="glyphicon glyphicon-plus"></i> Tambah Kegiatan',
                        ['/strategic/activity/create'],
                        ['role' => 'modal-remote', 'title' => 'Tambah Kegiatan', 'class' => 'btn btn-success']
                    )
                    ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Daftar Evaluasi Kegiatan Kerja',
                'before'=>'<div class="row">' .
                            '<div class="col-md-2">' .
                                DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'year',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'year-filter',
                                        'placeholder' => 'Filter Tahun',
                                        'class' => 'form-control additional-filter',
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'startView'=>'year',
                                        'minViewMode'=>'years',
                                        'format' => 'yyyy',
                                        'allowClear' => false
                                    ]
                                ]) .
                            '</div>' .
                            '<div class="col-md-2">' .
                                Select2::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'period',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'period-filter',
                                        'class' => 'form-control additional-filter',
                                        'placeholder' => '-- Filter Period --',
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                    'data' => [
                                        'Kuartal 1' => 'Kuartal 1', 
                                        'Kuartal 2' => 'Kuartal 2',
                                        'Kuartal 3' => 'Kuartal 3',
                                        'Kuartal 4' => 'Kuartal 4',
                                    ],
                                ]) .
                            '</div>' .
                            '<div class="col-md-3">' .
                                Select2::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'unit_id',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'divisi-filter',
                                        'class' => 'form-control additional-filter',
                                        'placeholder' => '-- Filter Unit Kerja --',
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                    'data' => $listUnit,
                                ]) .
                            '</div>' .
                        '</div>',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>