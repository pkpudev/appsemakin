<?php

use app\models\Okr;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="activity-evaluation-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'okr_id')->widget(Select2::classname(), [
		'data' => ArrayHelper::map(Okr::find()
										->where(['pic_id' => $model->pic_id])
										->andWhere(['type' => 'KR'])
										// ->andWhere(['position_structure_id' => $model->position_structure_id])
										->andWhere(['year' => $model->initiative->okr->year])
										->orderBy('okr_code asc')
										->all(), 'id', function($model){
			return $model->okr_code . ' - ' . $model->okr;
		}),
		'theme' => Select2::THEME_BOOTSTRAP,
		'options' => [
						'prompt' => '-- Pilih OKR --',
					],
		'pluginOptions' => [
			'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
			]
	])->label('OKR'); ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
