<?php

use app\models\ActivityIssue;
use app\models\ActivityMov;
use app\models\Employee;
use app\models\VwEmployeeManager;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header'=>'Unit',
        'filter'=>false,
        'format'=>'html',
        'value'=>function($model, $key, $index, $widget){
            return '<b>'./*@$model->vwDepartment->department_code*/substr($model->initiative->position->unit->unit_code, 0, -8).' '.$model->initiative->position->unit->unit_name.'</b>';
        },
        'group'=>true,
        'groupedRow'=>true,                    // move grouped column to a single grouped row
        'groupOddCssClass'=>'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass'=>'kv-grouped-row',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'initiatives_code',
        'header'=>'Kode Program Kerja',
        'value'=>function($model){
            return $model->initiative->initiatives_code;
        },
        'group'=>true,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Program Kerja',
        'attribute' => 'initiatives',
        'value'=>function($model){
            return $model->initiative->workplan_name?:'';
        },
        'group'=>true,
        'width' => '250px',
        'subGroupOf'=>1
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'activity_code',
        'label' => 'Kode Kegiatan',
        'value'=>function($model){
            return $model->activity_code?:'';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Nama Kegiatan',
        'attribute'=>'activity_name',
        'value'=>function($model){
            return $model->activity_name ? $model->activity_name /* . ' (' . $model->weight . '%)' */:'';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Tanggal Mulai ',
        'attribute'=>'start_plan',
        'value'=>function($model){
            return $model->start_plan?:'';
        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Tanggal Akhir ',
        'attribute'=>'finish_plan',
        'value'=>function($model){
            return $model->finish_plan?:'';
        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'pic_id',
        'label' => 'Ditugaskan Kepada',
        'format' => 'raw',
        'value'=>function($model){
            $upperId = VwEmployeeManager::findOne(['position_id' => $model->position_structure_id])->upper_id;
            if(Yii::$app->user->id == $upperId || Yii::$app->user->can('Admin')) $button = Html::a('<span class="fa fa-pencil"></span>', ['update-pic', 'id' => $model->id], ['role' => 'modal-remote', 'style' => 'color: var(--warning) !important;']);
            return '<table width="100%"><tr><td>' . ($model->pic->full_name?:'') . '</td><td width="15px">' . $button . '</td></table>';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(Employee::find()->where(['is_employee' => 1])->andWhere(['company_id' => 1])->andWhere(['user_status' => 1])->andWhere(['employee_type_id' => [1,2,5]])->orderBy('full_name')->asArray()->all(), 'id', 'full_name'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
        'width' => '150px'
    ],
    /* [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Bobot',
        'attribute' => 'weight',
        'value' => function($model){
            return $model->weight ? $model->weight . '%': '-';
        }
    ], */
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'actual_value',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td align="center">' .
                                ($model->actual_value? $model->actual_value:0) .
                            '</td>
                            <td width="10px">' .
                                Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['actual-value', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Nilai Realisasi Kegiatan',
                                            'style' => 'color: var(--warning) !important'
                                        ]) .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'documentation',
        'label' => 'Alat Verifikasi',
        'format' => 'raw',
        'value' => function($model){
            $count = ActivityMov::find()->where(['activity_id' => $model->id])->count();
            return Html::a($count . ' Dokumen', 
                            ['view-documentations', 'id' => $model->id], 
                            [
                                'role' => 'modal-remote',
                                'title' => 'Lihat Alat Verifikasi'
                            ]);
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'hAlign' => 'center'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'actual_note',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->actual_note?:'-') .
                            '</td>
                            <td width="10px">' .
                                Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['actual-note', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Catatan Realisasi Kegiatan',
                                            'style' => 'color: var(--warning) !important'
                                        ]) .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'issue',
        'label' => 'Kendala',
        'format' => 'raw',
        'value' => function($model){
            $count = ActivityIssue::find()->where(['activity_id' => $model->id])->count();
            return Html::a($count . ' Kendala', 
                            ['view-issue', 'id' => $model->id], 
                            [
                                'role' => 'modal-remote',
                                'title' => 'Lihat Kendala'
                            ]);
        },
        'width' => '100px',
        'contentOptions' => ['style' => 'background: #eee;'],
        'hAlign' => 'center'
    ],
    [
        'attribute' => 'okr_id',
        'label' => 'Kode OKR',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->okr->okr_code?:'-') .
                            '</td>
                            <td width="10px">' .
                                Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['set-okr', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Key Result',
                                            'style' => 'color: var(--warning) !important'
                                        ]) .
                            '</td>
                        </tr>
                    </table>';
        },
    ],
    /* [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'],
    ], */

];
