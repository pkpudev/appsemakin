<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
?>
<div class="activity-evaluation-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'evaluation_id',
            'activity_id',
            'actual_value',
            'actual_note:ntext',
        ],
    ]) ?>

</div>
