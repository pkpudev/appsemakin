<?php

use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\Url;

?>
<div class="vw-issue">
    <?= GridView::widget([
            'id' => 'issue',
            'dataProvider' => $dataProvider,
            'layout' => '{items}{pager}',
            'pjax' => true,
            'emptyText' => 'Tidak ada data Isu.',
            'tableOptions' => [
                'class' => 'table table-responsive table-striped table-bordered'
            ],
            'columns' =>  [
                [
                    'class' => 'kartik\grid\SerialColumn'
                ],
                [
                    'attribute' => 'issue',
                    'value' => function ($model) {
                        return ($model->issue) ?: '-';
                    },
                ],
                [
                    'attribute' => 'follow_up',
                    'value' => function ($model) {
                        return ($model->follow_up) ?: '-';
                    },
                ],
                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return ($model->status) ?: '-';
                    },
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'urlCreator' => function($action, $model, $key, $index) { 
                            return Url::to([$action,'id'=>$key]);
                    },   
                    'template' => '{update}&nbsp;&nbsp;{delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    ['update-issue', 'id' => $model->id], 
                                    [
                                        'title' => 'Ubah Isu ini',
                                        'role' => 'modal-remote',
                                        'style' => 'color: var(--warning) !important'
                                    ]
                                );
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="fa fa-trash"></span>',
                                ['delete-issue', 'id' => $model->id], 
                                [
                                    'title' => 'Hapus Isu ini',
                                    'role'=>'modal-remote',
                                    'data-confirm'=>false, 
                                    'data-method'=>false,
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Konfirmasi',
                                    'data-confirm-message'=>'Yakin ingin menghapus Isu ini?',
                                    'style' => 'color: var(--danger) !important'
                                ]
                            );
                        },
                    ],
                    'width' => '90px'
                ],
            ],
        ]) ?>
</div>