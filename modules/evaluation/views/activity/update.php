<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
?>
<div class="activity-evaluation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
