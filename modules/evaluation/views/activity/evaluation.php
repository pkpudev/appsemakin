<?php

$this->title = 'Evaluasi Kegiatan';
?>
<div class="evaluation">

    <div class="row">
        <?php foreach($evaluations as $evaluation): ?>
            <div class="col-md-3">
                <a href="/evaluation/activity?ActivityEvaluationSearch[year]=<?= $evaluation->year; ?>&ActivityEvaluationSearch[period]=<?= $evaluation->period; ?>">
                    <div class="box box-default">
                        <div class="box-body" style="text-align: center; height: 100px; padding-top: 35px; font-size: 20px; color: var(--linkColor);">
                            Evaluasi Kegiatan <?= $evaluation->period . ' tahun ' . $evaluation->year; ?>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>

</div>