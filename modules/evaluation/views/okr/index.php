<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\select2\Select2;

$this->title = 'Evaluasi OKR - ' . (Yii::$app->controller->action->id == 'individual' ? 'Individu' : 'Unit');
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="cost-type-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => true,
            'filterSelector' => '.additional-filter',
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => (Yii::$app->controller->action->id == 'individual' ? require(__DIR__.'/_columns.php') : require(__DIR__.'/_columns_unit.php')),
            'toolbar'=> [
                /* ['content'=>
                    Html::a('<i class="glyphicon glyphicon-send"></i> Kirim', ['send', 'type' => (Yii::$app->controller->action->id == 'individual' ? 'individual' : 'unit')],
                    ['role'=>'modal-remote', 'class'=>'btn btn-warning', 'title'=>'Kirim']) .
                    (($isManager || $isGeneralManager) && Yii::$app->controller->action->id == 'individual' ? Html::a('<i class="fa fa-check"></i> Disetujui', ['approve', 'type' => 'individual'],
                    ['role'=>'modal-remote', 'class'=>'btn btn-success', 'title'=>'Setujui']) : '') .
                    ($isGeneralManager && Yii::$app->controller->action->id == 'unit' ? Html::a('<i class="fa fa-check"></i> Disetujui', ['approve', 'type' => 'unit'],
                    ['role'=>'modal-remote', 'class'=>'btn btn-success', 'title'=>'Setujui']) : '')
                ], */
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Daftar Evaluasi OKR - ' . (Yii::$app->controller->action->id == 'individual' ? 'Individu' : 'Unit'),
                'before'=>'<table width="400px"><tr>' .
                            '<td width="50%">' .
                                DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'year',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'year-filter',
                                        'placeholder' => 'Filter Tahun',
                                        'class' => 'form-control additional-filter',
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'startView'=>'year',
                                        'minViewMode'=>'years',
                                        'format' => 'yyyy',
                                        'allowClear' => false
                                    ]
                                ]) .
                            '</td>' .
                            '<td width="50%" style="padding-left: 5px;">' .
                                Select2::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'unit_id',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'divisi-filter',
                                        'class' => 'form-control additional-filter',
                                        'placeholder' => '-- Filter Unit Kerja --',
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                    'data' => $listUnit,
                                ]) .
                            '</td>' .
                            '</tr></table>',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>