<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
/* @var $form yii\widgets\ActiveForm */
$arr = explode('; ', $model->function);
if(in_array('Pengendali', $arr)) $model->item_functions[0] = 'Pengendali';
if(in_array('Perencana', $arr)) $model->item_functions[1] = 'Perencana';
if(in_array('Pelaksana', $arr)) $model->item_functions[2] = 'Pelaksana';
?>

<div class="activity-evaluation-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'item_functions[0]', ['options' => ['class' => 'form-group']])->checkbox([
		'uncheck'      => null,
		'label'        => 'Pengendali',
		'value'        => 'Pengendali',
		'checked' => true
	]);?>

	<?= $form->field($model, 'item_functions[1]', ['options' => ['class' => 'form-group']])->checkbox([
		'uncheck'      => null,
		'label'        => 'Perencana',
		'value'        => 'Perencana',
		'checked' => true
	]);?>

	<?= $form->field($model, 'item_functions[2]', ['options' => ['class' => 'form-group']])->checkbox([
		'uncheck'      => null,
		'label'        => 'Pelaksana',
		'value'        => 'Pelaksana',
		'checked' => true
	]);?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
