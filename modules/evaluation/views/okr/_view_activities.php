<?php

use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\Url;

?>
<div class="vw-documentations">
    
    <b>Objective:</b><br />
    <?= $okrEvaluation->okr->parent->okr_code . ' - ' . $okrEvaluation->okr->parent->okr; ?><br />
    <br />
    <b>Key Result:</b><br />
    <?= $okrEvaluation->okr->okr_code . ' - ' . $okrEvaluation->okr->okr; ?><br />
    <br />
    <b>Target:</b><br />
    <?= $okrEvaluation->target ? number_format($okrEvaluation->target, 0, ',', '.') /* . '% ' */ .  ($okrEvaluation->okr->measure == '%' ? '':' ') . $okrEvaluation->okr->measure: '' ?><br />
    <br />
    <b>Realisasi:</b><br />
    <?= $okrEvaluation->real? number_format($okrEvaluation->real, 0, ',', '.') . ($okrEvaluation->okr->measure == '%' ? '':' ') . $okrEvaluation->okr->measure :0 ?><br />
    <br />

    <?= GridView::widget([
            'id' => 'documentations',
            'dataProvider' => $dataProvider,
            'layout' => '{items}{pager}',
            'pjax' => true,
            'emptyText' => 'Tidak ada data Dokumentasi.',
            'tableOptions' => [
                'class' => 'table table-responsive table-striped table-bordered'
            ],
            'columns' =>  [
                [
                    'class' => 'kartik\grid\SerialColumn'
                ],
                [
                    'attribute' => 'activity',
                    'value' => function ($model) {
                        return $model->activity ?: '-';
                    },
                ],
                [
                    'attribute' => 'start_date',
                    'value' => function ($model) {
                        return $model->start_date ? date('Y-m-d', strtotime($model->start_date)): '-';
                    },
                ],
                [
                    'attribute' => 'finish_date',
                    'value' => function ($model) {
                        return $model->finish_date ? date('Y-m-d', strtotime($model->finish_date)): '-';
                    },
                ],
                [
                    'attribute' => 'okr_value',
                    'value' => function ($model) {
                        return $model->okr_value ? $model->okr_value . '%': '-';
                    },
                ],
            ],
        ]) ?>
</div>