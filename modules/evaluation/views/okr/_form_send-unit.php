<?php

use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
/* @var $form yii\widgets\ActiveForm */

$unitId = [];
$isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one();
$isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one();
$isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one();
$isVicePrecident = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 8])->andWhere(['is_active' => true])->one();
if($isManager){
	$vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['year' => date('Y')])->all();
	foreach($vwEmployeeManagers as $vwEmployeeManager){
		array_push($unitId, $vwEmployeeManager->unit_id);
	}
} 
if($isGeneralManager){
	$vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['year' => date('Y')])->all();
	foreach($vwEmployeeManagers as $vwEmployeeManager){
		array_push($unitId, $vwEmployeeManager->unit_id);
	}

	if(Yii::$app->user->id == 38080){
		$employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
		foreach($employeePositions as $ep){
			// array_push($unitId, $ep->position->unit_structure_id);

			if($ep->position->level == 6){
				$gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
				foreach($gms as $gm){
					array_push($unitId, $gm->unit_structure_id);
				}
			}

		}
	}
}
if($isVicePrecident){
	$vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 8])->andWhere(['year' => date('Y')])->all();
	foreach($vwEmployeeManagers as $vwEmployeeManager){
		array_push($unitId, $vwEmployeeManager->unit_id);
	}

}
?>

<div class="activity-evaluation-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'year')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
                <?= $form->field($model, 'unit_structure_id')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => [
                                    'placeholder' => '-- Pilih Unit Struktur --'
                                ],
                    'select2Options' => [
                        'pluginOptions' => [
                                                'allowClear' => true,
                                                'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                            ]
                    ],
                    'pluginOptions' => [
                        'depends' => ['okr-year'],
                        'url' => Url::to(['/leadership/structure/get-units-by-year']),
                    ]
                ])->label('Unit'); ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
