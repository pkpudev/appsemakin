<?php

use kartik\file\FileInput;
use kartik\form\ActiveForm;

$model->storages = 0;
?>
<div class="add-file">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mov_name')->textInput(); ?>

    <?php if($model->isNewRecord): ?>
        <?= $form->field($model, 'storages')->radioList([0 => 'Upload File', 1 => 'Link'], ['style'=>'display:block ruby;'])->label('Tipe'); ?> 

        <div id="files-place">
            <i style="color: var(--danger) !important">Pastikan nama file tidak ada karater pagar (#), petik (', "), atau karakter unik lainya.</i>

            <?= $form->field($model, 'files[]')->widget(FileInput::classname(), [
                'options' => [
                    'multiple' => true,
                    'accept' => $isPdfOnly ? 'application/pdf' : ''
                ],    
                'pluginOptions' => [
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => false,
                    'showUpload' => false
                ]
            ])->label('File'); ?>
        </div>

        <div id="links-place" style="display: none;">
            <i style="color: var(--danger) !important">Pastikan link yang dilampirkan beserta tulisan http/https nya.</i>

            <?= $form->field($model, 'links')->textInput()->label('Link'); ?>
        </div>
    <?php endif; ?>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $('input[type="radio"]').on('click change', function(e) {
        // console.log(this.value);
        if(this.value == 0){
            $('#files-place').show();
            $('#links-place').hide();
        } else {
            $('#files-place').hide();
            $('#links-place').show();
        }
    });
</script>

