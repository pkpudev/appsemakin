<?php

use app\models\Activity;
use app\models\OkrOrgMov;
use app\models\PositionStructureEmployee;
use app\models\Status;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'parent_id',
        'label' => 'Unit',
        'value' => function($model){
            return $model->okr->parent->position->unit->unit_name ?: 'Organisasi';
        },
        'group' => true,
        'groupedRow' => true,                    // move grouped column to a single grouped row
        'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
    ],
    [
        'attribute' => 'parent_okr_code',
        'label' => 'Objective',
        'format' => 'raw',
        'value' => function($model){
            return '<b>' . $model->okr->parent->okr_code . '</b><br />' . $model->okr->parent->okr;
        },
        'group' => true,
        'subGroupOf' => 1,
        'width' => '250px'
    ],
    [
        'attribute' => 'okr_code',
        'label' => 'Key Result',
        'format' => 'raw',
        'value' => function($model){
            return '<b>' . $model->okr->okr_code . '</b><br />' . $model->okr->okr;
        },
        'group' => true,
        'subGroupOf' => 1,
        'width' => '250px'
    ],
    /*[ 
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'parent_okr_code',
        'label' => 'Objective Code',
        'value' => function($model){
            return $model->okr->parent->okr_code;
        },
        'width' => '120px',
        'group' => true,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'parent_okr',
        'label' => 'Objective',
        'value' => function($model){
            return $model->okr->parent->okr;
        },
        'width' => '200px',
        'group' => true,
        'subGroupOf' => 1
    ],
    [
        'attribute' => 'okr_code',
        'label' => 'Key Result Code',
        'value' => function($model){
            return $model->okr->okr_code;
        },
        'width' => '120px',
        'group' => true,
        'subGroupOf' => 1
    ], */
    [
        'label' => 'Target',
        'value' => function($model){
            return number_format($model->okr->target, 2, ",", ".") . ' ' . $model->okr->measure ;
        },
        'group' => true,
        'subGroupOf' => 3
    ],
    [
        'label' => 'Target K1',
        'value' => function($model){
            return number_format($model->okr->target_q1, 2, ",", ".") . ' ' . $model->okr->measure ;
        },
        'group' => true,
        'subGroupOf' => 3
    ],
    [
        'label' => 'Target K2',
        'value' => function($model){
            return number_format($model->okr->target_q2, 2, ",", ".") . ' ' . $model->okr->measure ;
        },
        'group' => true,
        'subGroupOf' => 3
    ],
    [
        'label' => 'Target K3',
        'value' => function($model){
            return number_format($model->okr->target_q3, 2, ",", ".") . ' ' . $model->okr->measure ;
        },
        'group' => true,
        'subGroupOf' => 3
    ],
    [
        'label' => 'Target K4',
        'value' => function($model){
            return number_format($model->okr->target_q4, 2, ",", ".") . ' ' . $model->okr->measure ;
        },
        'group' => true,
        'subGroupOf' => 3
    ],
    [
        'label' => 'Departemen',
        'attribute' => 'unit_structure_id',
        'format' => 'raw',
        'value' => function($model){
            return $model->unit_structure_id ? $model->unit->unit_name : '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(\app\models\UnitStructure::find()->where(['year' => $year])->andWhere(['level' => 2])->orderBy('unit_name')->asArray()->all(), 'id', 'unit_name'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>'', 'multiple' => true],
        'width' => '150px'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'role',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->role ?: '-') .
                            '</td>
                            <td width="10px">' .
                                (true ? Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['set-role', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Peran Unit',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'function',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->function ?: '-') .
                            '</td>
                            <td width="10px">' .
                            (true ?  Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['set-function', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Fungsi Unit',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'weight',
        'value' => function($model){
            return $model->weight ? number_format($model->weight, 2, ',', '.') . '%': '-';
        },
        'hAlign' => 'center',
        'width' => '50px'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'target',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->target ? number_format($model->target, 2, ',', '.') . ' ' . $model->okr->measure: '-') .
                            '</td>
                            <td width="10px">' .
                            (true ?  Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['set-target', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Target',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'hAlign' => 'center',
        'width' => '150px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'real',
        'label' => 'Realisasi KR',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->real? number_format($model->real, 0, ',', '.') . ' ' .$model->okr->measure :0) .
                            '</td>
                            <td width="10px">' .
                                ($model->status_id == Status::OKR_ORG_E_DRAFT ? Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['real', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Nilai Realisasi OKR',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'note',
        'label' => 'Catatan KR',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->note?: '-') .
                            '</td>
                            <td width="10px">' .
                            ($model->status_id == Status::OKR_ORG_E_DRAFT ?  Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['note', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Catatan OKR',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '200px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'output',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->output?:'-') .
                            '</td>
                            <td width="10px">' .
                                (true ? Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['output', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Output OKR',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '350px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Alat Verifikasi',
        'format' => 'raw',
        'value' => function($model){
            $count = OkrOrgMov::find()->where(['okr_evaluation_id' => $model->id])->count();
            return Html::a($count . ' Dokumen', 
                            ['view-documentations', 'id' => $model->id, 'okr_id' => $model->okr_id], 
                            // ['view-documentations', 'id' => $model->okr_id, 'unitId' => $model->unit_structure_id], 
                            [
                                'role' => 'modal-remote',
                                'title' => 'Lihat Alat Verifikasi'
                            ]);
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'hAlign' => 'center'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'performance_analysis',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->performance_analysis ?: '-') .
                            '</td>
                            <td width="10px">' .
                            ($model->status_id == Status::OKR_ORG_E_SENT ?  Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['performance-analysis', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Analisis Capaian',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'interpretation',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->interpretation ?: '-') .
                            '</td>
                            <td width="10px">' .
                            ($model->status_id == Status::OKR_ORG_E_SENT ?  Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['interpretation', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Interpretasi',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Status',
        'attribute' => 'status_id',
        'format' => 'raw',
        'width' => '60px',
        'value' => function ($model) {
            return ($model->status_id) ? $model->status->getStatusname($model->status_id) : '-';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(Status::find()->where(['scenario' => Status::SCENARIO_OKR_ORG_EVALUATION])->orderBy('id')->asArray()->all(), 'id', 'status'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
];
