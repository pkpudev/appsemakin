<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OkrEvaluation */

?>
<div class="okr-evaluation-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
