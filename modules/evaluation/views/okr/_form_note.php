<?php

use kartik\editors\Summernote;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-evaluation-form">

    <?php $form = ActiveForm::begin(); ?>

	<?php /* $form->field($model, 'actual_note')->widget(Summernote::class, [
		'useKrajeePresets' => true,
		'pluginOptions' => [
			'height' => 100,
			'dialogsFade' => true,
			'toolbar' => [
				['style1', ['style']],
				['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
				['para', ['ul', 'ol', 'paragraph', 'height']],
				['insert', ['table', 'hr']],
			],
		]
	]); */ ?>

	<?= $form->field($model, 'note')->textarea(['rows' => 3]); ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<script>
$('#ajaxCrudModal').on('shown.bs.modal', function() {
  $('#activityevaluation-actual_note').summernote();
})
</script>