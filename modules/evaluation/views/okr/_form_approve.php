<?php

use app\models\Calculation;
use app\models\CascadingType;
use app\models\OkrLevel;
use app\models\Polarization;
use app\models\PositionStructure;
use app\models\UnitStructure;
use app\models\Validity;
use app\models\VwEmployeeManager;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Okr */
/* @var $form yii\widgets\ActiveForm */

$unitId = [];
$vwEmployeeManagers = VwEmployeeManager::find()
											->where(['employee_id' => Yii::$app->user->id])
											->andWhere('level = 4 or level = 6')
											->andWhere(['is_active' => true])
											->andWhere(['year' => date('Y')])
											->all();
foreach($vwEmployeeManagers as $vwEmployeeManager){
	array_push($unitId, $vwEmployeeManager->unit_id);
}

?>

<div class="okr-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'year')->widget(DatePicker::classname(), [
		'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
		'pluginOptions' => [
			'autoclose'=>true,
			'todayHighlight' => true,
			'startView'=>'year',
			'minViewMode'=>'years',
			'format' => 'yyyy'
		]
	]) ?>

    <div class="row">
        <div class="col-md-4">
                <?= $form->field($model, 'unit_structure_id')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => [
                                    'placeholder' => '-- Pilih Unit Struktur --'
                                ],
                    'select2Options' => [
                        'pluginOptions' => [
                                                'allowClear' => true,
                                                'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                            ]
                    ],
                    'pluginOptions' => [
                        'depends' => ['okr-year'],
                        'url' => Url::to(['/leadership/structure/get-units-by-year']),
                    ]
                ])->label('Unit'); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'position_structure_id')->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => [
                                'placeholder' => 'Pilih Jabatan'
                            ],
                'select2Options' => [
                    'pluginOptions' => [
                                            'allowClear' => true,
                                            'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                        ]
                ],
                'pluginOptions' => [
                    'depends' => ['okr-unit_structure_id'],
                    'url' => Url::to(['/leadership/structure/get-positions2']),
                ]
            ])->label('Jabatan'); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'pic_id')->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => [
                                'placeholder' => 'Pilih PIC'
                            ],
                'select2Options' => [
                    'pluginOptions' => [
                                            'allowClear' => true,
                                            'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                        ]
                ],
                'pluginOptions' => [
                    'depends' => ['okr-position_structure_id'],
                    'url' => Url::to(['/leadership/structure/get-persons']),
                ]
            ]); ?>
        </div>
    </div>   


      
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>