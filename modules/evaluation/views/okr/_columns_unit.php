<?php

use app\models\Activity;
use app\models\Employee;
use app\models\Evaluation;
use app\models\Initiatives;
use app\models\InitiativesEvaluation;
use app\models\OkrEvaluationDocumentations;
use app\models\PositionStructureEmployee;
use app\models\Status;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'parent_id',
        'label' => 'Unit',
        'value' => function($model){
            return $model->okr->parent->position->unit->unit_name ?: 'Organisasi';
        },
        'group' => true,
        'groupedRow' => true,                    // move grouped column to a single grouped row
        'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'parent_okr_code',
        'label' => 'Objective Code',
        'value' => function($model){
            return $model->okr->parent->okr_code;
        },
        'width' => '120px',
        'group' => true,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'parent_okr',
        'label' => 'Objective',
        'value' => function($model){
            return $model->okr->parent->okr;
        },
        'width' => '200px',
        'group' => true,
        'subGroupOf' => 1
    ],
    [
        'attribute' => 'okr_code',
        'label' => 'Key Result Code',
        'value' => function($model){
            return $model->okr->okr_code;
        },
        'width' => '120px'
    ],
    [
        'attribute' => 'okr',
        'label' => 'Key Result',
        'value' => function($model){
            return $model->okr->okr;
        },
        'width' => '200px',
    ],
    [
        'label' => 'PIC',
        'attribute' => 'pic_id',
        'format' => 'raw',
        'value' => function($model){
            return $model->okr->pic_id ? $model->okr->pic->full_name . '<br />(' . $model->okr->position->position_name . ')' : '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(Employee::find()->where(['is_employee' => 1])->andWhere(['company_id' => 1])->andWhere(['user_status' => 1])->andWhere(['employee_type_id' => [1,2,5]])->orderBy('full_name')->asArray()->all(), 'id', 'full_name'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
        'width' => '150px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'evaluation_id',
        'label' => 'Waktu Evaluasi',
        'value' => function($model){
            return $model->evaluation->year . ' (' . $model->evaluation->period . ')';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(Evaluation::find()->orderBy('id desc')->all(), 'id', function($model){
            return $model->year . ' (' . $model->period . ')';
        }),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
        'width' => '150px'
    ],
    [
        'attribute' => 'target',
        'value' => function($model){
            return ($model->target ? number_format($model->target, 0, ',', '.') /* . '% ' */ .  ($model->okr->measure == '%' ? '':' ') . $model->okr->measure: '');
            // if($model->evaluation->year == 2023){
            // } else {
            //     return round($model->target, 2) . ' %';
            // }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'real',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                    ($model->real? number_format($model->real, 0, ',', '.') . ($model->okr->measure == '%' ? '':' ') . $model->okr->measure :0) .
                                    // (
                                    //     $model->evaluation->year == 2023 ?
                                    //     ($model->real? number_format($model->real, 0, ',', '.') /* . '% ' */ . ($model->okr->measure == '%' ? '':' ') . $model->okr->measure :0) :
                                    //     round($model->real ?: 0, 2) . ' %'
                                    // ) .
                            '</td>
                            <td width="10px">' .
                                ($model->evaluation->use_old_pattern ?
                                Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['real', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Nilai Realisasi OKR',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'achieve_value',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                    (
                                        $model->evaluation->year == 2023 ?
                                        ($model->achieve_value? number_format($model->achieve_value, 0, ',', '.') /* . '% ' */ . ($model->okr->measure == '%' ? '':' ') . $model->okr->measure :0) :
                                        round($model->achieve_value ?: 0, 2) . ' %'
                                    ) .
                            '</td>
                            <td width="10px">' .
                                /* ($model->evaluation->isCanEdit() ?
                                Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['real', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Nilai Realisasi OKR',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') . */
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'documentation',
        'label' => 'Alat Verifikasi',
        'format' => 'raw',
        'value' => function($model){
            $count = OkrEvaluationDocumentations::find()->where(['okr_evaluation_id' => $model->id])->count();
            return Html::a($count . ' Dokumen', 
                            ['view-documentations2', 'id' => $model->id], 
                            [
                                'role' => 'modal-remote',
                                'title' => 'Lihat Alat Verifikasi'
                            ]);
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'hAlign' => 'center'
    ],
    [
        'label' => 'Program Kerja',
        'format' => 'raw',
        'value' => function($model){
            $initiativesText = '';
            $activities = Initiatives::find()->where(['okr_id' => $model->okr->parent_id])->all();

            foreach($activities as $initiatives){

                $modelEvaluation = InitiativesEvaluation::findOne(['evaluation_id' => $model->evaluation_id, 'initiatives_id' => $initiatives->id]);
                if($modelEvaluation->real){
                    $persentation = '. <b>(Realisasi: ' . number_format((($modelEvaluation->real / $modelEvaluation->target)*100),2,',','.') . '%)</b>';
                }

                $initiativesText .= '&#8226; ' . $initiatives->workplan_name . $persentation . '</br>';
                $persentation = '';
            }
            return $initiativesText ?: '-';
        },
        /* 'value' => function($model){
            $initiativesText = '<table class="kv-grid-table table table-bordered table-striped table-condensed kv-table-wrap">';
            $activities = Initiatives::find()->where(['okr_id' => $model->okr->parent_id])->all();
            foreach($activities as $initiatives){
                $initiativesText .= '<tr><td>' . $initiatives->workplan_name . '</td><td>- %</td></tr>';
            }
            $initiativesText .= '</table>';
            return $initiativesText ?: '-';
        }, */
        'group' => true,
        'subGroupOf' => 1,
        'width' => '400px'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Status',
        'attribute' => 'status_id',
        'format' => 'raw',
        'width' => '60px',
        'value' => function ($model) {
            return ($model->status_id) ? $model->status->getStatusname($model->status_id) : '-';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(app\models\Status::find()->andWhere(['scenario' => Status::SCENARIO_OKR_EVALUATION])->orderBy('id')->asArray()->all(), 'id', 'status'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
];
