<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OkrEvaluation */
?>
<div class="okr-evaluation-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'evaluation_id',
            'okr_id',
            'target',
            'real:ntext',
        ],
    ]) ?>

</div>
