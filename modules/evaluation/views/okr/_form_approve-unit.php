<?php

use app\models\Calculation;
use app\models\CascadingType;
use app\models\OkrLevel;
use app\models\Polarization;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\UnitStructure;
use app\models\Validity;
use app\models\VwEmployeeManager;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Okr */
/* @var $form yii\widgets\ActiveForm */

if(Yii::$app->user->can('Admin')){
    $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => date('Y')])/* ->andWhere(['level' => [3, 5]]) */->orderBy('unit_code')->all(), 'id', function($model){return $model->unit_name;});
} else {
    $userID = Yii::$app->user->id;
    $unitId = [];
    $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
    foreach($employeePositions as $ep){
        // array_push($unitId, $ep->position->unit_structure_id);
    
        if($ep->position->level == 6){
            $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
            foreach($gms as $gm){
                array_push($unitId, $gm->unit_structure_id);
            }
        }
        if($ep->position->level == 8){
            $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
            foreach($gms as $gm){
                array_push($unitId, $gm->unit_structure_id);
            }
        }
    }

    $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $unitId])->orderBy('unit_code')->all(), 'id', function($model){return $model->unit_name;});
}

?>

<div class="okr-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'year')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
                <?= $form->field($model, 'unit_structure_id')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => [
                                    'placeholder' => '-- Pilih Unit Struktur --'
                                ],
                    'select2Options' => [
                        'pluginOptions' => [
                                                'allowClear' => true,
                                                'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                            ]
                    ],
                    'pluginOptions' => [
                        'depends' => ['okr-year'],
                        'url' => Url::to(['/leadership/structure/get-units-by-year']),
                    ]
                ])->label('Unit'); ?>
        </div>
    </div>  
      
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>