<?php

use app\models\PositionStructureEmployee;
use app\models\UnitStructure;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
/* @var $form yii\widgets\ActiveForm */

// $userID = Yii::$app->user->id;
// $listPosition = ArrayHelper::map(PositionStructureEmployee::find()->where(['employee_id' => $userID])->all(), 'position_structure_id', function($model){
//     return $model->position->position_name;
// });
?>

<div class="activity-evaluation-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'year')->widget(DatePicker::classname(), [
		'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
		'pluginOptions' => [
			'autoclose'=>true,
			'todayHighlight' => true,
			'startView'=>'year',
			'minViewMode'=>'years',
			'format' => 'yyyy'
		]
	]) ?>

	<?= $form->field($model, 'position_structure_id')->widget(DepDrop::classname(), [
		'type' => DepDrop::TYPE_SELECT2,
		'options' => [
						'placeholder' => 'Pilih Jabatan'
					],
		'select2Options' => [
			'pluginOptions' => [
									'allowClear' => true,
									'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
								]
		],
		'pluginOptions' => [
			'depends' => ['okr-year'],
			'url' => Url::to(['/leadership/structure/get-positions-by-year']),
		]
	])->label('Jabatan'); ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
