<?php

use app\models\Activity;
use app\models\OkrOrgMov;
use app\models\PositionStructureEmployee;
use app\models\Status;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'parent_id',
        'label' => 'Unit',
        'value' => function($model){
            return $model->okr->parent->position->unit->unit_name ?: 'Organisasi';
        },
        'group' => true,
        'groupedRow' => true,                    // move grouped column to a single grouped row
        'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'parent_okr_code',
        'label' => 'Objective Code',
        'value' => function($model){
            return $model->okr->parent->okr_code;
        },
        'width' => '120px',
        'group' => true,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'parent_okr',
        'label' => 'Objective',
        'value' => function($model){
            return $model->okr->parent->okr;
        },
        'width' => '200px',
        'group' => true,
        'subGroupOf' => 1
    ],
    [
        'attribute' => 'okr_code',
        'label' => 'Key Result Code',
        'value' => function($model){
            return $model->okr->okr_code;
        },
        'width' => '120px',
        'group' => true,
        'subGroupOf' => 1
    ],
    [
        'attribute' => 'okr',
        'label' => 'Key Result',
        'value' => function($model){
            return $model->okr->okr;
        },
        'width' => '200px',
        'group' => true,
        'subGroupOf' => 1
    ],
    [
        'label' => 'Target',
        'value' => function($model){
            return number_format($model->okr->target, 2, ",", ".") . ' ' . $model->okr->measure ;
        },
        'group' => true,
        'subGroupOf' => 4
    ],
    [
        'label' => 'Target K1',
        'value' => function($model){
            return number_format($model->okr->target_q1, 2, ",", ".") . ' ' . $model->okr->measure ;
        },
        'group' => true,
        'subGroupOf' => 4
    ],
    [
        'label' => 'Target K2',
        'value' => function($model){
            return number_format($model->okr->target_q2, 2, ",", ".") . ' ' . $model->okr->measure ;
        },
        'group' => true,
        'subGroupOf' => 4
    ],
    [
        'label' => 'Target K3',
        'value' => function($model){
            return number_format($model->okr->target_q3, 2, ",", ".") . ' ' . $model->okr->measure ;
        },
        'group' => true,
        'subGroupOf' => 4
    ],
    [
        'label' => 'Target K4',
        'value' => function($model){
            return number_format($model->okr->target_q4, 2, ",", ".") . ' ' . $model->okr->measure ;
        },
        'group' => true,
        'subGroupOf' => 4
    ],
    [
        'label' => 'Bobot',
        'value' => function($model){
            return number_format($model->weight, 2, ",", ".");
        },
    ],
    [
        'label' => 'Departemen',
        'attribute' => 'unit_structure_id',
        'format' => 'raw',
        'value' => function($model){
            return $model->unit_structure_id ? $model->unit->unit_name : '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(\app\models\UnitStructure::find()->where(['year' => $year])->andWhere(['level' => 2])->orderBy('unit_name')->asArray()->all(), 'id', 'unit_name'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>'', 'multiple' => true],
        'width' => '150px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'id',
        'label' => 'Waktu Evaluasi',
        'value' => function($model){
            return $model->evaluation->year . ' (' . $model->evaluation->period . ')';
        }
    ],
    // [
    //     'attribute' => 'target',
    //     'value' => function($model){
    //         return ($model->target ? $model->target . ' ' . $model->okr->measure: '');
    //     }
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'real',
        'label' => 'Realisasi KR',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->real? number_format($model->real, 0, ',', '.') . ' ' .$model->okr->measure :0) .
                            '</td>
                            <td width="10px">' .
                                (/* $model->status_id == Status::OKR_ORG_E_DRAFT */ true ? Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['real', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Nilai Realisasi OKR',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'note',
        'label' => 'Catatan KR',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->note?: '-') .
                            '</td>
                            <td width="10px">' .
                            (/* $model->status_id == Status::OKR_ORG_E_DRAFT */ true ?  Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['note', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Catatan OKR',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '200px',
    ],
    [
        'attribute' => 'output',
        'value' => function($model){
            return ($model->output ?: '-');
        },
        'width' => '350px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'note',
        'label' => 'Catatan Keluaran',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->output_note?: '-') .
                            '</td>
                            <td width="10px">' .
                            (/* $model->status_id == Status::OKR_ORG_E_DRAFT */ true ?  Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['output-note', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Catatan Keluaran',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '200px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Alat Verifikasi',
        'format' => 'raw',
        'value' => function($model){
            $count = OkrOrgMov::find()->where(['okr_evaluation_id' => $model->id])->count();
            return Html::a($count . ' Dokumen', 
                            ['view-documentations', 'id' => $model->id], 
                            // ['view-documentations', 'id' => $model->okr_id, 'unitId' => $model->unit_structure_id], 
                            [
                                'role' => 'modal-remote',
                                'title' => 'Lihat Alat Verifikasi'
                            ]);
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'hAlign' => 'center'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Status',
        'attribute' => 'status_id',
        'format' => 'raw',
        'width' => '60px',
        'value' => function ($model) {
            return ($model->status_id) ? $model->status->getStatusname($model->status_id) : '-';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(Status::find()->where(['scenario' => Status::SCENARIO_OKR_ORG_EVALUATION])->orderBy('id')->asArray()->all(), 'id', 'status'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },   
        'template' => '{sent}',
        'buttons' => [
            'sent' => function ($url, $model) {
                if ($model->status_id == Status::OKR_ORG_E_DRAFT) return Html::a('<span class="glyphicon glyphicon-send"></span>',
                    ['set-sent-organization', 'id' => $model->id], 
                    [
                        'role'=>'modal-remote', 
                        'title'=>'Kirim',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Konfirmasi',
                        'data-confirm-message'=>'Yakin ingin mengirim OKR Organisasi ini?',
                        'style' => 'color: var(--warning) !important'
                    ]
                );
            },
        ],
        'width' => '70px'
    ],
];
