<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Evaluation */
?>
<div class="evaluation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
