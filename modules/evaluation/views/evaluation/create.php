<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Evaluation */

?>
<div class="evaluation-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
