<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Evaluation */
?>
<div class="evaluation-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'period',
            'year',
            'start_date',
            'end_date',
            'status_id',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
        ],
    ]) ?>

</div>
