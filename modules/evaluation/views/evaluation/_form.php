<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Evaluation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="evaluation-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'year')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'period')->widget(Select2::classname(), [
                    'data' => [
                                'Kuartal 1' => 'Kuartal 1', 
                                'Kuartal 2' => 'Kuartal 2',
                                'Kuartal 3' => 'Kuartal 3',
                                'Kuartal 4' => 'Kuartal 4',
                            ],
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => ['prompt' => '-- Pilih Periode --'],
                    'pluginOptions' => [
                        'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                        ]
                ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal Mulai'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal Akhir'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
    </div>
      
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
