<?php

use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;

CrudAsset::register($this);

$this->title = 'Jam Kerja Individu';
?>
<style>
    .blink {
    animation: blinker 1s linear infinite;
  }

  @keyframes blinker {
    50% {
      opacity: 0;
    }
  }
</style>
<div class="hour">

  <?php if(Yii::$app->user->can('SuperUser')): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-body">
                    <?php
                        $year = Yii::$app->getRequest()->getQueryParam('VwEmployeeManagerSearch')['year'] ?: date('Y');
                        $period = Yii::$app->getRequest()->getQueryParam('VwEmployeeManagerSearch')['period'];
                        
                        if($period == 'Kuartal 1'){
                            $dateStart = $year . '-' . '01-01';
                            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '03-01'));
                        } else if($period == 'Kuartal 2'){
                            $dateStart = $year . '-' . '04-01';
                            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '06-01'));
                        } else if($period == 'Kuartal 3'){
                            $dateStart = $year . '-' . '07-01';
                            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '09-01'));
                        } else if($period == 'Kuartal 4'){
                            $dateStart = $year . '-' . '10-01';
                            $dateEnd = date("Y-m-t", strtotime( $year . '-' . '12-01'));
                        } else {
                            $dateStart = $year . '-' . '01-01';
                            $dateEnd = $year . '-' . '12-31';
                        }

                        $query = Yii::$app->db->createCommand("
                            SELECT 
                                pic_id, 
                                SUM(duration)
                            FROM management.activities 
                            WHERE 
                                parent_id IS NULL AND 
                                (
                                    (cast('$dateStart' as timestamp) >= start_date AND cast('$dateStart' as timestamp) <= due_date) OR 
                                    (cast('$dateEnd' as timestamp) >= start_date AND cast('$dateEnd' as timestamp) <= due_date) OR 
                                    (start_date >= '$dateStart' AND start_date <= '$dateEnd') OR 
                                    (due_date >= '$dateStart' AND due_date <= '$dateEnd') OR 
                                    (cast('$dateStart' as timestamp) <= start_date AND cast('$dateEnd' as timestamp) >= due_date)
                                )
                            group by pic_id
                        ")->queryAll();

                        $abnormalUnderload = 0;
                        $veryUnderload = 0;
                        $underload = 0;
                        $normal = 0;
                        $overload = 0;
                        $veryOverload = 0;
                        $abnormalOverload = 0;

                        $i = 0;
                        foreach($query as $q){

                            if($period){
                                $fte = 519;
                            } else {
                                $fte = 2076;
                            }
                
                            $val = round($q['sum'] / $fte, 2);
                            
                            if($val < 0.5){
                                $abnormalUnderload++;
                            } else if($val >= 0.5 && $val <= 0.8){
                                $veryUnderload++;
                            } else if($val >= 0.81 && $val <= 0.95){
                                $underload++;
                            } else if($val >= 0.96 && $val <= 1.05){
                                $normal++;
                            } else if($val >= 1.06 && $val <= 1.2){
                                $overload++;
                            } else if($val >= 1.21 && $val <= 1.5){
                                $veryOverload++;
                            } else if($val >= 1.51){
                                $abnormalOverload++;
                            }

                            $i++;
                        }
                        
                        $valArr = [];
                        array_push($valArr, ['name' => 'Abnormal Underload', 'y' => $abnormalUnderload]);
                        array_push($valArr, ['name' => 'Very Underload', 'y' => $veryUnderload]);
                        array_push($valArr, ['name' => 'Underload', 'y' => $underload]);
                        array_push($valArr, ['name' => 'Normal', 'y' => $normal]);
                        array_push($valArr, ['name' => 'Overload', 'y' => $overload]);
                        array_push($valArr, ['name' => 'Very Overload', 'y' => $veryOverload]);
                        array_push($valArr, ['name' => 'Abnormal Overload', 'y' => $abnormalOverload]);

                        // var_dump(json_encode($valArr));
                        // var_dump($i);

                        echo \dosamigos\highcharts\HighCharts::widget([
                            'clientOptions' => [
                                'chart' => [
                                        'type' => 'pie'
                                ],
                                'credits' => [
                                    'enabled' => false
                                ],
                                'title' => [
                                    'text' => 'FTE', 
                                    'style' => ['color' => 'var(--borderColor)', 'font-weight' => 'bold', 'font-size' => '17px']
                                ],
                                'plotOptions' => [
                                    'series' => [
                                        'dataLabels' => [
                                            'enabled' => true,
                                            'format' => '{point.name}: {point.y} ({point.percentage:.1f}%)'
                                        ]
                                    ]
                                ],
                                'tooltip' => [
                                    'headerFormat' => '<span style="font-size:11px">{point.name}</span><br>',
                                    'pointFormat' => '{point.name}: {point.y} ({point.percentage:.1f}%)'
                                ],
                                'series' => [
                                    [
                                        'colorByPoint' => true,
                                        'data' => $valArr
                                    ],
                                ],
                            ]
                        ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
  <?php endif; ?>

    <div class="box box-default">
        <div class="box-body">
            <?=  GridView::widget([
                'id'=>'okr-individual',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => '.additional-filter',
                'showPageSummary' => true,
                'responsiveWrap' => false,
                'pjax'=>false,
                'hover' => true,
                'export' => [
                    'label' => 'Export Excel atau PDF',
                    'header' => '',
                    'options' => [
                        'class' => 'btn btn-primary'
                    ], 
                    'menuOptions' => [ 
                            'style' => 'background: var(--background); width: 187px;'
                    ]
                ],
                'exportConfig' => [
                    GridView::PDF => true,
                    GridView::EXCEL => true,
                ],
                'columns' => require(__DIR__.'/_columns-hour.php'),
                'toolbar'=> [
                    ['content'=>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                        '{toggleData}'
                    ],
                    ['content'=>
                        '{export}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => 'Jam Kerja Individu',
                    'before'=>'<table><tr>' .
                                    '<td width="150px">' .
                                        DatePicker::widget([
                                            'model' => $searchModel,
                                            'attribute' => 'year',
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'id' => 'year-filter',
                                                'placeholder' => 'Tahun',
                                                'class' => 'form-control additional-filter',
                                            ],
                                            'removeButton' => false,
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'startView'=>'year',
                                                'minViewMode'=>'years',
                                                'format' => 'yyyy',
                                                'allowClear' => false
                                            ]
                                        ]) .
                                    '</td>' .
                                    '<td width="200px" style="padding-left: 10px;">' .
                                        Select2::widget([
                                            'model' => $searchModel,
                                            'attribute' => 'period',
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'id' => 'divisi-filter',
                                                'class' => 'form-control additional-filter',
                                                'placeholder' => '-- Periode --',
                                            ],
                                            'pluginOptions' => ['allowClear' => true],
                                            'data' => ['Kuartal 1' => 'Kuartal 1', 'Kuartal 2' => 'Kuartal 2', 'Kuartal 3' => 'Kuartal 3', 'Kuartal 4' => 'Kuartal 4'],
                                        ]) .
                                    '</td>' .
                                '</tr></table>',
                    'after'=>'',
                ]
            ])?>
        </div>
    </div>

</div>
<?php
$url = Yii::$app->controller->action->id;
$script = <<<JS
$(document.body).on("change", "#from_date-filter",function(){
    var from_date = jQuery('#from_date-filter').val() || '',
        to_date = jQuery('#to_date-filter').val() || '',
        url = '$url?VwEmployeeManagerSearch[from_date]=' + from_date 
                + '&VwEmployeeManagerSearch[to_date]=' + to_date
    window.location.href = url;
});
    
JS;
$this->registerJs($script, $this::POS_READY);
?>