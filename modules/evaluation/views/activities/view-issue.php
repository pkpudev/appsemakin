<?php

use app\models\Activities;
use yii\widgets\DetailView;

?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
            [
                'attribute'=>'reporting_date',
            ],
            [
                'attribute'=>'issue',
            ],
            [
                'attribute'=>'issue_type_id',
                'value' => function($model){
                    return $model->issue_type_id ? $model->type->issue_type : '-';
                },
            ],
            [
                'attribute'=>'issue_level',
                'value' => function($model){
                    return Activities::ISSUE_LEVEL[$model->issue_level];
                },
            ],
            [
                'attribute'=>'alternative_solution',
            ],
            [
                'attribute'=>'resolution',
            ],
            [
                'attribute'=>'due_date',
            ],
            [
                'attribute'=>'full_name',
                'label' => 'Nama',
                'value' => function($model){
                    return $model->activities->pic->full_name;
                }
            ],
            [
                'label' => 'Durasi Berlangsung Isu',
                'value' => function($model){
                    if($model->reporting_date && $model->due_date){
                        $date1 = date_create($model->reporting_date);
                        $date2 = date_create($model->due_date);
                        $diff = date_diff($date1,$date2);
                        return $diff->format("%R%a hari");
                    } else {
                        return '-';
                    }
                },
            ],
            [
                'attribute'=>'status_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->status->getStatusname($model->status_id);
                },
            ],
        ]
        ]) ?>

