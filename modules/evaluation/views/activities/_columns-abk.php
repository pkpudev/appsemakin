<?php

use app\components\DateHelper;
use app\models\ActivityCategory;
use app\models\Employee;
use app\models\PositionStructure;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'unit_code',
        'label'=>'Kode Unit',
        'value' => function($model){
            return $model->unit_code;
        },
        'width' => '80px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'unit_name',
        'label'=>'Nama Unit',
        'value' => function($model){
            return $model->unit_name;
        },
        'width' => '350px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'SDMSI',
        'value' => function($model){

            $countPerson = Yii::$app->db->createCommand("
                                                        SELECT
                                                            count(distinct employee_id)
                                                        FROM
                                                            management.position_structure__employee pse
                                                        LEFT JOIN management.position_structure ps ON pse.position_structure_id = ps.id
                                                        WHERE
                                                            unit_structure_id = {$model->id} AND
                                                            is_active = true
                                                        ")->queryScalar();

            $model->countPerson = $countPerson ?: 0;
            return $model->countPerson;
        },
        'hAlign' => 'center',
        'width' => '100px',
        'pageSummary' => true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Total ABK',
        'value' => function($model){
            $year = Yii::$app->getRequest()->getQueryParam('UnitStructureSearch')['year'] ?: date('Y');
            $period = Yii::$app->getRequest()->getQueryParam('UnitStructureSearch')['period'];
            
            if($period == 'Kuartal 1'){
                $dateStart = $year . '-' . '01-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '03-01'));
            } else if($period == 'Kuartal 2'){
                $dateStart = $year . '-' . '04-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '06-01'));
            } else if($period == 'Kuartal 3'){
                $dateStart = $year . '-' . '07-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '09-01'));
            } else if($period == 'Kuartal 4'){
                $dateStart = $year . '-' . '10-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '12-01'));
            } else {
                $dateStart = $year . '-' . '01-01';
                $dateEnd = $year . '-' . '12-31';
            }

            $hour = Yii::$app->db->createCommand("
                            SELECT 
                                SUM(duration)
                            FROM management.activities a
                            LEFT JOIN management.okr o ON a.okr_id = o.id
                            LEFT JOIN management.position_structure ps ON o.position_structure_id = ps.id
                            WHERE
                                ps.unit_structure_id = {$model->id} AND
                                a.parent_id IS NULL AND 
                                (
                                    (cast('$dateStart' as timestamp) >= start_date AND cast('$dateStart' as timestamp) <= due_date) OR 
                                    (cast('$dateEnd' as timestamp) >= start_date AND cast('$dateEnd' as timestamp) <= due_date) OR 
                                    (start_date >= '$dateStart' AND start_date <= '$dateEnd') OR 
                                    (due_date >= '$dateStart' AND due_date <= '$dateEnd') OR 
                                    (cast('$dateStart' as timestamp) <= start_date AND cast('$dateEnd' as timestamp) >= due_date)
                                )
                        ")->queryScalar();

            if($period){
                $fte = 519;
            } else {
                $fte = 2076;
            }

            $model->fte = round($hour / $fte, 2);
            return $model->fte;
        },
        'hAlign' => 'center',
        'width' => '100px',
        'pageSummary' => true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'BKU',
        'value' => function($model){
            return $model->countPerson ? round($model->fte/$model->countPerson, 2) : 0;
        },
        'hAlign' => 'center',
        'width' => '100px',
        'pageSummary' => true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'JKSDM',
        'value' => function($model){
            return round($model->fte);
        },
        'hAlign' => 'center',
        'width' => '100px',
        'pageSummary' => true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'+/-',
        'value' => function($model){
            return round(round($model->fte) - $model->countPerson);
        },
        'hAlign' => 'center',
        'width' => '100px',
        'pageSummary' => true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Kategori',
        'format' => 'raw',
        'value' => function($model){
            $val = $model->countPerson ? round($model->fte/$model->countPerson, 2) : 0;

            if($val < 0.5){
                $cat = '<span class="label label-default">Abnormal Underload</span>';
            } else if($val >= 0.5 && $val <= 0.8){
                $cat = '<span class="label label-info">Very Underload</span>';
            } else if($val >= 0.81 && $val <= 0.95){
                $cat = '<span class="label label-primary">Underload</span>';
            } else if($val >= 0.96 && $val <= 1.05){
                $cat = '<span class="label label-success">Normal</span>';
            } else if($val >= 1.06 && $val <= 1.2){
                $cat = '<span class="label label-warning">Overload</span>';
            } else if($val >= 1.21 && $val <= 1.5){
                $cat = '<span class="label label-danger" style="background: #de643b !important;">Very Overload</span>';
            } else if($val >= 1.51){
                $cat = '<span class="blink label label-danger">Ubnormal Overload</span>';
            }

            return $cat;
        },
        'hAlign' => 'center',
        'width' => '100px'
    ],

];
