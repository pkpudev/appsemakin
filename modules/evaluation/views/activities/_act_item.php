<?php

use johnitvn\ajaxcrud\CrudAsset;

CrudAsset::register($this);

?>
<div class="panel panel-default">
    <div class="panel-heading" style="border: 1px solid var(--background); margin: 5px 0; padding: 5px; border-radius: 5px; box-shadow: 3px 3px 5px rgba(0, 0, 0, 0.5); border-left: 5px solid var(--background);">
        <?php //Pjax::begin(['id' => "act-{$model->id}-pjax"]) ?>
            <div id="header-act-<?=$model->id; ?>">                                   
            </div>    
        <?php //Pjax::end() ?>
    </div>
    <div id="activity-<?= $model->id; ?>" class="panel-collapse collapse <?= /* $openedActivityId == $model->id ? 'in' : */ ''; ?>">
        <div class="panel-body" style="border: 1px solid var(--background);">
            <div id="detail-act-<?= $model->id; ?>">               
            </div>               
        </div>
    </div>
</div>

<?php
$parentId = $model->id;
$script = <<<JS
    $(document).ready(function () {
        var parentId = $parentId;
        // var interval = 5000;   //number of mili seconds between each call
        
        var refreshHeaderAct = function() {
            $.ajax({
                url: "/evaluation/activities/header-activity?id=" + parentId,
                cache: false,
                success: function(html) {
                    $("#header-act-"+ parentId).html(html);
                    // setTimeout(function() {
                    //     refreshHeaderAct();
                    // }, interval);
                }
            });
        };
        refreshHeaderAct();

        var refreshDetailAct = function() {
            $.ajax({
                url: "/evaluation/activities/detail-activity?id=" + parentId,
                cache: false,
                success: function(html) {
                    $("#detail-act-"+ parentId).html(html);
                    // setTimeout(function() {
                    //     refreshDetailAct();
                    // }, interval);
                }
            });
        };
        refreshDetailAct();

        $("#btnSubmit"+parentId).click(function(){
            alert("button");
        }); 
        

        $("#close").click(function(){
                                        alert("button");
                                    }); 
    });
JS;
$this->registerJs($script, $this::POS_READY);
?>