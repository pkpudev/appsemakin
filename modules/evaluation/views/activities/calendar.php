<?php

use yii\helpers\Url;

?>
<div class="calendar">

    <div class="row">
        <div class="col-md-12">

            <div class="box box-default">
                <div class="box-body">
                    <?= edofre\fullcalendar\Fullcalendar::widget([
                            'header'        => [
                                'left'   => 'today',
                                'center' => 'title',
                                'right'  => ' prev,next',
                            ],
                            'clientOptions' => [
                                'height' => 600,
                            ],
                            'events'        => Url::to(['get-calendar']),
                        ]);
                    ?>
                </div>
            </div>

        </div>
    </div>


</div>

<?php
$script = <<<JS


$(document).ready(function(){
    $( "body" ).addClass("sidebar-collapse");

    $('.fc-today-button').html('Hari Ini');

});

JS;
$this->registerJs($script, $this::POS_READY);
?>