<?php

use app\models\ActivityEvaluationValue;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\ArrayHelper;

$this->title = 'Review Kegiatan';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="review-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                // ['content'=>
                //     Html::a('<i class="fa fa-plus"></i> Tambah Review Kegiatan', ['create'],
                //     ['role'=>'modal-remote','title'=> 'Tambah Review Kegiatan','class'=>'btn btn-success'])
                // ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Daftar Review Kegiatan',
                'before'=>'',
                'after'=>BulkButtonWidget::widget([
                    'buttons'=>Html::a(
                        '<i class="fa fa-edit"></i>&nbsp; Pilih Penilaian',
                        ["bulk-review"],
                        [
                            "class" => "btn btn-info btn-xs",
                            'role' => 'modal-remote-bulk',
                            'data-confirm' => false, 'data-method' => false, // for overide yii data api
                            'data-request-method' => 'post',
                            'data-modal-size' => 'large',
                            'data-confirm-title' => 'Pilih Penilaian',
                            'data-confirm-message' =>  Html::dropDownList(
                                                                            'evaluation', 
                                                                            null, 
                                                                            ArrayHelper::map(
                                                                                    ActivityEvaluationValue::find()
                                                                                                ->orderBy('id')
                                                                                                ->all(), 
                                                                                                'evaluation', 'evaluation'), 
                                                                            ['class' => 'form-control']),
                        ]
                    ),
                ]).
                '<div class="clearfix"></div>'
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>