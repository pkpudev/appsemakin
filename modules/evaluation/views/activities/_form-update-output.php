<?php

use app\models\ActivityCategory;
use app\models\ActivityEvaluationValue;
use app\models\Employee;
use app\models\PositionStructure;
use app\models\Status;
use app\models\UnitStructure;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Activities */
/* @var $form yii\widgets\ActiveForm */

$model->storages = 0;
?>

<div class="activities-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'output')->textarea(['maxlength' => true, 'rows' => 3]) ?>

    <?= $form->field($model, 'storages')->radioList([0 => 'Upload File', 1 => 'Link'], ['style'=>'display:block ruby;'])->label('Tipe'); ?> 

    <div id="files-place">
        <i style="color: var(--danger) !important">Pastikan nama file tidak ada karater pagar (#), petik (', "), atau karakter unik lainya.</i>

        <?= $form->field($model, 'files[]')->widget(FileInput::classname(), [
            'options' => [
                'multiple' => true,
                'accept' => $isPdfOnly ? 'application/pdf' : ''
            ],    
            'pluginOptions' => [
                'showPreview' => true,
                'showCaption' => true,
                'showRemove' => false,
                'showUpload' => false
            ]
        ])->label('File'); ?>
    </div>

    <div id="links-place" style="display: none;">
        <i style="color: var(--danger) !important">Pastikan link yang dilampirkan beserta tulisan http/https nya.</i>

        <?= $form->field($model, 'links')->textInput()->label('Link'); ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
<script>
    $('input[type="radio"]').on('click change', function(e) {
        // console.log(this.value);
        if(this.value == 0){
            $('#files-place').show();
            $('#links-place').hide();
        } else {
            $('#files-place').hide();
            $('#links-place').show();
        }
    });
</script>
