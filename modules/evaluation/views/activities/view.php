<?php

use app\components\DateHelper;
use app\models\Activities;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Activities */
// $this->params['urlParams'] = $urlParams;
$this->params['page'] = $page;
?>
<div class="activities-view">
 
    <b>Detail Kegiatan</b>
    <?= DetailView::widget([
        'model' => $model,
        'template' => "<tr><th width='250px'>{label}</th><td>{value}</td></tr>",
        'attributes' => [
            [
                'attribute' => 'activity',
                'label' => $model->parent_id ? 'Sub Kegiatan' : 'Kegiatan'
            ],
            [
                'attribute' => 'pic_id',
                'value' => function($model){
                    return $model->pic->full_name;
                },
                'visible' => !$model->parent_id
            ],
            [
                'attribute' => 'activity_category_id',
                'value' => function($model){
                    return $model->category->activity_category;
                },
                'visible' => !$model->parent_id
            ],
            [
                'attribute' => 'position_structure_id',
                'value' => function($model){
                    return $model->positionStructure->position_name;
                },
                'visible' => !$model->parent_id
            ],
            [
                'attribute' => 'okr_id',
                'value' => function($model){
                    return $model->okr->okr_code . ' - ' . $model->okr->okr;
                },
                'visible' => !$model->parent_id
            ],
            [
                'attribute' => 'start_date',
                'value' => function($model){
                    return DateHelper::getTglIndo(date('Y-m-d', strtotime($model->start_date)));
                },
            ],
            [
                'attribute' => 'due_date',
                'value' => function($model){
                    return DateHelper::getTglIndo(date('Y-m-d', strtotime($model->due_date)));
                },
            ],
            [
                'attribute' => 'duration',
                'value' => function($model){
                    return $model->duration ? $model->hour . ' Jam ' . $model->minutes . ' Menit': '-';
                }
            ],
            [
                'attribute' => 'weight',
                'value' => function($model){
                    if($model->weight == 15){
                        return 'Mudah';
                    } else if($model->weight == 35){
                        return 'Sedang';
                    } else if($model->weight == 50){
                        return 'Berat';
                    } else {
                        return '-';
                    }
                },
                'visible' => !is_null($model->parent_id)
            ],
            [
                'attribute' => 'progress',
                'value' => function($model){
                    return $model->progress ? round($model->progress, 2) . '%' : '-';
                }
            ],
            [
                'attribute' => 'output',
                'value' => function($model){
                    return $model->output ?: '-';
                },
                'visible' => !$model->parent_id
            ],
            [
                'attribute' => 'status_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->status->getStatusName($model->status_id);
                }
            ],
            [
                'attribute' => 'okr_value',
                'value' => function($model){
                    return $model->okr_value ? round($model->okr_value, 2) . '%' : '-';
                },
                'visible' => !$model->parent_id
            ],
            // 'start_date',
            // 'finish_date',
            // 'due_date',
            // 'progress',
            // 'okr_value',
            // 'evaluation',
            // 'evaluation_value',
        ],
    ]) ?>
    
    <!-- DETAIL -->
    <?php if($dataProvideChild->getCount() > 0): ?>
        <b>Sub Kegiatan</b>
        <?= GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvideChild,
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'summary' => false,
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'attribute' => 'activity'
                ],
                [
                    'label' => 'Tingkat Kesulitan',
                    'value' => function($model){
                        return Activities::WEIGHT_TEXT[$model->weight];
                    }
                ],
                [
                    'attribute' => 'progress',
                    'width' => '120px',
                    'hAlign' => 'center',
                    'value' => function($model){
                        return $model->progress ? $model->progress . '%' : '-';
                    }
                ],
                [
                    'attribute' => 'duration',
                    'width' => '120px',
                    'hAlign' => 'center',
                    'value' => function($model){
                        return $model->duration ? $model->hour . ' Jam ' . $model->minutes . ' Menit': '-';
                    }
                ],
                [
                    'attribute' => 'status_id',
                    'width' => '120px',
                    'hAlign' => 'center',
                    'format' => 'raw',
                    'value' => function($model){
                        return $model->status_id ? $model->status->getStatusname($model->status_id): '-';
                    }
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'urlCreator' => function($action, $model, $key, $index) { 
                            return Url::to([$action,'id'=>$key]);
                    },   
                    'template' => '{update-progress}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',
                    'buttons' => [
                        'update-progress' => function ($url, $model) {
                            return Html::a('<span class="fa fa-dot-circle-o"></span>',
                                ['update-status', 'id' => $model->id, 'page' => $this->params['page'] , 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], 
                                [
                                    'title' => 'Update Status',
                                    'role'=>'modal-remote',
                                    'style' => 'color: var(--default) !important',
                                ]
                            );
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<span class="fa fa-pencil"></span>',
                                ['update', 'id' => $model->id, 'page' => $this->params['page'] , 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], 
                                [
                                    'title' => 'Update Kegiatan',
                                    'role'=>'modal-remote',
                                    'style' => 'color: var(--warning) !important',
                                ]
                            );
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="fa fa-trash"></span>',
                                ['delete', 'id' => $model->id, 'page' => $this->params['page'] , 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], 
                                [
                                    'title' => 'Hapus Kegiatan',
                                    'role'=>'modal-remote',
                                    'data-confirm' => false,
                                    'data-method' => false,
                                    'data-request-method' => 'post',
                                    'data-confirm-title' => 'Konfirmasi',
                                    'data-confirm-message' => 'Yakin ingin menghapus Kegiatan ini?',
                                    'style' => 'color: var(--danger) !important',
                                ]
                            );
                        },
                    ],
                    'width' => '50px'
                ],
                
            ],
            'toolbar'=> false,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => false
        ])?>
    <?php endif; ?>

    <b>File Keluaran</b>
    <?= GridView::widget([
        'id' => 'situtation-files',
        'dataProvider' => $dataProviderFile,
        'layout' => '{items}{pager}',
        'pjax' => true,
        'emptyText' => 'Tidak ada data File.',
        'tableOptions' => [
            'class' => 'table table-responsive table-striped table-bordered'
        ],
        'columns' =>  [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '20px',
            ],
            [
                'attribute' => 'files_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->files_id) ? Html::a($model->file->name, $model->file->location, ['target' => '_blank', 'data-pjax' => 0]) : '-';
                },
                'group' => true
            ],
            /* [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'vAlign'=>'middle',
                'urlCreator' => function($action, $model, $key, $index) { 
                        return Url::to([$action,'id'=>$key]);
                },   
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="fa fa-trash"></span>',
                            ['delete-file', 'id' => $model->id], 
                            [
                                'title' => 'Hapus File ini',
                                'role'=>'modal-remote',
                                'data-confirm'=>false, 
                                'data-method'=>false,
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Konfirmasi',
                                'data-confirm-message'=>'Yakin ingin menghapus File ini?',
                                'style' => 'color: var(--danger) !important'
                            ]
                        );
                    },
                ],
                'width' => '90px'
            ], */
        ],
    ]) ?>


    <b>Histori</b>
    <?= GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvideHistory,
            'pjax'=>true,
            'summary' => false,
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                'created_stamp',
                'action',
                [
                    'attribute' => 'created_by',
                    'value' => function ($model) {
                        return ($model->created_by) ? $model->createdBy0->full_name : '-';
                    },
                ],
                [
                    'attribute' => 'comment',
                    'value' => function ($model) {
                        return ($model->comment) ? $model->comment : '-';
                    },
                ]
            ],
            'toolbar'=> false,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => false
        ])?>

</div>
