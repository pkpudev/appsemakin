<?php

use app\models\ActivityCategory;
use app\models\Employee;
use app\models\PositionStructure;
use app\models\Status;
use app\models\UnitStructure;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\range\RangeInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Activities */
/* @var $form yii\widgets\ActiveForm */

if(is_null($model->progress)) $model->progress = 0;
if(!$isManager && !Yii::$app->user->can('Admin')) $model->position_structure_id = $model->position_structure_id . '-' . $model->pic_id;
if(!$model->hour) $model->hour = 0;
if(!$model->minutes) $model->minutes = 0;
?>

<div class="activities-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php /* $form->field($model, 'status_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(
                                    Status::find()
                                                ->where(['scenario' => Status::SCENARIO_ACTIVITY_EVALUATION])
                                                ->orderBy('id')
                                                ->all(), 
                                                'id', 'status'),
        'theme' => Select2::THEME_BOOTSTRAP,
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]); */ ?>
    <label>Kegiatan:</label><br />
    <?= $model->activity; ?><br /><br />

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'hour')->input('number', ['min'=>0, 'max' => 1000])->label('Jam') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'minutes')->input('number', ['min'=>0, 'max' => 59])->label('Menit') ?>
        </div>
    </div>

    <?= $form->field($model, 'progress')->widget(RangeInput::classname(), [
        'options' => ['placeholder' => 'Persentase (0 - 100)'],
        'html5Container' => ['style' => 'width:70%'],
        'html5Options' => ['min' => 0, 'max' => 100],
        'addon' => ['append' => ['content' => '%']]
    ]); ?>

    <label>Keterangan</label><br />
    1. Jika 0%, maka status akan berubah menjadi <b>Belum Dimulai</b>.<br />
    2. Jika lebih dari 0% dan kurang dari 100%, maka status akan berubah menjadi <b>Sedang Berlangsung</b>.<br />
    3. Jika 100%, maka status akan berubah menjadi <b>Selesai</b>.<br />

    <?php ActiveForm::end(); ?>
    
</div>