<?php

use app\models\Employee;
use app\models\Status;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ListView;
use yii\widgets\Pjax;

CrudAsset::register($this);

$this->title = 'KanbanPage ' . (($searchModel->pic_id) ? '- ' . $searchModel->pic->full_name : '');
$currentPage = Yii::$app->controller->action->id;
?>

<style>
    .box-filter {
        border: 1px solid var(--background);
        margin-bottom: 10px;
        padding: 5px;
        width: 100%;
        max-width: 800px;
    }

    .table-bordered, .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid var(--background) !important;
    }

    .btn-tab {
        border-color: var(--background) !important;
        color: var(--background) !important;
    }

    .btn-tab-active, .btn-tab:hover {
        background: var(--background) !important;
        color: #fff !important;
    }

    .dropbtn {  
        padding: 1px 6px;
        border-color: var(--background);
        background: var(--background) !important;
        border-radius: 50%;
        font-size: 15px;
        color: #fff !important;
    }

    
    .dropbtn:hover {  
        background: var(--hover) !important;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        right: 0;
        background-color: var(--background);
        width: max-content;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        /* border: 1px solid var(--background); */
        border-radius: 5px;
    }

    .dropdown-content a {
        color: #fff !important;
        padding:10px;
        text-decoration: none;
        display: block;
        text-align: left;
        border-radius: 5px;
    }

    .dropdown-content a:hover {background-color: var(--hover);}

    .dropdown:hover .dropdown-content {display: block;}

    .dropdown:hover .dropbtn {background-color: var(--hover);}

    hr {
        border-color: var(--background) !important;
    }

    .callout.callout-warning {
        background-color: rgb(111, 68, 137) !important;
        border-color: #603b78 !important;
    }

    /** DARK MODE */
    .box-filter {
        border: 1px solid #fff;
        margin-bottom: 10px;
        padding: 5px;
        width: 100%;
        max-width: 800px;
    }
    .dark-mode .btn-tab {
        border-color: #fff !important;
        color: #fff !important;
    }

    .dark-mode .table-bordered, 
    .dark-mode .table-bordered > thead > tr > th, 
    .dark-mode .table-bordered > tbody > tr > th, 
    .dark-mode .table-bordered > tfoot > tr > th, 
    .dark-mode .table-bordered > thead > tr > td, 
    .dark-mode .table-bordered > tbody > tr > td, 
    .dark-mode .table-bordered > tfoot > tr > td {
        border: 1px solid #fff !important;
    }

    .dark-mode hr {
        border-color: #fff !important;
    }
</style>

<div class="kanban">

    <div class="box box-default">
        <div class="box-body">

        
                    
            <div class="btn-group" style="margin-bottom: 10px;">
                <?= Html::a('Kegiatan Hari Ini', ['index'], ['class' => 'btn btn-default btn-tab ' . ($currentPage == 'index' ? 'btn-tab-active' : '') . ' btn-sm', 'style' => 'width: 170px;']); ?>
                <?= Html::a('Semua Kegiatan', ['all'], ['class' => 'btn btn-default btn-tab ' . ($currentPage == 'all' ? 'btn-tab-active' : '') . ' btn-sm', 'style' => 'width: 170px;']); ?>
                <?= $isManager ? Html::a('Kegiatan Staf', ['lower'], ['class' => 'btn btn-default btn-tab ' . ($currentPage == 'lower' ? 'btn-tab-active' : '') . ' btn-sm', 'style' => 'width: 170px;']) : ''; ?>
                <?= Html::a('Kanban Page', ['#'], ['class' => 'btn btn-default btn-tab btn-tab-active' . ' btn-sm', 'style' => 'width: 170px;']); ?>
            </div>

            <div class="box-filter">
                <label>Filter</label><br />
                <?php
                    $form = ActiveForm::begin([
                        'id' => 'search',
                        'type' => ActiveForm::TYPE_VERTICAL,
                        'enableClientValidation' => true,
                        'errorSummaryCssClass' => 'error-summary alert alert-error'
                    ]) 
                ?>
                    <table width="100%" style="margin-top: 10px;">
                        <tr>
                            <td width="25%" style="padding-right: 2px;">
                                <?= $form->field($searchModel, 'year')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'todayHighlight' => true,
                                        'startView'=>'year',
                                        'minViewMode'=>'years',
                                        'format' => 'yyyy'
                                    ]
                                ])->label(false) ?>
                            </td>
                            <td width="<?= /* (Yii::$app->user->can('Admin'))  ?  */'50%' /* : '75%' */; ?>" style="padding-right: 2px;">
                                <?= $form->field($searchModel, 'unit_structure_id')->widget(Select2::classname(), [
                                        'data' => $listUnit,
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        'options' => ['prompt' => '-- Pilih Unit --'],
                                        'pluginOptions' => [
                                            'allowClear'=>true,
                                        ]
                                    ])->label(false) ?>
                            </td>
                            <td width="25%" style="padding-right: 2px; <?= /* (Yii::$app->user->can('Admin')) ? '' : 'display: none;'; */'' ?>">
                                <?= $form->field($searchModel, 'pic_id')->widget(Select2::classname(), [
                                        'data' => $listEmployee,
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        'options' => ['prompt' => '-- Pilih PIC --'],
                                        'pluginOptions' => [
                                            'allowClear'=>true,
                                        ]
                                    ])->label(false) ?>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" style="<?= $isAdmin ? '' : 'display: none;';?>">
                        <tr>
                            <td width="50%">
                                <?= $form->field($searchModel, 'directorate_id')->widget(Select2::classname(), [
                                    'data' => $listDirectorate,
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => ['prompt' => '-- Pilih Directorate --'],
                                    'pluginOptions' => [
                                        'allowClear'=>true,
                                    ]
                                ])->label(false) ?>
                            </td>
                            <td style="padding-left: 10px;">
                                <?= $form->field($searchModel, 'departement_id')->widget(Select2::classname(), [
                                    'data' => $listDepartment,
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => ['prompt' => '-- Pilih Department --'],
                                    'pluginOptions' => [
                                        'allowClear'=>true,
                                    ]
                                ])->label(false) ?>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td style="padding-right: 2px;">
                                <?= $form->field($searchModel, 'activity')->textInput(['placeholder' => "Masukkan kata kunci lalu tekan tombol 'Enter' pada Keyboard"])->label(false); ?>
                            </td>
                            <td width="25%" style="padding-right: 2px;">
                                <?= $form->field($searchModel, 'project_id')->widget(Select2::classname(), [
                                        'options' => [
                                            'placeholder' => '-- Pilih Project --',
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'ajax' => [
                                                'delay' => 1000,
                                                'url' => Url::to('https://project.c27g.com/api/project/list?app_key=dBqTZrAwpAII41zxdIj0BM_siWPz6kPO'),
                                                'data' => new JsExpression('function(params) { return {q:params.term} }'),
                                                'processResults' => new JsExpression('function(data) {
                                                        var results = [];
                                                        $.each(data, function (index, item) {
                                                            results.push({
                                                                id: item.id,
                                                                text: item.text
                                                            })
                                                        })
                                                        return { results: results }
                                                    }'),
                                            ],
                                            'minimumInputLength' => 3,
                                            'templateResult' => new JsExpression("function(m) { 
                                                    if (!m.id) { return m.text }
                                                    var \$ipp = m.text
                                                    return \$ipp
                                                }"),
                                            'templateSelection' => new JsExpression("function(m) {
                                                    if (!m.id) { return m.text }
                                                    var \$ipp = m.text
                                                    return \$ipp
                                                }"),
                                        ],
                                    ])->label(false);
                                ?>
                            </td>
                            <td width="25%">
                                <?= $form->field($searchModel, 'picked_date')->widget(DateRangePicker::classname(), [
                                    'options' => [
                                                    'placeholder' => 'Filter Tanggal',
                                                    'class' => 'form-control'
                                                ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'todayHighlight' => true,
                                        'locale' => [
                                            'cancelLabel' => 'Clear',
                                            'format' => 'YYYY-MM-DD',
                                        ]
                                    ]
                                ])->label(false); ?>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td>
                                <?= $form->field($searchModel, 'okr_id')->widget(Select2::classname(), [
                                    'data' => $listOkr,
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => ['prompt' => '-- Pilih OKR --'],
                                    'pluginOptions' => [
                                        'allowClear'=>true,
                                    ]
                                ])->label(false) ?>
                            </td>
                        </tr>
                    </table>
                <?php ActiveForm::end() ?>
            </div>

            <div class="table-responsive kv-grid-container">
                <table width="100%" class="table table-bordered table-condensed kv-table-wrap">
                    <thead>
                        <tr>
                            <th width="33%" style="text-align: center;"><label style="color: var(--default);">Belum Dimulai (<?= $dataProviderBelumDimulai->getTotalCount(); ?>)</label></th>
                            <th width="33%" style="text-align: center;"><label style="color: var(--warning);">Sedang Berlangsung (<?= $dataProviderSedangBerlangsung->getTotalCount(); ?>)</label></th>
                            <th width="33%" style="text-align: center;"><label style="color: var(--success);">Selesai (<?= $dataProviderSelesai->getTotalCount(); ?>)</label></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="3" align="center" style="padding: 0;">
                                <?= Html::a('<span class="fa fa-plus"></span> Buat Rencana Kegiatan Baru', ['create', 'page' => Yii::$app->controller->action->id, 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['class' => 'btn btn-success', 'role' => 'modal-remote', 'style' => 'width: 100%;border: 0;']); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= ListView::widget([
                                    'dataProvider' => $dataProviderBelumDimulai,
                                    'options' => [
                                        'tag' => 'div',
                                        'class' => 'list-wrapper',
                                        'id' => 'list-wrapper',
                                    ],
                                    'layout' => /* "<div style='width: fit-content;float: left;'>{summary}</div>"  .
                                                "<div class='clearfix'></div>
                                                <hr style='margin: 10px 0;' /> */
                                                "<div class='panel-group' id='activities-grp'>{items}</div>\n
                                                <div style='padding-top: 10px;'><center>{pager}</center></div>",
                                    'itemView' => function ($model, $key, $index, $widget) {
                                        return $this->render('kanban/_item', ['model' => $model]);
    
                                        // or just do some echo
                                        // return $model->title . ' posted by ' . $model->author;
                                    },
                                    'itemOptions' => [
                                        'tag' => false,
                                    ],
                                    'pager' => [
                                        'nextPageLabel' => 'Selanjutnya',
                                        'prevPageLabel' => 'Sebelumnya',
                                        'maxButtonCount' => 5,
                                    ],
                                ]); ?>
                            </td>
                            <td>
                                <?= ListView::widget([
                                    'dataProvider' => $dataProviderSedangBerlangsung,
                                    'options' => [
                                        'tag' => 'div',
                                        'class' => 'list-wrapper',
                                        'id' => 'list-wrapper',
                                    ],
                                    'layout' => /* "<div style='width: fit-content;float: left;'>{summary}</div>"  .
                                                "<div class='clearfix'></div>
                                                <hr style='margin: 10px 0;' /> */
                                                "<div class='panel-group' id='activities-grp'>{items}</div>\n
                                                <div style='padding-top: 10px;'><center>{pager}</center></div>",
                                    'itemView' => function ($model, $key, $index, $widget) {
                                        return $this->render('kanban/_item', ['model' => $model]);
    
                                        // or just do some echo
                                        // return $model->title . ' posted by ' . $model->author;
                                    },
                                    'itemOptions' => [
                                        'tag' => false,
                                    ],
                                    'pager' => [
                                        'nextPageLabel' => 'Selanjutnya',
                                        'prevPageLabel' => 'Sebelumnya',
                                        'maxButtonCount' => 5,
                                    ],
                                ]); ?>
                            </td>
                            <td>
                                <?= ListView::widget([
                                    'dataProvider' => $dataProviderSelesai,
                                    'options' => [
                                        'tag' => 'div',
                                        'class' => 'list-wrapper',
                                        'id' => 'list-wrapper',
                                    ],
                                    'layout' => /* "<div style='width: fit-content;float: left;'>{summary}</div>"  .
                                                "<div class='clearfix'></div>
                                                <hr style='margin: 10px 0;' /> */
                                                "<div class='panel-group' id='activities-grp'>{items}</div>\n
                                                <div style='padding-top: 10px;'><center>{pager}</center></div>",
                                    'itemView' => function ($model, $key, $index, $widget) {
                                        return $this->render('kanban/_item', ['model' => $model]);
    
                                        // or just do some echo
                                        // return $model->title . ' posted by ' . $model->author;
                                    },
                                    'itemOptions' => [
                                        'tag' => false,
                                    ],
                                    'pager' => [
                                        'nextPageLabel' => 'Selanjutnya',
                                        'prevPageLabel' => 'Sebelumnya',
                                        'maxButtonCount' => 5,
                                    ],
                                ]); ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php
$url = Yii::$app->controller->action->id;
$script = <<<JS
$('#activitiessearch-activity').keypress(function (e) {
  if (e.which == 13) {
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
  }
});

$(document.body).on("change","#activitiessearch-year",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-unit_structure_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-directorate_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-departement_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-pic_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-status_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-picked_date",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-project_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-okr_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});
    
JS;
$this->registerJs($script, $this::POS_READY);
?>