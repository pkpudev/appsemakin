<?php

use app\components\DateHelper;
use app\models\Activities;
use app\models\ActivityCategory;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\VwEmployeeManager;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

CrudAsset::register($this);

$this->title = 'Evaluasi Kegiatan Kerja Staf';

$openedActivityId = Yii::$app->getRequest()->getQueryParam('activity_id');
?>
<style>
    .panel-group {
        margin-bottom: 0 !important;
    }

    .panel-default {
        border-color: transparent !important;
    }

    .panel-heading {
        background-color: transparent !important;
    }

    .panel-body {
        border-radius: 0 0 5px 5px;
        margin-top: -8px;
        border-top-color: transparent !important;
    }

    .btn-tab {
        border-color: var(--background) !important;
        color: var(--background) !important;
    }

    .btn-tab-active, .btn-tab:hover {
        background: var(--background) !important;
        color: #fff !important;
    }
</style>

<div class="activity">

    <?= $this->render('_header', [
            'cntAllAct' => $cntAllAct,
            'cntDoneAct' => $cntDoneAct,
            'cntProgressAct' => $cntProgressAct,
            'cntNewAct' => $cntNewAct,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
    ]) ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-body">
                    
                    <div class="btn-group">
                        <?= Html::a('Kegiatan Hari Ini', ['index'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
                        <?= Html::a('Semua Kegiatan', ['all-activity'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
                        <?= Html::a('Kegiatan Staf', ['#'], ['class' => 'btn btn-default btn-tab btn-sm btn-tab-active', 'style' => 'width: 170px;']); ?>
                    </div>
                    <?= Html::a('Tambah Kegiatan', ['create'], ['class' => 'btn btn-success pull-right btn-sm', 'role' => 'modal-remote']); ?>
                    <div class="clearfix"></div>

                    <div id="ajaxCrudDatatable" style="padding-top: 10px;">
                        <?php Pjax::begin(['id' => 'activities-list-pjax', 'timeout' => 5000]) ?>
                        <?php foreach($vwEmployeeManagers as $vw): ?>
                            
                            <div style="margin-bottom: 20px;">
                                <b style="background: #fff !important; padding-right: 10px;"><?= $vw->unit->unit_name; ?></b>
                                <hr style="margin-top: -10px; margin-bottom: 10px; border: 1px solid #eee;"/>

                                <div class="panel-group" id="grp-<?= $vw->unit_id; ?>">

                                    <?php
                                        $positionIds = PositionStructure::find()->select('id')->where(['unit_structure_id' => $vw->unit_id])->column();
                                        $activities = Activities::find()->where(['position_structure_id' => $positionIds])/* ->andWhere("pic_id != " . Yii::$app->user->id) */->andWhere('parent_id is null')->all();
                                        if(!$activities) echo "<i>Tidak ada Kegiatan.</i>";
                                        foreach($activities as $activity):
                                    ?>

                                        <!-- ACTIVITY <?= $activity->id; ?> -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading" style="border: 1px solid var(--background); margin: 5px 0; padding: 5px; border-radius: 5px; box-shadow: 3px 3px 5px rgba(0, 0, 0, 0.5); border-left: 5px solid var(--background);">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <a data-toggle="collapse" data-parent="#grp-<?= $vw->unit_id; ?>" href="#activity-<?= $activity->id; ?>" style="color: var(--background) !important;">
                                                                <?= $activity->activity; ?>
                                                            </a>
                                                        </td>
                                                        <td width="200px" align="center">
                                                            <?= $activity->pic->full_name; ?>
                                                        </td>
                                                        <td width="240px" align="center">
                                                            <?= DateHelper::getTglIndo(date('Y-m-d', strtotime($activity->start_date))) . ' - ' . DateHelper::getTglIndo(date('Y-m-d', strtotime($activity->due_date))); ?>
                                                            <?php
                                                                if(strtotime(date('Y-m-d')) > strtotime($activity->due_date)){
                                                                    echo '<span class="fa fa-exclamation-triangle" style="color: var(--danger) !important;" title="Melewati tanggal target selesai!"></span>';
                                                                }
                                                            ?>
                                                        </td>
                                                        <td width="120px" align="center">
                                                            <?= $activity->status->getStatusname($activity->status_id); ?>
                                                        </td>
                                                        <td width="150px" align="right">
                                                            <?= Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $activity->id, 'page' => Yii::$app->controller->action->id], 
                                                                [
                                                                    'role' => 'modal-remote',
                                                                    'title' => 'Hapus Kegiatan ini',
                                                                    'data-confirm' => false,
                                                                    'data-method' => false,
                                                                    'data-request-method' => 'post',
                                                                    'data-confirm-title' => 'Konfirmasi',
                                                                    'data-confirm-message' => 'Yakin ingin menghapus Kegiatan ini?',
                                                                    'class' => 'btn btn-danger btn-xs'
                                                                ]); ?>
                                                            <div class="btn-group">
                                                                <?= Html::a('<span class="fa fa-bullseye"></span>', ['update-output', 'id' => $activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-info btn-xs', 'title' => 'Penilaian', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                                                                <?= Html::a('<span class="fa fa-edit"></span>', ['update-evaluation', 'id' => $activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-success btn-xs', 'title' => 'Penilaian', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                                                                <?= Html::a('<span class="fa fa-info"></span>', ['view', 'id' => $activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-info btn-xs', 'title' => 'Detail', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                                                                <?= Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-warning btn-xs', 'title' => 'Ubah', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                                                                <?= Html::a('<span class="fa fa-plus"></span>', ['create', 'parent_id' => $activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-success btn-xs', 'title' => 'Tambah', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="activity-<?= $activity->id; ?>" class="panel-collapse collapse <?= $openedActivityId == $activity->id ? 'in' : ''; ?>">
                                                <div class="panel-body" style="border: 1px solid var(--background);">
                                                    <?php Pjax::begin(['id' => "sub-act-{$activity->id}-pjax"]) ?>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <b>Kategori Kegiatan: </b><br />
                                                                <?= $activity->category->activity_category; ?><br />
                                                                <br />
                                                                <b><?= $activity->activity_category_id == ActivityCategory::REGULER ? 'Key Result' : 'Unit Yang Didudukung'; ?>: </b><br />
                                                                <?php if($activity->activity_category_id == ActivityCategory::REGULER): ?>
                                                                    <?= $activity->okr->okr_code; ?> - <?= $activity->okr->okr; ?><br />
                                                                <?php else: ?>
                                                                    <?= $activity->supportedUnit->unit_name; ?>
                                                                <?php endif; ?>
                                                                <br />
                                                                <b>Deskripsi Kegiatan: </b><br />
                                                                <?= $activity->activity; ?><br />
                                                                <br />
                                                                <b>Keluaran Kegiatan: </b><br />
                                                                <?= $activity->output ?: '-'; ?>
                                                            </td>
                                                            <td width="450px" valign="top">
                                                                <table width="100%" border="1" style="border: 1px solid var(--background);">
                                                                    <tr>
                                                                        <td align="center" style="border: 1px solid var(--background); background: var(--background); color: #fff; font-weight: bolder;" width="150px">Nilai Atasan</td>
                                                                        <td align="center" style="border: 1px solid var(--background); background: var(--background); color: #fff; font-weight: bolder;" width="150px">Nilai OKR</td>
                                                                        <td align="center" style="border: 1px solid var(--background); background: var(--background); color: #fff; font-weight: bolder;" width="150px">Kendala</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" style="border: 1px solid var(--background);"><?= $activity->evaluation ?: '-'; ?></td>
                                                                        <td align="center" style="border: 1px solid var(--background);"><?= $activity->okr_value ?: '-'; ?></td>
                                                                        <td align="center" style="border: 1px solid var(--background);"><?= Html::a('0 Kendala', ['issue', 'activity_id' => $activity->id], ['role' => 'modal-remote', 'style' => 'font-weight: bolder;']) ?></td>
                                                                    </tr>
                                                                </table>
                                                                <table width="100%" border="1" style="border: 1px solid var(--background);">
                                                                    <tr>
                                                                        <td align="center" style="border: 1px solid var(--background); background: var(--background); color: #fff; font-weight: bolder;" width="50%">Jumlah Durasi</td>
                                                                        <td align="center" style="border: 1px solid var(--background); background: var(--background); color: #fff; font-weight: bolder;" width="50%">Progress</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" style="border: 1px solid var(--background);"><?= $activity->duration ?: 0; ?> Jam</td>
                                                                        <td align="center" style="border: 1px solid var(--background);"><?= round($activity->progress, 2) ?: 0; ?>%</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <!-- <div style="background: #aaa; padding: 5px;"> -->
                                                        <b>Sub Kegiatan: </b><br />
                                                        <?php
                                                            $sub1Activities = Activities::find()->where(['parent_id' => $activity->id])->orderBy('id')->all();

                                                            if(!$sub1Activities) echo '<i>Tidak mempunyai sub kegiatan.</i>';
                                                            foreach($sub1Activities as $sub1Activity):
                                                        ?>
                                                        <!-- SUB1 -->
                                                        <div class="panel-group" id="sub-activity-<?= $sub1Activity->parent_id; ?>">
                                                            <!-- SUB1.<?= $sub1Activity->id; ?> -->
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading" style="border: 1px solid var(--background); margin: 5px 0; padding: 5px; border-radius: 5px; box-shadow: 3px 3px 5px rgba(0, 0, 0, 0.5); border-left: 5px solid var(--background);">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <a data-toggle="collapse" data-parent="#sub-activity-<?= $sub1Activity->parent_id; ?>" href="#activity-<?= $sub1Activity->id; ?>" style="color: var(--background) !important; display: none;">
                                                                                    <?= $sub1Activity->activity; ?>
                                                                                </a>
                                                                                <?= Html::a(
                                                                                                $sub1Activity->activity, 
                                                                                                ['view', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id],
                                                                                                [
                                                                                                    'role' => 'modal-remote',
                                                                                                    'style' => 'color: var(--background) !important;'
                                                                                                ]
                                                                                                ); ?>
                                                                            </td>
                                                                            <td width="200px" align="center">
                                                                                <?= $sub1Activity->duration; ?> Jam
                                                                            </td>
                                                                            <td width="120px" align="center">
                                                                                <?= $sub1Activity->status->getStatusname($sub1Activity->status_id); ?>
                                                                                <!-- <span class="label label-success" style="width: 120px !important; display: block;">Selesai</span> -->
                                                                            </td>
                                                                            <td width="130px" align="right">
                                                                                <?= Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], 
                                                                                    [
                                                                                        'role' => 'modal-remote',
                                                                                        'title' => 'Hapus Kegiatan ini',
                                                                                        'data-confirm' => false,
                                                                                        'data-method' => false,
                                                                                        'data-request-method' => 'post',
                                                                                        'data-confirm-title' => 'Konfirmasi',
                                                                                        'data-confirm-message' => 'Yakin ingin menghapus Kegiatan ini?',
                                                                                        'class' => 'btn btn-danger btn-xs'
                                                                                    ]); ?>
                                                                                <div class="btn-group">
                                                                                    <?= Html::a('<span class="fa fa-dot-circle-o"></span>', ['update-status', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-default btn-xs', 'title' => 'Ubah Status', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                                                                                    <?= Html::a('<span class="fa fa-info"></span>', ['view', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-info btn-xs', 'title' => 'Detail', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                                                                                    <?= Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-warning btn-xs', 'title' => 'Ubah', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                                                                                    <?php // Html::a('<span class="fa fa-plus"></span>', ['create', 'parent_id' => $sub1Activity->id], ['class' => 'btn btn-success btn-xs', 'title' => 'Tambah', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="activity-<?= $sub1Activity->id; ?>" class="panel-collapse collapse">
                                                                    <div class="panel-body" style="border: 1px solid var(--background);">
                                                                        <b>Deskripsi Kegiatan: </b><br />
                                                                        <?= $sub1Activity->description; ?>
                                                                        <br /><br />
                                                                        <b>Sub Kegiatan: </b><br />
                                                                        <i>Tidak mempunyai sub kegiatan.</i>
                
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php endforeach; ?>  
                                                    <!-- </div> -->                                                  
                                                    <?php Pjax::end() ?>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>

                                </div>
                            </div>

                        <?php endforeach; ?>
                        <?php Pjax::end() ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>