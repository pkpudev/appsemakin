<?php

use app\models\Activities;
use app\models\ActivityCategory;
use app\models\Employee;
use app\models\Okr;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Activities */
/* @var $form yii\widgets\ActiveForm */

if(!$model->isNewRecord){
    if(!$isManager && !Yii::$app->user->can('Admin')) $model->position_structure_id = $model->position_structure_id . '-' . $model->pic_id;
    if(!$model->hour) $model->hour = 0;
    if(!$model->minutes) $model->minutes = 0;
} else {
    $model->hour = 0;
    $model->minutes = 0;
}

if(!$model->year) $model->year = date('Y');
?>

<div class="activities-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php if($model->isNewRecord): ?>
    <div class="row" style="display: none;">
        <div class="col-md-4">
            <?= $form->field($model, 'year')->widget(DatePicker::classname(), [
                                            'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'todayHighlight' => true,
                                                'startView'=>'year',
                                                'minViewMode'=>'years',
                                                'format' => 'yyyy'
                                            ]
                                        ])->label('Tahun') ?>
        </div>
    </div>
    <?php endif; ?>

    <?= $form->field($model, 'activity')->textarea(['rows' => 3])->label($modelParent ? 'Sub Kegiatan' : 'Kegiatan Utama') ?>

    <?php if(!$modelParent): ?>

        <?php if($isManager || $isGeneralManager || Yii::$app->user->can('Admin')): ?>
            <?= $form->field($model, 'pic_id')->widget(Select2::classname(), [
                'initValueText' => $model->pic_id ? $model->pic->full_name : '',
                'data' => ArrayHelper::merge([Yii::$app->user->id => Yii::$app->user->identity->full_name], ArrayHelper::map(
                                            VwEmployeeManager::find()
                                                                ->alias('vw')
                                                                ->joinWith('employee e')
                                                                ->where(['upper_id' => Yii::$app->user->id])
                                                                ->andWhere(['year' => [date('Y') - 1, date('Y')]])
                                                                ->orderBy('e.full_name')
                                                                ->all(), 
                                                                'employee_id', function($model){
                                                                    return $model->employee->full_name;
                                                                })),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => [
                                'prompt' => '-- Pilih Karyawan --',
                            ],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        <?php endif; ?>

        <?= $form->field($model, 'activity_category_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(ActivityCategory::find()->where(['is_active' => 1])->all(), 'id', 'activity_category'),
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                            'prompt' => '-- Pilih Kategori --',
                        ],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                ]
        ]); ?>

        <?php if($isManager || Yii::$app->user->can('Admin')): ?>
            <?php $model->position_structure_id = $model->position_structure_id . '-' . $model->pic_id; ?>
            <div id="reguler-place" style="<?= $model->activity_category_id == ActivityCategory::REGULER ? '' : 'display: none;'; ?>">
                <?= Html::hiddenInput('selected_id', $model->isNewRecord ? '' : $model->pic_id . '-' . $model->position_structure_id, ['id'=>'selected_id']); ?>
                <?= $form->field($model, 'position_structure_id')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => [
                        'placeholder' => '-- Pilih Jabatan --'
                    ],
                    'select2Options' => [
                        // 'initValueText' => $model->position_structure_id ? $model->positionStructure->position_name : '',
                        'options' => [
                                        'value' => $model->position_structure_id
                                    ],
                        'pluginOptions' => [
                                                'allowClear' => true,
                                                'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                            ]
                    ],
                    'pluginOptions' => [
                        'depends' => ['activities-pic_id'],
                        'initialize' => ( $model->isNewRecord) ? false : true,
                        'url' => Url::to(['/evaluation/activities/get-positions']),
                        'params' => ['selected_id'],
                    ]
                ]); ?>

                <?= $form->field($model, 'okr_id')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => [
                                    'placeholder' => '-- Pilih KR --'
                                ],
                    'select2Options' => [
                        'initValueText' => $model->okr_id ? $model->okr->okr : '',
                        'options' => [
                                        'value' => $model->okr_id
                                    ],
                        'pluginOptions' => [
                                                'allowClear' => true,
                                                'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                            ]
                    ],
                    'pluginOptions' => [
                        'depends' => ['activities-position_structure_id'],
                        'url' => Url::to(['/evaluation/activities/get-okrs']),
                    ]
                ]); ?>
            </div>
        <?php else: ?>
            <div id="reguler-place" style="<?= $model->activity_category_id == ActivityCategory::REGULER ? '' : 'display: none;'; ?>">
                <?= $form->field($model, 'position_structure_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(PositionStructureEmployee::find()
                                                            ->alias('pse')
                                                            ->joinWith('position p')
                                                            ->where(['pse.employee_id' => Yii::$app->user->id])
                                                            ->andWhere(['pse.is_active' => true])
                                                            ->andWhere(['p.year' => [date('Y')-1, date('Y')]])
                                                            ->all(), 
                                                            function($model){
                                                                return $model->position_structure_id . '-' . $model->employee_id;
                                                            }, function($model){
                                                                return $model->position->year . ' - ' . $model->position->position_name . ' (' . $model->position->unit->unit_name . ')';
                                                            }),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    // 'initValueText' => $model->position_structure_id ? $model->positionStructure->position_name : '',
                    'options' => [
                                    'value' => $model->position_structure_id,
                                    'prompt' => '-- Pilih Jabatan --',
                                ],
                    'pluginOptions' => [
                        'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                        ]
                ]); ?>

                <?php //if($model->isNewRecord): ?>
                <?= $form->field($model, 'okr_id')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => [
                                    'placeholder' => '-- Pilih KR --'
                                ],
                    'select2Options' => [
                        'pluginOptions' => [
                                                'allowClear' => true,
                                                'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                        ],
                        'initValueText' => $model->okr_id ? $model->okr->okr_code . ' - ' . $model->okr->okr : '',
                    ],
                    'pluginOptions' => [
                        'depends' => ['activities-position_structure_id'],
                        'url' => Url::to(['/evaluation/activities/get-okrs']),
                    ]
                ]); ?>
                
                <?php //else: ?>
                    <?php if($model->activity_category_id == ActivityCategory::REGULER): ?>
                    <?= /* $form->field($model, 'okr_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Okr::find()->where(['type' => 'KR'])->andWhere(['pic_id' => explode('-', $model->position_structure_id)[1]])->andWhere(['position_structure_id' => explode('-', $model->position_structure_id)[0]])->all(), 'id', 'okr'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => [
                                        'prompt' => '-- Pilih Key Result --',
                                    ],
                        'pluginOptions' => [
                            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                            ]
                    ]);*/'' ?>
                    <?php endif; ?>
                <?php //endif; ?>
            </div>
        <?php endif; ?>

        <div id="project-place"  style="<?= in_array($model->activity_category_id, [2,3,4]) || !$model->activity_category_id ? 'display: none;' : ''; ?>">
            <?= $form->field($model, 'project_id')->widget(Select2::classname(), [
                    'options' => [
                        'id' => 'ipp-value',
                        'placeholder' => '-- Pilih Project --',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                        'ajax' => [
                            'delay' => 1000,
                            'url' => Url::to('https://project.c27g.com/api/project/list?app_key=dBqTZrAwpAII41zxdIj0BM_siWPz6kPO'),
                            'data' => new JsExpression('function(params) { return {q:params.term} }'),
                            'processResults' => new JsExpression('function(data) {
                                    var results = [];
                                    $.each(data, function (index, item) {
                                        results.push({
                                            id: item.id,
                                            text: item.text
                                        })
                                    })
                                    return { results: results }
                                }'),
                        ],
                        'minimumInputLength' => 3,
                        'templateResult' => new JsExpression("function(m) { 
                                if (!m.id) { return m.text }
                                var \$ipp = m.text
                                return \$ipp
                            }"),
                        'templateSelection' => new JsExpression("function(m) {
                                if (!m.id) { return m.text }
                                var \$ipp = m.text
                                return \$ipp
                            }"),
                    ],
                ])->label('No. Project');
            ?>
        </div>
            
        <div id="other-place" style="<?= in_array($model->activity_category_id, [2,3,4]) ? '' : 'display: none;'; ?>">
            <?= $form->field($model, 'supported_unit_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                                            UnitStructure::find()
                                                        ->where(['year' => date('Y')])
                                                        ->orderBy('unit_name')
                                                        ->all(), 
                                                        'id', 'unit_name'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => [
                                'prompt' => '-- Pilih Unit --',
                            ],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Silahkan Pilih Tanggal Mulai'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'due_date')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Silahkan Pilih Tanggal Target Selesai'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]) ?>
            </div>
        </div>
    <?php endif; ?>

    <?php
        $modelChild = Activities::find()->where(['parent_id' => $model->id])->one();

        if($modelParent || (!$modelChild || $model->isNewRecord)):
    ?>
        <?php // $form->field($model, 'duration')->textInput(['maxlength' => true]) ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'hour')->input('number', ['min'=>0, 'max' => 1000])->label('Jam') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'minutes')->input('number', ['min'=>0, 'max' => 59])->label('Menit') ?>
            </div>
        </div>
        <?php // $form->field($model, 'progress')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>

    <?php if($modelParent): ?>

        <?= $form->field($model, 'weight')->widget(Select2::classname(), [
            'data' => [15 => 'Mudah', 35 => 'Sedang', 50 => 'Berat'],
            'theme' => Select2::THEME_BOOTSTRAP,
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                ]
        ]); ?>
    <?php endif; ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<script>
    $('#activities-activity_category_id').on('click change', function(e) {
        // console.log(this.value);
        if(this.value == 1){
            $('#reguler-place').show();
            $('#project-place').show();
            $('#other-place').hide();
        } else if(this.value == 2){
            $('#reguler-place').hide();
            $('#other-place').show();
            $('#project-place').hide();
        }  else if(this.value == 5){
            $('#project-place').show();
            $('#reguler-place').hide();
            $('#other-place').hide();
        } else {
            $('#reguler-place').hide();
            $('#other-place').hide();
            $('#project-place').hide();
        }
    });
</script>