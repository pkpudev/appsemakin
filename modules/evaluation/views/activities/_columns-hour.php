<?php

use app\components\DateHelper;
use app\models\ActivityCategory;
use app\models\Employee;
use app\models\PositionStructure;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'unit_code',
        'label'=>'Kode Unit',
        'value' => function($model){
            return $model->unit->unit_code;
        },
        'group' => true,
        'width' => '80px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'unit_name',
        'label'=>'Nama Unit',
        'value' => function($model){
            return $model->unit->unit_name;
        },
        'group' => true,
        'width' => '350px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'position_name',
        'label'=>'Jabatan',
        'value' => function($model){
            return $model->position->position_name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'employee_name',
        'label'=>'Nama Karyawan',
        'value' => function($model){
            return $model->employee->full_name;
        },
        'group' => true,
    ],
    [
        'label' => 'Jumlah Jam',
        'value' => function($model){
            
            $year = Yii::$app->getRequest()->getQueryParam('VwEmployeeManagerSearch')['year'] ?: date('Y');
            $period = Yii::$app->getRequest()->getQueryParam('VwEmployeeManagerSearch')['period'];
            
            if($period == 'Kuartal 1'){
                $dateStart = $year . '-' . '01-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '03-01'));
            } else if($period == 'Kuartal 2'){
                $dateStart = $year . '-' . '04-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '06-01'));
            } else if($period == 'Kuartal 3'){
                $dateStart = $year . '-' . '07-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '09-01'));
            } else if($period == 'Kuartal 4'){
                $dateStart = $year . '-' . '10-01';
                $dateEnd = date("Y-m-t", strtotime( $year . '-' . '12-01'));
            } else {
                $dateStart = $year . '-' . '01-01';
                $dateEnd = $year . '-' . '12-31';
            }

            $sum = Yii::$app->db->createCommand("
                                                SELECT 
                                                    SUM(duration) 
                                                FROM 
                                                    management.activities
                                                WHERE
                                                    parent_id IS NULL AND
                                                    pic_id = {$model->employee_id} AND
                                                    -- position_structure_id = {$model->position_id} AND
                                                    ((cast('{$dateStart}' as timestamp) >= start_date AND cast('{$dateStart}' as timestamp) <= due_date) OR 
                                                    (cast('{$dateEnd}' as timestamp) >= start_date AND cast('{$dateEnd}' as timestamp) <= due_date) OR
                                                    (start_date >= '{$dateStart}' AND start_date <= '{$dateEnd}') OR 
                                                    (due_date >= '{$dateStart}' AND due_date <= '{$dateEnd}') OR 
                                                    (cast('{$dateStart}' as timestamp) <= start_date AND cast('{$dateEnd}' as timestamp) >= due_date))
                                            ")->queryScalar();

            $model->duration = $sum;

            return DateHelper::getHourMinutes($sum);
        },
        'width' => '120px',
        'hAlign' => 'right',
        'group' => true,
        'subGroupOf' => 4
    ],
    [
        'label' => 'FTE',
        'value' => function($model){
            $period = Yii::$app->getRequest()->getQueryParam('VwEmployeeManagerSearch')['period'];
            
            if($period){
                $fte = 519;
            } else {
                $fte = 2076;
            }

            $model->fte = $model->duration / $fte; 
            return round($model->fte, 2);
        },
        'pageSummary' => true,
        'width' => '120px',
        'hAlign' => 'right',
        'group' => true,
        'subGroupOf' => 4,
        'visible' => Yii::$app->user->can("HR")
    ],
    [
        'label' => 'Kategori',
        'format' => 'raw',
        'value' => function($model){

            $val = round($model->fte, 2);

            if($val < 0.5){
                $cat = '<span class="label label-default">Abnormal Underload</span>';
            } else if($val >= 0.5 && $val <= 0.8){
                $cat = '<span class="label label-info">Very Underload</span>';
            } else if($val >= 0.81 && $val <= 0.95){
                $cat = '<span class="label label-primary">Underload</span>';
            } else if($val >= 0.96 && $val <= 1.05){
                $cat = '<span class="label label-success">Normal</span>';
            } else if($val >= 1.06 && $val <= 1.2){
                $cat = '<span class="label label-warning">Overload</span>';
            } else if($val >= 1.21 && $val <= 1.5){
                $cat = '<span class="label label-danger" style="background: #de643b !important;">Very Overload</span>';
            } else if($val >= 1.51){
                $cat = '<span class="blink label label-danger">Ubnormal Overload</span>';
            }

            return $cat;
        },
        'width' => '120px',
        'hAlign' => 'right',
        'group' => true,
        'subGroupOf' => 4,
        'visible' => Yii::$app->user->can("HR")
    ]

];
