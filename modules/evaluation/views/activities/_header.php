<?php

use app\models\Employee;
use app\models\Status;
use app\models\UnitStructure;
use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$currentPage = Yii::$app->controller->action->id;

$year = Yii::$app->getRequest()->getQueryParam('year') ?: date('Y');

?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                    
                        <div class="btn-group" style="margin-bottom: 10px;">
                            <?= Html::a('Kegiatan Hari Ini', ['index'], ['class' => 'btn btn-default btn-tab ' . ($currentPage == 'index' ? 'btn-tab-active' : '') . ' btn-sm', 'style' => 'width: 170px;']); ?>
                            <?= Html::a('Semua Kegiatan', ['all'], ['class' => 'btn btn-default btn-tab ' . ($currentPage == 'all' ? 'btn-tab-active' : '') . ' btn-sm', 'style' => 'width: 170px;']); ?>
                            <?= $isManager ? Html::a('Kegiatan Staf', ['lower'], ['class' => 'btn btn-default btn-tab ' . ($currentPage == 'lower' ? 'btn-tab-active' : '') . ' btn-sm', 'style' => 'width: 170px;']) : ''; ?>
                            <?= Html::a('Kanban Page', ['kanban'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
                        </div>

                        <div class="box-filter">
                        <?php
                            $form = ActiveForm::begin([
                                'id' => 'search',
                                'type' => ActiveForm::TYPE_VERTICAL,
                                'enableClientValidation' => true,
                                'errorSummaryCssClass' => 'error-summary alert alert-error'
                            ]) 
                        ?>
                            <label>Filter</label>
                            <table width="100%" style="margin-top: 10px;">
                                <tr>
                                    <td width="25%" style="padding-right: 2px;">
                                        <?= $form->field($searchModel, 'year')->widget(DatePicker::classname(), [
                                            'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'todayHighlight' => true,
                                                'startView'=>'year',
                                                'minViewMode'=>'years',
                                                'format' => 'yyyy'
                                            ]
                                        ])->label(false) ?>
                                    </td>
                                    <td width="<?= (Yii::$app->user->can('Admin') && in_array($currentPage, ['all', 'lower']))  ? '25%' : '50%'; ?>" style="padding-right: 2px;">
                                        <?= $form->field($searchModel, 'unit_structure_id')->widget(Select2::classname(), [
                                                'data' => $listUnit,
                                                'theme' => Select2::THEME_BOOTSTRAP,
                                                'options' => ['prompt' => '-- Pilih Unit --'],
                                                'pluginOptions' => [
                                                    'allowClear'=>true,
                                                ]
                                            ])->label(false) ?>
                                    </td>
                                    <td width="25%" style="padding-right: 2px; <?= (Yii::$app->user->can('Admin') && in_array($currentPage, ['all', 'lower'])) ? '' : 'display: none;'; ?>">
                                        <?= $form->field($searchModel, 'pic_id')->widget(Select2::classname(), [
                                                'data' => $listEmployee,
                                                'theme' => Select2::THEME_BOOTSTRAP,
                                                'options' => ['prompt' => '-- Pilih PIC --'],
                                                'pluginOptions' => [
                                                    'allowClear'=>true,
                                                ]
                                            ])->label(false) ?>
                                    </td>
                                    <td width="25%" style="padding-right: 2px;">
                                        <?= $form->field($searchModel, 'status_id')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Status::find()->andWhere(['scenario' => Status::SCENARIO_ACTIVITY_EVALUATION])->all(), 'id', 'status'),
                                                'theme' => Select2::THEME_BOOTSTRAP,
                                                'options' => ['prompt' => '-- Pilih Status --'],
                                                'pluginOptions' => [
                                                    'allowClear'=>true,
                                                ]
                                            ])->label(false) ?>
                                    </td>
                                </tr>
                            </table>
                            <?php if($isAdmin): ?>
                            <table width="100%">
                                <tr>
                                    <td width="50%">
                                        <?= $form->field($searchModel, 'directorate_id')->widget(Select2::classname(), [
                                            'data' => $listDirectorate,
                                            'theme' => Select2::THEME_BOOTSTRAP,
                                            'options' => ['prompt' => '-- Pilih Directorate --'],
                                            'pluginOptions' => [
                                                'allowClear'=>true,
                                            ]
                                        ])->label(false) ?>
                                    </td>
                                    <td style="padding-left: 2px;">
                                        <?= $form->field($searchModel, 'departement_id')->widget(Select2::classname(), [
                                            'data' => $listDepartment,
                                            'theme' => Select2::THEME_BOOTSTRAP,
                                            'options' => ['prompt' => '-- Pilih Department --'],
                                            'pluginOptions' => [
                                                'allowClear'=>true,
                                            ]
                                        ])->label(false) ?>
                                    </td>
                                </tr>
                            </table>
                            <?php endif; ?>
                            <table width="100%">
                                <tr>
                                    <td style="padding-right: 2px;">
                                        <?= $form->field($searchModel, 'activity')->textInput(['placeholder' => "Masukkan kata kunci lalu tekan tombol 'Enter' pada Keyboard"])->label(false); ?>
                                    </td>
                                    <td width="25%" style="padding-right: 2px;">
                                        <?= $form->field($searchModel, 'project_id')->widget(Select2::classname(), [
                                                'options' => [
                                                    'placeholder' => '-- Pilih Project --',
                                                ],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'ajax' => [
                                                        'delay' => 1000,
                                                        'url' => Url::to('https://project.c27g.com/api/project/list?app_key=dBqTZrAwpAII41zxdIj0BM_siWPz6kPO'),
                                                        'data' => new JsExpression('function(params) { return {q:params.term} }'),
                                                        'processResults' => new JsExpression('function(data) {
                                                                var results = [];
                                                                $.each(data, function (index, item) {
                                                                    results.push({
                                                                        id: item.id,
                                                                        text: item.text
                                                                    })
                                                                })
                                                                return { results: results }
                                                            }'),
                                                    ],
                                                    'minimumInputLength' => 3,
                                                    'templateResult' => new JsExpression("function(m) { 
                                                            if (!m.id) { return m.text }
                                                            var \$ipp = m.text
                                                            return \$ipp
                                                        }"),
                                                    'templateSelection' => new JsExpression("function(m) {
                                                            if (!m.id) { return m.text }
                                                            var \$ipp = m.text
                                                            return \$ipp
                                                        }"),
                                                ],
                                            ])->label(false);
                                        ?>
                                    </td>
                                    <td width="25%" style="display: <?= in_array($currentPage, ['all', 'lower']) ? '' : 'none'; ?>;">
                                        <?= $form->field($searchModel, 'picked_date')->widget(DateRangePicker::classname(), [
                                            'options' => [
                                                            'placeholder' => 'Filter Tanggal',
                                                            'class' => 'form-control'
                                                        ],
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'todayHighlight' => true,
                                                'locale' => [
                                                    'cancelLabel' => 'Clear',
                                                    'format' => 'YYYY-MM-DD',
                                                ]
                                            ]
                                        ])->label(false); ?>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <?= $form->field($searchModel, 'okr_id')->widget(Select2::classname(), [
                                            'data' => $listOkr,
                                            'theme' => Select2::THEME_BOOTSTRAP,
                                            'options' => ['prompt' => '-- Pilih OKR --'],
                                            'pluginOptions' => [
                                                'allowClear'=>true,
                                            ]
                                        ])->label(false) ?>
                                    </td>
                                </tr>
                            </table>
                            <?php ActiveForm::end() ?>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <table width="100%" style="margin-top: 45px;">
                            <tr>
                                <td width="20%">
                                    <div style="height: 100px; background: var(--info); margin: 5px; border-radius: 5px; border: 1px solid #eee; text-align: center; color: #fff;">
                                        <b style="font-size: 45px;"><?= $cntAllAct; ?></b>
                                        <br />
                                        <b>Kegiatan</b>
                                    </div>
                                </td>
                                <td width="20%">
                                    <div style="height: 100px; background: var(--success); margin: 5px; border-radius: 5px; border: 1px solid #eee; text-align: center; color: #fff;">
                                        <b style="font-size: 45px;"><?= $cntDoneAct; ?></b>
                                        <br />
                                        <b>Selesai</b>
                                    </div>
                                </td>
                                <td width="20%">
                                    <div style="height: 100px; background: var(--warning); margin: 5px; border-radius: 5px; border: 1px solid #eee; text-align: center; color: #fff;">
                                        <b style="font-size: 45px;"><?= $cntProgressAct; ?></b>
                                        <br />
                                        <b>Berlangsung</b>
                                    </div>
                                </td>
                                <td width="20%">
                                    <div style="height: 100px; background: var(--default); margin: 5px; border-radius: 5px; border: 1px solid #eee; text-align: center; color: #fff;">
                                        <b style="font-size: 45px;"><?= $cntNewAct; ?></b>
                                        <br />
                                        <b>Belum Dimulai</b>
                                    </div>
                                </td>
                                <td width="20%">
                                    <div style="height: 100px; background: var(--danger); margin: 5px; border-radius: 5px; border: 1px solid #eee; text-align: center; color: #fff;">
                                        <b style="font-size: 45px;"><?= $cntCancelAct; ?></b>
                                        <br />
                                        <b>Dibatalkan</b>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$url = $currentPage;
$script = <<<JS
$('#activitiessearch-activity').keypress(function (e) {
  if (e.which == 13) {
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
  }
});

$(document.body).on("change","#activitiessearch-year",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-unit_structure_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-pic_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-status_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-picked_date",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-project_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-directorate_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-departement_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});

$(document.body).on("change","#activitiessearch-okr_id",function(){
    var year = jQuery('#activitiessearch-year').val() || '',
        unit_structure_id = jQuery('#activitiessearch-unit_structure_id').val() || '',
        status_id = jQuery('#activitiessearch-status_id').val() || '',
        activity = jQuery('#activitiessearch-activity').val() || '',
        pic_id = jQuery('#activitiessearch-pic_id').val() || '',
        picked_date = jQuery('#activitiessearch-picked_date').val() || '',
        project_id = jQuery('#activitiessearch-project_id').val() || '',
        directorate_id = jQuery('#activitiessearch-directorate_id').val() || '',
        departement_id = jQuery('#activitiessearch-departement_id').val() || '',
        okr_id = jQuery('#activitiessearch-okr_id').val() || '',
        url = '$url?ActivitiesSearch[year]=' + year 
                + '&ActivitiesSearch[unit_structure_id]=' + unit_structure_id
                + '&ActivitiesSearch[status_id]=' + status_id
                + '&ActivitiesSearch[activity]=' + activity
                + '&ActivitiesSearch[pic_id]=' + pic_id
                + '&ActivitiesSearch[picked_date]=' + picked_date
                + '&ActivitiesSearch[project_id]=' + project_id
                + '&ActivitiesSearch[directorate_id]=' + directorate_id
                + '&ActivitiesSearch[departement_id]=' + departement_id
                + '&ActivitiesSearch[okr_id]=' + okr_id
    window.location.href = url;
});
    
JS;
$this->registerJs($script, $this::POS_READY);
?>