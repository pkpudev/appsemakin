<?php

use app\models\ActivityCategory;
use app\models\Employee;
use app\models\PositionStructure;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
    	'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'pic_id',
        'label' => 'Penanggung Jawab',
        'value' => function($model){
            return $model->pic->full_name;
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(Employee::find()->where(['is_employee' => 1])->andWhere(['company_id' => 1])->andWhere(['user_status' => 1])->andWhere(['employee_type_id' => [1,2,5]])->orderBy('full_name')->asArray()->all(), 'id', 'full_name'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
        
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'position_structure_id',
        'value' => function($model){
            return $model->positionStructure->position_name;
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(PositionStructure::find()->where(['year' => date('Y')])->orderBy('position_name')->all(), 'id', 'position_name'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'okr_text',
        'label' => 'Key Result',
        'value' => function($model){
            return $model->okr->okr_code . ' - ' . $model->okr->okr;
        },
        // 'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'activity',
        'label' => 'Kegiatan Utama'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'output',
        'value' => function($model){
            return $model->output ?: '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'activity_category_id',
        'value' => function($model){
            return $model->category->activity_category;
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(ActivityCategory::find()->orderBy('activity_category')->all(), 'id', 'activity_category'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
        'width' => '120px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'duration',
        'value' => function($model){
            return $model->duration ? "{$model->hour} Jam {$model->minutes} menit": '-';
        }, 
        'width' => '120px',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Status',
        'format' => 'raw',
        'value' => function($model){
            $target = strtotime(date('Y-m-d', strtotime($model->due_date)));
            $done = strtotime(date('Y-m-d', strtotime($model->finish_date)));
            if($done > $target){
                return '<span class="label label-danger">Terlambat</span>';
            } else {
                return '<span class="label label-success">Tepat Waktu</span>';
            }
        },
        'width' => '120px',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },   
        'template' => '{view}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<span class="fa fa-eye"></span>',
                    ['view', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], 
                    [
                        'title' => 'Lihat Kendala ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--primary) !important'
                    ]
                );
            },
        ],
    ],

];
