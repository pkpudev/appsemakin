<?php

use app\models\ActivityCategory;
use app\components\DateHelper;
use app\models\Activities;
use app\models\Status;
use app\models\VwEmployeeManager;
use yii\helpers\Html;

$hasChild = Activities::find()->where(['parent_id' => $model->id])->one();
$isManager = VwEmployeeManager::find()
                                    ->where(['position_id' => $model->position_structure_id])
                                    ->andWhere(['employee_id' => Yii::$app->user->id])
                                    ->andWhere(['level' => 4])
                                    ->one();
?>


<table width="100%">
    <tr>
        <td rowspan="2">
            <a data-toggle="collapse" data-parent="#activities-grp" href="#activity-<?= $model->id; ?>">
                <?= $model->activity; ?>
            </a>
        </td>
        <td width="200px" align="left">
            <?= $model->pic->full_name; ?>
        </td>
        <td width="240px" align="center">
            <?= DateHelper::getTglIndo(date('Y-m-d', strtotime($model->start_date))) . ' - ' . DateHelper::getTglIndo(date('Y-m-d', strtotime($model->due_date))); ?>
            <?php
                if(strtotime(date('Y-m-d')) > strtotime($model->due_date) && $model->status_id != Status::ACT_DONE){
                    echo '<span class="fa fa-exclamation-triangle" style="color: var(--danger) !important;" title="Melewati tanggal target selesai!"></span>';
                }
            ?>
        </td>
        <td width="120px" align="center">
            <?= $model->status->getStatusname($model->status_id); ?>
        </td>
    </tr>
    <tr>
        <td align="left"><i title="<?= $model->positionStructure->position_name; ?>"><?= strlen($model->positionStructure->position_name) > 25 ? substr($model->positionStructure->position_name, 0, 25) . '...' : $model->positionStructure->position_name; ?></i></td>
        <td align="center" style="padding-top: 5px;" >
            <?= $model->status_id == Status::ACT_DONE ? '<div class="alert alert-success" style="margin: 0;padding: 2px;width: fit-content;font-size: 11px;"><span class="fa fa-calendar"></span> Diselesaikan Pada ' . DateHelper::getTglIndo(date('Y-m-d', strtotime($model->finish_date))) . '</div>' : ''; ?>
        </td>
        <td align="center" style="padding-top: 5px;" width="175px">
            <div class="btn-group">
                <?php 
                $upperId = VwEmployeeManager::findOne(['employee_id' => $model->pic_id, 'position_id' => $model->position_structure_id])->upper_id;
                    if(in_array(Yii::$app->user->id, [$model->pic_id, $upperId])): 
                ?>
                    <?= $hasChild ? '' : Html::a('<span class="fa fa-dot-circle-o"></span>', ['update-status', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-default btn-xs', 'title' => 'Ubah Progress', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                    <?= Html::a('<span class="fa fa-plus"></span>', ['create', 'parent_id' => $model->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-success btn-xs', 'title' => 'Tambah Sub Kegiatan', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                    <?= Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-warning btn-xs', 'title' => 'Ubah', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                    <?= Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-info btn-xs', 'title' => 'Detail', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                    <?= $model->activity_category_id == ActivityCategory::REGULER/*  && !$isManager */ ? Html::a('<span class="fa fa-edit"></span>', ['update-evaluation', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-success btn-xs', 'title' => 'Kontribusi Kegiatan', 'style' => 'width: 25px', 'role' => 'modal-remote']) : ''; ?>
                    <?= Html::a('<span class="fa fa-bullseye"></span>', ['update-output', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-info btn-xs', 'title' => 'Keluaran Hasil Kegiatan', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                    <?= Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], 
                    [
                        'role' => 'modal-remote',
                        'title' => 'Hapus Kegiatan ini',
                        'data-confirm' => false,
                        'data-method' => false,
                        'data-request-method' => 'post',
                        'data-confirm-title' => 'Konfirmasi',
                        'data-confirm-message' => 'Yakin ingin menghapus Kegiatan ini?',
                        'class' => 'btn btn-danger btn-xs'
                    ]); ?>
                <?php endif; ?>
            </div>
        </td>
    </tr>
</table>    