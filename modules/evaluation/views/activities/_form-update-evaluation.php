<?php

use app\components\DateHelper;
use app\models\Activities;
use app\models\ActivityCategory;
use app\models\ActivityEvaluationValue;
use app\models\Calculation;
use app\models\Employee;
use app\models\Evaluation;
use app\models\OkrEvaluation;
use app\models\PositionStructure;
use app\models\Status;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Activities */
/* @var $form yii\widgets\ActiveForm */

$isUpper = VwEmployeeManager::find()
                                            ->where(['employee_id' => $model->pic_id])
                                            ->andWhere(['upper_id' => Yii::$app->user->id])
                                            ->andWhere(['position_id' => $model->position_structure_id])
                                            ->one();

$isPic = $model->pic_id == Yii::$app->user->id;

$year = date('Y', strtotime($model->start_date));

$q1 = Activities::find()->where(['okr_id' => $model->okr_id])->andWhere(['pic_id' => $model->pic_id])->andWhere("'$year-01-01' <= start_date and start_date <= '" . date("Y-m-t", strtotime( $year . '-' . '03-01')) . "'");
$q2 = Activities::find()->where(['okr_id' => $model->okr_id])->andWhere(['pic_id' => $model->pic_id])->andWhere("'$year-04-01' <= start_date and start_date <= '" . date("Y-m-t", strtotime( $year . '-' . '06-01')) . "'");
$q3 = Activities::find()->where(['okr_id' => $model->okr_id])->andWhere(['pic_id' => $model->pic_id])->andWhere("'$year-07-01' <= start_date and start_date <= '" . date("Y-m-t", strtotime( $year . '-' . '09-01')) . "'");
$q4 = Activities::find()->where(['okr_id' => $model->okr_id])->andWhere(['pic_id' => $model->pic_id])->andWhere("'$year-10-01' <= start_date and start_date <= '" . date("Y-m-t", strtotime( $year . '-' . '12-01')) . "'");

$valQ1 = $q1->one() ? $q1->sum('okr_value') : 0;
$valQ2 = $q2->one() ? $q2->sum('okr_value') : 0;
$valQ3 = $q3->one() ? $q3->sum('okr_value') : 0;
$valQ4 = $q4->one() ? $q4->sum('okr_value') : 0;

$percentageQ1 =  $model->okr->measure == '%' ? number_format($valQ1, 2, ',', '.') . '%' : number_format($valQ1, 2, ',', '.') . '%';
$percentageQ2 =  $model->okr->measure == '%' ? number_format($valQ2, 2, ',', '.') . '%' : number_format($valQ2, 2, ',', '.') . '%';
$percentageQ3 =  $model->okr->measure == '%' ? number_format($valQ3, 2, ',', '.') . '%' : number_format($valQ3, 2, ',', '.') . '%';
$percentageQ4 =  $model->okr->measure == '%' ? number_format($valQ4, 2, ',', '.') . '%' : number_format($valQ4, 2, ',', '.') . '%';

$isQ1 = strtotime($year . '-01-01') <= strtotime($model->start_date) && strtotime($model->start_date) <= strtotime(date("Y-m-t", strtotime( $year . '-' . '03-01')));
$isQ2 = strtotime($year . '-04-01') <= strtotime($model->start_date) && strtotime($model->start_date) <= strtotime(date("Y-m-t", strtotime( $year . '-' . '06-01')));
$isQ3 = strtotime($year . '-07-01') <= strtotime($model->start_date) && strtotime($model->start_date) <= strtotime(date("Y-m-t", strtotime( $year . '-' . '09-01')));
$isQ4 = strtotime($year . '-10-01') <= strtotime($model->start_date) && strtotime($model->start_date) <= strtotime(date("Y-m-t", strtotime( $year . '-' . '12-01')));

$evaluation1 = Evaluation::findOne(['year' => date('Y', strtotime($model->start_date)), 'period' => 'Kuartal 1']);
$evaluation2 = Evaluation::findOne(['year' => date('Y', strtotime($model->start_date)), 'period' => 'Kuartal 2']);
$evaluation3 = Evaluation::findOne(['year' => date('Y', strtotime($model->start_date)), 'period' => 'Kuartal 3']);
$evaluation4 = Evaluation::findOne(['year' => date('Y', strtotime($model->start_date)), 'period' => 'Kuartal 4']);

$realQ1 = OkrEvaluation::findOne(['okr_id' => $model->okr_id, 'pic_id' => $model->pic_id, 'evaluation_id' => $evaluation1->id])->real ?: 0;
$realQ2 = OkrEvaluation::findOne(['okr_id' => $model->okr_id, 'pic_id' => $model->pic_id, 'evaluation_id' => $evaluation2->id])->real ?: 0;
$realQ3 = OkrEvaluation::findOne(['okr_id' => $model->okr_id, 'pic_id' => $model->pic_id, 'evaluation_id' => $evaluation3->id])->real ?: 0;
$realQ4 = OkrEvaluation::findOne(['okr_id' => $model->okr_id, 'pic_id' => $model->pic_id, 'evaluation_id' => $evaluation4->id])->real ?: 0;

$val = [];
if($valQ1) array_push($val, ($model->okr->measure == '%' ? $valQ1 : (round((($valQ1 * $model->okr->target_q1) / 100), 2) ?:0)));
if($valQ2) array_push($val, ($model->okr->measure == '%' ? $valQ2 : (round((($valQ2 * $model->okr->target_q2) / 100), 2) ?:0)));
if($valQ3) array_push($val, ($model->okr->measure == '%' ? $valQ3 : (round((($valQ3 * $model->okr->target_q3) / 100), 2) ?:0)));
if($valQ4) array_push($val, ($model->okr->measure == '%' ? $valQ4 : (round((($valQ4 * $model->okr->target_q4) / 100), 2) ?:0)));

if($model->okr->calculation_id == Calculation::SUM){
    $lastValue = array_sum($val);
} else if($model->okr->calculation_id == Calculation::AVERAGE){
    $lastValue = $val ? array_sum($val)/count($val) : 0;
} else if($model->okr->calculation_id == Calculation::LAST){
    $lastValue = end($val);
} else if($model->okr->calculation_id == Calculation::MIN){
    $lastValue = $val ? min($val) : 0;
}

// var_dump($valQ1);
// var_dump($valQ2);

$labelText = '';
if($isQ1){
    $labelText = 'Kontribusi Terhadap Capaian OKR Kuartal 1 (%)';
} else if($isQ2){
    $labelText = 'Kontribusi Terhadap Capaian OKR Kuartal 2 (%)';
} else if($isQ3){
    $labelText = 'Kontribusi Terhadap Capaian OKR Kuartal 3 (%)';
} else if($isQ4){
    $labelText = 'Kontribusi Terhadap Capaian OKR Kuartal 4 (%)';
}

?>

<style>
    .callout.callout-warning {
        background-color: rgb(111, 68, 137) !important;
        border-color: #603b78 !important;
    }
</style>

<div class="activities-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(($isUpper || $model->okr->year == 2023) || $isPic): ?>
        <div class="callout callout-warning">
            <b>Objective:</b><br />
            <?= $model->okr->parent->okr_code . ' - ' . $model->okr->parent->okr; ?><br />
            <br />
            <b>Key Result:</b><br />
            <?= $model->okr->okr_code . ' - ' . $model->okr->okr; ?><br />
            <br />
            <b>Kegiatan:</b><br />
            <?= $model->activity; ?><br />
            <br />
            <table width="100%">
                <tr>
                    <td width="50%"><b>Tanggal Mulai</b></td>
                    <td><b>Tanggal Selesai</b></td>
                </tr>
                <tr>
                    <td><?= DateHelper::getTglIndo(date('Y-m-d', strtotime($model->start_date))); ?></td>
                    <td><?= DateHelper::getTglIndo((date('Y-m-d', strtotime($model->due_date ?: $model->finish_date)))); ?></td>
                </tr>
            </table>
            <br />
            <b>Persentase Capaian Key Result:</b><br />
            <?= '';// ($model->okr->achievement_percentage ? $model->okr->achievement_percentage: 0).'%'; ?> 
            <?= '';// $model->okr->measure == '%' ? '' : ' (' . ($model->okr->target ? round(($model->okr->achievement_percentage * $model->okr->target) / 100, 2) : 0) . ' ' . $model->okr->measure . ') '; ?>
            <!-- dari target <?= '';// ($model->okr->target ?: 0) . ($model->okr->measure == '%' ? $model->okr->measure : ' ' . $model->okr->measure); ?> -->

            <div style="background: #fff;padding: 5px; color: var(--background);border-radius: 3px;">
                <table class="kv-grid-table table table-bordered table-condensed kv-table-wrap" style="margin-bottom: 0px;">
                    <tbody>
                        <tr>
                            <td></td>
                            <td style="<?= $isQ1 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><b>Kuartal 1</b></td>
                            <td style="<?= $isQ2 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><b>Kuartal 2</b></td>
                            <td style="<?= $isQ3 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><b>Kuartal 3</b></td>
                            <td style="<?= $isQ4 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><b>Kuartal 4</b></td>
                            <td><b>Hasil Akhir</b></td>
                        </tr>
                        <tr>
                            <td><b>Target</b></td>
                            <td style="<?= $isQ1 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= is_numeric($model->okr->target_q1) ? number_format($model->okr->target_q1, 0, ',', '.') . ' ' . $model->okr->measure : '-'; ?></td>
                            <td style="<?= $isQ2 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= is_numeric($model->okr->target_q2) ? number_format($model->okr->target_q2, 0, ',', '.') . ' ' . $model->okr->measure : '-'; ?></td>
                            <td style="<?= $isQ3 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= is_numeric($model->okr->target_q3) ? number_format($model->okr->target_q3, 0, ',', '.') . ' ' . $model->okr->measure : '-'; ?></td>
                            <td style="<?= $isQ4 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= is_numeric($model->okr->target_q4) ? number_format($model->okr->target_q4, 0, ',', '.') . ' ' . $model->okr->measure : '-'; ?></td>
                            <td><?= is_numeric($model->okr->target) ? number_format($model->okr->target, 0, ',', '.') . ' ' . $model->okr->measure . ' (' . $model->okr->calculation->calculation_type . ')' : '-'; ?></td>
                        </tr>
                        <tr>
                            <td><b>Realisasi</b></td>
                            <td style="<?= $isQ1 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= round($realQ1) . ' '. $model->okr->measure; ?></td>
                            <td style="<?= $isQ2 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= round($realQ2) . ' '. $model->okr->measure; ?></td>
                            <td style="<?= $isQ3 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= round($realQ3) . ' '. $model->okr->measure; ?></td>
                            <td style="<?= $isQ4 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= round($realQ4) . ' '. $model->okr->measure; ?></td>
                            <td><?= number_format($lastValue, 0, ',', '.') . ' ' . $model->okr->measure . ' (' . $model->okr->calculation->calculation_type . ')'; ?></td>
                        </tr>
                        <tr>
                            <td><b>Capaian</b></td>
                            <td style="<?= $isQ1 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= is_numeric($valQ1) ? $percentageQ1 : '-'; ?></td>
                            <td style="<?= $isQ2 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= is_numeric($valQ2) ? $percentageQ2 : '-'; ?></td>
                            <td style="<?= $isQ3 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= is_numeric($valQ3) ? $percentageQ3 : '-'; ?></td>
                            <td style="<?= $isQ4 ? 'background: var(--background) !important; color: #fff !important;':'' ?>"><?= is_numeric($valQ4) ? $percentageQ4 : '-'; ?></td>
                            <td><?= number_format($lastValue, 0, ',', '.') . ' ' . $model->okr->measure . ' (' . $model->okr->calculation->calculation_type . ')'; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?= $form->field($model, 'okr_value')->textInput(['type' => 'number', 'maxlength' => true])->label($labelText) ?>
        <?php if($isUpper): ?>
            <?= $form->field($model, 'evaluation')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                                            ActivityEvaluationValue::find()
                                                        ->orderBy('id')
                                                        ->all(), 
                                                        'evaluation', 'evaluation'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => [
                                'prompt' => '-- Pilih Penilaian --',
                            ],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        <?php endif; ?>
    <?php endif; ?>

    <?php ActiveForm::end(); ?>
    
</div>
<script>
$(document).on('keyup', '#activities-okr_value', function(event) {
   var v = this.value;
   if($.isNumeric(v) === false) {
        //chop off the last char entered
        this.value = this.value.slice(0,-1);
   }
});
</script>