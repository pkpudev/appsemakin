<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\select2\Select2;

$this->title = 'Kendala/Isu Kegiatan';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="issue-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => require(__DIR__.'/_columns-issues.php'),
            'toolbar'=> [
                // ['content'=>
                //     Html::a('<i class="fa fa-plus"></i> Tambah Kendala/Isu Kegiatan', ['create'],
                //     ['role'=>'modal-remote','title'=> 'Tambah Kendala/Isu Kegiatan','class'=>'btn btn-success'])
                // ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Daftar Kendala/Isu Kegiatan',
                'before' => /* (Yii::$app->user->can('Admin') or Yii::$app->user->can('BOD')) ? */
                    '<div class="row">' .
                    '<div class="col-md-2">' .
                        DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'year',
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'id' => 'year-filter',
                                'placeholder' => 'Filter Tahun',
                                'class' => 'form-control additional-filter',
                            ],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'startView'=>'year',
                                'minViewMode'=>'years',
                                'format' => 'yyyy',
                                'allowClear' => false
                            ]
                        ]) .
                    '</div>' .
                    '<div class="col-md-3">' .
                        Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'unit_structure_id',
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'id' => 'divisi-filter',
                                'class' => 'form-control additional-filter',
                                'placeholder' => '-- Filter Unit Kerja --',
                            ],
                            'pluginOptions' => ['allowClear' => true],
                            'data' => $listUnit,
                        ]) .
                    '</div>' .
                '</div>',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>