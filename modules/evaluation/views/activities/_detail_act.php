<?php

use app\models\Activities;
use app\models\ActivitiesIssue;
use app\models\ActivityCategory;
use yii\bootstrap\Progress;
use yii\helpers\Html;

?>
<style>
    body {
        --current-text-color: #fff; 
        --current-background-color: var(--background); 
    }
    
    .dark-mode {
        --current-text-color: #2a2f32 !important; 
        --current-background-color: #fff !important; 
    }
</style>

<table width="100%">
    <tr>
        <td>
            <b>Kategori Kegiatan: </b><br />
            <?= $model->category->activity_category; ?><br />
            <br />
            <b><?= $model->activity_category_id == ActivityCategory::REGULER ? 'Key Result' : 'Unit Yang Didudukung'; ?>: </b><br />
            <?php if($model->activity_category_id == ActivityCategory::REGULER): ?>
                <?= $model->okr->okr_code; ?> - <?= $model->okr->okr; ?><br />
            <?php else: ?>
                <?= $model->supportedUnit->unit_name; ?>
            <?php endif; ?>
            <br />
            <b>Keluaran Kegiatan: </b><br />
            <?= $model->output ?: '-'; ?>
            <?php if($model->project_id): ?>
            <br />
            <b>No. Project: </b><br />
            <?php
                $ch = curl_init('https://project.c27g.com/api/project/list?app_key=dBqTZrAwpAII41zxdIj0BM_siWPz6kPO&id=' . $model->project_id);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // execute!
                $response = curl_exec($ch);
                $results = json_decode($response, true);

                // close the connection, release resources used
                curl_close($ch);

                echo $results['text'] ;
            ?>
            <?php endif; ?>
        </td>
        <td width="450px" valign="top">
            <table width="100%" border="1" style="border: 1px solid var(--current-background-color);">
                <tr>
                    <td align="center" style="border: 1px solid var(--current-background-color); background: var(--current-background-color); color: var(--current-text-color); font-weight: bolder;" width="150px">Nilai Atasan</td>
                    <td align="center" style="border: 1px solid var(--current-background-color); background: var(--current-background-color); color: var(--current-text-color); font-weight: bolder;" width="150px">Nilai OKR</td>
                    <td align="center" style="border: 1px solid var(--current-background-color); background: var(--current-background-color); color: var(--current-text-color); font-weight: bolder;" width="150px">Kendala/Isu</td>
                </tr>
                <tr>
                    <td align="center" style="border: 1px solid var(--current-background-color);"><?= $model->evaluation ?: '-'; ?></td>
                    <td align="center" style="border: 1px solid var(--current-background-color);"><?= $model->okr_value ? $model->okr_value.'%' : '-'; ?></td>
                    <td align="center" style="border: 1px solid var(--current-background-color);">
                        <?php $cntIssue = ActivitiesIssue::find()->where(['activities_id' => $model->id])->count(); ?>
                        <?= Html::a($cntIssue . ' Kendala/Isu', ['issue', 'activity_id' => $model->id], ['role' => 'modal-remote', 'style' => 'font-weight: bolder;']) ?>
                        <?= Html::a('<span class="fa fa-plus"></span>', ['create-issue', 'activity_id' => $model->id], ['class'=>'btn btn-danger btn-xs', 'role' => 'modal-remote', 'style' => 'margin-top: -3px;font-size: 10px;']) ?>
                    </td>
                </tr>
            </table>
            <table width="100%" border="1" style="border: 1px solid var(--current-background-color);">
                <tr>
                    <td align="center" style="border: 1px solid var(--current-background-color); background: var(--current-background-color); color: var(--current-text-color); font-weight: bolder;" width="50%">Jumlah Durasi</td>
                    <td align="center" style="border: 1px solid var(--current-background-color); background: var(--current-background-color); color: var(--current-text-color); font-weight: bolder;" width="50%">Progress</td>
                </tr>
                <tr>
                    <td align="center" style="border: 1px solid var(--current-background-color);"><?= $model->hour ?: 0; ?> Jam <?= $model->minutes ?: 0; ?> Menit</td>
                    <td align="center" style="border: 1px solid var(--current-background-color);"><?= round($model->progress, 2) ?: 0; ?>%</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<b>Sub Kegiatan: </b><br />
<?php
    $sub1Activities = Activities::find()->where(['parent_id' => $model->id])->orderBy('id')->all();

    if(!$sub1Activities) echo '<i>Tidak mempunyai sub kegiatan.</i>';
    foreach($sub1Activities as $sub1Activity):
?>
<div class="panel-group" id="sub-activity-<?= $sub1Activity->parent_id; ?>">
    <!-- SUB1.<?= $sub1Activity->id; ?> -->
    <div class="panel panel-default">
        <div class="panel-heading" style="border: 1px solid var(--background); margin: 5px 0; padding: 5px; border-radius: 5px; border-left: 5px solid var(--background);">
            <table width="100%">
                <tr>
                    <td>
                        <a data-toggle="collapse" data-parent="#sub-activity-<?= $sub1Activity->parent_id; ?>" href="#activity-<?= $sub1Activity->id; ?>" style="color: var(--background) !important; display: none;">
                            <?= $sub1Activity->activity; ?>
                        </a>
                        <?= Html::a(
                                        $sub1Activity->activity, 
                                        ['view', 'id' => $sub1Activity->id],
                                        [
                                            'role' => 'modal-remote',
                                            // 'style' => 'color: var(--background) !important;'
                                        ]
                                        ); ?>
                    </td>
                    <td width="50px">
                        <?= Activities::WEIGHT_TEXT[$sub1Activity->weight]; ?>
                    </td>
                    <td width="150px">
                        <?php 
                            // $totalWeight = Activities::find()->where(['parent_id' => $model->id])->sum('weight');
                            $totalWeight = $sub1Activity->weight;

                            if($sub1Activity->progress){
                                $progress = $sub1Activity->progress/100;
                                $progressText = round(($sub1Activity->weight*$progress), 2) . '/' . $totalWeight;
                            } else {
                                $progressText = '0/' . $totalWeight;

                            }
                        ?>
                        <?= Progress::widget(['percent' => $sub1Activity->progress, 'label' => $sub1Activity->progress.'%', 'options' => ['style' => 'margin-bottom: 0;']]); ?>
                    </td>
                    <td width="120px" align="center">
                        <?= $sub1Activity->hour ?: 0; ?> Jam <?= $sub1Activity->minutes ?: 0; ?> Menit
                    </td>
                    <td width="120px" align="center">
                        <?= $sub1Activity->status->getStatusname($sub1Activity->status_id); ?>
                        <!-- <span class="label label-success" style="width: 120px !important; display: block;">Selesai</span> -->
                    </td>
                    <td width="130px" align="right">
                        <div class="btn-group">
                            <?= Html::a('<span class="fa fa-dot-circle-o"></span>', ['update-status', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-default btn-xs', 'title' => 'Ubah Progress', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                            <?= Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-warning btn-xs', 'title' => 'Ubah', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                            <?= Html::a('<span class="fa fa-info"></span>', ['view', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-info btn-xs', 'title' => 'Detail', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                            <?= Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], 
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Hapus Kegiatan ini',
                                    'data-confirm' => false,
                                    'data-method' => false,
                                    'data-request-method' => 'post',
                                    'data-confirm-title' => 'Konfirmasi',
                                    'data-confirm-message' => 'Yakin ingin menghapus Kegiatan ini?',
                                    'class' => 'btn btn-danger btn-xs'
                                ]); ?>
                            <?php // Html::a('<span class="fa fa-plus"></span>', ['create', 'parent_id' => $sub1Activity->id], ['class' => 'btn btn-success btn-xs', 'title' => 'Tambah', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="activity-<?= $sub1Activity->id; ?>" class="panel-collapse collapse">
            <div class="panel-body" style="border: 1px solid var(--background);">
                <b>Deskripsi Kegiatan: </b><br />
                <?= $sub1Activity->description; ?>
                <br /><br />
                <b>Sub Kegiatan: </b><br />
                <i>Tidak mempunyai sub kegiatan.</i>

            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>  