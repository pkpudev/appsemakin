<?php

use app\models\UnitStructure;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

CrudAsset::register($this);

$this->title = 'ABK';
?>
<style>
    .blink {
    animation: blinker 1s linear infinite;
  }

  @keyframes blinker {
    50% {
      opacity: 0;
    }
  }
</style>
<div class="abk">

    <div class="box box-default">
        <div class="box-body">
            <?=  GridView::widget([
                'id'=>'okr-individual',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => '.additional-filter',
                'showPageSummary' => true,
                'responsiveWrap' => false,
                'pjax'=>false,
                'hover' => true,
                'export' => [
                    'label' => 'Export Excel atau PDF',
                    'header' => '',
                    'options' => [
                        'class' => 'btn btn-primary'
                    ], 
                    'menuOptions' => [ 
                            'style' => 'background: var(--background); width: 187px;'
                    ]
                ],
                'exportConfig' => [
                    GridView::PDF => true,
                    GridView::EXCEL => true,
                ],
                'columns' => require(__DIR__.'/_columns-abk.php'),
                'toolbar'=> [
                    ['content'=>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                        '{toggleData}'
                    ],
                    ['content'=>
                        '{export}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => 'Jam Kerja Individu',
                    'before'=>'<table><tr>' .
                                    '<td width="150px">' .
                                        DatePicker::widget([
                                            'model' => $searchModel,
                                            'attribute' => 'year',
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'id' => 'year-filter',
                                                'placeholder' => 'Tahun',
                                                'class' => 'form-control additional-filter',
                                            ],
                                            'removeButton' => false,
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'startView'=>'year',
                                                'minViewMode'=>'years',
                                                'format' => 'yyyy',
                                                'allowClear' => false
                                            ]
                                        ]) .
                                    '</td>' .
                                    '<td width="200px" style="padding-left: 10px;">' .
                                        Select2::widget([
                                            'model' => $searchModel,
                                            'attribute' => 'period',
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'id' => 'period-filter',
                                                'class' => 'form-control additional-filter',
                                                'placeholder' => '-- Periode --',
                                            ],
                                            'pluginOptions' => ['allowClear' => true],
                                            'data' => ['Kuartal 1' => 'Kuartal 1', 'Kuartal 2' => 'Kuartal 2', 'Kuartal 3' => 'Kuartal 3', 'Kuartal 4' => 'Kuartal 4'],
                                        ]) .
                                    '</td>' .
                                    '<td width="350px" style="padding-left: 10px;">' .
                                        Select2::widget([
                                            'model' => $searchModel,
                                            'attribute' => 'department_id',
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'id' => 'departement-filter',
                                                'class' => 'form-control additional-filter',
                                                'placeholder' => '-- Departement --',
                                            ],
                                            'pluginOptions' => ['allowClear' => true],
                                            'data' => ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['level' => 2])->orderBy('unit_code')->all(), 'id', function($model){
                                                return $model->unit_code . ' - ' . $model->unit_name;
                                            }),
                                        ]) .
                                    '</td>' .
                                '</tr></table>',
                    'after'=>'',
                ]
            ])?>
        </div>
    </div>

</div>
<?php
$url = Yii::$app->controller->action->id;
$script = <<<JS
$(document.body).on("change", "#from_date-filter",function(){
    var from_date = jQuery('#from_date-filter').val() || '',
        to_date = jQuery('#to_date-filter').val() || '',
        url = '$url?VwEmployeeManagerSearch[from_date]=' + from_date 
                + '&VwEmployeeManagerSearch[to_date]=' + to_date
    window.location.href = url;
});
    
JS;
$this->registerJs($script, $this::POS_READY);
?>