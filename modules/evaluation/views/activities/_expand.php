<?php

use app\models\Activities;
use yii\helpers\Html;
use yii\widgets\Pjax;

?>
<div class="expanded-row">

    <?= Html::a('<span class="fa fa-plus"></span>', ['create', 'parent_id' => $model->id], ['class' => 'btn btn-success btn-xs', 'title' => 'Tambah', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>

    <?php Pjax::begin(['id' => "sub-act-{$model->id}-pjax"]) ?>
    <?php 
        $cnt = Activities::find()->where(['parent_id' => $model->id])->count();
        echo "Jumlah sub-act: $cnt";
    ?>
    <?php Pjax::end() ?>
</div>