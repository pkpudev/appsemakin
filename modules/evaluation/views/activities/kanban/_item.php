<?php

use app\components\DateHelper;
use app\components\PhotosHelper;
use app\models\Activities;
use app\models\Status;
use yii\helpers\Html;

?>
<style>
    .custom-box {
        max-width: 80%;
        border-radius: 3px; 
        border: 1px solid #eee; 
        padding: 5px; 
        margin: 10px auto 15px; 
        box-shadow: 3px 3px 5px rgba(0, 0, 0, 0.5);
        word-wrap: break-word
    }

    .custom-box:hover {
        box-shadow: 3px 3px 6px rgba(0, 0, 0, 0.7);
    }
</style>
<div class="item" id="act-<?= $model->id; ?>">


</div>


<?php
$modelId = $model->id;
$params = Yii::$app->getRequest()->getQueryParam('ActivitiesSearch');

$urlParams = '';
if($params){
    foreach($params as $key => $val){
        $urlParams .= '&ActivitiesSearch[' . $key . ']=' . $val;
    }
}


$script = <<<JS
    $(document).ready(function () {
        var modelId = $modelId
            urlParams = "$urlParams";
        // var interval = 5000;   //number of mili seconds between each call
        
        var refreshAct = function() {
            $.ajax({
                url: "/evaluation/activities/detail-kanban?id=" + modelId + urlParams,
                cache: false,
                success: function(html) {
                    $("#act-"+ modelId).html(html);
                }
            });
        };
        refreshAct();
    });
JS;
$this->registerJs($script, $this::POS_READY);
?>