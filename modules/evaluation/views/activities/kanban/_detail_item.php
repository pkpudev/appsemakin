<?php

use app\components\DateHelper;
use app\components\PhotosHelper;
use app\models\Activities;
use app\models\ActivitiesFiles;
use app\models\ActivitiesIssue;
use app\models\ActivityCategory;
use app\models\Status;
use app\models\VwEmployeeManager;
use yii\helpers\Html;

$hasChild = Activities::find()->where(['parent_id' => $model->id])->one();

$cntFiles = ActivitiesFiles::find()->where(['activities_id' => $model->id])->count();
?>
<style>
    .blink {
        animation: blinker 1s linear infinite;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }
</style>
<div class="custom-box">
    <table width="100%">
        <tr>
            <td valign="top">
                <?php
                    if($model->status_id == Status::ACT_DONE){
                        $strikethrough = false;
                        $iconMainActivity = "fa-check-circle-o";
                        $colorMainActivity = "var(--success)";
                        $titleMainActivity = "Sudah Selesai";
                    } else if($model->status_id == Status::ACT_ON_PROGRESS){
                        $strikethrough = false;
                        $iconMainActivity = "fa-spinner";
                        $colorMainActivity = "var(--warning)";
                        $titleMainActivity = "Sedang Berlangsung";
                    } else { 
                        $strikethrough = false;
                        $iconMainActivity = "";
                    }
                ?>
                <?= $strikethrough ? '<del>' : ''; ?>
                    <?= Html::a($model->activity, ['view', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['role' => 'modal-remote', 'style' => 'font-size: 15px;']); ?>
                <?= $strikethrough ? '</del>' : ''; ?>
                <br /><br />
                
                <?php
                    $childs = Activities::find()->where(['parent_id' => $model->id])->orderBy('id')->all();
                    if($childs):
                ?>
                    <span style="font-size: 13px; display: <?= $model->status_id == Status::ACT_DONE ? 'none':'block'; ?>;">
                        Sub Kegiatan: <br />
                        <table width="100%" style="margin-bottom: 15px;">
                            <?php $cntDone = 0; $no = 1; foreach($childs as $child): ?>
                                <tr>
                                    <td width="18px" align="center" valign="top">
                                        <?php
                                            if($child->status_id == Status::ACT_DONE){
                                                $strikethrough = false;
                                                $icon = 'fa-check-circle-o';
                                                $color = 'var(--success)';
                                                $title = 'Sudah Selesai';
                                            } else if($child->status_id == Status::ACT_ON_PROGRESS){
                                                $strikethrough = false;
                                                $icon = 'fa-spinner';
                                                $color = 'var(--warning)';
                                                $title = 'Sedang Berlangsung';
                                            } else { 
                                                $icon = '';
                                                $strikethrough = false;
                                            }
                                        ?>
                                        <span class="fa <?= $icon; ?>" style="color: <?= $color; ?>;" title="<?= $title; ?>"></span>
                                    </td>
                                    <td width="18px" valign="top">
                                            <?= $no; ?>.
                                    </td>
                                    <td valign="top">
                                        <?= $strikethrough ? '<del>' : ''; ?>
                                            <?= Html::a($child->activity, ['view', 'id' => $child->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['role' => 'modal-remote']); ?>
                                        <?= $strikethrough ? '</del>' : ''; ?>
                                    </td>
                                </tr>
                                
                                <?php 
                                    if($child->status_id == Status::ACT_DONE){
                                        $cntDone++;
                                    } 
                                ?>
                            <?php $no++; endforeach; ?>
                        </table>
                    </span>
                <?php endif; ?>
            </td>
            <td width="30px" align="center" valign="top">
                <div class="btn-group-vertical" style="display: none;">
                    <?= $hasChild ? '' : Html::a('<span class="fa fa-dot-circle-o"></span>', ['update-status', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['class' => 'btn btn-default btn-xs', 'title' => 'Ubah Progress', 'style' => 'color: var(--default) !important; width: 25px;', 'role' => 'modal-remote']); ?>
                    <?= Html::a('<span class="fa fa-plus"></span>', ['create', 'parent_id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['class' => 'btn btn-success btn-xs', 'title' => 'Tambah Sub Kegiatan', 'style' => 'color: var(--success) !important; width: 25px;', 'role' => 'modal-remote']); ?>
                    <?= Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['class' => 'btn btn-warning btn-xs', 'title' => 'Ubah', 'style' => 'color: var(--warning) !important; width: 25px;', 'role' => 'modal-remote']); ?>
                    <?= Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['class' => 'btn btn-info btn-xs', 'title' => 'Detail', 'role' => 'modal-remote', 'style' => 'color: var(--info) !important; width: 25px;']); ?>
                    <?= $model->activity_category_id == ActivityCategory::REGULER/*  && !$isManager */ ? Html::a('<span class="fa fa-edit"></span>', ['update-evaluation', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['class' => 'btn btn-success btn-xs', 'title' => 'Kontribusi Kegiatan', 'style' => 'width: 25px;', 'role' => 'modal-remote']) : ''; ?>
                    <?= Html::a('<span class="fa fa-bullseye"></span>', ['update-output', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['class' => 'btn btn-info btn-xs', 'title' => 'Keluaran Hasil Kegiatan', 'style' => 'width: 25px;', 'role' => 'modal-remote']); ?>
                   <?= Html::a('<span class="fa fa-exclamation-triangle"></span>', ['create-issue', 'activity_id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['class'=>'btn btn-danger btn-xs', 'role' => 'modal-remote', 'style' => 'margin-top: -3px;font-size: 10px;']); ?>
                   <?= Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], 
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Hapus Kegiatan ini',
                                    'data-confirm' => false,
                                    'data-method' => false,
                                    'data-request-method' => 'post',
                                    'data-confirm-title' => 'Konfirmasi',
                                    'data-confirm-message' => 'Yakin ingin menghapus Kegiatan ini?',
                                    'class' => 'btn btn-danger btn-xs', 
                                    'style' => 'color: var(--danger) !important; width: 25px;'
                                ]); ?>
                                <br />
                </div>
                <?php 
                $upperId = VwEmployeeManager::findOne(['employee_id' => $model->pic_id, 'position_id' => $model->position_structure_id])->upper_id;
                    if(in_array(Yii::$app->user->id, [$model->pic_id, $upperId])): 
                ?>
                    <div class="dropdown" style="float:right;">
                        <button class="btn btn-default dropbtn"><span class="fa fa-ellipsis-h"></span></button>
                        <div class="dropdown-content">
                            <?= $hasChild ? '' : Html::a('<span class="fa fa-dot-circle-o"></span>&nbsp;&nbsp;Ubah Progress', ['update-status', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['title' => 'Ubah Progress', 'role' => 'modal-remote']); ?>
                            <?= Html::a('<span class="fa fa-plus"></span>&nbsp;&nbsp;Tambah Sub Kegiatan', ['create', 'parent_id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['title' => 'Tambah Sub Kegiatan', 'role' => 'modal-remote']); ?>
                            <?= Html::a('<span class="fa fa-pencil"></span>&nbsp;&nbsp;Ubah Kegiatan', ['update', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['title' => 'Ubah', 'role' => 'modal-remote']); ?>
                            <?= Html::a('<span class="fa fa-eye"></span>&nbsp;&nbsp;Lihat Detail Kegiatan', ['view', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['title' => 'Detail', 'role' => 'modal-remote']); ?>
                            <?= $model->activity_category_id == ActivityCategory::REGULER/*  && !$isManager */ ? Html::a('<span class="fa fa-edit"></span>&nbsp;&nbsp;Kontribusi Kegiatan', ['update-evaluation', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['title' => 'Kontribusi Kegiatan', 'role' => 'modal-remote']) : ''; ?>
                            <?= Html::a('<span class="fa fa-bullseye"></span>&nbsp;&nbsp;Keluaran Hasil Kegiatan', ['update-output', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['title' => 'Keluaran Hasil Kegiatan', 'role' => 'modal-remote']); ?>
                            <?= Html::a('<span class="fa fa-exclamation-triangle"></span>&nbsp;&nbsp;Tambah Kendala/Issue', ['create-issue', 'activity_id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], ['title' => 'Tambah Kendala/Issue Kegiatan', 'role' => 'modal-remote']); ?>
                            <?= Html::a('<span class="fa fa-trash"></span>&nbsp;&nbsp;Hapus Kegiatan', ['delete', 'id' => $model->id, 'page' => 'kanban', 'ActivitiesSearch' => Yii::$app->getRequest()->getQueryParam('ActivitiesSearch')], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Hapus Kegiatan ini',
                                            'data-confirm' => false,
                                            'data-method' => false,
                                            'data-request-method' => 'post',
                                            'data-confirm-title' => 'Konfirmasi',
                                            'data-confirm-message' => 'Yakin ingin menghapus Kegiatan ini?'
                                        ]); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <?php if($childs): ?>
                    <span class="fa <?= $iconMainActivity; ?>" style="color:  <?= $colorMainActivity; ?>;" title="<?= $titleMainActivity; ?>"></span> <?= $cntDone . '/' . count($childs); ?>
                    &nbsp;
                    <?= $cntFiles ? '<span class="fa fa-paperclip"></span> ' . $cntFiles : ''; ?>
                <?php else: ?>
                    <?= $cntFiles ? '<span class="fa fa-paperclip"></span> ' . $cntFiles : ''; ?>
                <?php endif; ?>
                
                <?php
                    $cntIssue = ActivitiesIssue::find()->where(['activities_id' => $model->id])->count();

                    if($cntIssue){
                        echo "<span class='fa fa-exclamation-triangle'></span> $cntIssue";
                    }
                ?>
            </td>
            <td>
        </tr>
        <?php if($model->project_id): ?>
        <tr>
            <td colspan="2">
                <?php
                    $ch = curl_init('https://project.c27g.com/api/project/list?app_key=dBqTZrAwpAII41zxdIj0BM_siWPz6kPO&id=' . $model->project_id);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                    // execute!
                    $response = curl_exec($ch);
                    $results = json_decode($response, true);

                    // close the connection, release resources used
                    curl_close($ch);

                    echo $results['text'] ;
                ?>
            </td>
            <td>
        </tr>
        <?php endif; ?>
    </table>
    <hr style="margin: 5px 0; border-color: var(--background)" />
    <table style="table-layout: fixed;width: 100%;">
        <tr>
            <?php if($model->status_id == Status::ACT_DONE): ?>
                <td>
                    <div class="alert alert-success" style="margin: 0;padding: 2px;width: fit-content;font-size: 11px;">
                        <span class="fa fa-calendar"></span> Diselesaikan pada <?= DateHelper::getTglIndo(date('Y-m-d', strtotime($model->finish_date))); ?>
                    </div>
                </td>
            <?php else: ?>
                <td>
                    <div class="<?= (strtotime(date('Y-m-d')) > strtotime($model->due_date) && $model->status_id != Status::ACT_DONE) ? 'alert alert-danger' : ''; ?>" style="margin: 0;padding: 2px;width: fit-content;font-size: 11px;">
                        <span class="fa fa-calendar"></span> <?= DateHelper::getTglIndo(date('Y-m-d', strtotime($model->due_date))); ?>
                    </div>
                </td>
            <?php endif; ?>
            <td align="right"><b><?= $model->pic->full_name; ?></b></td>
            <td rowspan="2" width="35px" align="center" valign="middle" style="padding-left: 4px;">
                <div class="cropcircle" style="width: 25px; height: 25px; background-position: center center; background-repeat: no-repeat; background-size: cover; background-image: url('<?= PhotosHelper::getProfilePhotosUrl($model->pic_id); ?>')">
            </td>
        </tr>
        <tr>
            <td><?= $model->okr_value ? '<i><span class="fa fa-star blink" style="color: var(--warning) !important;"></span> Nilai Kontribusi: ' . $model->okr_value . '%</i>' : ''; ?></td>
            <td align="right" style="text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width: 100%;" nowrap="nowrap">
                <?php $category = explode('(', $model->category->activity_category); ?>
                <i title="<?= $model->position_structure_id ? $model->positionStructure->position_name : $category[0]; ?>">
                    <?= $model->position_structure_id ? $model->positionStructure->position_name : $category[0]; ?>
                </i>
            </td>
        </tr>
    </table>
</div>