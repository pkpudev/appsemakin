<?php

use app\components\DateHelper;
use app\models\Activities;
use app\models\IssueType;
use app\models\Status;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'unit_id',
        'filter' => false,
        'format' => 'html',
        'value' => function ($model, $key, $index, $widget) {
            return '<b>' . $model->activities->okr->position->unit->unit_code . ' ' . $model->activities->okr->position->unit->unit_name . '</b>';
        },
        'group' => true,
        'groupedRow' => true,                    // move grouped column to a single grouped row
        'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'okr',
        'label'=>'OKR',
        'format'=>'raw',
        'value' => function($model){
            return '<b>' . $model->activities->okr->okr_code . '</b><br />' . $model->activities->okr->okr;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'activity',
        'label'=>'Kegiatan',
        'value' => function($model){
            return $model->activities->activity;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'reporting_date',
        'value' => function($model){
            return $model->reporting_date ? DateHelper::getTglIndo($model->reporting_date) : '-';
        },
        'filterType'=>GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => ['format' => 'yyyy-mm-dd', 'autoclose' => true, 'todayHighlight' => true,],
        ],
        'contentOptions' => ['style' => 'width:150px;'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'issue',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'issue_type_id',
        'value' => function($model){
            return $model->issue_type_id ? $model->type->issue_type : '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(IssueType::find()->asArray()->all(), 'id', 'issue_type'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
        'width' => '150px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'issue_level',
        'value' => function($model){
            return Activities::ISSUE_LEVEL[$model->issue_level];
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>Activities::ISSUE_LEVEL,
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
        'width' => '150px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'alternative_solution',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'due_date',
        'value' => function($model){
            return $model->due_date ? DateHelper::getTglIndo($model->due_date) : '-';
        },
        'filterType'=>GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => ['format' => 'yyyy-mm-dd', 'autoclose' => true, 'todayHighlight' => true,],
        ],
        'contentOptions' => ['style' => 'width:150px;'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'full_name',
        'label' => 'Nama',
        'value' => function($model){
            return $model->activities->pic->full_name;
        }
    ],
    [
        'label' => 'Durasi Berlangsung Isu',
        'value' => function($model){
            if($model->reporting_date && $model->due_date){
                $date1 = date_create($model->reporting_date);
                $date2 = date_create($model->due_date);
                $diff = date_diff($date1,$date2);
                return $diff->format("%R%a hari");
            } else {
                return '-';
            }
        },
        'hAlign' => 'center'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status_id',
        'format' => 'raw',
        'value' => function($model){
            return $model->status->getStatusname($model->status_id);
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(Status::find()->where(['scenario' => Status::SCENARIO_ISSUE])->asArray()->all(), 'id', 'full_name'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
        'width' => '150px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'resolution',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },   
        'template' => '{update}&nbsp&nbsp;{view}&nbsp&nbsp;{delete}',
        'buttons' => [
            'update' => function ($url, $model) {
                return Html::a('<span class="fa fa-pencil"></span>',
                    ['update-issue', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], 
                    [
                        'title' => 'Ubah Kendala ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--warning) !important'
                    ]
                );
            },
            'view' => function ($url, $model) {
                return Html::a('<span class="fa fa-eye"></span>',
                    ['view-issue', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], 
                    [
                        'title' => 'Lihat Kendala ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--primary) !important'
                    ]
                );
            },
            'delete' => function ($url, $model) {
                return Html::a('<span class="fa fa-trash"></span>',
                    ['delete-issue', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], 
                    [
                        'title' => 'Hapus Kendala ini',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Konfirmasi',
                        'data-confirm-message'=>'Yakin ingin menghapus Kendala ini?',
                        'style' => 'color: var(--danger) !important'
                    ]
                );
            },
        ],
        'width' => '70px'
    ],

];
