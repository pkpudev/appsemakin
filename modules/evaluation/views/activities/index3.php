<?php

use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

CrudAsset::register($this);

?>
<style>
    .panel-group {
        margin-bottom: 0 !important;
    }

    .panel-default {
        border-color: transparent !important;
    }

    .panel-heading {
        background-color: transparent !important;
    }

    .panel-body {
        border-radius: 0 0 5px 5px;
        margin-top: -8px;
        border-top-color: transparent !important;
    }

    .btn-tab {
        border-color: var(--background) !important;
        color: var(--background) !important;
    }

    .btn-tab-active, .btn-tab:hover {
        background: var(--background) !important;
        color: #fff !important;
    }
</style>
<div class="">
    <?= $this->render('_header', [
            'cntAllAct' => $cntAllAct,
            'cntDoneAct' => $cntDoneAct,
            'cntProgressAct' => $cntProgressAct,
            'cntNewAct' => $cntNewAct,
            'cntCancelAct' => $cntCancelAct,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
    ]) ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-body">
                    
                    <div class="btn-group">
                        <?= Html::a('Kegiatan Hari Ini', ['#'], ['class' => 'btn btn-default btn-tab btn-tab-active btn-sm', 'style' => 'width: 170px;']); ?>
                        <?= Html::a('Semua Kegiatan', ['all'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
                        <?= $isManager ? Html::a('Kegiatan Staf', ['lower'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']) : ''; ?>
                    </div>
                    <?= Html::a('Tambah Kegiatan', ['create'], ['class' => 'btn btn-success pull-right btn-sm', 'role' => 'modal-remote']); ?>
                    <div class="clearfix"></div>

                    <div id="ajaxCrudDatatable" style="padding-top: 10px;">

                        <?php Pjax::begin(['id' => 'activities-list-pjax', 'timeout' => 5000]) ?>
                            <?= ListView::widget([
                                'dataProvider' => $dataProvider,
                                'options' => [
                                    'tag' => 'div',
                                    'class' => 'list-wrapper',
                                    'id' => 'list-wrapper',
                                ],
                                'layout' => "{summary}
                                            <div class='panel-group' id='activities-grp'>{items}</div>\n
                                            <div class='pull-right' style='margin-top: 20px'>{pager}</div>\n
                                            <div class='clearfix'></div>",
                                'itemView' => function ($model, $key, $index, $widget) {
                                    return $this->render('_act_item', ['model' => $model]);

                                    // or just do some echo
                                    // return $model->title . ' posted by ' . $model->author;
                                },
                                'itemOptions' => [
                                    'tag' => false,
                                ],
                                'pager' => [
                                    'nextPageLabel' => 'Selanjutnya',
                                    'prevPageLabel' => 'Sebelumnya',
                                    'maxButtonCount' => 5,
                                ],
                            ]); ?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>