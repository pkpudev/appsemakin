<?php

use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

CrudAsset::register($this);

if(Yii::$app->controller->action->id == 'index'){       
    $this->title = 'Kegiatan Hari Ini';
} else if(Yii::$app->controller->action->id == 'all'){
    $this->title = 'Semua Kegiatan';
} else if(Yii::$app->controller->action->id == 'lower'){
    $this->title = 'Semua Kegiatan Bawahan';
}

?>
<style>
    .box-filter {
        border: 1px solid var(--background) !important;
        margin-bottom: 10px;
        padding: 5px;
        width: 100%;
        max-width: 800px;
    }

    .panel-group {
        margin-bottom: 0 !important;
    }

    .panel-default {
        border-color: transparent !important;
    }

    .panel-heading {
        background-color: transparent !important;
    }

    .panel-body {
        border-radius: 0 0 5px 5px;
        margin-top: -8px;
        border-top-color: transparent !important;
    }

    .btn-tab {
        border-color: var(--background) !important;
        color: var(--background) !important;
    }

    .btn-tab-active, .btn-tab:hover {
        background: var(--background) !important;
        color: #fff !important;
    }

    .table-bordered, .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid var(--background) !important;
    }

    /** DARK MODE */
    .dark-mode .box-filter {
        border: 1px solid #fff !important;
        margin-bottom: 10px;
        padding: 5px;
        width: 100%;
        max-width: 800px;
    }
    .dark-mode .btn-tab {
        border-color: #fff !important;
        color: #fff !important;
    }

    .dark-mode .table-bordered, .table-bordered > thead > tr > th, 
    .dark-mode .table-bordered > tbody > tr > th, 
    .dark-mode .table-bordered > tfoot > tr > th, 
    .dark-mode .table-bordered > thead > tr > td, 
    .dark-mode .table-bordered > tbody > tr > td, 
    .dark-mode .table-bordered > tfoot > tr > td {
        border: 1px solid #fff;
    }
    
    .dark-mode .panel-default > .panel-heading {
        color: #fff !important;
    }
</style>
<div class="">

    <?= $this->render('_header', [
            'isAdmin' => $isAdmin,
            'searchModel' => $searchModel,
            'cntAllAct' => $cntAllAct,
            'cntDoneAct' => $cntDoneAct,
            'cntProgressAct' => $cntProgressAct,
            'cntNewAct' => $cntNewAct,
            'cntCancelAct' => $cntCancelAct,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
            'listUnit' => $listUnit,
            'listDirectorate' => $listDirectorate,
            'listDepartment' => $listDepartment,
            'listEmployee' => $listEmployee,
            'listOkr' => $listOkr,
    ]) ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-body">
                    <div id="ajaxCrudDatatable" >
                        <?= Html::a('Tambah Rencana Kegiatan', ['create'], ['class' => 'btn btn-success pull-right btn-sm', 'role' => 'modal-remote']); ?>
                        <?php Pjax::begin(['id' => 'activities-list-pjax', 'timeout' => 5000]) ?>
                            <?= ListView::widget([
                                'dataProvider' => $dataProvider,
                                'options' => [
                                    'tag' => 'div',
                                    'class' => 'list-wrapper',
                                    'id' => 'list-wrapper',
                                ],
                                'layout' => "<div style='width: fit-content;float: left;'>{summary}</div>"  .
                                            "<div class='clearfix'></div>
                                            <hr style='margin: 10px 0;' />
                                            <div class='panel-group' id='activities-grp'>{items}</div>\n
                                            <div class='pull-right' style='margin-top: 20px'>{pager}</div>\n
                                            <div class='clearfix'></div>",
                                'itemView' => function ($model, $key, $index, $widget) {
                                    return $this->render('_act_item', ['model' => $model]);

                                    // or just do some echo
                                    // return $model->title . ' posted by ' . $model->author;
                                },
                                'itemOptions' => [
                                    'tag' => false,
                                ],
                                'pager' => [
                                    'nextPageLabel' => 'Selanjutnya',
                                    'prevPageLabel' => 'Sebelumnya',
                                    'maxButtonCount' => 5,
                                ],
                            ]); ?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<?php
$script = <<<JS
    $( "#refresh-activity" ).click(function() {
        alert( "refresh-activity" );
    });
JS;
$this->registerJs($script, $this::POS_READY);
?>