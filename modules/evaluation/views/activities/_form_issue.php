<?php

use app\models\Activities;
use app\models\IssueType;
use app\models\Status;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

?>
<div class="issue-form">
    
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reporting_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Silahkan Pilih Tanggal Pelaporan'],
        'pluginOptions' => [
            'autoclose' => true,
            'todayHighlight' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'issue')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'issue_type_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(IssueType::find()->where(['is_active' => 1])->all(), 'id', 'issue_type'),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
                        'prompt' => '-- Pilih Jenis Kendala --',
                    ],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]); ?>

    <?= $form->field($model, 'issue_level')->widget(Select2::classname(), [
        'data' => Activities::ISSUE_LEVEL,
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
                        'prompt' => '-- Pilih Level Kendala --',
                    ],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]); ?>

    <?= $form->field($model, 'alternative_solution')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'resolution')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'due_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Silahkan Pilih Tenggat Waktu'],
        'pluginOptions' => [
            'autoclose' => true,
            'todayHighlight' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'status_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Status::find()->where(['scenario' => Status::SCENARIO_ISSUE])->all(), 'id', 'status'),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
                        // 'prompt' => '-- Pilih Status --',
                    ],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]); ?>

    <?php ActiveForm::end(); ?>
    
</div>