<?php

use app\models\Status;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="issue-list">
    <?= GridView::widget([
        'id'=>'issue-list',
        'dataProvider' => $dataProvider,
        'pjax'=>true,
        'export' => [
            'label' => 'Export Excel atau PDF',
            'header' => '',
            'options' => [
                'class' => 'btn btn-primary'
            ], 
            'menuOptions' => [ 
                    'style' => 'background: var(--background); width: 187px;'
            ]
        ],
        'exportConfig' => [
            GridView::PDF => true,
            GridView::EXCEL => true,
        ],
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'reporting_date',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'issue',
            ],
            // [
            //     'class'=>'\kartik\grid\DataColumn',
            //     'attribute'=>'issue_type_id',
            // ],
            // [
            //     'class'=>'\kartik\grid\DataColumn',
            //     'attribute'=>'issue_level',
            // ],
            // [
            //     'class'=>'\kartik\grid\DataColumn',
            //     'attribute'=>'alternative_solution',
            // ],
            // [
            //     'class'=>'\kartik\grid\DataColumn',
            //     'attribute'=>'resolution',
            // ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'due_date',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'status_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->status->getStatusname($model->status_id);
                }
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'vAlign'=>'middle',
                'urlCreator' => function($action, $model, $key, $index) { 
                        return Url::to([$action,'id'=>$key]);
                },   
                'template' => '{update}&nbsp&nbsp;{view}&nbsp&nbsp;{delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="fa fa-pencil"></span>',
                            ['update-issue', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], 
                            [
                                'title' => 'Ubah Kendala ini',
                                'role'=>'modal-remote',
                                'style' => 'color: var(--warning) !important'
                            ]
                        );
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<span class="fa fa-eye"></span>',
                            ['view-issue', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], 
                            [
                                'title' => 'Lihat Kendala ini',
                                'role'=>'modal-remote',
                                'style' => 'color: var(--primary) !important'
                            ]
                        );
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="fa fa-trash"></span>',
                            ['delete-issue', 'id' => $model->id, 'page' => Yii::$app->controller->action->id], 
                            [
                                'title' => 'Hapus Kendala ini',
                                'role'=>'modal-remote',
                                'data-confirm'=>false, 
                                'data-method'=>false,
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Konfirmasi',
                                'data-confirm-message'=>'Yakin ingin menghapus Kendala ini?',
                                'style' => 'color: var(--danger) !important'
                            ]
                        );
                    },
                ],
                'width' => '70px'
            ],
        ],
        'toolbar'=> false,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => false
    ])?>
</div>