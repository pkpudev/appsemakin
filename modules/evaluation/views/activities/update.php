<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Activities */
?>
<div class="activities-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelParent' => $modelParent,
        'isManager' => $isManager,
        'isGeneralManager' => $isGeneralManager,
    ]) ?>

</div>
