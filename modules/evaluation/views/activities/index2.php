<?php

use app\models\ActivityEvaluationValue;
use app\models\UnitStructure;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$this->title = 'Penilaian Kegiatan';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="review-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                // ['content'=>
                //     Html::a('<i class="fa fa-plus"></i> Tambah Review Kegiatan', ['create'],
                //     ['role'=>'modal-remote','title'=> 'Tambah Review Kegiatan','class'=>'btn btn-success'])
                // ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Daftar Penilaian Kegiatan',
                'before'=>
                            '<table><tr>' .
                                '<td width="150px" style="padding-right: 2px;">' .
                                    DatePicker::widget([
                                        'model' => $searchModel,
                                        'attribute' => 'year',
                                        'pjaxContainerId' => 'crud-datatable-pjax',
                                        'options' => [
                                            'id' => 'year-filter',
                                            'class' => 'form-control additional-filter',
                                            'placeholder'=>'-- Filter Tahun --',
                                        ],
                                        'pluginOptions' => [
                                            'autoclose'=>true,
                                            'startView'=>'year',
                                            'minViewMode'=>'years',
                                            'format' => 'yyyy',
                                            'allowClear' => false
                                        ]
                                    ]) .
                                '</td>' .
                                '<td width="300px">' . Select2::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'unit_structure_id',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'unit_structure_id-filter',
                                        'class' => 'form-control additional-filter',
                                        'placeholder'=>'-- Filter Unit --',
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                    'data' => ArrayHelper::map(UnitStructure::find()->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                                        return $model->unit_code . ' - ' . $model->unit_name;
                                    }),
                                ]) . '</td>'.
                            '</tr></table>'
                            ,
                'after'=>BulkButtonWidget::widget([
                    'buttons'=> ($isManager ? Html::a(
                        '<i class="fa fa-edit"></i>&nbsp; Pilih Penilaian',
                        ["bulk-review"],
                        [
                            "class" => "btn btn-info btn-xs",
                            'role' => 'modal-remote-bulk',
                            'data-confirm' => false, 'data-method' => false, // for overide yii data api
                            'data-request-method' => 'post',
                            'data-modal-size' => 'large',
                            'data-confirm-title' => 'Pilih Penilaian',
                            'data-confirm-message' =>  Html::dropDownList(
                                                                            'evaluation', 
                                                                            null, 
                                                                            ArrayHelper::map(
                                                                                    ActivityEvaluationValue::find()
                                                                                                ->orderBy('id')
                                                                                                ->all(), 
                                                                                                'evaluation', 'evaluation'), 
                                                                            ['class' => 'form-control']),
                        ]
                    ) : ''),
                ]).
                '<div class="clearfix"></div>'
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>