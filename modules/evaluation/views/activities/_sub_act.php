<?php

use app\models\Activities;
use yii\bootstrap\Progress;
use yii\helpers\Html;

?>

<?php
    $sub1Activities = Activities::find()->where(['parent_id' => $model->id])->orderBy('id')->all();

    if(!$sub1Activities) echo '<i>Tidak mempunyai sub kegiatan.</i>';
    foreach($sub1Activities as $sub1Activity):
?>
<div class="panel-group" id="sub-activity-<?= $sub1Activity->parent_id; ?>">
    <!-- SUB1.<?= $sub1Activity->id; ?> -->
    <div class="panel panel-default">
        <div class="panel-heading" style="border: 1px solid var(--background); margin: 5px 0; padding: 5px; border-radius: 5px; box-shadow: 3px 3px 5px rgba(0, 0, 0, 0.5); border-left: 5px solid var(--background);">
            <table width="100%">
                <tr>
                    <td>
                        <a data-toggle="collapse" data-parent="#sub-activity-<?= $sub1Activity->parent_id; ?>" href="#activity-<?= $sub1Activity->id; ?>" style="color: var(--background) !important; display: none;">
                            <?= $sub1Activity->activity; ?>
                        </a>
                        <?= Html::a(
                                        $sub1Activity->activity, 
                                        ['view', 'id' => $sub1Activity->id],
                                        [
                                            'role' => 'modal-remote',
                                            'style' => 'color: var(--background) !important;'
                                        ]
                                        ); ?>
                    </td>
                    <td width="50px">
                        <?= Activities::WEIGHT_TEXT[$sub1Activity->weight]; ?>
                    </td>
                    <td width="150px">
                        <?php 
                            // $totalWeight = Activities::find()->where(['parent_id' => $model->id])->sum('weight');
                            $totalWeight = $sub1Activity->weight;

                            if($sub1Activity->progress){
                                $progress = $sub1Activity->progress/100;
                                $progressText = round(($sub1Activity->weight*$progress), 2) . '/' . $totalWeight;
                            } else {
                                $progressText = '0/' . $totalWeight;

                            }
                        ?>
                        <?= Progress::widget(['percent' => $sub1Activity->progress, 'label' => $sub1Activity->progress.'%', 'options' => ['style' => 'margin-bottom: 0;']]); ?>
                    </td>
                    <td width="80px" align="center">
                        <?= $sub1Activity->duration; ?> Jam
                    </td>
                    <td width="120px" align="center">
                        <?= $sub1Activity->status->getStatusname($sub1Activity->status_id); ?>
                        <!-- <span class="label label-success" style="width: 120px !important; display: block;">Selesai</span> -->
                    </td>
                    <td width="130px" align="right">
                        <div class="btn-group">
                            <?= Html::a('<span class="fa fa-dot-circle-o"></span>', ['update-status', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-default btn-xs', 'title' => 'Ubah Progress', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                            <?= Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-warning btn-xs', 'title' => 'Ubah', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                            <?= Html::a('<span class="fa fa-info"></span>', ['view', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], ['class' => 'btn btn-info btn-xs', 'title' => 'Detail', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                            <?= Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $sub1Activity->id, 'page' => Yii::$app->controller->action->id], 
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Hapus Kegiatan ini',
                                    'data-confirm' => false,
                                    'data-method' => false,
                                    'data-request-method' => 'post',
                                    'data-confirm-title' => 'Konfirmasi',
                                    'data-confirm-message' => 'Yakin ingin menghapus Kegiatan ini?',
                                    'class' => 'btn btn-danger btn-xs'
                                ]); ?>
                            <?php // Html::a('<span class="fa fa-plus"></span>', ['create', 'parent_id' => $sub1Activity->id], ['class' => 'btn btn-success btn-xs', 'title' => 'Tambah', 'style' => 'width: 25px', 'role' => 'modal-remote']); ?>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="activity-<?= $sub1Activity->id; ?>" class="panel-collapse collapse">
            <div class="panel-body" style="border: 1px solid var(--background);">
                <b>Deskripsi Kegiatan: </b><br />
                <?= $sub1Activity->description; ?>
                <br /><br />
                <b>Sub Kegiatan: </b><br />
                <i>Tidak mempunyai sub kegiatan.</i>

            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>  