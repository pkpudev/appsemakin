<?php

use app\models\Evaluation;
use app\models\InitiativesEvaluation;
use app\models\OkrEvaluation;
use app\models\OkrLevel;
use app\models\Status;

$arr= [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'unit_code',
        'label'=>'Kode Unit',
        'value' => function($model){
            return $model->unit_code;
        },
        'group' => true,
        'width' => '80px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'unit_name',
        'label'=>'Nama Unit',
        'value' => function($model){
            return $model->unit_name;
        },
        'group' => true,
        'width' => '350px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'position_name',
        'label'=>'Jabatan',
        'group' => true,
        'value' => function($model){
            return $model->position_name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'employee_name',
        'label'=>'Nama Karyawan',
        'value' => function($model){
            return $model->full_name;
        }
    ],
    [
        'label' => 'Draf',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }
            
            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o'])
                                ->where(['oe.status_id' => Status::OKR_E_DRAFT])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere(['o.position_structure_id' => $model->position_structure_id])
                                ->andWhere(['oe.pic_id' => $model->pic_id])
                                ->count();
            
            return $count ?: 0;
        },
        'pageSummary' => true,
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'label' => '%',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }
            
            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o'])
                                ->where(['oe.status_id' => Status::OKR_E_DRAFT])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere(['o.position_structure_id' => $model->position_structure_id])
                                ->andWhere(['oe.pic_id' => $model->pic_id])
                                ->count();
            
            $totalCount = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o'])
                                ->where(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere(['o.position_structure_id' => $model->position_structure_id])
                                ->andWhere(['oe.pic_id' => $model->pic_id])
                                ->count();
            if($count){
                return round((($count/$totalCount)*100) ?: 0, 2);
            } else {
                return 0;
            }
        },
        'pageSummary' => function ($summary, $data, $widget) { 
            $average = array_sum($data)/count($data);
            return round($average, 2); 
        },
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'label' => 'Dikirim',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }
            
            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o'])
                                ->where(['oe.status_id' => Status::OKR_E_SENT])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere(['o.position_structure_id' => $model->position_structure_id])
                                ->andWhere(['oe.pic_id' => $model->pic_id])
                                ->count();
            
            return $count ?: 0;
        },
        'pageSummary' => true,
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'label' => '%',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }
            
            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o'])
                                ->where(['oe.status_id' => Status::OKR_E_SENT])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere(['o.position_structure_id' => $model->position_structure_id])
                                ->andWhere(['oe.pic_id' => $model->pic_id])
                                ->count();

            $totalCount = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o'])
                                ->where(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere(['o.position_structure_id' => $model->position_structure_id])
                                ->andWhere(['oe.pic_id' => $model->pic_id])
                                ->count();
            
            if($count){
                return round((($count/$totalCount)*100) ?: 0, 2);
            } else {
                return 0;
            }
        },
        'pageSummary' => function ($summary, $data, $widget) { 
            $average = array_sum($data)/count($data);
            return round($average, 2); 
        },
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'label' => 'Disetujui',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }
            
            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o'])
                                ->where(['oe.status_id' => Status::OKR_E_APPROVE])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere(['o.position_structure_id' => $model->position_structure_id])
                                ->andWhere(['oe.pic_id' => $model->pic_id])
                                ->count();
            
            return $count ?: 0;
        },
        'pageSummary' => true,
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'label' => '%',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }
            
            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o'])
                                ->where(['oe.status_id' => Status::OKR_E_APPROVE])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere(['o.position_structure_id' => $model->position_structure_id])
                                ->andWhere(['oe.pic_id' => $model->pic_id])
                                ->count();
            
            $totalCount = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o'])
                                ->where(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere(['o.okr_level_id' => OkrLevel::INDIVIDUAL])
                                ->andWhere(['o.type' => 'KR'])
                                ->andWhere(['o.position_structure_id' => $model->position_structure_id])
                                ->andWhere(['oe.pic_id' => $model->pic_id])
                                ->count();
            
            if($count){
                return round((($count/$totalCount)*100) ?: 0, 2);
            } else {
                return 0;
            }
        },
        'pageSummary' => function ($summary, $data, $widget) { 
            $average = array_sum($data)/count($data);
            return round($average, 2); 
        },
        'hAlign' => 'center',
        'width' => '120px'
    ],
];

return $arr;
