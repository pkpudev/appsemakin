<?php

use app\models\Evaluation;
use app\models\InitiativesEvaluation;
use app\models\OkrEvaluation;
use app\models\OkrLevel;
use app\models\Status;

$arr= [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_code',
        'label'=>'Kode Unit',
        'value' => function($model){
            return $model->unit_code;
        },
        'width' => '150px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_name',
        'label'=>'Nama Unit',
        'value' => function($model){
            return $model->unit_name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Draf',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }

            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o', 'okr.position p'])
                                ->where(['oe.status_id' => Status::OKR_E_DRAFT])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere('o.okr_level_id not in (' . OkrLevel::ORGANIZATION . ', ' . OkrLevel::INDIVIDUAL . ')')
                                ->andWhere(['p.unit_structure_id' => $model->unit_structure_id])
                                ->count();
            
            return $count ?: 0;
        },
        'pageSummary' => true,
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => '%',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }

            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o', 'okr.position p'])
                                ->where(['oe.status_id' => Status::OKR_E_DRAFT])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere('o.okr_level_id not in (' . OkrLevel::ORGANIZATION . ', ' . OkrLevel::INDIVIDUAL . ')')
                                ->andWhere(['p.unit_structure_id' => $model->unit_structure_id])
                                ->count();

            $totalCount = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o', 'okr.position p'])
                                ->where(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere('o.okr_level_id not in (' . OkrLevel::ORGANIZATION . ', ' . OkrLevel::INDIVIDUAL . ')')
                                ->andWhere(['p.unit_structure_id' => $model->unit_structure_id])
                                ->count();
            
            if($totalCount && $count){
                $hasil = (($count/$totalCount)*100);
            }
            
            return $hasil ?: 0;
        },
        'pageSummary' => function ($summary, $data, $widget) { 
            $average = array_sum($data)/count($data);
            return round($average, 2); 
        },
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Dikirim',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }

            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o', 'okr.position p'])
                                ->where(['oe.status_id' => Status::OKR_E_SENT])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere('o.okr_level_id not in (' . OkrLevel::ORGANIZATION . ', ' . OkrLevel::INDIVIDUAL . ')')
                                ->andWhere(['p.unit_structure_id' => $model->unit_structure_id])
                                ->count();
            
            return $count ?: 0;
        },
        'pageSummary' => true,
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => '%',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }

            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o', 'okr.position p'])
                                ->where(['oe.status_id' => Status::OKR_E_SENT])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere('o.okr_level_id not in (' . OkrLevel::ORGANIZATION . ', ' . OkrLevel::INDIVIDUAL . ')')
                                ->andWhere(['p.unit_structure_id' => $model->unit_structure_id])
                                ->count();

            $totalCount = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o', 'okr.position p'])
                                ->where(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere('o.okr_level_id not in (' . OkrLevel::ORGANIZATION . ', ' . OkrLevel::INDIVIDUAL . ')')
                                ->andWhere(['p.unit_structure_id' => $model->unit_structure_id])
                                ->count();
            
            if($totalCount && $count){
                $hasil = (($count/$totalCount)*100);
            }
            
            return $hasil ?: 0;
        },
        'pageSummary' => function ($summary, $data, $widget) { 
            $average = array_sum($data)/count($data);
            return round($average, 2); 
        },
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Disetujui',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }

            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o', 'okr.position p'])
                                ->where(['oe.status_id' => Status::OKR_E_APPROVE])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere('o.okr_level_id not in (' . OkrLevel::ORGANIZATION . ', ' . OkrLevel::INDIVIDUAL . ')')
                                ->andWhere(['p.unit_structure_id' => $model->unit_structure_id])
                                ->count();
            
            return $count ?: 0;
        },
        'pageSummary' => true,
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => '%',
        'value' => function($model){
            $id = Yii::$app->getRequest()->getQueryParam('id');
            
            if(!$id){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            }

            $count = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o', 'okr.position p'])
                                ->where(['oe.status_id' => Status::OKR_E_APPROVE])
                                ->andWhere(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere('o.okr_level_id not in (' . OkrLevel::ORGANIZATION . ', ' . OkrLevel::INDIVIDUAL . ')')
                                ->andWhere(['p.unit_structure_id' => $model->unit_structure_id])
                                ->count();

            $totalCount = OkrEvaluation::find()
                                ->alias('oe')
                                ->joinWith(['evaluation e', 'okr o', 'okr.position p'])
                                ->where(['oe.evaluation_id' => $evaluation->id])
                                ->andWhere('o.okr_level_id not in (' . OkrLevel::ORGANIZATION . ', ' . OkrLevel::INDIVIDUAL . ')')
                                ->andWhere(['p.unit_structure_id' => $model->unit_structure_id])
                                ->count();
                
            if($totalCount && $count){
                $hasil = (($count/$totalCount)*100);
            }
            
            return $hasil ?: 0;
        },
        'pageSummary' => function ($summary, $data, $widget) { 
            $average = array_sum($data)/count($data);
            return round($average, 2); 
        },
        'hAlign' => 'center',
        'width' => '120px'
    ],
];

return $arr;
