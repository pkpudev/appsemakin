<?php

use app\models\Evaluation;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

if($evaluation){
    $this->title = 'Progres Evaluasi Tahun ' . $evaluation->year . ' Periode ' . $evaluation->period;
} else {
    $this->title = 'Progres Evaluasi';
}
?>
<style>
    .box-dashboard {
        border: 1px solid var(--background);
        border-radius: 5px;
        padding: 5px; text-align: center; margin: 1px auto;
    }
</style>

<div class="monitoring">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-body">
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box-dashboard">
                                
                                <center><b style="color: var(--borderColor);font-size: 17px;">Filter</b></center>
                                <table width="100%">
                                    <tr>
                                        <td width="33%">
                                            <?= Select2::widget([
                                                    'name' => 'evaluation-filter',
                                                    'value' => $evaluation->id,
                                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                                    'options' => [
                                                        'id' => 'evaluation-filter',
                                                        'placeholder' => '-- Filter Periode --',
                                                        'onchange' => 'changeFilterPeriod($(this).val())',
                                                    ],
                                                    // 'pluginOptions' => ['allowClear' => true],
                                                    'data' => ArrayHelper::map(Evaluation::find()->orderBy('year asc, period asc')->all(), 'id', function($model){
                                                        return $model->year . ' - ' . $model->period;
                                                    })
                                                ])
                                            ?>
                                        </td>
                                        <td width="33%" style="padding-left: 5px;">
                                            <?= Select2::widget([
                                                    'name' => 'directorate-filter',
                                                    'value' => $directorateId,
                                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                                    'options' => [
                                                        'id' => 'directorate-filter',
                                                        'placeholder' => '-- Filter Directorate --',
                                                        'onchange' => 'changeFilterDirectorate($(this).val())',
                                                    ],
                                                    'pluginOptions' => ['allowClear' => true],
                                                    'data' => $listDirectorate,
                                                ])
                                            ?>
                                        </td>
                                        <td width="33%" style="padding-left: 5px;">
                                            <?= Select2::widget([
                                                    'name' => 'department-filter',
                                                    'value' => $departmentId,
                                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                                    'options' => [
                                                        'id' => 'department-filter',
                                                        'placeholder' => '-- Filter Department --',
                                                        'onchange' => 'changeFilterDepartment($(this).val())',
                                                    ],
                                                    'pluginOptions' => ['allowClear' => true],
                                                    'data' => $listDepartment,
                                                ])
                                            ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                    <!-- PROKER -->
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-12">
                            <div class="box-dashboard" style="text-align: left;">
                                <?= GridView::widget([
                                    'id'=>'proker',
                                    'dataProvider' => $dataProviderInitiatives,
                                    'filterModel' => $searchModelInitiatives,
                                    'showPageSummary' => true,
                                    'responsiveWrap' => false,
                                    'pjax'=>true,
                                    'hover' => true,
                                    'export' => [
                                        'label' => 'Export Excel atau PDF',
                                        'header' => '',
                                        'options' => [
                                            'class' => 'btn btn-primary'
                                        ], 
                                        'menuOptions' => [ 
                                                'style' => 'background: var(--background); width: 187px;'
                                        ]
                                    ],
                                    'exportConfig' => [
                                        GridView::PDF => true,
                                        GridView::EXCEL => true,
                                    ],
                                    'columns' => require(__DIR__.'/_columns-initiatives.php'),
                                    'toolbar'=> [
                                        ['content'=>
                                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                                            '{toggleData}'
                                        ],
                                        ['content'=>
                                            '{export}'
                                        ],
                                    ],
                                    'striped' => true,
                                    'condensed' => true,
                                    'responsive' => true,
                                    'panel' => [
                                        'type' => 'primary',
                                        'heading' => 'Program Kerja',
                                        'before'=>'',
                                        'after'=>'',
                                    ]
                                ])?>
                            </div>
                        </div>
                    </div>

                    <!-- OKR UNIT -->
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-12">
                            <div class="box-dashboard" style="text-align: left;">
                                <?= GridView::widget([
                                    'id'=>'okr-unit',
                                    'dataProvider' => $dataProviderOkrUnit,
                                    'filterModel' => $searchModelOkrUnit,
                                    'showPageSummary' => true,
                                    'responsiveWrap' => false,
                                    'pjax'=>true,
                                    'hover' => true,
                                    'export' => [
                                        'label' => 'Export Excel atau PDF',
                                        'header' => '',
                                        'options' => [
                                            'class' => 'btn btn-primary'
                                        ], 
                                        'menuOptions' => [ 
                                                'style' => 'background: var(--background); width: 187px;'
                                        ]
                                    ],
                                    'exportConfig' => [
                                        GridView::PDF => true,
                                        GridView::EXCEL => true,
                                    ],
                                    'columns' => require(__DIR__.'/_columns-okr-unit.php'),
                                    'toolbar'=> [
                                        ['content'=>
                                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                                            '{toggleData}'
                                        ],
                                        ['content'=>
                                            '{export}'
                                        ],
                                    ],
                                    'striped' => true,
                                    'condensed' => true,
                                    'responsive' => true,
                                    'panel' => [
                                        'type' => 'primary',
                                        'heading' => 'OKR Unit',
                                        'before'=>'',
                                        'after'=>'',
                                    ]
                                ])?>
                            </div>
                        </div>
                    </div>

                    <!-- OKR INDIVIDU -->
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-12">
                            <div class="box-dashboard" style="text-align: left;">
                                <?=  GridView::widget([
                                    'id'=>'okr-individual',
                                    'dataProvider' => $dataProviderOkrIndividual,
                                    'filterModel' => $searchModelOkrIndividual,
                                    'showPageSummary' => true,
                                    'responsiveWrap' => false,
                                    'pjax'=>true,
                                    'hover' => true,
                                    'export' => [
                                        'label' => 'Export Excel atau PDF',
                                        'header' => '',
                                        'options' => [
                                            'class' => 'btn btn-primary'
                                        ], 
                                        'menuOptions' => [ 
                                                'style' => 'background: var(--background); width: 187px;'
                                        ]
                                    ],
                                    'exportConfig' => [
                                        GridView::PDF => true,
                                        GridView::EXCEL => true,
                                    ],
                                    'columns' => require(__DIR__.'/_columns-okr-individual.php'),
                                    'toolbar'=> [
                                        ['content'=>
                                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                                            '{toggleData}'
                                        ],
                                        ['content'=>
                                            '{export}'
                                        ],
                                    ],
                                    'striped' => true,
                                    'condensed' => true,
                                    'responsive' => true,
                                    'panel' => [
                                        'type' => 'primary',
                                        'heading' => 'OKR Individu',
                                        'before'=>'',
                                        'after'=>'',
                                    ]
                                ])?>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    function changeFilterPeriod(val) {
        var period = val;

        window.location.replace("/evaluation/monitoring/index?id=" + val);
    }
    function changeFilterDirectorate(val) {

        window.location.replace("/evaluation/monitoring/index?directorateId=" + val);
    }
    function changeFilterDepartment(val) {

        window.location.replace("/evaluation/monitoring/index?departmentId=" + val);
    }
</script>