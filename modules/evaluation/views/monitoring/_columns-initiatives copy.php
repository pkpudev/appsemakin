<?php

use app\models\Evaluation;
use app\models\InitiativesEvaluation;
use app\models\Status;

$arr= [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
        'headerOptions' => ['class' => 'stick-relative zero-col'],
        'contentOptions' => ['class' => 'stick-relative zero-col']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_code',
        'headerOptions' => ['class' => 'stick-relative first-col'],
        'contentOptions' => ['class' => 'stick-relative first-col'],
        'width' => '150px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_name',
        // 'noWrap' => true,
        'headerOptions' => ['class' => 'stick-relative second-col'],
        'contentOptions' => ['class' => 'stick-relative second-col']
    ],
    [
        'label' => 'Draf',
        'value' => function($model){
            $year = Yii::$app->getRequest()->getQueryParam('year');
            $period = Yii::$app->getRequest()->getQueryParam('period');

            if(!$year) $year = date('Y');
            if(!$period){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            } else {
                $evaluation = Evaluation::findOne(['year' => $year, 'period' => $period]);
            }

            $countDraft = InitiativesEvaluation::find()
                                ->alias('ie')
                                ->joinWith(['evaluation e', 'initiatives i', 'initiatives.position p'])
                                ->where(['ie.status_id' => Status::OKR_E_DRAFT])
                                ->andWhere(['ie.evaluation_id' => $evaluation->id])
                                ->andWhere(['p.unit_structure_id' => $model->id])
                                ->count();
            
            return $countDraft ?: 0;
        },
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'label' => 'Dikirim',
        'value' => function($model){
            $year = Yii::$app->getRequest()->getQueryParam('year');
            $period = Yii::$app->getRequest()->getQueryParam('period');

            if(!$year) $year = date('Y');
            if(!$period){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            } else {
                $evaluation = Evaluation::findOne(['year' => $year, 'period' => $period]);
            }

            $countDraft = InitiativesEvaluation::find()
                                ->alias('ie')
                                ->joinWith(['evaluation e', 'initiatives i', 'initiatives.position p'])
                                ->where(['ie.status_id' => Status::OKR_E_SENT])
                                ->andWhere(['ie.evaluation_id' => $evaluation->id])
                                ->andWhere(['p.unit_structure_id' => $model->id])
                                ->count();
            
            return $countDraft ?: 0;
        },
        'hAlign' => 'center',
        'width' => '120px'
    ],
    [
        'label' => 'Disetujui',
        'value' => function($model){
            $year = Yii::$app->getRequest()->getQueryParam('year');
            $period = Yii::$app->getRequest()->getQueryParam('period');

            if(!$year) $year = date('Y');
            if(!$period){
                $evaluation = Evaluation::find()->orderBy('id desc')->one();
            } else {
                $evaluation = Evaluation::findOne(['year' => $year, 'period' => $period]);
            }

            $countDraft = InitiativesEvaluation::find()
                                ->alias('ie')
                                ->joinWith(['evaluation e', 'initiatives i', 'initiatives.position p'])
                                ->where(['ie.status_id' => Status::OKR_E_APPROVE])
                                ->andWhere(['ie.evaluation_id' => $evaluation->id])
                                ->andWhere(['p.unit_structure_id' => $model->id])
                                ->count();
            
            return $countDraft ?: 0;
        },
        'hAlign' => 'center',
        'width' => '120px'
    ],
];

return $arr;
