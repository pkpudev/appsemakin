<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-evaluation-form">

    <?php $form = ActiveForm::begin(); ?>

	<?php 
		$period = explode(' ', $model->evaluation->period); 

		$listVal = [];
		for($i = $period[1] ? $period[1]+1 : 1; $i<=4; $i++){
			array_push($listVal, ['id' => 'Kuartal ' . $i, 'value' => 'Kuartal ' . $i]);
		}
	?>

	<?= $form->field($model, 'postpone')->dropDownList(ArrayHelper::map($listVal, 'id', 'value'), ['class' => 'form-control', 'prompt' => '-- Pilih Salah Satu --'])->label('Ditunda Ke-') ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
