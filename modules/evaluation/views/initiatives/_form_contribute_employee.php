<?php

use app\models\Employee;
use app\models\PositionStructureEmployee;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluation */
/* @var $form yii\widgets\ActiveForm */

$model->employees_involved = null;
?>

<div class="activity-evaluation-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'employees_involved')->widget(Select2::classname(), [
		'data' => ArrayHelper::map(Employee::find()->where(['user_status' => 1])->andWhere(['is_employee' => 1])->all(), 'id', 'full_name'),
		'theme' => Select2::THEME_BOOTSTRAP,
		'options' => [
						'prompt' => '-- Pilih Karyawan --',
					],
		'pluginOptions' => [
			'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
			]
	]); ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
