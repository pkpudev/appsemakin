<?php

use app\models\Employee;
use kartik\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\InitiativesEvaluation */
?>
<div class="initiatives-evaluation-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'initiatives_id',
                'value' => function ($model) {
                    return ($model->initiatives) ? $model->initiatives->workplan_name : '-';
                }
            ],
            [
                'attribute'=>'target',
                'value' => function($model){
                    return $model->target ? $model->target . $model->initiatives->measure: 0;
                }
            ],
            [
                'attribute'=>'real',
                'value' => function($model){
                    return $model->real ? $model->real . '%': 0;
                }
            ],
            [
                'attribute' => 'employees_involved',
                'format' => 'raw',
                'filter' => false,
                'value' => function($model){
                    $employeeIds =  explode(", ",$model->employees_involved);
                    $text = '';
                    foreach($employeeIds as $employeeId){
                        $text .= Employee::findOne((int)$employeeId)->full_name . '<br />';
                    }

                    return $text;
                }
            ],
        ],
    ]) ?>

    <?= GridView::widget([
        'id'=>'crud-datatable2',
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'filterSelector' => '.additional-filter',
        'pjax'=>true,
        'export' => [
            'label' => 'Export Excel atau PDF',
            'header' => '',
            'options' => [
                'class' => 'btn btn-primary'
            ], 
            'menuOptions' => [ 
                    'style' => 'background: var(--background); width: 187px;'
            ]
        ],
        'exportConfig' => [
            GridView::PDF => true,
            GridView::EXCEL => true,
        ],
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'header' => 'No',
                'width' => '30px',
            ],
            [
                'attribute' => 'activity_name',
            ],
            [
                'attribute' => 'pic_id',
                'value' => function($model){
                    return $model->pic->full_name;
                }
            ],
            [
                'attribute' => 'weight',
                'value' => function($model){
                    return $model->weight . '%';
                }
            ],
            [
                'attribute' => 'actual_value',
                'value' => function($model){
                    return $model->actual_value ? $model->actual_value . '%': 0;
                }
            ],
        ],
        'toolbar'=> [
            ['content'=>
                '{export}'
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => false
    ]) ?>

</div>
