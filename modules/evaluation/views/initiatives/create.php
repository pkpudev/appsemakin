<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InitiativesEvaluation */

?>
<div class="initiatives-evaluation-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
