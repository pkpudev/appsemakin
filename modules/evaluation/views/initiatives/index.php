<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\select2\Select2;

$this->title = 'Evaluasi Program Kerja';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>
.blink {
    animation: blink 1s linear infinite;
}    
@keyframes blink{
0%{opacity: 0;}
50%{opacity: .5;}
100%{opacity: 1;}
}
</style>
<div class="cost-type-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'rowOptions' => function ($model, $index, $widget, $grid) {
                if ($model->postpone){
                    return ['class' => 'danger'];
                } else if ($model->postponed_from){
                    return ['class' => 'warning'];
                }
            },
            'showPageSummary' => true,
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    ($isManager ||$isGeneralManager ? Html::a('<i class="glyphicon glyphicon-send"></i> Kirim', ['send'],
                    ['role'=>'modal-remote', 'class'=>'btn btn-warning', 'title'=>'Kirim']) : '') .
                    ($isGeneralManager ? Html::a('<i class="fa fa-check"></i> Disetujui', ['approved'],
                    ['role'=>'modal-remote', 'class'=>'btn btn-success', 'title'=>'Setujui']) : '')
                ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Daftar Evaluasi Program Kerja',
                'before'=>'<table width="400px"><tr>' .
                            '<td width="50%">' .
                                DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'year',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'year-filter',
                                        'placeholder' => 'Filter Tahun',
                                        'class' => 'form-control additional-filter',
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'startView'=>'year',
                                        'minViewMode'=>'years',
                                        'format' => 'yyyy',
                                        'allowClear' => false
                                    ]
                                ]) .
                            '</td>' .
                            '<td style="padding-left: 5px;">' .
                                Select2::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'unit_id',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'divisi-filter',
                                        'class' => 'form-control additional-filter',
                                        'placeholder' => '-- Filter Unit Kerja --',
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                    'data' => $listUnit,
                                ]) .
                            '</td>' .
                            '</tr></table>',
                'after'=>'<b>Keterangan:</b><br/>
                            1. Background Merah: Program Kerja yang telah ditunda ke kuartal selanjutnya.<br/>
                            2. Background Kuning: Program kerja yang telah ditunda dari kuartal sebelumnya.',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>