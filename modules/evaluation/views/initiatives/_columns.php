<?php

use app\models\Employee;
use app\models\Evaluation;
use app\models\InitiativesMov;
use app\models\Status;
use app\models\VwEmployeeManager;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'header' => 'No',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'unit_id',
        'filter' => false,
        'format' => 'html',
        'value' => function ($model, $key, $index, $widget) {
            return '<b>' . $model->initiatives->okr->position->unit->unit_code . ' ' . $model->initiatives->okr->position->unit->unit_name . '</b>';
        },
        'group' => true,
        'groupedRow' => true,                    // move grouped column to a single grouped row
        'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'okr_code',
        'label' => 'Kode OKR',
        'value' => function($model){
            return $model->initiatives->okr->okr_code;
        },
        'group' => true,
        'subGroupOf' => 1
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'okr',
        'label' => 'Objectives',
        'value' => function($model){
            return $model->initiatives->okr->okr;
        },
        'group' => true,
        'subGroupOf' => 1
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'key_result',
        'label' => 'Key Results',
        'value' => function($model){
            return $model->initiatives->okr->getKeyResult($model->initiatives->okr_id);
        },
        'format' => 'raw',
        'group' => true,
        'subGroupOf' => 1
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Kode Program Kerja',
        'attribute'=>'initiatives_code',
        'value'=>function($model) {
            return ($model->initiatives) ? $model->initiatives->initiatives_code : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Program Kerja',
        'attribute' => 'workplan_name',
        'value' => function ($model) {
            return ($model->initiatives) ? $model->initiatives->workplan_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'evaluation_id',
        'label' => 'Waktu Evaluasi',
        'value' => function($model){
            return $model->evaluation->year . ' (' . $model->evaluation->period . ')';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(Evaluation::find()->all(), 'id', function($model){
            return $model->year . ' (' . $model->period . ')';
        }),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
        'width' => '150px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'weight',
        'value' => function($model){
            return $model->weight ? round($model->weight, 2): 0;
        },
        'pageSummary' => true
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'target',
    //     'value' => function($model){
    //         return $model->target ? $model->target . $model->initiatives->measure: 0;
    //     }
    // ],
    [
        'attribute' => 'target',
        'value' => function($model){
            return ($model->target ? number_format($model->target, 0, ',', '.') .  ($model->initiatives->okr->measure == '%' ? '':' ') . $model->initiatives->okr->measure: '');
            // if($model->evaluation->year == 2023){
            // } else {
            //     return round($model->target, 2) . ' %';
            // }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'real',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                    ($model->real? number_format($model->real, 0, ',', '.') . ($model->initiatives->okr->measure == '%' ? '':' ') . $model->initiatives->okr->measure :0) .
                                    // (
                                    //     $model->evaluation->year == 2023 ?
                                    //     ($model->real? number_format($model->real, 0, ',', '.') /* . '% ' */ . ($model->initiatives->okr->measure == '%' ? '':' ') . $model->initiatives->okr->measure :0) :
                                    //     round($model->real ?: 0, 2) . ' %'
                                    // ) .
                            '</td>
                            <td width="10px">' .
                                ($model->evaluation->use_old_pattern ?
                                Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['real', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Nilai Realisasi OKR',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'achieve_value',
        'format' => 'raw',
        'value' => function($model){
            return '<table width="100%">
                        <tr>
                            <td>' .
                                    (
                                        $model->evaluation->year == 2023 ?
                                        ($model->achieve_value? number_format($model->achieve_value, 0, ',', '.') /* . '% ' */ . ($model->initiatives->okr->measure == '%' ? '':' ') . $model->initiatives->okr->measure :0) :
                                        round($model->achieve_value ?: 0, 2) . ' %'
                                    ) .
                            '</td>
                            <td width="10px">' .
                                /* ($model->evaluation->isCanEdit() ?
                                Html::a('<span class="fa fa-pencil"><span/>', 
                                        ['real', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Ubah Nilai Realisasi OKR',
                                            'style' => 'color: var(--warning) !important'
                                        ]) : '') . */
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'documentation',
        'label' => 'Alat Verifikasi',
        'format' => 'raw',
        'value' => function($model){
            $count = InitiativesMov::find()->where(['initiatives_evaluation_id' => $model->id])->count();
            return Html::a($count . ' Dokumen', 
                            ['view-documentations', 'id' => $model->id], 
                            [
                                'role' => 'modal-remote',
                                'title' => 'Lihat Alat Verifikasi'
                            ]);
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'hAlign' => 'center'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'employees_involved',
        'format' => 'raw',
        'filter' => false,
        'value' => function($model){
            $employeeIds =  explode(", ",$model->employees_involved);
            $text = '';
            foreach($employeeIds as $employeeId){
                $text .= Employee::findOne((int)$employeeId)->full_name . '<br />';
            }


            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->employees_involved ? $text :'-') .
                            '</td>
                            <td width="10px">' .
                                ($model->evaluation->isCanEdit() ?
                                Html::a('<span class="fa fa-plus"><span/>', 
                                        ['contribute', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Tambah Karyawan Berkontribusi',
                                            'style' => 'color: var(--success) !important'
                                        ]) .
                                Html::a('<span class="fa fa-trash"><span/>', 
                                        ['reset-contribute', 'id' => $model->id], 
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Reset Karyawan Berkontribusi',
                                            'style' => 'color: var(--danger) !important',
                                            'data-confirm'=>false, 
                                            'data-method'=>false,
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Konfirmasi',
                                            'data-confirm-message'=>'Yakin ingin me-reset Karyawan Berkontribusi ini?',
                                        ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
        'width' => '150px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'note',
        'format' => 'raw',
        'value' => function($model){
            
            return '<table width="100%">
                        <tr>
                            <td>' .
                                ($model->note?:'-') .
                            '</td>
                            <td width="10px">' .
                                ($model->evaluation->isCanEdit() ?
                                Html::a('<span class="fa fa-pencil"><span/>', 
                                ['note', 'id' => $model->id], 
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Ubah Catatan Program Kerja',
                                    'style' => 'color: var(--warning) !important'
                                ]) : '') .
                            '</td>
                        </tr>
                    </table>';
        },
        'contentOptions' => ['style' => 'background: #eee;'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Status',
        'attribute' => 'status_id',
        'format' => 'raw',
        'width' => '60px',
        'value' => function ($model) {
            return ($model->status_id) ? $model->status->getStatusname($model->status_id) : '-';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(app\models\Status::find()->andWhere(['scenario' => Status::SCENARIO_OKR_EVALUATION])->orderBy('id')->asArray()->all(), 'id', 'status'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },   
        'template' => '{postpone}&nbsp&nbsp;{view}&nbsp&nbsp;{postponed}',
        'buttons' => [
            'postpone' => function ($url, $model) {
                $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one() || Yii::$app->user->can('Admin');
    
                if($isManager) return Html::a('<span class="fa fa-clock-o"></span>',
                    ['postpone', 'id' => $model->id], 
                    [
                        'title' => 'Tunda Program Kerja ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--danger) !important'
                    ]
                );
            },
            'view' => function ($url, $model) {
                return Html::a('<span class="fa fa-eye"></span>',
                    ['view', 'id' => $model->id], 
                    [
                        'title' => 'Lihat ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--primary) !important'
                    ]
                );
            },
            'postponed' => function ($url, $model) {
                if($model->postponed_from)
                return '<span class="fa fa-exclamation-triangle blink" title="Ditunda dari ' . $model->postponed_from . '" style="color: var(--danger)"></span>';
            },
        ],
        'width' => '70px'
    ],
];
