<?php

namespace app\modules\api2\controllers;

use Yii;
use yii\web\Controller;

/**
 * Mobile controller for the `api2` module
 */
class MobileController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public $enableCsrfValidation = false;
    protected $token = "1d948fc757105891bcd3c8f3814d2a93";

    /*** ACTIVITIES */
    public function actionGetActivities($offset=0, $limit=200){
        $request = Yii::$app->request;
        $tokenReq = $request->get('token');
        $userId = $request->get('uid');
        $response = [];

        if($this->token == $tokenReq){

            if($userId) $filterPic = " AND (a.pic_id = $userId)";
            
            $query = Yii::$app->db->createCommand("SELECT 
                                                    a.id, 
                                                    se.full_name, 
                                                    a.activity, 
                                                    ac.activity_category, 
                                                    ps.position_name, 
                                                    o.okr,
                                                    a.start_date,
                                                    a.due_date,
                                                    a.finish_date,
                                                    a.duration,
                                                    a.progress,
                                                    a.output,
                                                    s.status,
                                                    a.okr_value
                                                FROM 
                                                    management.activities a
                                                    LEFT JOIN sdm_employee se on a.pic_id = se.id
                                                    LEFT JOIN management.status s on a.status_id = s.id
                                                    LEFT JOIN management.activity_category ac on a.activity_category_id = ac.id
                                                    LEFT JOIN management.okr o on a.okr_id = o.id
                                                    LEFT JOIN management.position_structure ps on a.position_structure_id = ps.id
                                                    LEFT JOIN management.unit_structure us on a.supported_unit_id = us.id
                                                WHERE 
                                                    a.parent_id is null
                                                    $filterPic
                                                ORDER BY 
                                                    a.start_date DESC 
                                                OFFSET $offset
                                                LIMIT $limit")->queryAll();

            foreach($query as $q){
                $q = [
                        'id' => $q['id'], 
                        'full_name' => $q['full_name'], 
                        'activity' => $q['activity'], 
                        'activity_category' => $q['activity_category'], 
                        'position_name' => $q['position_name'], 
                        'okr' => $q['okr'],
                        'start_date' => $q['start_date'],
                        'due_date' => $q['due_date'],
                        'finish_date' => $q['finish_date'],
                        'duration' => $q['duration'],
                        'progress' => $q['progress'],
                        'output' => $q['output'],
                        'status' => $q['status'],
                        'okr_value' =>$q[ 'okr_value']
                ];
                array_push($response, $q);
            }

            $response = [
                "result" => $response
            ];
        } else {
            $response = ['status'=>'invalid token'];
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $response;
    }
}
