<?php

use app\models\Vision;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Management */

$this->title = "Lihat Manajemen";

CrudAsset::register($this);
?>
<div class="management-view">

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <div class="box box-default">
                <div class="box-header">
                    <label>Detail Manajemen</label>
                    <?= Html::a('Ubah Manajemen', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-xs pull-right', 'role' => 'modal-remote']); ?>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'management_name',
                            'period_start',
                            'period_end',
                            [
                                'attribute'=>'created_by',
                                'value' => function($model){
                                    return $model->created_by ? $model->createdBy0->full_name : '-';
                                }
                            ],
                            [
                                'attribute'=>'created_at',
                                'value' => function($model){
                                    return $model->created_at ?: '-';
                                }
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'updated_by',
                                'value' => function($model){
                                    return $model->updated_by ? $model->updatedBy0->full_name : '-';
                                }
                            ],
                            [
                                'attribute'=>'updated_at',
                                'value' => function($model){
                                    return $model->updated_at ?: '-';
                                }
                            ],
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header">
                    <label>Visi</label>
                    <?= Html::a($modelVision ? 'Tambah Penjabaran' : 'Tambah Visi', ['add-vision', 'id' => $model->id], ['class' => 'btn btn-warning btn-xs pull-right', 'role' => 'modal-remote']); ?>
                </div>
                <div class="box-body">
                    <?= $modelVision->vision ? "<h3><center>$modelVision->vision</center></h3>": ''; ?>
                    <?= GridView::widget([
                        'id' => 'vision',
                        'dataProvider' => $dataProviderVision,
                        'layout' => '{items}{pager}',
                        'pjax' => true,
                        'emptyText' => 'Tidak ada data Visi.',
                        'tableOptions' => [
                            'class' => 'table table-responsive table-striped table-bordered'
                        ],
                        'columns' =>  [
                            [
                                'class' => 'kartik\grid\SerialColumn'
                            ],
                            // [
                            //     'attribute' => 'vision',
                            //     'value' => function ($model) {
                            //         return ($model->vision) ?: '-';
                            //     },
                            //     'group' => true
                            // ],
                            [
                                'attribute' => 'description',
                                'value' => function ($model) {
                                    return ($model->description) ?: '-';
                                },
                            ],
                            [
                                'attribute' => 'indicator',
                                'value' => function ($model) {
                                    return ($model->indicator) ?: '-';
                                },
                            ],
                            [
                                'attribute' => 'explanation',
                                'value' => function ($model) {
                                    return ($model->explanation) ?: '-';
                                },
                            ],
                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'dropdown' => false,
                                'vAlign'=>'middle',
                                'urlCreator' => function($action, $model, $key, $index) { 
                                        return Url::to([$action,'id'=>$key]);
                                },   
                                'template' => '{edit}&nbsp;&nbsp;{delete}',
                                'buttons' => [
                                    'edit' => function ($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                                ['update-vision', 'id' => $model->id], 
                                                [
                                                    'title' => 'Ubah Visi ini',
                                                    'role' => 'modal-remote',
                                                    'style' => 'color: var(--warning) !important'
                                                ]
                                            );
                                    },
                                    'delete' => function ($url, $model) {
                                        return Html::a('<span class="fa fa-trash"></span>',
                                            ['delete-vision', 'id' => $model->id], 
                                            [
                                                'title' => 'Hapus Visi ini',
                                                'role'=>'modal-remote',
                                                'data-confirm'=>false, 
                                                'data-method'=>false,
                                                'data-request-method'=>'post',
                                                'data-confirm-title'=>'Konfirmasi',
                                                'data-confirm-message'=>'Yakin ingin menghapus Visi ini?',
                                                'style' => 'color: var(--danger) !important'
                                            ]
                                        );
                                    },
                                ],
                                'width' => '90px'
                            ],
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header">
                    <label>Misi</label>
                    <?php if($modelVision): ?>
                    <?= Html::a('Tambah Misi', ['add-mission', 'id' => $modelVision->id], ['class' => 'btn btn-warning btn-xs pull-right', 'role' => 'modal-remote']); ?>
                    <?php else: ?>
                       <div class="pull-right">Visi belum tersedia.</div>
                    <?php endif; ?>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                                'id' => 'mission',
                                'dataProvider' => $dataProviderMission,
                                'layout' => '{items}{pager}',
                                'pjax' => true,
                                'emptyText' => 'Tidak ada data Misi.',
                                'tableOptions' => [
                                    'class' => 'table table-responsive table-striped table-bordered'
                                ],
                                'columns' =>  [
                                    [
                                        'class' => 'kartik\grid\SerialColumn'
                                    ],
                                    [
                                        'attribute' => 'mission',
                                        'value' => function ($model) {
                                            return ($model->mission) ?: '-';
                                        },
                                    ],
                                    [
                                        'class'=>'\kartik\grid\DataColumn',
                                        'attribute'=>'created_by',
                                        'value' => function($model){
                                            return $model->created_by ? $model->createdBy0->full_name : '-';
                                        }
                                    ],
                                    [
                                        'class'=>'\kartik\grid\DataColumn',
                                        'attribute'=>'created_at',
                                        'value' => function($model){
                                            return $model->created_at ?: '-';
                                        },
                                        'filter' => false
                                    ],
                                    [
                                        'class'=>'\kartik\grid\DataColumn',
                                        'attribute'=>'updated_by',
                                        'value' => function($model){
                                            return $model->updated_by ? $model->updatedBy0->full_name : '-';
                                        }
                                    ],
                                    [
                                        'class'=>'\kartik\grid\DataColumn',
                                        'attribute'=>'updated_at',
                                        'value' => function($model){
                                            return $model->updated_at ?: '-';
                                        },
                                        'filter' => false
                                    ],
                                    [
                                        'class' => 'kartik\grid\ActionColumn',
                                        'dropdown' => false,
                                        'vAlign'=>'middle',
                                        'urlCreator' => function($action, $model, $key, $index) { 
                                                return Url::to([$action,'id'=>$key]);
                                        },   
                                        'template' => '{edit}&nbsp;&nbsp;{delete}',
                                        'buttons' => [
                                            'edit' => function ($url, $model) {
                                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                                        ['update-mission', 'id' => $model->id], 
                                                        [
                                                            'title' => 'Ubah Misi ini',
                                                            'role' => 'modal-remote',
                                                            'style' => 'color: var(--warning) !important'
                                                        ]
                                                    );
                                            },
                                            'delete' => function ($url, $model) {
                                                return Html::a('<span class="fa fa-trash"></span>',
                                                    ['delete-mission', 'id' => $model->id], 
                                                    [
                                                        'title' => 'Hapus Misi ini',
                                                        'role'=>'modal-remote',
                                                        'data-confirm'=>false, 
                                                        'data-method'=>false,
                                                        'data-request-method'=>'post',
                                                        'data-confirm-title'=>'Konfirmasi',
                                                        'data-confirm-message'=>'Yakin ingin menghapus Misi ini?',
                                                        'style' => 'color: var(--danger) !important'
                                                    ]
                                                );
                                            },
                                        ],
                                        'width' => '90px'
                                    ],
                                ],
                            ]) ?>
                </div>
            </div>

        </div>
        <div class="col-md-2"></div>
    </div>
 

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

