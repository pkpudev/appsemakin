<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Management */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="management-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'management_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'period_start')->textInput() ?>

    <?= $form->field($model, 'period_end')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
