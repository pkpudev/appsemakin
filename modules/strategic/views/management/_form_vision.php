<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Management */
/* @var $form yii\widgets\ActiveForm */

$model->vision = $visionText;
?>

<div class="management-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vision')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
    
    <?= $form->field($model, 'indicator')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'explanation')->textarea(['rows' => 3]) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
