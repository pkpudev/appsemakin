<?php
use yii\helpers\Html;
use \kartik\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Initiatives */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="initiatives-form">

    <?php $form = ActiveForm::begin([
        'id' => 'initiatives',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error'
    ]);?>

    <div class="box box-success">
        <?php echo $form->errorSummary($model); ?>
        <p>

            <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>
            <font color="blue">*catatan : Status RAB yang ada pada program kerja/IKU-IS tersebut akan berubah mengikuti proses approval ini.</font>
        </p>
    </div>
        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton('OK', ['class' => 'btn btn-success']) ?>
            </div>
        <?php } ?>
    <?php ActiveForm::end(); ?>
    
</div>
