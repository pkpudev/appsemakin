<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\select2\Select2;
use app\models\Departments;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BudgetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Approval Program Kerja';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>
.vertical {
  writing-mode: vertical-rl;
  line-height:2rem; 
  transform:rotate(0deg); 
  -moz-transform: rotate(-180deg);
}
</style>
<div class="budget-index">
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'showPageSummary' => true,
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns_approval.php'),
            'toolbar' => [
                [
                    'content' =>
                        Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']
                        ) .
                        '{toggleData}' .
                        Html::a(
                            '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                            '#',
                            ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Excel']
                        )
                        // '{export}'
                ],
            ],
            'striped' => true,
            'hover' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-money"></i> Rencana Kerja & Anggaran',
                'before' => (Yii::$app->user->can('BSC Admin')) ?
                    '<div class="row">' .
                        '<div class="col-md-2">' .
                            Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'year',
                                'pjaxContainerId' => 'crud-datatable-pjax',
                                'options' => [
                                    'id' => 'year-filter',
                                    'class' => 'form-control additional-filter',
                                    // 'placeholder'=>'-- Filter Tahun --',
                                ],
                                'pluginOptions' => ['allowClear' => false],
                                'data' => $yearItems,
                            ]) .
                        '</div>' .
                        '<div class="col-md-3">' .
                            Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'departments_id',
                                'pjaxContainerId' => 'crud-datatable-pjax',
                                'options' => [
                                    'id' => 'divisi-filter',
                                    'class' => 'form-control additional-filter',
                                    'placeholder' => '-- Filter Divisi --',
                                ],
                                'pluginOptions' => ['allowClear' => true],
                                'data' => ArrayHelper::map(
                                    Departments::find()
                                        ->where(['company_id' => Yii::$app->user->identity->company_id])
                                        ->andWhere(['not in', 'level', [0]])
                                        ->orderBy('deptname')
                                        ->all(),
                                    'deptid',
                                    'deptname'
                                ),
                            ]) .
                        '</div>' .
                    '</div>'
                    : '<div class="col-md-2">' .
                        Select2::widget([
                            // 'value' => $currentYear,
                            'model' => $searchModel,
                            'attribute' => 'year',
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'id' => 'year-filter',
                                'class' => 'form-control additional-filter',
                                // 'placeholder'=>'-- Filter Tahun --',
                            ],
                            'pluginOptions' => ['allowClear' => false],
                            'data' => $yearItems,
                        ]) .
                    '</div>',
                    'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-ok"></i>&nbsp; Approve All',
                                ["bulk-approve"] ,
                                [
                                    "class"=>"btn btn-success btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-modal-size'=>'large',
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Anda menyetujui item yang dipilih?',
                                    'data-confirm-message'=>Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:100%;'])
                                ]).' '.Html::a('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Revision All',
                                ["bulk-revision"] ,
                                [
                                    "class"=>"btn btn-warning btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-modal-size'=>'large',
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Anda akan meminta revisi item yang dipilih?',
                                    'data-confirm-message'=>Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:100%;'])
                                ]).' '.Html::a('<i class="glyphicon glyphicon-remove"></i>&nbsp; Reject All',
                                ["bulk-reject"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-modal-size'=>'large',
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Anda akan menolak item yang dipilih?',
                                    'data-confirm-message'=>Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:100%;'])
                                ]),
                        ]).
                        '<div class="clearfix"></div>',
            ]
        ]) ?>
    </div>
</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "", // always need it for jquery plugin
    "size" => Modal::SIZE_LARGE,
]) ?>
<?php Modal::end(); ?>
