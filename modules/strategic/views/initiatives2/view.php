<?php

use app\models\Budget;
use app\models\CostPriority;
use app\models\CostType;
use app\models\FinancingType;
use app\models\Okr;
use app\models\PositionStructureEmployee;
use app\models\Risk;
use app\models\RiskMitigation;
use app\models\SourceOfFunds;
use app\models\Status;
use app\models\VwEmployeeManager;
use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Initiatives */

$this->title = 'Program Kerja';

CrudAsset::register($this);

$checkPse = PositionStructureEmployee::find()->where(['position_structure_id' => $model->position_structure_id])->andWhere(['employee_id' => Yii::$app->user->id])->one();

?>

<style>
.vertical {
  writing-mode: vertical-rl;
  line-height:2rem; 
  transform:rotate(0deg); 
  -moz-transform: rotate(-180deg);
}
</style>

<div class="initiatives-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <legend>Rincian Program Kerja/IKU-IS</legend>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'initiatives_code',
                                    'workplan_name:ntext',
                                    'implementation',
                                    [
                                        'attribute' => 'total_budget',
                                        'format' => 'html',
                                        'value' => 'Rp. ' . number_format($model->total_budget, 2, ',', '.'),
                                    ],
                                    'initiatives:ntext',
                                    // [
                                    //     'attribute' => 'target',
                                    //     'value' => number_format($model->target, 0, ',', '.') . ' ' . $model->measure,
                                    // ],
                                    // [
                                    //     'attribute' => 'target_q1',
                                    //     'value' => number_format($model->target_q1, 0, ',', '.') . ' ' . $model->measure,
                                    // ],
                                    // [
                                    //     'attribute' => 'target_q2',
                                    //     'value' => number_format($model->target_q2, 0, ',', '.') . ' ' . $model->measure,
                                    // ],
                                    // [
                                    //     'attribute' => 'target_q3',
                                    //     'value' => number_format($model->target_q3, 0, ',', '.') . ' ' . $model->measure,
                                    // ],
                                    // [
                                    //     'attribute' => 'target_q4',
                                    //     'value' => number_format($model->target_q4, 0, ',', '.') . ' ' . $model->measure,
                                    // ],
                                    [
                                        'attribute' => 'participatory_budgeting_id',
                                        'value' => ($model->participatory) ? $model->participatory->participatory_budgeting:'',
                                    ],
                                    [
                                        'attribute' => 'position_structure_id',
                                        'value' => $model->position_structure_id ? $model->position->position_name :''
                                    ],
                                    [
                                        'attribute'=>'validity_id',
                                        'value'=>($model->validity) ? $model->validity->validity_type : '',
                                    ],
                                    [
                                        'attribute'=>'controllability_id',
                                        'value'=>($model->controllability) ? $model->controllability->degree_of_controllability : '',
                                    ],
                                    [
                                        'attribute'=>'polarization_id',
                                        'value'=>($model->polarization) ? $model->polarization->polarization_type : '',
                                    ],
                                    [
                                        'attribute'=>'calculation_id',
                                        'value'=>($model->calculation) ? $model->calculation->calculation_type : '',
                                    ],
                                    [
                                        'label' => 'Sasaran Pelanggan',
                                        'format' => 'html',
                                        'value' => $model->customerList,
                                    ],
                                    /* [
                                        'label' => 'Pemangku Kepentingan',
                                        'format' => 'html',
                                        'value' => $model->stakeholderList,
                                    ], */
                                    'monitoring_tools',
                                    // 'assumption:ntext',
                                    'note:ntext',
                                    'grade',
                                    [
                                        'attribute'=>'status_id',
                                        'format'=>'html',
                                        'value'=>$model->status->getStatusname($model->status_id),
                                    ],
                                ],
                            ]) ?>
                        </div>
                        <div class="col-md-6" style="display: none;">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <!-- Key Result -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h4 class="box-title">Indikator Keberhasilan</h4>
                    <?php if(in_array($model->status_id, [Status::DRAFT, Status::REVISION])): ?>
                        <?= Html::a('Tambah Indikator Keberhasilan', ['create-okr', 'initiatives_id' => $model->id], ['class' => 'btn btn-success btn-xs pull-right', 'role' => 'modal-remote']); ?>
                    <?php endif; ?>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'id' => 'okr',
                        'pjax' => true,
                        'layout' => '{pager}{items}{pager}',
                        'dataProvider' => new ActiveDataProvider(['query' => Okr::find()
                                                                                    // ->where(['reference_no' => $model->initiatives_code])
                                                                                    ->where(['ref_initiatives_id' => $model->id])
                                                                                    ->andWhere(['type' => 'KR'])
                                                                                    ->orderBy('okr_code'), 
                        'pagination' => ['pageSize' => 10, 'pageParam' => 'page-budgets']]),
                        'pager' => [
                            'class' => yii\widgets\LinkPager::className(),
                            'firstPageLabel' => 'First',
                            'lastPageLabel' => 'Last'
                        ],
                        'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                        'headerRowOptions' => ['class' => 'x'],
                        'showPageSummary' => true,
                        'columns' => [
                            [
                                'class' => 'kartik\grid\SerialColumn',
                                'width' => '30px',
                            ],
                            /* [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute' => 'parent_okr_code',
                                'label' => 'Objective Code',
                                'value' => function($model){
                                    return $model->parent->okr_code;
                                },
                                'width' => '120px',
                                'group' => true,
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute' => 'parent_okr',
                                'label' => 'Objective',
                                'value' => function($model){
                                    return $model->parent->okr;
                                },
                                'width' => '200px',
                                'group' => true,
                                'subGroupOf' => 1
                            ], */
                            [
                                'attribute' => 'okr_code',
                                'label' => 'KR Code',
                                'width' => '120px'
                            ],
                            [
                                'attribute' => 'okr',
                                'label' => 'Indikator Keberhasilan',
                                'width' => '250px',
                            ],
                            [
                                'label' => 'Action',
                                'value' => function($model){
                                    $isManager = VwEmployeeManager::find()->where(['upper_id' => Yii::$app->user->id])->andWhere(['unit_id' => $model->position->unit_structure_id])->andWhere(['is_active' => true])->one();
                                    $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one();
                                    $check1 = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['position_structure_id' => $model->id])->one();
                                    $check2 = Yii::$app->user->id == $model->created_by;
                                    
                                    return //Html::a('<span class="fa fa-plus-circle"></span>', ['create-support', 'parent_id' => $model->id], ['role' => 'modal-remote', 'title' => 'Create Support Cascading']) . '<br />' .
                                            Html::a('<span class="fa fa-eye"></span>', ['/strategic/okr/view', 'id' => $model->id, 'source' => 'initiatives'], ['role' => 'modal-remote', 'title' => 'View Key Result', 'style' => 'color: var(--linkColor) !important;']) .
                                            (($check1 || $check2 || $isManager || $isGeneralManager || Yii::$app->user->can('Admin')) ?
                                            Html::a('&nbsp;&nbsp;<span class="fa fa-pencil"></span>', ['update-okr', 'id' => $model->id], ['role' => 'modal-remote', 'title' => 'Update Key Result', 'style' => 'color: var(--warning) !important;'])
                                            : '-') .
                                            (Yii::$app->user->can('Admin') || Yii::$app->user->can('Leader') || $isManager  || $check2 ? 
                                            Html::a(
                                                '&nbsp;&nbsp;<span class="fa fa-trash"></span>', 
                                                ['delete-okr', 'id' => $model->id], 
                                                [
                                                    'role'=>'modal-remote','title'=>'Hapus Key Result Ini',
                                                    'data-confirm'=>false, 'data-method'=>false,
                                                    'data-request-method'=>'post',
                                                    'data-toggle'=>'tooltip',
                                                    'data-confirm-title'=>'Konfirmasi',
                                                    'data-confirm-message'=>'Yakin ingin menghapus Key Result ini?',
                                                    'style' => 'color: var(--danger) !important;'
                                                ]) : '');
                                },
                                'hAlign' => 'center',
                                'format' => 'raw',
                                'visible' => in_array($model->status_id, [Status::DRAFT, Status::REVISION]) || ($checkPse && $model->status_id == Status::SEND)
                            ], 
                            [
                                'attribute' => 'pic_id',
                                'label' => 'Ditugaskan Kepada',
                                'format' => 'raw',
                                'value' => function($model){
                                    return $model->pic_id ? $model->pic->full_name . '<br />(' . $model->position->position_name . ')' : '-';
                                },
                                /* 'filterType'=>GridView::FILTER_SELECT2,
                                'filter'=>ArrayHelper::map(Employee::find()->where(['is_employee' => 1])->andWhere(['company_id' => 1])->andWhere(['user_status' => 1])->andWhere(['employee_type_id' => [1,2,5]])->orderBy('full_name')->asArray()->all(), 'id', 'full_name'),
                                'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
                                'filterInputOptions'=>['placeholder'=>''], */
                                'width' => '150px'
                            ],
                            /* [
                                'label' => 'Bobot',
                                'attribute' => 'weight',
                                'value' => function($model){
                                    return ($model->weight ?: '');
                                },
                                'pageSummary' => true,
                            ], */
                            [
                                'attribute' => 'target',
                                'value' => function($model){
                                    return ($model->target ? number_format($model->target, 0, ',', '.') : '') . ' ' . $model->measure;
                                }
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'timebound_q1',
                                'value' => function($model){
                                    return $model->timebound_q1 ?: '-';
                                }
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'target_q1',
                                'value' => function($model){
                                    return ($model->target_q1 ? number_format($model->target_q1, 0, ',', '.') . ' ' . $model->measure: '-');
                                },
                                'hAlign' => 'center'
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'timebound_q2',
                                'value' => function($model){
                                    return $model->timebound_q2 ?: '-';
                                }
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'target_q2',
                                'value' => function($model){
                                    return ($model->target_q2 ? number_format($model->target_q2, 0, ',', '.') . ' ' . $model->measure: '-');
                                },
                                'hAlign' => 'center'
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'timebound_q3',
                                'value' => function($model){
                                    return $model->timebound_q3 ?: '-';
                                }
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'target_q3',
                                'value' => function($model){
                                    return ($model->target_q3 ? number_format($model->target_q3, 0, ',', '.') . ' ' . $model->measure: '-');
                                },
                                'hAlign' => 'center'
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'timebound_q4',
                                'value' => function($model){
                                    return $model->timebound_q4 ?: '-';
                                }
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'target_q4',
                                'value' => function($model){
                                    return ($model->target_q4 ? number_format($model->target_q4, 0, ',', '.') . ' ' . $model->measure: '-');
                                },
                                'hAlign' => 'center'
                            ],
                            /* [
                                'attribute' => 'cascading_type_id',
                                'value' => function($model){
                                    return $model->cascading_type_id ? $model->cascadingType->cascading_type: '-';
                                }
                            ], */
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Risk -->
    <div class="row" style="display: none;">
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h4 class="box-title">Analisis Risiko</h4>
                    <?php if(in_array($model->status_id, [Status::DRAFT, Status::REVISION])): ?>
                        <?= Html::a('Tambah Risiko', ['create-risk', 'initiatives_id' => $model->id], ['class' => 'btn btn-success btn-xs pull-right', 'role' => 'modal-remote']); ?>
                    <?php endif; ?>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'id' => 'risk',
                        'pjax' => true,
                        'layout' => '{pager}{items}{pager}',
                        'dataProvider' => new ActiveDataProvider(['query' => Risk::find()
                                                                                    ->where(['initiatives_id' => $model->id]),
                        'pagination' => ['pageSize' => 10, 'pageParam' => 'page-budgets']]),
                        'pager' => [
                            'class' => yii\widgets\LinkPager::className(),
                            'firstPageLabel' => 'First',
                            'lastPageLabel' => 'Last'
                        ],
                        'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                        'headerRowOptions' => ['class' => 'x'],
                        'showPageSummary' => true,
                        'columns' => [
                            [
                                'class' => 'kartik\grid\SerialColumn',
                                'width' => '30px',
                            ],
                            [
                                'attribute' => 'id'
                            ],
                            [
                                'attribute' => 'risk_criteria',
                                'format' => 'raw',
                                'width' => '110px',
                                'value' => function($model){
                                    if(strtolower($model->risk_criteria) == 'critical risk'){
                                        $color = '#ed1f24';
                                    } else if(strtolower($model->risk_criteria) == 'high risk'){
                                        $color = '#f8981d';
                                    } else if(strtolower($model->risk_criteria) == 'medium risk'){
                                        $color = '#f3ec19';
                                    } else if(strtolower($model->risk_criteria) == 'low risk'){
                                        $color = '#94ca52';
                                    }

                                    return "<span style='background: $color !important; width: 15px; height: 15px; display: inline-block; border-radius: 50%; margin-right: 5px; border: 1px solid;'></span>{$model->risk_criteria}";
                                }
                            ],
                            [
                                'attribute' => 'potential_failure'
                            ],
                            [
                                'attribute' => 'potential_causes_of_failure'
                            ],
                            [
                                'attribute' => 'severity',
                                'width' => '140px',
                                'hAlign' => 'center'
                            ],
                            [
                                'attribute' => 'occurance',
                                // 'label' => 'Tingkat Kemungkinan ',
                                'width' => '140px',
                                'hAlign' => 'center'
                            ],
                            /* [
                                'attribute' => 'potential_effect_of_failure'
                            ], */
                            [
                                'label' => 'Rencana Tindak Lanjut',
                                'format' => 'raw',
                                'value' => function($model){
                                    $mitigations = RiskMitigation::find()->where(['risk_id' => $model->id])->all();

                                    if($mitigations){
                                        $text = '';
                                        $i = 1;
                                        foreach($mitigations as $mitigation){
                                            $text .= "$i. {$mitigation->recomended_action}<br/ >";
                                            $i++;
                                        }
                                        return $text;
                                    } else {   
                                        return '<i>Belum ada rencana tindak lanjut</i>';
                                    }
                                }
                            ],
                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'dropdown' => false,
                                'vAlign' => 'middle',
                                'template' => '{view} {update} {delete}',
                                'width' => '80px',
                                'buttons' => [
                                    'view' => function ($url, $model) {
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-eye-open"></span>',
                                            ['view-risk', 'id' => $model->id],
                                            [
                                                'role' => 'modal-remote', 'title' => Yii::t('app', 'View'), 'data-toggle' => 'tooltip',
                                            ]
                                        );
                                    },
                                    'update' => function ($url, $model) {
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                            ['update-risk', 'id' => $model->id],
                                            [
                                                'role' => 'modal-remote', 'title' => Yii::t('app', 'Update'), 'data-toggle' => 'tooltip', 'style' => 'color: var(--warning) !important;'
                                            ]
                                        );
                                    },
                                    'delete' => function ($url, $model) {
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-trash"></span>',
                                            ['delete-risk', 'id' => $model->id],
                                            [
                                                'title' => Yii::t('app', 'Delete Permanent'),
                                                'data-confirm' => '' . 'Are you sure to delete this item?' . '',
                                                'data-method' => 'post',
                                                'style' => 'color: var(--danger) !important;'
                                            ]
                                        );
                                    },
                                ],
                                'visible' => in_array($model->status_id, [Status::DRAFT, Status::REVISION]) || ($checkPse && $model->status_id == Status::SEND)
                            ]
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Budget -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h4 class="box-title">Rencana Anggaran</h4>
                    <?php
                        $hasPosition = PositionStructureEmployee::find()
                                                                ->alias('pse')
                                                                ->joinWith('position p')
                                                                ->where(['employee_id' => Yii::$app->user->id, 'p.year' => $model->okr->year])
                                                                ->andWhere(['p.level' => 2])
                                                                ->one();
                        // var_dump($position);
                    ?>
                    <?php if(in_array($model->status_id, [Status::DRAFT, Status::REVISION])): ?>
                        <?= Html::a('Tambah Anggaran', ['/strategic/budget/create-all', 'id' => $model->id], ['class' => 'btn btn-success btn-xs pull-right']); ?>
                    <?php endif; ?>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <?= GridView::widget([
                            'id' => 'budget-datatable',
                            'pjax' => true,
                            'layout' => '{pager}{items}{pager}',
                            'dataProvider' => new ActiveDataProvider(['query' => Budget::find()->where(['initiatives_id' => $model->id]), 
                            'pagination' => ['pageSize' => 10, 'pageParam' => 'page-budgets']]),
                            'pager' => [
                                'class' => yii\widgets\LinkPager::className(),
                                'firstPageLabel' => 'First',
                                'lastPageLabel' => 'Last'
                            ],
                            'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                            'headerRowOptions' => ['class' => 'x'],
                            'showPageSummary' => true,
                            'columns' => [
                                [
                                    'class' => 'kartik\grid\CheckboxColumn',
                                    'width' => '20px',
                                ],
                                [
                                    'class' => 'kartik\grid\SerialColumn',
                                    'width' => '30px',
                                ],
                                'description:ntext',
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Akun Biaya',
                                    'attribute' => 'account_id',
                                    'value' => function ($model) {
                                        return $model->account_id ? $model->account->account_name : '-';
                                    },
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Sumber Dana',
                                    'attribute' => 'source_of_funds_id',
                                    'value' => function ($model) {
                                        return $model->source_of_funds_id ? $model->sourceOfFunds->source_of_funds: '-';
                                    },
                                    'filterType'=>GridView::FILTER_SELECT2,
                                    'filter'=>ArrayHelper::map(app\models\SourceOfFunds::find()->orderBy('id')->asArray()->all(), 'id', 'source_of_funds'), 
                                    'filterWidgetOptions'=>[
                                        'pluginOptions'=>['allowClear'=>true],
                                    ],
                                    'filterInputOptions'=>['placeholder'=>''],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Jenis Biaya',
                                    'attribute' => 'cost_type_id',
                                    'value' => function ($model) {
                                        return $model->cost_type_id ? $model->costType->cost_type : '-';
                                    },
                                    'filterType'=>GridView::FILTER_SELECT2,
                                    'filter'=>ArrayHelper::map(app\models\CostType::find()->orderBy('id')->asArray()->all(), 'id', 'cost_type'), 
                                    'filterWidgetOptions'=>[
                                        'pluginOptions'=>['allowClear'=>true],
                                    ],
                                    'filterInputOptions'=>['placeholder'=>''],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Prioritas Pembiayaan',
                                    'attribute' => 'cost_priority_id',
                                    'value' => function ($model) {
                                        return $model->cost_priority_id ? $model->costPriority->cost_priority : '-';
                                    },
                                    'filterType'=>GridView::FILTER_SELECT2,
                                    'filter'=>ArrayHelper::map(app\models\CostPriority::find()->orderBy('id')->asArray()->all(), 'id', 'cost_priority'), 
                                    'filterWidgetOptions'=>[
                                        'pluginOptions'=>['allowClear'=>true],
                                    ],
                                    'filterInputOptions'=>['placeholder'=>''],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Jenis Pembiayaan',
                                    'attribute' => 'financing_type_id',
                                    'value' => function ($model) {
                                        return $model->financing_type_id ? $model->financingType->financing_type: '-';
                                    },
                                    'filterType'=>GridView::FILTER_SELECT2,
                                    'filter'=>$listFinancingType, 
                                    'filterWidgetOptions'=>[
                                        'pluginOptions'=>['allowClear'=>true],
                                    ],
                                    'filterInputOptions'=>['placeholder'=>''],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'attribute' => 'volume',
                                    'value' => function ($model) {
                                        return $model->volume . ' ' . $model->measure;
                                    },

                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'attribute' => 'frequency',
                                    'label' => 'Frekuensi',
                                    'hAlign' => 'right',
                                    'width' => '50px',
                                    'value' => function($model){
                                        return $model->frequency . ' ' . $model->freq_measure;
                                    }
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Satuan',
                                    'attribute' => 'unit_amount',
                                    'hAlign' => 'right',
                                    'format' => ['decimal', 0],
                                    'pageSummary' => 'TOTAL',
                                    'pageSummaryOptions' => ['class' => 'text-right text-warning'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Total Anggaran',
                                    'attribute' => 'total_amount',
                                    'hAlign' => 'right',
                                    'format' => ['decimal', 0/*, [NumberFormatter::DECIMAL_ALWAYS_SHOWN=>false,NumberFormatter::GROUPING_USED=>true]*/],
                                    'value' => function ($model) {
                                        return $model->total_amount ?: 0;
                                    },
                                    'pageSummary' => true
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Jan',
                                    'attribute' => 'month_01',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_01 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Feb',
                                    'attribute' => 'month_02',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_02 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Mar',
                                    'attribute' => 'month_03',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_03 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Apr',
                                    'attribute' => 'month_04',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_04 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Mei',
                                    'attribute' => 'month_05',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_05 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Jun',
                                    'attribute' => 'month_06',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_06 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Jul',
                                    'attribute' => 'month_07',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_07 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Agu',
                                    'attribute' => 'month_08',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_08 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Sep',
                                    'attribute' => 'month_09',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_09 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Okt',
                                    'attribute' => 'month_10',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_10 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Nov',
                                    'attribute' => 'month_11',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_11 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'label' => 'Des',
                                    'attribute' => 'month_12',
                                    'hAlign' => 'right',
                                    'vAlign' => 'top',
                                    'width' => '30px',
                                    'contentOptions' => ['class' => 'vertical'],
                                    'format' => ['decimal', 0],
                                    'value' => function ($model) {
                                        return $model->month_12 ?: 0;
                                    },
                                    'pageSummary' => true,
                                    'pageSummaryOptions' => ['class' => 'vertical'],
                                ],
                                [
                                    'class' => 'kartik\grid\ActionColumn',
                                    'dropdown' => false,
                                    'vAlign' => 'middle',
                                    'urlCreator' => function ($action, $model, $key, $index) {
                                        return Url::to(['/strategic/budget/'.$action, 'id' => ($model->id) ?: $key]);
                                    },
                                    'buttons' => [
                                        'view' => function ($url, $model) {
                                            if($model->id){
                                                return Html::a(
                                                    '<span class="glyphicon glyphicon-eye-open"></span>',
                                                    $url, 
                                                    [
                                                        'role'=>'modal-remote','title' => Yii::t('app', 'View'),'data-toggle'=>'tooltip',
                                                    ]
                                                );
                                            }else
                                            return false;
                                        },
                                        'update' => function ($url, $model) {
                                            if ($model->initiatives->status_id == app\models\Status::DRAFT or $model->initiatives->status_id == app\models\Status::REVISION) {
                                                return Html::a(
                                                    '<span class="glyphicon glyphicon-pencil"></span>',
                                                    ['/strategic/budget/update2', 'id' => $model->id],
                                                    [
                                                        'role' => 'modal-remote', 'title' => Yii::t('app', 'Update'), 'data-toggle' => 'tooltip', 'style' => 'color: var(--warning) !important;'
                                                    ]
                                                );
                                            } else
                                                return false;
                                        },
                                        'delete' => function ($url, $model) {
                                            if (!in_array($model->initiatives->status_id, [Status::DRAFT, Status::REVISION])) {
                                                return false;
                                            } else {
                                                return Html::a(
                                                    '<span class="glyphicon glyphicon-trash"></span>',
                                                    ['/strategic/budget/delete', 'id' => $model->id, 'page' => Yii::$app->controller->id],
                                                    [
                                                        'title' => Yii::t('app', 'Delete'),
                                                        'data-confirm' => '' . 'Are you sure to delete this item?' . '',
                                                        'data-method' => 'post',
                                                        'style' => 'color: var(--danger) !important;'
                                                    ]
                                                );
                                            }
                                        },
                                    ]
                                ],
                            ],
                            'panel' => [
                                'after' => BulkButtonWidget::widget([
                                    'buttons' => Html::a(
                                        'Pilih Sumber Dana',
                                        ["/strategic/budget/bulk-source-of-fund"],
                                        [
                                            "class" => "btn btn-success btn-xs",
                                            'role' => 'modal-remote-bulk',
                                            'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                            'data-request-method' => 'post',
                                            'data-modal-size' => 'large',
                                            'data-confirm-title' => 'Pilih Sumber Dana',
                                            'data-confirm-message' =>  Html::dropDownList('source-of-fund', null, ArrayHelper::map(SourceOfFunds::find()->where(['is_active' => true])->all(), 'id', 'source_of_funds'), ['class' => 'form-control']),
                                        ]
                                    ) . '&nbsp;' .
                                    Html::a(
                                        'Pilih Jenis Biaya',
                                        ["/strategic/budget/bulk-cost-type"],
                                        [
                                            "class" => "btn btn-warning btn-xs",
                                            'role' => 'modal-remote-bulk',
                                            'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                            'data-request-method' => 'post',
                                            'data-modal-size' => 'large',
                                            'data-confirm-title' => 'Pilih Jenis Pembiayaan',
                                            'data-confirm-message' =>  Html::dropDownList('cost-type', null, ArrayHelper::map(CostType::find()->where(['is_active' => true])->all(), 'id', 'cost_type'), ['class' => 'form-control']),
                                        ]
                                    ) . '&nbsp;' .
                                    Html::a(
                                        'Pilih Prioritas Pembiayaan',
                                        ["/strategic/budget/bulk-cost-priority"],
                                        [
                                            "class" => "btn btn-info btn-xs",
                                            'role' => 'modal-remote-bulk',
                                            'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                            'data-request-method' => 'post',
                                            'data-modal-size' => 'large',
                                            'data-confirm-title' => 'Pilih Prioritas Pembiayaan',
                                            'data-confirm-message' =>  Html::dropDownList('cost-priority', null, ArrayHelper::map(CostPriority::find()->where(['is_active' => true])->all(), 'id', 'cost_priority'), ['class' => 'form-control']),
                                        ]
                                    ) . '&nbsp;' .
                                    Html::a(
                                        'Pilih Jenis Pembiayaan',
                                        ["/strategic/budget/bulk-financing-type"],
                                        [
                                            "class" => "btn btn-danger btn-xs",
                                            'role' => 'modal-remote-bulk',
                                            'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                            'data-request-method' => 'post',
                                            'data-modal-size' => 'large',
                                            'data-confirm-title' => 'Pilih Jenis Pembiayaan',
                                            'data-confirm-message' =>  Html::dropDownList('financing-type', null, ArrayHelper::map(FinancingType::find()->where(['is_active' => true])->all(), 'id', 'financing_type'), ['class' => 'form-control']),
                                        ]
                                    ),
                                ]) .
                                    '<div class="clearfix"></div>',
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- History -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h4 class="box-title">History</h4>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <?= GridView::widget([
                            'layout' => '{pager}{items}{pager}',
                            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getInitiativesHistories()->orderBy('stamp'), 'pagination' => ['pageSize' => 10, 'pageParam' => 'page-initiativeshistories']]),
                            'pager' => [
                                'class' => yii\widgets\LinkPager::className(),
                                'firstPageLabel' => 'First',
                                'lastPageLabel' => 'Last'
                            ],
                            'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                            'headerRowOptions' => ['class' => 'x'],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'width' => '175px',
                                    'attribute' => 'stamp',
                                    'value' => function ($model) {
                                        return ($model->stamp) ? date('d M Y H:i:s', strtotime($model->stamp)) : '-';
                                    },
                                    // 'format' => ['date', 'dd MM yyyy HH:mm:ss'],
                                ],
                                'action',
                                [
                                    'attribute' => 'by',
                                    'value' => 'by0.full_name'
                                ],
                                'comment:ntext',
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    // 'tabindex' => null,
    "footer" => "", // always need it for jquery plugin
    "size" => Modal::SIZE_LARGE,
    // 'options'=>[
    //     'tabindex'=>false,
    // ],
]) ?>
<?php Modal::end(); ?>