// define function/codes
var fnHasBudget = function() {
	if ( jQuery( "#ask-has-budget" ).prop( "checked" ) == true ) {
		jQuery( "#has-budget" ).show();
	} else {
		jQuery( "#has-budget" ).hide();
	}
};
var fnCheckMonEvent = function( event ) {
	fnCheckMon( this );
};
var fnCheckMon = function( elem ) {
	var checked = false;
	if ( jQuery( elem ).prop( "checked" ) == true ) {
		checked = true;
		jQuery( ".mon" ).each(function(){
			if ( jQuery( this ).prop( "checked" ) == false ) {
				checked = false;
			}
		});
	}
	jQuery( "#allmon" ).prop( "checked", checked );
};
var fnCheckAllMon = function() {
	if ( jQuery( "#allmon" ).prop( "checked" ) == true ) {
		jQuery( ".mon" ).prop( "checked", true );
	} else {
		jQuery( ".mon" ).prop( "checked", false );
	}
	fnCheckQuartalAll();
};
var fnCheckQuartal = function( className, runSumTarget ) {
	if (runSumTarget == undefined) {
		runSumTarget = true;
	}
	jQuery( "#t" + className ).attr( "disabled", true );
	
	jQuery( "." + className ).each(function(){
		if ( jQuery( this ).prop( "checked" ) == true ) {
			jQuery( "#t" + className ).attr( "disabled", false );
			return false;
		}
	});
	if (runSumTarget) {
		fnSumTarget();
	}
};
var fnCheckQuartalEvent = function ( event ) {
	fnCheckQuartal( event.data.className );
};
var fnCheckQuartalAll = function() {
	fnCheckQuartal( "q1", false );
	fnCheckQuartal( "q2", false );
	fnCheckQuartal( "q3", false );
	fnCheckQuartal( "q4", false );
	fnSumTarget();
};
var fnCheckInvolveAll = function() {
	if ( jQuery( "#involve-all" ).prop( "checked" ) == true ) {
		jQuery( "#involve-depts" ).prop( "disabled", true );
	} else {
		jQuery( "#involve-depts" ).prop( "disabled", false );
	}
};
var fnSumTarget = function() {
	var target = 0;
	var calculation = $('#initiatives-calculation_id').val();
	var countTarget = 0;
	var arrayTarget = [];
	
	jQuery( ".tq" ).each(function(){
		var $tq = jQuery( this );
		if ( $tq.prop( "disabled" ) != true ) {
			var val = $tq.val();
			if ( jQuery.isNumeric(val) ) {
				if(calculation == 1){
					target += parseFloat(val);
				} else if(calculation == 2){
					target += parseFloat(val);
				} else if(calculation == 3){
					target = parseFloat(val);
				} else if(calculation == 4){
					arrayTarget.push(parseFloat(val));
				} else {
					target = '';
				}
			}

			countTarget++;
		}
	});
	if(calculation == 1){
		target = target/countTarget;
	} else if(calculation == 4){
		target = Math.min.apply(Math, arrayTarget);
	}
	if (target == 0) {
		target = "";
	}
	jQuery( "#target" ).val( target );
};
var fnSumBudget = function() {
	var budget = 0;
	jQuery( ".budget-amount" ).each(function(){
		var $f = jQuery( this );
		if ( $f.prop("type") == "text" ) {
			var amount = $f.maskMoney('unmasked')[0];
			if ( jQuery.isNumeric(amount) ) {
				budget += parseFloat(amount);
			}
		}
	});
	jQuery( "#budget-total-disp" ).maskMoney( "mask", budget );
};
var fnBudgetNumberingRow = function() {
	jQuery(".source-item .no").each( function(index) {
        jQuery( this ).html( (index + 1) );
    });
};
var fnBudgetBeforeDeleteEvent = function( event, item ) {
	var no = jQuery( item ).find( '.no' ).text();
    if (! confirm("Anda yakin ingin menghapus alokasi anggaran no. " + no + " ?")) {
        return false;
    }
    return true;
};
var fnBudgetAfterDeleteEvent = function( event ) {
	fnBudgetNumberingRow();
	fnSumBudget();
};
var fnCheckKetDivLain = function() {
	var selected = jQuery("#initiatives-kpi_distribution_id").find(':selected');
	if (selected.length) {
		var id = selected.attr('value');
		if (vars.kpiDivisions[id] !== undefined ) {
			console.log(vars.kpiDivisions[id]);
			jQuery("#involve-depts").find("option:disabled").prop('disabled', false);
			jQuery("#involve-depts").find("option[value="+vars.kpiDivisions[id]+"]").prop('disabled', true);
			jQuery('#involve-depts').select2({"theme":"krajee","width":"100%","placeholder":"-- Pilih Divisi --","language":"id-ID"});
		} 
	}
};

// setup events
jQuery( "#ask-has-budget" ).on( "click", fnHasBudget );
jQuery( "#allmon" ).on( "click", fnCheckAllMon );
jQuery( ".mon" ).on( "click", fnCheckMonEvent );
jQuery( ".q1" ).on( "click", {className: "q1"}, fnCheckQuartalEvent );
jQuery( ".q2" ).on( "click", {className: "q2"}, fnCheckQuartalEvent );
jQuery( ".q3" ).on( "click", {className: "q3"}, fnCheckQuartalEvent );
jQuery( ".q4" ).on( "click", {className: "q4"}, fnCheckQuartalEvent );
jQuery( ".tq" ).on( "keyup", fnSumTarget ).on( "change", fnSumTarget );
jQuery( "#involve-all" ).on( "click", fnCheckInvolveAll );
jQuery( ".container-items" ).on( "keyup", ".budget-amount", fnSumBudget );
jQuery( ".dynamicform_wrapper" ).on( "afterInsert", fnBudgetNumberingRow );
jQuery( ".dynamicform_wrapper" ).on( "beforeDelete", fnBudgetBeforeDeleteEvent );
jQuery( ".dynamicform_wrapper" ).on( "afterDelete", fnBudgetAfterDeleteEvent );
jQuery( "#initiatives-calculation_id" ).on( "change", fnSumTarget );

// running fn/codes on startup!
fnHasBudget();
fnCheckQuartalAll();
fnCheckInvolveAll();
fnSumBudget();
fnCheckKetDivLain();