<?php

use app\models\Measure;
use app\models\RiskCategory;
use app\models\VwEmployeeManager;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

// var_dump($model->risk->initiatives->position->id);
?>
<div class="org-kr">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'recomended_action')->textarea(['rows' => 2]) ?>

    
    <?= $form->field($model, 'due_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Silahkan Pilih Tenggat Waktu'],
        'pluginOptions' => [
            'autoclose' => true,
            'todayHighlight' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'pic_id')->widget(Select2::classname(), [
        'initValueText' => $model->pic_id ? $model->pic->full_name : '',
        'data' => /* ArrayHelper::merge([Yii::$app->user->id => Yii::$app->user->identity->full_name], */ ArrayHelper::map(
                                    VwEmployeeManager::find()
                                                        ->alias('vw')
                                                        ->joinWith('employee e')
                                                        ->where(['vw.unit_id' => $model->risk->initiatives->position->unit_structure_id])
                                                        ->orderBy('e.full_name')
                                                        ->all(), 
                                                        'employee_id', function($model){
                                                            return $model->employee->full_name;
                                                        })/* ) */,
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
                        'prompt' => '-- Pilih Karyawan --',
                    ],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]);?>

    <?php ActiveForm::end(); ?>
    
</div>
