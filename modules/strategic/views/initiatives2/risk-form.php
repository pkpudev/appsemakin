<?php

use app\models\Measure;
use app\models\RiskCategory;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

?>
<div class="org-kr">

    <?php $form = ActiveForm::begin(); ?>
    
    <b>Program Kerja: </b><br />
    <?= $initiatives->workplan_name; ?><br /><br />
    
    <?= $form->field($model, 'risk_category_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(RiskCategory::find()->where(['is_active' => true])->asArray()->all(), 'id', 'risk_category'),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['prompt' => '-- Pilih Kategori Risiko --'],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]); ?>

    <div class="row" style="display: none;">
        <div class="col-md-4">
            <?php /*  $form->field($model, 'severity')->widget(Select2::classname(), [
                'data' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tingkat Keparahan --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]);  */?>
        </div>
        <div class="col-md-4">
            <?php /* $form->field($model, 'occurance')->widget(Select2::classname(), [
                'data' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tingkat Kejadian --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
                ]);  */?>
        </div>
    </div>

    <?= $form->field($model, 'potential_failure')->textarea(['rows' => 2]) ?>

    <!-- <label style="padding-left: 10px; color: var(--borderColor);">Keparahan</label> -->
    <!-- <div style="border: 1px solid #eee;padding: 10px;border-radius: 5px; margin-top: -15px;"> -->
    
        <?= $form->field($model, 'potential_causes_of_failure')->textarea(['rows' => 2]) ?>
    
        <?= $form->field($model, 'severity')->dropDownList([1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5/* , 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10 */], ['class' => 'form-control']) ?>
    <!-- </div> -->

    <!-- <label style="padding-left: 10px; color: var(--borderColor);">Kejadian</label> -->
    <!-- <div style="border: 1px solid #eee;padding: 10px;border-radius: 5px; margin-top: 5px;"> -->
        <?php // $form->field($model, 'potential_causes_of_failure')->textarea(['rows' => 2]) ?>

        <?php // $form->field($model, 'current_process_controls_prevention')->textarea(['rows' => 2]) ?>

        <?= $form->field($model, 'occurance')->dropDownList([1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5/* , 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10 */], ['class' => 'form-control']) ?>
    <!-- </div>     -->

    <?php // $form->field($model, 'rpn')->textInput() ?>

    <?php // $form->field($model, 'risk_criteria')->textarea(['rows' => 2]) ?>

    <?php ActiveForm::end(); ?>
    
</div>
