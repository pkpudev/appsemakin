<?php
use yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$columns = [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Kegiatan Kerja',
        'attribute' => 'activity_name',
        'group' => true,  // enable grouping
        'subGroupOf'=>1
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'start_plan',
        'value' => function($model){
            return ($model->start_plan) ? date('d-m-Y',strtotime($model->start_plan)):'-';
        },
        'group' => true,  // enable grouping
        'subGroupOf'=>1
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'finish_plan',
        'value' => function($model){
            return ($model->finish_plan) ? date('d-m-Y',strtotime($model->finish_plan)):'-';
        },
        'group' => true,  // enable grouping
        'subGroupOf'=>1
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'pic_position_name',
    //     'value'=>function($model){
    //         return $model->pic_position_name?:'';
    //     },
    //     'group' => true,  // enable grouping
    //     'subGroupOf'=>1
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'documentation',
        'group' => true,  // enable grouping
        'subGroupOf'=>1
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Akun Biaya',
        'attribute' => 'account_id',
        'value' => function ($model) {
            return $model->getAccount($model->account_id) ?: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\Account::find()->orderBy('id')->asArray()->all(), 'id', 'account_name'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Volume',
        'attribute' => 'volume',
        'hAlign' => 'right',
        'width' => '50px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Satuan',
        'attribute' => 'budget_measure',
        'width' => '75px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'frequency',
        'label' => 'Frekuensi',
        'hAlign' => 'right',
        'width' => '50px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Biaya',
        'attribute' => 'unit_amount',
        'hAlign' => 'right',
        'format' => ['decimal', 0],
        'pageSummary' => 'TOTAL',
        'pageSummaryOptions' => ['class' => 'text-right text-warning'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Total Anggaran',
        'attribute' => 'total_amount',
        'hAlign' => 'right',
        'format' => ['decimal', 0/*, [NumberFormatter::DECIMAL_ALWAYS_SHOWN=>false,NumberFormatter::GROUPING_USED=>true]*/],
        'value' => function ($model) {
            return $model->total_amount ?: 0;
        },
        'pageSummary' => true
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Sumber Dana',
        'attribute' => 'source_of_funds_id',
        'value' => function ($model) {
            return $model->getSourceOfFunds($model->source_of_funds_id) ?: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\SourceOfFunds::find()->orderBy('id')->asArray()->all(), 'id', 'source_of_funds'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Jenis Pembiayaan',
        'attribute' => 'cost_type_id',
        'value' => function ($model) {
            return $model->getCostType($model->cost_type_id) ?: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\CostType::find()->orderBy('id')->asArray()->all(), 'id', 'cost_type'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Prioritas Pembiayaan',
        'attribute' => 'cost_priority_id',
        'value' => function ($model) {
            return $model->getCostPriority($model->cost_priority_id) ?: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\CostPriority::find()->orderBy('id')->asArray()->all(), 'id', 'cost_priority'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Jan',
        'attribute' => 'month_01',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_01 ?: 0;
        },
        // 'rowOptions'=>['class'=>'vertical'],
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Feb',
        'attribute' => 'month_02',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_02 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Mar',
        'attribute' => 'month_03',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_03 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Apr',
        'attribute' => 'month_04',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_04 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Mei',
        'attribute' => 'month_05',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_05 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Jun',
        'attribute' => 'month_06',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_06 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Jul',
        'attribute' => 'month_07',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_07 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Agu',
        'attribute' => 'month_08',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_08 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Sep',
        'attribute' => 'month_09',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_09 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Okt',
        'attribute' => 'month_10',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_10 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Nov',
        'attribute' => 'month_11',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_11 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Des',
        'attribute' => 'month_12',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_12 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
]; ?>

<div class="row">
    <div class="col-md-12">
        <?=GridView::widget([
            'id'=>'detail-proker',
            'dataProvider' => $dataProvider,
            'pjax'=>true,
            'showPageSummary'=>true,
            'columns' => $columns,
            'toolbar'=> [],
            'bordered' => true,
            'striped' => true,
            'hover'=>true,
            'condensed' => true,
            'responsive' => true,     
            'responsiveWrap' => false,
            'panel' => [
                'type' => 'default', 
                'heading' => false,
                'before'=>false,
                // 'after'=>false,
                'footer'=>false,
            ]
        ])?>
    </div>
</div>