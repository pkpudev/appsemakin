<?php

use app\models\search\ActivitySearch;
use yii\helpers\Html;
use yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'header'=>'No',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'unit_id',
        'filter' => false,
        'format' => 'html',
        'value' => function ($model, $key, $index, $widget) {
            return '<b>' . $model->unit->unit_code . ' ' . $model->unit->unit_name . '</b>';
        },
        'group' => true,
        'groupedRow' => true,                    // move grouped column to a single grouped row
        'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
        /*'groupFooter' => function ($model) { // Closure method
            return [
                'mergeColumns' => [[2, 5]], // columns to merge in summary
                'content' => [             // content to show in each summary cell
                    2 => 'TOTAL (' . strtoupper($model->unit->unit_name) . ')',
                    6 => GridView::F_SUM,
                    9 => GridView::F_SUM,
                ],
                'contentFormats' => [      // content reformatting for each summary cell
                    6 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    9 => ['format' => 'number', 'decimals' => 0],
                ],
                'contentOptions' => [      // content html attributes for each summary cell
                    2 => ['style' => 'font-variant:small-caps;text-align:right'],
                    6 => ['style' => 'text-align:right'],
                ],
                // html attributes for group summary row
                'options' => ['class' => 'success', 'style' => 'font-weight:bold;']
            ];
        }*/
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'okr_code',
        'label' => 'Kode OKR',
        'value' => function($model){
            return $model->okr_code;
        },
        'group' => true,
        'subGroupOf' => 1
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'okr',
        'label' => 'Objectives',
        'value' => function($model){
            return $model->okr;
        },
        'group' => true,
        'subGroupOf' => 1
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'key_result',
        'label' => 'Key Results',
        'value' => function($model){
            return $model->getKeyResult($model->id_objective);
        },
        'format' => 'raw',
        'group' => true,
        'subGroupOf' => 1
    ],
    /* [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '50px',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail' => function ($model, $key, $index, $column) {
            $query = ActivitySearch::find()->alias('a')
                ->joinWith(['initiative i','budgets b'])
                ->select("a.*, b.measure as budget_measure, b.*")
                ->where(['a.initiatives_id'=>$model->id])
                ->orderBy(['a.start_plan'=>SORT_ASC,'a.id'=>SORT_ASC]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]);
            return Yii::$app->controller->renderPartial('_columns_collapse', [
                'dataProvider' => $dataProvider,
            ]);
        },
        'expandIcon' => '<span class="glyphicon glyphicon-expand"></span>',
        'collapseIcon' => '<span class="glyphicon glyphicon-collapse-down"></span>',
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true,
    ], */
    [
    	'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Kode Program Kerja',
        'attribute'=>'initiatives_code',
        'value'=>function($model) {
            return ($model->initiative) ? $model->initiative->initiatives_code : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Program Kerja',
        'attribute' => 'workplan_name',
        'value' => function ($model) {
            return ($model->initiative) ? $model->initiative->workplan_name : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Bobot',
        'attribute' => 'i_grade',
        'value' => function ($model) {
            return ($model->initiative->grade) ? $model->initiative->grade : '';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Alat Verifikasi',
        'attribute' => 'i_tools',
        'value' => function($model){
            return ($model->initiative) ? $model->initiative->monitoring_tools : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Periode Pelaksanaan',
        'attribute' => 'i_implementation',
        'value' => function($model){
            return ($model->initiative) ? $model->initiative->implementation : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Target',
        'attribute' => 'i_target',
        'value' => function($model){
            return ($model->initiative) ? $model->initiative->target.' '.$model->initiative->measure : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'K1',
        'attribute' => 'i_q1',
        'value' => function($model){
            return ($model->initiative->target_q1) ? $model->initiative->target_q1 : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'K2',
        'attribute' => 'i_q2',
        'value' => function($model){
            return ($model->initiative->target_q2) ? $model->initiative->target_q2 : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'K3',
        'attribute' => 'i_q3',
        'value' => function($model){
            return ($model->initiative->target_q3) ? $model->initiative->target_q3 : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'K4',
        'attribute' => 'i_q4',
        'value' => function($model){
            return ($model->initiative->target_q4) ? $model->initiative->target_q4 : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Catatan',
        'attribute' => 'i_note',
        'value' => function ($model) {
            return ($model->initiative->note) ? $model->initiative->note : '';
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template'=>'{view}',// {approve} {revision} {reject}
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-eye-open"></span>',
                    $url, 
                    [
                        'role'=>'modal-remote','title' => Yii::t('app', 'View'),'data-toggle'=>'tooltip',
                    ]
                );
            },
            'approve' => function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-ok"></span>',
                    $url, 
                    [
                        'role'=>'modal-remote','title' => Yii::t('app', 'Approve'),'data-toggle'=>'tooltip',
                    ]
                );
            },
            'revision' => function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-edit"></span>',
                    $url, 
                    [
                        'role'=>'modal-remote','title' => Yii::t('app', 'Revisi'),'data-toggle'=>'tooltip',
                    ]
                );
            },
            'reject' => function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-remove"></span>',
                    $url, 
                    [
                        'role'=>'modal-remote','title' => Yii::t('app', 'Reject'),'data-toggle'=>'tooltip',
                    ]
                );
            },
        ],
    ],

];
