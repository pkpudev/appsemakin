<?php

use app\models\Status;
use yii\helpers\Html;
use yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use kartik\grid\GridView;

$listParticipatories = ArrayHelper::map(app\models\ParticipatoryBudgeting::find()->orderBy('id')->all(), 'id', 'participatory_budgeting');
$listParticipatories = ArrayHelper::merge([999999 => '-- Belum Diisi --'], $listParticipatories);

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'header' => 'No',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'unit_id',
        'filter' => false,
        'format' => 'html',
        'value' => function ($model, $key, $index, $widget) {
            return '<b>' . $model->unit->unit_code . ' ' . $model->unit->unit_name . '</b>';
        },
        'group' => true,
        'groupedRow' => true,                    // move grouped column to a single grouped row
        'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
        /*'groupFooter' => function ($model) { // Closure method
            return [
                'mergeColumns' => [[2, 5]], // columns to merge in summary
                'content' => [             // content to show in each summary cell
                    2 => 'TOTAL (' . strtoupper($model->unit->unit_name) . ')',
                    6 => GridView::F_SUM,
                    9 => GridView::F_SUM,
                ],
                'contentFormats' => [      // content reformatting for each summary cell
                    6 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    9 => ['format' => 'number', 'decimals' => 0],
                ],
                'contentOptions' => [      // content html attributes for each summary cell
                    2 => ['style' => 'font-variant:small-caps;text-align:right'],
                    6 => ['style' => 'text-align:right'],
                ],
                // html attributes for group summary row
                'options' => ['class' => 'success', 'style' => 'font-weight:bold;']
            ];
        }*/
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'okr_code',
        'label' => 'Kode OKR',
        'value' => function($model){
            return $model->okr_code;
        },
        'group' => true,
        'subGroupOf' => 1
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'okr',
        'label' => 'OKR',
        'value' => function($model){
            return $model->okr;
        },
        'group' => true,
        'subGroupOf' => 1
    ],
    /* [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'key_result',
        'label' => 'Key Results',
        'value' => function($model){
            return $model->getKeyResult($model->id_objective);
        },
        'format' => 'raw',
        'group' => true,
        'subGroupOf' => 1
    ], */
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Kode Program Kerja',
        'attribute'=>'initiatives_code',
        'value'=>function($model) {
            return ($model->initiative) ? $model->initiative->initiatives_code : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Program Kerja',
        'attribute' => 'workplan_name',
        'value' => function ($model) {
            return ($model->initiative) ? $model->initiative->workplan_name : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Bobot',
        'attribute' => 'i_grade',
        'value' => function ($model) {
            // return ($model->initiative->grade) ? $model->initiative->grade : '';
            return ($model->unit_weight) ? $model->unit_weight : '';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Alat Verifikasi',
        'attribute' => 'i_tools',
        'value' => function($model){
            return ($model->initiative) ? $model->initiative->monitoring_tools : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Periode Pelaksanaan',
        'attribute' => 'i_implementation',
        'value' => function($model){
            return ($model->initiative) ? $model->initiative->implementation : '-';
        }
    ],
    /* [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Target',
        'attribute' => 'i_target',
        'value' => function($model){
            return ($model->initiative) ? $model->initiative->target.' '.$model->initiative->measure : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'K1',
        'attribute' => 'i_q1',
        'value' => function($model){
            return ($model->initiative->target_q1) ? $model->initiative->target_q1 : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'K2',
        'attribute' => 'i_q2',
        'value' => function($model){
            return ($model->initiative->target_q2) ? $model->initiative->target_q2 : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'K3',
        'attribute' => 'i_q3',
        'value' => function($model){
            return ($model->initiative->target_q3) ? $model->initiative->target_q3 : '-';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'K4',
        'attribute' => 'i_q4',
        'value' => function($model){
            return ($model->initiative->target_q4) ? $model->initiative->target_q4 : '-';
        }
    ], */
    [
    	'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
        'visible' => Yii::$app->user->can('Admin'),
        'checkboxOptions' => function ($model, $key, $index, $column) {
            return ['value' => $model->id, 'disabled' => !$model->initiative];
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Jenis Anggaran Partisipatori',
        'attribute' => 'participatory_budgeting_id',
        'value' => function ($model) {
            return ($model->initiative->participatory) ? $model->initiative->participatory->participatory_budgeting:'';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $listParticipatories,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Status',
        'attribute' => 'status_id',
        'format' => 'raw',
        'width' => '60px',
        'value' => function ($model) {
            return ($model->initiative) ? $model->initiative->status->getStatusname($model->initiative->status_id) : '-';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(Status::find()->where(['scenario' => Status::SCENARIO_INITIATIVES])->orderBy('id')->all(), 'id', 'status'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'pic_id',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{view} {update} {delete} {revisi}',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => ($model->initiative) ? $model->initiative->id : $key]);
        },
        'buttons' => [
            'view' => function ($url, $model) {
                if ($model->initiative) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        $url,
                        [
                            'data-pjax' => 0, 'title' => Yii::t('app', 'View'), 'data-toggle' => 'tooltip',
                        ]
                    );
                } else
                    return false;
            },
            'update' => function ($url, $model) {
                if($model->initiative and ($model->initiative->status_id == Status::DRAFT or $model->initiative->status_id == Status::REVISION or Yii::$app->user->can('Admin'))) {
                return Html::a(
                    '<span class="glyphicon glyphicon-pencil"></span>',
                    $url,
                    [
                        'role' => 'modal-remote', 'title' => Yii::t('app', 'Update'), 'data-toggle' => 'tooltip', 'style' => 'color: var(--warning) !important;'
                    ]
                );
                }else
                    return false;
            },
            'delete' => function ($url, $model) {
                if ((!$model->initiative) or (in_array($model->initiative->status_id, [Status::APPROVED, Status::SEND, Status::REJECTED]))) {
                    return false;
                } else {
                    return Html::a(
                        '<span class="glyphicon glyphicon-trash"></span>',
                        $url,
                        [
                            'title' => Yii::t('app', 'Delete Permanent'),
                            'data-confirm' => '' . 'Are you sure to delete this item?' . '',
                            'data-method' => 'post',
                            'style' => 'color: var(--danger) !important;'
                        ]
                    );
                }
            },
            'revisi' => function ($url, $model, $key) {
                if($model->initiative->status_id == \app\models\Status::APPROVED && Yii::$app->user->can('Admin')){
                    return Html::a('<span class="fa fa-pencil-square-o"></span>', ['revisi', 'id' => $model->id], [
                        'data-modal-size' => 'large',
                        'role' => 'modal-remote', 'title' => 'Ubah Proker jadi Revisi',
                        'data-confirm' => false, 'data-method' => false, // for overide yii data api
                        'data-request-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Anda yakin?',
                        'data-confirm-message' => 'Anda yakin akan mengubah status jadi Revisi pada Proker ' . $model->initiatives_code,
                        'style' => 'color: var(--warning) !important;'
                    ]);
                }
            },
        ],
    ],

];
