<?php

use app\models\RiskMitigation;
use kartik\grid\GridView;
use yii\bootstrap\Html as BootstrapHtml;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

?>
<div class="view-risk">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'risk_category_id',
                'value' => function($model){
                    return $model->category->risk_category; 
                }
            ],
            /* [
                'attribute' => 'requirements',
            ], */
            [
                'attribute' => 'potential_failure',
            ],
            [
                'attribute' => 'potential_causes_of_failure',
            ],
            [
                'attribute' => 'severity',
            ],
            // 'potential_causes_of_failure:ntext',
            // [
            //     'attribute' => 'current_process_controls_prevention',
            // ],
            [
                'attribute' => 'occurance',
            ],
            /* [
                'attribute' => 'current_process_controls_detection',
            ],
            [
                'attribute' => 'detection',
            ], */
            [
                'attribute' => 'rpn',
            ],
            [
                'attribute' => 'risk_criteria',
                'format' => 'raw',
                'value' => function($model){
                    if(strtolower($model->risk_criteria) == 'critical risk'){
                        $color = '#ed1f24';
                    } else if(strtolower($model->risk_criteria) == 'high risk'){
                        $color = '#f8981d';
                    } else if(strtolower($model->risk_criteria) == 'medium risk'){
                        $color = '#f3ec19';
                    } else if(strtolower($model->risk_criteria) == 'low risk'){
                        $color = '#94ca52';
                    }

                    return "<span style='background: $color !important; width: 15px; height: 15px; display: inline-block; border-radius: 50%; margin-right: 5px; border: 1px solid;'></span>{$model->risk_criteria}";
                }
            ],
        ],
    ]) ?>

    <label>Rencana Tindak Lanjut</label>
    <?= Html::a('Tambah Rencana Tindak Lanjut', ['create-risk-mitigation', 'id' => $model->id], ['class' => 'btn btn-success btn-xs pull-right', 'role' => 'modal-remote']); ?>
    <?= GridView::widget([
        'layout' => '{pager}{items}{pager}',
        'dataProvider' => new ActiveDataProvider(['query' => RiskMitigation::find()
                                                                    ->where(['risk_id' => $model->id]),
        'pagination' => ['pageSize' => 10, 'pageParam' => 'page-budgets']]),
        'pager' => [
            'class' => yii\widgets\LinkPager::className(),
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
        'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
        'headerRowOptions' => ['class' => 'x'],
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            [
                'attribute' => 'pic_id',
                'value' => function($model){
                    return $model->pic->full_name;
                }
            ],
            [
                'attribute' => 'recomended_action'
            ],
            [
                'attribute' => 'due_date'
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'vAlign' => 'middle',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>',
                            ['update-risk-mitigation', 'id' => $model->id],
                            [
                                'role' => 'modal-remote', 'title' => Yii::t('app', 'Update'), 'data-toggle' => 'tooltip', 'style' => 'color: var(--warning) !important;'
                            ]
                        );
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            ['delete-risk-mitigation', 'id' => $model->id],
                            [
                                'role' => 'modal-remote', 'title' => Yii::t('app', 'Delete'), 'data-toggle' => 'tooltip', 'style' => 'color: var(--danger) !important;'
                            ]
                        );
                    },
                ],
            ]
        ],
    ]); ?>
</div>