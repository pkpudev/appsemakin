<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
// use app\components\widgets\Modal;
// use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\select2\Select2;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\Departments;
use app\models\UnitStructure;
use kartik\widgets\DatePicker;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\InitiativesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Approval Rencana Program Kerja (RPK)';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="initiatives-index">
    
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns-approval.php'),
            'toolbar' => [
                [
                    'content' => 
                        // '&nbsp;<em>* Klik kirim untuk proses Approval oleh BOD.</em>'.
                        Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']
                        ) .
                        '{toggleData}' .
                        Html::a(
                            '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                            '#',
                            ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Download Excel']
                        )
                ],
            ],
            'striped' => true,
            'hover' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-tasks"></i> Daftar Rencana Program Kerja',
                'before' => /* (Yii::$app->user->can('Admin') or Yii::$app->user->can('BOD')) ? */
                    '<div class="row">' .
                    '<div class="col-md-2">' .
                        DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'year',
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'id' => 'year-filter',
                                'placeholder' => 'Filter Tahun',
                                'class' => 'form-control additional-filter',
                            ],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'startView'=>'year',
                                'minViewMode'=>'years',
                                'format' => 'yyyy',
                                'allowClear' => false
                            ]
                        ]) .
                    '</div>' .
                    '<div class="col-md-3">' .
                        Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'unit_id',
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'id' => 'divisi-filter',
                                'class' => 'form-control additional-filter',
                                'placeholder' => '-- Filter Unit Kerja --',
                            ],
                            'pluginOptions' => ['allowClear' => true],
                            'data' => $listUnit,
                        ]) .
                    '</div>' .
                    '</div>'
                    /* : '<div class="col-md-2">' .
                        DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'year',
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'placeholder' => 'Filter Tahun',
                                'class' => 'form-control additional-filter',
                            ],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'startView'=>'year',
                                'minViewMode'=>'years',
                                'format' => 'yyyy',
                                'allowClear' => false
                            ]
                        ]) .
                    '</div>' */,
                'after'=>BulkButtonWidget::widget([
                    'buttons'=>Html::a('<i class="glyphicon glyphicon-ok"></i>&nbsp; Approve All',
                        ["bulk-approve"] ,
                        [
                            "class"=>"btn btn-success btn-xs",
                            'role'=>'modal-remote-bulk',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-modal-size'=>'large',
                            'data-request-method'=>'post',
                            'data-confirm-title'=>'Anda menyetujui item yang dipilih?',
                            'data-confirm-message'=>Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:100%;'])
                        ]).' '.Html::a('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Revision All',
                        ["bulk-revision"] ,
                        [
                            "class"=>"btn btn-warning btn-xs",
                            'role'=>'modal-remote-bulk',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-modal-size'=>'large',
                            'data-request-method'=>'post',
                            'data-confirm-title'=>'Anda akan meminta revisi item yang dipilih?',
                            'data-confirm-message'=>Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:100%;'])
                        ]).' '.Html::a('<i class="glyphicon glyphicon-remove"></i>&nbsp; Reject All',
                        ["bulk-reject"] ,
                        [
                            "class"=>"btn btn-danger btn-xs",
                            'role'=>'modal-remote-bulk',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-modal-size'=>'large',
                            'data-request-method'=>'post',
                            'data-confirm-title'=>'Anda akan menolak item yang dipilih?',
                            'data-confirm-message'=>Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:100%;'])
                        ]),
                ]).
                '<div class="clearfix"></div>',
            ]
        ]) ?>
    </div>
</div>

<?= Html::beginForm(['download'], 'post', ['id' => 'download-form']) ?>
<div></div>
<?= Html::endForm() ?>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    // 'tabindex' => null,
    "footer" => "", // always need it for jquery plugin
    "size" => Modal::SIZE_LARGE,
    // 'options'=>[
    //     'tabindex'=>false,
    // ],
]) ?>
<?php Modal::end(); ?>

<?php
    $jsReady = file_get_contents(__DIR__ . '/js/index.ready.js');
    $this->registerJs($jsReady, View::POS_READY);
?>