<?php

use app\models\Calculation;
use app\models\Controllability;
use app\models\Employee;
use app\models\Measure;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\OkrOrg;
use app\models\Polarization;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\Validity;
use app\models\VwEmployeeManager;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

    $unitdIds = [];
    $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['is_active' => true])->all();
    foreach($vwEmployeeManagers as $vwEmployeeManager){
        array_push($unitdIds, $vwEmployeeManager->unit_id);
    }

    $okrList = ArrayHelper::map(Okr::find()
                                    ->alias('o')
                                    ->joinWith('position p')
                                    ->andWhere(['in', 'p.unit_structure_id', $unitdIds])
                                    ->andWhere(['okr_level_id' => OkrLevel::INDIVIDUAL])->orderBy('okr_code asc')->all(), 'id', function($model){
        return $model->okr_code . ' - ' . $model->okr . ' (' . $model->pic->full_name . ')';
    });
    
$picList = ArrayHelper::map(PositionStructureEmployee::find()
                                                        ->alias('pse')
                                                        ->joinWith('position p')
                                                        ->where(['unit_structure_id' => $initiatives->position->unit_structure_id])->all(), 
                                                        'employee_id', function($model){
                                                            return $model->employee->full_name;
                                                        });
?>
<div class="org-kr">

    <?php $form = ActiveForm::begin([
        'id' => 'indicator',
        'type' => ActiveForm::TYPE_VERTICAL,
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error'
    ]); ?>

    <b>Program Kerja: </b><br />
    <?= $model->isNewRecord ? $initiatives->workplan_name : $modelParent->okr; ?><br /><br />

    <?= $form->field($model, 'okr')->textarea(['rows' => 2])->label('Indikator Keberhasilan') ?>

    <div class="row">
        <div class="col-md-4">
            <label>Polarization</label>
            <?= Html::activeDropDownList(
                    $model, 
                    'polarization_id',
                    ArrayHelper::map(Polarization::find()->where(['is_active' => true])->asArray()->all(), 'id', 'polarization_type'), 
                    [
                        'label' => 'Polarization',
                        'prompt' => '-- Pilih Polarization --',
                        'class' => 'form-control'
                    ]); ?>
            <?php /* $form->field($model, 'polarization_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Polarization::find()->where(['is_active' => true])->asArray()->all(), 'id', 'polarization_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Polarization --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); */ ?>
        </div>
        <div class="col-md-4">
            <label>Calculation</label>
            <?= Html::activeDropDownList(
                $model, 
                'calculation_id',
                ArrayHelper::map(Calculation::find()->where(['is_active' => true])->asArray()->all(), 'id', 'calculation_type'),
                [
                    'label' => 'Calculation',
                    'prompt' => '-- Pilih Calculation --',
                    'class' => 'form-control'
                ]); ?>
            <?php /* $form->field($model, 'calculation_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Calculation::find()->where(['is_active' => true])->asArray()->all(), 'id', 'calculation_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Calculation --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); */ ?>
        </div>
        <div class="col-md-4">
            <label>Nilai Validitas</label>
            <?= Html::activeDropDownList(
                $model, 
                'validity_id',
                ArrayHelper::map(Validity::find()->where(['is_active' => true])->asArray()->all(), 'id', 'validity_type'),
                [
                    'label' => 'Calculation',
                    'prompt' => '-- Pilih Nilai Validitas --',
                    'class' => 'form-control'
                ]); ?>
            <?php /* $form->field($model, 'validity_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Validity::find()->where(['is_active' => true])->asArray()->all(), 'id', 'validity_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Validitas --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); */ ?>
        </div>
    </div>

    <hr style="margin: 5px 0;" />

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'target')->textInput(['placeholder' => 'Hanya Angka & Titik'/* 'onkeyup' => "if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" */]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'measure')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Measure::find()->where(['is_active' => true])->asArray()->orderBy('measure')->all(), 'measure', 'measure'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Satuan --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label>Tingkat Pengendalian</label>
            <?= Html::activeDropDownList(
                $model, 
                'controllability_id',
                ArrayHelper::map(Controllability::find()->where(['is_active' => true])->asArray()->all(), 'id', 'degree_of_controllability'),
                [
                    'label' => 'Tingkat Pengendalian',
                    'prompt' => '-- Pilih Tingkat Pengendalian --',
                    'class' => 'form-control'
                ]); ?>
            <?php /* $form->field($model, 'controllability_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Controllability::find()->where(['is_active' => true])->asArray()->all(), 'id', 'degree_of_controllability'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tingkat Pengendalian --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); */ ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'position_structure_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(PositionStructure::find()->where(['unit_structure_id' => $initiatives->position->unit_structure_id])->all(), 'id', function($model){
                    return $model->position_name . ' (' . ($model->level0->level_name ?: 'Staf'). ')'; 
                }),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Jabatan --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ])->label('Jabatan'); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'pic_id')->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => [
                                'placeholder' => '-- Pilih PIC --'
                            ],
                'select2Options' => [
                    'initValueText' => $model->pic_id ? $model->pic->full_name : '',
                    'pluginOptions' => [
                                            'allowClear' => true,
                                            'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                        ]
                ],
                'pluginOptions' => [
                    'depends' => ['okr-position_structure_id'],
                    'url' => Url::to(['/leadership/structure/get-persons']),
                ]
            ]); ?>
        </div>
    </div>

    <hr style="margin: 5px 0;" />

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'target_q1')->textInput(['class' => 'tq form-control', 'disabled' => !$initiatives->q1, 'placeholder' => 'Hanya Angka & Titik'/* 'onkeyup' => "if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" */]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q2')->textInput(['class' => 'tq form-control', 'disabled' => !$initiatives->q2, 'placeholder' => 'Hanya Angka & Titik'/* 'onkeyup' => "if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" */]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q3')->textInput(['class' => 'tq form-control', 'disabled' => !$initiatives->q3, 'placeholder' => 'Hanya Angka & Titik'/* 'onkeyup' => "if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" */]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q4')->textInput(['class' => 'tq form-control', 'disabled' => !$initiatives->q4, 'placeholder' => 'Hanya Angka & Titik'/* 'onkeyup' => "if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" */]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q1')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal', 'disabled' => !in_array(true, [$initiatives->jan, $initiatives->feb, $initiatives->mar])],
                'pluginOptions' => [
                    'autoclose' => true,
                    'startView'=>'year',
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd',
                    'startDate' => $initiatives->year . '-01-01',
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q2')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal', 'disabled' => !in_array(true, [$initiatives->apr, $initiatives->may, $initiatives->jun])],
                'pluginOptions' => [
                    'autoclose' => true,
                    'startView'=>'year',
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd',
                    'startDate' => $initiatives->year . '-01-01',
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q3')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal', 'disabled' => !in_array(true, [$initiatives->jul, $initiatives->aug, $initiatives->sep])],
                'pluginOptions' => [
                    'autoclose' => true,
                    'startView'=>'year',
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd',
                    'startDate' => $initiatives->year . '-01-01',
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q4')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal', 'disabled' => !in_array(true, [$initiatives->oct, $initiatives->nov, $initiatives->dec])],
                'pluginOptions' => [
                    'autoclose' => true,
                    'startView'=>'year',
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd',
                    'startDate' => $initiatives->year . '-01-01',
                ]
            ]) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>
    
</div>