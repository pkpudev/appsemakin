<?php

use app\models\Measure;
use app\models\TargetType;
use kartik\depdrop\DepDrop;
use kartik\money\MaskMoney;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Target */
/* @var $form yii\widgets\ActiveForm */
$measures = ArrayHelper::map(Measure::find()->where(['is_active' => true])->orderBy('measure')->all(), 'measure', 'measure');

?>

<div class="target-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'target_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'target_type_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(TargetType::find()->orderBy('target_type asc')->all(), 'id', 'target_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tipe Target --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'year')->widget(DatePicker::classname(), [
                'options' => ['id' => 'year', 'placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'unit_structure_id')->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => ['id' => 'unit', 'placeholder' => 'Pilih Unit'],
                'select2Options' => [
                    'initValueText' => $model->unit->unit_name,
                    'pluginOptions' => [
                                            'allowClear' => true,
                                            'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                        ]
                ],
                'pluginOptions' => [
                    'depends' => ['year'],
                    'url' => Url::to(['/strategic/ceiling/unit']),
                ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'position_structure_id')->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => ['id' => 'position', 'placeholder' => 'Pilih Jabatan'],
                'select2Options' => [
                    'initValueText' => $model->position->position_name,
                    'pluginOptions' => [
                                            'allowClear' => true,
                                            'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                        ]
                ],
                'pluginOptions' => [
                    'depends' => ['unit'],
                    'url' => Url::to(['/strategic/target/position']),
                ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'employee_id')->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => ['id' => 'employee', 'placeholder' => 'Pilih Karyawan'],
                'select2Options' => [
                    'pluginOptions' => [
                                            'allowClear' => true,
                                            'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                        ]
                ],
                'pluginOptions' => [
                    'depends' => ['position'],
                    'url' => Url::to(['/strategic/target/employee']),
                ]
            ]); ?>
        </div>
    </div>

    <div id="budgets">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'month_01')->input('number', []) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'month_07')->input('number', []) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'month_02')->input('number', []) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'month_08')->input('number', []) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'month_03')->input('number', []) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'month_09')->input('number', []) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'month_04')->input('number', []) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'month_10')->input('number', []) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'month_05')->input('number', []) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'month_11')->input('number', []) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'month_06')->input('number', []) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'month_12')->input('number', []) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?=  $form->field($model, 'total_target')->widget(MaskMoney::classname(), ['readonly'=>true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'measure')->widget(Select2::classname(), [
                'data' => $measures,
                'options' => [
                    'placeholder' => '-- Pilih Jenis Ukuran --',
                ],
                'pluginOptions' => [
                    'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                ],
            ]); ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php
$jsScript = <<<JS

var mMons =  $('input[type="number"]'); // jq array mask money ALL MONTHLY BUDGET
var mSum = $("#target-total_target-disp"); //jq mask money SUM MONTHLY BUDGET

var sumMonthlyBudget = function() {
    var tot = 0;

    mMons.each(function() {
        // var val = $(this).maskMoney('unmasked')[0];
        var val = $(this).val();
        tot += $.isNumeric(val) ? parseFloat(val) : 0;
    });

    return tot;
};

var maskSumMonthlyBudget = function() {
    mSum.maskMoney( "mask", sumMonthlyBudget() );
};

/**
 * inisiasi awal
 */
var init = function() {
    maskSumMonthlyBudget();
};

/**
 * jika BUDGET BULANAN berubah
 */
mMons.on("keyup", maskSumMonthlyBudget);
mMons.on("change", maskSumMonthlyBudget);

/**
 * jalankan inisiasi awal on-ready
 */
init();

JS;

$this->registerJs($jsScript, \yii\web\View::POS_READY, 'rkat-budget-form-js');
?>