<?php

use app\models\TargetType;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'target_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'target_type_id',
        'value' => function($model){
            return $model->target_type_id ? $model->type->target_type: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(TargetType::find()->orderBy('target_type asc')->all(), 'id', 'target_type'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_code',
        'value' => function($model){
            return $model->unit->unit_code;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_structure_id',
        'value' => function($model){
            return $model->unit->unit_name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'year',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'total_target',
        'format' => ['decimal', 0],
        'value' => function($model){
            return $model->total_target ?:0;
        },
        'hAlign' => 'right',
        'pageSummary' => true,
        'hiddenFromExport' => true,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'total_target',
        'hidden' => true,
        'value' => function($model){
            return $model->total_target ?:0;
        },
        'hAlign' => 'right',
        'pageSummary' => true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'measure',
        'value' => function($model){
            return $model->measure ?: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\Measure::find()->orderBy('measure asc')->asArray()->all(), 'measure', 'measure'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
        'hAlign' => 'right',
    ],
    [
        'attribute'=>'month_01',
        'value' => function($model){
            return $model->month_01 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'attribute'=>'month_02',
        'value' => function($model){
            return $model->month_02 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'attribute'=>'month_03',
        'value' => function($model){
            return $model->month_03 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'attribute'=>'month_04',
        'value' => function($model){
            return $model->month_04 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'attribute'=>'month_05',
        'value' => function($model){
            return $model->month_05 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'attribute'=>'month_06',
        'value' => function($model){
            return $model->month_06 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'attribute'=>'month_07',
        'value' => function($model){
            return $model->month_07 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'attribute'=>'month_08',
        'value' => function($model){
            return $model->month_08 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'attribute'=>'month_09',
        'value' => function($model){
            return $model->month_09 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'attribute'=>'month_10',
        'value' => function($model){
            return $model->month_10 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'attribute'=>'month_11',
        'value' => function($model){
            return $model->month_11 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'attribute'=>'month_12',
        'value' => function($model){
            return $model->month_12 ?: 0;
        },
        'hidden' => true,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_by',
        'value' => function($model){
            return $model->created_by ? $model->createdBy0->full_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'value' => function($model){
            return $model->created_at ?: '-';
        },
        'filter' => false
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip', 'style' => 'color: var(--warning) !important'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item',
                          'style' => 'color: var(--danger) !important'],
    ],

];
