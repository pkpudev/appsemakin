<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Target */

?>
<div class="target-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
