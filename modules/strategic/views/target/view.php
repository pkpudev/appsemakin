<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Target */
?>
<div class="target-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'target_name',
            'year',
            [
                'attribute'=>'target_type_id',
                'value' => function($model){
                    return $model->target_type_id ? $model->type->target_type: '-';
                },
            ],
            [
                'attribute'=>'unit_structure_id',
                'value' => function($model){
                    return $model->unit->unit_name . ($model->position_structure_id ? ' / ' . $model->position->position_name : '');
                }
            ],
            [
                'attribute'=>'employee_id',
                'value' => function($model){
                    return $model->employee_id ? $model->employee->full_name : '-';
                }
            ],
            [
                'attribute'=>'measure',
                'value' => function($model){
                    return $model->measure ?: '-';
                }
            ],
            [
                'attribute'=>'month_01',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_01 ?: '-';
                }
            ],
            [
                'attribute'=>'month_02',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_02 ?: '-';
                }
            ],
            [
                'attribute'=>'month_03',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_03 ?: '-';
                }
            ],
            [
                'attribute'=>'month_04',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_04 ?: '-';
                }
            ],
            [
                'attribute'=>'month_05',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_05 ?: '-';
                }
            ],
            [
                'attribute'=>'month_06',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_06 ?: '-';
                }
            ],
            [
                'attribute'=>'month_07',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_07 ?: '-';
                }
            ],
            [
                'attribute'=>'month_08',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_08 ?: '-';
                }
            ],
            [
                'attribute'=>'month_09',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_09 ?: '-';
                }
            ],
            [
                'attribute'=>'month_10',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_10 ?: '-';
                }
            ],
            [
                'attribute'=>'month_11',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_11 ?: '-';
                }
            ],
            [
                'attribute'=>'month_12',
                'format' => ['decimal', 0],
                'value' => function($model){
                    return $model->month_12 ?: '-';
                }
            ],
            [
                'attribute'=>'total_target',
                'value' => function($model){
                    return $model->total_target ? number_format($model->total_target, 0, ',', '.') . ' ' . $model->measure : '-';
                }
            ],
            [
                'attribute'=>'created_by',
                'value' => function($model){
                    return $model->created_by ? $model->createdBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'created_at',
                'value' => function($model){
                    return $model->created_at ?: '-';
                },
            ],
            [
                'attribute'=>'updated_by',
                'value' => function($model){
                    return $model->updated_by ? $model->updatedBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($model){
                    return $model->updated_at ?: '-';
                },
            ],
        ],
    ]) ?>

</div>
