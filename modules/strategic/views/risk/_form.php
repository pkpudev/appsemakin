<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Risk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="risk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'initiatives_id')->textInput() ?>

    <?= $form->field($model, 'requirements')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'potential_failure')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'potential_effect_of_failure')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'severity')->textInput() ?>

    <?= $form->field($model, 'potential_causes_of_failure')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'current_process_controls_prevention')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'occurance')->textInput() ?>

    <?= $form->field($model, 'current_process_controls_detection')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'detection')->textInput() ?>

    <?= $form->field($model, 'rpn')->textInput() ?>

    <?= $form->field($model, 'risk_criteria')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'risk_category_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

  
    <?php if (!Yii::$app->request->isAjax){ ?>
          <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
