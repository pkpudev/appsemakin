<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Risk */
?>
<div class="risk-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'initiatives_id',
            'requirements:ntext',
            'potential_failure:ntext',
            'potential_effect_of_failure:ntext',
            'severity',
            'potential_causes_of_failure:ntext',
            'current_process_controls_prevention:ntext',
            'occurance',
            'current_process_controls_detection:ntext',
            'detection',
            'rpn',
            'risk_criteria:ntext',
            'risk_category_id',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
        ],
    ]) ?>

</div>
