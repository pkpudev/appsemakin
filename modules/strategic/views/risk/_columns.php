<?php

use app\models\RiskCategory;
use app\models\RiskMitigation;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'unit_id',
        'filter' => false,
        'format' => 'html',
        'value' => function ($model, $key, $index, $widget) {
            return '<b>' . $model->risk->initiatives->okr->position->unit->unit_code . ' ' . $model->risk->initiatives->okr->position->unit->unit_name . '</b>';
        },
        'group' => true,
        'groupedRow' => true,
        'groupOddCssClass' => 'kv-grouped-row',
        'groupEvenCssClass' => 'kv-grouped-row',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Kode Proker',
        'attribute'=>'initiatives_code',
        'value' => function($model){
            return $model->risk->initiatives->initiatives_code; 
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Program Kerja',
        'attribute'=>'initiatives_name',
        'value' => function($model){
            return $model->risk->initiatives->workplan_name; 
        }
    ],
    [
        'attribute' => 'risk_category_id',
        'label' => 'Kategori',
        'value' => function($model){
            return $model->risk->category->risk_category; 
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(RiskCategory::find()->orderBy('id')->all(), 'id', 'risk_category'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
    [
        'attribute' => 'potential_failure',
        'label' => 'Risiko',
        'value' => function($model){
            return $model->risk->potential_failure; 
        }
    ],
    /* [
        'attribute' => 'potential_effect_of_failure',
        'label' => 'Dampak',
        'value' => function($model){
            return $model->risk->potential_effect_of_failure; 
        }
    ], */
    [
        'attribute' => 'potential_causes_of_failure',
        'label' => 'Penyebab',
        'value' => function($model){
            return $model->risk->potential_causes_of_failure; 
        }
    ],
    [
        'attribute' => 'severity',
        'label' => 'Nilai Dampak',
        'value' => function($model){
            return $model->risk->severity; 
        }
    ],
    /* [
        'attribute' => 'potential_causes_of_failure',
        'label' => 'Penyebab Munculnya Masalah/Kerugian',
        'value' => function($model){
            return $model->risk->potential_causes_of_failure; 
        }
    ],
    [
        'attribute' => 'current_process_controls_prevention',
        'label' => 'Pencegahan Kontrol Proses Sekarang',
        'value' => function($model){
            return $model->risk->current_process_controls_prevention; 
        }
    ], */
    [
        'attribute' => 'occurance',
        'label' => 'Nilai Peluang',
        'value' => function($model){
            return $model->risk->occurance; 
        }
    ],
    [
        'attribute' => 'rpn',
        'label' => 'Tingkat Risiko Sebelum Mitigasi ',
        'value' => function($model){
            return $model->risk->rpn; 
        }
    ],
    [
        'attribute' => 'risk_criteria',
        'label' => 'Kriteria',
        'format' => 'raw',
        'value' => function($model){
            if(strtolower($model->risk->risk_criteria) == 'critical risk'){
                $color = '#ed1f24';
            } else if(strtolower($model->risk->risk_criteria) == 'high risk'){
                $color = '#f8981d';
            } else if(strtolower($model->risk->risk_criteria) == 'medium risk'){
                $color = '#f3ec19';
            } else if(strtolower($model->risk->risk_criteria) == 'low risk'){
                $color = '#94ca52';
            }

            return "<span style='background: $color !important; width: 15px; height: 15px; display: inline-block; border-radius: 50%; margin-right: 5px; border: 1px solid;'></span>{$model->risk->risk_criteria}";
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [
                        'Low Risk' => 'Low Risk',
                        'Medium Risk' => 'Medium Risk',
                        'High Risk' => 'High Risk',
                        'Critical Risk' => 'Critical Risk',
                    ],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
        'width' => '120px'
    ],
    [
        'attribute' => 'recomended_action'
    ],
    [
        'attribute' => 'due_date',
        'filterType'=>GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => ['format' => 'yyyy-mm-dd', 'autoclose' => true, 'todayHighlight' => true,],
        ],
        'contentOptions' => ['style' => 'width:150px;'],
    ]
];
