<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Risk */
?>
<div class="risk-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
