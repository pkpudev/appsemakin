<?php

use kartik\depdrop\DepDrop;
use kartik\money\MaskMoney;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ceiling */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ceiling-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'year')->widget(Select2::classname(), [
        'data' => [
            2021 => 2021,
            2022 => 2022,
            2023 => 2023,
            2024 => 2024,
            2025 => 2025,
        ],
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['id' => 'year', 'prompt' => '-- Pilih Tahun --'],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]); ?>

    <?= $form->field($model, 'unit_structure_id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'options' => ['placeholder' => 'Pilih Unit'],
        'select2Options' => [
            'initValueText' => $model->unit_structure_id ? $model->unit->unit_name : '',
            'pluginOptions' => [
                                    'allowClear' => true,
                                    'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                ]
        ],
        'pluginOptions' => [
            'depends' => ['year'],
            'url' => Url::to(['/strategic/ceiling/unit']),
        ]
    ]); ?>

    
    <?= $form->field($model, 'position_structure_id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'options' => [
                        'placeholder' => 'Pilih Jabatan'
                    ],
        'select2Options' => [
            'pluginOptions' => [
                                    'allowClear' => true,
                                    'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                ]
        ],
        'pluginOptions' => [
            'depends' => ['ceiling-unit_structure_id'],
            'url' => Url::to(['/strategic/ceiling/position']),
        ]
    ])->label('Jabatan'); ?>

    <?= $form->field($model, 'ceiling_amount')->widget(MaskMoney::classname(), [
        'pluginOptions' => [
            'prefix' => 'Rp ',
            // 'suffix' => ' ¢',        
            'thousands' => '.',
            'decimal' => ',',
            'allowNegative' => false
        ]
    ]); ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
