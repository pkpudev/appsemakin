<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ceiling */
?>
<div class="ceiling-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'unit_structure_id',
                'value' => function($model){
                    return $model->unit->unit_name;
                }
            ],
            [
                'attribute'=>'year',
            ],
            [
                'attribute'=>'ceiling_amount',
                'format' => 'currency'
            ],
            [
                'attribute'=>'created_by',
                'value' => function($model){
                    return $model->created_by ? $model->createdBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'created_at',
                'value' => function($model){
                    return $model->created_at ?: '-';
                },
                'filter' => false
            ],
            [
                'attribute'=>'updated_by',
                'value' => function($model){
                    return $model->updated_by ? $model->updatedBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($model){
                    return $model->updated_at ?: '-';
                },
                'filter' => false
            ],
        ],
    ]) ?>

</div>
