<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ceiling */

?>
<div class="ceiling-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
