<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Directorate',
        'value' => function($model){
            return ($model->position->level == 1) ? $model->position->position_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Department',
        'value' => function($model){
            return ($model->position->level == 2) ? $model->position->position_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Division/Squad',
        'value' => function($model){
            return ($model->position->level == 3 || $model->position->level == 5) ? $model->position->position_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'position Code',
        'value' => function($model){
            return $model->position->position_code ?: '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'objective_code',
        'label'=>'Objective Code',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'objective_desc',
        'label'=>'Objective Description',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'key_result_code',
        'label'=>'Key Result Code',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'key_result_desc',
        'label'=>'Key Result Description',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'target',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'measure',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'timebound_q1',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'timebound_q2',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'timebound_q3',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'timebound_q4',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_by',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_by',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'],
    ],

];
