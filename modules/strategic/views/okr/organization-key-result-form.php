<?php

use app\models\Controllability;
use app\models\Measure;
use app\models\OkrOrg;
use app\models\Perspective;
use app\models\PortfolioType;
use app\models\Validity;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

?>
<div class="org-kr">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($modelParent): ?>
        <b>Objective: </b><br />
        <?= $modelParent->okr; ?><br />
    <?php else: ?>
        <?= $form->field($model, 'parent_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(OkrOrg::find()->where(['type' => 'O'])->orderBy('okr_code asc')->all(), 'id', function($model){
                return '(' . $model->start_year . ' - ' . $model->end_year . ') ' . $model->okr_code . ' - ' . $model->okr;
            }),
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['prompt' => '-- Pilih Tipe Objective --'],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                ]
        ]); ?>
    <?php endif; ?>

    <?= $form->field($model, 'okr')->textarea(['rows' => 2])->label('Key Result') ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'perspective_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Perspective::find()->orderBy('perspective asc')->all(), 'id', 'perspective'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tipe Objective --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'portfolio_type_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(PortfolioType::find()->orderBy('portfolio_type asc')->all(), 'id', 'portfolio_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tipe Objective --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'controllability_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Controllability::find()->where(['is_active' => true])->asArray()->all(), 'id', 'degree_of_controllability'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tingkat Pengendalian --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'validity_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Validity::find()->where(['is_active' => true])->asArray()->all(), 'id', 'validity_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Polarization --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'weight')->textInput([]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'target')->textInput([]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'measure')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Measure::find()->where(['is_active' => true])->asArray()->all(), 'measure', 'measure'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Satuan --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <?= $form->field($model, 'implementation_model')->textarea(['rows' => 2]) ?>

    <?php ActiveForm::end(); ?>
    
</div>