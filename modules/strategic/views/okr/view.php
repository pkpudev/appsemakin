<?php

use app\models\Okr;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Okr */
?>
<div class="okr-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'template' => "<tr><th width='150px'>{label}</th><td>{value}</td></tr>",
        'attributes' => [
            [
                'label' => 'Objective',
                'value' => function($model){
                    return $model->parent->okr_code . ' - ' . $model->parent->okr;
                }
            ],
            [
                'attribute' => 'okr_code',
                'label' => 'Key Result',
                'value' => function($model){
                    return $model->okr_code . ' - ' . $model->okr;
                }
            ],
            [
                'attribute' => 'okr_level_id',
                'value' => function($model){
                    return $model->level->okr_level; 
                }
            ],
            [
                'attribute' => 'position_structure_id',
                'label' => 'Jabatan',
                'value' => function($model){
                    return $model->position->position_name;
                }
            ],
            [
                'attribute' => 'pic_id',
                'label' => 'PIC',
                'value' => function($model){
                    return $model->pic_id ? $model->pic->full_name : '-';
                }
            ],
            /* [
                'attribute' => 'weight',
                'label' => 'Bobot',
                'value' => function($model){
                    return $model->weight ? : '-';
                }
            ],
            [
                'attribute' => 'unit_weight',
                'label' => 'Bobot Unit',
                'value' => function($model){
                    return $model->unit_weight ? : '-';
                }
            ], */
        ],
    ]) ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'support_type',
                        'value' => function($model){
                            return $model->support_type ? $model->getSupport() : '-';
                        }
                    ],
                    [
                        'attribute' => 'polarization_id',
                        'value' => function($model){
                            return $model->polarization_id ? $model->polarization->polarization_type : '-';
                        }
                    ],
                    [
                        'attribute' => 'calculation_id',
                        'value' => function($model){
                            return $model->calculation_id ? $model->calculation->calculation_type : '-';
                        }
                    ],
                    [
                        'attribute' => 'validity_id',
                        'value' => function($model){
                            return $model->validity_id ? $model->validity->validity_type : '-';
                        }
                    ],
                    [
                        'attribute' => 'controllability_id',
                        'value' => function($model){
                            return $model->controllability_id ? $model->controll->degree_of_controllability : '-';
                        }
                    ],
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'created_by',
                        'value' => function($model){
                            return $model->createdBy0->full_name; 
                        }
                    ],
                    'created_at',
                    [
                        'attribute' => 'updated_by',
                        'value' => function($model){
                            return $model->updated_by ? $model->updatedBy0->full_name : '-'; 
                        }
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => function($model){
                            return $model->updated_by ? $model->updated_at : '-'; 
                        }
                    ]
                ],
            ]) ?>
        </div>
    </div>

    <label>Target OKR</label>
    <table class="kv-grid-table table table-hover table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
        <thead>
            <tr>
                <th>Target K1</th>
                <th>Tenggat K1</th>
                <th>Target K2</th>
                <th>Tenggat K2</th>
                <th>Target K3</th>
                <th>Tenggat K3</th>
                <th>Target K4</th>
                <th>Tenggat K4</th>
                <th>Total Target</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td id="target_q1_text"><?= $model->target_q1 ? $model->target_q1 . ' ' . $model->measure: '-' ?></td>
                <td id="timebound_q1_text"><?= $model->timebound_q1 ? $model->timebound_q1: '-' ?></td>
                <td id="target_q2_text"><?= $model->target_q2 ? $model->target_q2 . ' ' . $model->measure: '-' ?></td>
                <td id="timebound_q2_text"><?= $model->timebound_q2 ? $model->timebound_q2: '-' ?></td>
                <td id="target_q3_text"><?= $model->target_q3 ? $model->target_q3 . ' ' . $model->measure: '-' ?></td>
                <td id="timebound_q3_text"><?= $model->timebound_q3 ? $model->timebound_q3: '-' ?></td>
                <td id="target_q4_text"><?= $model->target_q4 ? $model->target_q4 . ' ' . $model->measure: '-' ?></td>
                <td id="timebound_q4_text"><?= $model->timebound_q4 ? $model->timebound_q4: '-' ?></td>
                <td id="target_total_text"><?= $model->target ? $model->target . ' ' . $model->measure: '-' ?></td>
            </tr>
        </tbody>
    </table>

    <br />
    <div style="<?= $source == 'initiatives' ? 'display: none' : '' ?>">
        <label><?= $model->cascadingType->cascading_type; ?></label>
        <table class="kv-grid-table table table-hover table-bordered table-striped table-condensed" style="margin-bottom: 0 !important;">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Unit</th>
                    <th>Jabatan</th>
                    <th>PIC</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $childs = Okr::find()->where(['parent_id' => $model->id])->orderBy('id')->all();
                    $i = 1;
                    foreach ($childs as $child):
                ?>
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $child->position->unit->unit_name ?></td>
                        <td><?= $child->position->position_name ?></td>
                        <td><?= $child->pic_id ? $child->pic->full_name : '-' ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

</div>
