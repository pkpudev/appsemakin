<?php

use app\models\Okr;
use app\models\OkrLevel;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\widgets\Pjax;

$this->title = 'OKR - List';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
?>
<style>
    .modal-content {
        width: 800px;
    }

    #crud-datatable-pjax {
        height: 300px; overflow-x: auto;
    }

    .obj-act {
        background: #ddd;
        padding: 2px 10px;
        border-radius: 5px;
    }

    .btn-tab {
        border-color: var(--background) !important;
        color: var(--background) !important;
    }

    .btn-tab-active, .btn-tab:hover {
        background: var(--background) !important;
        color: #fff !important;
    }
</style>
<div class="okr-index">
    
    <div class="box box-default" style="width: 710px; margin-bottom: -5px;">
        <div class="box-body">
            <div class="btn-group">
                <?= Html::a('OKR Organisasi', ['organization'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
                <?php // Html::a('Lihat OKR dalam format List', ['#'], ['class' => 'btn btn-default btn-tab btn-tab-active btn-sm']); ?>
                <?= Html::a('Lihat OKR dalam format Tabel', ['index-table'], ['class' => 'btn btn-default btn-tab btn-sm']); ?>
                <?= Html::a('OKR Individu', ['individual'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
            </div>
        </div>
    </div>

    <div class="box box-default">
        <div class="box-body" style="overflow: auto;">
            <?= Html::a('Create Objective of Organization', ['create', 'level' => 1], ['class' => 'btn btn-success btn-sm', 'role' => 'modal-remote']); ?>
            <table class="pull-right" style="width: 600px;">
                <tr>
                    <td style="padding-right: 3px;">
                        <?= DatePicker::widget([
                            'name' => 'year-filter',
                            'value' => $yearFilter,
                            'options' => [
                                'id' => 'year-filter',
                                'class' => 'form-control',
                                'placeholder' => 'Filter Tahun',
                                'onchange' => 'changeYear($(this).val())',
                            ],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'startView'=>'year',
                                'minViewMode'=>'years',
                                'format' => 'yyyy'
                            ]
                        ]); ?>
                    </td>
                    <td width="50%" style="padding-left: 3px;">
                        <?= Select2::widget([
                            'name' => 'unit-filter',
                            'value' => $unitFilter,
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'id' => 'unit-filter',
                                'class' => 'form-control additional-filter',
                                'placeholder' => '-- Filter Unit Kerja --',
                                'onchange' => 'changeUnit($(this).val())',
                            ],
                            'pluginOptions' => ['allowClear' => true],
                            'data' => $listUnit,
                        ]); ?>
                    </td>
                </tr>
            </table>

            <hr />

            <div style="width: 2500px; height: 500px;">
                <?php //Pjax::begin(['id' => 'crud-datatable-pjax']) ?>
                <?php foreach($okrsLv1 as $okrLv1): ?>
                
                    <a href="#" title="Expand/Collapse" data-toggle="collapse" data-target="#okr-<?= $okrLv1->id; ?>" style = 'color: rgb(143, 97, 169) !important; font-weight: bolder; z-index: 999;'>+</a>&nbsp;
                    <?= Html::a($okrLv1->okr_code . ' (' . ($okrLv1->type == 'O' ? 'Objective' : 'Key Result') . ') - ' . $okrLv1->okr, ['index', 'id' => $okrLv1->id], ['id' => 'link-okr-' . $okrLv1->id, 'style' => 'color: rgb(143, 97, 169) !important; font-weight: bolder; margin-left: 10px;', 'class' => ($okrLv1->id == $id ? 'obj-act':'') ]);?><br />
                    <div id='okr-<?= $okrLv1->id; ?>' class="collapse in">
                        <?php 
                            $okrsLv2 = Okr::find()->where(['parent_id' => $okrLv1->id])->orderBy('okr_code asc');
                            if($showOkrIds){
                                $okrsLv2->andWhere(['in', 'id', $showOkrIds]);
                            }
                            $okrsLv2 = $okrsLv2->all();

                            if(!$okrsLv2 && $okrLv1->type == 'O') echo '<i style="margin-left: 30px">&#8226; OKR turunanya belum tersedia.</i>';
                            foreach($okrsLv2 as $okrLv2): 
                        ?>

                            <a href="#" title="Expand/Collapse" data-toggle="collapse" data-target="#okr-<?= $okrLv2->id; ?>" style = 'color: rgb(143, 97, 169) !important; font-weight: bolder; z-index: 999; margin-left: 20px;'>+</a>&nbsp;        
                            <?php if($okrLv2->type == 'O'): ?>
                                <?= Html::a($okrLv2->okr_code . ' (Objective - ' . $okrLv2->position->unit->unit_name . ') - ' . $okrLv2->okr, ['index', 'id' => $okrLv2->id], ['id' => 'link-okr-' . $okrLv2->id, 'style' => 'color: rgb(143, 97, 169) !important; font-weight: bolder; margin-left: 10px;', 'class' => ($okrLv2->id == $id ? 'obj-act':'')]);?><br />
                                <?php else: ?>
                                    <b style="margin-left: 10px"><?= $okrLv2->okr_code; ?> (Key Result) - <?= $okrLv2->okr; ?></b><br />
                            <?php endif; ?>
                            <div id='okr-<?= $okrLv2->id; ?>' class="collapse in">
                                <?php 
                                    $okrsLv3 = Okr::find()->where(['parent_id' => $okrLv2->id])->orderBy('okr_code asc');
                                    if($showOkrIds){
                                        $okrsLv3->andWhere(['in', 'id', $showOkrIds]);
                                    }
                                    $okrsLv3 = $okrsLv3->all();

                                    if(!$okrsLv3 && $okrLv2->type == 'O') echo '<i style="margin-left: 60px">&#8226; OKR turunanya belum tersedia.</i>';
                                    foreach($okrsLv3 as $okrLv3): 
                                ?>

                                    <a href="#" title="Expand/Collapse" data-toggle="collapse" data-target="#okr-<?= $okrLv3->id; ?>" style = 'color: rgb(143, 97, 169) !important; font-weight: bolder; z-index: 999; margin-left: 40px;'>+</a>&nbsp;
                                    <?php if($okrLv3->type == 'O'): ?>
                                        <?= Html::a($okrLv3->okr_code . ' (Objective - ' . $okrLv3->position->unit->unit_name . ') - ' . $okrLv3->okr, ['index', 'id' => $okrLv3->id], ['id' => 'link-okr-' . $okrLv3->id, 'style' => 'color: rgb(143, 97, 169) !important; font-weight: bolder; margin-left: 10px;', 'class' => ($okrLv3->id == $id ? 'obj-act':'') ]);?><br />
                                        <?php else: ?>
                                            <b style="margin-left: 10px"><?= $okrLv3->okr_code; ?> (Key Result) - <?= $okrLv3->okr; ?></b><br />
                                    <?php endif; ?>                                
                                    <div id='okr-<?= $okrLv3->id; ?>' class="collapse in">
                                        <?php 
                                            $okrsLv4 = Okr::find()->where(['parent_id' => $okrLv3->id])->orderBy('okr_code asc');
                                            if($showOkrIds){
                                                $okrsLv4->andWhere(['in', 'id', $showOkrIds]);
                                            }
                                            $okrsLv4 = $okrsLv4->all();

                                            if(!$okrsLv4 && $okrLv3->type == 'O') echo '<i style="margin-left: 90px">&#8226; OKR turunanya belum tersedia.</i>';
                                            foreach($okrsLv4 as $okrLv4): 
                                        ?>

                                            <a href="#" title="Expand/Collapse" data-toggle="collapse" data-target="#okr-<?= $okrLv4->id; ?>" style = 'color: rgb(143, 97, 169) !important; font-weight: bolder; z-index: 999; margin-left: 70px;'>+</a>&nbsp;
                                            <?php if($okrLv4->type == 'O'): ?>
                                                <?= Html::a($okrLv4->okr_code . ' (Objective - ' . $okrLv4->position->unit->unit_name . ') - ' . $okrLv4->okr, ['index', 'id' => $okrLv4->id], ['id' => 'link-okr-' . $okrLv4->id, 'style' => 'color: rgb(143, 97, 169) !important; font-weight: bolder; margin-left: 10px;', 'class' => ($okrLv4->id == $id ? 'obj-act':'') ]);?><br />
                                                <?php else: ?>
                                                    <b style="margin-left: 10px"><?= $okrLv4->okr_code; ?> (Key Result) - <?= $okrLv4->okr; ?></b><br />
                                            <?php endif; ?>                                
                                            <div id='okr-<?= $okrLv4->id; ?>' class="collapse in">
                                                <?php 
                                                    $okrsLv5 = Okr::find()->where(['parent_id' => $okrLv4->id])->orderBy('okr_code asc');
                                                    if($showOkrIds){
                                                        $okrsLv5->andWhere(['in', 'id', $showOkrIds]);
                                                    }
                                                    $okrsLv5 = $okrsLv5->all();
                                                    if(!$okrsLv5 && $okrLv4->type == 'O') echo '<i style="margin-left: 110px">&#8226; OKR turunanya belum tersedia.</i>';
                                                    foreach($okrsLv5 as $okrLv5): 
                                                ?>
                                                    
                                                    <a href="#" title="Expand/Collapse" data-toggle="collapse" data-target="#okr-<?= $okrLv5->id; ?>" style = 'color: rgb(143, 97, 169) !important; font-weight: bolder; z-index: 999; margin-left: 90px;'>+</a>&nbsp;
                                                    <?php if($okrLv5->type == 'O'): ?>
                                                        <?= Html::a($okrLv5->okr_code . ' (Objective - ' . $okrLv5->position->unit->unit_name . ') - ' . $okrLv5->okr, ['index', 'id' => $okrLv5->id], ['id' => 'link-okr-' . $okrLv5->id, 'style' => 'color: rgb(143, 97, 169) !important; font-weight: bolder; margin-left: 10px;', 'class' => ($okrLv5->id == $id ? 'obj-act':'') ]);?><br />
                                                        <?php else: ?>
                                                            <b style="margin-left: 10px"><?= $okrLv5->okr_code; ?> (Key Result) - <?= $okrLv5->okr; ?></b><br />
                                                    <?php endif; ?>                                
                                                    <div id='okr-<?= $okrLv5->id; ?>' class="collapse in">
                                                        <?php 
                                                            $okrsLv6 = Okr::find()->where(['parent_id' => $okrLv5->id])->orderBy('okr_code asc');
                                                            if($showOkrIds){
                                                                $okrsLv6->andWhere(['in', 'id', $showOkrIds]);
                                                            }
                                                            $okrsLv6 = $okrsLv6->all();
                                                            if(!$okrsLv6 && $okrLv5->type == 'O') echo '<i style="margin-left: 110px">&#8226; OKR turunanya belum tersedia.</i>';
                                                            foreach($okrsLv6 as $okrLv6): 
                                                        ?>

                                                            <a href="#" title="Expand/Collapse" data-toggle="collapse" data-target="#okr-<?= $okrLv6->id; ?>" style = 'color: rgb(143, 97, 169) !important; font-weight: bolder; z-index: 999; margin-left: 110px;'>+</a>&nbsp;
                                                            <?php if($okrLv6->type == 'O'): ?>
                                                                <?= Html::a($okrLv6->okr_code . ' (Objective - ' . $okrLv6->position->unit->unit_name . ') - ' . $okrLv6->okr, ['index', 'id' => $okrLv6->id], ['id' => 'link-okr-' . $okrLv6->id, 'style' => 'color: rgb(143, 97, 169) !important; font-weight: bolder; margin-left: 10px;', 'class' => ($okrLv6->id == $id ? 'obj-act':'') ]);?><br />
                                                                <?php else: ?>
                                                                    <b style="margin-left: 10px"><?= $okrLv6->okr_code; ?> (Key Result) - <?= $okrLv6->okr; ?></b><br />
                                                            <?php endif; ?>                                
                                                            <div id='okr-<?= $okrLv6->id; ?>' class="collapse in">
                                                                <?php 
                                                                    $okrsLv7 = Okr::find()->where(['parent_id' => $okrLv6->id])->andWhere("okr_level_id != 5")->orderBy('okr_code asc');
                                                                    if($showOkrIds){
                                                                        $okrsLv7->andWhere(['in', 'id', $showOkrIds]);
                                                                    }
                                                                    $okrsLv7 = $okrsLv7->all();
                                                                    if(!$okrsLv7 && $okrLv6->type == 'O') echo '<i style="margin-left: 140px">&#8226; OKR turunanya belum tersedia.</i>';
                                                                    foreach($okrsLv7 as $okrLv7): 
                                                                ?>
                                                                
                                                                    <a href="#" title="Expand/Collapse" data-toggle="collapse" data-target="#okr-<?= $okrLv7->id; ?>" style = 'color: rgb(143, 97, 169) !important; font-weight: bolder; z-index: 999; margin-left: 140px;'>+</a>&nbsp;
                                                                    <?php if($okrLv7->type == 'O'): ?>
                                                                        <?= Html::a($okrLv7->okr_code . ' (Objective - ' . $okrLv7->position->unit->unit_name . ') - ' . $okrLv7->okr, ['index', 'id' => $okrLv7->id], ['id' => 'link-okr-' . $okrLv7->id, 'style' => 'color: rgb(143, 97, 169) !important; font-weight: bolder; margin-left: 10px;', 'class' => ($okrLv7->id == $id ? 'obj-act':'') ]);?><br />
                                                                        <?php else: ?>
                                                                            <b style="margin-left: 10px"><?= $okrLv7->okr_code; ?> (Key Result) - <?= $okrLv7->okr; ?></b><br />
                                                                    <?php endif; ?>                                
                                                                    <div id='okr-<?= $okrLv7->id; ?>' class="collapse in">
                                                                        <?php 
                                                                            $okrsLv8 = Okr::find()->where(['parent_id' => $okrLv7->id])->andWhere("okr_level_id != 5")->orderBy('okr_code asc');
                                                                            if($showOkrIds){
                                                                                $okrsLv8->andWhere(['in', 'id', $showOkrIds]);
                                                                            }
                                                                            $okrsLv8 = $okrsLv8->all();
                                                                            if(!$okrsLv8 && $okrLv7->type == 'O') echo '<i style="margin-left: 160px">&#8226; OKR turunanya belum tersedia.</i>';
                                                                            foreach($okrsLv8 as $okrLv8): 
                                                                        ?>
                                                                            <?= Html::a('&#8226; ' . $okrLv8->okr_code . ' (' . ($okrLv8->type == 'O' ? 'Objective' : 'Key Result') . ') - ' . $okrLv8->okr, ['#'], ['data-toggle' => 'collapse', 'data-target' => '#okr-' . $okrLv8->id, 'style' => 'color: rgb(143, 97, 169) !important; font-weight: bolder;' ]);?><br />
                                                                        <?php endforeach; ?>
                                                                    </div>
                                                                
                                                                <?php endforeach; ?>
                                                            </div>
                                                        
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                            
                                        <?php endforeach; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>

                <?php endforeach; ?>
                <?php //Pjax::end() ?>
            </div>
        </div>
    </div>

        <?php if($model): ?>
            <div class="box box-default">
                <div class="box-body" id="okr-detail">

                <h3><?= $model->okr_code . ' - ' . ($model->position_structure_id ? $model->position->position_name . ' (' . $model->position->unit->unit_name . ')' : 'Organisasi'); ?></h3>
                    
                <div class="box-okr-<?= $model->id; ?>">

                    <div class="btn-group pull-right" style="position: absolute;right: 15px;">
                        <?php 
                            $check = Okr::find()->where(['parent_id' => $model->id])->one(); 
                            if(!$check) echo Html::a(
                                                    'Hapus Objective', 
                                                    ['delete', 'id' => $model->id], 
                                                    [
                                                        'class' => 'btn btn-warning btn-xs',
                                                        'role'=>'modal-remote','title'=>'Hapus Objective Ini',
                                                        'data-confirm'=>false, 'data-method'=>false,
                                                        'data-request-method'=>'post',
                                                        'data-toggle'=>'tooltip',
                                                        'data-confirm-title'=>'Konfirmasi',
                                                        'data-confirm-message'=>'Yakin ingin menghapus Objective ini?',
                                                        'style' => 'color: var(--danger) !important;'
                                                    ]);
                        ?>
                        <?= Html::a('Tambah Key Result', ['create', 'parent_id' => $model->id], ['class' => 'btn btn-success btn-xs', 'role' => 'modal-remote']); ?>
                    </div>
                    <b>Detail Current OKR - <?= $model->level->okr_level; ?></b>

                    <br />
                    <?= $this->render('_detail_current_kr', [
                        'dataProviderCurrentKr' => $dataProviderCurrentKr,
                    ]) ?>
                    <br />
                    <b>OKR 1 Level Above - <?= OkrLevel::findOne($model->okr_level_id-1)->okr_level ?: ''; ?></b>
                    <?= $this->render('_detail_below_kr', [
                        'dataProvider' => $dataProviderAboveKr,
                    ]) ?>
                    <br />
                    <b>OKR 1 Level Below -  <?= OkrLevel::findOne($model->okr_level_id+1)->okr_level ?: ''; ?> </b>
                    <?= $this->render('_detail_above_kr', [
                        'dataProvider' => $dataProviderBelowKr,
                    ]) ?>

                    </div>
                </div>
            </div>
        <?php endif; ?>

</div>

<script>
    function changeYear(val) {
        var year = val;
        var unit = $('#unit-filter').val();

        if(unit){
            window.location.replace("/strategic/okr/index?yearFilter=" + year + '&unitFilter=' + unit);
        } else {
            window.location.replace("/strategic/okr/index?yearFilter=" + year);
        }
    }

    function changeUnit(val) {
        var year = $('#year-filter').val();
        var unit = val;

        window.location.replace("/strategic/okr/index?yearFilter=" + year + '&unitFilter=' + unit);
    }

</script>

<?php
$script = <<< JS
    $('html,body').animate({scrollTop: 580}, 1000);
        
JS;

if($id) $this->registerJs($script);
?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
