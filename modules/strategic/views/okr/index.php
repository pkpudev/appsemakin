<?php

use app\models\Initiatives;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\VwEmployeeManager;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\web\View;

$this->title = 'OKR - Unit';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>

    .btn-tab {
        border-color: var(--background) !important;
        color: var(--background) !important;
    }

    .btn-tab-active, .btn-tab:hover {
        background: var(--background) !important;
        color: #fff !important;
    }
</style>
<div class="okr-index">

    <div class="box box-default" style="width: 365px; margin-bottom: -5px;">
        <div class="box-body">
            <div class="btn-group">
                <?php // Html::a('OKR Organisasi', ['organization'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
                <?php // Html::a('Lihat OKR dalam format List', ['index'], ['class' => 'btn btn-default btn-tab btn-sm']); ?>
                <?= Html::a('OKR Unit', ['index'], ['class' => 'btn btn-default btn-tab btn-tab-active btn-sm', 'style' => 'width: 170px;']); ?>
                <?= Html::a('OKR Individu', ['individual'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
            </div>
        </div>
    </div>


    <div class="box box-default">
        <div class="box-body">
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'filterSelector' => '.additional-filter',
                    'showPageSummary' => true,
                    'pjax'=>true,
                    'export' => [
                        'label' => 'Export Excel atau PDF',
                        'header' => '',
                        'options' => [
                            'class' => 'btn btn-primary'
                        ], 
                        'menuOptions' => [ 
                                'style' => 'background: var(--background); width: 187px;'
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => true,
                        GridView::EXCEL => true,
                    ],
                    'columns' => [
                        [
                            'class' => 'kartik\grid\SerialColumn',
                            'width' => '30px',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            // 'attribute' => 'parent_id',
                            'label' => 'Unit',
                            'value' => function($model){
                                return $model->parent->position->unit->unit_name ?: 'Organisasi';
                            },
                            'group' => true,
                            'groupedRow' => true,                    // move grouped column to a single grouped row
                            'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
                            'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'okr_code',
                            'label' => 'Kode O',
                            'value' => function($model){
                                return $model->parent->okr_code;
                            },
                            'width' => '120px',
                            'group' => true,
                            'subGroupOf' => 1
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'okr',
                            'label' => 'Objective',
                            'format' => 'raw',
                            'value' => function($model){
                                $check = $model->parent->okr_level_id <> OkrLevel::ORGANIZATION;
                                $hasChild = Okr::findOne(['parent_id' => $model->parent_id]);

                                return  $model->parent->okr . '<br />' . 
                                        '<div class="btn-group">' . 
                                        (($check && (Yii::$app->user->can('Admin') || Yii::$app->user->can('Admin Direktorat') || Yii::$app->user->can('Leader'))) ?
                                        '<br />' . Html::a('<span class="fa fa-plus"></span> Tambah Key Result', ['create', 'parent_id' => $model->parent_id], ['role' => 'modal-remote', 'class' => 'btn btn-success btn-xs']) : '') .
                                        ((!$check && Yii::$app->user->can('Admin')) ?
                                        '<br />' . Html::a('<span class="fa fa-plus"></span> Turunkan Full Cascade', ['create-full', 'parent_id' => $model->parent_id], ['role' => 'modal-remote', 'class' => 'btn btn-success btn-xs']) : '') .
                                        ((!$hasChild) ?
                                        Html::a('<span class="fa fa-trash"></span>', ['delete', 'id' => $model->parent_id], ['role' => 'modal-remote', 'class' => 'btn btn-danger btn-xs', 
                                        'data-confirm'=>false, 'data-method'=>false,
                                        'data-request-method'=>'post',
                                        'data-toggle'=>'tooltip',
                                        'data-confirm-title'=>'Konfirmasi',
                                        'data-confirm-message'=>'Yakin ingin menghapus Objective ini?',
                                        'style' => 'color: var(--danger) !important;']) . '<br />' : '') .
                                        '</div></br >';
                            },
                            'width' => '200px',
                            'group' => true,
                            'subGroupOf' => 1
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_okr_code',
                            'label' => 'Kode KR',
                            'value' => function($model){
                                return $model->child->okr_code ?: '-';
                            },
                            'width' => '120px',
                            // 'group' => true,
                            // 'subGroupOf' => 2
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_okr',
                            'label' => 'Key Result',
                            'value' => function($model){
                                return $model->child->okr ?: '-';
                            },
                            'width' => '200px',
                            // 'group' => true,
                            // 'subGroupOf' => 2
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'label' => 'Action',
                            'value' => function($model){
                                $check1 = Okr::find()->where(['parent_id' => $model->child_id])->one();
                                $check2 = !in_array($model->parent->okr_level_id, [OkrLevel::ORGANIZATION, OkrLevel::DIVISION]);
                                $check3 = $model->parent->pic_id == Yii::$app->user->id;
                                $check4 = Initiatives::find()->where(['okr_id' => $model->child_id])->one();
                                $check5 = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['year' => $model->year])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one();

                                // if(Yii::$app->user->id == 347478) var_dump(($check3 || $check5 || Yii::$app->user->can('Admin') || Yii::$app->user->can('Admin Direktorat')));

                                return 
                                        ($model->child_id ? Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->child_id], ['role' => 'modal-remote', 'title' => 'Lihat Key Result']) . '<br />' : '') .
                                        (($check3 || $check5 || Yii::$app->user->can('Admin') || Yii::$app->user->can('Admin Direktorat')) && $model->child_id ?
                                        Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model->child_id], ['role' => 'modal-remote', 'title' => 'Update Key Result', 'style' => 'color: var(--warning) !important;']) . '<br />'
                                        : '') .
                                        ($model->child_id && $check2 && ($check3 || $check5 || Yii::$app->user->can('Admin') || Yii::$app->user->can('Admin Direktorat')) ? Html::a('<span class="fa fa-plus-circle"></span>', ['create-support', 'parent_id' => $model->child_id], ['role' => 'modal-remote', 'title' => 'Create Support Cascading']) . '<br />' : '') .
                                        (!$check1 && !$check4 && ($check3 || $check5 || Yii::$app->user->can('Admin') || Yii::$app->user->can('Admin Direktorat')) ? 
                                        Html::a(
                                            '<span class="fa fa-trash"></span>', 
                                            ['delete', 'id' => $model->child_id], 
                                            [
                                                'role'=>'modal-remote','title'=>'Hapus Key Result Ini',
                                                'data-confirm'=>false, 'data-method'=>false,
                                                'data-request-method'=>'post',
                                                'data-toggle'=>'tooltip',
                                                'data-confirm-title'=>'Konfirmasi',
                                                'data-confirm-message'=>'Yakin ingin menghapus Key Result ini?',
                                                'style' => 'color: var(--danger) !important;'
                                            ]) : '') ;
                            },
                            'hAlign' => 'center',
                            'format' => 'raw',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_weight',
                            'label' => 'Bobot Obj',
                            'value' => function($model){
                                return $model->child->weight ? round($model->child->weight, 2): '';
                            },
                            'pageSummary' => true,
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            // 'attribute' => 'child_weight',
                            'label' => 'Bobot Unit',
                            'value' => function($model){
                                return $model->child->unit_weight ? round($model->child->unit_weight, 2): '';
                            },
                            'pageSummary' => true,
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_target',
                            'label' => 'Total Target',
                            'value' => function($model){
                                return $model->child->target ?  (is_numeric($model->child->target) && floor($model->child->target) != $model->child->target ?  number_format($model->child->target, 2, ',', '.') : $model->child->target) . ' ' . $model->child->measure: 0;
                            },
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_target_q1',
                            'label' => 'Target K1',
                            'value' => function($model){
                                return $model->child->target_q1 ? ((is_numeric($model->child->target_q1) && floor($model->child->target_q1) != $model->child->target_q1 ?  number_format($model->child->target_q1, 2, ',', '.') : $model->child->target_q1)) . ' ' . $model->child->measure: '-';
                            },
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_timebound_q1',
                            'label' => 'Tenggat K1',
                            'value' => function($model){
                                return $model->child->timebound_q1 ?: '-';
                            },
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_target_q2',
                            'label' => 'Target K2',
                            'value' => function($model){
                                return $model->child->target_q2 ? ((is_numeric($model->child->target_q2) && floor($model->child->target_q2) != $model->child->target_q2 ? number_format($model->child->target_q2, 2, ',', '.') : $model->child->target_q2)) . ' ' . $model->child->measure: '-';
                            },
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_timebound_q2',
                            'label' => 'Tenggat K2',
                            'value' => function($model){
                                return $model->child->timebound_q2 ?: '-';
                            },
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_target_q3',
                            'label' => 'Target K3',
                            'value' => function($model){
                                return $model->child->target_q3 ? ((is_numeric($model->child->target_q3) && floor($model->child->target_q3) != $model->child->target_q3 ? number_format($model->child->target_q3, 2, ',', '.') : $model->child->target_q3)) . ' ' . $model->child->measure: '-';
                            },
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_timebound_q3',
                            'label' => 'Tenggat K3',
                            'value' => function($model){
                                return $model->child->timebound_q3 ?: '-';
                            },
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_target_q4',
                            'label' => 'Target K4',
                            'value' => function($model){
                                return $model->child->target_q4 ? (is_numeric($model->child->target_q4) && floor($model->child->target_q4) != $model->child->target_q4 ? number_format($model->child->target_q4, 2, ',', '.') : $model->child->target_q4) . ' ' . $model->child->measure: '-';
                            },
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_timebound_q4',
                            'label' => 'Tenggat K4',
                            'value' => function($model){
                                return $model->child->timebound_q4 ?: '-';
                            },
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            // 'attribute' => 'child_timebound_q4',
                            'label' => 'Tipe Cascading',
                            'value' => function($model){
                                return $model->child->cascadingType->cascading_type ?: '-';
                            },
                        ],
                        /* [
                            'class'=>'\kartik\grid\DataColumn',
                            'label'=>'Lengkap?',
                            'attribute' => 'is_completed',
                            'format' => 'raw',
                            'value' => function($model){
                                $check = !$model->child->polarization_id || !$model->child->calculation_id || !$model->child->validity_id || !$model->child->controllability_id;
                                
                                return $check ? '<span class="fa fa-info-circle" style="color: var(--warning);" title="Harus Dilengkapi"></span>' : '<span class="fa fa-check" style="color: var(--success);" title="Oke"></span>';
                            },
                            'filterType'=>GridView::FILTER_SELECT2,
                            'filter'=>[1 => 'Lengkap', 2 => 'Belum Lengkap'],
                            'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
                            'filterInputOptions'=>['placeholder'=>''],
                            'hAlign' => 'center',
                        ], */
                    ],
                    'toolbar'=> [
                        [
                            'content' => Html::a('<i class="fa fa-plus"></i> Tambah OKR Organisasi', ['import-okr-org'],
                                                ['data-pjax'=>0, 'class'=>'btn btn-success'])/*  .
                                        Html::a('<i class="fa fa-plus"></i> Tambah Key Result', ['create', 'isInputKr' => true],
                                                ['role'=>'modal-remote', 'class'=>'btn btn-warning']) */,
                            'options' => [
                                'style' => Yii::$app->user->can('Admin') || Yii::$app->user->can('Admin Direktorat') ? '' : 'display: none;'
                            ]
                        ],
                        ['content'=>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                            '{toggleData}' .
                            Html::a(
                                '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                                '#',
                                ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Download Excel']
                            )
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'type' => 'primary',
                        'heading' => '<i class="fa fa-list-alt"></i> Daftar OKR',
                        'before' => //(Yii::$app->user->can('Admin') or Yii::$app->user->can('BOD') or Yii::$app->user->can('Employee')) ?
                                    '<div class="row">' .
                                        '<div class="col-md-2">' .
                                            DatePicker::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'year',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'year-filter',
                                                    'placeholder' => 'Filter Tahun',
                                                    'class' => 'form-control additional-filter',
                                                ],
                                                'removeButton' => false,
                                                'pluginOptions' => [
                                                    'autoclose'=>true,
                                                    'startView'=>'year',
                                                    'minViewMode'=>'years',
                                                    'format' => 'yyyy',
                                                ]
                                            ]) .
                                        '</div>' .
                                        '<div class="col-md-2">' .
                                            Select2::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'okr_level_id',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'okr_level_id-filter',
                                                    'class' => 'form-control additional-filter',
                                                    'placeholder' => '-- Filter Level --',
                                                ],
                                                'pluginOptions' => ['allowClear' => true],
                                                'data' => $listLevel,
                                            ]) .
                                        '</div>' .
                                    '</div>
                                    <div class="row" style="margin-top: 10px;">' .
                                        '<div class="col-md-2">' .
                                            Select2::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'unit_id',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'divisi-filter',
                                                    'class' => 'form-control additional-filter',
                                                    'placeholder' => '-- Filter Unit Kerja --',
                                                ],
                                                'pluginOptions' => ['allowClear' => true],
                                                'data' => $listUnit,
                                            ]) .
                                        '</div>' .
                                        '<div class="col-md-2">' .
                                            Select2::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'position_structure_id',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'jabatan-filter',
                                                    'class' => 'form-control additional-filter',
                                                    'placeholder' => '-- Filter Jabatan --',
                                                ],
                                                'pluginOptions' => ['allowClear' => true],
                                                'data' => $listPosition,
                                            ]) .
                                        '</div>' .
                                        '<div class="col-md-2">' .
                                            Select2::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'child_of',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'turunan-jabatan-filter',
                                                    'class' => 'form-control additional-filter',
                                                    'placeholder' => '-- Filter Turunan dari Jabatan --',
                                                ],
                                                'pluginOptions' => ['allowClear' => true],
                                                'data' => $listPosition,
                                            ]) .
                                        '</div>' .
                                    '</div>'
                                    /* : '<div class="col-md-2">' .
                                        DatePicker::widget([
                                            'model' => $searchModel,
                                            'attribute' => 'year',
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'placeholder' => 'Filter Tahun',
                                                'class' => 'form-control additional-filter',
                                            ],
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'startView'=>'year',
                                                'minViewMode'=>'years',
                                                'format' => 'yyyy',
                                                'allowClear' => false
                                            ]
                                        ]) .
                                    '</div>' */,
                        'after'=>'',
                    ]
                ])?>
            </div>
        </div>
    </div>
    
</div>

<?= Html::beginForm(['download'], 'post', ['id' => 'download-form']) ?>
<div></div>
<?= Html::endForm() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<?php
    $jsReady = file_get_contents(__DIR__ . '/js/index.ready.js');
    $this->registerJs($jsReady, View::POS_READY);
?>