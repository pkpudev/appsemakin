<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Okr */

?>
<div class="okr-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelParent' => $modelParent,
    ]) ?>
</div>
