<?php

use app\models\Calculation;
use app\models\CascadingType;
use app\models\Controllability;
use app\models\Measure;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\Polarization;
use app\models\PositionStructure;
use app\models\UnitStructure;
use app\models\Validity;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Okr */
/* @var $form yii\widgets\ActiveForm */

if($model->parent_id && ($parent = Okr::findOne($model->parent_id))){

    if($model->position_structure_id){
        $positionList = ArrayHelper::map(PositionStructure::find()->where(['responsible_to_id' => $parent->id])->orderBy('position_name asc')->all(), 'id', function($model){
            return $model->year . ' - ' . $model->position_name;
        });
    } else {

        $unitIds = [];
        $units = UnitStructure::find()->where(['year' => $parent->year])->andWhere(['level' => '1'])->all();
        foreach($units as $unit){
            array_push($unitIds, $unit->id);
        }
        
        $positionList = ArrayHelper::map(PositionStructure::find()->where(['in', 'unit_structure_id', $unitIds])->orderBy('position_name asc')->all(), 'id', 'position_name');
    }

    $model->year = $parent->year;
} else {
    $positionList = ArrayHelper::map(PositionStructure::find()->orderBy('position_name asc')->all(), 'id', function($model){
        return $model->year . ' - ' . $model->position_name;
    });
}

if($modelParent){
    if($modelParent->type == 'O'){
        $cascadingType = ArrayHelper::map(CascadingType::find()->where('id in (1, 2)')->orderBy('id asc')->asArray()->all(), 'id', 'cascading_type');
    } else {
        $cascadingType = ArrayHelper::map(CascadingType::find()->where('id in (3)')->orderBy('id asc')->asArray()->all(), 'id', 'cascading_type');
    }
} else {
    $cascadingType = null;
}
?>

<div class="okr-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(!$modelParent): ?>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'year')->widget(DatePicker::classname(), [
                    'options' => ['id', 'placeholder' => 'Silahkan Pilih Tahun'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'todayHighlight' => true,
                        'startView'=>'year',
                        'minViewMode'=>'years',
                        'format' => 'yyyy'
                    ]
                ]) ?>
            </div>
            <div class="col-md-9">
                <?= $form->field($model, 'parent_id')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => [
                                    'placeholder' => 'Pilih Objective'
                                ],
                    'select2Options' => [
                        'initValueText' => $model->parent->okr,
                        'options' => [
                                        'value' => $model->parent_id
                                    ],
                        'pluginOptions' => [
                                                'allowClear' => true,
                                                'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                            ]
                    ],
                    'pluginOptions' => [
                        'depends' => ['okr-year'],
                        'url' => Url::to(['/strategic/okr/get-objectives']),
                    ]
                ]); ?>
            </div>
        </div>
    <?php else: ?>
        <label>Objective</label><br />
        <span><?= $modelParent->okr_code . ' - ' . $modelParent->okr; ?></span><br /><br />
    <?php endif; ?>
    
    <?= $form->field($model, 'okr')->textarea(['rows' => 2])->label('Key Result') ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'support_type')->dropDownList([1 => 'Epic', 2 => 'Enabler'], ['class' => 'form-control']) ?>
            <?php /* $form->field($model, 'support_type')->widget(Select2::classname(), [
                'data' => [1 => 'Epic', 2 => 'Enabler'],
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tipe --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); */ ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'polarization_id')->dropDownList(ArrayHelper::map(Polarization::find()->where(['is_active' => true])->asArray()->all(), 'id', 'polarization_type'), ['class' => 'form-control']) ?>
            <?php /* $form->field($model, 'polarization_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Polarization::find()->where(['is_active' => true])->asArray()->all(), 'id', 'polarization_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Polarization --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); */ ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'calculation_id')->dropDownList(ArrayHelper::map(Calculation::find()->where(['is_active' => true])->asArray()->all(), 'id', 'calculation_type'), ['class' => 'form-control']) ?>
            <?php /* $form->field($model, 'calculation_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Calculation::find()->where(['is_active' => true])->asArray()->all(), 'id', 'calculation_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Calculation --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); */ ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'validity_id')->dropDownList(ArrayHelper::map(Validity::find()->where(['is_active' => true])->asArray()->all(), 'id', 'validity_type'), ['class' => 'form-control']) ?>
            <?php /* $form->field($model, 'validity_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Validity::find()->where(['is_active' => true])->asArray()->all(), 'id', 'validity_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Validitas --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); */ ?>
        </div>
    </div>

    <hr style="margin: 5px 0;" />

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'controllability_id')->dropDownList(ArrayHelper::map(Controllability::find()->where(['is_active' => true])->asArray()->all(), 'id', 'degree_of_controllability'), ['class' => 'form-control']) ?>
            <?php /* $form->field($model, 'controllability_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Controllability::find()->where(['is_active' => true])->asArray()->all(), 'id', 'degree_of_controllability'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tingkat Pengendalian --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); */ ?>
        </div>
    </div>

    <hr style="margin: 5px 0;" />

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'target_q1')->textInput(['class' => 'tq form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q2')->textInput(['class' => 'tq form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q3')->textInput(['class' => 'tq form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q4')->textInput(['class' => 'tq form-control']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q1')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd',
                    'startDate' => $model->year . '-01-01',
                    'startView'=>'year',
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q2')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd',
                    'startDate' => $model->year . '-01-01',
                    'startView'=>'year',
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q3')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd',
                    'startDate' => $model->year . '-01-01',
                    'startView'=>'year',
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q4')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd',
                    'startDate' => $model->year . '-01-01',
                    'startView'=>'year',
                ]
            ]) ?>
        </div>
    </div>

    <hr style="margin: 5px 0;" />

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'target')->textInput(['id'=>'target', 'readonly' => '']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'measure')->dropDownList(ArrayHelper::map(Measure::find()->where(['is_active' => true])->orderBy('measure')->all(), 'measure', 'measure'), ['class' => 'form-control']) ?>
            <?php /* $form->field($model, 'measure')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Measure::find()->where(['is_active' => true])->asArray()->all(), 'measure', 'measure'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Satuan --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); */ ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php
$script = <<< JS

    jQuery('#okr-cascading_type_id').on('change', function(e){
        typeId = this.value;
        
        if(typeId == 1){
            $('#okr-place').hide();            
        } else {
            $('#okr-place').show();
        }
    })
        
JS;

$this->registerJs($script);
$this->registerJs(file_get_contents(__DIR__ . '/js/_form.js'));
?>
