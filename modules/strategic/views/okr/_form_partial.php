<?php

use app\models\Calculation;
use app\models\CascadingType;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\Polarization;
use app\models\PositionStructure;
use app\models\UnitStructure;
use app\models\Validity;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Okr */
/* @var $form yii\widgets\ActiveForm */

if($model->parent_id && ($parent = Okr::findOne($model->parent_id))){

    if($modelParent->position_structure_id){
        $positionList = ArrayHelper::map(PositionStructure::find()->where(['responsible_to_id' => $modelParent->position_structure_id])->orderBy('position_name asc')->all(), 'id', 'position_name');
    } else {

        $unitIds = [];
        $units = UnitStructure::find()->where(['year' => $parent->year])->andWhere(['level' => '1'])->all();
        foreach($units as $unit){
            array_push($unitIds, $unit->id);
        }
        
        $positionList = ArrayHelper::map(PositionStructure::find()->where(['in', 'unit_structure_id', $unitIds])->orderBy('position_name asc')->all(), 'id', 'position_name');
    }
} else {
    $positionList = ArrayHelper::map(PositionStructure::find()->orderBy('position_name asc')->all(), 'id', 'position_name');
}
?>

<div class="okr-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'position_structure_id')->widget(Select2::classname(), [
        'data' => $positionList,
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['prompt' => '-- Pilih Jabatan --'],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]); ?>

    <table class="table table-bordered table-striped table-condensed ">
        <?php 
            $krs = Okr::find()->where(['parent_id' => $modelParent->id])->andWhere(['type' => 'KR'])->all();
            if(!$krs) echo 'Tidak memiliki Key Result';

            $no = 1;
            foreach($krs as $kr):
        ?>
            <tr>
                <td width="30px" rowspan="3" align="center"><?= $form->field($model, 'krs[]')->checkbox(['label' => '', 'value' => $kr->id, 'uncheck' => null]); ?></td>
                <td width="40px" rowspan="3" align="center"><?= $no; ?></td>
                <td width="182.5px"><label>Key Result Code</label><br /><?= $kr->okr_code; ?></td>
                <td width="182.5px"><label>Key Result Description</label><br /><?= $kr->okr; ?></td>
                <td width="182.5px"><?= $form->field($model, 'kr_targets[' . $kr->id . ']')->textInput(['value' => $kr->target])->label('Target') ?></td>
                <td width="182.5px"><?= $form->field($model, 'kr_measures[' . $kr->id . ']')->textInput(['value' => $kr->measure])->label('Measure') ?></td>
            </tr>
            <tr>
                <td><?= $form->field($model, 'targets_q1[' . $kr->id . ']')->textInput(['value' => $kr->target_q1]) ?></td>
                <td><?= $form->field($model, 'targets_q2[' . $kr->id . ']')->textInput(['value' => $kr->target_q2]) ?></td>
                <td><?= $form->field($model, 'targets_q3[' . $kr->id . ']')->textInput(['value' => $kr->target_q3]) ?></td>
                <td><?= $form->field($model, 'targets_q4[' . $kr->id . ']')->textInput(['value' => $kr->target_q4]) ?></td>
            </tr>
            <tr>
                <td>
                    <?= $form->field($model, 'timebounds_q1[' . $kr->id . ']')->widget(DatePicker::classname(), [
                        'options' => [
                            'placeholder' => 'Silahkan Pilih Tanggal',
                            'value' => $kr->timebound_q1
                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'layout' => '{picker}{input}'
                    ]) ?>
                </td>
                <td>
                    <?= $form->field($model, 'timebounds_q2[' . $kr->id . ']')->widget(DatePicker::classname(), [
                        'options' => [
                            'placeholder' => 'Silahkan Pilih Tanggal',
                            'value' => $kr->timebound_q2
                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'layout' => '{picker}{input}'
                    ]) ?>
                </td>
                <td>
                    <?= $form->field($model, 'timebounds_q3[' . $kr->id . ']')->widget(DatePicker::classname(), [
                        'options' => [
                            'placeholder' => 'Silahkan Pilih Tanggal',
                            'value' => $kr->timebound_q3
                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'layout' => '{picker}{input}'
                    ]) ?>
                </td>
                <td>
                    <?= $form->field($model, 'timebounds_q4[' . $kr->id . ']')->widget(DatePicker::classname(), [
                        'options' => [
                            'placeholder' => 'Silahkan Pilih Tanggal',
                            'value' => $kr->timebound_q4
                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'layout' => '{picker}{input}'
                    ]) ?>
                </td>
            </tr>
        <?php $no++; endforeach; ?>
    </table>
        
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>