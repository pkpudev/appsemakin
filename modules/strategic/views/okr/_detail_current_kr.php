<?php

use app\models\Okr;
use kartik\grid\GridView;
use yii\helpers\Html;

?>

<?= GridView::widget([
    'id'=>'current-kr',
    'dataProvider' => $dataProviderCurrentKr,
    'layout' => '{items}{pager}',
    'pjax' => true,
    'emptyText' => 'Tidak ada data OKR.',
    'tableOptions' => [
        'class' => 'table table-responsive table-striped table-bordered'
    ],
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute' => 'parent_id',
            'label' => 'Unit',
            'value' => function($model){
                return $model->parent->position->unit->unit_name ?: 'Organisasi';
            },
            'group' => true
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute' => 'parent_id',
            'label' => 'Objective Code',
            'value' => function($model){
                return $model->parent->okr_code;
            },
            'width' => '120px',
            'group' => true,
            'subGroupOf' => 1
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute' => 'parent_id',
            'label' => 'Objective Description',
            'value' => function($model){
                return $model->parent->okr;
            },
            'width' => '200px',
            'group' => true,
            'subGroupOf' => 1
        ],
        [
            'attribute' => 'parent_id',
            'label' => 'Action',
            'value' => function($model){
                return /* Html::a('<span class="fa fa-circle"></span>', ['create-full', 'parent_id' => $model->parent_id], ['role' => 'modal-remote', 'title' => 'Create Full Cascading']) . '&nbsp;&nbsp;' .
                        Html::a('<span class="fa fa-pie-chart"></span>', ['create-partial', 'parent_id' => $model->parent_id], ['role' => 'modal-remote', 'title' => 'Create Partial Cascading']) . '<br />' . */
                        Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model->parent_id], ['role' => 'modal-remote', 'title' => 'Update Objective', 'style' => 'color: var(--warning) !important;']);
            },
            'format' => 'raw',
            'hAlign' => 'center',
            'group' => true,
            'subGroupOf' => 1
        ], 
        [
            'attribute' => 'okr_code',
            'label' => 'Key Result Code',
            'width' => '120px'
        ],
        [
            'attribute' => 'okr',
            'label' => 'Key Result Description',
            'width' => '200px',
        ],
        [
            'attribute' => 'id',
            'label' => 'Action',
            'value' => function($model){
                $check = Okr::find()->where(['parent_id' => $model->id])->one();
                return Html::a('<span class="fa fa-plus-circle"></span>', ['create-support', 'parent_id' => $model->id], ['role' => 'modal-remote', 'title' => 'Create Support Cascading']) . '<br />' .
                        Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model->id], ['role' => 'modal-remote', 'title' => 'Update Key Result', 'style' => 'color: var(--warning) !important;']) . '<br />' .
                        (!$check ? 
                        Html::a(
                            '<span class="fa fa-trash"></span>', 
                            ['delete', 'id' => $model->id], 
                            [
                                'role'=>'modal-remote','title'=>'Hapus Key Result Ini',
                                'data-confirm'=>false, 'data-method'=>false,
                                'data-request-method'=>'post',
                                'data-toggle'=>'tooltip',
                                'data-confirm-title'=>'Konfirmasi',
                                'data-confirm-message'=>'Yakin ingin menghapus Key Result ini?',
                                'style' => 'color: var(--danger) !important;'
                            ]) : '');
            },
            'hAlign' => 'center',
            'format' => 'raw',
        ], 
        [
            'attribute' => 'controllability_id',
            'value' => function($model){
                return ($model->controllability_id) ? $model->controll->degree_of_controllability : '-';
            }
        ],
        [
            'attribute' => 'support_type',
            'value' => function($model){
                return ($model->support_type == 1) ? 'Epic' : 'Enabler';
            }
        ],
        [
            'attribute' => 'polarization_id',
            'value' => function($model){
                return $model->polarization->polarization_type;
            }
        ],
        [
            'attribute' => 'calculation_id',
            'value' => function($model){
                return $model->calculation->calculation_type;
            }
        ],
        [
            'attribute' => 'validity_id',
            'value' => function($model){
                return $model->validity->validity_type;
            }
        ],
        [
            'attribute' => 'weight',
        ],
        [
            'attribute' => 'target',
            'label' => 'Target',
            'value' => function($model){
                return $model->target . ' ' . $model->measure;
            }
        ],
        [
            'attribute' => 'target_q1',
            'value' => function($model){
                return $model->target_q1 ?: '-';
            }
        ],
        [
            'attribute' => 'timebound_q1',
            'value' => function($model){
                return $model->timebound_q1 ?: '-';
            }
        ],
        [
            'attribute' => 'target_q2',
            'value' => function($model){
                return $model->target_q2 ?: '-';
            }
        ],
        [
            'attribute' => 'timebound_q2',
            'value' => function($model){
                return $model->timebound_q2 ?: '-';
            }
        ],
        [
            'attribute' => 'target_q3',
            'value' => function($model){
                return $model->target_q3 ?: '-';
            }
        ],
        [
            'attribute' => 'timebound_q3',
            'value' => function($model){
                return $model->timebound_q3 ?: '-';
            }
        ],
        [
            'attribute' => 'target_q4',
            'value' => function($model){
                return $model->target_q4 ?: '-';
            }
        ],
        [
            'attribute' => 'timebound_q4',
            'value' => function($model){
                return $model->timebound_q4 ?: '-';
            }
        ],
        [
            'attribute' => 'cascading_type_id',
            'value' => function($model){
                return $model->cascading_type_id ? $model->cascadingType->cascading_type: '-';
            }
        ],
    ],
])?>