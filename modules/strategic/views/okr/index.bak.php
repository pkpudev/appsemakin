<?php

use app\models\PositionStructure;
use app\models\UnitStructure;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;

$this->title = 'OKR';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="okr-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-plus"></i> Tambah OKR', ['create'],
                    ['role'=>'modal-remote','title'=> 'Tambah OKR','class'=>'btn btn-success'])
                ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Daftar OKR',
                'before'=> 
                            '<div class="row">' . 
                                '<div class="col-md-2">' .
                                    DatePicker::widget([
                                        'model' => $searchModel,
                                        'attribute' => 'year',
                                        'pjaxContainerId' => 'crud-datatable-pjax',
                                        'options' => [
                                            'id' => 'year-filter',
                                            'class' => 'form-control additional-filter',
                                            'placeholder' => 'Tahun',
                                            // 'onchange' => 'to($(this).val())',
                                            'value' => $year
                                        ],
                                        'pluginOptions' => [
                                            'autoclose'=>true,
                                            'startView'=>'year',
                                            'minViewMode'=>'years',
                                            'format' => 'yyyy'
                                        ]
                                    ]) .
                                '</div>' .
                                '<div class="col-md-2">' .
                                    Select2::widget([
                                        'model' => $searchModel,
                                        'attribute' => 'position_structure_id',
                                        'pjaxContainerId' => 'crud-datatable-pjax',
                                        'options' => [
                                            'id' => 'position_structure_id-filter',
                                            'class' => 'form-control additional-filter',
                                            'prompt' => '-- Pilih Struktur Jabatan --',
                                            // 'onchange' => 'refreshPage($(this).val())',
                                            'value' => $positionStrukturId
                                        ],
                                        'pluginOptions' => ['allowClear' => true],
                                        'data' => ArrayHelper::map(PositionStructure::find()->all(), 'id', 'position_name')
                                    ]) .
                                '</div>' .
                            '</div>'
                            ,
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>