<?php

use app\models\Calculation;
use app\models\CascadingType;
use app\models\OkrLevel;
use app\models\Polarization;
use app\models\PositionStructure;
use app\models\Validity;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Okr */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="okr-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'year')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
        'pluginOptions' => [
            'autoclose'=>true,
            'todayHighlight' => true,
            'startView'=>'year',
            'minViewMode'=>'years',
            'format' => 'yyyy'
        ]
    ]) ?>

    <?= $form->field($model, 'okr')->textarea(['rows' => 2])->label('Objective') ?>
      
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php
$script = <<< JS

    var numb = 1;
    var id = $("#serial").text();
    var link = "";

    document.getElementById("btn").addEventListener("click", function(event){
        event.preventDefault();
        $("#form_main").clone().insertAfter('#form_main');
        // document.getElementById("serial").innerHTML = numb;
        document.getElementById("action").innerHTML = "<button class='btn btn-danger btn-sm' id='"+numb+"' onclick='rem(this.id)'><span class='glyphicon glyphicon-trash'></span></button>";
        numb++;
        $("#form_main:last").find('input').val("");
    });

    function rem(id){
             $('#'+id).parent().parent().remove();
    }
        
JS;

$this->registerJs($script);
?>