<?php

use app\models\Okr;
use app\models\OkrLevel;
use app\models\PositionStructureEmployee;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\web\View;

$this->title = 'OKR - Organisasi';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>
    .btn-tab {
        border-color: var(--background) !important;
        color: var(--background) !important;
    }

    .btn-tab-active, .btn-tab:hover {
        background: var(--background) !important;
        color: #fff !important;
    }
</style>
<div class="okr-index">

    <div class="box box-default">
        <div class="box-body">
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'filterSelector' => '.additional-filter',
                    'showPageSummary' => true,
                    'pjax'=>true,
                    'emptyText' => 'Tidak ada data Key Result. Silahkan tambah Objective Organisasi terlebih dahulu lalu buat Key Result ' . 
                                    Html::a('disini', ['create-organization-key-result'], ['data-pjax' => 0, 'role' => 'modal-remote']) . '.',
                    'export' => [
                        'label' => 'Export Excel atau PDF',
                        'header' => '',
                        'options' => [
                            'class' => 'btn btn-primary'
                        ], 
                        'menuOptions' => [ 
                                'style' => 'background: var(--background); width: 187px;'
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => true,
                        GridView::EXCEL => true,
                    ],
                    'columns' => [
                        [
                            'class' => 'kartik\grid\SerialColumn',
                            'width' => '30px',
                        ],
                        [
                            'attribute' => 'perspective_id',
                            'value' => function($model){
                                return $model->perspective_id ? $model->perspective->perspective: '-';
                            },
                            'filterType'=>GridView::FILTER_SELECT2,
                            'filter'=>ArrayHelper::map(app\models\Perspective::find()->orderBy('id')->asArray()->all(), 'id', 'perspective'), 
                            'filterWidgetOptions'=>[
                                'pluginOptions'=>['allowClear'=>true],
                            ],
                            'filterInputOptions'=>['placeholder'=>''],
                            'group' => true,
                        ],
                        [
                            'label' => 'Periode',
                            'value' => function($model){
                                return $model->start_year . ' - ' . $model->end_year;
                            },
                            'width' => '120px',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'parent_okr_code',
                            'label' => 'Objective Code',
                            'value' => function($model){
                                return $model->parent->okr_code;
                            },
                            'width' => '120px',
                            'group' => true,
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'parent_okr',
                            'label' => 'Objective',
                            'value' => function($model){
                                return $model->parent->okr;
                            },
                            'width' => '200px',
                            'group' => true,
                            'subGroupOf' => 2
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            // 'attribute' => 'parent_weight',
                            'label' => 'Bobot Objective (%)',
                            'value' => function($model){
                                return $model->parent->weight ?: '-';
                            },
                            'group' => true,
                            'subGroupOf' => 3
                        ],
                        [
                            'label' => 'Action',
                            'value' => function($model){
                                return 
                                        Html::a('<span class="fa fa-pencil"></span>', ['update-organization-objective', 'id' => $model->parent_id], ['role' => 'modal-remote', 'title' => 'Update Objective', 'style' => 'color: var(--warning) !important;']) .
                                        '&nbsp;&nbsp;' .
                                        Html::a('<span class="fa fa-plus"></span>', ['create-organization-key-result', 'parent_id' => $model->parent_id], ['role' => 'modal-remote', 'title' => 'Tambah Key Result', 'style' => 'color: var(--success) !important;'])
                                       
                                        /* (!$check ? 
                                        Html::a(
                                            '<span class="fa fa-trash"></span>', 
                                            ['delete', 'id' => $model->id], 
                                            [
                                                'role'=>'modal-remote','title'=>'Hapus Key Result Ini',
                                                'data-confirm'=>false, 'data-method'=>false,
                                                'data-request-method'=>'post',
                                                'data-toggle'=>'tooltip',
                                                'data-confirm-title'=>'Konfirmasi',
                                                'data-confirm-message'=>'Yakin ingin menghapus Key Result ini?',
                                                'style' => 'color: var(--danger) !important;'
                                            ]) : '') */;
                            },
                            'width' => '50px',
                            'group' => true,
                            'hAlign' => 'center',
                            'format' => 'raw',
                            'subGroupOf' => 3
                        ],
                        [
                            'attribute' => 'okr_code',
                            'label' => 'Key Result Code',
                            'width' => '120px'
                        ],
                        [
                            'attribute' => 'okr',
                            'label' => 'Key Result',
                            'width' => '200px',
                        ],
                        [
                            'label' => 'Action',
                            'value' => function($model){
                                // $check = Okr::find()->where(['parent_id' => $model->id])->one();
                                $check = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['position_structure_id' => $model->id])->one();
                                return //Html::a('<span class="fa fa-plus-circle"></span>', ['create-support', 'parent_id' => $model->id], ['role' => 'modal-remote', 'title' => 'Create Support Cascading']) . '<br />' .
                                        ($check || Yii::$app->user->can('Admin')) ?
                                        Html::a('<span class="fa fa-pencil"></span>', ['update-organization-key-result', 'id' => $model->id], ['role' => 'modal-remote', 'title' => 'Update Key Result', 'style' => 'color: var(--warning) !important;'])
                                        : '-';
                                        /* (!$check ? 
                                        Html::a(
                                            '<span class="fa fa-trash"></span>', 
                                            ['delete', 'id' => $model->id], 
                                            [
                                                'role'=>'modal-remote','title'=>'Hapus Key Result Ini',
                                                'data-confirm'=>false, 'data-method'=>false,
                                                'data-request-method'=>'post',
                                                'data-toggle'=>'tooltip',
                                                'data-confirm-title'=>'Konfirmasi',
                                                'data-confirm-message'=>'Yakin ingin menghapus Key Result ini?',
                                                'style' => 'color: var(--danger) !important;'
                                            ]) : '') */;
                            },
                            'hAlign' => 'center',
                            'format' => 'raw',
                            /* 'visible' => function($model){
                                $check = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['position_structure_id' => $model->id])->one();
                                return $check || Yii::$app->user->can('Admin');
                            } */
                        ], 
                        [
                            'attribute' => 'portfolio_type_id',
                            'value' => function($model){
                                return $model->portfolio_type_id ? $model->portfolio->portfolio_type: '';
                            },
                            'filterType'=>GridView::FILTER_SELECT2,
                            'filter'=>ArrayHelper::map(app\models\PortfolioType::find()->orderBy('id')->asArray()->all(), 'id', 'portfolio_type'), 
                            'filterWidgetOptions'=>[
                                'pluginOptions'=>['allowClear'=>true],
                            ],
                            'filterInputOptions'=>['placeholder'=>''],
                        ],
                        [
                            'attribute' => 'target',
                            'value' => function($model){
                                return ($model->target ? number_format($model->target, 0, ',', '.') : '') . ' ' . $model->measure;
                            }
                        ],
                        [
                            'attribute' => 'weight',
                            'value' => function($model){
                                return $model->weight ?: '';
                            },
                            'pageSummary' => true
                        ],
                        [
                            'label' => 'Alat Verifikasi',
                            'format' => 'raw',
                            'value' => function($model){
                                $okr = Okr::findOne(['okr_org_id' => $model->id, 'okr_level_id' => OkrLevel::ORGANIZATION]);

                                if($okr)
                                return 
                                    implode('<br/>', \yii\helpers\ArrayHelper::map($okr->movs, 'id', function ($model) {
                                        return '&#8226; ' . $model->mov_name;
                                    })) . '<hr style="margin: 5px;" />' .
                                    '<center>' . Html::a('<span class="fa fa-pencil"></span> Ubah', ['movs', 'id' => $okr->id], ['class' => 'btn btn-warning btn-xs', 'role' => 'modal-remote']) . '</center>';
                            }
                        ],
                    ],
                    'toolbar'=> [
                        ['content'=>
                            Html::a('<i class="glyphicon glyphicon-plus"></i> Tambah Objective Organisasi', ['create-organization-objective'],
                            ['role'=>'modal-remote', 'class'=>'btn btn-success', 'title'=>'Tambah Objective Organisasi'])
                        ],
                        ['content'=>
                            Html::a('<i class="glyphicon glyphicon-plus"></i> Tambah Key Result Organisasi', ['create-organization-key-result'],
                            ['role'=>'modal-remote', 'class'=>'btn btn-warning', 'title'=>'Tambah Key Result Organisasi'])
                        ],
                        ['content'=>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                            '{toggleData}' .
                            '{export}'/*  .
                            Html::a(
                                '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                                '#',
                                ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Download Excel']
                            ) */
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'type' => 'primary',
                        'heading' => '<i class="fa fa-list-alt"></i> Daftar OKR',
                        'before' => //(Yii::$app->user->can('Admin') or Yii::$app->user->can('BOD') or Yii::$app->user->can('Employee')) ?
                                    '<div class="row">' .
                                        '<div class="col-md-2">' .
                                            DatePicker::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'start_year',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'start_year-filter',
                                                    'placeholder' => 'Filter Tahun',
                                                    'class' => 'form-control additional-filter',
                                                ],
                                                'pluginOptions' => [
                                                    'autoclose'=>true,
                                                    'startView'=>'year',
                                                    'minViewMode'=>'years',
                                                    'format' => 'yyyy',
                                                    'allowClear' => true
                                                ]
                                            ]) .
                                        '</div>' .
                                        '<div class="col-md-2">' .
                                            DatePicker::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'end_year',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'end_year-filter',
                                                    'placeholder' => 'Filter Tahun',
                                                    'class' => 'form-control additional-filter',
                                                ],
                                                'pluginOptions' => [
                                                    'autoclose'=>true,
                                                    'startView'=>'year',
                                                    'minViewMode'=>'years',
                                                    'format' => 'yyyy',
                                                    'allowClear' => true
                                                ]
                                            ]) .
                                        '</div>' .
                                    '</div>'
                                    /* : '<div class="col-md-2">' .
                                        DatePicker::widget([
                                            'model' => $searchModel,
                                            'attribute' => 'year',
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'placeholder' => 'Filter Tahun',
                                                'class' => 'form-control additional-filter',
                                            ],
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'startView'=>'year',
                                                'minViewMode'=>'years',
                                                'format' => 'yyyy',
                                                'allowClear' => false
                                            ]
                                        ]) .
                                    '</div>' */,
                        'after'=>'',
                    ]
                ])?>
            </div>
        </div>
    </div>
    
</div>

<?= Html::beginForm(['download'], 'post', ['id' => 'download-form']) ?>
<div></div>
<?= Html::endForm() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<?php
    $jsReady = file_get_contents(__DIR__ . '/js/index.ready.js');
    $this->registerJs($jsReady, View::POS_READY);
?>