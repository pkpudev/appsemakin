<?php

use kartik\grid\GridView;
use yii\helpers\Html;

?>

<?= GridView::widget([
    'id'=>'current-kr',
    'dataProvider' => $dataProviderCurrentKr,
    'pjax'=>true,
    'export' => [
        'label' => 'Export Excel atau PDF',
        'header' => '',
        'options' => [
            'class' => 'btn btn-primary'
        ], 
        'menuOptions' => [ 
                'style' => 'background: var(--background); width: 187px;'
        ]
    ],
    'exportConfig' => [
        GridView::PDF => true,
        GridView::EXCEL => true,
    ],
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'label' => 'Unit'
        ],
        [
            'attribute' => 'parent_id'
        ],
        [
            'attribute' => 'okr_code'
        ],
        [
            'attribute' => 'okr'
        ],
    ],
    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
            '{toggleData}'
        ],
        ['content'=> '{export}'],
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'panel' => [
        'type' => 'primary',
        'heading' => '<i class="fa fa-list-alt"></i> Daftar OKR',
        'before'=> '',
        'after'=>'',
    ]
])?>