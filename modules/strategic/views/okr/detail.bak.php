
            
    <table class="kv-grid-table table table-bordered table-striped table-condensed kv-table-wrap">
        <tr>
            <td width="200px"><b>Code</b></td>
            <td><?= $model->okr_code ?: '-'; ?></td>
        </tr>
        <tr>
            <td><b><?= $model->type == 'O' ? 'Objective Description' : 'Key Result Description'; ?><b></td>
            <td><?= $model->okr ?: '-'; ?></td>
        </tr>
    </table>
    <table class="table table-bordered table-striped table-condensed text-center">
        <tr>
            <th>Support</th>
            <th>Polarization</th>
            <th>Calculation</th>
            <th>Validity Type</th>
            <th>Weight</th>
            <th>Measure</th>
        </tr>
        <tr>
            <td><?= ($model->support_type == 1) ? 'Epic' : 'Enabler'; ?></td>
            <td><?= $model->polarization_id ? $model->polarization->polarization_type : '-'; ?></td>
            <td><?= $model->calculation_id ? $model->calculation->calculation_type : '-'; ?></td>
            <td><?= $model->validity_id ? $model->validity->validity_type : '-'; ?></td>
            <td><?= $model->weight ?: '-'; ?></td>
            <td><?= $model->measure ?: '-'; ?></td>
        </tr>
    </table>
    <table class="table table-bordered table-striped table-condensed text-center">
        <tr>
            <th colspan="8">Target</th>
        </tr>
        <tr>
            <th colspan="2">Quartal 1</th>
            <th colspan="2">Quartal 2</th>
            <th colspan="2">Quartal 3</th>
            <th colspan="2">Quartal 4</th>
        </tr>
        <tr>
            <th>Value</th>
            <th>Time</th>
            <th>Value</th>
            <th>Time</th>
            <th>Value</th>
            <th>Time</th>
            <th>Value</th>
            <th>Time</th>
        </tr>
        <tr>
            <td><?= $model->target_q1 ?: '-'; ?></td>
            <td><?= $model->timebound_q1 ?: '-'; ?></td>
            <td><?= $model->target_q2 ?: '-'; ?></td>
            <td><?= $model->timebound_q2 ?: '-'; ?></td>
            <td><?= $model->target_q3 ?: '-'; ?></td>
            <td><?= $model->timebound_q3 ?: '-'; ?></td>
            <td><?= $model->target_q4 ?: '-'; ?></td>
            <td><?= $model->timebound_q4 ?: '-'; ?></td>
        </tr>
    </table>

    <div style="border: 5px solid var(--background); width: 100%; padding: 10px; border-radius: 5px; margin-top: 5px;">
        <label>OKR 1 Level Diatas</label>
        <?php
            if($thisModelParent = Okr::findOne($model->parent_id)){
        ?>
            <table class="kv-grid-table table table-bordered table-striped table-condensed kv-table-wrap">
                <tr>
                    <td width="200px"><b>Code</b></td>
                    <td><?= $thisModelParent->okr_code ?: '-'; ?></td>
                </tr>
                <tr>
                    <td><b><?= $thisModelParent->type == 'O' ? 'Objective Description' : 'Key Result Description'; ?><b></td>
                    <td><?= $thisModelParent->okr ?: '-'; ?></td>
                </tr>
            </table>
        <?php } else { ?>
            <br /><i>Tidak ada OKR diatas OKR ini.</i>
        <?php } ?>
    </div>
    <div style="border: 5px solid var(--background); width: 100%; padding: 10px; border-radius: 5px; margin-top: 5px;">
        <label>OKR 1 Level Dibawah</label>
        <?php
            if($thisModelChilds = Okr::find()->where(['parent_id' => $model->id])->orderBy('type desc')->all()){
        ?>
            <table class="kv-grid-table table table-bordered table-striped table-condensed kv-table-wrap">
                <tr>
                    <th>Code</th>
                    <th>Type</th>
                    <th>Description</th>
                </tr>
                <?php foreach($thisModelChilds as $mc): ?>
                    <tr>
                        <td><?= $mc->okr_code ?: '-'; ?></td>
                        <td><?= $mc->type == 'O' ? 'Objective' : 'Key Result'; ?></td>
                        <td><?= $mc->okr ?: '-'; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php } else { ?>
            <br /><i>Tidak ada OKR dibawah OKR ini.</i>
        <?php } ?>
    </div>