<?php

use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use yii\helpers\Html;

?>
<div class="organization-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'start_year')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tahun Mulai'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'end_year')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tahun Mulai'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
    </div>


    <?= $form->field($model, 'okr')->textarea(['rows' => 2])->label('Objective') ?>

    <?= $form->field($model, 'weight')->textInput()->label('Bobot Objective (%)') ?>
      
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>