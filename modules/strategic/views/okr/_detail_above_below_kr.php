<?php

use kartik\grid\GridView;
use yii\helpers\Html;

?>

<?= GridView::widget([
    'id'=>'above-below-kr',
    'dataProvider' => $dataProvider,
    'layout' => '{items}{pager}',
    'pjax' => true,
    'emptyText' => 'Tidak ada data OKR.',
    'tableOptions' => [
        'class' => 'table table-responsive table-striped table-bordered'
    ],
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute' => 'parent_id',
            'label' => 'Unit',
            'value' => function($model){
                return $model->parent->position->unit->unit_name ?: 'Organisasi';
            },
            'group' => true
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute' => 'parent_id',
            'label' => 'Objective Code',
            'value' => function($model){
                return $model->parent->okr_code;
            },
            'width' => '120px',
            'group' => true
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute' => 'parent_id',
            'label' => 'Objective Description',
            'value' => function($model){
                return $model->parent->okr;
            },
            'width' => '200px',
            'group' => true
        ],
        [
            'attribute' => 'okr_code',
            'label' => 'Key Result Code',
            'width' => '120px'
        ],
        [
            'attribute' => 'okr',
            'label' => 'Key Result Description',
            'width' => '200px',
        ],
        [
            'attribute' => 'weight',
        ],
        [
            'attribute' => 'target',
            'label' => 'Target',
            'value' => function($model){
                return $model->weight . ' ' . $model->measure;
            }
        ],
        [
            'attribute' => 'target_q1',
            'value' => function($model){
                return $model->target_q1 ?: '-';
            }
        ],
        [
            'attribute' => 'timebound_q1',
            'value' => function($model){
                return $model->timebound_q1 ?: '-';
            }
        ],
        [
            'attribute' => 'target_q2',
            'value' => function($model){
                return $model->target_q2 ?: '-';
            }
        ],
        [
            'attribute' => 'timebound_q2',
            'value' => function($model){
                return $model->timebound_q2 ?: '-';
            }
        ],
        [
            'attribute' => 'target_q3',
            'value' => function($model){
                return $model->target_q3 ?: '-';
            }
        ],
        [
            'attribute' => 'timebound_q3',
            'value' => function($model){
                return $model->timebound_q3 ?: '-';
            }
        ],
        [
            'attribute' => 'target_q4',
            'value' => function($model){
                return $model->target_q4 ?: '-';
            }
        ],
        [
            'attribute' => 'timebound_q4',
            'value' => function($model){
                return $model->timebound_q4 ?: '-';
            }
        ],
        [
            'attribute' => 'cascading_type_id',
            'value' => function($model){
                return $model->cascading_type_id ? $model->cascadingType->cascading_type: '-';
            }
        ],
    ],
])?>