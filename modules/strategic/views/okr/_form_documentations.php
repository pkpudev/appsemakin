<?php

use kartik\file\FileInput;
use kartik\form\ActiveForm;

?>
<div class="add-file">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mov_name')->textInput(); ?>

    <?php ActiveForm::end(); ?>

</div>

