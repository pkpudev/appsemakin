<?php

use app\models\Calculation;
use app\models\CascadingType;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\Polarization;
use app\models\PositionStructure;
use app\models\UnitStructure;
use app\models\Validity;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Okr */
/* @var $form yii\widgets\ActiveForm */


if($modelParent->parent_id && ($parent = Okr::findOne($modelParent->parent_id))){
    if($modelParent->position_structure_id){

        // var_dump($modelParent->position_structure_id);die;

        $units = Yii::$app->db->createCommand("SELECT id FROM management.unit_structure WHERE superior_unit_id = {$parent->position->unit_structure_id}")->queryColumn();
        // var_dump($units);die;

        $positionList = ArrayHelper::map(PositionStructure::find()->where(['unit_structure_id' => $units])->andWhere(['level' => [4, 5, 6]])->orderBy('position_name asc')->all(), 'id', 'position_name');
    } else {

        $unitIds = [];
        $units = UnitStructure::find()->where(['year' => $parent->year])->andWhere(['level' => '1'])->all();
        foreach($units as $unit){
            array_push($unitIds, $unit->id);
        }
        
        $positionList = ArrayHelper::map(PositionStructure::find()->where(['in', 'unit_structure_id', $unitIds])->orderBy('position_name asc')->all(), 'id', 'position_name');
    }
} else {
    $positionList = ArrayHelper::map(PositionStructure::find()->orderBy('position_name asc')->all(), 'id', 'position_name');
}

if($modelParent){
    if($modelParent->type == 'O'){
        $cascadingType = ArrayHelper::map(CascadingType::find()->where('id in (1, 2)')->orderBy('id asc')->asArray()->all(), 'id', 'cascading_type');
    } else {
        $cascadingType = ArrayHelper::map(CascadingType::find()->where('id in (3)')->orderBy('id asc')->asArray()->all(), 'id', 'cascading_type');
    }
} else {
    $cascadingType = null;
}
?>

<div class="okr-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'position_structure_id')->widget(Select2::classname(), [
        'data' => $positionList,
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['prompt' => '-- Pilih Jabatan --'],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            'multiple' => true
            ]
    ])->label('Diberikan kepada'); ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
