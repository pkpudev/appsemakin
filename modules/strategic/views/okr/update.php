<?php

use app\models\Calculation;
use app\models\CascadingType;
use app\models\Controllability;
use app\models\Measure;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\Polarization;
use app\models\PositionStructure;
use app\models\UnitStructure;
use app\models\Validity;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Okr */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="okr-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <label>Objective</label><br />
    <span><?= $model->parent->okr_code . ' - ' . $model->parent->okr; ?></span><br /><br />
        
    <?= $form->field($model, 'okr')->textarea(['rows' => 2])->label('Key Result') ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'support_type')->widget(Select2::classname(), [
                'data' => [1 => 'Epic', 2 => 'Enabler'],
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tipe --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'polarization_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Polarization::find()->where(['is_active' => true])->asArray()->all(), 'id', 'polarization_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Polarization --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'calculation_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Calculation::find()->where(['is_active' => true])->asArray()->all(), 'id', 'calculation_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Polarization --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'validity_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Validity::find()->where(['is_active' => true])->asArray()->all(), 'id', 'validity_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Polarization --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <hr style="margin: 5px 0;" />

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'controllability_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Controllability::find()->where(['is_active' => true])->asArray()->all(), 'id', 'degree_of_controllability'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tingkat Pengendalian --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <hr style="margin: 5px 0;" />

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'target_q1')->textInput(['class' => 'tq form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q2')->textInput(['class' => 'tq form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q3')->textInput(['class' => 'tq form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q4')->textInput(['class' => 'tq form-control']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q1')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q2')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q3')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q4')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
    </div><hr style="margin: 5px 0;" />

    <div class="row">
        <div class="col-md-6">
        <?= $form->field($model, 'target')->textInput(['id'=>'target', 'readonly' => '']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'measure')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Measure::find()->where(['is_active' => true])->asArray()->all(), 'measure', 'measure'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Satuan --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php
/* $script = <<< JS

    jQuery('#okr-cascading_type_id').on('change', function(e){
        typeId = this.value;
        
        if(typeId == 1){
            $('#okr-place').hide();            
        } else {
            $('#okr-place').show();
        }
    })
        
JS; */

$this->registerJs($script);
$this->registerJs(file_get_contents(__DIR__ . '/js/_form.js'));
?>
