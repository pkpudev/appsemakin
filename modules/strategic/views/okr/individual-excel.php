<?php

use app\models\Calculation;
use app\models\CascadingType;
use app\models\OkrLevel;
use app\models\Polarization;
use app\models\PositionStructure;
use app\models\Validity;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Okr */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="okr-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'year')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'unit_structure_id')->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => [
                                'placeholder' => 'Pilih Unit'
                            ],
                'select2Options' => [
                    'pluginOptions' => [
                                            'allowClear' => true,
                                            'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                        ]
                ],
                'pluginOptions' => [
                    'depends' => ['okr-year'],
                    'url' => Url::to(['/leadership/structure/get-unit']),
                ]
            ])->label('Unit'); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'position_structure_id')->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => [
                                'placeholder' => 'Pilih Jabatan'
                            ],
                'select2Options' => [
                    'pluginOptions' => [
                                            'allowClear' => true,
                                            'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                        ]
                ],
                'pluginOptions' => [
                    'depends' => ['okr-unit_structure_id'],
                    'url' => Url::to(['/leadership/structure/get-positions']),
                ]
            ])->label('Jabatan'); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'pic_id')->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => [
                                'placeholder' => 'Pilih PIC'
                            ],
                'select2Options' => [
                    'pluginOptions' => [
                                            'allowClear' => true,
                                            'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                        ]
                ],
                'pluginOptions' => [
                    'depends' => ['okr-position_structure_id'],
                    'url' => Url::to(['/leadership/structure/get-persons']),
                ]
            ]); ?>
        </div>
    </div>
    
    <?= $form->field($model, 'files')->widget(FileInput::classname(), [
        'options' => ['multiple' => false],    
        'pluginOptions' => [
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => false,
            'showUpload' => false
        ]
    ]); ?>

    <div style="border: 1px solid #eee;padding: 5px;border-radius: 5px;">
        <b>Hal-hal yang harus diperhatikan sebelum upload excel: </b><br />
        1. Pastikan OKR dimulai di baris ke-17.<br />
        2. Objective Code di kolom A.<br />
        3. Key Result Code di kolom C.<br />
        4. Value/Target di kolom F.<br />
        5. Measure di kolom G.<br />
        6. Value Q1 di kolom H.<br />
        7. Value Q2 di kolom J.<br />
        8. Value Q3 di kolom L.<br />
        9. Value Q4 di kolom N.<br />
        10. Pastikan kolom Value berisi Angka.<br />
        11. Pastikan tidak ada baris yang kosong antara OKR.
    </div>
    


      
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php
$script = <<< JS

    var numb = 1;
    var id = $("#serial").text();
    var link = "";

    document.getElementById("btn").addEventListener("click", function(event){
        event.preventDefault();
        $("#form_main").clone().insertAfter('#form_main');
        // document.getElementById("serial").innerHTML = numb;
        document.getElementById("action").innerHTML = "<button class='btn btn-danger btn-sm' id='"+numb+"' onclick='rem(this.id)'><span class='glyphicon glyphicon-trash'></span></button>";
        numb++;
        $("#form_main:last").find('input').val("");
    });

    function rem(id){
             $('#'+id).parent().parent().remove();
    }
        
JS;

$this->registerJs($script);
?>