<?php

use app\models\Okr;
use app\models\OkrLevel;
use app\models\PositionStructureEmployee;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\web\View;

$this->title = 'OKR - Tabel';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>

    .btn-tab {
        border-color: var(--background) !important;
        color: var(--background) !important;
    }

    .btn-tab-active, .btn-tab:hover {
        background: var(--background) !important;
        color: #fff !important;
    }
</style>
<div class="okr-index">

    <div class="box box-default" style="width: 710px; margin-bottom: -5px;">
        <div class="box-body">
            <div class="btn-group">
                <?= Html::a('OKR Organisasi', ['organization'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
                <?php // Html::a('Lihat OKR dalam format List', ['index'], ['class' => 'btn btn-default btn-tab btn-sm']); ?>
                <?= Html::a('Lihat OKR dalam format Tabel', ['#'], ['class' => 'btn btn-default btn-tab btn-tab-active btn-sm']); ?>
                <?= Html::a('OKR Individu', ['individual'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
            </div>
        </div>
    </div>


    <div class="box box-default">
        <div class="box-body">
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'filterSelector' => '.additional-filter',
                    'pjax'=>true,
                    'export' => [
                        'label' => 'Export Excel atau PDF',
                        'header' => '',
                        'options' => [
                            'class' => 'btn btn-primary'
                        ], 
                        'menuOptions' => [ 
                                'style' => 'background: var(--background); width: 187px;'
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => true,
                        GridView::EXCEL => true,
                    ],
                    'columns' => [
                        [
                            'class' => 'kartik\grid\SerialColumn',
                            'width' => '30px',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            // 'attribute' => 'parent_id',
                            'label' => 'Unit',
                            'value' => function($model){
                                return $model->unit_name ?: 'Organisasi';
                            },
                            'group' => true,
                            'groupedRow' => true,                    // move grouped column to a single grouped row
                            'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
                            'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'okr_code',
                            'label' => 'Objective Code',
                            'value' => function($model){
                                return $model->okr_code;
                            },
                            'width' => '120px',
                            'group' => true,
                            'subGroupOf' => 1
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'okr',
                            'label' => 'Objective',
                            'format' => 'raw',
                            'value' => function($model){
                                return 
                                        $model->okr . '<br /><br />' . 
                                        ($model->okr_level_id <> OkrLevel::ORGANIZATION ? Html::a('<span class="fa fa-plus"></span> Tambah Key Result', ['create', 'parent_id' => $model->id], ['class' => 'btn btn-success btn-xs', 'role' => 'modal-remote']) : '');
                            },
                            'width' => '200px',
                            'group' => true,
                            'subGroupOf' => 1
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_okr_code',
                            'label' => 'Key Result Code',
                            'value' => function($model){
                                return $model->child_okr_code ?: '-';
                            },
                            'width' => '120px',
                            'group' => true,
                            'subGroupOf' => 3
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_okr',
                            'label' => 'Key Result',
                            'value' => function($model){
                                return $model->child_okr ?: '-';
                            },
                            'width' => '200px',
                            'group' => true,
                            'subGroupOf' => 3
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'label' => 'Action',
                            'value' => function($model){
                                $check1 = Okr::find()->where(['parent_id' => $model->child_id])->one();
                                $check2 = $model->pic_id == Yii::$app->user->id;
                                return 
                                        (($check2 || Yii::$app->user->can('Admin')) && $model->child_id ?
                                        Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model->child_id], ['role' => 'modal-remote', 'title' => 'Update Key Result', 'style' => 'color: var(--warning) !important;'])
                                        : '') . 
                                        (!$check1 && $model->child_id ? 
                                        Html::a(
                                            '&nbsp;<span class="fa fa-trash"></span>', 
                                            ['delete', 'id' => $model->child->id], 
                                            [
                                                'role'=>'modal-remote','title'=>'Hapus Key Result Ini',
                                                'data-confirm'=>false, 'data-method'=>false,
                                                'data-request-method'=>'post',
                                                'data-toggle'=>'tooltip',
                                                'data-confirm-title'=>'Konfirmasi',
                                                'data-confirm-message'=>'Yakin ingin menghapus Key Result ini?',
                                                'style' => 'color: var(--danger) !important;'
                                            ]) : '') . '<br />' .
                                            // Html::a('<span class="fa fa-circle"></span>', ['create-full', 'parent_id' => $model->parent_id], ['role' => 'modal-remote', 'title' => 'Create Full Cascading']) . '&nbsp;' .
                                            // Html::a('<span class="fa fa-pie-chart"></span>', ['create-partial', 'parent_id' => $model->parent_id], ['role' => 'modal-remote', 'title' => 'Create Partial Cascading']) . '<br />' . 
                                            ($model->child_id ? Html::a('<span class="fa fa-plus-circle"></span>', ['create-support', 'parent_id' => $model->child_id], ['role' => 'modal-remote', 'title' => 'Create Support Cascading']) : '') ;
                            },
                            'hAlign' => 'center',
                            'format' => 'raw',
                            'group' => true,
                            'subGroupOf' => 4
                            /* 'visible' => function($model){
                                $check = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['position_structure_id' => $model->id])->one();
                                return $check || Yii::$app->user->can('Admin');
                            } */
                        ], 
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_weight',
                            'label' => 'Bobot',
                            'value' => function($model){
                                return $model->child_weight ?: '';
                            },
                            'group' => true,
                            'subGroupOf' => 4
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_target_q1',
                            'label' => 'Target Kuartal 1',
                            'value' => function($model){
                                return $model->child_target_q1 ? $model->child_target_q1 . ' ' . $model->child_measure: '-';
                            },
                            'group' => true,
                            'subGroupOf' => 4
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_timebound_q1',
                            'label' => 'Kuartal 1',
                            'value' => function($model){
                                return $model->child_timebound_q1 ?: '-';
                            },
                            'group' => true,
                            'subGroupOf' => 4
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_target_q2',
                            'label' => 'Target Kuartal 2',
                            'value' => function($model){
                                return $model->child_target_q2 ? $model->child_target_q2 . ' ' . $model->child_measure: '-';
                            },
                            'group' => true,
                            'subGroupOf' => 4
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_timebound_q2',
                            'label' => 'Kuartal 2',
                            'value' => function($model){
                                return $model->child_timebound_q2 ?: '-';
                            },
                            'group' => true,
                            'subGroupOf' => 4
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_target_q3',
                            'label' => 'Target Kuartal 3',
                            'value' => function($model){
                                return $model->child_target_q3 ? $model->child_target_q3 . ' ' . $model->child_measure: '-';
                            },
                            'group' => true,
                            'subGroupOf' => 4
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_timebound_q3',
                            'label' => 'Kuartal 3',
                            'value' => function($model){
                                return $model->child_timebound_q3 ?: '-';
                            },
                            'group' => true,
                            'subGroupOf' => 4
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_target_q4',
                            'label' => 'Target Kuartal 4',
                            'value' => function($model){
                                return $model->child_target_q4 ? $model->child_target_q4 . ' ' . $model->child_measure: '-';
                            },
                            'group' => true,
                            'subGroupOf' => 4
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'child_timebound_q4',
                            'label' => 'Kuartal 4',
                            'value' => function($model){
                                return $model->child_timebound_q4 ?: '-';
                            },
                            'group' => true,
                            'subGroupOf' => 4
                        ],
                        // [
                        //     'attribute' => 'cascading_type_id',
                        //     'value' => function($model){
                        //         return $model->child->cascading_type_id ? $model->child->cascadingType->cascading_type: '-';
                        //     }
                        // ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'label'=>'Lengkap?',
                            'attribute' => 'is_completed',
                            'format' => 'raw',
                            'value' => function($model){
                                $check = !$model->child_polarization_id || !$model->child_calculation_id || !$model->child_validity_id || !$model->child_controllability_id;
                                
                                return $check ? '<span class="fa fa-info-circle" style="color: var(--warning);" title="Harus Dilengkapi"></span>' : '<span class="fa fa-check" style="color: var(--success);" title="Oke"></span>';
                            },
                            'filterType'=>GridView::FILTER_SELECT2,
                            'filter'=>[1 => 'Lengkap', 2 => 'Belum Lengkap'],
                            'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
                            'filterInputOptions'=>['placeholder'=>''],
                            'hAlign' => 'center',
                            'group' => true,
                            'subGroupOf' => 4
                        ],
                    ],
                    'toolbar'=> [
                        [
                            'content' => Html::a('<i class="fa fa-plus"></i> Tambah OKR Organisasi', ['import-okr-org'],
                                                ['data-pjax'=>0, 'class'=>'btn btn-success']) .
                                        Html::a('<i class="fa fa-plus"></i> Tambah Key Result', ['create'],
                                                ['role'=>'modal-remote', 'class'=>'btn btn-warning']) 
                        ],
                        ['content'=>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                            '{toggleData}' .
                            Html::a(
                                '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                                '#',
                                ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Download Excel']
                            )
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'type' => 'primary',
                        'heading' => '<i class="fa fa-list-alt"></i> Daftar OKR',
                        'before' => //(Yii::$app->user->can('Admin') or Yii::$app->user->can('BOD') or Yii::$app->user->can('Employee')) ?
                                    '<div class="row">' .
                                        '<div class="col-md-2">' .
                                            DatePicker::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'year',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'year-filter',
                                                    'placeholder' => 'Filter Tahun',
                                                    'class' => 'form-control additional-filter',
                                                ],
                                                'pluginOptions' => [
                                                    'autoclose'=>true,
                                                    'startView'=>'year',
                                                    'minViewMode'=>'years',
                                                    'format' => 'yyyy',
                                                    'allowClear' => false
                                                ]
                                            ]) .
                                        '</div>' .
                                        '<div class="col-md-3">' .
                                            Select2::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'unit_id',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'divisi-filter',
                                                    'class' => 'form-control additional-filter',
                                                    'placeholder' => '-- Filter Unit Kerja --',
                                                ],
                                                'pluginOptions' => ['allowClear' => true],
                                                'data' => $listUnit,
                                            ]) .
                                        '</div>' .
                                    '</div>'
                                    /* : '<div class="col-md-2">' .
                                        DatePicker::widget([
                                            'model' => $searchModel,
                                            'attribute' => 'year',
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'placeholder' => 'Filter Tahun',
                                                'class' => 'form-control additional-filter',
                                            ],
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'startView'=>'year',
                                                'minViewMode'=>'years',
                                                'format' => 'yyyy',
                                                'allowClear' => false
                                            ]
                                        ]) .
                                    '</div>' */,
                        'after'=>'',
                    ]
                ])?>
            </div>
        </div>
    </div>
    
</div>

<?= Html::beginForm(['download'], 'post', ['id' => 'download-form']) ?>
<div></div>
<?= Html::endForm() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<?php
    $jsReady = file_get_contents(__DIR__ . '/js/index.ready.js');
    $this->registerJs($jsReady, View::POS_READY);
?>