<?php

use app\models\Employee;
use app\models\Okr;
use app\models\PositionStructureEmployee;
use app\models\VwEmployeeManager;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\web\View;

$this->title = 'OKR - Individu';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>

    .btn-tab {
        border-color: var(--background) !important;
        color: var(--background) !important;
    }

    .btn-tab-active, .btn-tab:hover {
        background: var(--background) !important;
        color: #fff !important;
    }
</style>
<div class="okr-index">

    <div class="box box-default" style="width: 365px; margin-bottom: -5px;">
        <div class="box-body">
            <div class="btn-group">
                <?php // Html::a('OKR Organisasi', ['organization'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
                <?php // Html::a('Lihat OKR dalam format List', ['index'], ['class' => 'btn btn-default btn-tab btn-sm']); ?>
                <?= Html::a('OKR Unit', ['index'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
                <?= Html::a('OKR Individu', ['individual'], ['class' => 'btn btn-default btn-tab btn-tab-active btn-sm', 'style' => 'width: 170px;']); ?>
            </div>
        </div>
    </div>


    <div class="box box-default">
        <div class="box-body">
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'showPageSummary' => true,
                    'filterSelector' => '.additional-filter',
                    'pjax'=>true,
                    'emptyText' => 'Tidak ada data Key Result. Silahkan tambah Objective Individu terlebih dahulu lalu buat Key Result ' . 
                                    Html::a('disini', ['create-individual-key-result'], ['data-pjax' => 0, 'role' => 'modal-remote']) . '.',
                    'export' => [
                        'label' => 'Export Excel atau PDF',
                        'header' => '',
                        'options' => [
                            'class' => 'btn btn-primary'
                        ], 
                        'menuOptions' => [ 
                                'style' => 'background: var(--background); width: 187px;'
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => true,
                        GridView::EXCEL => true,
                    ],
                    'columns' => [
                        [
                            'class' => 'kartik\grid\SerialColumn',
                            'width' => '30px',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'parent_id',
                            'label' => 'Unit',
                            'value' => function($model){
                                return $model->parent->position->unit->unit_name ?: 'Organisasi';
                            },
                            'group' => true,
                            'groupedRow' => true,                    // move grouped column to a single grouped row
                            'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
                            'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'parent_okr_code',
                            'label' => 'Objective Code',
                            'value' => function($model){
                                return $model->parent->okr_code;
                            },
                            'width' => '120px',
                            'group' => true,
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'parent_okr',
                            'label' => 'Objective',
                            'value' => function($model){
                                return $model->parent->okr;
                            },
                            'width' => '200px',
                            'group' => true,
                            'subGroupOf' => 1
                        ],
                        // [
                        //     'label' => 'Action',
                        //     'value' => function($model){
                        //         return 
                        //                 Html::a('<span class="fa fa-pencil"></span>', ['update-individual-objective', 'id' => $model->parent_id], ['role' => 'modal-remote', 'title' => 'Update Objective', 'style' => 'color: var(--warning) !important;']) .
                        //                 '&nbsp;&nbsp;' .
                        //                 Html::a('<span class="fa fa-plus"></span>', ['create-individual-key-result', 'parent_id' => $model->parent_id], ['role' => 'modal-remote', 'title' => 'Tambah Key Result', 'style' => 'color: var(--success) !important;'])
                                       
                        //                 /* (!$check ? 
                        //                 Html::a(
                        //                     '<span class="fa fa-trash"></span>', 
                        //                     ['delete', 'id' => $model->id], 
                        //                     [
                        //                         'role'=>'modal-remote','title'=>'Hapus Key Result Ini',
                        //                         'data-confirm'=>false, 'data-method'=>false,
                        //                         'data-request-method'=>'post',
                        //                         'data-toggle'=>'tooltip',
                        //                         'data-confirm-title'=>'Konfirmasi',
                        //                         'data-confirm-message'=>'Yakin ingin menghapus Key Result ini?',
                        //                         'style' => 'color: var(--danger) !important;'
                        //                     ]) : '') */;
                        //     },
                        //     'width' => '50px',
                        //     'hAlign' => 'center',
                        //     'format' => 'raw',
                        //     'group' => true,
                        //     'subGroupOf' => 1
                        // ],
                        [
                            'attribute' => 'okr_code',
                            'label' => 'Key Result Code',
                            'width' => '120px'
                        ],
                        [
                            'attribute' => 'okr',
                            'label' => 'Key Result',
                            'width' => '200px',
                        ],
                        // [
                        //     'label' => 'Action',
                        //     'value' => function($model){
                        //         // $check = Okr::find()->where(['parent_id' => $model->id])->one();
                        //         // var_dump(VwEmployeeManager::find()->where(['upper_id' => Yii::$app->user->id])->andWhere(['position_id' => $model->position_structure_id])->andWhere(['is_active' => true])->createCommand()->getRawSql());
                        //         $isManager = VwEmployeeManager::find()->where(['upper_id' => Yii::$app->user->id])->andWhere(['position_id' => $model->position_structure_id])->andWhere(['is_active' => true])->one();
                        //         $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one();
                        //         $check = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['position_structure_id' => $model->id])->one();
                        //         return //Html::a('<span class="fa fa-plus-circle"></span>', ['create-support', 'parent_id' => $model->id], ['role' => 'modal-remote', 'title' => 'Create Support Cascading']) . '<br />' .
                        //                 (($check || $isManager || $isGeneralManager || Yii::$app->user->can('Admin')) ?
                        //                 Html::a('<span class="fa fa-pencil"></span>', ['update-individual-key-result', 'id' => $model->id], ['role' => 'modal-remote', 'title' => 'Update Key Result', 'style' => 'color: var(--warning) !important;'])
                        //                 : '-') .
                        //                 (Yii::$app->user->can('Admin') || Yii::$app->user->can('Leader') || $isManager ? 
                        //                 Html::a(
                        //                     '&nbsp;<span class="fa fa-trash"></span>', 
                        //                     ['delete-one-key-result', 'id' => $model->id], 
                        //                     [
                        //                         'role'=>'modal-remote','title'=>'Hapus Key Result Ini',
                        //                         'data-confirm'=>false, 'data-method'=>false,
                        //                         'data-request-method'=>'post',
                        //                         'data-toggle'=>'tooltip',
                        //                         'data-confirm-title'=>'Konfirmasi',
                        //                         'data-confirm-message'=>'Yakin ingin menghapus Key Result ini?',
                        //                         'style' => 'color: var(--danger) !important;'
                        //                     ]) : '') /* . '<br />' .
                        //                     Html::a('<span class="fa fa-circle"></span>', ['create-full', 'parent_id' => $model->parent_id], ['role' => 'modal-remote', 'title' => 'Create Full Cascading']) . '&nbsp;' .
                        //                     Html::a('<span class="fa fa-pie-chart"></span>', ['create-partial', 'parent_id' => $model->parent_id], ['role' => 'modal-remote', 'title' => 'Create Partial Cascading']) . '<br />' . 
                        //                     Html::a('<span class="fa fa-plus-circle"></span>', ['create-support', 'parent_id' => $model->id], ['role' => 'modal-remote', 'title' => 'Create Support Cascading']) */;
                        //     },
                        //     'hAlign' => 'center',
                        //     'format' => 'raw',
                        //     /* 'visible' => function($model){
                        //         $check = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['position_structure_id' => $model->id])->one();
                        //         return $check || Yii::$app->user->can('Admin');
                        //     } */
                        // ], 
                        [
                            'attribute' => 'pic_id',
                            'format' => 'raw',
                            'value' => function($model){
                                return $model->pic_id ? $model->pic->full_name . '<br />(' . $model->position->position_name . ')' : '-';
                            },
                            'filterType'=>GridView::FILTER_SELECT2,
                            'filter'=>ArrayHelper::map(Employee::find()->where(['is_employee' => 1])->andWhere(['company_id' => 1])->andWhere(['user_status' => 1])->andWhere(['employee_type_id' => [1,2,5]])->orderBy('full_name')->asArray()->all(), 'id', 'full_name'),
                            'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
                            'filterInputOptions'=>['placeholder'=>''],
                            'width' => '150px'
                        ],
                        [
                            'label' => 'Bobot Objective',
                            'attribute' => 'weight',
                            'value' => function($model){
                                return ($model->weight ? round($model->weight, 2) : '');
                            },
                            'pageSummary' => true,
                        ],
                        [
                            'label' => 'Bobot Individu',
                            'attribute' => 'unit_weight',
                            'value' => function($model){
                                return ($model->unit_weight ? round($model->unit_weight, 2): '');
                            },
                            'pageSummary' => true,
                        ],
                        [
                            'attribute' => 'target',
                            'value' => function($model){
                                return ($model->target ? number_format($model->target, 2, ',', '.') : '') . ' ' . $model->measure;
                            }
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'timebound_q1',
                            'value' => function($model){
                                return $model->timebound_q1 ?: '-';
                            }
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'target_q1',
                            'value' => function($model){
                                return ($model->target_q1 ? number_format($model->target_q1, 2, ',', '.') . ' ' . $model->measure: '-');
                            },
                            'hAlign' => 'center'
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'timebound_q2',
                            'value' => function($model){
                                return $model->timebound_q2 ?: '-';
                            }
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'target_q2',
                            'value' => function($model){
                                return ($model->target_q2 ? number_format($model->target_q2, 2, ',', '.') . ' ' . $model->measure: '-');
                            },
                            'hAlign' => 'center'
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'timebound_q3',
                            'value' => function($model){
                                return $model->timebound_q3 ?: '-';
                            }
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'target_q3',
                            'value' => function($model){
                                return ($model->target_q3 ? number_format($model->target_q3, 2, ',', '.') . ' ' . $model->measure: '-');
                            },
                            'hAlign' => 'center'
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'timebound_q4',
                            'value' => function($model){
                                return $model->timebound_q4 ?: '-';
                            }
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'target_q4',
                            'value' => function($model){
                                return ($model->target_q4 ? number_format($model->target_q4, 2, ',', '.') . ' ' . $model->measure: '-');
                            },
                            'hAlign' => 'center'
                        ],
                        [
                            'attribute' => 'cascading_type_id',
                            'value' => function($model){
                                return $model->cascading_type_id ? $model->cascadingType->cascading_type: '-';
                            }
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'label'=>'Lengkap?',
                            'attribute' => 'is_completed',
                            'format' => 'raw',
                            'value' => function($model){
                                $check = /* !$model->support_type ||  */!$model->polarization_id || !$model->calculation_id || !$model->validity_id || !$model->controllability_id;
                                
                                return $check ? '<span class="fa fa-info-circle" style="color: var(--warning);" title="Harus Dilengkapi"></span>' : '<span class="fa fa-check" style="color: var(--success);" title="Oke"></span>';
                            },
                            'filterType'=>GridView::FILTER_SELECT2,
                            'filter'=>[1 => 'Lengkap', 2 => 'Belum Lengkap'],
                            'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
                            'filterInputOptions'=>['placeholder'=>''],
                            'hAlign' => 'center'
                        ],
                    ],
                    'toolbar'=> [
                        /* ['content'=>
                            (Yii::$app->user->can('Admin') ? Html::a('<i class="fa fa-cloud-upload"></i> Tambah OKR dari Excel', ['create-individual-okr-excel'],
                            ['role'=>'modal-remote', 'class'=>'btn btn-success', 'title'=>'Tambah Objective Individu']) : '') .
                            (Yii::$app->user->can('Leader') || Yii::$app->user->can('Admin') || VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one() ?Html::a('<i class="glyphicon glyphicon-plus"></i> Tambah Key Result Individu', ['create-individual-key-result'],
                            ['role'=>'modal-remote', 'class'=>'btn btn-warning', 'title'=>'Tambah Key Result Individu']) : '') .
                            (Yii::$app->user->can('Admin') ? Html::a('<i class="fa fa-trash"></i> Hapus Key Result Individu', ['delete-individual-key-result'],
                            ['role'=>'modal-remote', 'class'=>'btn btn-danger', 'title'=>'Hapus Key Result Individu']) : '')
                        ], */
                        ['content'=>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                            '{toggleData}' .
                            Html::a(
                                '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                                '#',
                                ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Download Excel']
                            )/*  .
                            Html::a(
                                '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                                '#',
                                ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Download Excel']
                            ) */
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'type' => 'primary',
                        'heading' => '<i class="fa fa-list-alt"></i> Daftar OKR',
                        'before' => '<div class="row">' .
                                        '<div class="col-md-2">' .
                                            DatePicker::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'year',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'year-filter',
                                                    'placeholder' => 'Filter Tahun',
                                                    'class' => 'form-control additional-filter',
                                                ],
                                                'pluginOptions' => [
                                                    'autoclose'=>true,
                                                    'startView'=>'year',
                                                    'minViewMode'=>'years',
                                                    'format' => 'yyyy',
                                                    'allowClear' => false
                                                ]
                                            ]) .
                                        '</div>' .
                                        '<div class="col-md-3">' .
                                            Select2::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'unit_id',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'divisi-filter',
                                                    'class' => 'form-control additional-filter',
                                                    'placeholder' => '-- Filter Unit Kerja --',
                                                ],
                                                'pluginOptions' => ['allowClear' => true],
                                                'data' => $listUnit,
                                            ]) .
                                        '</div>' .
                                    '</div>',
                        'after'=>'',
                    ]
                ])?>
            </div>
        </div>
    </div>
    
</div>

<?= Html::beginForm(['download', 'type' => 'individual'], 'post', ['id' => 'download-form']) ?>
<div></div>
<?= Html::endForm() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<?php
    $jsReady = file_get_contents(__DIR__ . '/js/index.ready.js');
    $this->registerJs($jsReady, View::POS_READY);
?>