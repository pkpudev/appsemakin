// define function/codes
var fnSumTarget = function() {
	var target = 0;
	var calculation = $('#okr-calculation_id').val();
	var countTarget = 0;
	var arrayTarget = [];
	
	jQuery( ".tq" ).each(function(){
		var $tq = jQuery( this );
		if ( $tq.prop( "disabled" ) != true ) {
			var val = $tq.val();
			if ( jQuery.isNumeric(val) ) {
				if(calculation == 1){
					target += parseFloat(val);
				} else if(calculation == 2){
					target += parseFloat(val);
				} else if(calculation == 3){
					target = parseFloat(val);
				} else if(calculation == 4){
					arrayTarget.push(parseFloat(val));
				} else {
					target = '';
				}
			}

			countTarget++;
		}
	});
	if(calculation == 1){
		target = target/countTarget;
	} else if(calculation == 4){
		target = Math.min.apply(Math, arrayTarget);
	}
	if (target == 0) {
		target = "";
	}
	jQuery( "#target" ).val( target );
};


// setup events
jQuery( ".tq" ).on( "keyup", fnSumTarget ).on( "change", fnSumTarget );
jQuery( "#okr-calculation_id" ).on( "change", fnSumTarget );

// running fn/codes on startup!
// fnSumBudget();