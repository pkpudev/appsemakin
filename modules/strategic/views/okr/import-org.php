<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

$this->title = 'Tambah OKR Organisasi';

CrudAsset::register($this);
?>
<div>
    <?=GridView::widget([
        'id'=>'crud-datatable',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterSelector' => '.additional-filter',
        // 'showPageSummary' => true,
        'pjax'=>true,
        'emptyText' => 'Tidak ada data Key Result. Silahkan tambah Objective Organisasi terlebih dahulu lalu buat Key Result ' . 
                        Html::a('disini', ['create-organization-key-result'], ['data-pjax' => 0, 'role' => 'modal-remote']) . '.',
        'export' => [
            'label' => 'Export Excel atau PDF',
            'header' => '',
            'options' => [
                'class' => 'btn btn-primary'
            ], 
            'menuOptions' => [ 
                    'style' => 'background: var(--background); width: 187px;'
            ]
        ],
        'exportConfig' => [
            GridView::PDF => true,
            GridView::EXCEL => true,
        ],
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            [
                'label' => 'Periode',
                'value' => function($model){
                    return $model->start_year . ' - ' . $model->end_year;
                }
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute' => 'parent_okr_code',
                'label' => 'Objective Code',
                'value' => function($model){
                    return $model->parent->okr_code;
                },
                'width' => '120px',
                'group' => true,
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute' => 'parent_okr',
                'label' => 'Objective',
                'value' => function($model){
                    return $model->parent->okr;
                },
                'width' => '200px',
                'group' => true,
                'subGroupOf' => 1
            ],
            [
                'attribute' => 'okr_code',
                'label' => 'Key Result Code',
                'width' => '120px'
            ],
            [
                'attribute' => 'okr',
                'label' => 'Key Result',
                'width' => '200px',
            ],
            [
                'attribute' => 'portfolio_type_id',
                'value' => function($model){
                    return $model->portfolio_type_id ? $model->portfolio->portfolio_type: '';
                },
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>ArrayHelper::map(app\models\PortfolioType::find()->orderBy('id')->asArray()->all(), 'id', 'portfolio_type'), 
                'filterWidgetOptions'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
                'filterInputOptions'=>['placeholder'=>''],
            ],
            [
                'attribute' => 'target',
                'value' => function($model){
                    return ($model->target ?: '') . ' ' . $model->measure;
                }
            ],
            [
                'attribute' => 'weight',
                'value' => function($model){
                    return $model->weight ?: '';
                },
            ],
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'width' => '20px',
            ],
        ],
        'toolbar'=> [
            ['content'=>
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                '{toggleData}' .
                Html::a(
                    '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                    '#',
                    ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Download Excel']
                )
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'type' => 'primary',
            'heading' => '<i class="fa fa-list-alt"></i> Daftar OKR',
            'before' => //(Yii::$app->user->can('Admin') or Yii::$app->user->can('BOD') or Yii::$app->user->can('Employee')) ?
                        '<div class="row">' .
                            '<div class="col-md-2">' .
                                DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'start_year',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'start_year-filter',
                                        'placeholder' => 'Filter Tahun',
                                        'class' => 'form-control additional-filter',
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'startView'=>'year',
                                        'minViewMode'=>'years',
                                        'format' => 'yyyy',
                                        'allowClear' => false
                                    ]
                                ]) .
                            '</div>' .
                            '<div class="col-md-2">' .
                                DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'end_year',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'end_year-filter',
                                        'placeholder' => 'Filter Tahun',
                                        'class' => 'form-control additional-filter',
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'startView'=>'year',
                                        'minViewMode'=>'years',
                                        'format' => 'yyyy',
                                        'allowClear' => false
                                    ]
                                ]) .
                            '</div>' .
                        '</div>'
                        /* : '<div class="col-md-2">' .
                            DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'year',
                                'pjaxContainerId' => 'crud-datatable-pjax',
                                'options' => [
                                    'placeholder' => 'Filter Tahun',
                                    'class' => 'form-control additional-filter',
                                ],
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'startView'=>'year',
                                    'minViewMode'=>'years',
                                    'format' => 'yyyy',
                                    'allowClear' => false
                                ]
                            ]) .
                        '</div>' */,
            'after'=> BulkButtonWidget::widget([
                        'buttons' => Html::a(
                            'Buat OKR Organisasi',
                            ["create-from-org"],
                            [
                                "class" => "btn btn-success btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                'data-request-method' => 'post',
                                'data-modal-size' => 'large',
                                'data-confirm-title' => 'Buat OKR Organisasi',
                                'data-confirm-message' =>  DatePicker::widget([
                                    'name' => 'year',
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'todayHighlight' => true,
                                        'startView'=>'year',
                                        'minViewMode'=>'years',
                                        'format' => 'yyyy'
                                    ]
                                ]),
                            ]
                        )
                    ]),
        ]
    ])?>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>