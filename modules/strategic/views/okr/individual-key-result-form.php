<?php

use app\models\Calculation;
use app\models\Controllability;
use app\models\Employee;
use app\models\Measure;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\OkrOrg;
use app\models\Polarization;
use app\models\Validity;
use app\models\VwEmployeeManager;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

// if(!Yii::$app->user->can('Admin')){
    $unitdIds = [];
    $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['is_active' => true])->all();
    foreach($vwEmployeeManagers as $vwEmployeeManager){
        array_push($unitdIds, $vwEmployeeManager->unit_id);
    }

    $okrList = ArrayHelper::map(Okr::find()
                                    ->alias('o')
                                    ->joinWith('position p')
                                    ->andWhere(['in', 'p.unit_structure_id', $unitdIds])
                                    ->andWhere(['okr_level_id' => OkrLevel::INDIVIDUAL])->orderBy('okr_code asc')->all(), 'id', function($model){
        return $model->okr_code . ' - ' . $model->okr . ' (' . $model->pic->full_name . ')';
    });
// } else {
//     $okrList = ArrayHelper::map(Okr::find()->orderBy('okr_code asc')->all(), 'id', function($model){
//         return $model->okr_code . ' - ' . $model->okr;
//     });
// }

?>
<div class="org-kr">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($modelParent): ?>
        <b>Objective: </b><br />
        <?= $modelParent->okr; ?><br />
    <?php else: ?>
        <?= $form->field($model, 'parent_id')->widget(Select2::classname(), [
            'data' => $okrList,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['prompt' => '-- Pilih Objective --'],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                ]
        ])->label('Objective'); ?>
    <?php endif; ?>

    <?= $form->field($model, 'okr')->textarea(['rows' => 2])->label('Key Result') ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'polarization_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Polarization::find()->where(['is_active' => true])->asArray()->all(), 'id', 'polarization_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Polarization --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'calculation_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Calculation::find()->where(['is_active' => true])->asArray()->all(), 'id', 'calculation_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Calculation --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'validity_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Validity::find()->where(['is_active' => true])->asArray()->all(), 'id', 'validity_type'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Validitas --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <hr style="margin: 5px 0;" />

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'target')->textInput([]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'measure')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Measure::find()->where(['is_active' => true])->asArray()->all(), 'measure', 'measure'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Satuan --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'controllability_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Controllability::find()->where(['is_active' => true])->asArray()->all(), 'id', 'degree_of_controllability'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Tingkat Pengendalian --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'pic_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Employee::find()->where(['is_employee' => 1])->andWhere(['company_id' => 1])->andWhere(['user_status' => 1])->andWhere(['employee_type_id' => [1,2,5]])->orderBy('full_name')->asArray()->all(), 'id', 'full_name'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih PIC --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <hr style="margin: 5px 0;" />

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'target_q1')->textInput(['class' => 'tq form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q2')->textInput(['class' => 'tq form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q3')->textInput(['class' => 'tq form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'target_q4')->textInput(['class' => 'tq form-control']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q1')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q2')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q3')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'timebound_q4')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>
    
</div>