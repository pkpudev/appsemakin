<?php

use app\models\Okr;
use app\models\OkrLevel;
use yii\helpers\Html;

use johnitvn\ajaxcrud\CrudAsset;

app\assets\AppAsset::register($this);
CrudAsset::register($this);

dmstr\web\AdminLteAsset::register($this);
?>
<div class="box-okr-<?= $model->id; ?>">

    <div class="btn-group pull-right" style="position: absolute;right: 15px;">
        <?php 
            $check = Okr::find()->where(['parent_id' => $model->id])->one(); 
            if(!$check) echo Html::a(
                                    'Hapus Objective', 
                                    ['delete', 'id' => $model->id], 
                                    [
                                        'class' => 'btn btn-warning btn-xs',
                                        'role'=>'modal-remote','title'=>'Hapus Objective Ini',
                                        'data-confirm'=>false, 'data-method'=>false,
                                        'data-request-method'=>'post',
                                        'data-toggle'=>'tooltip',
                                        'data-confirm-title'=>'Konfirmasi',
                                        'data-confirm-message'=>'Yakin ingin menghapus Objective ini?',
                                        'style' => 'color: var(--danger) !important;'
                                    ]);
        ?>
        <?= Html::a('Tambah Key Result', ['create', 'parent_id' => $model->id], ['class' => 'btn btn-success btn-xs', 'role' => 'modal-remote']); ?>
    </div>
    <b>Detail Current OKR - <?= $model->level->okr_level; ?></b>

<br />
    <?= $this->render('_detail_current_kr', [
        'dataProviderCurrentKr' => $dataProviderCurrentKr,
    ]) ?>
    <br />
    <b>OKR 1 Level Above - <?= OkrLevel::findOne($model->okr_level_id-1)->okr_level ?: ''; ?></b>
    <?= $this->render('_detail_above_below_kr', [
        'dataProvider' => $dataProviderAboveKr,
    ]) ?>
    <br />
    <b>OKR 1 Level Below -  <?= OkrLevel::findOne($model->okr_level_id+1)->okr_level ?: ''; ?> </b>
    <?= $this->render('_detail_above_below_kr', [
        'dataProvider' => $dataProviderBelowKr,
    ]) ?>

</div>