<?php

use app\models\Okr;
use app\models\OkrLevel;
use app\models\PositionStructureEmployee;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\web\View;

$this->title = 'OKR - Tabel';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>

    .btn-tab {
        border-color: var(--background) !important;
        color: var(--background) !important;
    }

    .btn-tab-active, .btn-tab:hover {
        background: var(--background) !important;
        color: #fff !important;
    }
</style>
<div class="okr-index">

    <div class="box box-default" style="width: 710px; margin-bottom: -5px;">
        <div class="box-body">
            <div class="btn-group">
                <?= Html::a('OKR Organisasi', ['organization'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
                <?php // Html::a('Lihat OKR dalam format List', ['index'], ['class' => 'btn btn-default btn-tab btn-sm']); ?>
                <?= Html::a('Lihat OKR dalam format Tabel', ['#'], ['class' => 'btn btn-default btn-tab btn-tab-active btn-sm']); ?>
                <?= Html::a('OKR Individu', ['individual'], ['class' => 'btn btn-default btn-tab btn-sm', 'style' => 'width: 170px;']); ?>
            </div>
        </div>
    </div>


    <div class="box box-default">
        <div class="box-body">
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'filterSelector' => '.additional-filter',
                    'pjax'=>true,
                    'export' => [
                        'label' => 'Export Excel atau PDF',
                        'header' => '',
                        'options' => [
                            'class' => 'btn btn-primary'
                        ], 
                        'menuOptions' => [ 
                                'style' => 'background: var(--background); width: 187px;'
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => true,
                        GridView::EXCEL => true,
                    ],
                    'columns' => [
                        [
                            'class' => 'kartik\grid\SerialColumn',
                            'width' => '30px',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            // 'attribute' => 'parent_id',
                            'label' => 'Unit',
                            'value' => function($model){
                                return $model->unit_name ?: 'Organisasi';
                            },
                            'group' => true,
                            'groupedRow' => true,                    // move grouped column to a single grouped row
                            'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
                            'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'okr_code',
                            'label' => 'Objective Code',
                            'value' => function($model){
                                return $model->okr_code;
                            },
                            'width' => '120px',
                            'group' => true,
                            'subGroupOf' => 1
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute' => 'okr',
                            'label' => 'Objective',
                            'format' => 'raw',
                            'value' => function($model){
                                return 
                                        $model->okr . '<br /><br />' . 
                                        ($model->okr_level_id <> OkrLevel::ORGANIZATION ? Html::a('<span class="fa fa-plus"></span> Tambah Key Result', ['create', 'parent_id' => $model->id], ['class' => 'btn btn-success btn-xs', 'role' => 'modal-remote']) : '');
                            },
                            'width' => '200px',
                            'group' => true,
                            'subGroupOf' => 1
                        ],
                        [
                            'header' => '<table width="100%">
                                            <tr>
                                                <td width="100px">Key Result Code</td>
                                                <td>Key Result</td>
                                                <td width="50px">Action</td>
                                                <td width="90px" align="center">Bobot</td>
                                                <td width="100px" align="center">Target Kuartal 1</td>
                                                <td width="100px" align="center">Kuartal 1</td>
                                                <td width="100px" align="center">Target Kuartal 2</td>
                                                <td width="100px" align="center">Kuartal 2</td>
                                                <td width="100px" align="center">Target Kuartal 3</td>
                                                <td width="100px" align="center">Kuartal 3</td>
                                                <td width="100px" align="center">Target Kuartal 4</td>
                                                <td width="100px" align="center">Kuartal 4</td>
                                            </tr>
                                        </table>',
                            'format' => 'raw',
                            'contentOptions' => ['style' => 'padding: 0;'],
                            'value' => function($model){
                                $childs = Okr::find()->where(['parent_id' => $model->id])->andWhere(['type' => 'KR'])->all();
                                if($childs){
                                    $string = '<table class="kv-grid-table table table-bordered table-striped table-condensed kv-table-wrap" style="margin-bottom: 0px;">';
                                    foreach($childs as $child){
                                        $check1 = Okr::find()->where(['parent_id' => $child->id])->one();
                                        $check2 = $child->pic_id == Yii::$app->user->id;

                                        $string .= '<tr>
                                                        <td width="100px">' . $child->okr_code . '</td>
                                                        <td width="">' . $child->okr . '</td>
                                                        <td width="50px" align="center">' . 
                                                            (($check2 || Yii::$app->user->can('Admin')) && $child->id ?
                                                            Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $child->id], ['role' => 'modal-remote', 'title' => 'Update Key Result', 'style' => 'color: var(--warning) !important;']) . '<br />'
                                                            : '') . 
                                                            (!$check1 && $child->id ? 
                                                            Html::a(
                                                                '<span class="fa fa-trash"></span>', 
                                                                ['delete', 'id' => $child->id], 
                                                                [
                                                                    'role'=>'modal-remote','title'=>'Hapus Key Result Ini',
                                                                    'data-confirm'=>false, 'data-method'=>false,
                                                                    'data-request-method'=>'post',
                                                                    'data-toggle'=>'tooltip',
                                                                    'data-confirm-title'=>'Konfirmasi',
                                                                    'data-confirm-message'=>'Yakin ingin menghapus Key Result ini?',
                                                                    'style' => 'color: var(--danger) !important;'
                                                                ]) . '<br />' : '') .
                                                                ($child->id ? Html::a('<span class="fa fa-plus-circle"></span>', ['create-support', 'parent_id' => $child->id], ['role' => 'modal-remote', 'title' => 'Create Support Cascading']) : '')
                                                        . '</td>
                                                        <td width="100px" align="center">' . ($child->weight ?:'-') . '</td>
                                                        <td width="100px" align="center">' . ($child->target_q1 ?:'-') . '</td>
                                                        <td width="100px" align="center">' . ($child->timebound_q1 ?:'-') . '</td>
                                                        <td width="100px" align="center">' . ($child->target_q2 ?:'-') . '</td>
                                                        <td width="100px" align="center">' . ($child->timebound_q2 ?:'-') . '</td>
                                                        <td width="100px" align="center">' . ($child->target_q3 ?:'-') . '</td>
                                                        <td width="100px" align="center">' . ($child->timebound_q3 ?:'-') . '</td>
                                                        <td width="100px" align="center">' . ($child->target_q4 ?:'-') . '</td>
                                                        <td width="100px" align="center">' . ($child->timebound_q4 ?:'-') . '</td>
                                                    </tr>';
                                    }
                                    $string .= '</table>';

                                    return $string;
                                } else {
                                    return '';
                                }
                            },
                        ]
                    ],
                    'toolbar'=> [
                        [
                            'content' => Html::a('<i class="fa fa-plus"></i> Tambah OKR Organisasi', ['import-okr-org'],
                                                ['data-pjax'=>0, 'class'=>'btn btn-success']) .
                                        Html::a('<i class="fa fa-plus"></i> Tambah Key Result', ['create'],
                                                ['role'=>'modal-remote', 'class'=>'btn btn-warning']) 
                        ],
                        ['content'=>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                            '{toggleData}' .
                            Html::a(
                                '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                                '#',
                                ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Download Excel']
                            )
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'type' => 'primary',
                        'heading' => '<i class="fa fa-list-alt"></i> Daftar OKR',
                        'before' => //(Yii::$app->user->can('Admin') or Yii::$app->user->can('BOD') or Yii::$app->user->can('Employee')) ?
                                    '<div class="row">' .
                                        '<div class="col-md-2">' .
                                            DatePicker::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'year',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'year-filter',
                                                    'placeholder' => 'Filter Tahun',
                                                    'class' => 'form-control additional-filter',
                                                ],
                                                'pluginOptions' => [
                                                    'autoclose'=>true,
                                                    'startView'=>'year',
                                                    'minViewMode'=>'years',
                                                    'format' => 'yyyy',
                                                    'allowClear' => false
                                                ]
                                            ]) .
                                        '</div>' .
                                        '<div class="col-md-3">' .
                                            Select2::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'unit_id',
                                                'pjaxContainerId' => 'crud-datatable-pjax',
                                                'options' => [
                                                    'id' => 'divisi-filter',
                                                    'class' => 'form-control additional-filter',
                                                    'placeholder' => '-- Filter Unit Kerja --',
                                                ],
                                                'pluginOptions' => ['allowClear' => true],
                                                'data' => $listUnit,
                                            ]) .
                                        '</div>' .
                                    '</div>'
                                    /* : '<div class="col-md-2">' .
                                        DatePicker::widget([
                                            'model' => $searchModel,
                                            'attribute' => 'year',
                                            'pjaxContainerId' => 'crud-datatable-pjax',
                                            'options' => [
                                                'placeholder' => 'Filter Tahun',
                                                'class' => 'form-control additional-filter',
                                            ],
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'startView'=>'year',
                                                'minViewMode'=>'years',
                                                'format' => 'yyyy',
                                                'allowClear' => false
                                            ]
                                        ]) .
                                    '</div>' */,
                        'after'=>'',
                    ]
                ])?>
            </div>
        </div>
    </div>
    
</div>

<?= Html::beginForm(['download'], 'post', ['id' => 'download-form']) ?>
<div></div>
<?= Html::endForm() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<?php
    $jsReady = file_get_contents(__DIR__ . '/js/index.ready.js');
    $this->registerJs($jsReady, View::POS_READY);
?>