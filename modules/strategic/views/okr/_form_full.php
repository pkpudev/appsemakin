<?php

use app\models\Calculation;
use app\models\CascadingType;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\Polarization;
use app\models\PositionStructure;
use app\models\UnitStructure;
use app\models\Validity;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Okr */
/* @var $form yii\widgets\ActiveForm */

if($model->parent_id && ($parent = Okr::findOne($model->parent_id))){

    if($modelParent->position_structure_id){
        $positionList = ArrayHelper::map(PositionStructure::find()->where(['responsible_to_id' => $modelParent->position_structure_id])->orderBy('position_name asc')->all(), 'id', 'position_name');
    } else {

        $unitIds = [];
        $units = UnitStructure::find()->where(['year' => $parent->year])->andWhere(['level' => '1'])->all();
        foreach($units as $unit){
            array_push($unitIds, $unit->id);
        }
        
        $positionList = ArrayHelper::map(PositionStructure::find()->where(['in', 'unit_structure_id', $unitIds])->orderBy('position_name asc')->all(), 'id', 'position_name');
    }
} else {
    $positionList = ArrayHelper::map(PositionStructure::find()->orderBy('position_name asc')->all(), 'id', 'position_name');
}
?>

<div class="okr-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'position_structure_id')->widget(Select2::classname(), [
        'data' => $positionList,
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['prompt' => '-- Pilih Jabatan --'],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]); ?>

    <table class="table table-bordered table-striped table-condensed ">
        <tr>
            <th></th>
            <th>#</th>
            <th>Key Result Code</th>
            <th>Key Result Description</th>
            <th width="100px">Target</th>
        </tr>
        <?php 
            $krs = Okr::find()->where(['parent_id' => $modelParent->id])->andWhere(['type' => 'KR'])->all();
            if(!$krs) echo 'Tidak memiliki Key Result';

            $no = 1;
            foreach($krs as $kr):
        ?>
            <tr>
                <td width="30px" align="center"><?= $form->field($model, 'krs[]')->checkbox(['label' => '', 'value' => $kr->id, 'uncheck' => null]); ?></td>
                <td width="40px" align="center"><?= $no; ?></td>
                <td><?= $kr->okr_code; ?></td>
                <td><?= $kr->okr; ?></td>
                <td><?= $kr->target . ' ' . $kr->measure; ?></td>
            </tr>
        <?php $no++; endforeach; ?>
    </table>
        
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
