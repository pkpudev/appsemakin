<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Departments;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rencana Kegiatan Kerja (RKK)';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="activity-index">
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns.php'),
            'toolbar' => [
                [
                    'content' => (/* (($info or Yii::$app->user->can('Admin')) and $searchModel->lock == false) */1==1 ?
                        Html::a(
                            '<i class="glyphicon glyphicon-plus"></i> Tambah Kegiatan',
                            ['create'],
                            ['role' => 'modal-remote', 'title' => 'Tambah Kegiatan', 'class' => 'btn btn-success']
                        ) : '') .
                        Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']
                        ) .
                        '{toggleData}'.
                        Html::a(
                            '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                            '#',
                            ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Download Excel']
                        )
                ],
            ],
            'striped' => true,
            'hover' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i> List Kegiatan',
                'before' => /* (Yii::$app->user->can('Admin') or Yii::$app->user->can('BOD')) ? */
                            '<div class="row">' .
                            '<div class="col-md-2">' .
                                DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'year',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'year-filter',
                                        'placeholder' => 'Filter Tahun',
                                        'class' => 'form-control additional-filter',
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'startView'=>'year',
                                        'minViewMode'=>'years',
                                        'format' => 'yyyy',
                                        'allowClear' => false
                                    ]
                                ]) .
                            '</div>' .
                            '<div class="col-md-3">' .
                                Select2::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'unit_id',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'id' => 'divisi-filter',
                                        'class' => 'form-control additional-filter',
                                        'placeholder' => '-- Filter Unit Kerja --',
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                    'data' => $listUnit,
                                ]) .
                            '</div>' .
                            '</div>'
                            /* : '<div class="col-md-2">' .
                                DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'year',
                                    'pjaxContainerId' => 'crud-datatable-pjax',
                                    'options' => [
                                        'placeholder' => 'Filter Tahun',
                                        'class' => 'form-control additional-filter',
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'startView'=>'year',
                                        'minViewMode'=>'years',
                                        'format' => 'yyyy',
                                        'allowClear' => false
                                    ]
                                ]) .
                            '</div>' */,
                'after' => BulkButtonWidget::widget([
                    'buttons' => Html::a(
                        '<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All',
                        ["bulk-delete"],
                        [
                            "class" => "btn btn-danger btn-xs",
                            'role' => 'modal-remote-bulk',
                            'data-confirm' => false, 'data-method' => false, // for overide yii data api
                            'data-request-method' => 'post',
                            'data-confirm-title' => 'Are you sure?',
                            'data-confirm-message' => 'Are you sure want to delete this item'
                        ]
                    ),
                ]) .
                    '<div class="clearfix"></div>',
            ]
        ]) ?>
    </div>
</div>
<?= Html::beginForm(['download'], 'post', ['id' => 'download-form']) ?>
<div></div>
<?= Html::endForm() ?>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "", // always need it for jquery plugin
    "size" => Modal::SIZE_LARGE,
]) ?>
<?php Modal::end(); ?>
<?php
    $jsReady = file_get_contents(__DIR__ . '/js/index.ready.js');
    $this->registerJs($jsReady, View::POS_READY);
?>