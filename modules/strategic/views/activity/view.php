<?php

use kartik\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Activity */
?>
<div class="activity-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label'=>'Unit',
                'format'=>'html',
                'value'=>function($model){
                    return substr($model->initiative->position->unit->unit_code, 0, -8).' '.$model->initiative->position->unit->unit_name;
                },
            ],
            [
                'label'=>'Kode Program Kerja',
                'value'=>function($model){
                    return $model->initiative->initiatives_code;
                },
            ], 
            [
                'label'=>'Program Kerja',
                'attribute'=>'strategic_initiatives_id',
                'value'=>function($model){
                    return $model->initiative->workplan_name?:'';
                },
            ],
            [
                'label'=>'Bobot',
                'value'=>function($model){
                    return $model->initiative->grade?:'';
                },
            ],
            [
                'attribute'=>'activity_code',
            ],
            [
                'attribute'=>'activity_name',
            ],
            [
                'attribute'=>'weight',
            ],
            [
                'attribute'=>'pic_id',
                'value'=>function($model){
                    return $model->pic->full_name?:'';
                },
            ],
            [
                'attribute'=>'start_plan',
                'value' => function($model){
                    return ($model->start_plan) ? date('d-m-Y',strtotime($model->start_plan)):'-';
                },
            ],
            [
                'attribute'=>'finish_plan',
                'value' => function($model){
                    return ($model->finish_plan) ? date('d-m-Y',strtotime($model->finish_plan)):'-';
                },
            ],
            'documentation',
            [
                'attribute' => 'difficulty_level',
                'value' => function($model){
                    $array = [1 => 'Rendah (Low)', 2 => 'Sedang  (Medium)', 3 => 'Tinggi (High)'];
                    return $array[$model->difficulty_level] ?: '-';
                }
            ],
            [
                'attribute' => 'resources_level',
                'value' => function($model){
                    $array = [1 => 'Rendah (Low)', 2 => 'Sedang  (Medium)', 3 => 'Tinggi (High)'];
                    return $array[$model->resources_level] ?: '-';
                }
            ],
            [
                'attribute' => 'is_mov',
                'value' => function($model){
                    $array = [1 => 'Ya', 0 => 'Tidak'];
                    return $array[$model->is_mov] ?: '-';
                }
            ],
            [
                'attribute' => 'dissatisfaction_level',
                'value' => function($model){
                    $array = [1 => 'Rendah (Low)', 2 => 'Sedang  (Medium)', 3 => 'Tinggi (High)'];
                    return $array[$model->dissatisfaction_level] ?: '-';
                }
            ],
            [
                'attribute'=>'resources',
                'value' => function($model){
                    return ($model->resources) ?:'-';
                },
            ],
            [
                'attribute'=>'note',
                'value' => function($model){
                    return ($model->note) ?:'-';
                },
            ],
        ],
    ]) ?>


    <?= GridView::widget([
        'layout' => '{pager}{items}{pager}',
        'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getActivityHistories()->orderBy('stamp'), 'pagination' => ['pageSize' => 10, 'pageParam' => 'page-initiativeshistories']]),
        'pager' => [
            'class' => yii\widgets\LinkPager::className(),
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ],
        'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
        'headerRowOptions' => ['class' => 'x'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'width' => '175px',
                'attribute' => 'stamp',
                'value' => function ($model) {
                    return ($model->stamp) ? date('d M Y H:i:s', strtotime($model->stamp)) : '-';
                },
                // 'format' => ['date', 'dd MM yyyy HH:mm:ss'],
            ],
            'action',
            [
                'attribute' => 'by',
                'value' => 'by0.full_name'
            ],
            'comment:ntext',
        ],
    ]); ?>

</div>
