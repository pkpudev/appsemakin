<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Activity */
?>
<div class="activity-update">

    <?= $this->render('_form', [
        'model' => $model,
        'initiativeList' => $initiativeList,
        'picList' => $picList
    ]) ?>

</div>
