<?php

use app\models\Employee;
use yii\helpers\Html;
use yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header'=>'Unit',
        'filter'=>false,
        'format'=>'html',
        'value'=>function($model, $key, $index, $widget){
            return '<b>'./*@$model->vwDepartment->department_code*/substr($model->initiative->position->unit->unit_code, 0, -8).' '.$model->initiative->position->unit->unit_name.'</b>';
        },
        'group'=>true,
        'groupedRow'=>true,                    // move grouped column to a single grouped row
        'groupOddCssClass'=>'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass'=>'kv-grouped-row',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header'=>'Kode Program Kerja',
        'value'=>function($model){
            return $model->initiative->initiatives_code;
        },
        'group'=>true,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Program Kerja',
        'attribute'=>'strategic_initiatives_id',
        'value'=>function($model){
            return $model->initiative->workplan_name?:'';
        },
        'group'=>true,
        'subGroupOf'=>1
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'header'=>'Bobot',
        'value'=>function($model){
            return $model->initiative->grade?:'';
        },
        'group'=>true,
        'subGroupOf'=>2
    ],
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'activity_code',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'activity_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'weight',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'pic_id',
        'value'=>function($model){
            return $model->pic->full_name?:'';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(Employee::find()->where(['is_employee' => 1])->andWhere(['company_id' => 1])->andWhere(['user_status' => 1])->andWhere(['employee_type_id' => [1,2,5]])->orderBy('full_name')->asArray()->all(), 'id', 'full_name'),
        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true],],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'start_plan',
        'value' => function($model){
            return ($model->start_plan) ? date('d-m-Y',strtotime($model->start_plan)):'-';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'finish_plan',
        'value' => function($model){
            return ($model->finish_plan) ? date('d-m-Y',strtotime($model->finish_plan)):'-';
        },
    ],
    'documentation',
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'resources',
        'value' => function($model){
            return ($model->resources) ?:'-';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'note',
        'value' => function($model){
            return ($model->note) ?:'-';
        },
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'status_id',
    //     'format'=>'raw',
    //     'width'=>'80px',
    //     'value'=>function($model) {
    //         return ($model->status) ? $model->status->getStatusname($model->status_id) : '-';
    //     },
    //     'filterType'=>GridView::FILTER_SELECT2,
    //     'filter'=>ArrayHelper::map(app\models\Status::find()->orderBy('id')->asArray()->all(), 'id', 'status'), 
    //     'filterWidgetOptions'=>[
    //         'pluginOptions'=>['allowClear'=>true],
    //     ],
    //     'filterInputOptions'=>['placeholder'=>''],
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template'=>'{view} {update} {delete}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        // 'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        // 'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        // 'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
        //                   'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
        //                   'data-request-method'=>'post',
        //                   'data-toggle'=>'tooltip',
        //                   'data-confirm-title'=>'Are you sure?',
        //                   'data-confirm-message'=>'Are you sure want to delete this item'], 
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a(
                    '<span class="fa fa-eye"></span>',
                    $url,
                    [
                        'role' => 'modal-remote', 'title' => Yii::t('app', 'View'), 'data-toggle' => 'tooltip',
                    ]
                );
            },
            'update' => function ($url, $model) {
                if ($model->initiative->status_id == app\models\Status::DRAFT or $model->initiative->status_id == app\models\Status::REVISION) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-pencil"></span>',
                        $url,
                        [
                            'role' => 'modal-remote', 'title' => Yii::t('app', 'Update'), 'data-toggle' => 'tooltip', 'style' => 'color: var(--warning) !important;'
                        ]
                    );
                } else
                    return false;
            },
            'delete' => function ($url, $model) {
                if ($model->initiative->status_id <> app\models\Status::DRAFT and $model->initiative->status_id <> app\models\Status::REVISION) {
                    return false;
                } else {
                    return Html::a(
                        '<span class="glyphicon glyphicon-trash"></span>',
                        $url,
                        [
                            'title' => Yii::t('app', 'Delete'),
                            'data-confirm' => '' . 'Are you sure to delete this item?' . '',
                            'data-method' => 'post',
                            'style' => 'color: var(--danger) !important;'
                        ]
                    );
                }
            },
        ],
    ],

];   