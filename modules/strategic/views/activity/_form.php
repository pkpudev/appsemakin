<?php

use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use \kartik\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="activity-form">

    <?php $form = ActiveForm::begin([
        'id' => 'activity',
        'type' => ActiveForm::TYPE_VERTICAL,
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error'
    ]);?>


    <?= $form->field($model, 'initiatives_id')
        ->widget(Select2::classname(), [
            'data' => $initiativeList,
            // 'initValueText' => $model->initiatives_id ? $model->initiatives->initiatives_code . ' - ' . $model->initiatives->workplan_name : '',
            'options' => [
                'id' => 'proker',
                'placeholder' => '-- Pilih Proker --',
                'value' => $model->initiatives_id
            ],
            'pluginOptions' => [
                'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
            ],
        ]);
    ?>

    <?= $form->field($model, 'activity_name')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'pic_id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'data'=>$picList,
        'options' => [
            'placeholder' => 'Pilih PIC',
            'value' => $model->pic_id
        ],
        'select2Options' => [
            'pluginOptions' => [
                'allowClear' => true,
                'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
            ]
        ],
        'pluginOptions' => [
            'depends' => ['proker'],
            'url' => Url::to(['/strategic/activity/position-employee']),
        ]
    ]); ?>
    
    <?= $form->field($model, 'start_plan')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>

    <?= $form->field($model, 'finish_plan')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>
    
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'difficulty_level')->widget(Select2::classname(), [
                'data'=>[1 => 'Rendah (Low)', 2 => 'Sedang  (Medium)', 3 => 'Tinggi (High)'],
                'options'=>['prompt'=>'--- Pilih '.$model->getAttributeLabel('difficulty_level').' ---'],
                'pluginOptions'=>['allowClear'=>true],
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'resources_level')->widget(Select2::classname(), [
                'data'=>[1 => 'Rendah (Low)', 2 => 'Sedang  (Medium)', 3 => 'Tinggi (High)'],
                'options'=>['prompt'=>'--- Pilih '.$model->getAttributeLabel('resources_level').' ---'],
                'pluginOptions'=>['allowClear'=>true],
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'is_mov')->widget(Select2::classname(), [
                'data'=>[1 => 'Ya', 0 => 'Tidak'],
                'options'=>['prompt'=>'--- Pilih '.$model->getAttributeLabel('is_mov').' ---'],
                'pluginOptions'=>['allowClear'=>true],
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'dissatisfaction_level')->widget(Select2::classname(), [
                'data'=>[1 => 'Rendah (Low)', 2 => 'Sedang  (Medium)', 3 => 'Tinggi (High)'],
                'options'=>['prompt'=>'--- Pilih '.$model->getAttributeLabel('dissatisfaction_level').' ---'],
                'pluginOptions'=>['allowClear'=>true],
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'documentation')->textarea(['rows' => 3]) ?>
    
    <?= $form->field($model, 'resources')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 2]) ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>
 
    <?php ActiveForm::end(); ?>
</div>
