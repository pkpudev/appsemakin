<?php

use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Initiatives */
?>
<div class="initiatives-view">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <legend>Rincian Program Kerja/IKU-IS</legend>
                </div>
                <div class="col-md-6">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'initiatives_code',
                            'workplan_name:ntext',
                            'implementation',
                            [
                                'attribute' => 'total_budget',
                                'format' => 'html',
                                'value' => 'Rp. ' . number_format($model->total_budget, 2, ',', '.'),
                            ],
                            'initiatives:ntext',
                            [
                                'attribute' => 'target',
                                'value' => number_format($model->target, 0, ',', '.') . ' ' . $model->measure,
                            ],
                            [
                                'attribute' => 'target_q1',
                                'value' => number_format($model->target_q1, 0, ',', '.') . ' ' . $model->measure,
                            ],
                            [
                                'attribute' => 'target_q2',
                                'value' => number_format($model->target_q2, 0, ',', '.') . ' ' . $model->measure,
                            ],
                            [
                                'attribute' => 'target_q3',
                                'value' => number_format($model->target_q3, 0, ',', '.') . ' ' . $model->measure,
                            ],
                            [
                                'attribute' => 'target_q4',
                                'value' => number_format($model->target_q4, 0, ',', '.') . ' ' . $model->measure,
                            ],
                            [
                                'attribute' => 'participatory_budgeting_id',
                                'value' => ($model->participatory) ? $model->participatory->participatory_budgeting:'',
                            ]
                        ],
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'attribute' => 'position_structure_id',
                                'value' => $model->position_structure_id ? $model->position->position_name :''
                            ],
                            [
                                'attribute'=>'validity_id',
                                'value'=>($model->validity) ? $model->validity->validity_type : '',
                            ],
                            [
                                'attribute'=>'controllability_id',
                                'value'=>($model->controllability) ? $model->controllability->degree_of_controllability : '',
                            ],
                            [
                                'attribute'=>'polarization_id',
                                'value'=>($model->polarization) ? $model->polarization->polarization_type : '',
                            ],
                            [
                                'attribute'=>'calculation_id',
                                'value'=>($model->calculation) ? $model->calculation->calculation_type : '',
                            ],
                            [
                                'label' => 'Sasaran Pelanggan',
                                'format' => 'html',
                                'value' => $model->customerList,
                            ],
                            [
                                'label' => 'Pemangku Kepentingan',
                                'format' => 'html',
                                'value' => $model->stakeholderList,
                            ],
                            'monitoring_tools',
                            'assumption:ntext',
                            'note:ntext',
                            'grade',
                            [
                                'attribute'=>'status_id',
                                'format'=>'html',
                                'value'=>$model->status->getStatusname($model->status_id),
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Budget -->
    <?php //echo if ($model->has_budget == true) { ?> 
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h4 class="box-title">Rencana Kegiatan & Anggaran</h4>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <?= GridView::widget([
                                'layout' => '{pager}{items}{pager}',
                                'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getBudgets(), 
                                'pagination' => ['pageSize' => 10, 'pageParam' => 'page-budgets']]),
                                'pager' => [
                                    'class' => yii\widgets\LinkPager::className(),
                                    'firstPageLabel' => 'First',
                                    'lastPageLabel' => 'Last'
                                ],
                                'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                                'headerRowOptions' => ['class' => 'x'],
                                'showPageSummary' => true,
                                'columns' => [
                                    [
                                        'class' => 'kartik\grid\SerialColumn',
                                        'width' => '30px',
                                    ],
                                    [
                                        'class' => 'kartik\grid\DataColumn',
                                        'attribute' => 'activity_id',
                                    	'value' => function ($model) {
                                    		return $model->activity_id ? $model->activity->activity_name:'';
                                    	},
                                    	// 'group' => true,
                                    ],
                                    // 'description:ntext',
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Akun Biaya',
                                        'attribute' => 'account_id',
                                        'value' => function ($model) {
                                            return $model->account_id ? $model->account->account_name : '-';
                                        },
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'attribute' => 'volume',
                                        'value' => function ($model) {
                                            return $model->volume . ' ' . $model->measure;
                                        },
                                        // 'pageSummary'=>'TOTAL',
                                        // 'pageSummaryOptions'=>['class'=>'text-right text-warning'],

                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Satuan',
                                        'attribute' => 'unit_amount',
                                        'hAlign' => 'right',
                                        'format' => ['decimal', 0],
                                        'pageSummary' => 'TOTAL',
                                        'pageSummaryOptions' => ['class' => 'text-right text-warning'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Jan',
                                        'attribute' => 'month_01',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_01 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Feb',
                                        'attribute' => 'month_02',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_02 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Mar',
                                        'attribute' => 'month_03',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_03 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Apr',
                                        'attribute' => 'month_04',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_04 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Mei',
                                        'attribute' => 'month_05',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_05 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Jun',
                                        'attribute' => 'month_06',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_06 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Jul',
                                        'attribute' => 'month_07',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_07 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Agu',
                                        'attribute' => 'month_08',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_08 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Sep',
                                        'attribute' => 'month_09',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_09 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Okt',
                                        'attribute' => 'month_10',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_10 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Nov',
                                        'attribute' => 'month_11',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_11 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'label' => 'Des',
                                        'attribute' => 'month_12',
                                        'hAlign' => 'right',
                                        'vAlign' => 'top',
                                        'width' => '30px',
                                        'contentOptions' => ['class' => 'vertical'],
                                        'format' => ['decimal', 0],
                                        'value' => function ($model) {
                                            return $model->month_12 ?: 0;
                                        },
                                        'pageSummary' => true,
                                        'pageSummaryOptions' => ['class' => 'vertical'],
                                    ],
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php //} ?>

    <!-- History -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h4 class="box-title">History</h4>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <?= GridView::widget([
                            'layout' => '{pager}{items}{pager}',
                            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getInitiativesHistories()->orderBy('stamp'), 'pagination' => ['pageSize' => 10, 'pageParam' => 'page-initiativeshistories']]),
                            'pager' => [
                                'class' => yii\widgets\LinkPager::className(),
                                'firstPageLabel' => 'First',
                                'lastPageLabel' => 'Last'
                            ],
                            'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                            'headerRowOptions' => ['class' => 'x'],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'width' => '175px',
                                    'attribute' => 'stamp',
                                    'value' => function ($model) {
                                        return ($model->stamp) ? date('d M Y H:i:s', strtotime($model->stamp)) : '-';
                                    },
                                    // 'format' => ['date', 'dd MM yyyy HH:mm:ss'],
                                ],
                                'action',
                                [
                                    'attribute' => 'by',
                                    'value' => 'by0.full_name'
                                ],
                                'comment:ntext',
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>