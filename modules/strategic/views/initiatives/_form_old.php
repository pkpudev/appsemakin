<?php

use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Json;
use \kartik\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\number\NumberControl;
use kartik\money\MaskMoney;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\widgets\Select2;
use app\models\Measure;
use app\models\MonitoringTools;

/* @var $this yii\web\View */
/* @var $model app\models\Initiatives */
/* @var $form yii\widgets\ActiveForm */

$measures = ArrayHelper::map(Measure::find()->where(['company_id' => Yii::$app->user->identity->company_id, 'is_active' => true])->orderBy('measure')->all(), 'measure', 'measure');
$monitoringTools = ArrayHelper::map(MonitoringTools::find()->where(['company_id' => Yii::$app->user->identity->company_id, 'is_active' => true])->orderBy('monitoring_tools')->all(), 'monitoring_tools', 'monitoring_tools');
// var_dump(Yii::$app->request->post());
// echo "<br /><br />";
// var_dump($model->deleted_source_ids);
// print_r($beneficiaryTypes);
?>

<div class="initiatives-form">

    <?php $form = ActiveForm::begin([
        'id' => 'initiatives',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error'
    ]); ?>

    <div class="box box-success">
        <?php echo $form->errorSummary(ArrayHelper::merge([$model], $model->budgets)); ?>
        <p>

            <?php /* $form->field($model, 'kpi_distribution_id')
                ->widget(Select2::classname(), [
                    'data' => $kpiList,
                    'options' => [
                        'placeholder' => '-- Pilih IKU --',
                    ],
                    'pluginOptions' => [
                        'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                    ],
                ]); */
            ?>

            <?= $form->field($model, 'workplan_name')->textarea(['rows' => 2]) ?>

            <!-- <?= $form->field($model, 'objective')->textarea(['rows' => 2]) ?> -->
            <?= $form->field($model, 'objectives')
                        ->widget(Select2::classname(), [
                            'data' => $objectives,
                            'options' => [
                                'placeholder' => '-- Pilih Sasaran Proker --',
                                'multiple' => true,
                            ],
                            'showToggleAll' => false,
                            // 'pluginOptions' => [
                            //     'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                            // ],
                        ])->label('Sasaran Pelanggan');
            ?>

            <?= $form->beginField($model, 'implementation') ?>
            <?= Html::activeLabel($model, 'implementation', ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-2">
                        <?= $form->field($model, 'allmon')->checkbox(['id' => 'allmon']) ?>
                    </div>
                    <div class="col-md-10">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <?= $form->field($model, 'jan')->checkbox(['class' => 'mon q1']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'feb')->checkbox(['class' => 'mon q1']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'mar')->checkbox(['class' => 'mon q1']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'apr')->checkbox(['class' => 'mon q2']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'may')->checkbox(['class' => 'mon q2']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'jun')->checkbox(['class' => 'mon q2']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <?= $form->field($model, 'jul')->checkbox(['class' => 'mon q3']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'aug')->checkbox(['class' => 'mon q3']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'sep')->checkbox(['class' => 'mon q3']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'oct')->checkbox(['class' => 'mon q4']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'nov')->checkbox(['class' => 'mon q4']) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'dec')->checkbox(['class' => 'mon q4']) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-offset-2 col-md-10">
                <?= Html::error($model, 'implementation', ['class' => 'help-block']) ?>
            </div>
            <?= $form->endField() ?>

            <?= $form->field($model, 'initiatives')->textarea(['rows' => 2]) ?>

            <?= $form->beginField($model, 'target') ?>
            <?= Html::activeLabel($model, 'target', ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'target_q1')->label("Jan-Mar")->input('number', ['min' => 1, 'placeholder' => 'K1', 'id' => 'tq1', 'class' => 'tq']) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'target_q2')->label("Apr-Jun")->input('number', ['min' => 1, 'placeholder' => 'K2', 'id' => 'tq2', 'class' => 'tq']) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'target_q3')->label("Jul-Sep")->input('number', ['min' => 1, 'placeholder' => 'K3', 'id' => 'tq3', 'class' => 'tq']) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'target_q4')->label("Okt-Des")->input('number', ['min' => 1, 'placeholder' => 'K4', 'id' => 'tq4', 'class' => 'tq']) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'target')->label("Jumlah")->error(false)->input('number', ['min' => 1, 'placeholder' => 'Total', 'readonly' => true, 'id' => 'target']) ?>
            </div>
            <div class="col-md-offset-2 col-md-10">
                <?= Html::error($model, 'target', ['class' => 'help-block']) ?>
            </div>
            <?= $form->endField() ?>

            <?= $form->field($model, 'measure')->widget(Select2::classname(), [
                            'data' => $measures,
                            'options' => [
                                'placeholder' => '-- Pilih Jenis Ukuran --',
                            ],
                            // 'pluginOptions' => [
                            //     'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                            // ],
                        ]); ?>

            <?= $form->beginField($model, 'involvement_dept_ids') ?>
            <?= Html::activeLabel($model, 'involvement_dept_ids', ['class' => 'control-label col-md-2']) ?>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'involvement_all_depts')->checkbox(['id' => 'involve-all']) ?>
                    </div>
                    <div class="col-md-9">
                        <?= Select2::widget([
                            'model' => $model,
                            'attribute' => "involvement_dept_ids",
                            'data' => $departments,
                            'options' => [
                                'id' => 'involve-depts',
                                'placeholder' => '-- Pilih Divisi --',
                                'multiple' => true,
                            ],
                            'showToggleAll' => false,
                        ])
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-offset-2 col-md-10">
                <?= Html::error($model, 'involvement_dept_ids', ['class' => 'help-block']) ?>
            </div>
            <?= $form->endField() ?>

            <!-- <?= $form->field($model, 'monitoring_tool_vals')
                            ->widget(Select2::classname(), [
                                'data' => $monitoringTools,
                                'options' => [
                                    'placeholder' => '-- Pilih Media Monitoring --',
                                    'multiple' => true,
                                ],
                                'showToggleAll' => false,
                                'pluginOptions' => [
                                    'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                                ],
                            ]);
                    ?> -->

            <?= $form->field($model, 'monitoring_tools')->textarea(['rows' => 2]) ?>
            <?php //= $form->field($model, 'monitoring_tools')->dropDownList($monitoringTools, ['prompt' => '-- Pilih Media Monitoring --']) 
            ?>

            <?= $form->field($model, 'pic_text')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'assumption')->textarea(['rows' => 2]) ?>

            <?= $form->field($model, 'note')->textarea(['rows' => 2]) ?>

            <?= $form->field($model, 'has_budget')->hint('Checklist bila ingin membuat anggaran')->checkbox(['id' => 'ask-has-budget']) ?>
            <?php //var_dump($beneficiaryTypes) 
            ?>
            <div id="has-budget">

                <?= $form->field($model, 'total_budget_plan')
                            ->widget(MaskMoney::classname())
                        // ->widget(NumberControl::classname(), ['maskedInputOptions' => ['prefix' => 'Rp. ', 'groupSeparator' => '.', 'radixPoint' => ','],]) 
                ?>

                <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper',
                            'widgetBody' => '.container-items',
                            'widgetItem' => '.source-item',
                            'limit' => 10,
                            'min' => 1,
                            'insertButton' => '.add-source',
                            'deleteButton' => '.remove-source',
                            'model' => $budgetSourceModel,
                            'formId' => 'initiatives',
                            'formFields' => [
                                'source_of_funds_id',
                                'amount',
                                'asnaf',
                            ],

                        ]); ?>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Sumber Dana</th>
                            <th>Alokasi Anggaran</th>
                            <th>Penerima Manfaat</th>
                            <th class="text-center" style="width: 90px;">
                                <button type="button" class="add-source btn btn-success btn-xs"><span class="fa fa-plus"></span></button>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="container-items">
                        <?php $form->formConfig['showLabels'] = false; ?>
                        <?php if (empty($model->budgets)) $model->budgets = [$budgetSourceModel]; ?>
                        <?php foreach ($model->budgets as $index => $fundSource) : ?>
                            <tr class="source-item">
                                <td class="no"><?= ($index + 1) ?></td>
                                <td class="vcenter">
                                    <?php
                                                // necessary for update action.
                                                if (!$fundSource->isNewRecord) {
                                                    echo Html::activeHiddenInput($fundSource, "[{$index}]id");
                                                }
                                    ?>
                                    <?= $form->field($fundSource, "[{$index}]source_of_funds_id")
                                                    ->dropDownList($sourceOfFunds, ['prompt' => '-- Sumber Dana --'])
                                    ?>
                                </td>
                                <td>
                                    <?= $form->field($fundSource, "[{$index}]amount")
                                                    ->widget(MaskMoney::classname(), ['options' => ['class' => 'budget-amount']])
                                                // ->widget(NumberControl::classname(), ['maskedInputOptions' => ['prefix' => 'Rp. ', 'groupSeparator' => '.', 'radixPoint' => ','],'options'=>['class'=>'budget-amount']])
                                    ?>
                                </td>
                                <td>
                                    <?= $form->field($fundSource, "[{$index}]asnaf")
                                                    ->widget(Select2::classname(), [
                                                        'data' => $beneficiaryTypes,
                                                        'options' => [
                                                            'placeholder' => '-- Pilih Penerima Manfaat --',
                                                            'multiple' => true
                                                        ],
                                                    ])
                                    ?>
                                </td>
                                <td class="text-center vcenter" style="width: 90px; verti">
                                    <button type="button" class="remove-source btn btn-danger btn-xs"><span class="fa fa-minus"></span></button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td style="text-align: right;"><?= Html::activeLabel($model, 'verify_total_budget_plan', ['class' => 'control-label']) ?></td>
                            <td>
                                <?= $form->field($model, 'verify_total_budget_plan')
                                                                ->label(false)
                                                                ->widget(MaskMoney::classname(), ['disabled' => true, 'options' => ['id' => 'budget-total']])
                                ?>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
                <?php DynamicFormWidget::end(); ?>
            </div>

        </p>
        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<?php
$vars = Json::encode([
    'kpiDivisions' => $kpiDivisions,
]);
$headJs =
    <<<HEADJS
var vars = {$vars};
HEADJS;
$this->registerJs($headJs, View::POS_HEAD);
$this->registerJs(file_get_contents(__DIR__ . '/js/_form.js'));
