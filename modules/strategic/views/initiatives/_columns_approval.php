<?php

use yii\helpers\Html;
use yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\search\ActivitySearch;

return [
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'unit_id',
        'filter' => false,
        'format' => 'html',
        'value' => function ($model, $key, $index, $widget) {
            return '<b>' . $model->unit->unit_code . ' ' . $model->unit->unit_name . '</b>';
        },
        'group' => true,
        'groupedRow' => true,                    // move grouped column to a single grouped row
        'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
        /*'groupFooter' => function ($model) { // Closure method
            return [
                'mergeColumns' => [[2, 5]], // columns to merge in summary
                'content' => [             // content to show in each summary cell
                    2 => 'TOTAL (' . strtoupper($model->unit->unit_name) . ')',
                    6 => GridView::F_SUM,
                    9 => GridView::F_SUM,
                ],
                'contentFormats' => [      // content reformatting for each summary cell
                    6 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    9 => ['format' => 'number', 'decimals' => 0],
                ],
                'contentOptions' => [      // content html attributes for each summary cell
                    2 => ['style' => 'font-variant:small-caps;text-align:right'],
                    6 => ['style' => 'text-align:right'],
                ],
                // html attributes for group summary row
                'options' => ['class' => 'success', 'style' => 'font-weight:bold;']
            ];
        }*/
    ],
    /* [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '50px',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail' => function ($model, $key, $index, $column) {
            $query = ActivitySearch::find()->alias('a')
                ->joinWith(['initiative i','budgets b'])
                ->select("a.*, b.measure as budget_measure, b.*")
                ->where(['a.strategic_initiatives_id'=>$model->id])
                ->orderBy(['a.start_plan'=>SORT_ASC,'a.id'=>SORT_ASC]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]);
            return Yii::$app->controller->renderPartial('_columns_collapse', [
                'dataProvider' => $dataProvider,
            ]);
        },
        'expandIcon' => '<span class="glyphicon glyphicon-expand"></span>',
        'collapseIcon' => '<span class="glyphicon glyphicon-collapse-down"></span>',
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'kode_kpi',
        'label'=>'Kode IKU',
        'value'=>function($model){
            return $model->kpiDistribution?$model->kpiDistribution->kpi_distribution_code:'-';
        },
        'group'=>true,
        'subGroupOf'=>2
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Deskripsi IKU',
        'attribute'=>'kpi',
        'value'=>function($model){
            return $model->kpiDistribution->kpi?$model->kpiDistribution->kpi->description:'-';
        },
        'group'=>true,
        'subGroupOf'=>2
    ],
    /*[
    	'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'header'=>'No',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Program Kerja',
        'attribute'=>'workplan_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Pelaksanaan',
        'attribute'=>'implementation',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Jumlah Anggaran',
        'attribute'=>'total_budget',
        'hAlign'=>'right',
        'format'=>['decimal',0],
        'value'=>function($model) {
            return ($model->total_budget) ? : 0;
        },
        'pageSummary' => true,
        // 'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Kode IKU-IS',
        'attribute'=>'initiatives_code',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Deskripsi IKU-IS',
        'attribute'=>'initiatives',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Target',
        'attribute'=>'target',
        'hAlign'=>'right',
        'width'=>'60px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Ukuran',
        'attribute'=>'measure',
        'width'=>'75px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'grade',
        'width'=>'75px',
        'pageSummary' => true,
        // 'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ], */
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'validity_id',
        'value'=>function($model) {
            return ($model->initiative->validity) ? $model->initiative->validity->validity_type : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'controllability_id',
        'value'=>function($model) {
            return ($model->initiative->controllability) ? $model->initiative->controllability->degree_of_controllability : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Media Monitoring',
        'attribute'=>'i_tools',
        'value'=>function($model) {
            return ($model->initiative) ? $model->initiative->monitoring_tools : '';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'PJ Pencapaian',
        'attribute'=>'pic_text',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Catatan',
        'attribute'=>'note',
    ],*/
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template'=>'{view} {approve} {revision} {reject}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-eye-open"></span>',
                    $url, 
                    [
                        'role'=>'modal-remote','title' => Yii::t('app', 'View'),'data-toggle'=>'tooltip',
                    ]
                );
            },
            'approve' => function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-ok"></span>',
                    $url, 
                    [
                        'role'=>'modal-remote','title' => Yii::t('app', 'Approve'),'data-toggle'=>'tooltip',
                    ]
                );
            },
            'revision' => function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-edit"></span>',
                    $url, 
                    [
                        'role'=>'modal-remote','title' => Yii::t('app', 'Revisi'),'data-toggle'=>'tooltip',
                    ]
                );
            },
            'reject' => function ($url, $model) {
                return Html::a(
                    '<span class="glyphicon glyphicon-remove"></span>',
                    $url, 
                    [
                        'role'=>'modal-remote','title' => Yii::t('app', 'Reject'),'data-toggle'=>'tooltip',
                    ]
                );
            },
        ],
    ],

];

$this->title = 'Approval Program Kerja';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="initiatives-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => $column,
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ],
            ],          
            'striped' => true,
            'hover'=>true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,         
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="fa fa-tasks"></i> List Rencana Program Kerja',
                // 'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-ok"></i>&nbsp; Approve All',
                                ["bulk-approve"] ,
                                [
                                    "class"=>"btn btn-success btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-modal-size'=>'large',
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Anda menyetujui item yang dipilih?',
                                    'data-confirm-message'=>Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:100%;'])
                                ]).' '.Html::a('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Revision All',
                                ["bulk-revision"] ,
                                [
                                    "class"=>"btn btn-warning btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-modal-size'=>'large',
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Anda akan meminta revisi item yang dipilih?',
                                    'data-confirm-message'=>Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:100%;'])
                                ]).' '.Html::a('<i class="glyphicon glyphicon-remove"></i>&nbsp; Reject All',
                                ["bulk-reject"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-modal-size'=>'large',
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Anda akan menolak item yang dipilih?',
                                    'data-confirm-message'=>Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:100%;'])
                                ]),
                        ]).
                        '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>