<?php

/* @var $this yii\web\View */
/* @var $model app\models\Initiatives */

?>
<div class="initiatives-create">
    <?= $this->render('_form', [
            'model' => $model,
            'objectives' => $objectives,
            'objectiveList' => $objectiveList,
            'objectiveDivisions' => $objectiveDivisions,
            'participatories' => $participatories
    ]) ?>
</div>
