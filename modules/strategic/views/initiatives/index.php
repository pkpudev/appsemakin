<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
// use app\components\widgets\Modal;
// use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\select2\Select2;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\Departments;
use app\models\ParticipatoryBudgeting;
use app\models\UnitStructure;
use kartik\widgets\DatePicker;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\InitiativesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rencana Program Kerja (RPK)';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<?php 
    $textAnggaran = '';
    foreach($info as $i){
        $textAnggaran .= "Total Anggaran unit " . $i['unit'] . " yang telah dibuat : <b>Rp. " . ($i['total_budget'] ? number_format($i['total_budget'], 0, ',', '.') : 0) . 
        "</b>. Pagu Anggaran Unit " . $i['unit'] ." : <b>Rp. " . ($i['ceiling_amount'] ? number_format($i['ceiling_amount'], 0, ',', '.') : 0) . "</b>.<br />";
    }?>
<div class="initiatives-index">
    <?php if ($info) : ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="fa fa-bullhorn"></i> Info!</h4>
                <?= $textAnggaran; ?>
            </div>
    <?php endif; ?>
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns.php'),
            'toolbar' => [
                [
                    'content' => (/* (($info or Yii::$app->user->can('Admin'))) */ 1==1 ?
                        Html::a('<i class="glyphicon glyphicon-plus"></i> Tambah Program Kerja', ['create'], ['role' => 'modal-remote', 'title' => 'Tambah Program Kerja', 'class' => 'btn btn-success']) .
                        Html::a(
                            '<i class="glyphicon glyphicon-send"></i>&nbsp; Kirim',
                            ['send'],
                            [
                                'role' => 'modal-remote',
                                'title' => 'Kirim untuk proses Approval oleh BOD',
                                'class' => 'btn btn-warning',
                                'data-modal-size' => 'large',
                                'data-request-method' => 'post',
                                'data-confirm-title' => '<b>Anda yakin untuk mengirim RKAT ' . $searchModel->year . '?</b>',
                                'data-confirm-message' => (Yii::$app->user->can('Admin')) ?
                                    Html::hiddenInput('send-year', $searchModel->year) .
                                    Html::dropDownList(
                                                        'unit_structure_id', '', 
                                                        $listUnit,
                                                        [
                                                            'prompt' => '-- Pilih Unit --',
                                                            'class' => 'form-control'
                                                        ]
                                                    ) .
                                    Html::textarea('confirm-comment', null, ['placeholder' => 'Komentar/Pesan', 'rows' => 3, 'class' => 'form-control', 'style' => 'margin-top: 5px;']) .
                                        '<em>Klik <span class="label label-primary">OK</span> maka :<br>* Program Kerja dan Anggaran yang berstatus <b>DRAFT/REVISION</b> akan berubah status menjadi <b>SEND</b>.<br>* Data yang berstatus <b>SEND</b> tidak dapat Anda edit.</em>'
                                    :
                                    '<div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="fa fa-bullhorn"></i> Perhatian!</h4>
                                        ' . $textAnggaran . '
                                    </div>' . 
                                    Html::dropDownList(
                                                        'unit_structure_id', '', 
                                                        $listUnit,
                                                        [
                                                            'prompt' => '-- Pilih Unit --',
                                                            'class' => 'form-control'
                                                        ]
                                                    ) .
                                    Html::hiddenInput('send-year', $searchModel->year) . Html::textarea('confirm-comment', null, ['placeholder' => 'Komentar/Pesan', 'rows' => 3, 'class' => 'form-control', 'style' => 'margin-top: 5px;']) .
                                        '<em>Klik <span class="label label-primary">OK</span> maka :<br>* Program Kerja dan Anggaran yang berstatus <b>DRAFT/REVISION</b> akan berubah status menjadi <b>SEND</b>.<br>* Data yang berstatus <b>SEND</b> tidak dapat Anda edit.</em>',
                            ]
                        ): '') .
                        // '&nbsp;<em>* Klik kirim untuk proses Approval oleh BOD.</em>'.
                        Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']
                        ) .
                        '{toggleData}' .
                        Html::a(
                            '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                            '#',
                            ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Download Excel']
                        )
                        //'{export}'
                ],
            ],
            'striped' => true,
            'hover' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-tasks"></i> Daftar Rencana Program Kerja',
                'before' => /* (Yii::$app->user->can('Admin') or Yii::$app->user->can('BOD')) ? */
                    '<div class="row">' .
                    '<div class="col-md-2">' .
                        DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'year',
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'id' => 'year-filter',
                                'placeholder' => 'Filter Tahun',
                                'class' => 'form-control additional-filter',
                            ],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'startView'=>'year',
                                'minViewMode'=>'years',
                                'format' => 'yyyy',
                                'allowClear' => false
                            ]
                        ]) .
                    '</div>' .
                    '<div class="col-md-3">' .
                        Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'unit_id',
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'id' => 'divisi-filter',
                                'class' => 'form-control additional-filter',
                                'placeholder' => '-- Filter Unit Kerja --',
                            ],
                            'pluginOptions' => ['allowClear' => true],
                            'data' => $listUnit,
                        ]) .
                    '</div>' .
                    '</div>'
                    /* : '<div class="col-md-2">' .
                        DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'year',
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'placeholder' => 'Filter Tahun',
                                'class' => 'form-control additional-filter',
                            ],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'startView'=>'year',
                                'minViewMode'=>'years',
                                'format' => 'yyyy',
                                'allowClear' => false
                            ]
                        ]) .
                    '</div>' */,
                    'after' => BulkButtonWidget::widget([
                        'buttons' =>
                        Html::a(
                            '<i class="glyphicon glyphicon-trash"></i>&nbsp; Pilih Jenis Anggaran Partisipatory',
                            ["bulk-set-participatory"],
                            [
                                "class" => "btn btn-info btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                'data-request-method' => 'post',
                                'data-modal-size' => 'large',
                                'data-confirm-title' => 'Pilih Jenis Anggaran Partisipatory',
                                'data-confirm-message' =>  Html::dropDownList('participatory', null, ArrayHelper::map(ParticipatoryBudgeting::find()->where(['is_active' => true])->orderBy('id')->all(), 'id', 'participatory_budgeting'), ['class' => 'form-control']),
                            ]
                        ),
                    ]) .
                        '<div class="clearfix"></div>',
            ]
        ]) ?>
    </div>
</div>

<?= Html::beginForm(['download'], 'post', ['id' => 'download-form']) ?>
<div></div>
<?= Html::endForm() ?>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    // 'tabindex' => null,
    "footer" => "", // always need it for jquery plugin
    "size" => Modal::SIZE_LARGE,
    // 'options'=>[
    //     'tabindex'=>false,
    // ],
]) ?>
<?php Modal::end(); ?>

<?php
    $jsReady = file_get_contents(__DIR__ . '/js/index.ready.js');
    $this->registerJs($jsReady, View::POS_READY);
?>