<?php

use app\models\Calculation;
use app\models\Controllability;
use app\models\Customers;
use app\models\InitiativesCustomers;
use app\models\InitiativesStakeholder;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Json;
use \kartik\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\number\NumberControl;
use kartik\money\MaskMoney;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\widgets\Select2;
use app\models\Measure;
use app\models\MonitoringTools;
use app\models\Polarization;
use app\models\Stakeholder;
use app\models\Validity;

/* @var $this yii\web\View */
/* @var $model app\models\Initiatives */
/* @var $form yii\widgets\ActiveForm */

$measures = ArrayHelper::map(Measure::find()->where(['is_active' => true])->orderBy('measure asc')->all(), 'measure', 'measure');
$customers = ArrayHelper::map(Customers::find()->where(['is_active' => true])->orderBy('customer')->all(), 'id', 'customer');
$stakeholder = ArrayHelper::map(Stakeholder::find()->orderBy('stakeholder')->all(), 'id', 'stakeholder');
?>

<div class="initiatives-form">

    <?php $form = ActiveForm::begin([
        'id' => 'initiatives',
        'type' => ActiveForm::TYPE_VERTICAL,
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error'
    ]); ?>

    <?php echo $form->errorSummary(ArrayHelper::merge([$model], $model->budgets)); ?>
    <p>

        <?= $form->field($model, 'okr_id')
            ->widget(Select2::classname(), [
                'data' => $objectiveList,
                'initValueText' => $model->okr_id ? $model->okr->okr_code . ' - ' . $model->okr->okr : '',
                'options' => [
                    'placeholder' => '-- Pilih Objective --',
                    'value' => $model->okr_id
                ],
                'pluginOptions' => [
                    'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                ],
            ]);
        ?>

        <?= $form->field($model, 'workplan_name')->textarea(['rows' => 2]) ?>

        <?= $form->field($model, 'initiatives')->textarea(['rows' => 2]) ?>

        <?php 
            if(!$model->isNewRecord){
                $idCust = [];
                $customerss = InitiativesCustomers::find()->where(['initiatives_id' => $model->id])->all();
                foreach($customerss as $cust){
                    array_push($idCust, $cust->customers_id);
                }
                $model->objectives =  $idCust; 
            }
        ?>
        <?= $form->field($model, 'objectives')
                    ->widget(Select2::classname(), [
                        'data' => $customers,
                        'options' => [
                            'placeholder' => '-- Pilih Sasaran Proker --',
                            'multiple' => true,
                        ],
                        'showToggleAll' => false,
                        // 'pluginOptions' => [
                        //     'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                        // ],
                    ])->label('Sasaran Pelanggan');
        ?>

        <div class="row">
            <div class="col-md-3">
                <label>Polarization</label>
                <?= Html::activeDropDownList(
                                                $model, 
                                                'polarization_id',
                                                ArrayHelper::map(Polarization::find()->where(['is_active' => true])->asArray()->all(), 'id', 'polarization_type'), 
                                                [
                                                    'label' => 'Polarization',
                                                    'prompt' => '-- Pilih Polarization --',
                                                    'class' => 'form-control'
                                                ]
                                            ) ?>
            </div>
            <div class="col-md-3">
                <label>Calculation</label>
                <?= Html::activeDropDownList(
                                                $model, 
                                                'calculation_id',
                                                ArrayHelper::map(Calculation::find()->where(['is_active' => true])->asArray()->all(), 'id', 'calculation_type'),
                                                [
                                                    'label' => 'Calculation',
                                                    'prompt' => '-- Pilih Calculation --',
                                                    'class' => 'form-control'
                                                ]
                                            ) ?>
            </div>
            <div class="col-md-3">
                <label>Nilai Validitas</label>
                <?= Html::activeDropDownList(
                                                $model, 
                                                'validity_id',
                                                ArrayHelper::map(Validity::find()->where(['is_active' => true])->asArray()->all(), 'id', 'validity_type'),
                                                [
                                                    'label' => 'Calculation',
                                                    'prompt' => '-- Pilih Nilai Validitas --',
                                                    'class' => 'form-control'
                                                ]
                                            ) ?>
            </div>
            <div class="col-md-3">
                <label>Tingkat Pengendalian</label>
                <?= Html::activeDropDownList(
                                                $model, 
                                                'controllability_id',
                                                ArrayHelper::map(Controllability::find()->where(['is_active' => true])->asArray()->all(), 'id', 'degree_of_controllability'),
                                                [
                                                    'label' => 'Tingkat Pengendalian',
                                                    'prompt' => '-- Pilih Tingkat Pengendalian --',
                                                    'class' => 'form-control'
                                                ]
                                            ) ?>
            </div>
        </div>

        <?= $form->beginField($model, 'implementation') ?>
        <?= Html::activeLabel($model, 'implementation', ['class' => 'control-label col-md-12', 'style' => 'padding-left: 0;']) ?>
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'allmon')->checkbox(['id' => 'allmon']) ?>
                </div>
                <div class="col-md-10">

                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'jan')->checkbox(['class' => 'mon q1']) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'feb')->checkbox(['class' => 'mon q1']) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'mar')->checkbox(['class' => 'mon q1']) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'apr')->checkbox(['class' => 'mon q2']) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'may')->checkbox(['class' => 'mon q2']) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'jun')->checkbox(['class' => 'mon q2']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'jul')->checkbox(['class' => 'mon q3']) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'aug')->checkbox(['class' => 'mon q3']) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'sep')->checkbox(['class' => 'mon q3']) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'oct')->checkbox(['class' => 'mon q4']) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'nov')->checkbox(['class' => 'mon q4']) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'dec')->checkbox(['class' => 'mon q4']) ?>
                </div>
            </div>
        <div class="col-md-offset-2 col-md-10">
            <?= Html::error($model, 'implementation', ['class' => 'help-block']) ?>
        </div>
        <?= $form->endField() ?>

        <?= $form->beginField($model, 'target') ?>
        <?= Html::activeLabel($model, 'target', ['class' => 'control-label col-md-12', 'style' => 'padding-left: 0;']) ?>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'target_q1')->label("Jan-Mar")->input('number', ['min' => 0, 'placeholder' => 'K1', 'id' => 'tq1', 'class' => 'tq']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'target_q2')->label("Apr-Jun")->input('number', ['min' => 0, 'placeholder' => 'K2', 'id' => 'tq2', 'class' => 'tq']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'target_q3')->label("Jul-Sep")->input('number', ['min' => 0, 'placeholder' => 'K3', 'id' => 'tq3', 'class' => 'tq']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'target_q4')->label("Okt-Des")->input('number', ['min' => 0, 'placeholder' => 'K4', 'id' => 'tq4', 'class' => 'tq']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'target')->label("Jumlah")->error(false)->input('number', ['min' => 1, 'placeholder' => 'Total', 'readonly' => true, 'id' => 'target']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'measure')->widget(Select2::classname(), [
                    'data' => $measures,
                    'options' => [
                        'placeholder' => '-- Pilih Jenis Ukuran --',
                    ],
                    // 'pluginOptions' => [
                    //     'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                    // ],
                ]); ?>
            </div>
        </div>

        <div class="col-md-offset-2 col-md-10">
            <?= Html::error($model, 'target', ['class' => 'help-block']) ?>
        </div>
        <?= $form->endField() ?>

        <?= $form->beginField($model, 'involvement_dept_ids') ?>
        <?= Html::activeLabel($model, 'involvement_dept_ids', ['class' => 'control-label']) ?>
        
        <div class="row">

            <?php 
                if(!$model->isNewRecord){
                    $idStake = [];
                    $stakeholders = InitiativesStakeholder::find()->where(['initiatives_id' => $model->id])->all();
                    foreach($stakeholders as $stake){
                        array_push($idStake, $stake->stakeholder_id);
                    }
                    $model->involvement_dept_ids =  $idStake; 
                }
            ?>
            <div class="col-md-12">
                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => "involvement_dept_ids",
                    'data' => $stakeholder,
                    'options' => [
                        'id' => 'involve-depts',
                        'placeholder' => '-- Pilih Pemangku Kepentingan --',
                        'multiple' => true,
                    ],
                    'showToggleAll' => false,
                ])
                ?>
            </div>
        </div>
            
        <div class="col-md-offset-2 col-md-10">
            <?= Html::error($model, 'involvement_dept_ids', ['class' => 'help-block']) ?>
        </div>
        <?= $form->endField() ?>

        <!-- <?= $form->field($model, 'monitoring_tool_vals')
                        ->widget(Select2::classname(), [
                            'data' => $monitoringTools,
                            'options' => [
                                'placeholder' => '-- Pilih Media Monitoring --',
                                'multiple' => true,
                            ],
                            'showToggleAll' => false,
                            'pluginOptions' => [
                                'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                            ],
                        ]);
                ?> -->

        <?= $form->field($model, 'monitoring_tools')->textarea(['rows' => 2]) ?>
        <?php //= $form->field($model, 'monitoring_tools')->dropDownList($monitoringTools, ['prompt' => '-- Pilih Media Monitoring --']) 
        ?>

        <!-- <?php // $form->field($model, 'pic_text')->textInput(['maxlength' => true]) ?> -->

        <?= $form->field($model, 'participatory_budgeting_id')->widget(Select2::classname(), [
            'data' => $participatories,
            'options' => [
                'placeholder' => '-- Pilih Jenis Anggaran Partisipatori --',
            ],
            // 'pluginOptions' => [
            //     'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
            // ],
        ]); ?>
        
        <?php // $form->field($model, 'assumption')->textarea(['rows' => 2]) ?>

        <?= $form->field($model, 'note')->textarea(['rows' => 2]) ?>

    </p>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

<?php
$vars = Json::encode([
    'kpiDivisions' => $kpiDivisions,
]);
$headJs =
    <<<HEADJS
var vars = {$vars};
HEADJS;
$this->registerJs($headJs, View::POS_HEAD);
$this->registerJs(file_get_contents(__DIR__ . '/js/_form.js'));
