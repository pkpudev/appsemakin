<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\select2\Select2;
use app\models\Departments;
use app\models\CostType;
use app\models\CostPriority;
use app\models\FinancingType;
use app\models\SourceOfFunds;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BudgetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rencana Anggaran Kerja (RAK)';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>
.vertical {
  writing-mode: vertical-rl;
  line-height:2rem; 
  transform:rotate(0deg); 
  -moz-transform: rotate(-180deg);
}
</style>
<div class="budget-index">
    <?php if ($info) : ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="fa fa-bullhorn"></i> Info!</h4>
                <?php foreach($info as $i): ?>
                    Total Anggaran unit <?= $i['unit']; ?> yang telah dibuat : <b>Rp. <?= $i['total_budget'] ? number_format($i['total_budget'], 2, ',', '.') : 0 ?></b>. Pagu Anggaran Unit <?= $i['unit']; ?> : <b>Rp. <?= $i['ceiling_amount'] ? number_format($i['ceiling_amount'], 2, ',', '.') : 0 ?></b><br />
                <?php endforeach; ?>
            </div>
    <?php endif; ?>
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'budget-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'showPageSummary' => true,
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns2.php'),
            'toolbar' => [
                [
                    'content' => (/* (($info or Yii::$app->user->can('Admin')) and $searchModel->lock == false) */ 1==0 ? Html::a('<i class="glyphicon glyphicon-plus"></i> Tambah Anggaran', ['create-all'], ['data-pjax' => 0, 'title' => 'Tambah Anggaran', 'class' => 'btn btn-success']) : '') .
                        Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']
                        ) .
                        '{toggleData}' .
                        Html::a(
                            '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                            '#',
                            ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Excel']
                        )
                        .'{export}'
                ],
            ],
            'striped' => true,
            'hover' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-money"></i> Rencana Kerja & Anggaran',
                'before' => /* (Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')) ? */
                    '<div class="row">' .
                        '<div class="col-md-2">' .
                            DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'year',
                                'pjaxContainerId' => 'crud-datatable-pjax',
                                'options' => [
                                    'id' => 'year-filter',
                                    'placeholder' => 'Filter Tahun',
                                    'class' => 'form-control additional-filter',
                                ],
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'startView'=>'year',
                                    'minViewMode'=>'years',
                                    'format' => 'yyyy',
                                    'allowClear' => false
                                ]
                            ]) .
                        '</div>' .
                        '<div class="col-md-3">' .
                            Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'unit_id',
                                'pjaxContainerId' => 'budget-datatable-pjax',
                                'options' => [
                                    'id' => 'divisi-filter',
                                    'class' => 'form-control additional-filter',
                                    'placeholder' => '-- Filter Unit Kerja --',
                                ],
                                'pluginOptions' => ['allowClear' => true],
                                'data' => $listUnit,
                            ]) .
                        '</div>' .
                        '<div class="col-md-2">' .
                            Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'position_structure_id',
                                'pjaxContainerId' => 'budget-datatable-pjax',
                                'options' => [
                                    'id' => 'jabatan-filter',
                                    'class' => 'form-control additional-filter',
                                    'placeholder' => '-- Filter Jabatan --',
                                ],
                                'pluginOptions' => ['allowClear' => true],
                                'data' => $listPosition,
                            ]) .
                        '</div>' .
                    '</div>'
                    /*Html::a('<i class="glyphicon glyphicon-send"></i>&nbsp; Kirim', ['send'],
                    [
                        'role'=>'modal-remote',
                        'title'=> 'Kirim untuk proses Approval',
                        'class'=>'btn btn-warning',
                        'data-modal-size'=>'large',
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'<b>Anda yakin untuk mengirim RKAT?</b>',
                        'data-confirm-message'=>
                            (Yii::$app->user->can('Admin'))?
                            Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:90%;']).
                            '<br><br><em>Klik <span class="label label-primary">OK</span> maka :<br>* Program Kerja dan Anggaran yang berstatus <b>DRAFT/REVISION</b> akan berubah status menjadi <b>SEND</b>.<br>* Data yang berstatus <b>SEND</b> tidak dapat Anda edit.</em>'
                            :
                            '<div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="fa fa-bullhorn"></i> Perhatian!</h4>
                                    Total Anggaran Divisi Anda yang telah dibuat : <b>Rp. '.number_format($info->total_budget,0,',','.').'</b>. Plafon Anggaran Divisi Anda : <b>Rp. '.number_format($info->quota,0,',','.').'</b>
                            </div>'.Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:90%;']).
                            '<br><br><em>Klik <span class="label label-primary">OK</span> maka :<br>* Program Kerja dan Anggaran yang berstatus <b>DRAFT/REVISION</b> akan berubah status menjadi <b>SEND</b>.<br>* Data yang berstatus <b>SEND</b> tidak dapat Anda edit.</em>',
                    ]).
                    '&nbsp;<em>* Klik kirim untuk proses Approval oleh BOD.</em>'*/
                    /* : '<div class="col-md-2">' .
                        Select2::widget([
                            // 'value' => $currentYear,
                            'model' => $searchModel,
                            'attribute' => 'year',
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'id' => 'year-filter',
                                'class' => 'form-control additional-filter',
                                // 'placeholder'=>'-- Filter Tahun --',
                            ],
                            'pluginOptions' => ['allowClear' => false],
                            'data' => $yearItems,
                        ]) */,
                    'after' => BulkButtonWidget::widget([
                        'buttons' => Html::a(
                            'Pilih Sumber Dana',
                            ["bulk-source-of-fund"],
                            [
                                "class" => "btn btn-success btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                'data-request-method' => 'post',
                                'data-modal-size' => 'large',
                                'data-confirm-title' => 'Pilih Sumber Dana',
                                'data-confirm-message' =>  Html::dropDownList('source-of-fund', null, ArrayHelper::map(SourceOfFunds::find()->where(['is_active' => true])->all(), 'id', 'source_of_funds'), ['class' => 'form-control']),
                            ]
                        ) . '&nbsp;' .
                        Html::a(
                            'Pilih Jenis Biaya',
                            ["bulk-cost-type"],
                            [
                                "class" => "btn btn-warning btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                'data-request-method' => 'post',
                                'data-modal-size' => 'large',
                                'data-confirm-title' => 'Pilih Jenis Pembiayaan',
                                'data-confirm-message' =>  Html::dropDownList('cost-type', null, ArrayHelper::map(CostType::find()->where(['is_active' => true])->all(), 'id', 'cost_type'), ['class' => 'form-control']),
                            ]
                        ) . '&nbsp;' .
                        Html::a(
                            'Pilih Prioritas Pembiayaan',
                            ["bulk-cost-priority"],
                            [
                                "class" => "btn btn-info btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                'data-request-method' => 'post',
                                'data-modal-size' => 'large',
                                'data-confirm-title' => 'Pilih Prioritas Pembiayaan',
                                'data-confirm-message' =>  Html::dropDownList('cost-priority', null, ArrayHelper::map(CostPriority::find()->where(['is_active' => true])->all(), 'id', 'cost_priority'), ['class' => 'form-control']),
                            ]
                        ) . '&nbsp;' .
                        Html::a(
                            'Pilih Jenis Pembiayaan',
                            ["bulk-financing-type"],
                            [
                                "class" => "btn btn-danger btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                'data-request-method' => 'post',
                                'data-modal-size' => 'large',
                                'data-confirm-title' => 'Pilih Jenis Pembiayaan',
                                'data-confirm-message' =>  Html::dropDownList('financing-type', null, ArrayHelper::map(FinancingType::find()->where(['is_active' => true])->all(), 'id', 'financing_type'), ['class' => 'form-control']),
                            ]
                        ),
                    ]) .
                        '<div class="clearfix"></div>',
            ]
        ]) ?>
    </div>
</div>

<?= Html::beginForm(['download2'], 'post', ['id' => 'download-form']) ?>
<div></div>
<?= Html::endForm() ?>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "", // always need it for jquery plugin
    "size" => Modal::SIZE_LARGE,
]) ?>
<?php Modal::end(); ?>

<?php
    $jsReady = file_get_contents(__DIR__ . '/js/index.ready.js');
    $this->registerJs($jsReady, View::POS_READY);
?>