<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Budget */

?>
<div class="budget-create">
    <?= $this->render('_form', [
        'model' => $model,
        'kpiDList' => $kpiDList,
        'activityList' => $activityList
    ]) ?>
</div>
