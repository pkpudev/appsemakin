<?php

use app\models\Ceiling;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\{ActiveForm, Select2};
use yii\helpers\ArrayHelper;
use kartik\money\MaskMoney;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Measure;
use kartik\widgets\DepDrop;

$measuresVolume = array_merge([''=>''], ArrayHelper::map(Measure::find()->where(['used_on' => 1])->orderBy('measure')->all(), 'measure', 'measure'));
$measuresFreq = array_merge([''=>''], ArrayHelper::map(Measure::find()->where(['used_on' => 2])->orderBy('measure')->all(), 'measure', 'measure'));

$initiativesId = Yii::$app->getRequest()->getQueryParam('id');

$this->title = 'Form Anggaran';
?>

<div class="budget-create">

    <?php $form = ActiveForm::begin([
        'id' => 'budget',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error'
    ]); ?>

    <div class="box box-danger">
        <div class="box-body">
            <?php
            $errorSummary = [$model];
            foreach ($model->budgets as $budget) {
                $errorSummary[] = $budget;
            }
            ?>
            <?php echo $form->errorSummary($errorSummary); ?>
            <p>

                <?php /* $form->field($model, 'initiatives_id')
                    ->widget(Select2::classname(), [
                        'data' => $initiativeList,
                        'options' => [
                            'id' => 'proker',
                            'placeholder' => '-- Pilih Proker --',
                        ],
                        'pluginOptions' => [
                            'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                        ],
                    ]); */
                ?>
                
                <div class="row" id="proker-info-container" style="display: block;">
                    <div class="col-md-offset-2 col-md-10">
                        <table id="proker-info" class="table table-striped table-bordered detail-view">
                            <tbody>
                                <tr>
                                    <th>Info Proker yang Dipilih:</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Program Kerja</th>
                                    <td><?= $initiatives->workplan_name; ?></td>
                                </tr>
                                <tr>
                                    <th>Pelaksanaan<!--  (Bulan) --></th>
                                    <td id="proker-implementation"><?= $initiatives->implementation; ?></td>
                                </tr>
                                <!-- <tr><th>Total Anggaran Proker Dipilih</th><td id="proker-budget-detailed"></td></tr> -->
                                <tr>
                                    <th>Info Pagu Anggaran :</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Total Pagu Anggaran</th>
                                    <td id="proker-ceiling"><?= number_format(Ceiling::getPlafon($initiatives->okr->position->unit_structure_id, $initiatives->okr->position_structure_id, $initiatives->okr->year), 2, ',', '.'); ?></td>
                                </tr>
                                <tr>
                                    <th>Saldo Pagu Anggaran</th>
                                    <td id="proker-budget-unused"><?= number_format(Ceiling::getPlafonBalance($initiatives->okr->position->unit_structure_id, $initiatives->okr->position_structure_id, $initiatives->okr->year), 2, ',', '.'); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <?php /* $form->field($model, 'activity_id')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => $activityList,
                    'options' => ['id' => 'activity_id-id', 'placeholder' => '--- Pilih Kegiatan ---'],
                    'select2Options' => [
                        'pluginOptions' => [
                            'allowClear' => true,
                            'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                        ]
                    ],
                    'pluginOptions' => [
                        'depends' => ['proker'],
                        'url' => yii\helpers\Url::to(['activity-list']),
                        // 'params'=>['input-type-1', 'input-type-2']
                    ]
                ]); */ ?>

                <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper',
                    'widgetBody' => '.container-items',
                    'widgetItem' => '.source-item',
                    'limit' => 10,
                    'min' => 1,
                    'insertButton' => '.add-source',
                    'deleteButton' => '.remove-source',
                    'model' => $budgetModel,
                    'formId' => 'budget',
                    'formFields' => [
                        'initatives_id',
                        'description',
                        'account_id',
                        'volume',
                        'measure',
                        'frequency',
                        'freq_measure',
                        'unit_amount',
                        'total_amount',
                        'months',
                        'note'
                    ],

                ]); ?>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Deskripsi</th>
                            <th width="250px">Akun Pembiayaan</th>
                            <th width="80px">Volume</th>
                            <th width="80px">Satuan</th>
                            <th width="80px">Frekuensi</th>
                            <th width="80px">Satuan</th>
                            <th width="180px">Biaya</th>
                            <th width="180px">Total Anggaran</th>
                            <th width="180px">Bulan</th>
                            <th>Catatan</th>
                            <th class="text-center" style="width: 90px;">
                                <button type="button" class="add-source btn btn-success btn-xs"><span class="fa fa-plus"></span></button>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="container-items">
                        <?php $form->formConfig['showLabels'] = false; ?>
                        <?php if (empty($model->budgets)) $model->budgets = [$budgetModel]; ?>
                        <?php foreach ($model->budgets as $index => $budget) : ?>
                            <?php $budget->initiatives_id = $initiativesId; ?>
                            <tr class="source-item">
                                <td class="no">
                                        <?= ($index + 1) ?>
                                </td>
                                <td>
                                    <?= $form->field($budget, "[{$index}]description")->input('text') ?>
                                    <?= $form->field($budget, "[{$index}]initiatives_id")->hiddenInput(['value' => $initiativesId])->label(false); ?>
                                </td>
                                <td class="vcenter">
                                    <?php
                                                // necessary for update action.
                                                if (!$budget->isNewRecord) {
                                                    echo Html::activeHiddenInput($budget, "[{$index}]id");
                                                }
                                    ?>
                                    <?= $form->field($budget, "[{$index}]account_id")
                                        ->widget(Select2::classname(), [
                                            'data' => $accountList,
                                            'options' => [
                                                'placeholder' => '-- Akun --',
                                            ],
                                            // 'pluginOptions' => [
                                            //     'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                                            // ],
                                        ]);
                                    ?>
                                </td>
                                <td>
                                    <?= $form->field($budget, "[{$index}]volume")->input('number', ['min' => 1, 'placeholder' => 'masukan angka saja', 'class' => 'volume']) ?>
                                </td>
                                <td>
                                    <?= $form->field($budget, "[{$index}]measure")->dropDownList($measuresVolume, ['style' => 'width: 80px']) ?>
                                </td>
                                <td>
                                    <?= $form->field($budget, "[{$index}]frequency")->input('number', ['min' => 1, 'placeholder' => 'masukan angka saja', 'class' => 'frequency']) ?>
                                </td>
                                <td>
                                    <?= $form->field($budget, "[{$index}]freq_measure")->dropDownList($measuresFreq, ['style' => 'width: 80px']) ?>
                                </td>
                                <td>
                                    <?php  //$form->field($budget, "[{$index}]unit_amount")->widget(MaskMoney::classname(), ['options' => ['class' => 'unit-amount']]) ?>
                                    <?= $form->field($budget, "[{$index}]unit_amount")->input('number', ['min' => 1, 'class' => 'unit-amount']) ?>
                                </td>
                                <td>
                                    <?= $form->field($budget, "[{$index}]total_amount")->widget(MaskMoney::classname(), ['disabled' => true, 'options' => ['class' => 'total-amount']]) ?>
                                </td>
                                <td>
                                    <a href="#" class="all-months">all</a>
                                    <?= $form->field($budget, "[{$index}]months")
                                                    ->widget(Select2::classname(), [
                                                        'data' => $budget->getMonths(),
                                                        'options' => [
                                                            'placeholder' => '-- Pilih Bulan --',
                                                            'multiple' => true,
                                                            'class' => 'month',
                                                        ],
                                                    ])
                                    ?>
                                </td>
                                <td>
                                    <?= $form->field($budget, "[{$index}]note")->input('text') ?>
                                </td>
                                <td class="text-center vcenter" style="width: 90px;">
                                    <button type="button" class="remove-source btn btn-danger btn-xs"><span class="fa fa-minus"></span></button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right;"><?= Html::activeLabel($model, 'verify_total_amount', ['class' => 'control-label']) ?></td>
                            <td>
                                <?= $form->field($model, 'verify_total_amount')
                                                                ->label(false)
                                                                ->widget(MaskMoney::classname(), ['disabled' => true, 'options' => ['id' => 'verify-total-amount']])
                                ?>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
                <?php DynamicFormWidget::end(); ?>
            </p>
            <?php if (!Yii::$app->request->isAjax) { ?>
                <!-- <div class="form-group"> -->
                    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                <!-- </div> -->
            <?php } ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script>
    var $hasDepdrop = $(widgetOptionsRoot.widgetItem).find('[data-krajee-depdrop]');
  if ($hasDepdrop.length > 0) {
      $hasDepdrop.each(function() {
          if ($(this).data('select2') === undefined) {
              $(this).removeData().off();
              $(this).unbind();
              _restoreKrajeeDepdrop($(this));
          }
      });
  }

  // "kartik-v/yii2-widget-select2"
  var $hasSelect2 = $(widgetOptionsRoot.widgetItem).find('[data-krajee-select2]');
  if ($hasSelect2.length > 0) {
      $hasSelect2.each(function() {
          var id = $(this).attr('id');
          var configSelect2 = eval($(this).attr('data-krajee-select2'));

          if ($(this).data('select2')) {
              $(this).select2('destroy');
          }

          var configDepdrop = $(this).data('depdrop');
          if (configDepdrop) {
              configDepdrop = $.extend(true, {}, configDepdrop);
              $(this).removeData().off();
              $(this).unbind();
              _restoreKrajeeDepdrop($(this));
          }
          var s2LoadingFunc = typeof initSelect2Loading != 'undefined' ? initSelect2Loading : initS2Loading;
          var s2OpenFunc = typeof initSelect2DropStyle != 'undefined' ? initSelect2Loading : initS2Loading;
          $.when($('#' + id).select2(configSelect2)).done(s2LoadingFunc(id, '.select2-container--krajee'));

          var kvClose = 'kv_close_' + id.replace(/\-/g, '_');

          $('#' + id).on('select2:opening', function(ev) {
              s2OpenFunc(id, kvClose, ev);
          });

          $('#' + id).on('select2:unselect', function() {
              window[kvClose] = true;
          });

          if (configDepdrop) {
              var loadingText = (configDepdrop.loadingText) ? configDepdrop.loadingText : 'Loading ...';
              initDepdropS2(id, loadingText);
          }
      });
  }
</script>

<?php $this->registerJs(file_get_contents(__DIR__ . '/js/create-all.js'));
