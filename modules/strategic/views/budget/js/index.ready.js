function download() {
    var settings = jQuery('#crud-datatable').yiiGridView('data').settings;
    var data = {};
    $.each($(settings.filterSelector).serializeArray(), function () {
        if (!(this.name in data)) {
            data[this.name] = [];
        }
        data[this.name].push(this.value);
    });

    var namesInFilter = Object.keys(data);

    $.each(yii.getQueryParams(settings.filterUrl), function (name, value) {
        if (namesInFilter.indexOf(name) === -1 && namesInFilter.indexOf(name.replace(/\[\d*\]$/, '')) === -1) {
            if (!$.isArray(value)) {
                value = [value];
            }
            if (!(name in data)) {
                data[name] = value;
            } else {
                $.each(value, function (i, val) {
                    if ($.inArray(val, data[name])) {
                        data[name].push(val);
                    }
                });
            }
        }
    });

    var $form = jQuery('#download-form'),
        $formDiv = $form.find('div');

    $formDiv.empty();
    $.each(data, function (name, values) {
        $.each(values, function (index, value) {
            $formDiv.append($('<input/>').attr({type: 'hidden', name: name, value: value}));
        });
    });
    
    $form.submit();
}

function clickDownload(event) {
    event.preventDefault();

    var $year = jQuery('#year-filter');
    if ($year.length) {
        if ($year.find(':selected').val() == '') {
            alert("Filter Tahun wajib dipilih!");
            return false;
        }
    }

    var $divisi = jQuery('#divisi-filter');
    if ($divisi.length) {
        if ($divisi.find(':selected').val() == '') {
            alert("Filter Divisi wajib dipilih!");
            return false;
        }
    }
    download();
}

jQuery(document).on("click", "#download-btn", clickDownload);