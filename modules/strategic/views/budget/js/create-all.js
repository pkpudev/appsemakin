var jProkerContainer = $("#proker-info-container");
var jProker = $("#proker"); // jq PROKER dropdown field
var jImple = $("#proker-implementation"); // jq info proker IMPLEMENTASI
var jBudgetPlan = $("#proker-budget-plan"); // jq info proker TOTAL RENCANA ANGGARAN
var jBudgetDetailed = $("#proker-budget-detailed"); // jq info proker TOTAL RINCIAN ANGGARAN
var jCeiling = $("#proker-ceiling"); // jq info proker TOTAL PAGU ANGGARAN
var jBudgetUnused = $("#proker-budget-unused"); // jq info proker TOTAL SISA ANGGARAN
// var mBudget = $("#proker-budget-disp"); // jq mask money info proker TOTAL ANGGARAN
var jBudgetSource = $("#budget-source");
var jKpi = $("#proker-kpi"); // jq info proker Deskripsi IKU-IS
var jTarget = $("#proker-target"); // jq info proker TARGET
var jAddSource = $("button.add-source");

var budgetUnused = 0;
var months = [];

//-------------------------------------
// get PROKER INFO
//-------------------------------------
var prokerInfo = function() {
    var prokerId = jProker.val();
    if (prokerId != "" && prokerId != null && prokerId != 0) {
        jQuery.ajax({
            url: "/strategic/budget/proker-info",
            data: {id: prokerId, showBudgetSource: '1'},
        })
        .done(function(result){
            if ( jQuery.isEmptyObject(result) ) {
                // jProkerContainer.hide();
                // disableAllMonths();
            } else {
            	budgetUnused = result.budgetUnusedNum;

                jImple.text( result.implementation );
                jBudgetPlan.html( result.budgetPlan );
                jBudgetDetailed.html( result.budgetDetailed );
                jCeiling.html( result.ceiling );
                jBudgetUnused.html( result.budgetUnused );
                // mBudget.maskMoney( "mask", result.budget );
                jKpi.html( result.kpi );
                jTarget.text( result.target );

                jQuery.each(result.nmonths, function(key, value) {
                    if (value.show == true) {
                    	if ($("select#multibudget-0-months option[value="+key+"]").length == 0) {
                    		$("select.month").append("<option value="+key+">"+value.label+"</option>");
                    	}
                    } else {
                    	if ($("select#multibudget-0-months option[value="+key+"]").length != 0) {
                    		$("select.month option[value="+key+"]").remove();
                    	}
                    }
                });
                months = result.nmonths;

                jBudgetSource.html("");
                jQuery.each(result.budgetSources, function(key, value) {
                    jBudgetSource.append("<tr><td>"+ value.fund_source +"</td><td>"+ value.amount +"</td><td>"+ value.asnaf +"</td></tr>");
                });

                jProkerContainer.show();
            }
        });
    } else {
        // jProkerContainer.hide();
        // disableAllMonths();
    }
};

var fnVolumeEvent = function(event) {
	var no = jQuery(this).parent().parent().parent().parent().find('.no').text();

	fnSumTotalAmount(parseInt(no) - 1);
};
var fnAmountEvent = function(event) {
	var no = jQuery(this).parent().parent().parent().parent().find('.no').text();

	fnSumTotalAmount(parseInt(no) - 1);
};
var fnFrequencyEvent = function(event) {
    var no = jQuery(this).parent().parent().parent().parent().find('.no').text();

    fnSumTotalAmount(parseInt(no) - 1);
};
var fnSumTotalAmount = function(i) {
	var jVolume = jQuery('#multibudget-'+i+'-volume');
	// var jAmount = jQuery('#multibudget-'+i+'-unit_amount-disp');
	var jAmount = jQuery('#multibudget-'+i+'-unit_amount');
    var jFrequency = jQuery('#multibudget-'+i+'-frequency');

	var volume = jVolume.val();
	// var amount = jAmount.maskMoney('unmasked')[0];
	var amount = jAmount.val();
    var frequency = jFrequency.val();

	volume = jQuery.isNumeric(volume) ? parseInt(volume) : 1;
	amount = jQuery.isNumeric(amount) ? parseFloat(amount) : 0;
    frequency = jQuery.isNumeric(frequency) ? parseInt(frequency) : 1;

	jQuery('#multibudget-'+i+'-total_amount-disp').maskMoney( "mask", (volume * amount * frequency) );
	jQuery('#multibudget-'+i+'-initiatives_id').val($('#multibudget-0-initiatives_id').val());

	fnSumVerifyTotalAmount();
};
var fnSumAllTotalAmount = function() {
	jQuery('.no').each(function(){
		fnSumTotalAmount( parseInt(jQuery(this).text()) - 1 );
	});
};
var fnSumVerifyTotalAmount = function() {
	var total = 0;
	jQuery( ".total-amount" ).each(function(){
		var $f = jQuery( this );
		if ( $f.prop("type") == "text" ) {
			var amount = $f.maskMoney('unmasked')[0];
			if ( jQuery.isNumeric(amount) ) {
				total += parseFloat(amount);
			}
		}
	});
	jQuery( "#verify-total-amount-disp" ).maskMoney( "mask", total );
};
var addSource = function() {
	jQuery.each(months, function(key, value) {
        if (value.show == true) {
        	if ($("select.month option[value="+key+"]").filter(":last").length == 0) {
        		$("select.month").filter(":last").append("<option value="+key+">"+value.label+"</option>");
        	}
        } else {
        	if ($("select.month option[value="+key+"]").filter(":last").length != 0) {
        		$("select.month option[value="+key+"]").filter(":last").remove();
        	}
        }
    });
};
var fnBudgetNumberingRow = function() {
	jQuery(".source-item .no").each( function(index) {
        jQuery( this ).html( (index + 1) );
    });
};
var fnSelectAllMonthsEvent = function(event) {
    event.preventDefault();

    var $bulanSelect2 = jQuery(this).parent().find('select.month');
    var vals = [];

    jQuery.each($bulanSelect2.find('option'), function(key, value) {
        vals.push( jQuery(this).attr('value') );
    });

    $bulanSelect2.val(vals);
    $bulanSelect2.trigger('change');
};
var fnAfterInsertItemEvent = function(event) {
	fnBudgetNumberingRow();
	fnSumAllTotalAmount();
	addSource();
};
var fnBudgetBeforeDeleteEvent = function( event, item ) {
	var no = jQuery( item ).find( '.no' ).text();
    if (! confirm("Anda yakin ingin menghapus rincian anggaran no. " + no + " ?")) {
        return false;
    }
    return true;
};
var fnBudgetAfterDeleteEvent = function( event ) {
	fnBudgetNumberingRow();
	fnSumVerifyTotalAmount();
};
var fnBeforeSubmitEvent = function( event ) {
	alert("before submit");
	return false;
};
/**
 * inisiasi awal
 */
var init = function() {
    prokerInfo();
    fnSumVerifyTotalAmount();
    // jTot.val( mTot.maskMoney("unmasked")[0] ); // resolve hidden value TOTAL AMOUNT while field is hidden
    // maskSumMonthlyBudget();
};

jProker.on("change", prokerInfo);

jQuery( ".container-items" ).on( "keyup", ".volume", fnVolumeEvent ).on( "change", ".volume", fnVolumeEvent );
jQuery( ".container-items" ).on( "keyup", ".unit-amount", fnAmountEvent );
jQuery( ".container-items" ).on( "keyup", ".frequency", fnFrequencyEvent ).on( "change", ".frequency", fnFrequencyEvent );
jQuery( ".container-items" ).on( "click", ".all-months", fnSelectAllMonthsEvent );
jQuery( ".dynamicform_wrapper" ).on( "afterInsert", fnAfterInsertItemEvent );
jQuery( ".dynamicform_wrapper" ).on( "beforeDelete", fnBudgetBeforeDeleteEvent );
jQuery( ".dynamicform_wrapper" ).on( "afterDelete", fnBudgetAfterDeleteEvent );
// jQuery( document ).on("beforeSubmit", "form#budget", fnBeforeSubmitEvent);

init();