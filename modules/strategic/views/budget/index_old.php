<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\select2\Select2;
use app\models\Departments;
use app\models\CostType;
use app\models\CostPriority;
use app\models\SourceOfFunds;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BudgetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rencana Anggaran Kerja (RAK)';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="budget-index">
    <?php if ($info) : ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="fa fa-bullhorn"></i> Info!</h4>
            Total Anggaran Divisi Anda yang telah dibuat : <b>Rp. <?= $info->total_budget ? number_format($info->total_budget, 0, ',', '.') : 0 ?></b>. Plafon Anggaran Divisi Anda : <b>Rp. <?= $info->quota ? number_format($info->quota, 0, ',', '.') : 0 ?></b>
        </div>
    <?php endif; ?>
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => '.additional-filter',
            'showPageSummary' => true,
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns_old.php'),
            'toolbar' => [
                [
                    'content' => ((($info or Yii::$app->user->can('BSC Admin')) and $searchModel->lock == false) ?/*Html::a('<i class="glyphicon glyphicon-plus"></i> Tambah Anggaran', ['create'], ['role'=>'modal-remote','title'=> 'Create new Budgets','class'=>'btn btn-success']) .*/ Html::a('<i class="glyphicon glyphicon-plus"></i> Tambah Anggaran', ['create-all'], ['role' => 'modal-remote', 'title' => 'Create new Multi Budgets', 'class' => 'btn btn-success']) : '') .
                        Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']
                        ) .
                        '{toggleData}' .
                        Html::a(
                            '<i class="text-success glyphicon glyphicon-floppy-remove"></i> Excel</a>',
                            '#',
                            ['data-pjax' => 0, 'id' => 'download-btn', 'class' => 'btn btn-default', 'title' => 'Excel']
                        )
                        // '{export}'
                ],
            ],
            'striped' => true,
            'hover' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-money"></i> Rencana Kerja & Anggaran',
                'before' => (Yii::$app->user->can('BSC Admin')) ?
                    '<div class="row">' .
                        '<div class="col-md-2">' .
                            Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'year',
                                'pjaxContainerId' => 'crud-datatable-pjax',
                                'options' => [
                                    'id' => 'year-filter',
                                    'class' => 'form-control additional-filter',
                                    // 'placeholder'=>'-- Filter Tahun --',
                                ],
                                'pluginOptions' => ['allowClear' => false],
                                'data' => $yearItems,
                            ]) .
                        '</div>' .
                        '<div class="col-md-3">' .
                            Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'departments_id',
                                'pjaxContainerId' => 'crud-datatable-pjax',
                                'options' => [
                                    'id' => 'divisi-filter',
                                    'class' => 'form-control additional-filter',
                                    'placeholder' => '-- Filter Divisi --',
                                ],
                                'pluginOptions' => ['allowClear' => true],
                                'data' => ArrayHelper::map(
                                    Departments::find()
                                        ->where(['company_id' => Yii::$app->user->identity->company_id])
                                        ->andWhere(['not in', 'level', [0]])
                                        ->orderBy('deptname')
                                        ->all(),
                                    'deptid',
                                    'deptname'
                                ),
                            ]) .
                        '</div>' .
                    '</div>'
                    /*Html::a('<i class="glyphicon glyphicon-send"></i>&nbsp; Kirim', ['send'],
                    [
                        'role'=>'modal-remote',
                        'title'=> 'Kirim untuk proses Approval',
                        'class'=>'btn btn-warning',
                        'data-modal-size'=>'large',
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'<b>Anda yakin untuk mengirim RKAT?</b>',
                        'data-confirm-message'=>
                            (Yii::$app->user->can('BSC Admin'))?
                            Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:90%;']).
                            '<br><br><em>Klik <span class="label label-primary">OK</span> maka :<br>* Program Kerja dan Anggaran yang berstatus <b>DRAFT/REVISION</b> akan berubah status menjadi <b>SEND</b>.<br>* Data yang berstatus <b>SEND</b> tidak dapat Anda edit.</em>'
                            :
                            '<div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="fa fa-bullhorn"></i> Perhatian!</h4>
                                    Total Anggaran Divisi Anda yang telah dibuat : <b>Rp. '.number_format($info->total_budget,0,',','.').'</b>. Plafon Anggaran Divisi Anda : <b>Rp. '.number_format($info->quota,0,',','.').'</b>
                            </div>'.Html::textarea('confirm-comment',null,['placeholder'=>'Komentar/Pesan','rows'=>3,'style'=>'width:90%;']).
                            '<br><br><em>Klik <span class="label label-primary">OK</span> maka :<br>* Program Kerja dan Anggaran yang berstatus <b>DRAFT/REVISION</b> akan berubah status menjadi <b>SEND</b>.<br>* Data yang berstatus <b>SEND</b> tidak dapat Anda edit.</em>',
                    ]).
                    '&nbsp;<em>* Klik kirim untuk proses Approval oleh BOD.</em>'*/
                    : '<div class="col-md-2">' .
                        Select2::widget([
                            // 'value' => $currentYear,
                            'model' => $searchModel,
                            'attribute' => 'year',
                            'pjaxContainerId' => 'crud-datatable-pjax',
                            'options' => [
                                'id' => 'year-filter',
                                'class' => 'form-control additional-filter',
                                // 'placeholder'=>'-- Filter Tahun --',
                            ],
                            'pluginOptions' => ['allowClear' => false],
                            'data' => $yearItems,
                        ]) .
                    '</div>',
                    'after' => BulkButtonWidget::widget([
                        'buttons' => Html::a(
                            '<i class="glyphicon glyphicon-trash"></i>&nbsp; Pilih Sumber Dana',
                            ["bulk-source-of-found"],
                            [
                                "class" => "btn btn-success btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                'data-request-method' => 'post',
                                'data-confirm-title' => 'Pilih Sumber Dana',
                                'data-confirm-message' =>  Html::dropDownList('source-of-found', null, ArrayHelper::map(SourceOfFunds::find()->where(['is_active' => true])->all(), 'id', 'source_of_funds'), ['class' => 'form-control']),
                            ]
                        ) . '&nbsp;' . 
                        Html::a(
                            '<i class="glyphicon glyphicon-trash"></i>&nbsp; Pilih Jenis Pembiayaan',
                            ["bulk-cost-type"],
                            [
                                "class" => "btn btn-warning btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                'data-request-method' => 'post',
                                'data-confirm-title' => 'Pilih Jenis Pembiayaan',
                                'data-confirm-message' =>  Html::dropDownList('cost-type', null, ArrayHelper::map(CostType::find()->where(['is_active' => true])->all(), 'id', 'cost_type'), ['class' => 'form-control']),
                            ]
                        ) . '&nbsp;' . 
                        Html::a(
                            '<i class="glyphicon glyphicon-trash"></i>&nbsp; Pilih Prioritas Pembiayaan',
                            ["bulk-cost-priority"],
                            [
                                "class" => "btn btn-info btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false, // for overide yii data api
                                'data-request-method' => 'post',
                                'data-confirm-title' => 'Pilih Prioritas Pembiayaan',
                                'data-confirm-message' =>  Html::dropDownList('cost-priority', null, ArrayHelper::map(CostPriority::find()->where(['is_active' => true])->all(), 'id', 'cost_priority'), ['class' => 'form-control']),
                            ]
                        ),
                    ]) .
                        '<div class="clearfix"></div>',
            ]
        ]) ?>
    </div>
</div>

<?= Html::beginForm(['download'], 'post', ['id' => 'download-form']) ?>
<div></div>
<?= Html::endForm() ?>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "", // always need it for jquery plugin
    "size" => Modal::SIZE_LARGE,
]) ?>
<?php Modal::end(); ?>

<?php
    $jsReady = file_get_contents(__DIR__ . '/js/index.ready.js');
    $this->registerJs($jsReady, View::POS_READY);
?>