<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use \yii\helpers\ArrayHelper;

$listFinancingType = ArrayHelper::map(app\models\FinancingType::find()->orderBy('id')->asArray()->all(), 'id', 'financing_type');
$listFinancingType = ArrayHelper::merge([999999 => '-- Belum Diisi --'], $listFinancingType);



return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'filter' => false,
        'attribute' => 'unit_name',
        'format' => 'html',
        'value' => function ($model, $key, $index, $widget) {
            return '<b>' . $model->unit_code . ' ' . $model->unit_name . '</b>';
        },
        'group' => true,
        'groupedRow' => true,                    // move grouped column to a single grouped row
        'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
        'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
        'groupFooter' => function ($model) { // Closure method
            return [
                'mergeColumns' => [[0, 9]], // columns to merge in summary
                'content' => [             // content to show in each summary cell
                    0 => 'TOTAL ANGGARAN ' . strtoupper($model->unit_name),
                    10 => GridView::F_SUM,
                    // 12 => GridView::F_SUM,
                    // 13 => GridView::F_SUM,
                    // 14 => GridView::F_SUM,
                    15 => GridView::F_SUM,
                    16 => GridView::F_SUM,
                    17 => GridView::F_SUM,
                    18 => GridView::F_SUM,
                    19 => GridView::F_SUM,
                    20 => GridView::F_SUM,
                    21 => GridView::F_SUM,
                    22 => GridView::F_SUM,
                    23 => GridView::F_SUM,
                    24 => GridView::F_SUM,
                    25 => GridView::F_SUM,
                    26 => GridView::F_SUM,
                ],
                'contentFormats' => [      // content reformatting for each summary cell
                    10 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    // 12 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    // 13 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    // 14 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    15 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    16 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    17 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    18 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    19 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    20 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    21 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    22 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    23 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    24 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    25 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    26 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                ],
                'contentOptions' => [      // content html attributes for each summary cell
                    0 => ['style' => 'font-variant:small-caps;text-align:right'],
                    10 => ['style' => 'text-align:right'],
                    // 12 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    // 13 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    // 14 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    15 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    16 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    17 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    18 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    19 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    20 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    21 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    22 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    23 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    24 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    25 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    26 => ['class' => 'vertical', 'style' => 'text-align:right'],
                ],
                // html attributes for group summary row
                'options' => ['class' => 'success', 'style' => 'font-weight:bold;']
            ];
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Program Kerja',
        'attribute' => 'workplan_name',
        'group' => true,  // enable grouping
        'subGroupOf' => 1,
        /*'groupFooter' => function ($model) { // Closure method
            return [
                'mergeColumns' => [[0, 9]], // columns to merge in summary
                'content' => [             // content to show in each summary cell
                    0 => 'Jumlah (' . $model->workplan_name . ')',
                    10 => GridView::F_SUM,
                    // 12 => GridView::F_SUM,
                    // 13 => GridView::F_SUM,
                    // 14 => GridView::F_SUM,
                    15 => GridView::F_SUM,
                    16 => GridView::F_SUM,
                    17 => GridView::F_SUM,
                    18 => GridView::F_SUM,
                    19 => GridView::F_SUM,
                    20 => GridView::F_SUM,
                    21 => GridView::F_SUM,
                    22 => GridView::F_SUM,
                    23 => GridView::F_SUM,
                    24 => GridView::F_SUM,
                    25 => GridView::F_SUM,
                    26 => GridView::F_SUM,
                ],
                'contentFormats' => [      // content reformatting for each summary cell
                    10 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    // 12 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    // 13 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    // 14 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    15 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    16 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    17 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    18 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    19 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    20 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    21 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    22 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    23 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    24 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    25 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                    26 => ['format' => 'number', 'decimals' => 0, 'decPoint' => ',', 'thousandSep' => '.'],
                ],
                'contentOptions' => [      // content html attributes for each summary cell
                    0 => ['style' => 'font-variant:small-caps;text-align:right'],
                    10 => ['style' => 'text-align:right'],
                    // 12 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    // 13 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    // 14 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    15 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    16 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    17 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    18 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    19 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    20 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    21 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    22 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    23 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    24 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    25 => ['class' => 'vertical', 'style' => 'text-align:right'],
                    26 => ['class' => 'vertical', 'style' => 'text-align:right'],
                ],
                // html attributes for group summary row
                'options' => ['class' => 'info', 'style' => 'font-weight:bold;']
            ];
        }*/
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Kegiatan Kerja',
        'attribute' => 'activity_name',
        'group' => true,  // enable grouping
        'subGroupOf' => 2,
    ],
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => ($model->budget_id) ?: $key]);
        },
        'buttons' => [
            'view' => function ($url, $model) {
                if($model->budget_id){
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        $url, 
                        [
                            'role'=>'modal-remote','title' => Yii::t('app', 'View'),'data-toggle'=>'tooltip',
                        ]
                    );
                }else
                return false;
            },
            'update' => function ($url, $model) {
                if ($model->i_status_id == app\models\Status::DRAFT or $model->i_status_id == app\models\Status::REVISION) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-pencil"></span>',
                        $url,
                        [
                            'role' => 'modal-remote', 'title' => Yii::t('app', 'Update'), 'data-toggle' => 'tooltip', 'style' => 'color: var(--warning) !important;'
                        ]
                    );
                } else
                    return false;
            },
            'delete' => function ($url, $model) {
                if ($model->i_status_id == app\models\Status::DRAFT and $model->i_status_id == app\models\Status::REVISION) {
                    return false;
                } else {
                    return Html::a(
                        '<span class="glyphicon glyphicon-trash"></span>',
                        $url,
                        [
                            'title' => Yii::t('app', 'Delete'),
                            'data-confirm' => '' . 'Are you sure to delete this item?' . '',
                            'data-method' => 'post',
                            'style' => 'color: var(--danger) !important;'
                        ]
                    );
                }
            },
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Akun Biaya',
        'attribute' => 'account_id',
        'value' => function ($model) {
            return $model->getAccount($model->account_id) ?: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\Account::find()->orderBy('id')->asArray()->all(), 'id', 'account_name'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Volume',
        'attribute' => 'volume',
        'hAlign' => 'right',
        'width' => '50px',
        'value' => function($model){
            return $model->volume . ' ' . $model->budget_measure;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'frequency',
        'label' => 'Frekuensi',
        'hAlign' => 'right',
        'width' => '50px',
        'value' => function($model){
            return $model->frequency . ' ' . $model->freq_measure;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Biaya',
        'attribute' => 'unit_amount',
        'hAlign' => 'right',
        'format' => ['decimal', 0],
        'pageSummary' => 'TOTAL',
        'pageSummaryOptions' => ['class' => 'text-right text-warning'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Total Anggaran',
        'attribute' => 'total_amount',
        'hAlign' => 'right',
        'format' => ['decimal', 0/*, [NumberFormatter::DECIMAL_ALWAYS_SHOWN=>false,NumberFormatter::GROUPING_USED=>true]*/],
        'value' => function ($model) {
            return $model->total_amount ?: 0;
        },
        'pageSummary' => true
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Sumber Dana',
        'attribute' => 'source_of_funds_id',
        'value' => function ($model) {
            return $model->getSourceOfFunds($model->source_of_funds_id) ?: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\SourceOfFunds::find()->orderBy('id')->asArray()->all(), 'id', 'source_of_funds'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Jenis Biaya',
        'attribute' => 'cost_type_id',
        'value' => function ($model) {
            return $model->getCostType($model->cost_type_id) ?: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\CostType::find()->orderBy('id')->asArray()->all(), 'id', 'cost_type'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Prioritas Pembiayaan',
        'attribute' => 'cost_priority_id',
        'value' => function ($model) {
            return $model->getCostPriority($model->cost_priority_id) ?: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(app\models\CostPriority::find()->orderBy('id')->asArray()->all(), 'id', 'cost_priority'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Jenis Pembiayaan',
        'attribute' => 'financing_type_id',
        'value' => function ($model) {
            return $model->getFinancingType($model->financing_type_id) ?: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>$listFinancingType, 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Kode RKAT',
        'attribute' => 'budget_code',
        // 'value' => function ($model) {
            // var_dump($model->id);
            // var_dump($model->budgets);
        //     return implode('<br/>', \yii\helpers\ArrayHelper::map($model->budgets, 'id', function ($model) {
        //         return $model->budget_code;
        //     }));
        // },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Jan',
        'attribute' => 'month_01',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_01 ?: 0;
        },
        // 'rowOptions'=>['class'=>'vertical'],
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Feb',
        'attribute' => 'month_02',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_02 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Mar',
        'attribute' => 'month_03',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_03 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Apr',
        'attribute' => 'month_04',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_04 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Mei',
        'attribute' => 'month_05',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_05 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Jun',
        'attribute' => 'month_06',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_06 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Jul',
        'attribute' => 'month_07',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_07 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Agu',
        'attribute' => 'month_08',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_08 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Sep',
        'attribute' => 'month_09',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_09 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Okt',
        'attribute' => 'month_10',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_10 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Nov',
        'attribute' => 'month_11',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_11 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Des',
        'attribute' => 'month_12',
        'hAlign' => 'right',
        'vAlign' => 'top',
        'width' => '30px',
        'contentOptions' => ['class' => 'vertical'],
        'format' => ['decimal', 0],
        'value' => function ($model) {
            return $model->month_12 ?: 0;
        },
        'pageSummary' => true,
        'pageSummaryOptions' => ['class' => 'text-right text-warning vertical'],
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'label'=>'Status',
    //     'attribute'=>'budget_status',
    //     'width'=>'60px',
    //     'format'=>'raw',
    //     'value'=>function($model){
    //     	return ($model->budgetStatus)?$model->budgetStatus->getStatusname($model->budget_status):'-';
    //     },
    //     'filterType'=>GridView::FILTER_SELECT2,
    //     'filter'=>ArrayHelper::map(app\models\Status::find()->orderBy('id')->asArray()->all(), 'id', 'status'), 
    //     'filterWidgetOptions'=>[
    //         'pluginOptions'=>['allowClear'=>true],
    //     ],
    //     'filterInputOptions'=>['placeholder'=>''],
    // ],

];
