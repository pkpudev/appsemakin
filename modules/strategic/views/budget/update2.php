<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Budget */
?>
<div class="budget-update">

    <?= $this->render('_form2', [
        'model' => $model,
        'kpiDList' => $kpiDList,
        'activityList' => $activityList,
        'accountList' => $accountList
    ]) ?>

</div>
