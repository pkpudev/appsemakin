<?php

use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Budget */
?>
<div class="budget-view">
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
        <div class="box-body">
            <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'label'=>'Nama Program Kerja',
                        'value'=>$model->initiatives->workplan_name,
                    ],
                    /* [
                        'attribute'=>'activity_id',
                        'value'=>$model->activity->activity_name,
                    ], */
                    'note:ntext',
                    [
                        'attribute'=>'volume',
                        'value'=>$model->volume.' '.$model->measure,
                    ],
                    [
                        'attribute'=>'frequency',
                        'value'=>$model->frequency.' '.$model->freq_measure,
                    ],
                    [
                        'attribute'=>'unit_amount',
                        'format'=>'html',
                        'value'=>'Rp. '.number_format($model->unit_amount,0,',','.'),
                    ],
                    [
                        'attribute'=>'total_amount',
                        'format'=>'html',
                        'value'=>'<b>Rp. '.number_format($model->total_amount,0,',','.').'</b>',
                    ],
                ],
            ]) ?>
            </div>
            <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute'=>'budget_code',
                        'format'=>'html',
                        'value'=>'<b>'.$model->budget_code.'</b>',
                    ],
                    [
                        'attribute' => 'account_id',
                        'value' => function ($model) {
                            return $model->account_id ? $model->account->daf_account_no . ' ' . $model->account->account_name : '-';
                        },
                    ],
                    [
                        'label' => 'Sumber Dana',
                        'attribute' => 'source_of_funds_id',
                        'value' => function ($model) {
                            return $model->source_of_funds_id ? $model->sourceOfFunds->source_of_funds: '-';
                        },
                    ],
                    [
                        'label' => 'Jenis Biaya',
                        'attribute' => 'cost_type_id',
                        'value' => function ($model) {
                            return $model->cost_type_id ? $model->costType->cost_type : '-';
                        },
                    ],
                    [
                        'label' => 'Prioritas Pembiayaan',
                        'attribute' => 'cost_priority_id',
                        'value' => function ($model) {
                            return $model->cost_priority_id ? $model->costPriority->cost_priority: '-';
                        },
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'label' => 'Jenis Pembiayaan',
                        'attribute' => 'financing_type_id',
                        'value' => function ($model) {
                            return $model->financing_type_id ? $model->financingType->financing_type: '-';
                        },
                    ],
                    [
                        'attribute'=>'status_id',
                        'format'=>'raw',
                        'value'=>$model->status->getStatusname($model->status_id),
                    ],
                ],
            ]) ?>
            </div>
        </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid box-primary">
            <div class="box-header with-border">
                <h4 class="box-title">Alokasi Anggaran /Bulan</h4>
            </div>
            <div class="box-body">
                <div class="col-md-4">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute'=>'month_01',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_01,0,',','.'),
                        ],
                        [
                            'attribute'=>'month_02',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_02,0,',','.'),
                        ],
                        [
                            'attribute'=>'month_03',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_03,0,',','.'),
                        ],
                        [
                            'attribute'=>'month_04',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_04,0,',','.'),
                        ],
                    ],
                ]) ?>
                </div>
                <div class="col-md-4">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute'=>'month_05',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_05,0,',','.'),
                        ],
                        [
                            'attribute'=>'month_06',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_06,0,',','.'),
                        ],
                        [
                            'attribute'=>'month_07',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_07,0,',','.'),
                        ],
                        [
                            'attribute'=>'month_08',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_08,0,',','.'),
                        ],
                    ],
                ]) ?>
                </div>
                <div class="col-md-4">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute'=>'month_09',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_09,0,',','.'),
                        ],
                        [
                            'attribute'=>'month_10',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_10,0,',','.'),
                        ],
                        [
                            'attribute'=>'month_11',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_11,0,',','.'),
                        ],
                        [
                            'attribute'=>'month_12',
                            'format'=>'html',
                            'value'=>'Rp. '.number_format($model->month_12,0,',','.'),
                        ],
                    ],
                ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-warning">
        <div class="box-header with-border">
            <h4 class="box-title">History</h4>
        </div>
        <div class="box-body">
            <div class="table-responsive"> 
                <?= GridView::widget([
                    'layout' => '{pager}{items}{pager}',
                    'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getBudgetHistories()->orderBy('stamp'), 'pagination' => ['pageSize' => 5, 'pageParam'=>'page-budgethistories']]),
                    'pager' => [
                        'class' => yii\widgets\LinkPager::className(),
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last'        ],
                    'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                    'headerRowOptions' => ['class'=>'x'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'width' => '175px',
                            'attribute' => 'stamp',
                            'value' => function($model){
                                return ($model->stamp) ? date('d M Y H:i:s',strtotime($model->stamp)):'-';
                            },
                            // 'format' => ['date', 'dd MM yyyy HH:mm:ss'],
                        ],
                        'action',
                        [
                            'attribute' => 'by',
                            'value' => 'by0.full_name'
                        ],
                        'comment:ntext',
                    ],
                ]); ?>
            </div>
        </div>
        </div>
    </div>
</div>
</div>
