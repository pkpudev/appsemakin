<?php
use yii\helpers\Html;
use yii\helpers\Url;
use \kartik\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\money\MaskMoney;
use app\models\Measure;
use app\models\SourceOfFunds;
use app\models\CostType;
use app\models\CostPriority;
use app\models\FinancingType;
use app\models\Initiatives;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

$measuresVolume = array_merge([''=>''], ArrayHelper::map(Measure::find()->where(['used_on' => 1])->orderBy('measure')->all(), 'measure', 'measure'));
$measuresFreq = array_merge([''=>''], ArrayHelper::map(Measure::find()->where(['used_on' => 2])->orderBy('measure')->all(), 'measure', 'measure'));
$sourceOfFunds = ArrayHelper::map(SourceOfFunds::find()->where(['is_active'=>true])->all(), 'id', 'source_of_funds');
$costType = ArrayHelper::map(CostType::find()->where(['is_active'=>true])->all(), 'id', 'cost_type');
$costPriority = ArrayHelper::map(CostPriority::find()->where(['is_active'=>true])->all(), 'id', 'cost_priority');
$financingType = ArrayHelper::map(FinancingType::find()->where(['is_active'=>true])->all(), 'id', 'financing_type');
?>

<div class="budget-form">

    <?php $form = ActiveForm::begin([
        'id' => 'budget',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error'
    ]);?>

    <div class="box box-danger">
        <?php echo $form->errorSummary($model); ?>
        <p>

            <?= $form->field($model, 'initiatives_id')
                ->widget(Select2::classname(), [
                    'data' => $kpiDList,
                    'initValueText' => $model->activity_id ? $model->activity->initiative->workplan_name : '',
                    'options' => [
                        'id' => 'proker',
                        'placeholder' => '-- Pilih Proker --',
                        'value' => $model->activity_id ? $model->activity->initiatives_id : '',
                    ],
                    'pluginOptions' => [
                        'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                    ],
                ])->label('Proker');
            ?>

            <div class="row" id="proker-info-container" style="display: none;">
                <div class="col-md-offset-2 col-md-10">
                    <table id="proker-info" class="table table-striped table-bordered detail-view">
                        <tbody>
                            <tr><th>Pelaksanaan (Bulan)</th><td id="proker-implementation"></td></tr>
                            <!-- <tr><th>Total Rencana Anggaran</th><td id="proker-budget-plan"></td></tr> -->
                            <tr><th>Total Rincian Anggaran</th><td id="proker-budget-detailed"></td></tr>
                            <!-- <tr><th>Saldo Sisa Anggaran</th><td id="proker-budget-unused"></td></tr> -->
                            <tr><th>Deskripsi IKU-IS</th><td id="proker-kpi"></td></tr>
                            <tr><th>Target</th><td id="proker-target"></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <?= $form->field($model, 'activity_id')->widget(DepDrop::classname(), [
                'type'=>DepDrop::TYPE_SELECT2,
                'data'=>$activityList,
                'options'=>['id'=>'activity_id-id', 'placeholder'=>'--- Pilih Kegiatan ---'],
                'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                'pluginOptions'=>[
                    'depends'=>['proker'],
                    'url'=>yii\helpers\Url::to(['activity-list']),
                    // 'params'=>['input-type-1', 'input-type-2']
                ]
            ])->label('Kegiatan'); ?>

            <!-- <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?> -->
            <?= $form->field($model, 'account_id')
                ->widget(Select2::classname(), [
                    'data' => $accountList,
                    'options' => [
                        'placeholder' => '-- Akun --',
                    ],
                    'pluginOptions' => [
                        'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
                    ],
                ]);
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <?= $form->field($model, 'volume')->input('number', ['min'=>1, 'placeholder' => 'masukan angka saja']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'measure')->dropDownList($measuresVolume) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <?= $form->field($model, 'frequency')->input('number', ['min'=>1, 'placeholder' => 'masukan angka saja']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'freq_measure')->dropDownList($measuresFreq) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'unit_amount')->input('number', ['class'=>'unit-amount', 'min'=>1, 'placeholder' => 'masukan angka saja']) ?>
                </div>
                <div class="col-md-6">
                    <?=  $form->field($model, 'total_amount')->widget(MaskMoney::classname(), ['disabled'=>true]) ?>
                </div>
            </div>

            <div class="row">
	        <div class="col-md-12">
	        	<div class="box box-solid box-primary">
			    <div class="box-header with-border">
			        <h4 class="box-title">Alokasi Anggaran /Bulan</h4>
			    </div>
			    <div class="box-body" id="budgets">
			        <div class="col-md-6">
			            <?php // $form->field($model, 'month_01')->widget(MaskMoney::classname(), ['options'=>['id'=>'jan']/*, 'disabled'=>true*/]) ?>
			            <?php // $form->field($model, 'month_02')->widget(MaskMoney::classname(), ['options'=>['id'=>'feb']/*, 'disabled'=>true*/]) ?>
			            <?php // $form->field($model, 'month_03')->widget(MaskMoney::classname(), ['options'=>['id'=>'mar']/*, 'disabled'=>true*/]) ?>
			            <?php // $form->field($model, 'month_04')->widget(MaskMoney::classname(), ['options'=>['id'=>'apr']/*, 'disabled'=>true*/]) ?>
			            <?php // $form->field($model, 'month_05')->widget(MaskMoney::classname(), ['options'=>['id'=>'may']/*, 'disabled'=>true*/]) ?>
			            <?php // $form->field($model, 'month_06')->widget(MaskMoney::classname(), ['options'=>['id'=>'jun']/*, 'disabled'=>true*/]) ?>

                        <?= $form->field($model, 'month_01')->input('number', ['id'=>'jan']) ?>
                        <?= $form->field($model, 'month_02')->input('number', ['id'=>'feb']) ?>
                        <?= $form->field($model, 'month_03')->input('number', ['id'=>'mar']) ?>
                        <?= $form->field($model, 'month_04')->input('number', ['id'=>'apr']) ?>
                        <?= $form->field($model, 'month_05')->input('number', ['id'=>'may']) ?>
                        <?= $form->field($model, 'month_06')->input('number', ['id'=>'jun']) ?>
			        </div>
			        <div class="col-md-6">
			            <?php // $form->field($model, 'month_07')->widget(MaskMoney::classname(), ['options'=>['id'=>'jul']/*, 'disabled'=>true*/]) ?>
			            <?php // $form->field($model, 'month_08')->widget(MaskMoney::classname(), ['options'=>['id'=>'aug']/*, 'disabled'=>true*/]) ?>
			            <?php // $form->field($model, 'month_09')->widget(MaskMoney::classname(), ['options'=>['id'=>'sep']/*, 'disabled'=>true*/]) ?>
			            <?php // $form->field($model, 'month_10')->widget(MaskMoney::classname(), ['options'=>['id'=>'oct']/*, 'disabled'=>true*/]) ?>
			            <?php // $form->field($model, 'month_11')->widget(MaskMoney::classname(), ['options'=>['id'=>'nov']/*, 'disabled'=>true*/]) ?>
			            <?php // $form->field($model, 'month_12')->widget(MaskMoney::classname(), ['options'=>['id'=>'dec']/*, 'disabled'=>true*/]) ?>

                        <?= $form->field($model, 'month_07')->input('number', ['id'=>'jul']) ?>
                        <?= $form->field($model, 'month_08')->input('number', ['id'=>'aug']) ?>
                        <?= $form->field($model, 'month_09')->input('number', ['id'=>'sep']) ?>
                        <?= $form->field($model, 'month_10')->input('number', ['id'=>'oct']) ?>
                        <?= $form->field($model, 'month_11')->input('number', ['id'=>'nov']) ?>
                        <?= $form->field($model, 'month_12')->input('number', ['id'=>'dec']) ?>
		            </div>
	            </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">Total Alokasi Anggaran (Setahun)</label>
                            <div class="col-md-10">
                                <?= MaskMoney::widget(['name'=>'sum-budget', 'options'=>['id'=>'sum-monthly-budget'], 'disabled'=>true])?>
                            </div>
                        </div>
                    </div>
	            </div>
                </div>
            </div>
            </div>

            <?= $form->field($model, 'source_of_funds_id')->dropDownList($sourceOfFunds,['prompt'=>'Select...']) ?>
            <?= $form->field($model, 'cost_type_id')->dropDownList($costType,['prompt'=>'Select...']) ?>
            <?= $form->field($model, 'cost_priority_id')->dropDownList($costPriority,['prompt'=>'Select...']) ?>
            <?= $form->field($model, 'financing_type_id')->dropDownList($financingType,['prompt'=>'Select...']) ?>
            <?= $form->field($model, 'note')->textarea(['rows' => 2]) ?>

        </p>
    	<?php if (!Yii::$app->request->isAjax){ ?>
    	  	<div class="form-group">
    	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    	    </div>
    	<?php } ?>
    </div>
    <?php ActiveForm::end(); ?>
    
</div>

<?php
$prokerInfoUrl = Url::to(['/strategic/budget/proker-info']);
// selector id for js
$selIdVolume = Html::getInputId($model, 'volume');
$selIdUnitMount = Html::getInputId($model, 'unit_amount');
$selIdFrequency = Html::getInputId($model, 'frequency');
$selIdTotalMount = Html::getInputId($model, 'total_amount');
$jsScript = <<<JS

var jProkerContainer = $("#proker-info-container");
var jProker = $("#proker"); // jq PROKER dropdown field
var jImple = $("#proker-implementation"); // jq info proker IMPLEMENTASI
var jBudgetPlan = $("#proker-budget-plan"); // jq info proker TOTAL RENCANA ANGGARAN
var jBudgetDetailed = $("#proker-budget-detailed"); // jq info proker TOTAL RINCIAN ANGGARAN
var jBudgetUnused = $("#proker-budget-unused"); // jq info proker TOTAL SISA ANGGARAN
// var mBudget = $("#proker-budget-disp"); // jq mask money info proker TOTAL ANGGARAN
var jKpi = $("#proker-kpi"); // jq info proker Deskripsi IKU-IS
var jTarget = $("#proker-target"); // jq info proker TARGET
var jVol = $("#$selIdVolume"); // jq VOLUME
var jUm = $("#$selIdUnitMount"); // jq UNIT AMOUNT
var mUm = $("#$selIdUnitMount"); // jq mask money UNIT AMOUNT
// var mUm = $("#$selIdUnitMount-disp"); // jq mask money UNIT AMOUNT
var jFreq = $("#$selIdFrequency"); // jq FREQ
var jTot = $("#$selIdTotalMount"); // jq TOTAL AMOUNT
var mTot = $("#$selIdTotalMount-disp"); // jq mask money TOTAL AMOUNT
var mMons = $("#budgets").find("input[type='number']"); // jq array mask money ALL MONTHLY BUDGET
var mSum = $("#sum-monthly-budget-disp"); //jq mask money SUM MONTHLY BUDGET

//-------------------------------------
// get PROKER INFO
//-------------------------------------
var prokerInfo = function() {
    var prokerId = jProker.val();
    if (prokerId != "" && prokerId != null && prokerId != 0) {
        jQuery.ajax({
            url: "$prokerInfoUrl",
            data: {id: prokerId},
        })
        .done(function(result){
            if ( jQuery.isEmptyObject(result) ) {
                jProkerContainer.hide();
                disableAllMonths();
            } else {
                jImple.text( result.implementation );
                jBudgetPlan.html( result.budgetPlan );
                jBudgetDetailed.html( result.budgetDetailed );
                jBudgetUnused.html( result.budgetUnused );
                // mBudget.maskMoney( "mask", result.budget );
                jKpi.html( result.kpi );
                jTarget.text( result.target );

                if (result.months !== undefined && result.months !== null) {
                    jQuery.each(result.months, function(key, value) {
                        jQuery("#" + key).prop("disabled", !value);
                        // jQuery("#" + key + "-disp").prop("disabled", !value);
                    });
                } else {
                    disableAllMonths();
                }

                jProkerContainer.show();
            }
        });
    } else {
        jProkerContainer.hide();
        disableAllMonths();
    }
};

var disableAllMonths = function() {
    mMons.each(function(){
        jQuery(this).prop('disabled', true);
    });
};

/**
 * hitung dan update TOTAL AMOUNT
 */
var updateTotalAmount = function() {
    
    var vol = jVol.val(),
        // um = mUm.maskMoney('unmasked')[0],
        um = mUm.val(),
        freq = jFreq.val(),
        tot;

    vol = $.isNumeric(vol) ? parseFloat(vol) : 0;
    um = $.isNumeric(um) ? parseFloat(um) : 0;
    freq = $.isNumeric(freq) ? parseFloat(freq) : 0;
    tot = vol * um * freq;

    jTot.val(tot);
    mTot.maskMoney("mask", tot);
};

var sumMonthlyBudget = function() {
    var tot = 0;
    var jtot = jTot.val();

    mMons.each(function() {
        // var val = $(this).maskMoney('unmasked')[0];
        var val = $(this).val();
        tot += $.isNumeric(val) ? parseFloat(val) : 0;
    });

    // alert(Math.trunc(tot) + ' ' + Math.trunc(jtot))
    if(Math.trunc(tot) != Math.trunc(jtot)){
        $('#submit-btn').prop('disabled', true);
    } else {
        $('#submit-btn').prop('disabled', false);
    }

    return tot;
};

var maskSumMonthlyBudget = function() {
    mSum.maskMoney( "mask", sumMonthlyBudget() );
};

/**
 * inisiasi awal
 */
var init = function() {
    prokerInfo();
    jTot.val( mTot.maskMoney("unmasked")[0] ); // resolve hidden value TOTAL AMOUNT while field is hidden
    maskSumMonthlyBudget();
};

jProker.on("change", prokerInfo);

/**
 * jika VOLUME berubah
 */
jVol.on("keyup", updateTotalAmount);
jVol.on("change", updateTotalAmount);

/**
 * jika UNIT AMOUNT berubah
 */
mUm.on("keyup", updateTotalAmount);
mUm.on("change", updateTotalAmount);

/**
 * jika FREQUENCY berubah
 */
jFreq.on("keyup", updateTotalAmount);
jFreq.on("change", updateTotalAmount);

/**
 * jika BUDGET BULANAN berubah
 */
mMons.on("keyup", maskSumMonthlyBudget);
mMons.on("change", maskSumMonthlyBudget);

/**
 * jalankan inisiasi awal on-ready
 */
init();

JS;

$this->registerJs($jsScript, \yii\web\View::POS_READY, 'rkat-budget-form-js');
?>