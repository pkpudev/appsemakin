<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinancingTypeCeiling */
?>
<div class="financing-type-ceiling-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
