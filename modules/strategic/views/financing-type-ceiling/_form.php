<?php

use app\models\FinancingType;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinancingTypeCeiling */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="financing-type-ceiling-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'year')->widget(DatePicker::classname(), [
                'options' => ['id', 'placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ])->label('Tahun') ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'unit_structure_id')->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => [
                                'placeholder' => 'Pilih Unit Struktur'
                            ],
                'select2Options' => [
                    'options' => [
                                    'value' => $model->unit_structure_id
                                ],
                    'pluginOptions' => [
                                            'allowClear' => true,
                                            'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                        ]
                ],
                'pluginOptions' => [
                    'depends' => ['financingtypeceiling-year'],
                    'url' => Url::to(['/leadership/structure/get-unit']),
                ]
            ])->label('Unit Struktur'); ?>
        </div>
    </div>

    <?= $form->field($model, 'financing_type_id')
        ->widget(Select2::classname(), [
            'data' => ArrayHelper::map(FinancingType::find()->where(['is_active' => true])->asArray()->all(), 'id', 'financing_type'),
            'options' => [
                'placeholder' => '-- Pilih Jenis Pembiayaan --',
            ],
            'pluginOptions' => [
                'dropdownParent' => (Yii::$app->request->isAjax ? new \yii\web\JsExpression("$('#ajaxCrudModal')") : false),
            ],
        ])->label('Jenis Pembiayaan'); ?>

    <?= $form->field($model, 'ceiling_amount')->textInput() ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
