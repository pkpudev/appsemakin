<?php

use app\models\FinancingType;
use app\models\UnitStructure;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_structure_id',
        'value' => function($model){
            return $model->unit_structure_id ? $model->unit->unit_code . ' - ' . $model->unit->unit_name : '-';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(UnitStructure::find()->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
            return $model->unit_code . ' - ' . $model->unit_name;
        }),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'financing_type_id',
        'value' => function($model){
            return $model->financing_type_id ? $model->financingType->financing_type : '-';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(FinancingType::find()->andWhere(['is_active' => true])->orderBy('id')->asArray()->all(), 'id', 'financing_type'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ceiling_amount',
        'format' => 'Currency',
        'hAlign' => 'right'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_by',
        'filter' => false
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'],
    ],

];
