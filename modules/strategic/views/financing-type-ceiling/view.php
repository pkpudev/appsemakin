<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinancingTypeCeiling */
?>
<div class="financing-type-ceiling-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'unit_structure_id',
            'financing_type_id',
            'ceiling_amount',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
        ],
    ]) ?>

</div>
