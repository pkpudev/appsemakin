<?php

namespace app\modules\strategic\controllers\initiatives;

use Yii;
use app\models\Creator;
use app\models\search\OkrSearch;
use app\models\UnitStructure;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class DownloadAction extends \yii\base\Action
{
	public function run()
	{
		$searchModel = new OkrSearch();
		$dataProvider = $searchModel->searchForInitiatives(Yii::$app->request->post());
		$query = $dataProvider->query;
		if (!$searchModel->status_id) {
			// $query->andFilterWhere(['i.status_id' => 4]); // Approved
		}
		$models = $query->all();
		
		$year = $searchModel->year;
		$formatter = Yii::$app->formatter;
		$logo = 'pkpu.png';
		$lembaga = 'Human Initiative';

		$unit = UnitStructure::findOne($searchModel->unit_id);
		$directorate = $unit->unit_name ?: '';
		$level = $unit->level ?: '';
		$dir = strtoupper($directorate);
		
		if($level == 1){
			$dir = "DIREKTORAT {$dir}";
		} else if($level == 2){
			$dir = "DEPARTEMEN {$dir}";
		} else if($level == 3){
			$dir = "DIVISI {$dir}";
		} else if($level == 4){
			$dir = "BIDANG {$dir}";
		} else if($level == 5){
			$dir = "SQUAD {$dir}";
		}

		$deptName = @$unit->unit_name;
		$deptCode = @$unit->unit_code;
		$deptnameUppercase = strtoupper($deptName);

		// Load excel from template
		$spreadsheet = IOFactory::load(Yii::getAlias("@app/templates/excel/initiatives.xlsx"));
		$spreadsheet->setActiveSheetIndexByName('PROKER');

		// logo cell
		$drawing = new Drawing();
		$drawing->setName('Logo');
		$drawing->setDescription('Logo');
		$drawing->setPath(Yii::getAlias("images/{$logo}"));
		$drawing->setCoordinates('B2');
		$drawing->setHeight(100);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// instansi cell
		$spreadsheet->getActiveSheet()->setCellValue('instansi', strtoupper($lembaga));

		// divisi cell
		// $spreadsheet->getActiveSheet()->setCellValue('divisi', $dir);
		$spreadsheet->getActiveSheet()->setCellValue('divisi', $deptCode . ' ' . $deptnameUppercase);

		// tahun cell
		$spreadsheet->getActiveSheet()->setCellValue('tahun', 'PERIODE ' . $year);

		// label_total_divisi cell
		// $spreadsheet->getActiveSheet()->setCellValue('label_total_divisi', "TOTAL ({$deptnameUppercase})");
		// $spreadsheet->getActiveSheet()->setCellValue('label_total_divisi', "{$deptnameUppercase}");


		$startRow = 11;
		$currentRow = null;
		$totalBudgetRow = 12;
		$totalBudgetValue = 0;
		$totalBobot = 0;
		$oldOkrCode = null;
		$oldOkrCodeValue = null;
		$oldOkrObjective = null;
		$oldOkrObjectiveValue = null;
		$oldOkrKeyResult = null;
		$oldOkrKeyResultValue = null;
		
		foreach ($models as $key => $model) {
			if ($currentRow === null) {
				$currentRow = $startRow;
			} else {
				$currentRow++;
				$spreadsheet->getActiveSheet()->insertNewRowBefore($totalBudgetRow);
				$totalBudgetRow++;
			}

			$newOkrCodeValue = $model->okr_code;
			if ($newOkrCodeValue != $oldOkrCodeValue) {
				if ($oldOkrCodeValue !== null) {
					// BSC Aspek
					$lastOkrCodeCell = "B" . ($currentRow - 1);
					if ($oldOkrCode != $lastOkrCodeCell) {
						$spreadsheet->getActiveSheet()->mergeCells("$oldOkrCode:$lastOkrCodeCell");
					}
					$spreadsheet->getActiveSheet()->setCellValue($oldOkrCode, $oldOkrCodeValue ?: '');
				}

				$oldOkrCode = 'B' . $currentRow;
				$oldOkrCodeValue = $newOkrCodeValue;
			}

			$newOkrObjectiveValue = $model->okr;
			if ($newOkrObjectiveValue != $oldOkrObjectiveValue) {
				if ($oldOkrObjectiveValue !== null) {
					// BSC Aspek
					$lastOkrObjectiveCell = "C" . ($currentRow - 1);
					if ($oldOkrObjective != $lastOkrObjectiveCell) {
						$spreadsheet->getActiveSheet()->mergeCells("$oldOkrObjective:$lastOkrObjectiveCell");
					}
					$spreadsheet->getActiveSheet()->setCellValue($oldOkrObjective, $oldOkrObjectiveValue ?: '');
				}

				$oldOkrObjective = 'C' . $currentRow;
				$oldOkrObjectiveValue = $newOkrObjectiveValue;
			}

			$newOkrKeyResultValue = @$model->getKeyResultExcel($model->id_objective);
			if ($newOkrKeyResultValue != $oldOkrKeyResultValue) {
				if ($oldOkrKeyResultValue !== null) {
					// BSC Aspek
					$lastOkrKeyResultCell = "D" . ($currentRow - 1);
					if ($oldOkrKeyResult != $lastOkrKeyResultCell) {
						$spreadsheet->getActiveSheet()->mergeCells("$oldOkrKeyResult:$lastOkrKeyResultCell");
					}
					$spreadsheet->getActiveSheet()->setCellValue($oldOkrKeyResult, $oldOkrKeyResultValue ?: '');
				}

				$oldOkrKeyResult = 'D' . $currentRow;
				$oldOkrKeyResultValue = $newOkrKeyResultValue;
			}

			// KODE PROKER
			$spreadsheet->getActiveSheet()->setCellValue('E' . $currentRow, @$model->initiative->initiatives_code ?: '');

			// PROGRAM KERJA
			$spreadsheet->getActiveSheet()->setCellValue('F' . $currentRow, @$model->initiative->workplan_name ?: '');

			// BOBOT
			$spreadsheet->getActiveSheet()->setCellValue('G' . $currentRow, ($grade = $model->initiative->grade ?: 0));

			// ALAT VERIFIKASI
			$spreadsheet->getActiveSheet()->setCellValue('H' . $currentRow, @$model->initiative->monitoring_tools ?: '');

			// PERIODE PELAKSANAAN
			$spreadsheet->getActiveSheet()->setCellValue('I' . $currentRow, $model->initiative->implementation ?: '');

			// TOTAL TARGET
			$spreadsheet->getActiveSheet()->setCellValue('J' . $currentRow, $model->initiative->target .  ' ' . $model->initiative->measure ?: '');

			// TARGET - K1
			$spreadsheet->getActiveSheet()->setCellValue('K' . $currentRow, $model->initiative->target_q1 ?: '');

			// TARGET - K2
			$spreadsheet->getActiveSheet()->setCellValue('L' . $currentRow, $model->initiative->target_q2 ?: '');

			// TARGET - K3
			$spreadsheet->getActiveSheet()->setCellValue('M' . $currentRow, $model->initiative->target_q3 ?: '');

			// TARGET - K4
			$spreadsheet->getActiveSheet()->setCellValue('N' . $currentRow, $model->initiative->target_q4 ?: '');

			// CATATAN
			$spreadsheet->getActiveSheet()->setCellValue('O' . $currentRow, @$model->initiative->note ?: '');

			$totalBobot += $grade;
		}

		if ($oldOkrCodeValue !== null) {
			// BSC Aspek
			$lastOkrCodeCell = "B$currentRow";
			if ($oldOkrCode != $lastOkrCodeCell) {
				$spreadsheet->getActiveSheet()->mergeCells("$oldOkrCode:$lastOkrCodeCell");
			}
			$spreadsheet->getActiveSheet()->setCellValue($oldOkrCode, $oldOkrCodeValue ?: '');
		}

		if ($oldOkrObjectiveValue !== null) {
			// BSC Aspek
			$lastOkrObjectiveCell = "C$currentRow";
			if ($oldOkrObjective != $lastOkrObjectiveCell) {
				$spreadsheet->getActiveSheet()->mergeCells("$oldOkrObjective:$lastOkrObjectiveCell");
			}
			$spreadsheet->getActiveSheet()->setCellValue($oldOkrObjective, $oldOkrObjectiveValue ?: '');
		}

		if ($oldOkrKeyResultValue !== null) {
			// BSC Aspek
			$lastOkrKeyResultCell = "D$currentRow";
			if ($oldOkrKeyResult != $lastOkrKeyResultCell) {
				$spreadsheet->getActiveSheet()->mergeCells("$oldOkrKeyResult:$lastOkrKeyResultCell");
			}
			$spreadsheet->getActiveSheet()->setCellValue($oldOkrKeyResult, $oldOkrKeyResultValue ?: '');
		}

		// total_bobot cell
		// $spreadsheet->getActiveSheet()->setCellValue('total_jumlah_anggaran', $formatter->asDecimal($totalBudgetValue));

		// label_total_divisi cell
		// $spreadsheet->getActiveSheet()->setCellValue('total_bobot', $formatter->asDecimal($totalBobot));

		// Set page orientation and size
		$spreadsheet->getActiveSheet()
			->getPageSetup()
			->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
		$spreadsheet->getActiveSheet()
			->getPageSetup()
			->setPaperSize(PageSetup::PAPERSIZE_A4);

		// Rename worksheet
		// $spreadsheet->getActiveSheet()->setTitle('Printing');

		$filename = trim("PROKER $year $deptName") . ".xlsx";

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');

		Yii::$app->end();
	}
}
