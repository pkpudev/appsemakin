<?php

namespace app\modules\rkat\controllers\initiatives;

use Yii;
use app\models\search\KpiDistributionSearch;
use app\models\Creator;
use app\models\VwEmployeeManager;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class DownloadAction extends \yii\base\Action
{
	public function run()
	{
		$searchModel = new KpiDistributionSearch();
		$dataProvider = $searchModel->searchForInitiatives(Yii::$app->request->post());
		$query = $dataProvider->query;
		if (!$searchModel->status_id) {
			$query->andFilterWhere(['i.status_id' => 4]); // Approved
		}
		$models = $query->all();
		$year = $searchModel->year;
		if ($searchModel->departments_id) {
			$creator = Creator::find()
				->where([
					'departments_id' => $searchModel->departments_id,
					'year' => $year,
				])->one();
		} else {
			$creator = \app\models\Creator::find()->where(['employee_id' => Yii::$app->user->id, 'year' => $year])->one();
		}
		$pjName = @$creator->employee->full_name ?: '';
		$formatter = Yii::$app->formatter;
		// if (@$creator->employee_id) {
		// 	$eManager = VwEmployeeManager::find()->where(['employee_id' => $creator->employee_id])->one();
		// 	$upperName = @$eManager->upper->full_name;
		// }
		$companyId = @$searchModel->kpi->bsc->company_id ?: (@$creator->employee->company_id ?: Yii::$app->user->identity->company_id);
		if ($companyId == 1) {
			$logo = 'pkpu.png';
			$lembaga = 'Human Initiative';
		} elseif ($companyId == 2) {
			$logo = 'izi.png';
			$lembaga = 'Inisiatif Zakat Indonesia (IZI)';
		} elseif ($companyId == 3) {
			$logo = 'cmn.png';
			$lembaga = 'Cipta Mandiri Nusantara (CMN)';
		} else {
			throw new \yii\web\HttpException(404, "Download tidak tersedia selain lembaga Human Initiative, Inisiatif Zakat Indonesia (IZI) dan Cipta Mandiri Nusantara (CMN)");
		}
		$directorate = @$creator->vwDepartment->directorate->deptname ?: '';
		$dir = strtoupper($directorate);
		$dir = ((strpos($dir, 'DIREKTUR') !== false) || (strpos($dir, 'DIREKTORAT') !== FALSE)) ? $dir : "DIREKTORAT {$dir}";

		$deptName = @$creator->department->deptname;
		$deptCode = @$creator->department_code;
		$deptnameUppercase = strtoupper($deptName);

		// Load excel from template
		$spreadsheet = IOFactory::load(Yii::getAlias("@app/templates/excel/PROKER.xlsx"));
		$spreadsheet->setActiveSheetIndexByName('PROKER');

		// logo cell
		$drawing = new Drawing();
		$drawing->setName('Logo');
		$drawing->setDescription('Logo');
		$drawing->setPath(Yii::getAlias("@webroot/images/{$logo}"));
		$drawing->setCoordinates('B2');
		$drawing->setHeight(90);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// instansi cell
		$spreadsheet->getActiveSheet()->setCellValue('instansi', strtoupper($lembaga));

		// divisi cell
		// $spreadsheet->getActiveSheet()->setCellValue('divisi', $dir);
		$spreadsheet->getActiveSheet()->setCellValue('divisi', $deptCode . ' ' . $deptnameUppercase);

		// tahun cell
		$spreadsheet->getActiveSheet()->setCellValue('tahun', 'PERIODE ' . $year);

		// kode dan divisi cell
		// $spreadsheet->getActiveSheet()->setCellValue('kode_dan_divisi', @$creator->department_code . ' ' . $deptnameUppercase);

		// label_total_divisi cell
		$spreadsheet->getActiveSheet()->setCellValue('label_total_divisi', "TOTAL ({$deptnameUppercase})");



		// tempat_tanggal cell
		// $spreadsheet->getActiveSheet()->setCellValue('tanggal', "Jakarta, " . $formatter->asDate('now', 'long'));

		// ttd_nama_atasan cell
		// $spreadsheet->getActiveSheet()->setCellValue('atasan_nama', @$upperName ?: '');

		// ttd_jabatan_atasan cell
		// $spreadsheet->getActiveSheet()->setCellValue('atasan_jabatan', @$eManager->upper->position->position_name ?: '');

		// ttd_nama_pj cell
		// $spreadsheet->getActiveSheet()->setCellValue('pj_nama', $pjName);

		// ttd_jabatan_pj cell
		// $spreadsheet->getActiveSheet()->setCellValue('pj_jabatan', @$creator->employee->position->position_name ?: '');

		$startRow = 11;
		$currentRow = null;
		$totalBudgetRow = 12;
		$totalBudgetValue = 0;
		$totalBobot = 0;
		$oldKpiDistributionCodeCell = null;
		$oldKpiDistributionCodeValue = null;

		foreach ($models as $key => $model) {
			if ($currentRow === null) {
				$currentRow = $startRow;
			} else {
				$currentRow++;
				$spreadsheet->getActiveSheet()->insertNewRowBefore($totalBudgetRow);
				$totalBudgetRow++;
			}

			$newKpiDistributionCodeValue = $model->kpi_distribution_code;
			if ($newKpiDistributionCodeValue != $oldKpiDistributionCodeValue) {
				if ($oldKpiDistributionCodeValue !== null) {
					// BSC Aspek
					$lastKpiDistributionCodeCell = "B" . ($currentRow - 1);
					if ($oldKpiDistributionCodeCell != $lastKpiDistributionCodeCell) {
						$spreadsheet->getActiveSheet()->mergeCells("$oldKpiDistributionCodeCell:$lastKpiDistributionCodeCell");
					}
					$spreadsheet->getActiveSheet()->setCellValue($oldKpiDistributionCodeCell, $oldKpiDistributionCodeValue ?: '');
				}

				$oldKpiDistributionCodeCell = 'B' . $currentRow;
				$oldKpiDistributionCodeValue = $newKpiDistributionCodeValue;
			}

			// DESKRIPSI
			$spreadsheet->getActiveSheet()->setCellValue('C' . $currentRow, @$model->initiatives);

			// NO
			$spreadsheet->getActiveSheet()->setCellValue('D' . $currentRow, ($key + 1));

			// PROGRAM KERJA
			$spreadsheet->getActiveSheet()->setCellValue('E' . $currentRow, @$model->initiative->workplan_name ?: '');

			// PELAKSANAAN - Tanggal Mulai
			$spreadsheet->getActiveSheet()->setCellValue('F' . $currentRow, @$model->initiative->getStartActivityDate() ?: '');

			// PELAKSANAAN - Tanggal Akhir
			$spreadsheet->getActiveSheet()->setCellValue('G' . $currentRow, @$model->initiative->getEndActivityDate() ?: '');

			// PELAKSANAAN - Periode
			$spreadsheet->getActiveSheet()->setCellValue('H' . $currentRow, @$model->initiative->implementation ?: '');

			// JUMLAH ANGGARAN
			/* $totalBudgetPlan = (float) (@$model->initiative->total_budget_plan ?: 0);
			$totalBudgetDetails = (float) (@$model->initiative->total_budget ?: 0);
			if (round($totalBudgetPlan) != round($totalBudgetDetails)) {
				$totalBudgetText .= " ({$formatter->asDecimal($totalBudgetDetails)})";
				$totalBudgetValue += $totalBudgetDetails;
			} else {
				$totalBudgetValue += $totalBudgetPlan;
				$totalBudgetText = $formatter->asDecimal($totalBudgetPlan);
			}
			$spreadsheet->getActiveSheet()->setCellValue('I' . $currentRow, $totalBudgetText ?: ''); */

			// KODE IKU-IS
			$spreadsheet->getActiveSheet()->setCellValue('I' . $currentRow, @$model->initiative->initiatives_code ?: '');

			// DESKRIPSI IKU-IS
			$spreadsheet->getActiveSheet()->setCellValue('J' . $currentRow, @$model->initiative->initiatives ?: '');

			// TARGET - NILAI
			$target = ($model->initiative) ? number_format($model->initiative->target, 0, ',', '.') . ' ' . $model->initiative->measure : '';
			$spreadsheet->getActiveSheet()->setCellValue('K' . $currentRow, $target ?: '');

			// TARGET - SATUAN
			$spreadsheet->getActiveSheet()->setCellValue('L' . $currentRow, $model->initiative->measure ?: '');

			// TARGET - K1
			$spreadsheet->getActiveSheet()->setCellValue('M' . $currentRow, $model->initiative->target_q1 ?: '');

			// TARGET - K2
			$spreadsheet->getActiveSheet()->setCellValue('N' . $currentRow, $model->initiative->target_q2 ?: '');

			// TARGET - K3
			$spreadsheet->getActiveSheet()->setCellValue('O' . $currentRow, $model->initiative->target_q3 ?: '');

			// TARGET - K4
			$spreadsheet->getActiveSheet()->setCellValue('P' . $currentRow, $model->initiative->target_q4 ?: '');

			// BOBOT
			$spreadsheet->getActiveSheet()->setCellValue('Q' . $currentRow, ($grade = $model->grade ?: 0));

			// PJ PENCAPAIAN
			$spreadsheet->getActiveSheet()->setCellValue('R' . $currentRow, @$model->initiative->pic_text ?: '');

			// ASUMSI
			$spreadsheet->getActiveSheet()->setCellValue('S' . $currentRow, @$model->initiative->assumption ?: '');

			// CATATAN
			$spreadsheet->getActiveSheet()->setCellValue('T' . $currentRow, @$model->initiative->note ?: '');

			$totalBobot += $grade;
		}
		if ($oldKpiDistributionCodeValue !== null) {
			// BSC Aspek
			$lastKpiDistributionCodeCell = "B$currentRow";
			if ($oldKpiDistributionCodeCell != $lastKpiDistributionCodeCell) {
				$spreadsheet->getActiveSheet()->mergeCells("$oldKpiDistributionCodeCell:$lastKpiDistributionCodeCell");
			}
			$spreadsheet->getActiveSheet()->setCellValue($oldKpiDistributionCodeCell, $oldKpiDistributionCodeValue ?: '');
		}

		// total_bobot cell
		// $spreadsheet->getActiveSheet()->setCellValue('total_jumlah_anggaran', $formatter->asDecimal($totalBudgetValue));

		// label_total_divisi cell
		$spreadsheet->getActiveSheet()->setCellValue('total_bobot', $formatter->asDecimal($totalBobot));

		// Set page orientation and size
		$spreadsheet->getActiveSheet()
			->getPageSetup()
			->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
		$spreadsheet->getActiveSheet()
			->getPageSetup()
			->setPaperSize(PageSetup::PAPERSIZE_A4);

		// Rename worksheet
		// $spreadsheet->getActiveSheet()->setTitle('Printing');

		$filename = trim("PROKER $year $deptName") . ".xlsx";

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');

		Yii::$app->end();
	}
}
