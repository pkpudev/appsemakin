<?php

namespace app\modules\strategic\controllers;

use app\models\Account;
use Yii;
use app\models\Bsc;
use app\models\Budget;
use app\models\Status;
use app\models\BudgetHistory;
use app\models\Ceiling;
use app\models\PositionStructureEmployee;
use app\models\VwDepartments;
use app\models\Initiatives as ModelsInitiatives;
use app\models\PositionStructure;
use app\models\UnitStructure;
use app\models\search\BudgetSearch;
use app\models\search\InitiativesSearch;
use app\models\search\ActivitySearch;
use app\models\VwEmployeeManager;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\strategic\controllers\budget\{CreateAllAction, DownloadAction, DownloadAction2};
use yii\helpers\Json;

/**
 * BudgetController implements the CRUD actions for Budget model.
 */
class BudgetController extends Controller
{
    public function actions()
    {
        return [
            'create-all' => CreateAllAction::class,
            'download' => DownloadAction::class,
            'download2' => DownloadAction2::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                    'bulk-source-of-fund' => ['post'],
                    'bulk-cost-type' => ['post'],
                    'bulk-cost-priority' => ['post'],
                ],
            ],
            // [
            //     'class' => 'yii\filters\AjaxFilter',
            //     'only' => ['create-all'],
            // ],
        ];
    }

    /**
     * Lists all Budget models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ActivitySearch();
        $searchModel->year = 2022;
        $dataProvider = $searchModel->searchForBudget(Yii::$app->request->queryParams);
        
        $info = [];
        $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
        foreach($employeePositions as $ep){
            if($ceiling = Ceiling::findOne(['unit_structure_id' => $ep->position->unit_structure_id])){
                array_push($info, ['unit' => $ceiling->unit->unit_name, 'ceiling_amount' => $ceiling->ceiling_amount, 'total_budget' => ModelsInitiatives::getTotalBudget($ep->position->unit_structure_id)]);
            }
        }

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD') || Yii::$app->user->can('Keuangan')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];

            $vwEmployeeManager = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->one();

            $directorates = UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
            foreach($directorates as $directorate){
                array_push($dept, $directorate->id);
                
                $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                foreach($divisions as $division){
                    array_push($dept, $division->id);
                }
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'id', $dept])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;

            $vwEmployeeManager = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->one();

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }
            
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }

        return $this->render('index', [
            'info' => $info,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
        ]);
    }

    public function actionIndex2()
    {
        $searchModel = new BudgetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $info = [];
        $employeePositions = PositionStructureEmployee::find()->alias('pse')->joinWith('position p')->where(['employee_id' => Yii::$app->user->id])->andWhere(['p.year' => $searchModel->year])->all();
        foreach($employeePositions as $ep){
            if($ceiling = Ceiling::findOne(['unit_structure_id' => $ep->position->unit_structure_id, 'year' => $searchModel->year])){
                array_push($info, ['unit' => $ceiling->unit->unit_name, 'ceiling_amount' => $ceiling->ceiling_amount, 'total_budget' => ModelsInitiatives::getTotalBudget($ep->position->unit_structure_id)]);
            }
        }

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD') || Yii::$app->user->can('Keuangan')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listPosition = ArrayHelper::map(PositionStructure::find()->where(['year' => $searchModel->year])->orderBy('position_name')->all(), 'id', function($model){
                return $model->position_name . ' (' . $model->unit->unit_name . ')';
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];

            $vwEmployeeManager = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->one();
            array_push($dept, $vwEmployeeManager->directorate_id);

            $directorates = UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
            foreach($directorates as $directorate){
                array_push($dept, $directorate->id);
                
                $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                foreach($divisions as $division){
                    array_push($dept, $division->id);
                }
            }
                
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'id', $dept])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listPosition = ArrayHelper::map(PositionStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'unit_structure_id', $dept])->orderBy('position_name')->all(), 'id', function($model){
                return $model->position_name . ' (' . $model->unit->unit_name . ')';
            });
        } else {
            $userID = Yii::$app->user->id;
            $isLeader = false;

            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);

                $units = Yii::$app->db->createCommand("SELECT id FROM management.unit_structure WHERE superior_unit_id = {$ep->position->unit_structure_id}")->queryColumn();
                foreach($units as $unit){
                    array_push($dept, $unit);
                }
                
                array_push($dept, $units);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }

                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }
            
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listPosition = ArrayHelper::map(PositionStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'unit_structure_id', $dept])->orderBy('position_name')->all(), 'id', function($model){
                return $model->position_name . ' (' . $model->unit->unit_name . ')';
            });
        }

        return $this->render('index2', [
            'info' => $info,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
            'listPosition' => $listPosition,
        ]);
    }

    /**
     * Displays a single Budget model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "<b>RAB #IKU-IS ".$model->activity->initiative->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> ($model->status_id == Status::DRAFT or $model->status_id == Status::REVISION)?
                            Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update2','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']) :
                            Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];    
        }else{
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Budget model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /* public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Budget();

        if(Yii::$app->user->can('BSC Admin')){
            $initiatives = \app\models\Initiatives::find()
                ->alias('i')
                ->select(['i.id', 'i.workplan_name', 'i.initiatives_code'])
                ->joinWith('kpiDistribution.kpi.objective.bsc b')
                ->where([
                    'has_budget'=>true,
                    'b.company_id'=>Yii::$app->user->identity->company_id,
                    'b.year'=>Bsc::getCurrentYear()
                ])->groupBy(['i.id'])
                ->having('(COALESCE(i.total_budget_plan, 0) - COALESCE(i.total_budget, 0) > 0)')
                ->orderby('initiatives_code')
                ->all();
        }else{
            $departments_id = \app\models\Creator::find()
                ->select('departments_id')
                ->where(['or',['employee_id'=>Yii::$app->user->id], ['admin_id'=>Yii::$app->user->id]])
                ->andWhere(['year'=>Bsc::getCurrentYear()])
                ->one();
            $initiatives = \app\models\Initiatives::find()
                ->alias('i')
                ->select(['i.id', 'i.workplan_name', 'i.initiatives_code'])
                ->joinWith(['kpiDistribution kd', 'kpiDistribution.kpi.objective.bsc b'])
                ->where(['has_budget'=>true,
                    'b.company_id'=>Yii::$app->user->identity->company_id,
                    'b.year'=>Bsc::getCurrentYear(),
                    'kd.departments_id'=>@$departments_id->departments_id
                ])->groupBy(['i.id'])
                ->having('(COALESCE(i.total_budget_plan, 0) - COALESCE(i.total_budget, 0) > 0)')
                ->orderby('initiatives_code')
                ->all();
        }
        $kpiDList = [];
        $activityList = [];
        foreach ($initiatives as $init) {
            $kpiDList[$init->id] = $init->initiatives_code . " - " . $init->workplan_name;
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "<b>Tambah Rincian Anggaran</b>",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'kpiDList' => $kpiDList,
                        'activityList' => $activityList
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                //simpan history draft budget
                $action = 'Input Draft';
                $this->history($model->id, $action);
                
                if ($initiative = $model->initiative) {
                    //$initiative->implementation = $model->filledMonths;
                    //$initiative->save();
                    $initiative->renewImplementationValue();
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "<b>Tambah Rincian Anggaran</b>",
                    'content'=>'<span class="text-success">Tambah Rincian Anggaran sukses</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Tambah Lagi',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "<b>Tambah Rincian Anggaran</b>",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'kpiDList' => $kpiDList,
                        'activityList' => $activityList
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                //simpan history draft budget
                $action = 'Input Draft';
                $this->history($model->id, $action);

                if ($initiative = $model->initiative) {
                    //$initiative->implementation = $model->filledMonths;
                    //$initiative->save();
                    $initiative->renewImplementationValue();
                }

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'kpiDList' => $kpiDList,
                    'activityList' => $activityList
                ]);
            }
        }
    } */

    /**
     * Updates an existing Budget model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if(Yii::$app->user->can('Admin')){
            $initiatives = ModelsInitiatives::find()
                                ->where(['<>', 'status_id', [Status::REJECTED, Status::DELETED]])
                                ->orderBy('id asc')
                                ->all();
        }else{
            $unitStructureId = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
            foreach($employeePositions as $ep){
                array_push($unitStructureId, $ep->position->unit_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($unitStructureId, $gm->unit_structure_id);
                    }
                }
            }
         
            $initiatives = ModelsInitiatives::find()
                                ->alias('i')
                                ->joinWith('position p')
                                ->where(['<>', 'status_id', [Status::REJECTED, Status::DELETED]])
                                ->andWhere(['in', 'p.unit_structure_id', $unitStructureId])
                                ->orderBy('id asc')
                                ->all();
        }

        $activities = \app\models\Activity::find()->where(['initiatives_id'=>$model->activity->initiatives_id])->orderBy('start_plan')->all();
        $accounts = Account::find()->where(['company_id'=>Yii::$app->user->identity->company_id, 'is_active'=>true, 'level'=>4])->andWhere(['or',['like','daf_account_no','555%',false],['like','daf_account_no','556%',false],['like','daf_account_no','122%',false],['like','daf_account_no','522%',false],['like','daf_account_no','523%',false]])->orderBy('daf_account_no')->all();
        $kpiDList = [];
        $activityList = [];
        $accountList = [];
        foreach ($initiatives as $init) {
            $kpiDList[$init->id] = $init->initiatives_code . " - " . $init->workplan_name;
        }
        foreach ($activities as $act) {
            $activityList[$act->id] = $act->activity_name;
        }
        foreach ($accounts as $row) {
            $accountList[$row->id] = $row->daf_account_no . " - " . $row->account_name;
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Anggaran #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'kpiDList' => $kpiDList,
                        'activityList' => $activityList,
                        'accountList' => $accountList
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['id' => 'submit-btn', 'class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                if ($initiative = $model->activity->initiative) {
                    $initiative->renewImplementationValue();
                }
                //simpan history update budget
                $action = 'Edit Anggaran';
                $this->history($model->id, $action);
                return [
                    // 'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Anggaran #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update Budget #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'kpiDList' => $kpiDList,
                        'activityList' => $activityList,
                        'accountList' => $accountList
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['id' => 'submit-btn', 'class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                if ($initiative = $model->initiative) {
                    $initiative->renewImplementationValue();
                }
                //simpan history update budget
                $action = 'Edit Anggaran';
                $this->history($model->id, $action);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'kpiDList' => $kpiDList,
                    'activityList' => $activityList,
                    'accountList' => $accountList
                ]);
            }
        }
    }

    public function actionUpdate2($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if(Yii::$app->user->can('Admin')){
            $initiatives = ModelsInitiatives::find()
                                ->where(['<>', 'status_id', [Status::REJECTED, Status::DELETED]])
                                ->orderBy('id asc')
                                ->all();
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];

            $vwEmployeeManager = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $model->year])->one();

            $directorates = UnitStructure::find()->where(['year' => date('Y')])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
            foreach($directorates as $directorate){
                array_push($dept, $directorate->id);
                
                $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                foreach($divisions as $division){
                    array_push($dept, $division->id);
                }
            }

            $initiatives = ModelsInitiatives::find()
                                ->alias('i')
                                ->joinWith('position p')
                                ->where(['<>', 'status_id', [Status::REJECTED, Status::DELETED]])
                                ->andWhere(['in', 'p.unit_structure_id', $dept])
                                ->orderBy('id asc')
                                ->all();
        } else{
            $unitStructureId = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
            foreach($employeePositions as $ep){
                array_push($unitStructureId, $ep->position->unit_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($unitStructureId, $gm->unit_structure_id);
                    }
                }
            }
         
            $initiatives = ModelsInitiatives::find()
                                ->alias('i')
                                ->joinWith('position p')
                                ->where(['<>', 'status_id', [Status::REJECTED, Status::DELETED]])
                                ->andWhere(['in', 'p.unit_structure_id', $unitStructureId])
                                ->orderBy('id asc')
                                ->all();
        }

        $accounts = Account::find()->where(['company_id'=>Yii::$app->user->identity->company_id, 'is_active'=>true, 'level'=>4])->andWhere(['or',['like','daf_account_no','555%',false],['like','daf_account_no','556%',false],['like','daf_account_no','122%',false],['like','daf_account_no','522%',false],['like','daf_account_no','523%',false]])->orderBy('daf_account_no')->all();
        $kpiDList = [];
        $accountList = [];
        foreach ($initiatives as $init) {
            $kpiDList[$init->id] = $init->initiatives_code . " - " . $init->workplan_name;
        }
        foreach ($accounts as $row) {
            $accountList[$row->id] = $row->daf_account_no . " - " . $row->account_name;
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Anggaran #".$id,
                    'content'=>$this->renderAjax('update2', [
                        'model' => $model,
                        'kpiDList' => $kpiDList,
                        'accountList' => $accountList
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['id' => 'submit-btn', 'class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save(false)){
                if ($initiative = $model->initiatives) {
                    $initiative->renewImplementationValue();
                }
                //simpan history update budget
                $action = 'Edit Anggaran';
                $this->history($model->id, $action);

                Yii::$app->response->format = Response::FORMAT_JSON;
                // Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index2']));
                return [
                    'forceReload'=>'#budget-datatable-pjax',
                    'title'=> "Anggaran #".$id,
                    'content'=>'Anggaran berhasil diubah',
                    // 'content'=>$this->renderAjax('view', [
                    //     'model' => $model,
                    // ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update Budget #".$id,
                    'content'=>$this->renderAjax('update2', [
                        'model' => $model,
                        'kpiDList' => $kpiDList,
                        'accountList' => $accountList
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['id' => 'submit-btn', 'class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                if ($initiative = $model->initiatives) {
                    $initiative->renewImplementationValue();
                }
                //simpan history update budget
                $action = 'Edit Anggaran';
                $this->history($model->id, $action);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update2', [
                    'model' => $model,
                    'kpiDList' => $kpiDList,
                    'accountList' => $accountList
                ]);
            }
        }
    }

    /**
     * Delete an existing Budget model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $page = null)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $initiativeId = $model->initiatives_id;
        $model->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index2']));
            return [
                    'forceClose'=>true,
                    // 'forceReload'=>'#crud-datatable-pjax'
                ];
        }else{
            /*
            *   Process for non-ajax request
            */
            if($page == 'initiatives2'){
                return $this->redirect(['/strategic/initiatives2/view', 'id' => $initiativeId]);
            } else {
                return $this->redirect(['index']);
            }
        }
    }

     /**
     * Delete multiple existing Budget model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionBulkSourceOfFund()
    {
        $request = Yii::$app->request;
        $sourceOfFundsId = $request->post('source-of-fund');
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->source_of_funds_id = $sourceOfFundsId;
            $model->save();            
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'forceReload'=>'#budget-datatable-pjax',
                    'title'=> 'Info',
                    'content'=>'Anggaran berhasil diubah',
                    'footer'=> Html::button('Tutup', ['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionBulkCostType()
    {
        $request = Yii::$app->request;
        $costType = $request->post('cost-type');
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->cost_type_id = $costType;
            $model->save(false);            
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'forceReload'=>'#budget-datatable-pjax',
                    'title'=> 'Info',
                    'content'=>'Anggaran berhasil diubah',
                    'footer'=> Html::button('Tutup', ['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionBulkCostPriority()
    {
        $request = Yii::$app->request;
        $costPriority = $request->post('cost-priority');
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->cost_priority_id = $costPriority;
            $model->save(false);            
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'forceReload'=>'#budget-datatable-pjax',
                    'title'=> 'Info',
                    'content'=>'Anggaran berhasil diubah',
                    'footer'=> Html::button('Tutup', ['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionBulkFinancingType()
    {
        $request = Yii::$app->request;
        $financingType = $request->post('financing-type');
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->financing_type_id = $financingType;
            $model->save(false);            
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'forceReload'=>'#budget-datatable-pjax',
                    'title'=> 'Info',
                    'content'=>'Anggaran berhasil diubah',
                    'footer'=> Html::button('Tutup', ['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Budget model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Budget the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Budget::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function history($id, $action, $comment='')
    {
        $history=new BudgetHistory;
        // $history->by = Yii::$app->user->identity->id;
        // $history->created_ip = Yii::$app->request->userIP;
        $history->budget_id = $id;
        $history->action = $action;
        $history->comment = $comment;
        $history->save();
    }

    public function actionProkerInfo($id, $showBudgetSource = '0', $showNmonths = '0')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (($info = \app\modules\strategic\models\Initiatives::findOne($id)) !== null) {
            $metadata = $info->getMetadataValues();
            if ( $months = @$metadata['implementation'] ) {
                if (isset($months['all']))
                    unset($months['all']);
                if (isset($months['year']))
                    unset($months['year']);
                if (isset($months['q1']))
                    unset($months['q1']);
                if (isset($months['q2']))
                    unset($months['q2']);
                if (isset($months['q3']))
                    unset($months['q3']);
                if (isset($months['q4']))
                    unset($months['q4']);
            }

            $deptid = $info->okr->position->unit_structure_id;
            $year = $info->okr->year;
            
            // Berdasarkan ceiling.ceiling_amount :
            $budgetUnused = \app\models\Ceiling::getPlafonBalance($deptid, $year);
            // Berdasarkan iniatives.total_budget_plan :
            // $budgetUnused = $info->total_budget_plan - $info->total_budget;
            
            $ceiling = \app\models\Ceiling::getPlafon($deptid, $year);

            $formatter = Yii::$app->formatter;

            $nmonths = [
                '01'=> ['label' => 'jan', 'show' => @$months['jan']],
                '02'=> ['label' => 'feb', 'show' => @$months['feb']],
                '03'=> ['label' => 'mar', 'show' => @$months['mar']],
                '04'=> ['label' => 'apr', 'show' => @$months['apr']],
                '05'=> ['label' => 'may', 'show' => @$months['may']],
                '06'=> ['label' => 'jun', 'show' => @$months['jun']],
                '07'=> ['label' => 'jul', 'show' => @$months['jul']],
                '08'=> ['label' => 'aug', 'show' => @$months['aug']],
                '09'=> ['label' => 'sep', 'show' => @$months['sep']],
                '10'=> ['label' => 'oct', 'show' => @$months['oct']],
                '11'=> ['label' => 'nov', 'show' => @$months['nov']],
                '12'=> ['label' => 'dec', 'show' => @$months['dec']],
            ];

            return [
                'implementation'    => $info->implementation,
                'budgetDetailed'    => $formatter->asCurrency($info->total_budget),
                'budgetUnused'      => $formatter->asCurrency($budgetUnused),
                'ceiling'           => $formatter->asCurrency($ceiling),
                'budgetUnusedNum'   => $budgetUnused,
                'kpi'               => $formatter->asNtext($info->initiatives),
                'target'            => "{$info->target} {$info->measure}",
                'months'            => $months,
                'nmonths'           => $nmonths,
            ];
        } else {
            return [];
        }
    }

    public function actionActivityList() 
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $initiative_id = $parents[0];
                // $param1 = null;
                // $param2 = null;
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];
                    // $param1 = $params[0]; // get the value of input-type-1
                    // $param2 = $params[1]; // get the value of input-type-2
                }
     
                $activities = \app\models\Activity::find()->where(['initiatives_id'=>$initiative_id])->orderBy('start_plan')->all();
                $out = [];
                foreach ($activities as $act) {
                    $out[] = ['id'=>$act->id, 'name'=>$act->activity_code.' '.$act->activity_name];
                }
                
                echo \yii\helpers\Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
}
