<?php

namespace app\modules\strategic\controllers;

use Yii;
use app\models\Activity;
use app\models\ActivityHistory;
use app\models\Bsc;
use app\models\Ceiling;
use app\models\Initiatives;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\Status;
use app\models\VwDepartments;
use app\models\search\ActivitySearch;
use app\models\UnitStructure;
use app\modules\strategic\controllers\activity\DownloadAction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * ActivityController implements the CRUD actions for Activity model.
 */
class ActivityController extends Controller
{
    public function actions()
    {
        return [
            'download' => DownloadAction::class,
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Activity models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ActivitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $info = [];
        $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
        foreach($employeePositions as $ep){
            if($ceiling = Ceiling::findOne(['unit_structure_id' => $ep->position->unit_structure_id])){
                array_push($info, ['unit' => $ceiling->unit->unit_name, 'ceiling_amount' => $ceiling->ceiling_amount, 'total_budget' => Initiatives::getTotalBudget($ep->position->unit_structure_id)]);
            }
        }

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }

        return $this->render('index', [
            'info' => $info,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
        ]);
    }


    /**
     * Displays a single Activity model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Activity #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Activity model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Activity();
        $user = Yii::$app->getUser();
        $identity = $user->getIdentity();
        $okr = Okr::find()->orderBy('year desc')->one();
        $year = $okr->year ?: date('Y');

        if($user->can('Admin')){
            $initiativeList = ArrayHelper::map(Initiatives::find()
                                                ->where(['<>', 'status_id', [Status::REJECTED, Status::DELETED]])
                                                ->orderBy('id asc')
                                                ->all(), 'id', function($model){
                                                    return $model->initiatives_code . ' - ' . $model->workplan_name;
                                                });
                                                
        }else{

            $unitStructureId = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
            foreach($employeePositions as $ep){
                array_push($unitStructureId, $ep->position->unit_structure_id);
            }
         
            $initiativeList = ArrayHelper::map(Initiatives::find()
                        ->alias('i')
                        ->joinWith('position p')
                        ->where(['<>', 'i.status_id', [Status::REJECTED, Status::DELETED]])
                        ->andWhere(['in', 'p.unit_structure_id', $unitStructureId])
                        ->orderBy('i.id asc')
                        ->all(), 'id', function($model){
                            return $model->initiatives_code . ' - ' . $model->workplan_name;
                        });
        }
        
        $picList = [];

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Rencana Kegiatan Kerja",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'initiativeList' => $initiativeList,
                        'picList' => $picList
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){

                ActivityHistory::createHistory($model->id, 'Input Kegiatan', '');

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Rencana Kegiatan Kerja",
                    'content'=>'<span class="text-success">Rencana Kegiatan Kerja berhasil ditambahkan. </span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Tambah Lagi',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Tambah Rencana Kegiatan Kerja",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'initiativeList' => $initiativeList,
                        'picList' => $picList
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'initiativeList' => $initiativeList,
                    'picList' => $picList
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Activity model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $user = Yii::$app->getUser();
        $identity = $user->getIdentity();
        $okr = Okr::find()->orderBy('year desc')->one();
        $year = $okr->year ?: date('Y');

        if($user->can('Admin')){
            $initiativeList = ArrayHelper::map(Initiatives::find()
                                                ->where(['<>', 'status_id', [Status::REJECTED, Status::DELETED]])
                                                ->orderBy('id asc')
                                                ->all(), 'id', function($model){
                                                    return $model->initiatives_code . ' - ' . $model->workplan_name;
                                                });
                                                
        }else{

            $unitStructureId = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
            foreach($employeePositions as $ep){
                array_push($unitStructureId, $ep->position->unit_structure_id);
            }
         
            $initiativeList = ArrayHelper::map(Initiatives::find()
                        ->alias('i')
                        ->joinWith('position p')
                        ->where(['<>', 'i.status_id', [Status::REJECTED, Status::DELETED]])
                        ->andWhere(['in', 'p.unit_structure_id', $unitStructureId])
                        ->orderBy('i.id asc')
                        ->all(), 'id', function($model){
                            return $model->initiatives_code . ' - ' . $model->workplan_name;
                        });
        }

        $picList = [];
        $pse = PositionStructureEmployee::find()->joinWith(['position p'])->where(['p.unit_structure_id' => $model->initiative->okr->position->unit_structure_id])->all();
        foreach($pse as $ps){
            $picList[$ps->employee_id] = $ps->employee->full_name . ' - ' . $ps->position->position_name;
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Activity #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    'initiativeList' => $initiativeList,
                        'picList' => $picList
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){

                ActivityHistory::createHistory($model->id, 'Update Kegiatan', '');

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Activity #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    'initiativeList' => $initiativeList,
                        'picList' => $picList
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update Activity #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    'initiativeList' => $initiativeList,
                        'picList' => $picList
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                
                ActivityHistory::createHistory($model->id, 'Update Kegiatan', '');

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'initiativeList' => $initiativeList,
                    'picList' => $picList
                ]);
            }
        }
    }


    public function actionPositionEmployee() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $idProker = $_POST['depdrop_parents'];

            $unitStructureId = Initiatives::findOne($idProker)->okr->position->unit_structure_id;

            $out = [];
            $pse = PositionStructureEmployee::find()->joinWith(['position p'])->where(['p.unit_structure_id' => $unitStructureId])->all();
            foreach($pse as $ps){
                array_push($out, ['id' => $ps->employee_id, 'name' => $ps->employee->full_name . ' - ' . $ps->position->position_name]);
            }
        }
        return ['output'=>$out, 'selected'=>''];
    }

    /**
     * Delete an existing Activity model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Activity model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Activity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Activity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Activity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
