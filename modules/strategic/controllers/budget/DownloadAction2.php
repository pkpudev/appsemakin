<?php

namespace app\modules\strategic\controllers\budget;

use Yii;
use app\models\search\BudgetSearch;
use app\models\UnitStructure;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class DownloadAction2 extends \yii\base\Action
{
	public function run()
	{
        $searchModel = new BudgetSearch();
		
		$post = Yii::$app->request->post();
		// var_dump($post);die;
		$init['BudgetSearch']['year'] = $post['BudgetSearch']['year'];
		$init['BudgetSearch']['unit_id'] = $post['BudgetSearch']['unit_id'];
		$init['BudgetSearch']['workplan_name'] = $post['BudgetSearch']['workplan_name'];
		$init['BudgetSearch']['activity_name'] = $post['BudgetSearch']['activity_name'];
		$init['BudgetSearch']['account_id'] = $post['BudgetSearch']['account_id'];
		$init['BudgetSearch']['volume'] = $post['BudgetSearch']['volume'];
		$init['BudgetSearch']['frequency'] = $post['BudgetSearch']['frequency'];
		$init['BudgetSearch']['unit_amount'] = $post['BudgetSearch']['unit_amount'];
		$init['BudgetSearch']['total_amount'] = $post['BudgetSearch']['total_amount'];
		$init['BudgetSearch']['source_of_funds_id'] = $post['BudgetSearch']['source_of_funds_id'];
		$init['BudgetSearch']['cost_type_id'] = $post['BudgetSearch']['cost_type_id'];
		$init['BudgetSearch']['cost_priority_id'] = $post['BudgetSearch']['cost_priority_id'];
		
		$dataProvider = $searchModel->search($init);
		// var_dump($searchModel);die;
		// $dataProvider = $searchModel->searchForBudget(Yii::$app->request->post());

		$models = $dataProvider->query->all();
		$year = $searchModel->year;
		$unit = UnitStructure::findOne($searchModel->unit_id);
		$directorate = $unit->unit_name ?: '';
		$level = $unit->level ?: '';
		$dir = strtoupper($directorate);
		
		$deptName = @$unit->unit_name;
		$deptCode = @$unit->unit_code;
		$deptnameUppercase = strtoupper($deptName);

		if($level == 1){
			$dir = "DIREKTORAT {$dir} ({$deptCode})";
		} else if($level == 2){
			$dir = "DEPARTEMEN {$dir} ({$deptCode})";
		} else if($level == 3){
			$dir = "DIVISI {$dir} ({$deptCode})";
		} else if($level == 4){
			$dir = "BIDANG {$dir} ({$deptCode})";
		} else if($level == 5){
			$dir = "SQUAD {$dir} ({$deptCode})";
		}
		
		$logo = 'pkpu.png';
		$lembaga = 'Human Initiative';

		// Load excel from template
		$spreadsheet = IOFactory::load(Yii::getAlias("@app/templates/excel/budget.xlsx"));
		$spreadsheet->setActiveSheetIndexByName('BUDGET');

		// logo cell
		$drawing = new Drawing();
		$drawing->setName('Logo');
		$drawing->setDescription('Logo');
		$drawing->setPath(Yii::getAlias("@webroot/images/{$logo}"));
		$drawing->setCoordinates('B2');
		$drawing->setHeight(100);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// instansi cell
		$spreadsheet->getActiveSheet()->setCellValue('instansi', strtoupper($lembaga));

		// kode_iku_divisi cell
		$spreadsheet->getActiveSheet()->setCellValue('kode_divisi', ($deptCode = @$deptCode ?: ''));

		// divisi_kerja cell
		$spreadsheet->getActiveSheet()->setCellValue('divisi', ($deptName = @$deptName ?: ''));

		// direktorat cell
		$spreadsheet->getActiveSheet()->setCellValue('direktorat', $dir);

		// periode_kinerja cell
		$spreadsheet->getActiveSheet()->setCellValue('tahun', "PERIODE $year");

		$columns = ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		$amountColumns = ['I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V'];
		$countModels = count($models);
		$startRow = 13;
		$currentRow = $startRow;
		$totalAnggaranProkerRow = 14;
		$totalAnggaranDivisiRow = 15;
		$prokerNo = 1;
		$budgetNo = 1;
		$totalAnggaranDivisi = 0;
		$totalAnggaranDivisiBulan1 = 0;
		$totalAnggaranDivisiBulan2 = 0;
		$totalAnggaranDivisiBulan3 = 0;
		$totalAnggaranDivisiBulan4 = 0;
		$totalAnggaranDivisiBulan5 = 0;
		$totalAnggaranDivisiBulan6 = 0;
		$totalAnggaranDivisiBulan7 = 0;
		$totalAnggaranDivisiBulan8 = 0;
		$totalAnggaranDivisiBulan9 = 0;
		$totalAnggaranDivisiBulan10 = 0;
		$totalAnggaranDivisiBulan11 = 0;
		$totalAnggaranDivisiBulan12 = 0;
		$oldProkerNoCell = 'B' . $currentRow;
		$oldProkerCell = 'C' . $currentRow;
		$oldKodeAnggaranCell = 'X' . $currentRow;
		$oldSumberDanaCell = 'Y' . $currentRow;
		$oldKodeAsnafCell = 'Z' . $currentRow;
		$startAmountRow = $currentRow;
		$oldProker = null;
		$oldKodeAnggaran = null;
		$oldSumberDana = null;
		$oldKodeAsnaf = null;
		$totalProkerRows = [];

		foreach ($models as $key => $model) {

			$lastRow = $currentRow - 1;

			// PROKER
			$newProker = $model->workplan_name;
			if ($oldProker === null) {
				$oldProker = $newProker;
				// $oldKodeAnggaran = $model->i_budget_code;
				// $oldSumberDana = $model->fundSource;
				// $oldKodeAsnaf = $model->asnaf;
			} else {
				if ($newProker != $oldProker) {
					// Proker					
					$lastProkerCell = "C$lastRow";
					if ($oldProkerCell != $lastProkerCell) {
						$lastProkerNoCell = "B$lastRow";
						$lastKodeAnggaranCell = "X$lastRow";
						$lastSumberDanaNoCell = "Y$lastRow";
						$lastKodeAsnafCell = "Z$lastRow";

						$spreadsheet->getActiveSheet()->mergeCells("$oldProkerNoCell:$lastProkerNoCell");
						$spreadsheet->getActiveSheet()->mergeCells("$oldProkerCell:$lastProkerCell");
						// $spreadsheet->getActiveSheet()->mergeCells("$oldKodeAnggaranCell:$lastKodeAnggaranCell");
						// $spreadsheet->getActiveSheet()->mergeCells("$oldSumberDanaCell:$lastSumberDanaNoCell");
						// $spreadsheet->getActiveSheet()->mergeCells("$oldKodeAsnafCell:$lastKodeAsnafCell");
					}
					$spreadsheet->getActiveSheet()->setCellValue($oldProkerNoCell, $prokerNo ?: '');
					$spreadsheet->getActiveSheet()->setCellValue($oldProkerCell, $oldProker ?: '');
					// $spreadsheet->getActiveSheet()->setCellValue($oldKodeAnggaranCell, $oldKodeAnggaran ?: '');
					// $spreadsheet->getActiveSheet()->setCellValue($oldSumberDanaCell, $oldSumberDana ?: '');
					// $spreadsheet->getActiveSheet()->setCellValue($oldKodeAsnafCell, $oldKodeAsnaf ?: '');

					$spreadsheet->getActiveSheet()->setCellValue("B{$totalAnggaranProkerRow}", "Jumlah Anggaran $oldProker");
					foreach ($amountColumns as $col) {
						$startCell = $col . $startAmountRow;
						$lastCell = $col . $lastRow;
						if ($startCell != $lastCell) {
							$sumFormula = "=SUM({$col}{$startAmountRow}:{$col}{$lastRow})";
						} else {
							$sumFormula = "=$startCell";
						}
						$spreadsheet->getActiveSheet()->setCellValue("{$col}{$totalAnggaranProkerRow}", $sumFormula);
					}
					$totalProkerRows[] = $totalAnggaranProkerRow;

					$spreadsheet->getActiveSheet()->insertNewRowBefore($totalAnggaranDivisiRow);
					$totalAnggaranDivisiRow++;
					foreach ($columns as $col) {
						$spreadsheet->getActiveSheet()
							->duplicateStyle(
								$spreadsheet->getActiveSheet()->getStyle("{$col}{$startRow}"),
								$col . ($totalAnggaranDivisiRow - 1)
							);
					}

					$spreadsheet->getActiveSheet()->insertNewRowBefore($totalAnggaranDivisiRow);
					$spreadsheet->getActiveSheet()
						->duplicateStyle(
							$spreadsheet->getActiveSheet()->getStyle("B{$totalAnggaranProkerRow}:I{$totalAnggaranProkerRow}"),
							"B{$totalAnggaranDivisiRow}:I{$totalAnggaranDivisiRow}"
						);
					foreach ($amountColumns as $col) {
						$spreadsheet->getActiveSheet()
							->duplicateStyle(
								$spreadsheet->getActiveSheet()->getStyle("{$col}{$totalAnggaranProkerRow}"),
								"{$col}{$totalAnggaranDivisiRow}"
							);
					}
					$spreadsheet->getActiveSheet()
						->duplicateStyle(
							$spreadsheet->getActiveSheet()->getStyle("W{$totalAnggaranProkerRow}:Z{$totalAnggaranProkerRow}"),
							"W{$totalAnggaranDivisiRow}:Z{$totalAnggaranDivisiRow}"
						);
					$spreadsheet->getActiveSheet()->mergeCells("B{$totalAnggaranDivisiRow}:I{$totalAnggaranDivisiRow}");
					$spreadsheet->getActiveSheet()->mergeCells("W{$totalAnggaranDivisiRow}:Z{$totalAnggaranDivisiRow}");

					$currentRow++;
					$totalAnggaranProkerRow = $totalAnggaranDivisiRow;
					$totalAnggaranDivisiRow++;

					$oldProkerNoCell = "B$currentRow";
					$oldProkerCell = "C$currentRow";
					$oldKodeAnggaranCell = "W$currentRow";
					$oldSumberDanaCell = "X$currentRow";
					$oldKodeAsnafCell = "Z$currentRow";
					$startAmountRow = $currentRow;
					$budgetNo = 1;
					$prokerNo++;

					$oldProker = $newProker;
					$oldKodeAnggaran = $model->budget_code;
					$oldSumberDana = $model->source_of_funds_id ? $model->sourceOfFunds->source_of_funds: '';
				} else {
					$spreadsheet->getActiveSheet()->insertNewRowBefore($totalAnggaranProkerRow);
					$totalAnggaranProkerRow++;
					$totalAnggaranDivisiRow++;
				}
			}

			// Kode Akun 
			$spreadsheet->getActiveSheet()->setCellValue('D' . $currentRow, (($model->description) ? $model->description:'') ?: '');

			// Kode Akun
			$spreadsheet->getActiveSheet()->setCellValue('E' . $currentRow, ($model->getAccountDafNo($model->account_id) ?: ''));
			
			// Rincian Kebutuhan
			$spreadsheet->getActiveSheet()->setCellValue('F' . $currentRow, ($model->getAccountName($model->account_id)? : ''));

			// Volume
			$spreadsheet->getActiveSheet()->setCellValue('G' . $currentRow, ($model->volume ?: 0) . ' ' . $model->budget_measure);

			// Frekuensi
			$spreadsheet->getActiveSheet()->setCellValue('H' . $currentRow, ($model->frequency ?: 0)  . ' ' . $model->freq_measure);

			// Satuan
			$spreadsheet->getActiveSheet()->setCellValue('I' . $currentRow, $model->unit_amount ?: 0);

			// Jumlah
			$spreadsheet->getActiveSheet()->setCellValue('J' . $currentRow, $model->total_amount ?: 0);

			// Budget Januari
			$spreadsheet->getActiveSheet()->setCellValue('K' . $currentRow, $model->month_01 ?: 0);

			// Budget Februari
			$spreadsheet->getActiveSheet()->setCellValue('L' . $currentRow, $model->month_02 ?: 0);

			// Budget Maret
			$spreadsheet->getActiveSheet()->setCellValue('M' . $currentRow, $model->month_03 ?: 0);

			// Budget April
			$spreadsheet->getActiveSheet()->setCellValue('N' . $currentRow, $model->month_04 ?: 0);

			// Budget Mei
			$spreadsheet->getActiveSheet()->setCellValue('O' . $currentRow, $model->month_05 ?: 0);

			// Budget Juni
			$spreadsheet->getActiveSheet()->setCellValue('P' . $currentRow, $model->month_06 ?: 0);

			// Budget Juli
			$spreadsheet->getActiveSheet()->setCellValue('Q' . $currentRow, $model->month_07 ?: 0);

			// Budget Agustus
			$spreadsheet->getActiveSheet()->setCellValue('R' . $currentRow, $model->month_08 ?: 0);

			// Budget September
			$spreadsheet->getActiveSheet()->setCellValue('S' . $currentRow, $model->month_09 ?: 0);

			// Budget Oktober
			$spreadsheet->getActiveSheet()->setCellValue('T' . $currentRow, $model->month_10 ?: 0);

			// Budget November
			$spreadsheet->getActiveSheet()->setCellValue('U' . $currentRow, $model->month_11 ?: 0);

			// Budget Desember
			$spreadsheet->getActiveSheet()->setCellValue('V' . $currentRow, $model->month_12 ?: 0);

			// Budget Code
			$spreadsheet->getActiveSheet()->setCellValue('W' . $currentRow, $model->budget_code ? $model->budget_code : '');

			// Sumber Dana
			$spreadsheet->getActiveSheet()->setCellValue('X' . $currentRow, $model->source_of_funds_id ? $model->sourceOfFunds->source_of_funds : '');

			// Jenis Pembiayaan
			$spreadsheet->getActiveSheet()->setCellValue('Y' . $currentRow, $model->financing_type_id ? $model->financingType->financing_type: '');

			// Prioritas Pembiayaan
			$spreadsheet->getActiveSheet()->setCellValue('Z' . $currentRow, $model->cost_priority_id ? $model->costPriority->cost_priority : '');

			$currentRow++;
			$budgetNo++;
		}

		if ($oldProker !== null) {

			$lastRow = $currentRow - 1;
			// Proker					
			$lastProkerCell = "C$lastRow";
			if ($oldProkerCell != $lastProkerCell) {
				$lastProkerNoCell = "B$lastRow";
				// $lastKodeAnggaranCell = "X$lastRow";
				// $lastSumberDanaNoCell = "Y$lastRow";
				// $lastKodeAsnafCell = "Z$lastRow";

				$spreadsheet->getActiveSheet()->mergeCells("$oldProkerNoCell:$lastProkerNoCell");
				$spreadsheet->getActiveSheet()->mergeCells("$oldProkerCell:$lastProkerCell");
				// $spreadsheet->getActiveSheet()->mergeCells("$oldKodeAnggaranCell:$lastKodeAnggaranCell");
				// $spreadsheet->getActiveSheet()->mergeCells("$oldSumberDanaCell:$lastSumberDanaNoCell");
				// $spreadsheet->getActiveSheet()->mergeCells("$oldKodeAsnafCell:$lastKodeAsnafCell");
			}
			$spreadsheet->getActiveSheet()->setCellValue($oldProkerNoCell, $prokerNo ?: '');
			$spreadsheet->getActiveSheet()->setCellValue($oldProkerCell, $oldProker ?: '');
			$spreadsheet->getActiveSheet()->setCellValue($oldKodeAnggaranCell, $oldKodeAnggaran ?: '');
			$spreadsheet->getActiveSheet()->setCellValue($oldSumberDanaCell, $oldSumberDana ?: '');
			$spreadsheet->getActiveSheet()->setCellValue($oldKodeAsnafCell, $oldKodeAsnaf ?: '');

			$spreadsheet->getActiveSheet()->setCellValue("B{$totalAnggaranProkerRow}", "Jumlah Anggaran $oldProker");
			foreach ($amountColumns as $col) {
				$startCell = $col . $startAmountRow;
				$lastCell = $col . $lastRow;
				if ($startCell != $lastCell) {
					$sumFormula = "=SUM({$col}{$startAmountRow}:{$col}{$lastRow})";
				} else {
					$sumFormula = "=$startCell";
				}
				$spreadsheet->getActiveSheet()->setCellValue("{$col}{$totalAnggaranProkerRow}", $sumFormula);
			}
			$totalProkerRows[] = $totalAnggaranProkerRow;
		}

		// TOTAL ANGGARAN DIVISI
		$spreadsheet->getActiveSheet()->setCellValue("B{$totalAnggaranDivisiRow}", "TOTAL ANGGARAN " . strtoupper($deptName));
		foreach ($amountColumns as $col) {
			$cells = $totalProkerRows;
			foreach ($cells as $q => $row) {
				$cells[$q] = $col . $row;
			}
			$additionFormula = implode('+', $cells);
			$spreadsheet->getActiveSheet()->setCellValue("{$col}{$totalAnggaranDivisiRow}", "=$additionFormula");
		}

		// Set page orientation and size
		$spreadsheet->getActiveSheet()
			->getPageSetup()
			->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
		$spreadsheet->getActiveSheet()
			->getPageSetup()
			->setPaperSize(PageSetup::PAPERSIZE_A4);

		// Rename worksheet
		// $spreadsheet->getActiveSheet()->setTitle('Printing');

		$filename = trim("ANGGARAN $year $deptName") . ".xlsx";

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');

		Yii::$app->end();
	}
}
