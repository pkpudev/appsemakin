<?php

namespace app\modules\strategic\controllers\budget;

use Yii;
use yii\base\Action;
use yii\web\Response;
use yii\helpers\Html;
use app\models\{Initiatives, Okr, Account, PositionStructureEmployee, Status};
use app\modules\strategic\models\{MultiBudgetForm,MultiBudget};
use yii\helpers\ArrayHelper;

class CreateAllAction extends Action
{
	public function run($id = null)
    {

        $initiatives = Initiatives::findOne($id);

        $request = Yii::$app->request;
        $model = new MultiBudgetForm();
        $budgetModel = new MultiBudget();
        $user = Yii::$app->getUser();
        $identity = $user->getIdentity();
        $okr = Okr::find()->orderBy('year desc')->one();
        $year = $okr->year ?: date('Y');

        if($user->can('Admin')){
            $initiativeList = ArrayHelper::map(Initiatives::find()
                                                ->where(['<>', 'status_id', [Status::REJECTED, Status::DELETED]])
                                                ->orderBy('id asc')
                                                ->all(), 'id', function($model){
                                                    return $model->initiatives_code . ' - ' . $model->workplan_name;
                                                });
                                                
        }else{

            $unitStructureId = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
            foreach($employeePositions as $ep){
                array_push($unitStructureId, $ep->position->unit_structure_id);
            }
         
            $initiativeList = ArrayHelper::map(Initiatives::find()
                        ->alias('i')
                        ->joinWith('position p')
                        ->where(['<>', 'status_id', [Status::REJECTED, Status::DELETED]])
                        ->andWhere(['in', 'p.unit_structure_id', $unitStructureId])
                        ->orderBy('id asc')
                        ->all(), 'id', function($model){
                            return $model->initiatives_code . ' - ' . $model->workplan_name;
                        });
        }
        
        $accounts = Account::find()->where(['company_id'=>Yii::$app->user->identity->company_id, 'is_active'=>true, 'level'=>4])->andWhere(['or',['like','daf_account_no','555%',false],['like','daf_account_no','556%',false],['like','daf_account_no','122%',false],['like','daf_account_no','522%',false],['like','daf_account_no','523%',false]])->orderBy('daf_account_no')->all();
        $activityList = [];
        $kpiDList = [];
        $accountList = [];

        foreach ($accounts as $row) {
            $accountList[$row->id] = $row->daf_account_no . " - " . $row->account_name;
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isPost) {
                $model->activity_id = $_POST['MultiBudgetForm']['activity_id'];
                // $model->initiatives_id = $id;
                $model->budgetsPostData = $_POST['MultiBudget'];
                if ($model->save()) {
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "<b>Membuat Anggaran Proker</b>",
                        'content'=>'<span class="text-success">Tambah Rincian Anggaran sukses</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]),
            
                    ];  
                }
            }

            return [
                'title'=> "<b>Membuat Anggaran Proker</b>",
                'content'=>$this->controller->renderAjax('create-all', [
                    'model' => $model,
                    'budgetModel' => $budgetModel,
                    'initiativeList' => $initiativeList,
                    'activityList' => $activityList,
                    'accountList' => $accountList,
                    'initiatives' => $initiatives
                ]),
                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

            ];
        } else {
            if ($request->post()) {
                // $model->activity_id = $_POST['MultiBudgetForm']['activity_id'];
                // $model->initiatives_id = $id;
                $model->budgetsPostData = $_POST['MultiBudget'];
                if ($model->save()) {
                    if($id){
                        return Yii::$app->response->redirect(['/strategic/initiatives2/view', 'id' => $id]);
                    } else {
                        return Yii::$app->response->redirect(['strategic/budget/index']);
                    }
                } else {
                    return $this->controller->render('create-all', [
                        'model' => $model,
                        'budgetModel' => $budgetModel,
                        'initiativeList' => $initiativeList,
                        'activityList' => $activityList,
                        'accountList' => $accountList,
                        'initiatives' => $initiatives
                    ]);
                }
                
            }
            else{
                return $this->controller->render('create-all', [
                    'model' => $model,
                    'budgetModel' => $budgetModel,
                    'initiativeList' => $initiativeList,
                    'activityList' => $activityList,
                    'accountList' => $accountList,
                    'initiatives' => $initiatives
                ]);
            }
        }
    }
}