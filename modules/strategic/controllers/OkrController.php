<?php

namespace app\modules\strategic\controllers;

use app\components\FilesHelper;
use app\models\CascadingType;
use app\models\Initiatives;
use Yii;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\OkrMovType;
use app\models\OkrOrg;
use app\models\OkrOrgMov;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\search\OkrOrgSearch;
use app\models\search\OkrSearch;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use app\modules\strategic\controllers\okr\DownloadAction;
use PhpOffice\PhpSpreadsheet\IOFactory;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * OkrController implements the CRUD actions for Okr model.
 */
class OkrController extends Controller
{
    public function actions()
    {
        return [
            'download' => DownloadAction::class,
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Okr models.
     * @return mixed
     */
    /* public function actionIndex($yearFilter = null, $unitFilter = null, $id = null)
    {

        if(!$yearFilter) {
            $lastOkr = Okr::find()->orderBy('year desc')->one();
            $yearFilter = $lastOkr->year ?: date('Y');
        }

        if(Yii::$app->user->can('Admin')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $yearFilter])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->where(['year' => $yearFilter])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }
        
        $modelOkr = Okr::find()
                        ->where(['okr_level_id' => OkrLevel::ORGANIZATION])
                        ->andWhere(['type' => 'O'])
                        ->andWhere('parent_id is null');

        if($yearFilter){
            $modelOkr->andWhere(['year' => $yearFilter]);
        }

        if($unitFilter){
            $unit = UnitStructure::findOne($unitFilter);
            $positions = PositionStructure::find()->select('id')->where(['unit_structure_id' => $unitFilter])->all();
            $unitLevel = $unit->level;

            if($unitLevel == 1){
                
                if($positions){
                    $positionIds = [];
                    $showOkrIds = [];
                    foreach($positions as $p){
                        array_push($positionIds, $p->id);
                    }

                    $okrsLv1 = Okr::find()->where(['in', 'position_structure_id', $positionIds])->all();
                    foreach($okrsLv1 as $okrLv1){
                        array_push($showOkrIds, $okrLv1->id);

                        if($okrLv1->parent->type == 'KR'){
                            array_push($showOkrIds, $okrLv1->parent->parent_id);
                            array_push($showOkrIds, $okrLv1->parent_id);
                        } else {
                            array_push($showOkrIds, $okrLv1->parent_id);
                        }
                    }
                }

                //DIRECTORATE
            } else if($unitLevel == 2){
                
                if($positions){
                    $positionIds = [];
                    $showOkrIds = [];
                    foreach($positions as $p){
                        array_push($positionIds, $p->id);
                    }

                    $okrsLv3 = Okr::find()->where(['in', 'position_structure_id', $positionIds])->all();
                    $okrLv2Ids = [];
                    foreach($okrsLv3 as $okrLv3){
                        array_push($showOkrIds, $okrLv3->id);

                        if($okrLv3->parent->type == 'KR'){
                            array_push($okrLv2Ids, $okrLv3->parent->parent_id);
                            array_push($showOkrIds, $okrLv3->parent->parent_id);
                            array_push($showOkrIds, $okrLv3->parent_id);
                        } else {
                            array_push($okrLv2Ids, $okrLv3->parent_id);
                            array_push($showOkrIds, $okrLv3->parent_id);
                        }
                    }

                    $okrsLv1 = Okr::find()->where(['in', 'id', $okrLv2Ids])->all();
                    foreach($okrsLv1 as $okrLv1){
                        if($okrLv1->parent->type == 'KR'){
                            array_push($showOkrIds, $okrLv1->parent->parent_id);
                            array_push($showOkrIds, $okrLv1->parent_id);
                        } else {
                            array_push($showOkrIds, $okrLv1->parent_id);
                        }
                    }
                }

                //DEPARTEMENT
            } else if($unitLevel == 3){
                
                if($positions){
                    $positionIds = [];
                    $showOkrIds = [];
                    foreach($positions as $p){
                        array_push($positionIds, $p->id);
                    }

                    $okrsLv4 = Okr::find()->where(['in', 'position_structure_id', $positionIds])->all();
                    $okrLv3Ids = [];
                    foreach($okrsLv4 as $okrLv4){
                        array_push($showOkrIds, $okrLv4->id);

                        if($okrLv4->parent->type == 'KR'){
                            array_push($okrLv3Ids, $okrLv4->parent->parent_id);
                            array_push($showOkrIds, $okrLv4->parent->parent_id);
                            array_push($showOkrIds, $okrLv4->parent_id);
                        } else {
                            array_push($okrLv3Ids, $okrLv4->parent_id);
                            array_push($showOkrIds, $okrLv4->parent_id);
                        }
                    }

                    $okrsLv3 = Okr::find()->where(['in', 'id', $okrLv3Ids])->all();
                    $okrLv2Ids = [];
                    foreach($okrsLv3 as $okrLv3){
                        if($okrLv3->parent->type == 'KR'){
                            array_push($okrLv2Ids, $okrLv3->parent->parent_id);
                            array_push($showOkrIds, $okrLv3->parent->parent_id);
                            array_push($showOkrIds, $okrLv3->parent_id);
                        } else {
                            array_push($okrLv2Ids, $okrLv3->parent_id);
                            array_push($showOkrIds, $okrLv3->parent_id);
                        }
                    }

                    $okrsLv1 = Okr::find()->where(['in', 'id', $okrLv2Ids])->all();
                    foreach($okrsLv1 as $okrLv1){
                        if($okrLv1->parent->type == 'KR'){
                            array_push($showOkrIds, $okrLv1->parent->parent_id);
                            array_push($showOkrIds, $okrLv1->parent_id);
                        } else {
                            array_push($showOkrIds, $okrLv1->parent_id);
                        }
                    }
                }
                
                //DIVISI
            } else if($unitLevel == 4){

                //INDIVIDU
            } else {

            }
        }

        if($showOkrIds){
            // echo json_encode($showOkrIds);
            $modelOkr->andWhere(['in', 'id', $showOkrIds]);
        }
        
        $okrsLv1 = $modelOkr->all();

        if($model = Okr::findOne($id)){

            $dataProviderCurrentKr = new ActiveDataProvider([
                'query' => Okr::find()->where(['parent_id' => $id])->andWhere(['type' => 'KR']),
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['okr_code' => SORT_ASC]]
            ]);
    
            if($model->cascading_type_id == 3){
                $modelParent = Okr::findOne($model->parent_id);
                $dataProviderAboveKr = new ActiveDataProvider([
                    'query' => Okr::find()->where(['parent_id' => $modelParent->parent_id])->andWhere(['type' => 'KR']),
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                    'sort' => ['defaultOrder' => ['okr_code' => SORT_ASC]]
                ]);
    
            } else {
                $modelParent = Okr::findOne($model->parent_id);
                $dataProviderAboveKr = new ActiveDataProvider([
                    'query' => Okr::find()->where(['parent_id' => $modelParent->id])->andWhere(['type' => 'KR']),
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                    'sort' => ['defaultOrder' => ['okr_code' => SORT_ASC]]
                ]);
            }
    
            $queryBelow = Okr::find();
            $idKrBelowFromObj = [];
            if($okrObjectives = Okr::find()->where(['parent_id' => $id])->andWhere(['type' => 'O'])->all()){
                foreach($okrObjectives as $okrObj){
                    $okrKrs = Okr::find()->where(['parent_id' => $okrObj->id])->andWhere(['type' => 'KR'])->all();
                    foreach($okrKrs as $okrKr){
                        array_push($idKrBelowFromObj, $okrKr->id);
                    }
                }
    
            }
            if($okrKrs = Okr::find()->where(['parent_id' => $id])->andWhere(['type' => 'KR'])->all()){
                foreach($okrKrs as $okrKr){
                    $okrOKrs = Okr::find()->where(['parent_id' => $okrKr->id])->andWhere(['type' => 'O'])->all();
                    foreach($okrOKrs as $okrOKr){
                        $krs = Okr::find()->where(['parent_id' => $okrOKr->id])->andWhere(['type' => 'KR'])->all();
                        foreach($krs as $kr){
                            array_push($idKrBelowFromObj, $kr->id);
                        }
                    }
                }
    
            }
            $queryBelow->where(['in', 'id', $idKrBelowFromObj]);
    
            $dataProviderBelowKr = new ActiveDataProvider([
                'query' => $queryBelow,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['okr_code' => SORT_ASC]]
            ]);
        }


        return $this->render('index', [
            'id' => $id,
            'yearFilter' => $yearFilter,
            'unitFilter' => $unitFilter,
            'listUnit' => $listUnit,
            'okrsLv1' => $okrsLv1,
            'model' => $model,
            'dataProviderCurrentKr' => $dataProviderCurrentKr,
            'dataProviderAboveKr' => $dataProviderAboveKr,
            'dataProviderBelowKr' => $dataProviderBelowKr,
            'showOkrIds' => $showOkrIds,
        ]);
    } */

    public function actionIndex(){
        $searchModel = new OkrSearch();
        $dataProvider = $searchModel->searchVwOkr(Yii::$app->request->queryParams);

        if(Yii::$app->user->can('Admin')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listPosition = ArrayHelper::map(PositionStructure::find()->where(['year' => $searchModel->year])->orderBy('position_name')->all(), 'id', function($model){
                return $model->position_name . ' (' . $model->unit->unit_name . ')';
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];
            
            $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->all();
            
            foreach($vwEmployeeManagers as $vwEmployeeManager){
                $directorates = UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                foreach($directorates as $directorate){
                    array_push($dept, $directorate->id);
                    
                    $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                    foreach($divisions as $division){
                        array_push($dept, $division->id);
    
                        $division2s = UnitStructure::find()->where(['superior_unit_id' => $division->id])->orderBy('unit_code')->all();
                        foreach($division2s as $division2){
                            array_push($dept, $division2->id);
                        }
                    }
                }
            }


            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'id', $dept])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listPosition = ArrayHelper::map(PositionStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'unit_structure_id', $dept])->orderBy('position_name')->all(), 'id', function($model){
                return $model->position_name . ' (' . $model->unit->unit_name . ')';
            });
        } else {
            $userId = Yii::$app->user->id;
            $isLeader = false;

            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);

                if($ep->position->level == 6){
                    
                    $units = Yii::$app->db->createCommand("SELECT id FROM management.unit_structure WHERE superior_unit_id = {$ep->position->unit_structure_id}")->queryColumn();
                    foreach($units as $unit){
                        array_push($dept, $unit);
                    }
                    
                    array_push($dept, $units);

                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }

                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }

            /* var_dump($year);
            var_dump($dept); */
            // var_dump(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->createCommand()->getRawSql());
            
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listPosition = ArrayHelper::map(PositionStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'unit_structure_id', $dept])->orderBy('position_name')->all(), 'id', function($model){
                return $model->position_name . ' (' . $model->unit->unit_name . ')';
            });
        }

        $listLevel = ArrayHelper::map(OkrLevel::find()->where('id <> ' . OkrLevel::INDIVIDUAL)->orderBy('id')->all(), 'id', 'okr_level');

        $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['year' => $searchModel->year])->andWhere(['level' => 4])->andWhere(['is_active' => true])->one();
        $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['year' => $searchModel->year])->andWhere(['level' => 6])->andWhere(['is_active' => true])->one();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listPosition' => $listPosition,
            'listUnit' => $listUnit,
            'listLevel' => $listLevel,
            'isManager' => $isManager,
            'isGeneralManager' => $isGeneralManager,
        ]);
    }

    /* public function actionIndexList(){
        $searchModel = new OkrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->can('Admin')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }

        return $this->render('index-list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
        ]);
    } */

    /* public function actionIndexTable(){
        $searchModel = new OkrSearch();
        $dataProvider = $searchModel->search2(Yii::$app->request->queryParams);

        if(Yii::$app->user->can('Admin')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }

        return $this->render('index-table2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
        ]);
    } */



    /**
     * Displays a single Okr model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $source = 'okr')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($model->type == 'O'){
            $type = 'Objective';
        } else {
            $type = 'Key Result';
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Lihat $type",
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                        'source' => $source,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]) .
                            ((($model->parent->pic_id == Yii::$app->user->id) || Yii::$app->user->can('Admin')) ?
                            Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']) : '')
                ];
        }else{
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Okr model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($level = null, $parent_id = null, $isInputKr = false)
    {
        $request = Yii::$app->request;
        $model = new Okr();
        
        if($isInputKr){    
            $model->scenario = OKR::SCENARIO_INPUT_KR;
        }

        $model->okr_level_id = $level;
        if($modelParent = Okr::findOne($parent_id)){
            $model->scenario = OKR::SCENARIO_INPUT_KR;
            $model->parent_id = $parent_id;
            $model->okr_level_id = $modelParent->okr_level_id;
            $model->cascading_type_id = $modelParent->cascading_type_id;
            $model->year = $modelParent->year;
            $model->weight = $modelParent->weight;
            $model->position_structure_id = $modelParent->position_structure_id;
            $model->pic_id = $modelParent->pic_id;
        }

        if($request->isAjax){

            if($level == 1){
                $model->type = 'O';
                $views = 'create-org';
                $buttonLabel = 'Create more Objective';
                $title = 'Create Objective of Organization';
            } else {
                $model->type = 'KR';
                $views = 'create';
                $buttonLabel = 'Create more Key Result';
                $title = 'Tambah Key Result';
            }
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> $title,
                    'content'=>$this->renderAjax($views, [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> $title,
                    'content'=>'<span class="text-success">Berhasil menambah OKR</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];

            }else{
                // var_dump($model->errors);die;
                return [
                    'title'=> $title,
                    'content'=>$this->renderAjax($views, [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionImportOkrOrg(){
        $request = Yii::$app->request;
        $model = new Okr();
        $dataProvider = new ActiveDataProvider([
                            'query' => OkrOrg::find()
                        ]);

        
        $searchModel = new OkrOrgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($model->load($request->post())) {

            return $this->redirect(['index']);
        } else {
            return $this->render('import-org', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionCreateFromOrg(){
        $request = Yii::$app->request;
        $year = $request->post('year'); 

        $pks = explode(',', $request->post('pks'));
        foreach ($pks as $pk) {
            $okrOrg = OkrOrg::findOne($pk);
            $check = Okr::find()->where(['okr_org_id' => $pk])->andWhere(['year' => $year])->one();

            if($okrOrg && !$check){
                $parent = Okr::find()->where(['okr_org_id' => $okrOrg->parent_id])->andWhere(['year' => $year])->one();
                if(!$parent){
                    $parent = new Okr();
                    $parent->okr_level_id = OkrLevel::ORGANIZATION;
                    $parent->okr = $okrOrg->parent->okr;
                    $parent->okr_code = $okrOrg->parent->okr_code;
                    $parent->year = $year;
                    $parent->type = 'O';
                    $parent->okr_org_id = $okrOrg->parent_id;
                    $parent->save(false);
                }

                $newKr = new Okr();
                $newKr->okr_level_id = OkrLevel::ORGANIZATION;
                $newKr->okr = $okrOrg->okr;
                $newKr->okr_code = $okrOrg->okr_code;
                $newKr->year = $year;
                $newKr->type = 'KR';
                $newKr->parent_id = $parent->id;
                $newKr->target = $okrOrg->target;
                $newKr->measure = $okrOrg->measure;
                $newKr->okr_org_id = $okrOrg->id;
                $newKr->save(false);
            }

        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['/strategic/okr/index']));
            return [
                'title' => "<b>Info!</b>",
                'content' => '<span class="text-success">OKR Organisasi telah dibuat!</span>',
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionCreateFull($parent_id)
    {
        $request = Yii::$app->request;
        $model = new Okr();
        $modelParent = Okr::findOne($parent_id);
        
        $model->okr_level_id = $modelParent->okr_level_id+1;
        $model->cascading_type_id = 1;
        $model->type = 'O';
        $model->parent_id = $parent_id;
        $model->ref_id = $modelParent->id;
        $model->okr = $modelParent->okr;
        $model->year = $modelParent->year;



        if($request->isAjax){
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah OKR (Full Cascade)',
                    'content'=>$this->renderAjax('_form_full', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                if($model->save()){
                    foreach($model->krs as $mkr){
                        if($okr = Okr::findOne($mkr)){
                            $newKr = new Okr();
                            $newKr->parent_id = $model->id;
                            $newKr->ref_id = $okr->id;
                            $newKr->okr_level_id = $model->okr_level_id; 
                            $newKr->position_structure_id = $model->position_structure_id; 
                            $newKr->okr = $okr->okr; 
                            $newKr->type = $okr->type; 
                            $newKr->cascading_type_id = $model->cascading_type_id; 
                            $newKr->target = $okr->target; 
                            $newKr->measure = $okr->measure; 
                            $newKr->timebound_q1 = $okr->timebound_q1;
                            $newKr->timebound_q2 = $okr->timebound_q2;
                            $newKr->timebound_q3 = $okr->timebound_q3;
                            $newKr->timebound_q4 = $okr->timebound_q4;
                            $newKr->created_at = $okr->created_at; 
                            $newKr->created_by = $okr->created_by; 
                            $newKr->updated_at = $okr->updated_at; 
                            $newKr->updated_by = $okr->updated_by; 
                            $newKr->year = $okr->year; 
                            $newKr->version = $okr->version; 
                            $newKr->status_id = $okr->status_id; 
                            $newKr->controllability_id = $okr->controllability_id; 
                            $newKr->support_type = $okr->support_type; 
                            $newKr->polarization_id = $okr->polarization_id; 
                            $newKr->calculation_id = $okr->calculation_id; 
                            $newKr->validity_id = $okr->validity_id; 
                            $newKr->weight = $okr->weight; 
                            $newKr->target_q1 = $okr->target_q1;  
                            $newKr->target_q2 = $okr->target_q2;  
                            $newKr->target_q3 = $okr->target_q3;  
                            $newKr->target_q4 = $okr->target_q4;  
                            $newKr->save();
                        }
                    }

                    $msg = '<span class="text-success">Berhasil menambah OKR (Full Cascade)</span>';
                } else {
                    $msg = '<span class="text-danger">Gagal menambah OKR (Full Cascade). <br /> Error: ' . json_encode($model->errors) . '</span>';
                }

                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index', 'id' => $model->id]));
                
                return [
                    // 'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Tambah OKR (Full Cascade)',
                    'content'=>$msg,
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> 'Tambah OKR (Full Cascade)',
                    'content'=>$this->renderAjax('_form_full', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionCreatePartial($parent_id)
    {
        $request = Yii::$app->request;
        $model = new Okr();
        $modelParent = Okr::findOne($parent_id);
        
        $model->okr_level_id = $modelParent->okr_level_id+1;
        $model->cascading_type_id = 2;
        $model->type = 'O';
        $model->parent_id = $parent_id;
        $model->ref_id = $modelParent->id;
        $model->okr = $modelParent->okr;
        $model->year = $modelParent->year;

        if($request->isAjax){
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah OKR (Partial Cascade)',
                    'content'=>$this->renderAjax('_form_partial', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                // var_dump($request->post('Okr')['targets_q2'][7]);die;

                if($model->save()){
                    foreach($model->krs as $mkr){
                        if($okr = Okr::findOne($mkr)){
                            $newKr = new Okr();
                            $newKr->parent_id = $model->id;
                            $newKr->ref_id = $okr->id;
                            $newKr->okr_level_id = $model->okr_level_id; 
                            $newKr->position_structure_id = $model->position_structure_id; 
                            $newKr->okr = $okr->okr; 
                            $newKr->type = $okr->type; 
                            $newKr->cascading_type_id = $model->cascading_type_id; 
                            $newKr->target = $request->post('Okr')['kr_targets'][$mkr]; 
                            $newKr->measure = $request->post('Okr')['kr_measures'][$mkr]; 
                            $newKr->timebound_q1 = $request->post('Okr')['timebounds_q1'][$mkr];
                            $newKr->timebound_q2 = $request->post('Okr')['timebounds_q2'][$mkr];
                            $newKr->timebound_q3 = $request->post('Okr')['timebounds_q3'][$mkr];
                            $newKr->timebound_q4 = $request->post('Okr')['timebounds_q4'][$mkr];
                            $newKr->year = $okr->year; 
                            $newKr->version = $okr->version; 
                            $newKr->status_id = $okr->status_id; 
                            $newKr->controllability_id = $okr->controllability_id;
                            $newKr->support_type = $okr->support_type; 
                            $newKr->polarization_id = $okr->polarization_id; 
                            $newKr->calculation_id = $okr->calculation_id; 
                            $newKr->validity_id = $okr->validity_id; 
                            $newKr->weight = $okr->weight; 
                            $newKr->target_q1 = $request->post('Okr')['targets_q1'][$mkr];  
                            $newKr->target_q2 = $request->post('Okr')['targets_q2'][$mkr];
                            $newKr->target_q3 = $request->post('Okr')['targets_q3'][$mkr];
                            $newKr->target_q4 = $request->post('Okr')['targets_q4'][$mkr]; 
                            $newKr->save();
                        }
                    }

                    $msg = '<span class="text-success">Berhasil menambah OKR (Partial Cascade)</span>';
                } else {
                    $msg = '<span class="text-danger">Gagal menambah OKR (Partial Cascade). <br /> Error: ' . json_encode($model->errors) . '</span>';
                }

                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index', 'id' => $model->id]));

                return [
                    // 'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Tambah OKR (Partial Cascade)',
                    'content'=>$msg,
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> 'Tambah OKR (Partial Cascade)',
                    'content'=>$this->renderAjax('_form_partial', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionCreateSupport($parent_id)
    {
        $request = Yii::$app->request;
        $model = new Okr();
        $modelParent = Okr::findOne($parent_id);
        
        if($request->isAjax){
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah OKR (Support Cascade)',
                    'content'=>$this->renderAjax('_form-support', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                /* if(Yii::$app->user->id == 487497){
                    var_dump($model->position_structure_id);
                    die;
                }  */

                foreach($model->position_structure_id as $positionStructureId){
                    $newOkr = new Okr();
                    if($position = PositionStructure::findOne($positionStructureId)){

                        $positionStructrueEmployee = PositionStructureEmployee::findOne(['position_structure_id' => $positionStructureId]);
    
                        if(in_array($position->level, [9, 8])){
                            $newOkr->okr_level_id = OkrLevel::DIRECTORATE;
                            $newOkr->pic_id = $positionStructrueEmployee->employee_id;
                        } else if($position->level == 6){
                            $newOkr->okr_level_id = OkrLevel::DEPARTMENT;
                            $newOkr->pic_id = $positionStructrueEmployee->employee_id;
                        } else if(in_array($position->level, [4, 5])){
                            $newOkr->okr_level_id = OkrLevel::DIVISION;
                            $newOkr->pic_id = $positionStructrueEmployee->employee_id;
                        } else {
                            $newOkr->okr_level_id = OkrLevel::INDIVIDUAL;
                        }
                    } else {
                        $newOkr->okr_level_id = $modelParent->okr_level_id + 1;
                    }
    
                    $newOkr->position_structure_id = $positionStructureId;
                    $newOkr->okr = $modelParent->okr;
                    $newOkr->type = 'O';
                    $newOkr->cascading_type_id = 3;
                    $newOkr->year = $modelParent->year;
                    $newOkr->parent_id = $parent_id;
                    $newOkr->ref_id = $parent_id;
                    if($newOkr->save()){

                    } else {
                        var_dump($model->errors);die;
                    }
                }

                

                // if($model->save()){
                    $msg = '<span class="text-success">Berhasil menambah OKR (Support Cascade)</span>';

                    Yii::$app->response->format = Response::FORMAT_JSON;
                    // Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index', 'id' => $model->id]));
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> 'Tambah OKR (Support Cascade)',
                        'content'=>$msg,
                        'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                /* } else {
                    $msg = '<span class="text-danger">Gagal menyimpan Objective. <br /> Error: ' . json_encode($model->errors) . '</span>';
                    return [
                        // 'forceReload'=>'#crud-datatable-pjax',
                        'title'=> 'Tambah OKR (Support Cascade)',
                        'content'=>$msg,
                        'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                } */


            }else{
                return [
                    'title'=> 'Tambah OKR (Support Cascade)',
                    'content'=>$this->renderAjax('_form-support', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    /**
     * Updates an existing Okr model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $modelKr = $model->okr;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                
                if($model->cascading_type_id == CascadingType::FULL){
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'title'=> 'Ubah OKR',
                        'content'=>'Tipe cascadingnya Full, jadi tidak bisa diubah',
                        'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                    ];
                } else {

                    if($model->type == "O"){
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'title'=> 'Ubah Objective',
                            'content'=>$this->renderAjax('create-org', [
                                'model' => $model,
                            ]),
                            'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                        Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                        ];   
                    } else {
                        return [
                            'title'=> 'Ubah Key Result',
                            'content'=>$this->renderAjax('update', [
                                'model' => $model,
                            ]),
                            'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                        Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                        ];                        
                    }
                }

            }else if($model->load($request->post()) && $model->save()){

                // if($modelKr != $model->okr){
                // var_dump($model->cascading_type_id);
                // var_dump($model->cascading_type_id == CascadingType::FULL);
                // die;
                
                    if($model->cascading_type_id == CascadingType::FULL || (!$model->cascading_type_id && $model->okr_level_id == OkrLevel::ORGANIZATION)){
                        $childs = Okr::find()->where(['ref_id' => $model->id])->all();
                        foreach($childs as $child){
                            $child->okr = $model->okr;
                            $child->target_q1 = $model->target_q1;
                            $child->target_q2 = $model->target_q2;
                            $child->target_q3 = $model->target_q3;
                            $child->target_q4 = $model->target_q4;
                            $child->timebound_q1 = $model->timebound_q1;
                            $child->timebound_q2 = $model->timebound_q2;
                            $child->timebound_q3 = $model->timebound_q3;
                            $child->timebound_q4 = $model->timebound_q4;
                            $child->target = $model->target;
                            $child->measure = $model->measure;
                            $child->save(false);
                        }
                    } else if($model->cascading_type_id == CascadingType::SUPPORT){
                        $childs = Okr::find()->where(['parent_id' => $model->id])->andWhere(['type' => 'O'])->all();
                        foreach($childs as $child){
                            $child->okr = $model->okr;
                            $child->save(false);
                        }
                    }
                // }

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Object Key Result',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                 return [
                    'title'=> 'Ubah OKR',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Okr model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        
        $modelParentId = $model->parent_id;
        if($model->type == 'O'){
            $type = 'Objective';
        } else {
            $type = 'Key Result';
        }
        $model->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            // Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index'/* , 'id' => $modelParentId */]));
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title' => 'Info',
                'content' => '<span class="text-success">Berhasil menghapus ' . $type . '!</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionShowDetail($id){
        
        $model = $this->findModel($id);

        $dataProviderCurrentKr = new ActiveDataProvider([
            'query' => Okr::find()->where(['parent_id' => $id])->andWhere(['type' => 'KR']),
            'pagination' => [
                'pageSize' => 20,
            ],
            // 'sort' => ['defaultOrder' => ['okr_code' => SORT_ASC]]
        ]);

        if($model->cascading_type_id == 3){
            $modelParent = Okr::findOne($model->parent_id);
            $dataProviderAboveKr = new ActiveDataProvider([
                'query' => Okr::find()->where(['parent_id' => $modelParent->parent_id])->andWhere(['type' => 'KR']),
                'pagination' => [
                    'pageSize' => 20,
                ],
                // 'sort' => ['defaultOrder' => ['okr_code' => SORT_ASC]]
            ]);

        } else {
            $modelParent = Okr::findOne($model->parent_id);
            $dataProviderAboveKr = new ActiveDataProvider([
                'query' => Okr::find()->where(['parent_id' => $modelParent->id])->andWhere(['type' => 'KR']),
                'pagination' => [
                    'pageSize' => 20,
                ],
                // 'sort' => ['defaultOrder' => ['okr_code' => SORT_ASC]]
            ]);
        }

        $queryBelow = Okr::find()->where(['parent_id' => $id])->andWhere(['type' => 'KR']);
        $idKrBelowFromObj = [];
        if($okrObjectives = Okr::find()->where(['parent_id' => $id])->andWhere(['type' => 'O'])->all()){
            foreach($okrObjectives as $okrObj){
                $okrKrs = Okr::find()->where(['parent_id' => $okrObj->id])->andWhere(['type' => 'KR'])->all();
                foreach($okrKrs as $okrKr){
                    array_push($idKrBelowFromObj, $okrKr->id);
                }
            }

            if($idKrBelowFromObj) $queryBelow->orWhere(['in', 'id', $idKrBelowFromObj]);
        }

        $dataProviderBelowKr = new ActiveDataProvider([
            'query' => $queryBelow,
            'pagination' => [
                'pageSize' => 20,
            ],
            // 'sort' => ['defaultOrder' => ['okr_code' => SORT_ASC]]
        ]);

		$this->layout = '@app/views/layouts/blank';
        return $this->render('detail', [
            'model' => $model,
            'dataProviderCurrentKr' => $dataProviderCurrentKr,
            'dataProviderAboveKr' => $dataProviderAboveKr,
            'dataProviderBelowKr' => $dataProviderBelowKr,
        ]);
    }

    /** ORG */
    public function actionOrganization(){
        $searchModel = new OkrOrgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-organization', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /** DOCUMENTATIONS */
    public function actionMovs($id)
    {
        $request = Yii::$app->request;
        $dataProvider = new ActiveDataProvider([
            'query' => OkrMovType::find()->where(['okr_id' => $id])
        ]);


        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=>'Alat Verifikasi',
                    'content'=>$this->renderAjax('view-documents', [
                        'dataProvider' => $dataProvider,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Alat Verifikasi', ['create-documentations','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->redirect('index');
        }
    }

    public function actionCreateDocumentations($id)
    {
        $request = Yii::$app->request;
        $model = new OkrMovType();
        $model->okr_id = $id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>'<span class="text-success">Berhasil menambahkan Alat Verifikasi</span>',
                    'footer'=> Html::a('Kembali',['movs', 'id' => $id],['class'=>'btn btn-default pull-left','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tambah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionUpdateDocumentations($id)
    {
        $request = Yii::$app->request;
        $model = OkrMovType::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>'<span class="text-success">Berhasil mengubah Alat Verifikasi</span>',
                    'footer'=> Html::a('Kembali',['movs', 'id' => $model->okr_id],['class'=>'btn btn-default pull-left','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Ubah Alat Verifikasi",
                    'content'=>$this->renderAjax('_form_documentations', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionDeleteDocumentations($id)
    {
        $request = Yii::$app->request;
       OkrMovType::findOne($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    public function actionCreateOrganizationObjective()
    {
        $request = Yii::$app->request;
        $model = new OkrOrg();
        $model->type = 'O';

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah Objective Organisasi',
                    'content'=>$this->renderAjax('organization-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Tambah Objective Organisasi',
                    'content'=>'<span class="text-success">Berhasil menambah Objective Organisasi</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> 'Tambah Objective Organisasi',
                    'content'=>$this->renderAjax('organization-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['organization']);
        }

    }

    public function actionUpdateOrganizationObjective($id)
    {
        $request = Yii::$app->request;
        $model = OkrOrg::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Objective Organisasi',
                    'content'=>$this->renderAjax('organization-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save(false)){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Ubah Objective Organisasi',
                    'content'=>'<span class="text-success">Berhasil menambah Objective Organisasi</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> 'Ubah Objective Organisasi',
                    'content'=>$this->renderAjax('organization-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['organization']);
        }

    }

    public function actionCreateOrganizationKeyResult($parent_id = null)
    {
        $request = Yii::$app->request;
        $model = new OkrOrg();
        $modelParent = OkrOrg::findOne($parent_id);
        $model->type = 'KR';
        $model->parent_id = $parent_id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah Key Result Organisasi',
                    'content'=>$this->renderAjax('organization-key-result-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) ){

                $modelParent = OkrOrg::findOne($model->parent_id);
                $model->start_year = $modelParent->start_year;
                $model->end_year = $modelParent->end_year;
                $model->save();

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Tambah Key Result Organisasi',
                    'content'=>'<span class="text-success">Berhasil menambah Key Result Organisasi</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> 'Tambah Key Result Organisasi',
                    'content'=>$this->renderAjax('organization-key-result-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['organization']);
        }

    }

    public function actionUpdateOrganizationKeyResult($id = null)
    {
        $request = Yii::$app->request;
        $model = OkrOrg::findOne($id);
        $modelParent = OkrOrg::findOne($model->parent_id);
        $model->type = 'KR';

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Key Result Organisasi',
                    'content'=>$this->renderAjax('organization-key-result-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                $modelParent = OkrOrg::findOne($model->parent_id);
                $model->start_year = $modelParent->start_year;
                $model->end_year = $modelParent->end_year;

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Ubah Key Result Organisasi',
                    'content'=>'<span class="text-success">Berhasil menambah Key Result Organisasi</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> 'Ubah Key Result Organisasi',
                    'content'=>$this->renderAjax('organization-key-result-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['organization']);
        }

    }

    /** INDIVIDU */
    public function actionIndividual(){
        $searchModel = new OkrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->can('Admin')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];

            $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->all();

            foreach($vwEmployeeManagers as $vwEmployeeManager){
                $directorates = UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                foreach($directorates as $directorate){
                    array_push($dept, $directorate->id);
                    
                    $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                    foreach($divisions as $division){
                        array_push($dept, $division->id);
                    }
                }
            }

            
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'id', $dept])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userId = Yii::$app->user->id;
            $isLeader = false;

            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }

                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }

        return $this->render('index-individual', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
        ]);
    }

    public function actionCreateIndividualOkrExcel(){
        $request = Yii::$app->request;
        $model = new Okr();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah OKR Individual dari Excel',
                    'content'=>$this->renderAjax('individual-excel', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $datetime = date('Ymdhis');
                $year = $model->year;
                $type = 'KR';
                $pic = $model->pic_id;
                $position = $model->position_structure_id;
                
                $model->files = UploadedFile::getInstances($model, 'files');
                $excelLocation = FilesHelper::upload($model->files, $datetime, 'individual_okr', null, null, null);
                // $excelLocation = '/uploaded/individual_okr/20220322095027 - sample_ardi.xlsx';
                if($excelLocation) $msg = $this->readExcelFile($excelLocation, $type, $pic, $position, $year);

                if(!$msg) $msg = '<span class="text-success">Berhasil menambah Key Result Individual</span>';

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Tambah OKR Individual dari Excel',
                    'content'=>$msg,
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }
        }else{
            return $this->redirect(['organization']);
        }
    }

    public function actionUpdateIndividualObjective($id)
    {
        $request = Yii::$app->request;
        $model = Okr::findOne($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Objective Individu',
                    'content'=>$this->renderAjax('individual-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save(false)){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Ubah Objective Individu',
                    'content'=>'<span class="text-success">Berhasil mengubah Objective Individu</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> 'Ubah Objective Individu',
                    'content'=>$this->renderAjax('organization-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['individual']);
        }

    }

    public function actionCreateIndividualKeyResult($parent_id = null)
    {
        $request = Yii::$app->request;
        $model = new Okr();
        $modelParent = Okr::findOne($parent_id);
        $model->type = 'KR';
        $model->parent_id = $parent_id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah Key Result Individu',
                    'content'=>$this->renderAjax('individual-key-result-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $modelParent = Okr::findOne($model->parent_id);
                $model->parent_id = $modelParent->id;
                $model->okr_level_id = $modelParent->okr_level_id;
                $model->position_structure_id = $modelParent->position_structure_id;
                $model->pic_id = $modelParent->pic_id;
                $model->cascading_type_id = CascadingType::SUPPORT;
                $model->year = date('Y');
                $model->save();

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Tambah Key Result Individu',
                    'content'=>'<span class="text-success">Berhasil menambah Key Result Individu</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> 'Tambah Key Result Individu',
                    'content'=>$this->renderAjax('individual-key-result-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['individual']);
        }

    }

    public function actionCreateIndividualKeyResult2($parent_id = null)
    {
        $request = Yii::$app->request;
        $model = new Okr();
        $modelParent = Okr::findOne($parent_id);
        $model->type = 'KR';
        $model->parent_id = $parent_id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah Key Result Individu',
                    'content'=>$this->renderAjax('individual-key-result-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $modelParent = Okr::findOne($model->parent_id);
                $model->parent_id = $modelParent->id;
                $model->okr_level_id = $modelParent->okr_level_id;
                $model->position_structure_id = $modelParent->position_structure_id;
                $model->pic_id = $modelParent->pic_id;
                $model->cascading_type_id = CascadingType::SUPPORT;
                $model->year = date('Y');
                if($initiatives = Initiatives::findOne($model->initiatives_id)){
                    $model->reference_no = $initiatives->initiatives_code;
                }
                $model->save();

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Tambah Key Result Individu',
                    'content'=>'<span class="text-success">Berhasil menambah Key Result Individu</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> 'Tambah Key Result Individu',
                    'content'=>$this->renderAjax('individual-key-result-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['individual']);
        }

    }

    public function actionUpdateIndividualKeyResult($id)
    {
        $request = Yii::$app->request;
        $model = Okr::findOne($id);
        $modelParent = Okr::findOne($model->parent_id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Key Result Individu',
                    'content'=>$this->renderAjax('individual-key-result-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Ubah Key Result Individu',
                    'content'=>'<span class="text-success">Berhasil menambah Key Result Individu</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> 'Ubah Key Result Individu',
                    'content'=>$this->renderAjax('individual-key-result-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['organization']);
        }

    }
    
    public function actionDeleteIndividualKeyResult(){
        $request = Yii::$app->request;
        $model = new Okr();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Hapus OKR Individual',
                    'content'=>$this->renderAjax('individual-delete', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Hapus',['class'=>'btn btn-danger','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $year = $model->year;
                $pic = $model->pic_id;
                $position = $model->position_structure_id;
                
                $okrs = Okr::find()
                                    ->where(['year' => $year])
                                    ->andWhere(['position_structure_id' => $position])
                                    ->andWhere(['pic_id' => $pic])
                                    ->andWhere(['okr_level_id' => OkrLevel::INDIVIDUAL])
                                    ->all();
                foreach($okrs as $okr){
                    $okr->delete();
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Hapus OKR Individual',
                    'content'=>'<span class="text-success">Berhasil menghapus Key Result Individual</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }
        }else{
            return $this->redirect(['individual']);
        }
    }

    
    public function actionDeleteOneKeyResult($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($model->okr_level_id == OkrLevel::INDIVIDUAL){
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['individual']);
        }


    }

    public function actionGetObjectives() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $year = $_POST['depdrop_parents'];

            if(Yii::$app->user->can('Admin')){
                $out = [];
                $objs = Okr::find()->alias('o')->joinWith('position p')->where(['o.year' => $year])->andWhere(['type' => 'O'])->all();
                foreach($objs as $obj){
                    array_push($out, ['id' => $obj->id, 'name' => $obj->okr_code . ' - ' . $obj->okr]);
                }
            } else {
                $userId = Yii::$app->user->id;
                $dept = [];
                $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);
    
                    if($ep->position->level == 6){
                        $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                        foreach($gms as $gm){
                            array_push($dept, $gm->unit_structure_id);
                        }
                    }
                }
    
                $out = [];
                $objs = Okr::find()->alias('o')->joinWith('position p')->where(['o.year' => $year])->andWhere(['type' => 'O'])->andWhere(['in', 'p.unit_structure_id', $dept])->all();
                foreach($objs as $obj){
                    array_push($out, ['id' => $obj->id, 'name' => $obj->okr_code . ' - ' . $obj->okr]);
                }
            }
        }
        return ['output'=>$out, 'selected'=>''];
    }

    public function actionGetKeyResults() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $year = $_POST['depdrop_parents'];

            $userId = Yii::$app->user->id;
            $dept = [];

            if(Yii::$app->user->can('BOD')){
                $directorates = UnitStructure::find()->where(['year' => $year])->andWhere(['level' => 1])->orderBy('unit_code')->all();
                foreach($directorates as $directorate){
                    array_push($dept, $directorate->id);
                }
            } elseif(Yii::$app->user->can('Admin Direktorat')){
                $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $year])->all();

                foreach($vwEmployeeManagers as $vwEmployeeManager){
                    array_push($dept, $vwEmployeeManager->unit_id);
                    
                    $directorates = UnitStructure::find()->where(['year' => $year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                    foreach($directorates as $directorate){
                        array_push($dept, $directorate->id);
                        
                        $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                        foreach($divisions as $division){
                            array_push($dept, $division->id);
                        }
                    }
                }

                // var_dump($dept);die;
                
            } else {
                $employeePositions = PositionStructureEmployee::find()->alias('pse')->joinWith('position p')->where(['pse.employee_id' => $userId])->andWhere(['p.year' => $year])->all();
                foreach($employeePositions as $ep){
                    array_push($dept, $ep->position->unit_structure_id);
    
                    if($ep->position->level == 6){
                        $deptUnits = PositionStructure::find()->where(['unit_structure_id' => $ep->position->unit_structure_id])->all();
                        foreach($deptUnits  as $deptUnit){
                            $gms = PositionStructure::find()->where(['responsible_to_id' => $deptUnit->id])->all();
                            foreach($gms as $gm){
                                array_push($dept, $gm->unit_structure_id);
                            }
                        }
                    }
                }
            }

            $out = [];
            $objs = Okr::find()
                            ->alias('o')
                            ->joinWith('position p')
                            ->where(['o.year' => $year])
                            ->andWhere(['type' => 'KR'])
                            ->andWhere(['in', 'p.unit_structure_id', $dept])
                            ->andWhere("o.okr_level_id <> 5")
                            ->orderBy('o.okr_code')
                            ->all();
            foreach($objs as $obj){
                array_push($out, ['id' => $obj->id, 'name' => $obj->position->unit->unit_name . ' - ' . $obj->okr_code . ' - ' . $obj->okr]);
            }
        }
        return ['output'=>$out, 'selected'=>''];
    }

    public function actionGetTarget($id){

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $keyResult = Okr::findOne($id);

        if($keyResult){
            return [
                'items' => [
                    'target_q1' => $keyResult->target_q1 ? $keyResult->target_q1 . ' ' . $keyResult->measure : '-',
                    'timebound_q1' => $keyResult->timebound_q1 ?: '-',
                    'target_q2' => $keyResult->target_q2 ? $keyResult->target_q2 . ' ' . $keyResult->measure : '-',
                    'timebound_q2' => $keyResult->timebound_q2 ?: '-',
                    'target_q3' => $keyResult->target_q3 ? $keyResult->target_q3 . ' ' . $keyResult->measure : '-',
                    'timebound_q3' => $keyResult->timebound_q3 ?: '-',
                    'target_q4' => $keyResult->target_q4 ? $keyResult->target_q4 . ' ' . $keyResult->measure : '-',
                    'timebound_q4' => $keyResult->timebound_q4 ?: '-',
                ]
            ];
        } else {
            return [
                'items' => [
                    'target_q1' => '-',
                    'timebound_q1' => '-',
                    'target_q2' => '-',
                    'timebound_q2' => '-',
                    'target_q3' => '-',
                    'timebound_q3' => '-',
                    'target_q4' => '-',
                    'timebound_q4' => '-',
                ]
            ];
        }
    }

    protected function readExcelFile($path, $type, $pic, $position, $year){
        
        $location = Yii::getAlias('@app/web' . $path);

        $inputFileType = IOFactory::identify($location);
        $objReader = IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($location);
        $sheetData = $objPHPExcel->getSheet(0)->toArray(null, true, true, true);
        $baseRow = 17;

        $prevObjectiveCode = '';
        $prevParentId = '';

        $msg = '';

        while (!empty($sheetData[$baseRow]['D'])) {

            $objectiveCode = (string) $sheetData[$baseRow]['A'];
            $objectiveDescription = (string) $sheetData[$baseRow]['B'];
            $keyResultCode = (string) $sheetData[$baseRow]['C'];
            $keyResultDescription = (string) $sheetData[$baseRow]['D'];
            $weight = (string) $sheetData[$baseRow]['E'];
            $value = (string) $sheetData[$baseRow]['F'];
            $measure = (string) $sheetData[$baseRow]['G'];
            $value_q1 = (float) $sheetData[$baseRow]['H'];
            $time_q1 = (string) $sheetData[$baseRow]['I'];
            $value_q2 = (float) $sheetData[$baseRow]['J'];
            $time_q2 = (string) $sheetData[$baseRow]['K'];
            $value_q3 = (float) $sheetData[$baseRow]['L'];
            $time_q3 = (string) $sheetData[$baseRow]['M'];
            $value_q4 = (float) $sheetData[$baseRow]['N'];
            $time_q4 = (string) $sheetData[$baseRow]['O'];

            $objectiveCode = preg_replace('/\s+/', '', $objectiveCode);
            $keyResultCode = preg_replace('/\s+/', '', $keyResultCode);
            
            if($objectiveCode && $keyResultCode){
                $cascadingType = 1;
            } else {
                $cascadingType = 3;
            }

            if($parentId = $this->getParentKrId(($objectiveCode ?: $prevObjectiveCode), $cascadingType, $pic, $position, $year, $prevObjectiveCode, $prevParentId)){
                $okr = new Okr();
                $okr->year = $year;
                $okr->okr_level_id = OkrLevel::INDIVIDUAL;
                $okr->type = $type;
                $okr->okr = $keyResultDescription;
                
                $okr->parent_id = $parentId;
                $okr->ref_id = ($cascadingType == CascadingType::SUPPORT) ? $parentId : $this->getRefId($keyResultCode);
                $okr->support_type = ($cascadingType == CascadingType::SUPPORT) ? Okr::findOne($okr->ref_id)->support_type : Okr::findOne($okr->ref_id)->parent->support_type;
                    
                $okr->pic_id = $pic;
                $okr->position_structure_id = $position;
                $okr->cascading_type_id = $cascadingType;
                $okr->target = $value;
                $okr->measure = $measure;
                $okr->target_q1 = $value_q1;
                $okr->timebound_q1 = $value_q1 ?date("Y-m-t", strtotime( $year . '-' . '03-01')) : null;
                $okr->target_q2 = $value_q2;
                $okr->timebound_q2 = $value_q2 ?date("Y-m-t", strtotime( $year . '-' . '06-01')) : null;
                $okr->target_q3 = $value_q3;
                $okr->timebound_q3 = $value_q3 ?date("Y-m-t", strtotime( $year . '-' . '09-01')) : null;
                $okr->target_q4 = $value_q4;
                $okr->timebound_q4 = $value_q4 ?date("Y-m-t", strtotime( $year . '-' . '12-01')) : null;
                if(!$okr->save()){

                    // var_dump($value_q1);
                    // var_dump($okr->errors);die;
                    $msg = json_encode($okr->errors);
                }
    
                $prevObjectiveCode = $objectiveCode ?: $prevObjectiveCode;
                $prevParentId = $parentId ?: $prevParentId;
            } else {
                // var_dump($objectiveCode);
            }

            $baseRow++;
        }

        return $msg;
    }

    protected function getParentKrId($code, $type, $picId, $position, $year, $prevObjectiveCode = null, $prevParentId = null){

        $okrParent = Okr::find()->where(['okr_code' => $code])->andWhere('okr_level_id <> ' . OkrLevel::INDIVIDUAL)->one();

        if($code == $prevObjectiveCode){
            $okrId = $prevParentId;
        } else if($type != CascadingType::FULL){
            if($okrParent){
                $newOkrParent = new Okr();
                $newOkrParent->year = $year;
                $newOkrParent->okr_level_id = OkrLevel::INDIVIDUAL;
                $newOkrParent->type = 'O';
                $newOkrParent->okr = $okrParent->okr;
                $newOkrParent->pic_id = $picId;
                $newOkrParent->position_structure_id = $position;
                $newOkrParent->cascading_type_id = $okrParent->cascading_type_id;
                $newOkrParent->target = $okrParent->target;
                $newOkrParent->measure = $okrParent->measure;
                $newOkrParent->target_q1 = $okrParent->target_q1;
                $newOkrParent->timebound_q1 = $okrParent->timebound_q1;
                $newOkrParent->target_q2 = $okrParent->target_q2;
                $newOkrParent->timebound_q2 = $okrParent->timebound_q2;
                $newOkrParent->target_q3 = $okrParent->target_q3;
                $newOkrParent->timebound_q3 = $okrParent->timebound_q3;
                $newOkrParent->target_q4 = $okrParent->target_q4;
                $newOkrParent->timebound_q4 = $okrParent->timebound_q4;
                $newOkrParent->parent_id = $okrParent->id;
                $newOkrParent->ref_id = $okrParent->id;
                if($newOkrParent->save()){
                    $okrId = $newOkrParent->id;
                } else {
                    // var_dump('a');
                    // var_dump($newOkrParent->errors);die;
                }
            }
        } else {
            if($okrParent){
                $newOkrParentSupport = new Okr();
                $newOkrParentSupport->year = $year;
                $newOkrParentSupport->okr_level_id = OkrLevel::INDIVIDUAL;
                $newOkrParentSupport->type = $okrParent->type;
                $newOkrParentSupport->okr = $okrParent->okr;
                $newOkrParentSupport->pic_id = $picId;
                $newOkrParentSupport->position_structure_id = $position;
                $newOkrParentSupport->cascading_type_id = $okrParent->cascading_type_id;
                $newOkrParentSupport->target = $okrParent->target;
                $newOkrParentSupport->measure = $okrParent->measure;
                $newOkrParentSupport->target_q1 = $okrParent->target_q1;
                $newOkrParentSupport->timebound_q1 = $okrParent->timebound_q1;
                $newOkrParentSupport->target_q2 = $okrParent->target_q2;
                $newOkrParentSupport->timebound_q2 = $okrParent->timebound_q2;
                $newOkrParentSupport->target_q3 = $okrParent->target_q3;
                $newOkrParentSupport->timebound_q3 = $okrParent->timebound_q3;
                $newOkrParentSupport->target_q4 = $okrParent->target_q4;
                $newOkrParentSupport->timebound_q4 = $okrParent->timebound_q4;
                $newOkrParentSupport->parent_id = $okrParent->id;
                $newOkrParentSupport->ref_id = $okrParent->id;
                if($newOkrParentSupport->save()){
                    $okrId = $newOkrParentSupport->id;
                } else {
                    // var_dump('b');
                    // var_dump($newOkrParentSupport->errors);die;
                }
            }
        }


        return $okrId ?: null;
    }

    protected function getRefId($code){
        $okrId = Okr::findOne(['okr_code' => $code])->id;

        return $okrId ?: null;
    }

    protected function getDateFormat($date){
        $tanggal = explode(' ', $date);

        if(in_array($tanggal[1], ['Januari', 'januari'])){
            $month = '01';
        } else if(in_array($tanggal[1], ['Februari', 'februari'])){
            $month = '02';
        } else if(in_array($tanggal[1], ['Maret', 'maret'])){
            $month = '03';
        } else if(in_array($tanggal[1], ['April', 'april'])){
            $month = '04';
        } else if(in_array($tanggal[1], ['Mei', 'mei'])){
            $month = '05';
        } else if(in_array($tanggal[1], ['Juni', 'juni'])){
            $month = '06';
        } else if(in_array($tanggal[1], ['Juli', 'juli'])){
            $month = '07';
        } else if(in_array($tanggal[1], ['Agustus', 'agustus'])){
            $month = '08';
        } else if(in_array($tanggal[1], ['September', 'september'])){
            $month = '09';
        } else if(in_array($tanggal[1], ['Oktober', 'oktober'])){
            $month = '10';
        } else if(in_array($tanggal[1], ['November', 'november'])){
            $month = '11';
        } else if(in_array($tanggal[1], ['Desember', 'desember'])){
            $month = '12';
        }

        return $tanggal[2] . '-' . $month . '-' . $tanggal[0];
    }

    /**
     * Finds the Okr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Okr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Okr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
