<?php

namespace app\modules\strategic\controllers;

use Yii;
use app\models\Management;
use app\models\Mission;
use app\models\search\ManagementSearch;
use app\models\Vision;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ManagementController implements the CRUD actions for Management model.
 */
class ManagementController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'delete-mission' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Management models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ManagementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Management model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        
        $modelVision = Vision::findOne(['management_id' => $id]);

        $dataProviderVision = new ActiveDataProvider([
            'query' => Vision::find()->where(['management_id' => $id]),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        $dataProviderMission = new ActiveDataProvider([
            'query' => Mission::find()->where(['vision_id' => $modelVision->id]),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelVision' => $modelVision,
            'dataProviderVision' => $dataProviderVision,
            'dataProviderMission' => $dataProviderMission
        ]);
    }

    /**
     * Creates a new Management model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Management();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Buat Manajemen',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Buat Manajemen',
                    'content'=>'<span class="text-success">Berhasil membuat Manajemen</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])

                ];
            }else{
                return [
                    'title'=> 'Buat Manajemen',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    /**
     * Updates an existing Management model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Manajemen',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['view', 'id' => $model->id]));
                return [
                    'title'=> 'Ubah Manajemen',
                    'content'=>'<span class="text-success">Berhasil mengubah Manajemen</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                 return [
                    'title'=> 'Ubah Manajemen',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete an existing Management model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionAddVision($id)
    {
        $request = Yii::$app->request;
        
        $model = new Vision();
        $model->management_id = $id;
        $visionText = Vision::findOne(['management_id' => $id])->vision;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah Visi',
                    'content'=>$this->renderAjax('_form_vision', [
                        'model' => $model,
                        'visionText' => $visionText,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post())){

                if($model->save()){
                    $msg = '<span class="text-success">Berhasil menambah Visi</span>';
                } else {
                    $msg = '<span class="text-danger">Gagal menambah Visi (' . json_encode($model->errors) . ')</span>';
                }

                return [
                    'forceReload' => '#vision-pjax',
                    'title'=> 'Tambah Visi',
                    'content'=>$msg,
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }
        }else{
            return $this->redirect(['view', 'id' => $id]);
        }
    }

    public function actionUpdateVision($id)
    {
        $request = Yii::$app->request;
     
        $model = Vision::findOne($id);
        $managementId = $model->management_id;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Visi',
                    'content'=>$this->renderAjax('_form_vision', [
                        'model' => $model,
                        'visionText' => $model->vision,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post())){

                if($model->save()){
                    $msg = '<span class="text-success">Berhasil mengubah Visi</span>';
                } else {
                    $msg = '<span class="text-danger">Gagal mengubah Visi (' . json_encode($model->errors) . ')</span>';
                }

                return [
                    'forceReload' => '#vision-pjax',
                    'title'=> 'Ubah Visi',
                    'content'=>$msg,
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }
        }else{
            return $this->redirect(['view', 'id' => $managementId]);
        }
    }
    
    public function actionDeleteVision($id)
    {
        $request = Yii::$app->request;
        $model = Vision::findOne($id);
        $managementId = $model->management_id;
        $model->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#vision-pjax'];
        }else{
            return $this->redirect(['view', 'id' => $managementId]);
        }
    }

    public function actionAddMission($id)
    {
        $request = Yii::$app->request;
     
        $modelVision = Vision::findOne($id);
        $model = new Mission();
        $model->vision_id = $id;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah Misi',
                    'content'=>$this->renderAjax('_form_mission', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload' => '#mission-pjax',
                    'title'=> 'Tambah Misi',
                    'content'=>'<span class="text-success">Berhasil menambah Misi</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                 return [
                    'title'=> 'Tambah Misi',
                    'content'=>$this->renderAjax('_form_mission', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['view', 'id' => $modelVision->management_id]);
        }
    }

    public function actionUpdateMission($id)
    {
        $request = Yii::$app->request;
     
        $model = Mission::findOne($id);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Misi',
                    'content'=>$this->renderAjax('_form_mission', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload' => '#mission-pjax',
                    'title'=> 'Ubah Misi',
                    'content'=>'<span class="text-success">Berhasil mengubah Misi</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                 return [
                    'title'=> 'Ubah Misi',
                    'content'=>$this->renderAjax('_form_mission', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['view', 'id' => $model->vision->management_id]);
        }
    }

    public function actionDeleteMission($id)
    {
        $request = Yii::$app->request;
        $model = Mission::findOne($id);
        $model->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#mission-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Management model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Management the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Management::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
