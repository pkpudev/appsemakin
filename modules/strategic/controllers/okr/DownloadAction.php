<?php

namespace app\modules\strategic\controllers\okr;

use app\models\Employee;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\search\OkrSearch;
use Yii;
use app\models\UnitStructure;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class DownloadAction extends \yii\base\Action
{
	public function run($type = null)
	{
		$searchModel = new OkrSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->post(), 'KR', $type);
		$query = $dataProvider->query;
		if (!$searchModel->status_id) {
			// $query->andFilterWhere(['i.status_id' => 4]); // Approved
		}
		$models = $query->all();
		
		$year = $searchModel->year;
		$logo = 'pkpu.png';
		$lembaga = 'Human Initiative';

		$unit = UnitStructure::findOne($searchModel->unit_id);
		$directorate = $unit->unit_name ?: '';
		$dir = strtoupper($directorate);
		$dir = ((strpos($dir, 'DIREKTUR') !== false) || (strpos($dir, 'DIREKTORAT') !== FALSE)) ? $dir : "DIREKTORAT {$dir}";

		$deptName = @$unit->unit_name;
		$deptCode = @$unit->unit_code;
		$deptnameUppercase = strtoupper($deptName);

		// Load excel from template
		$spreadsheet = IOFactory::load(Yii::getAlias("@app/templates/excel/okr.xlsx"));
		$spreadsheet->setActiveSheetIndexByName('OKR');

		// logo cell
		$drawing = new Drawing();
		$drawing->setName('Logo');
		$drawing->setDescription('Logo');
		$drawing->setPath(Yii::getAlias("images/{$logo}"));
		$drawing->setCoordinates('B2');
		$drawing->setHeight(90);
		$drawing->setOffsetY(8); 
		$drawing->setOffsetX(150); 
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// instansi cell
		$spreadsheet->getActiveSheet()->setCellValue('instansi', strtoupper($lembaga));

		// divisi cell
		// $spreadsheet->getActiveSheet()->setCellValue('divisi', $dir);
		$spreadsheet->getActiveSheet()->setCellValue('divisi', $deptCode . ' ' . $deptnameUppercase);

		// tahun cell
		$spreadsheet->getActiveSheet()->setCellValue('tahun', 'PERIODE ' . $year);

		// label_total_divisi cell
		// $spreadsheet->getActiveSheet()->setCellValue('label_total_divisi', "TOTAL ({$deptnameUppercase})");

		$spreadsheet->getActiveSheet()->setCellValue('D8', @$unit->unit_code ?: '');
		$spreadsheet->getActiveSheet()->setCellValue('D9', @$unit->unit_name ?: '');
		$spreadsheet->getActiveSheet()->setCellValue('D10', $unit ? (@$unit->getDepartmentCode() ?: ''): '');
		$spreadsheet->getActiveSheet()->setCellValue('D11', $unit ? (@$unit->getDepartmentName() ?: ''): '');
		$spreadsheet->getActiveSheet()->setCellValue('D12', $unit ? (@$unit->getDirectorateCode() ?: ''): '');
		$spreadsheet->getActiveSheet()->setCellValue('D13', $unit ? (@$unit->getDirectorateName() ?: ''): '');

		if($position = PositionStructure::find()->where(['unit_structure_id' => $unit->id])->andWhere('level is not null')->orderBy('level desc')->one()){
			// $positionName = $position->position_name;
			// $picPosition = PositionStructureEmployee::findOne(['position_structure_id' => $position->id])->employee->full_name;
			// $supPositionName = $position->resp->position_name;
			// $supPicPosition = PositionStructureEmployee::findOne(['position_structure_id' => $position->responsible_to_id])->employee->full_name;

			if($positionDir = PositionStructure::find()->where(['unit_structure_id' => $position->getDirectorateId()])->andWhere('level is not null')->orderBy('level desc')->one()){
				if($unit->level == 2){
					if($unit->year == 2022){
						$dirPositionName =  PositionStructureEmployee::findOne(['position_structure_id' => 7])->position->position_name;
						$dirPicPosition = PositionStructureEmployee::findOne(['position_structure_id' => 7])->employee->full_name;
					} else {
						$dirPositionName =  PositionStructureEmployee::findOne(['position_structure_id' => 2427])->position->position_name;
						$dirPicPosition = PositionStructureEmployee::findOne(['position_structure_id' => 2427])->employee->full_name;
					}
				} else {
					$dirPositionName = $positionDir->position_name;
					$dirPicPosition = PositionStructureEmployee::findOne(['position_structure_id' => $positionDir->id])->employee->full_name;
				}
			}
		}

		if(!$searchModel->pic_id){
			$position = PositionStructure::find()->where(['unit_structure_id' => $unit->id])->andWhere('level is not null')->orderBy('level desc')->one();
			$searchModel->pic_id = PositionStructureEmployee::findOne(['position_structure_id' => $position->id])->employee_id;
		}

		$q = Yii::$app->db->createCommand("
											select 
												se.full_name,
												ps.position_name,
												se2.full_name as \"upper\",
												ps2.position_name as \"sup_pos\"
											from
												management.position_structure__employee pse
											left join management.position_structure ps on pse.position_structure_id = ps.id
											left join management.unit_structure us on ps.unit_structure_id = us.id 
											left join sdm_employee se on pse.employee_id = se.id
											left join management.position_structure ps2 on ps.responsible_to_id = ps2.id
											left join management.position_structure__employee pse2 on pse2.position_structure_id = ps2.id 
											left join sdm_employee se2 on pse2.employee_id = se2.id
											where
												ps.unit_structure_id =" . $searchModel->unit_id . " and
												pse.employee_id = " . $searchModel->pic_id . "
										")->queryOne();

		$picPosition = $q['full_name'];
		$positionName = $q['position_name'];
		$supPicPosition = $q['upper'];
		$supPositionName = $q['sup_pos'];

		// var_dump($q);die;

		$spreadsheet->getActiveSheet()->setCellValue('M8', $picPosition ?: '');
		$spreadsheet->getActiveSheet()->setCellValue('M9', $positionName ?: '');
		$spreadsheet->getActiveSheet()->setCellValue('M10', $supPicPosition ?: '');
		$spreadsheet->getActiveSheet()->setCellValue('M11', $supPositionName ?: '');
		$spreadsheet->getActiveSheet()->setCellValue('M12', $dirPicPosition ?: '');
		$spreadsheet->getActiveSheet()->setCellValue('M13', $dirPositionName ?: '');


		$startRow = 19;
		$currentRow = null;
		$totalBudgetRow = 20;
		$totalBudgetValue = 0;
		$totalBobot = 0;
		$oldObjectiveCode = null;
		$oldObjectiveCodeValue = null;
		$oldObjective = null;
		$oldObjectiveValue = null;
		$oldBobot = null;
		$oldBobotValue = null;
		
		foreach ($models as $key => $model) {
			if ($currentRow === null) {
				$currentRow = $startRow;
			} else {
				$currentRow++;
				$spreadsheet->getActiveSheet()->insertNewRowBefore($totalBudgetRow);
				$totalBudgetRow++;
			}

			$newProkerCodeValue = $model->parent->okr_code;
			if ($newProkerCodeValue != $oldObjectiveCodeValue) {
				if ($oldObjectiveCodeValue !== null) {
					$lastProkerCodeCell = "B" . ($currentRow - 1);
					if ($oldObjectiveCode != $lastProkerCodeCell) {
						$spreadsheet->getActiveSheet()->mergeCells("$oldObjectiveCode:$lastProkerCodeCell");
					}
					$spreadsheet->getActiveSheet()->setCellValue($oldObjectiveCode, $oldObjectiveCodeValue ?: '');
				}

				$oldObjectiveCode = 'B' . $currentRow;
				$oldObjectiveCodeValue = $newProkerCodeValue;
			}

			$newProkerValue = $model->parent->okr;
			if ($newProkerValue != $oldObjectiveValue) {
				if ($oldObjectiveValue !== null) {
					$lastProkerCell = "C" . ($currentRow - 1);
					if ($oldObjective != $lastProkerCell) {
						$spreadsheet->getActiveSheet()->mergeCells("$oldObjective:$lastProkerCell");
					}
					$spreadsheet->getActiveSheet()->setCellValue($oldObjective, $oldObjectiveValue ?: '');
				}

				$oldObjective = 'C' . $currentRow;
				$oldObjectiveValue = $newProkerValue;
			}

			// KODE OKR
			$spreadsheet->getActiveSheet()->setCellValue('D' . $currentRow, @$model->okr_code ?: '');

			// OKR
			$spreadsheet->getActiveSheet()->setCellValue('E' . $currentRow, @$model->okr ?: '');

			// BOBOT
			$spreadsheet->getActiveSheet()->setCellValue('F' . $currentRow, @($model->target ?: '') . ' ' . $model->measure ?: '');

			// TARGET Q1
			$spreadsheet->getActiveSheet()->setCellValue('G' . $currentRow, @$model->target_q1 ?: '');

			// DUE DATE 1
			$spreadsheet->getActiveSheet()->setCellValue('H' . $currentRow, @$model->timebound_q1 ?: '');

			// TARGET Q2
			$spreadsheet->getActiveSheet()->setCellValue('I' . $currentRow, @$model->target_q2 ?: '');

			// DUE DATE 2
			$spreadsheet->getActiveSheet()->setCellValue('J' . $currentRow, @$model->timebound_q2 ?: '');

			// TARGET Q3
			$spreadsheet->getActiveSheet()->setCellValue('K' . $currentRow, @$model->target_q3 ?: '');

			// DUE DATE 3
			$spreadsheet->getActiveSheet()->setCellValue('L' . $currentRow, @$model->timebound_q3 ?: '');

			// TARGET Q4
			$spreadsheet->getActiveSheet()->setCellValue('M' . $currentRow, @$model->target_q4);

			//  DUE DATE 4
			$spreadsheet->getActiveSheet()->setCellValue('N' . $currentRow, @$model->timebound_q4 ?: '');

			//  CASCADING
			$spreadsheet->getActiveSheet()->setCellValue('O' . $currentRow, @$model->cascading_type_id ? $model->cascadingType->cascading_type: '');

			// $totalBobot += $grade;
		}

		if ($oldObjectiveCodeValue !== null) {
			// BSC Aspek
			$lastProkerCodeCell = "B$currentRow";
			if ($oldObjectiveCode != $lastProkerCodeCell) {
				$spreadsheet->getActiveSheet()->mergeCells("$oldObjectiveCode:$lastProkerCodeCell");
			}
			$spreadsheet->getActiveSheet()->setCellValue($oldObjectiveCode, $oldObjectiveCodeValue ?: '');
		}

		if ($oldObjectiveValue !== null) {
			// BSC Aspek
			$lastProkerCell = "C$currentRow";
			if ($oldObjective != $lastProkerCell) {
				$spreadsheet->getActiveSheet()->mergeCells("$oldObjective:$lastProkerCell");
			}
			$spreadsheet->getActiveSheet()->setCellValue($oldObjective, $oldObjectiveValue ?: '');
		}

		
		if($searchModel->pic_id){
			$pse = PositionStructureEmployee::find()
																		->alias('pse')
																		->joinWith('position p')
																		->where(['pse.employee_id' => $searchModel->pic_id])
																		->andWhere(['p.unit_structure_id' => $searchModel->unit_id])
																		->one();

			$spreadsheet->getActiveSheet()->setCellValue('C' . ($currentRow+13), $picPosition ?: '');
			$spreadsheet->getActiveSheet()->setCellValue('C' . ($currentRow+14), $positionName ?: '');
			// $spreadsheet->getActiveSheet()->setCellValue('B' . ($currentRow+4), '');
			$spreadsheet->getActiveSheet()->setCellValue('B' . ($currentRow+4), 'Depok, 5 November 2024');
			// $spreadsheet->getActiveSheet()->setCellValue('C' . ($currentRow+13), '');
			// $spreadsheet->getActiveSheet()->setCellValue('C' . ($currentRow+14), '');
			$spreadsheet->getActiveSheet()->setCellValue('G' . ($currentRow+20), '');
			$spreadsheet->getActiveSheet()->setCellValue('L' . ($currentRow+13), $supPicPosition);
			$spreadsheet->getActiveSheet()->setCellValue('L' . ($currentRow+14), $supPositionName);
			
			$spreadsheet->getActiveSheet()->setCellValue('M10', $picPosition ?: '');
			$spreadsheet->getActiveSheet()->setCellValue('M11', $positionName ?: '');
		} else {
			$spreadsheet->getActiveSheet()->setCellValue('C' . ($currentRow+13), $supPicPosition ?: '');
			$spreadsheet->getActiveSheet()->setCellValue('C' . ($currentRow+14), $supPositionName ?: '');
			$spreadsheet->getActiveSheet()->setCellValue('L' . ($currentRow+13), $picPosition ?: '');
			$spreadsheet->getActiveSheet()->setCellValue('L' . ($currentRow+14), $positionName ?: '');
			$spreadsheet->getActiveSheet()->setCellValue('G' . ($currentRow+20), $dirPicPosition ?: '');
			$spreadsheet->getActiveSheet()->setCellValue('F' . ($currentRow+21), $dirPositionName ?: '');
		}


		// total_bobot cell
		// $spreadsheet->getActiveSheet()->setCellValue('total_jumlah_anggaran', $formatter->asDecimal($totalBudgetValue));

		// label_total_divisi cell
		// $spreadsheet->getActiveSheet()->setCellValue('total_bobot', $formatter->asDecimal($totalBobot));

		// Set page orientation and size
		$spreadsheet->getActiveSheet()
			->getPageSetup()
			->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
		$spreadsheet->getActiveSheet()
			->getPageSetup()
			->setPaperSize(PageSetup::PAPERSIZE_A4)
			->setFirstPageNumber(1);

		// Rename worksheet
		// $spreadsheet->getActiveSheet()->setTitle('Printing');

		$filename = trim("OKR $year $deptName") . ".xlsx";

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');

		Yii::$app->end();
	}
}
