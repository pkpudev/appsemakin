<?php

namespace app\modules\strategic\controllers;

use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use Yii;
use app\models\Risk;
use app\models\search\RiskSearch;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * RiskController implements the CRUD actions for Risk model.
 */
class RiskController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Risk models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RiskSearch();
        $dataProvider = $searchModel->searchRisk(Yii::$app->request->queryParams);

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD') || Yii::$app->user->can('Risk Management Officer')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];

            $vwEmployeeManager = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->one();

            $directorates = UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
            foreach($directorates as $directorate){
                array_push($dept, $directorate->id);
                
                $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                foreach($divisions as $division){
                    array_push($dept, $division->id);
                }
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'id', $dept])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $isLeader = false;

            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }

                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }


            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
        ]);
    }


    /**
     * Displays a single Risk model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Risk #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Risk model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Risk();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Risk",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new Risk",
                    'content'=>'<span class="text-success">Create Risk success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Create new Risk",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Risk model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Risk #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Risk #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                 return [
                    'title'=> "Update Risk #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-secondary float-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Risk model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Risk model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Risk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Risk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Risk::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
