<?php

namespace app\modules\strategic\controllers\activity;

use app\models\search\ActivitySearch;
use Yii;
use app\models\UnitStructure;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class DownloadAction extends \yii\base\Action
{
	public function run()
	{
		$searchModel = new ActivitySearch();
		$dataProvider = $searchModel->search(Yii::$app->request->post());
		$query = $dataProvider->query;
		if (!$searchModel->status_id) {
			// $query->andFilterWhere(['i.status_id' => 4]); // Approved
		}
		$models = $query->all();
		
		$year = $searchModel->year;
		$logo = 'pkpu.png';
		$lembaga = 'Human Initiative';

		$unit = UnitStructure::findOne($searchModel->unit_id);
		$directorate = $unit->unit_name ?: '';
		$level = $unit->level ?: '';
		$dir = strtoupper($directorate);

		$deptName = @$unit->unit_name;
		$deptCode = @$unit->unit_code;
		$deptnameUppercase = strtoupper($deptName);

		if($level == 1){
			$dir = "DIREKTORAT {$dir} ({$deptCode})";
		} else if($level == 2){
			$dir = "DEPARTEMEN {$dir} ({$deptCode})";
		} else if($level == 3){
			$dir = "DIVISI {$dir} ({$deptCode})";
		} else if($level == 4){
			$dir = "BIDANG {$dir} ({$deptCode})";
		} else if($level == 5){
			$dir = "SQUAD {$dir} ({$deptCode})";
		}

		// Load excel from template
		$spreadsheet = IOFactory::load(Yii::getAlias("@app/templates/excel/activity.xlsx"));
		$spreadsheet->setActiveSheetIndexByName('KEGIATAN');

		// logo cell
		$drawing = new Drawing();
		$drawing->setName('Logo');
		$drawing->setDescription('Logo');
		$drawing->setPath(Yii::getAlias("images/{$logo}"));
		$drawing->setCoordinates('B2');
		$drawing->setHeight(100	);
		$drawing->setWorksheet($spreadsheet->getActiveSheet());

		// instansi cell
		$spreadsheet->getActiveSheet()->setCellValue('instansi', strtoupper($lembaga));

		// divisi cell
		// $spreadsheet->getActiveSheet()->setCellValue('divisi', $dir);
		$spreadsheet->getActiveSheet()->setCellValue('divisi', $dir);

		// tahun cell
		$spreadsheet->getActiveSheet()->setCellValue('tahun', 'PERIODE ' . $year);

		// label_total_divisi cell
		$spreadsheet->getActiveSheet()->setCellValue('label_total_divisi', "{$deptnameUppercase}");


		$startRow = 12;
		$currentRow = null;
		$totalBudgetRow = 13;
		$totalBudgetValue = 0;
		$totalBobot = 0;
		$oldProkerCode = null;
		$oldProkerCodeValue = null;
		$oldProker = null;
		$oldProkerValue = null;
		$oldBobot = null;
		$oldBobotValue = null;
		
		foreach ($models as $key => $model) {
			if ($currentRow === null) {
				$currentRow = $startRow;
			} else {
				$currentRow++;
				$spreadsheet->getActiveSheet()->insertNewRowBefore($totalBudgetRow);
				$totalBudgetRow++;
			}

			$newProkerCodeValue = $model->initiative->initiatives_code;
			if ($newProkerCodeValue != $oldProkerCodeValue) {
				if ($oldProkerCodeValue !== null) {
					$lastProkerCodeCell = "B" . ($currentRow - 1);
					if ($oldProkerCode != $lastProkerCodeCell) {
						$spreadsheet->getActiveSheet()->mergeCells("$oldProkerCode:$lastProkerCodeCell");
					}
					$spreadsheet->getActiveSheet()->setCellValue($oldProkerCode, $oldProkerCodeValue ?: '');
				}

				$oldProkerCode = 'B' . $currentRow;
				$oldProkerCodeValue = $newProkerCodeValue;
			}

			$newProkerValue = $model->initiative->workplan_name;
			if ($newProkerValue != $oldProkerValue) {
				if ($oldProkerValue !== null) {
					// BSC Aspek
					$lastProkerCell = "C" . ($currentRow - 1);
					if ($oldProker != $lastProkerCell) {
						$spreadsheet->getActiveSheet()->mergeCells("$oldProker:$lastProkerCell");
					}
					$spreadsheet->getActiveSheet()->setCellValue($oldProker, $oldProkerValue ?: '');
				}

				$oldProker = 'C' . $currentRow;
				$oldProkerValue = $newProkerValue;
			}

			$newBobotValue = @$model->initiative->grade;
			if ($newBobotValue != $oldBobotValue) {
				if ($oldBobotValue !== null) {
					// BSC Aspek
					$lastBobotCell = "D" . ($currentRow - 1);
					if ($oldBobot != $lastBobotCell) {
						$spreadsheet->getActiveSheet()->mergeCells("$oldBobot:$lastBobotCell");
					}
					$spreadsheet->getActiveSheet()->setCellValue($oldBobot, $oldBobotValue ?: '');
				}

				$oldBobot = 'D' . $currentRow;
				$oldBobotValue = $newBobotValue;
			}

			// KODE KEGIATAN
			$spreadsheet->getActiveSheet()->setCellValue('E' . $currentRow, @$model->activity_code ?: '');

			// KEGIATAN
			$spreadsheet->getActiveSheet()->setCellValue('F' . $currentRow, @$model->activity_name ?: '');

			// BOBOT
			$spreadsheet->getActiveSheet()->setCellValue('G' . $currentRow, ($grade = $model->weight ?: 0));

			// DITUGASKAN KEPADA
			$spreadsheet->getActiveSheet()->setCellValue('H' . $currentRow, @$model->pic_id ? $model->pic->full_name: '');

			// MULAI
			$spreadsheet->getActiveSheet()->setCellValue('I' . $currentRow, $model->start_plan ? date('d-m-Y',strtotime($model->start_plan)) : '');

			// AKHIR
			$spreadsheet->getActiveSheet()->setCellValue('J' . $currentRow, $model->finish_plan ? date('d-m-Y',strtotime($model->finish_plan)) : '');

			// ALAT VERIFIKASI
			$spreadsheet->getActiveSheet()->setCellValue('K' . $currentRow, $model->documentation ?: '');

			// SUMBER DAYA
			$spreadsheet->getActiveSheet()->setCellValue('L' . $currentRow, $model->resources ?: '');

			// CATATAN
			$spreadsheet->getActiveSheet()->setCellValue('M' . $currentRow, $model->note ?: '');

			$totalBobot += $grade;
		}

		if ($oldProkerCodeValue !== null) {
			// BSC Aspek
			$lastProkerCodeCell = "B$currentRow";
			if ($oldProkerCode != $lastProkerCodeCell) {
				$spreadsheet->getActiveSheet()->mergeCells("$oldProkerCode:$lastProkerCodeCell");
			}
			$spreadsheet->getActiveSheet()->setCellValue($oldProkerCode, $oldProkerCodeValue ?: '');
		}

		if ($oldProkerValue !== null) {
			// BSC Aspek
			$lastProkerCell = "C$currentRow";
			if ($oldProker != $lastProkerCell) {
				$spreadsheet->getActiveSheet()->mergeCells("$oldProker:$lastProkerCell");
			}
			$spreadsheet->getActiveSheet()->setCellValue($oldProker, $oldProkerValue ?: '');
		}

		if ($oldBobotValue !== null) {
			// BSC Aspek
			$lastBobotCell = "D$currentRow";
			if ($oldBobot != $lastBobotCell) {
				$spreadsheet->getActiveSheet()->mergeCells("$oldBobot:$lastBobotCell");
			}
			$spreadsheet->getActiveSheet()->setCellValue($oldBobot, $oldBobotValue ?: '');
		}

		// total_bobot cell
		// $spreadsheet->getActiveSheet()->setCellValue('total_jumlah_anggaran', $formatter->asDecimal($totalBudgetValue));

		// label_total_divisi cell
		// $spreadsheet->getActiveSheet()->setCellValue('total_bobot', $formatter->asDecimal($totalBobot));

		// Set page orientation and size
		$spreadsheet->getActiveSheet()
			->getPageSetup()
			->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
		$spreadsheet->getActiveSheet()
			->getPageSetup()
			->setPaperSize(PageSetup::PAPERSIZE_A4);

		// Rename worksheet
		// $spreadsheet->getActiveSheet()->setTitle('Printing');

		$filename = trim("KEGIATAN $year $deptName") . ".xlsx";

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');

		Yii::$app->end();
	}
}
