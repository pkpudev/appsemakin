<?php

namespace app\modules\strategic\controllers;

use Yii;
use app\modules\strategic\models\Initiatives;
use app\modules\strategic\models\InitiativesSource;
use app\models\Status;
use app\models\Budget;
use app\models\Bsc;
use app\models\BudgetHistory;
use app\models\Activity;
use app\models\ActivityHistory;
use app\models\InitiativesHistory;
use app\models\VwDepartments;
use app\models\InitiativesInvolvement;
use app\models\ParticipatoryBudgeting;
use app\models\search\InitiativesSearch;
use app\models\search\KpiDistributionSearch;
use app\components\Model;
use app\models\CascadingType;
use app\models\Ceiling;
use app\models\Customers;
use app\models\Initiatives as ModelsInitiatives;
use app\models\InitiativesCustomers;
use app\models\InitiativesStakeholder;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\Risk;
use app\models\RiskMitigation;
use app\models\search\OkrSearch;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\strategic\controllers\initiatives\DownloadAction;

/**
 * Initiatives2Controller implements the CRUD actions for Initiatives model.
 */
class Initiatives2Controller extends Controller
{
    public function actions()
    {
        return [
            'download' => DownloadAction::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Initiatives models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OkrSearch();
        $dataProvider = $searchModel->searchForInitiatives(Yii::$app->request->queryParams);

        $info = [];
        $employeePositions = PositionStructureEmployee::find()->alias('pse')->joinWith('position p')->where(['employee_id' => Yii::$app->user->id])->andWhere(['p.year' => $searchModel->year])->all();
        foreach($employeePositions as $ep){
            if($ceiling = Ceiling::findOne(['unit_structure_id' => $ep->position->unit_structure_id])){
                array_push($info, ['unit' => $ceiling->unit->unit_name, 'ceiling_amount' => $ceiling->ceiling_amount, 'total_budget' => ModelsInitiatives::getTotalBudget($ep->position->unit_structure_id)]);
            }
        }

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
            

            $listPosition = ArrayHelper::map(PositionStructure::find()->where(['year' => $searchModel->year])->orderBy('position_name')->all(), 'id', function($model){
                return $model->position_name . ' (' . $model->unit->unit_name . ')';
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];

            $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->all();
            
            foreach($vwEmployeeManagers as $vwEmployeeManager){
                $directorates = UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
                foreach($directorates as $directorate){
                    array_push($dept, $directorate->id);
                    
                    $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                    foreach($divisions as $division){
                        array_push($dept, $division->id);
    
                        $division2s = UnitStructure::find()->where(['superior_unit_id' => $division->id])->orderBy('unit_code')->all();
                        foreach($division2s as $division2){
                            array_push($dept, $division2->id);
                        }
                    }
                }
            }
            
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userId])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'id', $dept])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listPosition = ArrayHelper::map(PositionStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'unit_structure_id', $dept])->orderBy('position_name')->all(), 'id', function($model){
                return $model->position_name . ' (' . $model->unit->unit_name . ')';
            });
        } else {
            $userID = Yii::$app->user->id;
            $isLeader = false;
            
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    
                    $units = Yii::$app->db->createCommand("SELECT id FROM management.unit_structure WHERE superior_unit_id = {$ep->position->unit_structure_id}")->queryColumn();
                    foreach($units as $unit){
                        array_push($dept, $unit);
                    }
                    
                    array_push($dept, $units);

                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }

                if(in_array($ep->position->level, [2, 4, 5, 6])) $isLeader = true;
            }
            
            if(strtotime(Yii::$app->params['ttdKontrakDate']) > strtotime(date('Y-m-d')) && !$isLeader){
                $maxYear = date('Y');

                if($searchModel->year > $maxYear){
                    $year = '0000';
                } else {
                    $year = $searchModel->year;
                }
            } else {
                $year = $searchModel->year;
            }
            
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });

            $listPosition = ArrayHelper::map(PositionStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'unit_structure_id', $dept])->orderBy('position_name')->all(), 'id', function($model){
                return $model->position_name . ' (' . $model->unit->unit_name . ')';
            });
        }
        
        return $this->render('index', [
            'info' => $info,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
            'listPosition' => $listPosition,
        ]);
    }

    /**
     * Displays a single Initiatives model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $okr = ($model->okr_id)?$model->okr->okr_code:'-';
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "<b>Proker #".$model->initiatives_code." / Objective #".$okr."</b>",
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> ($model->status_id == Status::DRAFT or $model->status_id == Status::REVISION)?
                            Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']) :
                        (($model->status_id == Status::SEND and (Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')))?
                            Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('<i class="glyphicon glyphicon-ok"></i>&nbsp; Approve',['approve','id'=>$id],['class'=>'btn btn-success','role'=>'modal-remote']).
                            Html::a('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Revisi',['revision','id'=>$id],['class'=>'btn btn-warning','role'=>'modal-remote']).
                            Html::a('<i class="glyphicon glyphicon-remove"></i>&nbsp; Reject',['reject','id'=>$id],['class'=>'btn btn-danger','role'=>'modal-remote']) :
                        Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]))
                ];    
        }else{
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Initiatives model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->getRequest();
        $user = Yii::$app->getUser();
        $identity = $user->getIdentity();
        $okr = Okr::find()->orderBy('year desc')->one();
        $year = $okr->year ?: date('Y')+1;
        // $year = date('Y');

        $model = new Initiatives([
            'companyId' => $identity->company_id,
            // 'year' => $year,
        ]);

        if($user->can('Admin')){
            $objectives = Okr::find()
                            ->where(['type' => 'KR'])
                            ->andWhere(['year' => $year])
                            ->andWhere(['NOT IN', 'okr_level_id', [/* OkrLevel::ORGANIZATION,  */OkrLevel::INDIVIDUAL]])
                            // ->andWhere(['status_id' => Status::APPROVED
                            ->all();
        }else{

            $unitStructureId = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
            foreach($employeePositions as $ep){
                array_push($unitStructureId, $ep->position->unit_structure_id);
            }

            $objectives = Okr::find()
                            ->alias('o')
                            ->joinWith('position p')
                            ->where(['o.type' => 'KR'])
                            ->andWhere(['in', 'p.unit_structure_id', $unitStructureId])
                            ->andWhere(['NOT IN', 'o.okr_level_id', [/* OkrLevel::ORGANIZATION,  */OkrLevel::INDIVIDUAL]])
                            // ->andWhere(['status_id' => Status::APPROVED
                            ->all();
        }
        
        /* $objectiveList = [];
        foreach ($objectives as $objective) {
            $objectiveList[$objective->id] = $objective->okr_code . ' - ' . $objective->okr;
        } */

        $objectiveList = ArrayHelper::map($objectives, 'id', function($model){
            return $model->okr_code . ' - ' . $model->okr;
        }, function($model){
            return $model->parent->okr_code . ' - ' . $model->parent->okr;
        });

        $participatories = ArrayHelper::map(ParticipatoryBudgeting::find()->where(['is_active' => true])->orderBy('id')->all(), 'id', function($model){
            return $model->participatory_budgeting . ' - ' . $model->description;
        });

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "<b>Tambah Program Kerja</b>",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'objectives' => $objectives,
                        'objectiveList' => $objectiveList,
                        'participatories' => $participatories
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $this->save($model)){
                //simpan history draft proker
                $action = 'Input Draft Proker';
                $this->history($model->id, $action);
                
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['view', 'id' => $model->id]));
                return [
                    // 'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Info",
                    'content'=>'<span class="text-success">Berhasil membuat Program Kerja</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
        
                ];         
            }else{          
                return [
                    'title'=> "<b>Tambah Program Kerja</b>",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'objectives' => $objectives,
                        'objectiveList' => $objectiveList,
                        'participatories' => $participatories
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ( $this->save($model) ) {
                //simpan history draft proker
                $action = 'Input Draft Proker';
                $this->history($model->id, $action);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'objectives' => $objectives,
                    'objectiveList' => $objectiveList,
                    'participatories' => $participatories
                ]);
            }
        }  
    }

    protected function save(&$model)
    {
        $model->year = Okr::findOne($model->okr_id)->year; 
        // var_dump($model);die;
        
        if($model->save($model->isNewRecord)){

            if(!$model->isNewRecord){
                $deleteAllCustomers = InitiativesCustomers::find()->where(['initiatives_id' => $model->id])->all();
                foreach($deleteAllCustomers as $da){
                    $da->delete();
                }

                $deleteAllStakeholders = InitiativesStakeholder::find()->where(['initiatives_id' => $model->id])->all();
                foreach($deleteAllStakeholders as $ds){
                    $ds->delete();
                }
            }

            //InitiativesCustomers
            if($model->objectives){
                foreach($model->objectives as $ob){
                    $newInitiativeCustomers = new InitiativesCustomers();
                    $newInitiativeCustomers->initiatives_id = $model->id;
                    $newInitiativeCustomers->customers_id = $ob;
                    $newInitiativeCustomers->save();
                }
            }

            //InitiativesStakeholder
            if($model->involvement_dept_ids){
                foreach($model->involvement_dept_ids as $idi){
                    $newInitiativeStakeholder = new InitiativesStakeholder();
                    $newInitiativeStakeholder->initiatives_id = $model->id;
                    $newInitiativeStakeholder->stakeholder_id = $idi;
                    $newInitiativeStakeholder->save();
                }
            }

            return 1;
        } else {
            return 0;
        }

    }

    public function actionCreateOkr($initiatives_id){
        $request = Yii::$app->getRequest();
        $model = new Okr();
        $model->scenario = Okr::SCENARIO_INPUT_IK;
        $initiatives = Initiatives::findOne($initiatives_id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Indikator Keberhasilan",
                    'content'=>$this->renderAjax('indicator-form', [
                        'model' => $model,
                        'initiatives' => $initiatives,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){

                $objective = Okr::findOne([
                                            // 'reference_no' => $initiatives->initiatives_code, 
                                            // 'pic_id' => $model->pic_id, 
                                            // 'position_structure_id' =>$model->position_structure_id,
                                            'ref_initiatives_id' => $initiatives->id,
                                            'type' => 'O'
                                        ]);

                if(!$objective){
                    $objective = new Okr();
                    $objective->reference_no = $initiatives->initiatives_code;
                    // $objective->pic_id = $model->pic_id;
                    // $objective->position_structure_id =$model->position_structure_id;
                    $objective->okr = $initiatives->workplan_name;
                    $objective->type = 'O';
                    $objective->cascading_type_id = CascadingType::SUPPORT;
                    $objective->parent_id = $initiatives->okr_id;
                    $objective->okr_level_id = OkrLevel::INDIVIDUAL;
                    $objective->ref_id = $initiatives->okr_id;
                    $objective->okr_org_id = $initiatives->okr->okr_org_id;
                    $objective->reference_no = $initiatives->initiatives_code;
                    $objective->ref_initiatives_id = $initiatives->id;
                    $objective->year = $initiatives->year;
                    $objective->save();
                }

                $model->type = 'KR';
                $model->parent_id = $objective->id;
                $model->okr_level_id = $objective->okr_level_id;
                $model->position_structure_id = $model->position_structure_id;
                $model->pic_id = $model->pic_id;
                $model->cascading_type_id = CascadingType::SUPPORT;
                $model->reference_no = $initiatives->initiatives_code;
                $model->ref_initiatives_id = $initiatives->id;
                $model->year = $initiatives->year;
                if($model->save()){

                    $msg = '<span class="text text-success">Berhasil menambah Indikator Keberhasilan</span>';
                } else {
                    $msg = json_encode($model->errors);
                }


                
                
                return [
                    'forceReload'=>'#okr-pjax',
                    'content' => $msg,
                    'footer' => Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];         
            }else{           
                return [
                    'title'=> "Tambah Indikator Keberhasilan",
                    'content'=>$this->renderAjax('indicator-form', [
                        'model' => $model,
                        'initiatives' => $initiatives,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionUpdateOkr($id)
    {
        $request = Yii::$app->request;
        $model = Okr::findOne($id);
        $modelParent = Okr::findOne($model->parent_id);
        $initiatives = Initiatives::findOne($modelParent->ref_initiatives_id);
        // $initiatives = Initiatives::find()->alias('i')->joinWith('okr o')->where(['initiatives_code' => $model->reference_no])->andWhere(['o.year' => $model->year])->one();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Key Result',
                    'content'=>$this->renderAjax('indicator-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                        'initiatives' => $initiatives,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#okr-pjax',
                    'title'=> 'Ubah Key Result',
                    'content'=>'<span class="text-success">Berhasil menambah Key Result</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }else{
                return [
                    'title'=> 'Ubah Key Result',
                    'content'=>$this->renderAjax('indicator-form', [
                        'model' => $model,
                        'modelParent' => $modelParent,
                        'initiatives' => $initiatives,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionDeleteOkr($id)
    {
        $request = Yii::$app->request;
        $model = Okr::findOne($id);
        $initiativesId = $model->initiatives_id;
        $model->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#okr-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['view', 'id' => $initiativesId]);
        }
    }
    
    public function actionCreateRisk($initiatives_id){
        $request = Yii::$app->getRequest();
        $model = new Risk();
        $initiatives = Initiatives::findOne($initiatives_id);
        $model->initiatives_id = $initiatives_id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Risiko",
                    'content'=>$this->renderAjax('risk-form', [
                        'model' => $model,
                        'initiatives' => $initiatives,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){
                
                if($model->save()){                    
                    return [
                        'forceReload'=>'#risk-pjax',
                        'title'=> "Lihat Risk",
                        'content'=>$this->renderAjax('view-risk', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Edit',['update-risk','id'=>$model->id],['class'=>'btn btn-warning','role'=>'modal-remote']) 
                    ];  
                } else {
                    return [
                        'forceReload'=>'#risk-pjax',
                        'title'=> "Lihat Risk",
                        'content'=>'<span class="text text-danger>Gagal menambah Risiko</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];

                }
                
                       
            }else{           
                return [
                    'title'=> "Tambah Risiko",
                    'content'=>$this->renderAjax('risk-form', [
                        'model' => $model,
                        'initiatives' => $initiatives,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionViewRisk($id)
    {
        $request = Yii::$app->request;
        $model = Risk::findOne($id);
        
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Lihat Risk",
                    'content'=>$this->renderAjax('view-risk', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update-risk','id'=>$id],['class'=>'btn btn-warning','role'=>'modal-remote']) 
                ];    
        }else{
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateRisk($id){
        $request = Yii::$app->getRequest();
        $model = Risk::findOne($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Risiko",
                    'content'=>$this->renderAjax('risk-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){
                
                if($model->save()){                    
                    return [
                        'forceReload'=>'#risk-pjax',
                        'title'=> "Lihat Risk",
                        'content'=>$this->renderAjax('view-risk', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Edit',['update-risk','id'=>$model->id],['class'=>'btn btn-warning','role'=>'modal-remote']) 
                    ];  
                } else {
                    return [
                        'forceReload'=>'#risk-pjax',
                        'title'=> "Lihat Risk",
                        'content'=>'<span class="text text-danger>Gagal mengubah Risiko</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];

                }

            }else{           
                return [
                    'title'=> "Ubah Risiko",
                    'content'=>$this->renderAjax('risk-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionDeleteRisk($id)
    {
        $request = Yii::$app->request;
        $model = Risk::findOne($id);
        $initiativesId = $model->initiatives_id;
        $model->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#risk-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['view', 'id' => $initiativesId]);
        }
    }

    public function actionCreateRiskMitigation($id){
        $request = Yii::$app->getRequest();
        $model = new RiskMitigation();
        $model->risk_id = $id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Rencana Tindak Lanjut",
                    'content'=>$this->renderAjax('risk-mitigation-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){
                
                if($model->save()){  
                    $risk = Risk::findOne($id);                  
                    return [
                        'forceReload'=>'#risk-pjax',
                        'title'=> "Lihat Risiko",
                        'content'=>$this->renderAjax('view-risk', [
                            'model' => $risk,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Edit',['update-risk','id'=>$risk->id],['class'=>'btn btn-warning','role'=>'modal-remote']) 
                    ];  
                } else {
                    return [
                        'forceReload'=>'#risk-pjax',
                        'title'=> "Tambah Rencana Tindak Lanjut",
                        'content'=>'<span class="text text-danger>Gagal menambah Risiko</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];

                }
                
                       
            }else{           
                return [
                    'title'=> "Tambah Risiko",
                    'content'=>$this->renderAjax('risk-mitigation-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionUpdateRiskMitigation($id){
        $request = Yii::$app->getRequest();
        $model = RiskMitigation::findOne($id);
        $riskId = $model->risk_id;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Rencana Tindak Lanjut",
                    'content'=>$this->renderAjax('risk-mitigation-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){
                
                if($model->save()){  
                    $risk = Risk::findOne($riskId);                  
                    return [
                        'forceReload'=>'#risk-pjax',
                        'title'=> "Lihat Risiko",
                        'content'=>$this->renderAjax('view-risk', [
                            'model' => $risk,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Edit',['update-risk','id'=>$risk->id],['class'=>'btn btn-warning','role'=>'modal-remote']) 
                    ];  
                } else {
                    return [
                        'forceReload'=>'#risk-pjax',
                        'title'=> "Ubah Rencana Tindak Lanjut",
                        'content'=>'<span class="text text-danger>Gagal menambah Risiko</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];

                }
                
                       
            }else{           
                return [
                    'title'=> "Ubah Risiko",
                    'content'=>$this->renderAjax('risk-mitigation-form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionDeleteRiskMitigation($id)
    {
        $request = Yii::$app->request;
        $model = RiskMitigation::findOne($id);
        $riskId = $model->risk_id;
        $model->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            $risk = Risk::findOne($riskId);                  
            return [
                'title'=> "Lihat Risiko",
                'content'=>$this->renderAjax('view-risk', [
                    'model' => $risk,
                ]),
                'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Edit',['update-risk','id'=>$risk->id],['class'=>'btn btn-warning','role'=>'modal-remote']) 
            ];  
        }else{
            
            return $this->redirect(['index']);
        }
    }


    /**
     * Updates an existing Initiatives model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->getRequest();
        $user = Yii::$app->getUser();
        $identity = $user->getIdentity();

        $model = $this->findModel($id);
        $model->companyId = $identity->company_id;
        $year = $model->year;

        if($user->can('Admin')){
            $objectives = Okr::find()
                            ->where(['type' => 'O'])
                            ->andWhere(['year' => $year])
                            // ->andWhere(['status_id' => Status::APPROVED
                            ->all();
        }else{

            $positionIds = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
            foreach($employeePositions as $ep){
                array_push($positionIds, $ep->position_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($positionIds, $gm->id);
                    }
                }
            }

            $objectives = Okr::find()
                        ->where(['type' => 'O'])
                        ->andWhere(['in', 'position_structure_id', $positionIds])
                        // ->andWhere(['status_id' => Status::APPROVED
                        ->all();
        }
        
        $objectiveList = $objectiveDivisions = [];
        foreach ($objectives as $objective) {
            $objectiveList[$objective->id] = $objective->okr_code . ' - ' . $objective->okr;
            $objectiveDivisions[$objective->id] = $objective->position->unit_structure_id;
        }

        $participatories = ArrayHelper::map(ParticipatoryBudgeting::find()->where(['is_active' => true])->orderBy('id')->all(), 'id', function($model){
            return $model->participatory_budgeting . ' - ' . $model->description;
        });

        $iku = (@$model->okr_id)?$model->okr->okr_code:'-';

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "<b>Update Proker #".$model->initiatives_code." / Objective #".$iku."</b>",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'objectives' => $objectives,
                        'objectiveList' => $objectiveList,
                        'objectiveDivisions' => $objectiveDivisions,
                        'participatories' => $participatories
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $this->save($model)){
                //simpan history draft proker
                $action = 'Update Proker';
                $this->history($model->id, $action);
                
                return [
                    // 'forceClose'=>true, 
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "<b>Update Proker #".$model->initiatives_code." / Objective #".$iku."</b>",
                    'content'=>'<span class="text-success">Update Proker sukses</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])/* .
                            Html::a('Update Lagi',['update', 'id' => $model->id],['class'=>'btn btn-primary','role'=>'modal-remote']) */
        
                ];         
            }else{         
                return [
                    'title'=> "<b>Update Proker #".$model->initiatives_code." / Objective #".$iku."</b>",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'objectives' => $objectives,
                        'objectiveList' => $objectiveList,
                        'objectiveDivisions' => $objectiveDivisions,
                        'participatories' => $participatories
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ( $this->save($model) ) {
                //simpan history draft proker
                $action = 'Update Proker';
                $this->history($model->id, $action);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'objectives' => $objectives,
                    'objectiveList' => $objectiveList,
                    'objectiveDivisions' => $objectiveDivisions,
                    'participatories' => $participatories
                ]);
            }
        }
    }

    /**
     * Delete an existing Initiatives model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $year = $model->position->year;
        $unitId = $model->position->unit_structure_id;
        $model->delete();

        Okr::deleteAll(['ref_initiatives_id' => $id]);
        // $action = 'Delete';
        // $model = $this->findModel($id);
        // $model->status_id = Status::DELETED;
        // if($model->save(false))
        //     $this->history($model->id, $action);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true/* ,'forceReload'=>'#crud-datatable-pjax' */];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(["index?OkrSearch[year]=$year&OkrSearch[unit_id]=$unitId"]);
        }
    }

    public function actionRevisi($id)
    {
        $request = Yii::$app->request;
        // $this->findModel($id)->delete();
        $action = 'Ubah Status Revisi';
        $model = $this->findModel($id);
        $model->status_id = Status::REVISION;
        if($model->save(false)){
            $activities = Activity::find()->where(['initiatives_id' => $id])->all();
            foreach($activities as $activity){
                $activity->status_id = Status::REVISION;
                if($activity->save(false)){
                    $this->activityHistory($activity->id, $action);
                    $budgets = Budget::find()->where(['activity_id' => $activity->id])->all();
                    foreach($budgets as $budget){
                        $budget->status_id = Status::REVISION;
                        if($budget->save(false)){
                            $this->budgetHistory($budget->id, $action);
                        }
                    }
                }
            }
            $this->history($model->id, $action);
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

     /**
     * Delete multiple existing Initiatives model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }  
    }

    public function actionBulkSetParticipatory()
    {
        $request = Yii::$app->request;
        $participatoryId = $request->post('participatory');
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->participatory_budgeting_id = $participatoryId;
            $model->save(false);            
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }  
    }

    /**
     * Finds the Initiatives model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Initiatives the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Initiatives::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function history($id, $action, $comment='')
    {
        $history=new InitiativesHistory;
        $history->by = Yii::$app->user->identity->id;
        $history->ip = Yii::$app->request->userIP;
        $history->initiatives_id = $id;
        $history->action = $action;
        $history->comment = $comment;
        $history->save();
    }

    public function budgetHistory($id, $action, $comment='')
    {
        $history=new BudgetHistory;
        $history->by = Yii::$app->user->identity->id;
        $history->ip = Yii::$app->request->userIP;
        $history->budget_id = $id;
        $history->action = $action;
        $history->comment = $comment;
        $history->save();
    }

    public function activityHistory($id, $action, $comment='')
    {
        $history=new ActivityHistory;
        $history->by = Yii::$app->user->identity->id;
        $history->ip = Yii::$app->request->userIP;
        $history->activity_id = $id;
        $history->action = $action;
        $history->comment = $comment;
        $history->save();
    }

    public function actionSend()
    {
        $request = Yii::$app->request;
        $unit = $request->post( 'unit_structure_id' );
        $year = UnitStructure::findOne($unit)->year ?: 2022;
        $comment = $request->post( 'confirm-comment' );
        $action = 'Send';
        $isOverBudget = false;

        if($unit){
            //kirim Initiatives
            $initiatives = Initiatives::find()
                                            ->alias('i')
                                            ->joinWith(['okr o', 'okr.position p'])
                                            ->where(['or',['i.status_id'=>Status::DRAFT],['i.status_id'=>Status::REVISION]])
                                            ->andWhere(['in', 'p.unit_structure_id', $unit])
                                            ->andWhere(['o.year' => $year])
                                            ->all();
        
            $budget = Ceiling::find()->where(['unit_structure_id' => $unit])->sum('ceiling_amount::numeric');

            // $totalBudget = 0;
            // $positionStructures = PositionStructure::find()->where(['unit_structure_id' => $unit])->all();
            // foreach($positionStructures as $ps){
            //     $totalBudget += Initiatives::find()->where(['position_structure_id' => $ps->id])->andWhere('status_id <> 5')->sum('total_budget');
            // }

            $totalBudget = Budget::find()
                                ->alias('b')
                                ->joinWith(['initiatives i', 'initiatives.okr o', 'initiatives.okr.position p'])
                                ->where(['p.unit_structure_id' => $unit])
                                ->andWhere(['p.year' => $year])
                                ->andWhere(['not in', 'i.status_id', [Status::REJECTED, Status::DELETED]])
                                ->sum('total_amount');
                                                 
            if(round($totalBudget) > round($budget)){
                // var_dump(round($totalBudget));
                // var_dump(round($budget));die;
                $isOverBudget = true;
            } else {
                $isOverBudget = false;
                
                foreach ($initiatives as $i) {
                    $i->status_id = Status::SEND;
                    if($i->save(false)){
                        //simpan history initiative
                        $this->history($i->id, $action, $comment);
        
                        //kirim budget
                        $budgets = Budget::find()
                                            ->alias('b')
                                            ->where(['or',['b.status_id' => Status::DRAFT], ['b.status_id' => Status::REVISION]])
                                            ->andWhere(['initiatives_id' => $i->id])
                                            ->all();
                        if($budgets){
                            foreach ($budgets as $budget) {
                                $budget->status_id = Status::SEND;
                                if($budget->save(false)){
                                    //simpan history budget
                                    $this->budgetHistory($budget->id, $action, $comment);
                                }
                            }
                        }

                        /* //kirim activity
                        $activities = Activity::find()
                                                ->alias('a')->where(['or',['a.status_id'=>Status::DRAFT],['a.status_id'=>Status::REVISION]])
                                                ->andWhere(['initiatives_id'=>$i->id])
                                                ->all();
                        if($activities){
                            foreach ($activities as $activity) {
                                $activity->status_id = Status::SEND;
                                if($activity->save(false)){
                                    //simpan history activity
                                    $this->activityHistory($activity->id, $action, $comment);
        
        
                                    //kirim budget
                                    $budgets = Budget::find()
                                    ->alias('b')
                                    ->where(['or',['b.status_id'=>Status::DRAFT],['b.status_id'=>Status::REVISION]])
                                    ->andWhere(['activity_id'=>$activity->id])->all();
                                    if($budgets){
                                        foreach ($budgets as $budget) {
                                            $budget->status_id = Status::SEND;
                                            if($budget->save(false)){
                                                //simpan history budget
                                                $this->budgetHistory($budget->id, $action, $comment);
                                            }
                                        }
                                    }
                                }
                            }
                        } */
        
                    }
                }
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if(!$unit){
                return [
                    'title'=> "Info",
                    'content'=>'<span class="text-danger">Silahkan pilih Unit terlebih dahulu</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            } else if($isOverBudget){
                return [
                    'title'=> "Info",
                    'content'=>'<span class="text-danger">Total Anggaran melebihi Pagu Anggaran</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index']));
                return [
                            'forceClose'=>true,
                        ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionApproval()
    {
        $searchModel = new InitiativesSearch();
        $dataProvider = $searchModel->searchForApproval(Yii::$app->request->queryParams);

        return $this->render('approval', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /* public function actionApprovalAll()
    {
        $searchModel = new InitiativesSearch();
        $dataProvider = $searchModel->searchForApproval2(Yii::$app->request->queryParams);
        // $bsc_year = Bsc::find()->where(['company_id'=>Yii::$app->user->identity->company_id])->orderBy('year desc')->all();
        // $yearItems = ArrayHelper::map($bsc_year, 'year','year');

        return $this->render('approval_all', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            // 'yearItems' => $yearItems,
        ]);
    } */

    public function actionApprovalAll()
    {
        $searchModel = new OkrSearch();
        $dataProvider = $searchModel->searchForInitiatives(Yii::$app->request->queryParams);

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }
        
        return $this->render('approve-all', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
        ]);
    }

    public function actionApprove($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "<b>Approve Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('<i class="glyphicon glyphicon-eye-open"></i>&nbsp; View Proker',['view','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']).
                                Html::button('<i class="glyphicon glyphicon-ok"></i>&nbsp; Approve',['class'=>'btn btn-success','type'=>"submit"])
                ];         
            }else if($model->load($request->post())) {
                $comment = $_POST['Initiatives']['comment'];
                $action = 'Approved';
                $model->status_id = Status::APPROVED;
                if($model->save(false)){
                    //simpan history initiatives
                    $this->history($model->id, $action, $comment);
                    //update status activity & simpan history
                    $find_activity = Activity::find()->where(['initiatives_id'=>$id])->all();
                    foreach($find_activity as $activity){
                        if($activity->status_id == Status::SEND) {
                            $activity->status_id = Status::APPROVED;
                            if($activity->save(false)){
								$this->activityHistory($activity->id, $action, $comment);

                                //update status budget & simpan history
                                $find_budget = Budget::find()->where(['activity_id'=>$activity->id])->all();
                                foreach ($find_budget as $budget) {
                                    if($budget->status_id == Status::SEND) {
                                        $budget->status_id = Status::APPROVED;
                                        if($budget->save(false))
                                            $this->budgetHistory($budget->id, $action, $comment);
                                    }
                                }
                            }
                        }
                    }
                    //kirim email notif

                    Yii::$app->response->format = Response::FORMAT_JSON;
                    Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index']));
                    return [
                        // 'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "<b>Proker #".$model->initiatives_code.' Approved</b>',
                        'content'=>'<span class="text-success">RKA Proker #'.$model->initiatives_code.' telah sukses di-approve</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                } else {
                    return [
                        'title'=> 'Info',
                        'content'=>'Error: ' . json_encode($model->errors),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                }
            }else{
                 return [
                    'title'=> "<b>Approve Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Approve',['class'=>'btn btn-success','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form_approval', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionRevision($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "<b>Revisi Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('<i class="glyphicon glyphicon-eye-open"></i>&nbsp; View Proker',['view','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']).
                                Html::button('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Revisi',['class'=>'btn btn-warning','type'=>"submit"])
                ];         
            }else if($model->load($request->post())) {
                $comment = $_POST['Initiatives']['comment'];
                $action = 'Need Revision';
                $model->status_id = Status::REVISION;
                if($model->save(false)){
                    //simpan history initiatives
                    $this->history($model->id, $action, $comment);

                    //update status activity & simpan history
                    $find_activity = Activity::find()->where(['initiatives_id'=>$id])->all();
                    foreach($find_activity as $activity){
                        if($activity->status_id == Status::SEND) {
                            $activity->status_id = Status::REVISION;
                            if($activity->save(false)){
								$this->activityHistory($activity->id, $action, $comment);

                                //update status budget & simpan history
                                $find_budget = Budget::find()->where(['activity_id'=>$activity->id])->all();
                                foreach ($find_budget as $budget) {
                                    if($budget->status_id == Status::SEND) {
                                        $budget->status_id = Status::REVISION;
                                        if($budget->save(false))
                                            $this->budgetHistory($budget->id, $action, $comment);
                                    }
                                }
                            }
                        }
                    }
                    //kirim email notif

                    Yii::$app->response->format = Response::FORMAT_JSON;
                    Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index']));
                    return [
                        // 'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "<b>Proker #".$model->initiatives_code.'</b>',
                        'content'=>'<span class="text-success">Permintaan Revisi RKA Proker #'.$model->initiatives_code.' telah sukses dikirim</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                }
            }else{
                 return [
                    'title'=> "<b>Revisi Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Revisi',['class'=>'btn btn-warning','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form_approval', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionReject($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "<b>Reject Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('<i class="glyphicon glyphicon-eye-open"></i>&nbsp; View Proker',['view','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']).
                                Html::button('<i class="glyphicon glyphicon-remove"></i>&nbsp; Reject',['class'=>'btn btn-danger','type'=>"submit"])
                ];         
            }else if($model->load($request->post())) {
                $comment = $_POST['Initiatives']['comment'];
                $action = 'Rejected';
                $model->status_id = Status::REJECTED;
                if($model->save(false)){
                    //simpan history initiatives
                    $this->history($model->id, $action, $comment);

                    //update status activity & simpan history
                    $find_activity = Activity::find()->where(['initiatives_id'=>$id])->all();
                    foreach($find_activity as $activity){
                        if($activity->status_id == Status::SEND) {
                            $activity->status_id = Status::REJECTED;
                            if($activity->save(false)){
								$this->activityHistory($activity->id, $action, $comment);

                                //update status budget & simpan history
                                $find_budget = Budget::find()->where(['activity_id'=>$activity->id])->all();
                                foreach ($find_budget as $budget) {
                                    if($budget->status_id == Status::SEND) {
                                        $budget->status_id = Status::REJECTED;
                                        if($budget->save())
                                            $this->budgetHistory($budget->id, $action, $comment);
                                    }
                                }
                            }
                        }
                    }
                    //kirim email notif

                    Yii::$app->response->format = Response::FORMAT_JSON;
                    Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index']));
                    return [
                        // 'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "<b>Proker #".$model->initiatives_code.' Rejected</b>',
                        'content'=>'<span class="text-success">RKA Proker #'.$model->initiatives_code.' telah sukses di-reject</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                }
            }else{
                 return [
                    'title'=> "<b>Reject Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Reject',['class'=>'btn btn-danger','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form_approval', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionBulkApprove()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        $comment = $request->post( 'confirm-comment' );
        $action = 'Approved';
        foreach ( $pks as $pk ) {
            // $model = $this->findModel($pk);
            $model = ModelsInitiatives::findOne($pk);
            $model->status_id = Status::APPROVED;
            if ($model->save(false)) {
                //simpan history initiatives
                $this->history($model->id, $action, $comment);
                
                //update status activity & simpan history
                /* $find_activity = Activity::find()->where(['initiatives_id'=>$model->id])->all();
                foreach($find_activity as $activity){
                    if($activity->status_id == Status::SEND) {
                        $activity->status_id = Status::APPROVED;
                        if($activity->save(false)){
                            $this->activityHistory($activity->id, $action, $comment);

                            //update status budget & simpan history
                            $find_budget = Budget::find()->where(['activity_id'=>$activity->id])->all();
                            foreach ($find_budget as $budget) {
                                if($budget->status_id == Status::SEND) {
                                    $budget->status_id = Status::APPROVED;
                                    if($budget->save(false))
                                        $this->budgetHistory($budget->id, $action, $comment);
                                }
                            }
                        } else {
                            //donothing
                        }
                    } else {
                        //donothing
                    }
                } */
                //kirim email notif
                
            } else {
                var_dump($model->errors);die;
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionBulkRevision()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        $comment = $request->post( 'confirm-comment' );
        $action = 'Need Revision';
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->status_id = Status::REVISION;//var_dump($model->save());var_dump($model->errors);die();
            if ($model->save(false)) {
                //simpan history initiatives
                $this->history($model->id, $action, $comment);

                //update status activity & simpan history
                $find_activity = Activity::find()->where(['initiatives_id'=>$model->id])->all();
                foreach($find_activity as $activity){
                    if($activity->status_id == Status::SEND) {
                        $activity->status_id = Status::REVISION;
                        if($activity->save(false)){
							$this->activityHistory($activity->id, $action, $comment);


                            //update status budget & simpan history
                            $find_budget = Budget::find()->where(['activity_id'=>$activity->id])->all();
                            foreach ($find_budget as $budget) {
                                if($budget->status_id == Status::SEND) {
                                    $budget->status_id = Status::REVISION;
                                    if($budget->save(false))
                                        $this->budgetHistory($budget->id, $action, $comment);
                                }
                            }
                        }
                    }
                }
                //kirim email notif
                // \app\components\EmailNotification::approvalStatusToCreator($model, $oldStatusId, $doingApprovalStatus, $comment);
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionBulkReject()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        $comment = $request->post( 'confirm-comment' );
        $action = 'Rejected';
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->status_id = Status::REJECTED;
            if ($model->save(false)) {
                //simpan history initiatives
                $this->history($model->id, $action, $comment);

                //update status activity & simpan history
                $find_activity = Activity::find()->where(['initiatives_id'=>$model->id])->all();
                foreach($find_activity as $activity){
                    if($activity->status_id == Status::SEND) {
                        $activity->status_id = Status::REJECTED;
                        if($activity->save(false)){
							$this->activityHistory($activity->id, $action, $comment);

                            //update status budget & simpan history
                            $find_budget = Budget::find()->where(['activity_id'=>$activity->id])->all();
                            foreach ($find_budget as $budget) {
                                if($budget->status_id == Status::SEND) {
                                    $budget->status_id = Status::REJECTED;
                                    if($budget->save(false))
                                        $this->budgetHistory($budget->id, $action, $comment);
                                }
                            }
                        }
                    }
                }
                //kirim email notif
                
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }
}
