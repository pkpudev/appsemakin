<?php

namespace app\modules\strategic\controllers;

use Yii;
use app\modules\strategic\models\Initiatives;
use app\modules\strategic\models\InitiativesSource;
use app\models\Status;
use app\models\Budget;
use app\models\Bsc;
use app\models\BudgetHistory;
use app\models\Activity;
use app\models\ActivityHistory;
use app\models\InitiativesHistory;
use app\models\VwDepartments;
use app\models\InitiativesInvolvement;
use app\models\ParticipatoryBudgeting;
use app\models\search\InitiativesSearch;
use app\models\search\KpiDistributionSearch;
use app\components\Model;
use app\models\Ceiling;
use app\models\Customers;
use app\models\Initiatives as ModelsInitiatives;
use app\models\InitiativesCustomers;
use app\models\InitiativesStakeholder;
use app\models\Okr;
use app\models\OkrLevel;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\search\OkrSearch;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\strategic\controllers\initiatives\DownloadAction;

/**
 * InitiativesController implements the CRUD actions for Initiatives model.
 */
class InitiativesController extends Controller
{
    public function actions()
    {
        return [
            'download' => DownloadAction::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Initiatives models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OkrSearch();
        $dataProvider = $searchModel->searchForInitiatives(Yii::$app->request->queryParams);

        $info = [];
        $employeePositions = PositionStructureEmployee::find()->alias('pse')->joinWith('position p')->where(['employee_id' => Yii::$app->user->id])->andWhere(['p.year' => $searchModel->year])->all();
        foreach($employeePositions as $ep){
            if($ceiling = Ceiling::findOne(['unit_structure_id' => $ep->position->unit_structure_id])){
                array_push($info, ['unit' => $ceiling->unit->unit_name, 'ceiling_amount' => $ceiling->ceiling_amount, 'total_budget' => ModelsInitiatives::getTotalBudget($ep->position->unit_structure_id)]);
            }
        }

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else if(Yii::$app->user->can('Admin Direktorat')){
            $userId = Yii::$app->user->id;
            $dept = [];

            $vwEmployeeManager = VwEmployeeManager::find()->where(['employee_id' => $userId])->andWhere(['year' => $searchModel->year])->one();

            $directorates = UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['superior_unit_id' => $vwEmployeeManager->directorate_id])->orderBy('unit_code')->all();
            foreach($directorates as $directorate){
                array_push($dept, $directorate->id);
                
                $divisions = UnitStructure::find()->where(['superior_unit_id' => $directorate->id])->orderBy('unit_code')->all();
                foreach($divisions as $division){
                    array_push($dept, $division->id);
                }
            }

            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->andWhere(['in', 'id', $dept])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
                
                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($dept, $gm->unit_structure_id);
                    }
                }
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }
        
        return $this->render('index', [
            'info' => $info,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
        ]);
    }

    /**
     * Displays a single Initiatives model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $okr = ($model->okr_id)?$model->okr->okr_code:'-';
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "<b>Proker #".$model->initiatives_code." / Objective #".$okr."</b>",
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> ($model->status_id == Status::DRAFT or $model->status_id == Status::REVISION)?
                            Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']) :
                        (($model->status_id == Status::SEND and (Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')))?
                            Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('<i class="glyphicon glyphicon-ok"></i>&nbsp; Approve',['approve','id'=>$id],['class'=>'btn btn-success','role'=>'modal-remote']).
                            Html::a('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Revisi',['revision','id'=>$id],['class'=>'btn btn-warning','role'=>'modal-remote']).
                            Html::a('<i class="glyphicon glyphicon-remove"></i>&nbsp; Reject',['reject','id'=>$id],['class'=>'btn btn-danger','role'=>'modal-remote']) :
                        Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]))
                ];    
        }else{
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Initiatives model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->getRequest();
        $user = Yii::$app->getUser();
        $identity = $user->getIdentity();
        $okr = Okr::find()->orderBy('year desc')->one();
        $year = $okr->year ?: date('Y');

        $model = new Initiatives([
            'companyId' => $identity->company_id,
            'year' => $year,
        ]);

        if($user->can('Admin')){
            $objectives = Okr::find()
                            ->where(['type' => 'O'])
                            ->andWhere(['year' => $year])
                            ->andWhere(['<>', 'okr_level_id', OkrLevel::ORGANIZATION])
                            // ->andWhere(['status_id' => Status::APPROVED
                            ->all();
        }else{

            $unitStructureId = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
            foreach($employeePositions as $ep){
                array_push($unitStructureId, $ep->position->unit_structure_id);
            }

            $objectives = Okr::find()
                            ->alias('o')
                            ->joinWith('position p')
                            ->where(['o.type' => 'O'])
                            ->andWhere(['in', 'p.unit_structure_id', $unitStructureId])
                            ->andWhere(['<>', 'o.okr_level_id', OkrLevel::ORGANIZATION])
                            // ->andWhere(['status_id' => Status::APPROVED
                            ->all();
        }
        
        $objectiveList = $objectiveDivisions = [];
        foreach ($objectives as $objective) {
            $objectiveList[$objective->id] = $objective->okr_code . ' - ' . $objective->okr;
            $objectiveDivisions[$objective->id] = $objective->position->unit_structure_id;
        }

        $participatories = ArrayHelper::map(ParticipatoryBudgeting::find()->where(['is_active' => true])->orderBy('id')->all(), 'id', 'participatory_budgeting');

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "<b>Tambah Program Kerja ".$year."</b>",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'objectives' => $objectives,
                        'objectiveList' => $objectiveList,
                        'objectiveDivisions' => $objectiveDivisions,
                        'participatories' => $participatories
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $this->save($model)){
                //simpan history draft proker
                $action = 'Input Draft Proker';
                $this->history($model->id, $action);
                
                return [
                    'forceClose'=>true, 'forceReload'=>'#crud-datatable-pjax',
                    // 'title'=> "<b>Create new Proker</b>",
                    // 'content'=>'<span class="text-success">Create Proker success</span>',
                    // 'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    //         Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "<b>Tambah Program Kerja ".$year."</b>",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'objectives' => $objectives,
                        'objectiveList' => $objectiveList,
                        'objectiveDivisions' => $objectiveDivisions,
                        'participatories' => $participatories
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ( $this->save($model) ) {
                //simpan history draft proker
                $action = 'Input Draft Proker';
                $this->history($model->id, $action);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'objectives' => $objectives,
                    'objectiveList' => $objectiveList,
                    'objectiveDivisions' => $objectiveDivisions,
                    'participatories' => $participatories
                ]);
            }
        }  
    }

    protected function save(&$model)
    {
        
        if($model->save($model->isNewRecord)){

            if(!$model->isNewRecord){
                $deleteAllCustomers = InitiativesCustomers::find()->where(['initiatives_id' => $model->id])->all();
                foreach($deleteAllCustomers as $da){
                    $da->delete();
                }

                $deleteAllStakeholders = InitiativesStakeholder::find()->where(['initiatives_id' => $model->id])->all();
                foreach($deleteAllStakeholders as $ds){
                    $ds->delete();
                }
            }

            //InitiativesCustomers
            if($model->objectives){
                foreach($model->objectives as $ob){
                    $newInitiativeCustomers = new InitiativesCustomers();
                    $newInitiativeCustomers->initiatives_id = $model->id;
                    $newInitiativeCustomers->customers_id = $ob;
                    $newInitiativeCustomers->save();
                }
            }

            //InitiativesStakeholder
            if($model->involvement_dept_ids){
                foreach($model->involvement_dept_ids as $idi){
                    $newInitiativeStakeholder = new InitiativesStakeholder();
                    $newInitiativeStakeholder->initiatives_id = $model->id;
                    $newInitiativeStakeholder->stakeholder_id = $idi;
                    $newInitiativeStakeholder->save();
                }
            }

            return 1;
        } else {
            return 0;
        }

    }

    /**
     * Updates an existing Initiatives model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->getRequest();
        $user = Yii::$app->getUser();
        $identity = $user->getIdentity();

        // $model = $this->findModel($id);
        $model = Initiatives::findOne($id);
        $model->companyId = $identity->company_id;
        $year = $model->year;

        if($user->can('Admin')){
            $objectives = Okr::find()
                            ->where(['type' => 'O'])
                            ->andWhere(['year' => $year])
                            // ->andWhere(['status_id' => Status::APPROVED
                            ->all();
        }else{

            $positionIds = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
            foreach($employeePositions as $ep){
                array_push($positionIds, $ep->position_structure_id);

                if($ep->position->level == 6){
                    $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                    foreach($gms as $gm){
                        array_push($positionIds, $gm->id);
                    }
                }
            }

            $objectives = Okr::find()
                        ->where(['type' => 'O'])
                        ->andWhere(['in', 'position_structure_id', $positionIds])
                        // ->andWhere(['status_id' => Status::APPROVED
                        ->all();
        }
        
        $objectiveList = $objectiveDivisions = [];
        foreach ($objectives as $objective) {
            $objectiveList[$objective->id] = $objective->okr_code . ' - ' . $objective->okr;
            $objectiveDivisions[$objective->id] = $objective->position->unit_structure_id;
        }

        $participatories = ArrayHelper::map(ParticipatoryBudgeting::find()->where(['is_active' => true])->orderBy('id')->all(), 'id', 'participatory_budgeting');

        $iku = (@$model->okr_id)?$model->okr->okr_code:'-';

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "<b>Update Proker #".$model->initiatives_code." / Objective #".$iku."</b>",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'objectives' => $objectives,
                        'objectiveList' => $objectiveList,
                        'objectiveDivisions' => $objectiveDivisions,
                        'participatories' => $participatories
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $this->save($model)){
                //simpan history draft proker
                $action = 'Update Proker';
                $this->history($model->id, $action);
                
                return [
                    'forceClose'=>true, 'forceReload'=>'#crud-datatable-pjax',
                    // 'title'=> "<b>Update Proker #".$model->initiatives_code." / Objective #".$iku."</b>",
                    // 'content'=>'<span class="text-success">Update Proker sukses</span>',
                    // 'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    //         Html::a('Update Lagi',['update', 'id' => $model->id],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{         
                return [
                    'title'=> "<b>Update Proker #".$model->initiatives_code." / Objective #".$iku."</b>",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'objectives' => $objectives,
                        'objectiveList' => $objectiveList,
                        'objectiveDivisions' => $objectiveDivisions,
                        'participatories' => $participatories
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ( $this->save($model) ) {
                //simpan history draft proker
                $action = 'Update Proker';
                $this->history($model->id, $action);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'objectives' => $objectives,
                    'objectiveList' => $objectiveList,
                    'objectiveDivisions' => $objectiveDivisions,
                    'participatories' => $participatories
                ]);
            }
        }
    }

    /**
     * Delete an existing Initiatives model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();
        // $action = 'Delete';
        // $model = $this->findModel($id);
        // $model->status_id = Status::DELETED;
        // if($model->save(false))
        //     $this->history($model->id, $action);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionRevisi($id)
    {
        $request = Yii::$app->request;
        // $this->findModel($id)->delete();
        $action = 'Ubah Status Revisi';
        $model = $this->findModel($id);
        $model->status_id = Status::REVISION;
        if($model->save(false)){
            $activities = Activity::find()->where(['initiatives_id' => $id])->all();
            foreach($activities as $activity){
                $activity->status_id = Status::REVISION;
                if($activity->save(false)){
                    $this->activityHistory($activity->id, $action);
                    $budgets = Budget::find()->where(['activity_id' => $activity->id])->all();
                    foreach($budgets as $budget){
                        $budget->status_id = Status::REVISION;
                        if($budget->save(false)){
                            $this->budgetHistory($budget->id, $action);
                        }
                    }
                }
            }
            $this->history($model->id, $action);
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

     /**
     * Delete multiple existing Initiatives model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }  
    }

    public function actionBulkSetParticipatory()
    {
        $request = Yii::$app->request;
        $participatoryId = $request->post('participatory');
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->participatory_budgeting_id = $participatoryId;
            $model->save(false);            
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }  
    }

    /**
     * Finds the Initiatives model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Initiatives the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Initiatives::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function history($id, $action, $comment='')
    {
        $history=new InitiativesHistory;
        $history->by = Yii::$app->user->identity->id;
        $history->ip = Yii::$app->request->userIP;
        $history->initiatives_id = $id;
        $history->action = $action;
        $history->comment = $comment;
        $history->save();
    }

    public function budgetHistory($id, $action, $comment='')
    {
        $history=new BudgetHistory;
        $history->by = Yii::$app->user->identity->id;
        $history->ip = Yii::$app->request->userIP;
        $history->budget_id = $id;
        $history->action = $action;
        $history->comment = $comment;
        $history->save();
    }

    public function activityHistory($id, $action, $comment='')
    {
        $history=new ActivityHistory;
        $history->by = Yii::$app->user->identity->id;
        $history->ip = Yii::$app->request->userIP;
        $history->activity_id = $id;
        $history->action = $action;
        $history->comment = $comment;
        $history->save();
    }

    public function actionSend()
    {
        $request = Yii::$app->request;
        $year = $request->post('send-year') ?: 2022;
        $unit = $request->post( 'unit_structure_id' );
        $comment = $request->post( 'confirm-comment' );
        $action = 'Send';

        if($unit){
            //kirim Initiatives
            $initiatives = Initiatives::find()
                                            ->alias('i')
                                            ->joinWith(['okr o', 'okr.position p'])
                                            ->where(['or',['i.status_id'=>Status::DRAFT],['i.status_id'=>Status::REVISION]])
                                            ->andWhere(['in', 'p.unit_structure_id', $unit])
                                            ->andWhere(['o.year'=>$year])
                                            ->all();
     
            $director_id = null;
            foreach ($initiatives as $i) {
                $i->status_id = Status::SEND;
                if($i->save(false)){
                    //simpan history initiative
                    $this->history($i->id, $action, $comment);
    
                    //kirim activity
                    $activities = Activity::find()
                                            ->alias('a')->where(['or',['a.status_id'=>Status::DRAFT],['a.status_id'=>Status::REVISION]])
                                            ->andWhere(['initiatives_id'=>$i->id])
                                            ->all();
                    if($activities){
                        foreach ($activities as $activity) {
                            $activity->status_id = Status::SEND;
                            if($activity->save(false)){
                                //simpan history activity
                                $this->activityHistory($activity->id, $action, $comment);
    
    
                                //kirim budget
                                $budgets = Budget::find()
                                ->alias('b')
                                ->where(['or',['b.status_id'=>Status::DRAFT],['b.status_id'=>Status::REVISION]])
                                ->andWhere(['activity_id'=>$activity->id])->all();
                                if($budgets){
                                    foreach ($budgets as $budget) {
                                        $budget->status_id = Status::SEND;
                                        if($budget->save(false)){
                                            //simpan history budget
                                            $this->budgetHistory($budget->id, $action, $comment);
                                        }
                                    }
                                }
                            }
                        }
                    }
    
                    // $dept = VwDepartments::find()->where(['deptid'=>$i->kpiDistribution->departments_id, 'year'=>$year])->one();
                    // $director_id = $dept->director_id;
                }
            }
            // Kirim email notif approval
            // if($director_id)
            //     \app\components\EmailNotification::approvalRkat($dept->director_id, $dept->deptname, $year);
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if(!$unit){

                return [
                    'title'=> "Info",
                    'content'=>'<span class="text-danger">Silahkan pilih Unit terlebih dahulu</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            } else {
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionApproval()
    {
        $searchModel = new InitiativesSearch();
        $dataProvider = $searchModel->searchForApproval(Yii::$app->request->queryParams);

        return $this->render('approval', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /* public function actionApprovalAll()
    {
        $searchModel = new InitiativesSearch();
        $dataProvider = $searchModel->searchForApproval2(Yii::$app->request->queryParams);
        // $bsc_year = Bsc::find()->where(['company_id'=>Yii::$app->user->identity->company_id])->orderBy('year desc')->all();
        // $yearItems = ArrayHelper::map($bsc_year, 'year','year');

        return $this->render('approval_all', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            // 'yearItems' => $yearItems,
        ]);
    } */

    public function actionApprovalAll()
    {
        $searchModel = new OkrSearch();
        $dataProvider = $searchModel->searchForInitiatives(Yii::$app->request->queryParams);

        if(Yii::$app->user->can('Admin') || Yii::$app->user->can('BOD')){
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        } else {
            $userID = Yii::$app->user->id;
            $dept = [];
            $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => $userID])->all();
            foreach($employeePositions as $ep){
                array_push($dept, $ep->position->unit_structure_id);
            }
            $listUnit = ArrayHelper::map(UnitStructure::find()->where(['in', 'id', $dept])->andWhere(['year' => $searchModel->year])->orderBy('unit_code')->all(), 'id', function($model){
                return $model->unit_code . ' - ' . $model->unit_name;
            });
        }
        
        return $this->render('approve-all', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnit' => $listUnit,
        ]);
    }

    public function actionApprove($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "<b>Approve Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('<i class="glyphicon glyphicon-eye-open"></i>&nbsp; View Proker',['view','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']).
                                Html::button('<i class="glyphicon glyphicon-ok"></i>&nbsp; Approve',['class'=>'btn btn-success','type'=>"submit"])
                ];         
            }else if($model->load($request->post())) {
                $comment = $_POST['Initiatives']['comment'];
                $action = 'Approved';
                $model->status_id = Status::APPROVED;
                if($model->save(false)){
                    //simpan history initiatives
                    $this->history($model->id, $action, $comment);
                    //update status activity & simpan history
                    $find_activity = Activity::find()->where(['initiatives_id'=>$id])->all();
                    foreach($find_activity as $activity){
                        if($activity->status_id == Status::SEND) {
                            $activity->status_id = Status::APPROVED;
                            if($activity->save(false)){
								$this->activityHistory($activity->id, $action, $comment);

                                //update status budget & simpan history
                                $find_budget = Budget::find()->where(['activity_id'=>$activity->id])->all();
                                foreach ($find_budget as $budget) {
                                    if($budget->status_id == Status::SEND) {
                                        $budget->status_id = Status::APPROVED;
                                        if($budget->save(false))
                                            $this->budgetHistory($budget->id, $action, $comment);
                                    }
                                }
                            }
                        }
                    }
                    //kirim email notif

                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "<b>Proker #".$model->initiatives_code.' Approved</b>',
                        'content'=>'<span class="text-success">RKA Proker #'.$model->initiatives_code.' telah sukses di-approve</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                } else {
                    return [
                        'title'=> 'Info',
                        'content'=>'Error: ' . json_encode($model->errors),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                }
            }else{
                 return [
                    'title'=> "<b>Approve Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Approve',['class'=>'btn btn-success','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form_approval', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionRevision($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "<b>Revisi Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('<i class="glyphicon glyphicon-eye-open"></i>&nbsp; View Proker',['view','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']).
                                Html::button('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Revisi',['class'=>'btn btn-warning','type'=>"submit"])
                ];         
            }else if($model->load($request->post())) {
                $comment = $_POST['Initiatives']['comment'];
                $action = 'Need Revision';
                $model->status_id = Status::REVISION;
                if($model->save(false)){
                    //simpan history initiatives
                    $this->history($model->id, $action, $comment);

                    //update status activity & simpan history
                    $find_activity = Activity::find()->where(['initiatives_id'=>$id])->all();
                    foreach($find_activity as $activity){
                        if($activity->status_id == Status::SEND) {
                            $activity->status_id = Status::REVISION;
                            if($activity->save(false)){
								$this->activityHistory($activity->id, $action, $comment);

                                //update status budget & simpan history
                                $find_budget = Budget::find()->where(['activity_id'=>$activity->id])->all();
                                foreach ($find_budget as $budget) {
                                    if($budget->status_id == Status::SEND) {
                                        $budget->status_id = Status::REVISION;
                                        if($budget->save(false))
                                            $this->budgetHistory($budget->id, $action, $comment);
                                    }
                                }
                            }
                        }
                    }
                    //kirim email notif

                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "<b>Proker #".$model->initiatives_code.'</b>',
                        'content'=>'<span class="text-success">Permintaan Revisi RKA Proker #'.$model->initiatives_code.' telah sukses dikirim</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                }
            }else{
                 return [
                    'title'=> "<b>Revisi Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Revisi',['class'=>'btn btn-warning','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form_approval', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionReject($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "<b>Reject Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('<i class="glyphicon glyphicon-eye-open"></i>&nbsp; View Proker',['view','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']).
                                Html::button('<i class="glyphicon glyphicon-remove"></i>&nbsp; Reject',['class'=>'btn btn-danger','type'=>"submit"])
                ];         
            }else if($model->load($request->post())) {
                $comment = $_POST['Initiatives']['comment'];
                $action = 'Rejected';
                $model->status_id = Status::REJECTED;
                if($model->save(false)){
                    //simpan history initiatives
                    $this->history($model->id, $action, $comment);

                    //update status activity & simpan history
                    $find_activity = Activity::find()->where(['initiatives_id'=>$id])->all();
                    foreach($find_activity as $activity){
                        if($activity->status_id == Status::SEND) {
                            $activity->status_id = Status::REJECTED;
                            if($activity->save(false)){
								$this->activityHistory($activity->id, $action, $comment);

                                //update status budget & simpan history
                                $find_budget = Budget::find()->where(['activity_id'=>$activity->id])->all();
                                foreach ($find_budget as $budget) {
                                    if($budget->status_id == Status::SEND) {
                                        $budget->status_id = Status::REJECTED;
                                        if($budget->save())
                                            $this->budgetHistory($budget->id, $action, $comment);
                                    }
                                }
                            }
                        }
                    }
                    //kirim email notif

                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "<b>Proker #".$model->initiatives_code.' Rejected</b>',
                        'content'=>'<span class="text-success">RKA Proker #'.$model->initiatives_code.' telah sukses di-reject</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                }
            }else{
                 return [
                    'title'=> "<b>Reject Program Kerja/Proker #".$model->initiatives_code.'</b>',
                    'content'=>$this->renderAjax('_form_approval', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Reject',['class'=>'btn btn-danger','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form_approval', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionBulkApprove()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        $comment = $request->post( 'confirm-comment' );
        $action = 'Approved';
        foreach ( $pks as $pk ) {
            // $model = $this->findModel($pk);
            $model = ModelsInitiatives::findOne($pk);
            $model->status_id = Status::APPROVED;
            if ($model->save(false)) {
                //simpan history initiatives
                $this->history($model->id, $action, $comment);                
            } else {
                var_dump($model->errors);die;
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionBulkRevision()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        $comment = $request->post( 'confirm-comment' );
        $action = 'Need Revision';
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->status_id = Status::REVISION;//var_dump($model->save());var_dump($model->errors);die();
            if ($model->save(false)) {
                //simpan history initiatives
                $this->history($model->id, $action, $comment);

                //update status activity & simpan history
                $find_activity = Activity::find()->where(['initiatives_id'=>$model->id])->all();
                foreach($find_activity as $activity){
                    if($activity->status_id == Status::SEND) {
                        $activity->status_id = Status::REVISION;
                        if($activity->save(false)){
							$this->activityHistory($activity->id, $action, $comment);


                            //update status budget & simpan history
                            $find_budget = Budget::find()->where(['activity_id'=>$activity->id])->all();
                            foreach ($find_budget as $budget) {
                                if($budget->status_id == Status::SEND) {
                                    $budget->status_id = Status::REVISION;
                                    if($budget->save(false))
                                        $this->budgetHistory($budget->id, $action, $comment);
                                }
                            }
                        }
                    }
                }
                //kirim email notif
                // \app\components\EmailNotification::approvalStatusToCreator($model, $oldStatusId, $doingApprovalStatus, $comment);
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    public function actionBulkReject()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        $comment = $request->post( 'confirm-comment' );
        $action = 'Rejected';
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->status_id = Status::REJECTED;
            if ($model->save(false)) {
                //simpan history initiatives
                $this->history($model->id, $action, $comment);

                //update status activity & simpan history
                $find_activity = Activity::find()->where(['initiatives_id'=>$model->id])->all();
                foreach($find_activity as $activity){
                    if($activity->status_id == Status::SEND) {
                        $activity->status_id = Status::REJECTED;
                        if($activity->save(false)){
							$this->activityHistory($activity->id, $action, $comment);

                            //update status budget & simpan history
                            $find_budget = Budget::find()->where(['activity_id'=>$activity->id])->all();
                            foreach ($find_budget as $budget) {
                                if($budget->status_id == Status::SEND) {
                                    $budget->status_id = Status::REJECTED;
                                    if($budget->save(false))
                                        $this->budgetHistory($budget->id, $action, $comment);
                                }
                            }
                        }
                    }
                }
                //kirim email notif
                
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }
}
