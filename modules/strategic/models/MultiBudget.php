<?php

namespace app\modules\strategic\models;

use app\models\Budget;
use yii\helpers\ArrayHelper;

class MultiBudget extends Budget
{
	public $months;

	public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'months' => 'Bulan',
        ]);
    }

    public function rules()
    {
    	return ArrayHelper::merge(parent::rules(), [
            ['months', 'required'],
            ['months', 'each', 'rule' => ['string', 'length' => 2]], // 01, 02, ... 12
    		['months', 'safe'],
    	]);
    }

    public function getMonths()
    {
    	return [
    		'01' => 'jan',
    		'02' => 'feb',
    		'03' => 'mar',
    		'04' => 'apr',
    		'05' => 'may',
    		'06' => 'jun',
    		'07' => 'jul',
    		'08' => 'aug',
    		'09' => 'sep',
    		'10' => 'oct',
    		'11' => 'nov',
    		'12' => 'dec',
    	];
    }

    public function beforeValidate()
    {
        $this->beforeValidateInternal();

        return parent::beforeValidate();
    }

    private function beforeValidateInternal()
    {
        if (!is_array($this->months)) {
            $this->months = [];
        }

        $this->setTotalAmount();

        $monthlyBudget = $this->getMonthlyAverageBudgetByTotalAmount();

        foreach ($this->months as $month) {
            $monthAttr = "month_{$month}";
            $this->{$monthAttr} = $monthlyBudget;
        }
    }

    public function getMonthlyAverageBudgetByTotalAmount()
    {
        if (empty($this->total_amount) || empty($this->months)) {
            return 0;
        }

        return $this->total_amount / count($this->months);
    }
}