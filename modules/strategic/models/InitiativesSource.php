<?php

namespace app\modules\rkat\models;

use Yii;
use app\models\InitiativesSource as BaseInitiativesSource;
use app\models\InitiativesSourceBeneficiary as SourBene;
use yii\helpers\ArrayHelper;
use app\components\db\TransactionTrait;

/**
 * This is the model class for table "bsc.initiatives__source".
 */
class InitiativesSource extends BaseInitiativesSource
{
    use TransactionTrait;

    public $asnaf;

    public function init()
    {
        parent::init();
        
        $this->saveJoin = true;
    }

    public function rules()
    {
        $rules = [
            [['asnaf', 'source_of_funds_id', 'amount'], 'required'],
            ['asnaf', 'each', 'rule' => ['integer']],
        ];
        
        $parent = parent::rules();
        // remove parent's required
        unset($parent[0]);
        
        return ArrayHelper::merge( $parent, $rules );
    }

    public function saveJoin()
    {
        $saved = true;
        if (empty($this->asnaf)) {
            SourBene::deleteAll(['initiatives__source_id' => $this->id]);
        }
        else {
            $existingAsnafIds = SourBene::find()
                ->select('beneficiary_type_id')
                ->where(['initiatives__source_id' => $this->id])
                ->asArray()
                ->column();
            $asnafIdsToDelete = array_diff($existingAsnafIds, $this->asnaf);
            SourBene::deleteAll([
                'initiatives__source_id' => $this->id,
                'beneficiary_type_id' => $asnafIdsToDelete,
            ]);
            $asnafIdsToSave = array_diff($this->asnaf, $existingAsnafIds);
            foreach ($asnafIdsToSave as $asnafId) {
                // Initiatives Source Beneficiary
                $sourBene = new SourBene();
                $sourBene->initiatives__source_id = $this->id;
                $sourBene->beneficiary_type_id = $asnafId;
                if ( ! $sourBene->save( $this->runValidation ) ) {
                    if ( $sourBene->hasErrors() ) {
                        foreach ($sourBene->getErrors() as $attribute => $errors) {
                            foreach ($errors as $error) {
                                $this->addError('asnaf', $error);
                            }
                        }
                    } else {
                        $this->addError('asnaf', "Asnaf has something error[s].");
                    }
                    $saved = false;
                    break;
                }
            }
        }
        return $saved;
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->loadBeneficiaries();
    }

    public function loadBeneficiaries()
    {
        $this->asnaf = [];
        foreach ($this->beneficiaries as $ben) {
            if (!in_array($ben->beneficiary_type_id, $this->asnaf)) {
                $this->asnaf[] = $ben->beneficiary_type_id;
            }
        }
    }

    public function getBeneficiaries()
    {
        return $this->hasMany(\app\models\InitiativesSourceBeneficiary::className(), ['initiatives__source_id' => 'id']);
    }
}
