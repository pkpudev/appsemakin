<?php

namespace app\modules\strategic\models;

use yii\base\Model;
use app\modules\strategic\models\MultiBudget;
use \app\models\Status;
use Yii;

class MultiBudgetForm extends Model
{
    public $initiatives_id;
	public $activity_id;
	public $verify_total_amount;

    public $budgetsPostData;
    public $budgets = [];

	public function attributeLabels()
    {
        return [
            'initiatives_id' => 'Program Kerja',
            'activity_id' => 'Kegiatan',
            'verify_total_amount' => 'Total Anggaran',
        ];
    }

    public function rules()
    {
    	return [
    		// [['activity_id'], 'required'],
            [['initiatives_id', 'activity_id'], 'integer'],
    		[['verify_total_amount'], 'safe'],
    	];
    }

    public function beforeValidate()
    {
        if (!parent::beforeValidate()){
            return false;
        }

        if ($this->activity_id=="") {
            $this->activity_id = null;
        }

        return true;
    }

    public function validate ( $attributeNames = null, $clearErrors = true )
    {
        $isValid = parent::validate($attributeNames, $clearErrors);
        $isValidInternal = $this->validateInternal();

        return $isValid && $isValidInternal;
    }

    protected function validateInternal()
    {
        if (is_array($this->budgetsPostData)) {
            $this->verify_total_amount = 0;

            $isValid = true;
            $availableBudget = null;
            foreach ($this->budgetsPostData as $key => $budgetData) {
                $budget = new MultiBudget();
                $budget->attributes = $budgetData;
                $budget->months = @$budgetData['months'];
                // $budget->strategic_initiatives_id = $this->strategic_initiatives_id;
                $budget->activity_id = $this->activity_id;
                // $budget->initiatives_id = @$budgetData['initiatives_id'];
                // var_dump($budget->initiatives_id);die;

                if (is_null($availableBudget) /* && !empty($budget->strategic_initiatives_id) */) {
                    // validasi dari initiative->total_budget_plan
                    // $availableBudget = $this->getAvailableBudget($budget->initiative);
                    // validasi dari plafon divisi
                    $availableBudget = $this->getAvailableCeilingBalance($budget->initiatives);
                }

                if (!$budget->validate()) {
                    $isValid = false;
                }

                $this->verify_total_amount += $budget->total_amount;
                $this->budgets[] = $budget;
            }

            // var_dump($this->verify_total_amount);
            // var_dump($availableBudget);
            // die;

            if ($this->verify_total_amount > $availableBudget) {
                $isValid = false;
                $balance = 'Rp. ' . number_format($availableBudget, 2, ",", ".");

                $this->addError('verify_total_amount', 'Total Anggaran melebihi Sisa Saldo. Saldo Pagu Anggaran = ' . $balance);
            }

            return $isValid;
        }

        $this->addError('*', "Rincian anggaran belum diisi.");
        return false;
    }

    private function getAvailableBudget($initiative)
    {
        $totalBudgetPlan = (float) (@$initiative->total_budget_plan ?: 0);
        $totalBudgetDetailed = (float) (@$initiative->total_budget ?: 0);
        return $totalBudgetPlan - $totalBudgetDetailed;
    }

    public function getAvailableCeilingBalance($initiative)
    {
        if (@$initiative->okr_id) {
            $okr = @$initiative->okr;
            $positionStructureId = $okr->position_structure_id;
            $unitStructureId = $okr->position->unit_structure_id;
            $year = $okr->year;

            $ceiling = \app\models\Ceiling::find()->where([
                'position_structure_id'    => $positionStructureId,
                'unit_structure_id'    => $unitStructureId,
                'year'              => $year,
            ])->one();
            if ($ceiling)
                $pagu = (float) $ceiling->ceiling_amount;
            else
                $pagu = 0;

            $terpakai = (float) (
                (new \yii\db\Query())
                ->select(["SUM(COALESCE(total_budget, 0))"])
                ->from('management.initiatives i')
                ->leftJoin('management.okr o', 'o.id = i.okr_id')
                ->leftJoin('management.position_structure p', 'p.id = o.position_structure_id')
                ->where([
                    'o.year' => $year,
                    'o.position_structure_id' => $positionStructureId,
                    'p.unit_structure_id' => $unitStructureId,
                ])
                ->andWhere(['not in', 'i.status_id', [Status::REJECTED, Status::DELETED]])
                ->scalar());

                /* if(Yii::$app->user->id == 487497){
                    var_dump($unitStructureId);
                    var_dump($year);
                    var_dump($pagu);
                    var_dump($terpakai);
                    var_dump((new \yii\db\Query())
                    ->select(["*"])
                    ->from('management.initiatives i')
                    ->leftJoin('management.okr o', 'o.id = i.okr_id')
                    ->leftJoin('management.position_structure p', 'p.id = o.position_structure_id')
                    ->where([
                        'o.year' => $year,
                        'p.unit_structure_id' => $unitStructureId,
                    ])
                    ->andWhere(['not in', 'i.status_id', [Status::REJECTED, Status::DELETED]])
                    ->createCommand()->getRawSql());
                    die;
                } */
            
            return $balance = $pagu - $terpakai;
        }
    }

    public function save($runValidation = true)
    {
        if ($runValidation) {
            if (!$this->validate()) {
                return false;
            }
        }

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            foreach ($this->budgets as $budget) {
                if($budget->save(false))
                {
                    $history=new \app\models\BudgetHistory;
                    $history->budget_id = $budget->id;
                    $history->action = 'Input Draft';
                    $history->save();
                }
            }
            $transaction->commit();

            return true;

        } catch (\Exception $e) {
            $transaction->rollBack();
            $this->addError("*", $e->getMessage());

            return false;
        }
    }
}