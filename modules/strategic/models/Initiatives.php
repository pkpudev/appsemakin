<?php

namespace app\modules\strategic\models;

use Yii;
use app\models\Initiatives as BaseInitiatives;
use app\models\InitiativesInvolvement as Involve;
use app\models\VwDepartments as Dept;
use app\models\Measure;
use app\models\Controllability as Control;
use app\models\Ceiling;
use app\models\Status;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\components\db\TransactionTrait;
use yii\base\InvalidConfigException;

/**
 * ======================================================================
 * PERHATIAN!!!
 * ...
 * Class ini dibuat untuk module RKAT controller INITIATIVES create/update
 * 
 * ======================================================================
 * This is the model class for table "bsc.strategic_initiatives".
 */
class Initiatives extends BaseInitiatives
{
    use TransactionTrait;

    // bulan-bulan pelaksanaan
    public $allmon; // sepanjang tahun, jan-des
    public $jan;
    public $feb;
    public $mar;
    public $apr;
    public $may;
    public $jun;
    public $jul;
    public $aug;
    public $sep;
    public $oct;
    public $nov;
    public $dec;
    public $q1;
    public $q2;
    public $q3;
    public $q4;

    public $involvement_dept_ids = [];
    public $monitoring_tool_vals;
    public $objectives;
    // InitiativesSource Models
    public $budgets = [];
    // Delete InitiativesSources by IDs
    public $deleted_source_ids;
    // Delete InvolvementInitiatives by IDs
    public $deleted_dept_ids;

    public $old_total_budget_plan;
    public $verify_total_budget_plan;
    public $companyId;
    public $year;

    private $involvement_depts = [];

    private static $_depts = [];

    public function init()
    {
        parent::init();

        $this->saveJoin = true;
    }

    public function attributeLabels()
    {
        $labels = [
            'allmon' => 'All',
            'jan' => 'Jan',
            'feb' => 'Feb',
            'mar' => 'Mar',
            'apr' => 'Apr',
            'may' => 'Mei',
            'jun' => 'Jun',
            'jul' => 'Jul',
            'aug' => 'Agu',
            'sep' => 'Sep',
            'oct' => 'Okt',
            'nov' => 'Nov',
            'dec' => 'Des',
            'target_q1' => 'K1',
            'target_q2' => 'K2',
            'target_q3' => 'K3',
            'target_q4' => 'K4',
            'involvement_dept_ids' => 'Pemangku Kepentingan',
            'involvement_all_depts' => 'Semua Divisi',
            'monitoring_tool_vals' => 'Media Monitoring',
            'objectives' => 'Sasaran',
            'verify_total_budget_plan' => 'Total Alokasi Anggaran',
        ];

        return ArrayHelper::merge(parent::attributeLabels(), $labels);
    }

    public function implementationAttributes()
    {
        return ['allmon', 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
    }

    public function rules()
    {
        $quarterChecked = function ($model, $attribute) {
            $qAttr = substr($attribute, 7);
            return (bool) $model->$qAttr;
        };
        $isTargetEmpty = function ($value) {
            return empty($value);
        };

        $rules = [
            // init
            [$this->implementationAttributes(), 'boolean'],
            [($this->implementationAttributes() + ['involvement_all_depts']), 'default', 'value' => false],
            [($this->implementationAttributes() + ['involvement_all_depts']), 'filter', 'filter' => 'boolval'],
            ['allmon', 'checkMonthsImplemetation'],
            ['involvement_dept_ids', 'each', 'rule' => ['integer']],
            ['monitoring_tool_vals', 'each', 'rule' => ['string']],
            ['objectives', 'each', 'rule' => ['string']],
            ['verify_total_budget_plan', 'number'],
            // required
            [[ /*'initiatives', 'monitoring_tool_vals'*/ 'monitoring_tools', /*'pic_text'*/], 'required'],
            ['allmon', 'implementationRequired'],
            // [['target_q1', 'target_q2', 'target_q3', 'target_q4'], 'required', 'when' => $quarterChecked, 'isEmpty' => $isTargetEmpty],
            // ['involvement_all_depts', 'involvementDeptRequired'],

            // -------------------------------------------------------------------------------------------------------
            // Bila 'memiliki anggaran'
            // -------------------------------------------------------------------------------------------------------
            // 'total perencanaan anggaran' wajib diisi
            // ['total_budget_plan', 'required', 'when' => $hasBudget],
            // // validasi alokasi anggaran dan sumber dana (INITIATIVES SOURCES)
            // // total budget tidak boleh melebihi pagu anggaran
            // ['total_budget_plan', 'validatePagu', 'when' => $hasBudget],
            // // menjumlahkan total alokasi anggaran
            // [['total_budget_plan'], 'totalBudgets', 'when' => $hasBudget],
            // // pastikan 'total perencanaan anggaran' = 'total alokasi anggaran'
            // ['total_budget_plan', 'compare', 'compareAttribute' => 'verify_total_budget_plan', 'operator' => '==', 'type' => 'number', 'when' => $hasBudget],
            // -------------------------------------------------------------------------------------------------------
            // Bila TIDAK 'memiliki anggaran'
            // -------------------------------------------------------------------------------------------------------
            // 'total perencanaan anggaran' wajib diisi
        ];

        return ArrayHelper::merge(parent::rules(), $rules);
    }

    public function checkMonthsImplemetation($attribute, $params, $validator)
    {
        if ($this->allmon) {
            // jika 'all' dicentang, semua bulan tercentang
            $this->jan = true;
            $this->feb = true;
            $this->mar = true;
            $this->apr = true;
            $this->may = true;
            $this->jun = true;
            $this->jul = true;
            $this->aug = true;
            $this->sep = true;
            $this->oct = true;
            $this->nov = true;
            $this->dec = true;
        } else {
            // jika salah satu bulan unchecked, 'all' bulan juga unchecked
            $count = 12;
            $countMon = 0;
            foreach ($this->implementationAttributes() as $attr) {
                if ($attr == 'allmon') {
                    continue;
                }
                if ($this->$attr == true) {
                    $countMon++;
                }
            }
            if ($count == $countMon) {
                $this->allmon = true;
            }
        }

        // isi quartal
        $this->q1 = ($this->jan || $this->feb || $this->mar);
        $this->q2 = ($this->apr || $this->may || $this->jun);
        $this->q3 = ($this->jul || $this->aug || $this->sep);
        $this->q4 = ($this->oct || $this->nov || $this->dec);
    }

    public function implementationRequired($attribute, $params, $validator)
    {
        $attributes = $this->implementationAttributes();
        $count = count($attributes);
        $emptyCount = 0;

        foreach ($attributes as $attr) {
            if (empty($this->$attr)) {
                $emptyCount++;
            } else {
                return;
            }
        }

        if ($emptyCount == $count) {
            $this->addError('implementation', 'Wajib dipilih bulan pelaksanaannya.');
        }
    }


    public function involvementDeptRequired($attribute, $params, $validator)
    {
        if ($this->involvement_all_depts == false && empty($this->involvement_dept_ids)) {
            $this->addError('involvement_dept_ids', "{$this->getAttributeLabel('involvement_dept_ids')} wajib diisi.");
        }
    }

    // public function validateBudgets($attribute, $params, $validator)
    // {
    //     if (!Model::validateMultiple($this->budgets)) {
    //         $this->addError('has_budget', "Cek lagi di alokasi anggaran dan sumber dana.");
    //     }
    // }

    public function ifHasNotBudget($attribute, $params, $validator)
    {
        $this->total_budget_plan = null;
        $this->budgets = [];
    }
/* 
    public function validatePagu($attribute, $params, $validator)
    {
        if ($this->kpi_distribution_id) {
            $kpiDistribution = $this->kpiDistribution;
            $departmentId = $kpiDistribution->departments_id;
            $objective = $kpiDistribution->kpi->objective;
            $bscId = $objective->bsc_id;
            $year = $objective->bsc->year;
            $ceiling = Ceiling::find()->where([
                'departments_id'    => $departmentId,
                'year'              => $year,
            ])->one();
            if ($ceiling)
                $pagu = (float) $ceiling->ceiling_amount;
            else
                $pagu = 0;

            $terpakai = (float) (
                (new \yii\db\Query())
                ->select(["SUM(COALESCE(total_budget_plan, 0))"])
                ->from('bsc.strategic_initiatives i')
                ->leftJoin('bsc.kpi_distribution kd', 'kd.id = i.kpi_distribution_id')
                ->leftJoin('bsc.kpi k', 'k.id = kd.kpi_id')
                ->leftJoin('bsc.strategic_objectives so', 'so.id = k.strategic_objectives_id')
                ->where([
                    'so.bsc_id' => $bscId,
                    'kd.departments_id' => $departmentId,
                    'k.status_id' => Status::APPROVED,
                ])
                ->andWhere(['not in', 'i.status_id', [Status::REJECTED, Status::DELETED]])
                ->scalar());

            $balance = $pagu - $terpakai;
            $balance = 'Rp. ' . number_format($balance, 0, ",", ".");
            if ($this->isNewRecord) {
                $terpakai += (float) $this->total_budget_plan;
            } else {
                $terpakai = $terpakai - ((float) $this->old_total_budget_plan) + ((float) $this->total_budget_plan);
            }

            if ($terpakai > $pagu) {
                $this->addError('total_budget_plan', "Pagu Anggaran tidak mencukupi. Saldo Pagu Anggaran = {$balance}.");
                // $this->addError('total_budget_plan', "Jumlah Anggaran {$terpakai} > Pagu Anggaran {$pagu}.");
            }
        }
    } */

    public function totalBudgets($attribute, $params, $validator)
    {
        $budgetTotal = 0;
        foreach ($this->budgets as $budget) {
            $budgetTotal += (float) $budget->amount;
        }
        $this->verify_total_budget_plan = $budgetTotal;
    }

    public function validateTotalBudgetPlan($attribute, $params, $validator)
    {
        $total = 0;
        foreach ($this->budgets as $fundSource) {
            $total += (float) $fundSource->amount;
        }
        if ($total != (float) $this->total_budget_plan) {
            $this->addError('total_budget_plan', "Jumlah Anggaran tidak sama dengan Jumlah Alokasi Anggaran.");
            $this->addError('verify_total_budget_plan', "Alokasi Anggaran belum balance.");
            foreach ($this->budgets as $key => $fundSource) {
                $fundSource->addError('amount', 'Anggaran #' . ($key + 1) . ' belum balance.');
            }
        }
    }

    public function getMetadataValues()
    {
        return [
            'implementation' => [
                'year' => $this->year,
                'all' => $this->allmon,
                'jan' => $this->jan,
                'feb' => $this->feb,
                'mar' => $this->mar,
                'apr' => $this->apr,
                'may' => $this->may,
                'jun' => $this->jun,
                'jul' => $this->jul,
                'aug' => $this->aug,
                'sep' => $this->sep,
                'oct' => $this->oct,
                'nov' => $this->nov,
                'dec' => $this->dec,
                'q1' => $this->q1,
                'q2' => $this->q2,
                'q3' => $this->q3,
                'q4' => $this->q4,
            ],
        ];
    }

    public function encodeMetadataToJson($metadata = null)
    {
        return Json::encode(((is_array($metadata) && !empty($metadata)) ? $metadata : $this->getMetadataValues()));
    }

    public function setMetadataValues($metadata = null)
    {
        $this->metadata = $this->encodeMetadataToJson($metadata);
    }

    public function decodeMetadataToPhp()
    {
        if (empty($this->metadata)) {
            $decoded = [];
        } else {
            $decoded = Json::decode($this->metadata);
        }
        return $decoded;
    }

    public function metadataToModel()
    {
        $metadata = $this->decodeMetadataToPhp();
        if (isset($metadata['implementation'])) {
            $imple = $metadata['implementation'];
            $this->year = @$imple['year'];
            $this->allmon = @$imple['all'] ?: false;
            $this->jan = @$imple['jan'] ?: false;
            $this->feb = @$imple['feb'] ?: false;
            $this->mar = @$imple['mar'] ?: false;
            $this->apr = @$imple['apr'] ?: false;
            $this->may = @$imple['may'] ?: false;
            $this->jun = @$imple['jun'] ?: false;
            $this->jul = @$imple['jul'] ?: false;
            $this->aug = @$imple['aug'] ?: false;
            $this->sep = @$imple['sep'] ?: false;
            $this->oct = @$imple['oct'] ?: false;
            $this->nov = @$imple['nov'] ?: false;
            $this->dec = @$imple['dec'] ?: false;
            $this->q1 = @$imple['q1'] ?: false;
            $this->q2 = @$imple['q2'] ?: false;
            $this->q3 = @$imple['q3'] ?: false;
            $this->q4 = @$imple['q4'] ?: false;
        }
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->metadataToModel();
        $this->checkYear();
        // $this->loadInvolvementDepartments();
        // $this->loadObjectivesValues();
        $this->loadMonitoringToolValues();
        // $this->budgets = $this->budgetSources;
        // $this->old_total_budget_plan = $this->total_budget_plan;
    }

    // public function loadInvolvementDepartments()
    // {
    //     if (!$this->involvement_all_depts) {
    //         $this->involvement_dept_ids = [];
    //         $gets = Involve::find()->where(['initiatives_id' => $this->id])->all();
    //         foreach ($gets as $get) {
    //             $this->involvement_dept_ids[] = $get->departments_id;
    //         }
    //         $this->involvement_depts = $gets;
    //     }
    // }

    // public function loadObjectivesValues()
    // {
    //     $values = explode("; ", $this->objective);
    //     foreach ($values as $value) {
    //         $value = trim($value);
    //     }
    //     $this->objectives = $values;
    // }

    public function loadMonitoringToolValues()
    {
        $values = explode(",", $this->monitoring_tools);
        foreach ($values as $value) {
            $value = trim($value);
        }
        $this->monitoring_tool_vals = $values;
    }

    private function checkYear()
    {
        if (empty($this->year)) {
            $this->year = @$this->okr->year;
        }
    }

    /* private function checkCompany()
    {
        if (empty($this->companyId)) {
            $this->companyId = @$this->kpiDistribution->kpi->objective->bsc->company_id; //@$this->kpiDistribution->vwDepartment->company_id;
        }
    } */

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->checkConfigInsertData();
            }
            
            $this->setImplementationValue();
            $this->setMetadataValues($this->metadata);
            
            // $this->setMonitoringToolsValue();  tidak aktif karena media monitoring tdk menggunakan multi select 

            return true;
        }
        return false;
    }

    protected function checkConfigInsertData()
    {
        if (!isset($this->companyId)) {
            throw new InvalidConfigException("CompanyId is not set.");
        }
        if (!isset($this->year)) {
            throw new InvalidConfigException("Year is not set.");
        }
    }

    public function setMonitoringToolsValue()
    {
        $value = implode(', ', $this->monitoring_tool_vals);

        $this->monitoring_tools = $value;
    }


    public function getImplementationValue()
    {
        if ($this->allmon) {
            return "{$this->getAttributeLabel("jan")}-{$this->getAttributeLabel("dec")} " . $this->year;
        }

        $months = [1 => 'jan', 2 => 'feb', 3 => 'mar', 4 => 'apr', 5 => 'may', 6 => 'jun', 7 => 'jul', 8 => 'aug', 9 => 'sep', 10 => 'oct', 11 => 'nov', 12 => 'dec'];
        $monthsName = $filledMonths = [];
        for ($i = 1; $i <= 12; $i++) {
            $monthsName[$i] = $this->getAttributeLabel($months[$i]);
            $filledMonths[$i] = $this->{$months[$i]};
        }
        $result = null;
        $lastMonth = null;
        // var_dump($filledMonths);
        for ($i = 1; $i <= 12; $i++) {
            // var_dump($i > 1 && $filledMonths[$i - 1] === false);
            if ($filledMonths[$i] == 1) {
                //echo "i = $i terisi<br />";
                if (empty($result)) {
                    $result = $monthsName[$i];
                } elseif ($i > 1 && $filledMonths[$i - 1] != 1) {
                    $result .= ', ' . $monthsName[$i];
                } elseif ($i == 12) {
                    if ($filledMonths[$i - 1] == 1) {
                        if ($filledMonths[$i - 2] == 1) {
                            $result .= '-' . $monthsName[$i];
                        } else {
                            $result .= ', ' . $monthsName[$i];
                        }
                    }
                } else {
                    $lastMonth = $monthsName[$i];
                }
                //echo "hasil: $result<br /><br />";
            } else {
                //echo "i = $i tidak terisi<br />";
                if ($lastMonth && $i > 2 && $filledMonths[$i - 2] == 1) {
                    if ($i > 3 && $filledMonths[$i - 3] == 1) {
                        $result .= '-' . $lastMonth;
                    } else {
                        $result .= ', ' . $lastMonth;
                    }
                }
                $lastMonth = null;
                //echo "hasil: $result<br /><br />";
            }
        }
        // die;

        return $result . " tahun " . $this->year;
    }

    public function setImplementationValue($value = null)
    {
        $this->implementation = $value ?: $this->getImplementationValue();
    }

    public function saveJoin()
    {
        return 1;
    }

}
