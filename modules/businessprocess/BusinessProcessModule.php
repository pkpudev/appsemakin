<?php

namespace app\modules\businessprocess;

/**
 * businessprocess module definition class
 */
class BusinessProcessModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\businessprocess\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
