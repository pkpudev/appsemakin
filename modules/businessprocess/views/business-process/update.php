<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BusinessProcess */
?>
<div class="business-process-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
