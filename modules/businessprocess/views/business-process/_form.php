<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BusinessProcess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="business-process-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'period_start')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'period_end')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
