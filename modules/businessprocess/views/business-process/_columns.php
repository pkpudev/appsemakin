<?php

use app\models\UnitStructure;
use kartik\grid\GridView;
use pkpudev\components\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'level',
        'value' => function($model){
            return $model->level ? 'Level ' . $model->level : '-';
        },
        'width' => '50px'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'document_type_id',
        'value' => function($model){
            return $model->document_type_id ? $model->type->document_type : '-';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(app\models\DocumentType::find()->orderBy('document_type')->asArray()->all(), 'id', 'document_type'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'document_no',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'title',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'file',
        'format' => 'raw',
        'value' => function($model){

            if(in_array($model->file->ext, ['doc', 'docx', 'DOC', 'DOCX'])){
                $faFile = 'file-word-o';
            } else if(in_array($model->file->ext, ['xls', 'xlsx', 'XLS', 'XLSX'])){
                $faFile = 'file-excel-o';
            } else if(in_array($model->file->ext, ['ppt', 'pptx', 'PPT', 'PPTX'])){
                $faFile = 'file-powerpoint-o';
            } else if(in_array($model->file->ext, ['pdf', 'PDF'])){
                $faFile = 'file-pdf-o';
            } else {
                $faFile = 'file-o';
            }

            if(in_array($model->editableFile->ext, ['doc', 'docx', 'DOC', 'DOCX'])){
                $faEditableFile = 'file-word-o';
            } else if(in_array($model->editableFile->ext, ['xls', 'xlsx', 'XLS', 'XLSX'])){
                $faEditableFile = 'file-excel-o';
            } else if(in_array($model->editableFile->ext, ['ppt', 'pptx', 'PPT', 'PPTX'])){
                $faEditableFile = 'file-powerpoint-o';
            } else if(in_array($model->editableFile->ext, ['pdf', 'PDF'])){
                $faEditableFile = 'file-pdf-o';
            } else {
                $faEditableFile = 'file-o';
            }

            return 
                    '<table width="100%">' .
                    '<tr>' .
                        '<td width="30px" align="center">' .
                            (($model->files_id) ?
                            '<span class="fa fa-' . $faFile . '" title="' . $model->file->ext . '"></span>' :
                             '-') .
                        '</td>' .
                        '<td>' .
                            (($model->files_id) ?
                            Html::a($model->file->name, $model->file->storage === 0 ? [$model->file->location] : $model->file->location, ['target' => '_blank', 'data-pjax' => 0]) :
                            '<i>Belum ada file</i>') .
                        '</td>' .
                        '<td width="120px" align="right"><i>Non-editable</i></td>' .
                        (Yii::$app->user->can('Admin') ? 
                            '<td width="30px" align="center">' . 
                                ($model->files_id ? 
                                    Html::a(
                                        '<span class="fa fa-pencil"></span>',
                                        ['document', 'id' => $model->id, 'column' => 'files_id', 'method' => 'add'],
                                        ['title' => 'Ubah Non-editable Dokumen ini', 'role' => 'modal-remote', 'style' => 'color: var(--warning) !important']
                                    ) : 
                                    Html::a(
                                        '<span class="fa fa-plus"></span>',
                                        ['document', 'id' => $model->id, 'column' => 'files_id', 'method' => 'add'],
                                        ['title' => 'Upload Non-editable Dokumen ini', 'role' => 'modal-remote', 'style' => 'color: var(--success) !important']
                                    )
                                 ).
                            '</td>' :
                        '') .
                    '</tr>' .
                    '<tr>' .
                        '<td width="30px" align="center">' .
                            (($model->editable_files_id) ?
                            '<span class="fa fa-' . $faEditableFile . '" title="' . $model->editableFile->ext . '"></span>' :
                            '-') .
                        '</td>' .
                        '<td>' .
                            (($model->editable_files_id) ?
                            Html::a($model->editableFile->name, $model->editableFile->storage === 0 ? [$model->editableFile->location] : $model->editableFile->location, ['target' => '_blank', 'data-pjax' => 0]) :
                            '<i>Belum ada file yang bisa diedit</i>') .
                        '</td>' .
                        '<td width="120px" align="right"><i>Editable</i></td>' .
                        (Yii::$app->user->can('Admin') ? 
                            '<td width="30px" align="center">' . 
                                ($model->editable_files_id ? 
                                    Html::a(
                                        '<span class="fa fa-pencil"></span>',
                                        ['document', 'id' => $model->id, 'column' => 'editable_files_id', 'method' => 'edit'],
                                        ['title' => 'Ubah Editable Dokumen ini', 'role' => 'modal-remote', 'style' => 'color: var(--warning) !important']
                                    ) : 
                                    Html::a(
                                        '<span class="fa fa-plus"></span>',
                                        ['document', 'id' => $model->id, 'column' => 'editable_files_id', 'method' => 'add'],
                                        ['title' => 'Upload Editable Dokumen ini', 'role' => 'modal-remote', 'style' => 'color: var(--success) !important']
                                    )
                                 ).
                            '</td>' :
                        '') .
                    '</tr>' .
                    '</table>';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_structure_id',
        'value' => function($model){
            return $model->unit_structure_id ? $model->unit->unit_name : '-';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(UnitStructure::find()->all(), 'id', function($model){return $model->year . ' - ' . $model->unit_name;}),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
        'width' => '250px'
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },   
        'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<span class="fa fa-eye"></span>',
                    ['view-document', 'id' => $model->id], 
                    [
                        'title' => 'Ubah Dokumen Standar Mutu ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--info) !important'
                    ]
                );
            },
            'update' => function ($url, $model) {
                return Html::a('<span class="fa fa-pencil"></span>',
                    ['update-document', 'id' => $model->id], 
                    [
                        'title' => 'Ubah Dokumen Standar Mutu ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--warning) !important'
                    ]
                );
            },
            'delete' => function ($url, $model) {
                return Html::a('<span class="fa fa-trash"></span>',
                    ['delete-document', 'id' => $model->id], 
                    [
                        'title' => 'Hapus Dokumen Standar Mutu ini',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Konfirmasi',
                        'data-confirm-message'=>'Yakin ingin menghapus Dokumen Standar Mutu ini?',
                        'style' => 'color: var(--danger) !important'
                    ]
                );
            },
        ],
        'visible' => Yii::$app->user->can('Admin'),
        'width' => '90px'
    ],

];
