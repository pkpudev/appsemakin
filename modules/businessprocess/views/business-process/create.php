<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BusinessProcess */

?>
<div class="business-process-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
