<?php

use app\models\DocumentType;
use app\models\UnitStructure;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QualityStandardDocuments */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord) $model->storages = 0;
?>

<div class="quality-standard-documents-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'period_start')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'period_end')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'unit_structure_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(UnitStructure::find()->all(), 'id', function($model){return $model->year . ' - ' . $model->unit_name;}),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => ['prompt' => '-- Pilih Unit Struktur --'],
                    'pluginOptions' => [
                        'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                        ]
                ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'level')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'document_type_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(DocumentType::find()->where(['is_active' => 1])->all(), 'id', 'document_type'),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => ['prompt' => '-- Pilih Tipe Dokumen --'],
                    'pluginOptions' => [
                        'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                        ]
                ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'document_no')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'document_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'version')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'validation_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal Pengesahan'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'implementation_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tanggal Pemberlakuan'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
    </div>

    <?php if($model->isNewRecord): ?>
    <?= $form->field($model, 'storages')->radioList([0 => 'Upload File', 1 => 'Link'], ['style'=>'display:block ruby;'])->label('Tipe'); ?> 

    <div id="files-place">
        <i style="color: var(--danger) !important">Pastikan nama file tidak ada karater pagar (#), petik (', "), atau karakter unik lainya.</i>

        <?= $form->field($model, 'files[]')->widget(FileInput::classname(), [
            'options' => ['multiple' => false],    
            'pluginOptions' => [
                'showPreview' => true,
                'showCaption' => true,
                'showRemove' => false,
                'showUpload' => false
            ]
        ])->label('File'); ?>
    </div>

    <div id="links-place" style="display: none;">
        <i style="color: var(--danger) !important">Pastikan link yang dilampirkan beserta tulisan http/https nya.</i>

        <?= $form->field($model, 'links')->textInput()->label('Link'); ?>
    </div>
    <?php endif; ?>
  
    <?php if (!Yii::$app->request->isAjax){ ?>
          <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<script>
    $('input[type="radio"]').on('click change', function(e) {
        // console.log(this.value);
        if(this.value == 0){
            $('#files-place').show();
            $('#links-place').hide();
        } else {
            $('#files-place').hide();
            $('#links-place').show();
        }
    });
</script>
