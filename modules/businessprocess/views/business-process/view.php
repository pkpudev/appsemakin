<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BusinessProcess */
?>
<div class="business-process-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'period_start',
            'period_end',
            'name',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
        ],
    ]) ?>

</div>
