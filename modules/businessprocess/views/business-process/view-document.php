<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BusinessProcess */
?>
<div class="business-process-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Periode',
                'value' => function($model){
                    return $model->period_start . ' - ' . $model->period_end;
                }
            ],
            [
                'attribute'=>'unit_structure_id',
                'value' => function($model){
                    return $model->unit_structure_id ? $model->unit->unit_name : '-';
                },
            ],
            [
                'attribute' => 'level',
                'value' => function($model){
                    return 'Level ' . $model->level;
                }
            ],
            [
                'attribute' => 'document_type_id',
                'value' => function($model){
                    return $model->document_type_id ? $model->type->document_type : '-';
                }
            ],
            [
                'attribute' => 'document_no',
                'value' => function($model){
                    return $model->document_no ?: '-';
                }
            ],
            [
                'attribute' => 'document_date',
                'value' => function($model){
                    return $model->document_date ?: '-';
                }
            ],
            [
                'attribute' => 'validation_date',
                'value' => function($model){
                    return $model->validation_date ?: '-';
                }
            ],
            [
                'attribute' => 'implementation_date',
                'value' => function($model){
                    return $model->implementation_date ?: '-';
                }
            ],
            [
                'attribute' => 'title',
                'value' => function($model){
                    return $model->title ?: '-';
                }
            ],
            [
                'attribute' => 'version',
                'value' => function($model){
                    return $model->version ?: '-';
                }
            ],
            [
                'attribute' => 'files_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->files_id ? Html::a($model->file->name, $model->file->storage === 0 ? [$model->file->location] : $model->file->location, ['target' => '_blank', 'data-pjax' => 0]) : '-';
                }
            ],
            [
                'attribute' => 'editable_files_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->editable_files_id ? Html::a($model->editableFile->name, $model->editableFile->storage === 0 ? [$model->editableFile->location] : $model->editableFile->location, ['target' => '_blank', 'data-pjax' => 0]) : '-';
                }
            ],
            [
                'attribute'=>'created_by',
                'value' => function($model){
                    return $model->created_by ? $model->createdBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'created_at',
                'value' => function($model){
                    return $model->created_at ?: '-';
                }
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'updated_by',
                'value' => function($model){
                    return $model->updated_by ? $model->updatedBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($model){
                    return $model->updated_at ?: '-';
                }
            ],
        ],
    ]) ?>

</div>
