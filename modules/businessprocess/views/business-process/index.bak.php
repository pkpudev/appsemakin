<?php

use app\models\BusinessProcessFiles;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use yii\widgets\Pjax;

$this->title = 'Proses Bisnis & Dokumen Mutu';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="situation-analysis-index">

    <div class="row">
        <div class="col-md-3">

            <div class="box box-default">
                <div class="box-body">
                    <label>Filter Tahun</label>
                    <table width="100%">
                        <tr>
                            <td width="50%">
                                <?= DatePicker::widget([
                                    'name' => 'year-from',
                                    'value' => $yearFrom,
                                    'options' => [
                                        'placeholder' => 'Dari Tahun',
                                        'onchange' => 'from($(this).val())',
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'startView'=>'year',
                                        'minViewMode'=>'years',
                                        'format' => 'yyyy'
                                    ]
                                ]); ?>
                            </td>
                            <td width="5px"></td>
                            <td>
                                <?= DatePicker::widget([
                                    'name' => 'year-to',
                                    'value' => $yearTo,
                                    'options' => [
                                        'placeholder' => 'Sampai Tahun',
                                        'onchange' => 'to($(this).val())',
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'startView'=>'year',
                                        'minViewMode'=>'years',
                                        'format' => 'yyyy'
                                    ]
                                ]); ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
        <div class="col-md-6"></div>
        <div class="col-md-3">

            <div class="box box-default" style="display: none;">
                <div class="box-body">
                    -- Box Button
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-6">
                            <label>Proses Bisnis (Business Process) Human Initiative</label>

                            <?php if(Yii::$app->user->can('Admin')): ?>
                                <?= Html::a('<i class="fa fa-plus"></i> Tambah Proses Bisnis', ['create'],
                                ['role'=>'modal-remote','title'=> 'Tambah Proses Bisnis','class'=>'btn btn-success btn-xs pull-right']); ?>
                            <?php endif; ?>
                            
                            <?php Pjax::begin(['id' => 'business-process-pjax']) ?>
                                <table class="table table-bordered table-striped table-condensed kv-table-wrap">
                                    <?php $i=1; foreach($businessProcess as $bp): ?>
                                    <tr>
                                        <td width="250px">
                                            <table width="100%">
                                                <tr>
                                                    <td valign="top" width="30px" style="text-align: center;padding-right: 10px;"><?= $i; ?></td>
                                                    <td>
                                                        <?= $bp->name; ?>
                                                    </td>
                                                    <td valign="top" align="right" width="50px">
                                                        <div class="btn-group">
                                                            <?= Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $bp->id], ['title' => 'Ubah Proses Bisnis ini', 'role'=>'modal-remote', 'class' => 'btn btn-warning btn-xs']); ?>
                                                            <?= Html::a(
                                                                        '<span class="fa fa-trash"></span>', 
                                                                        ['delete', 'id' => $bp->id], 
                                                                        [
                                                                            'class' => 'btn btn-danger btn-xs',
                                                                            'title' => 'Hapus Proses Bisnis ini',
                                                                            'role'=>'modal-remote',
                                                                            'data-confirm'=>false, 
                                                                            'data-method'=>false,
                                                                            'data-request-method'=>'post',
                                                                            'data-confirm-title'=>'Konfirmasi',
                                                                            'data-confirm-message'=>'Yakin ingin menghapus Proses Bisnis ini?'
                                                                        ]
                                                                    ); ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <table width="100%">
                                                            <?php
                                                                $files = BusinessProcessFiles::find()->where(['business_process_id' => $bp->id])->all();
                                                                
                                                                if(!$files){
                                                                    echo '<i>Tidak ada file</i>';
                                                                }

                                                                foreach($files as $bpf):
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?= Html::a($bpf->file->name, $bpf->file->storage === 0 ? [$bpf->file->location] : $bpf->file->location, ['target' => '_blank', 'data-pjax' => 0]); ?>
                                                                </td>
                                                                <td valign="top" align="right" width="50px">
                                                                    <?= Html::a(
                                                                        '<span class="fa fa-trash"></span>', 
                                                                        ['business-files', 'method' => 'del', 'id' => $bpf->id], 
                                                                        [
                                                                            'class' => 'btn btn-danger btn-xs',
                                                                            'title' => 'Hapus File Proses Bisnis ini',
                                                                            'role'=>'modal-remote',
                                                                            'data-confirm'=>false, 
                                                                            'data-method'=>false,
                                                                            'data-request-method'=>'post',
                                                                            'data-confirm-title'=>'Konfirmasi',
                                                                            'data-confirm-message'=>'Yakin ingin menghapus File Proses Bisnis ini?'
                                                                        ]
                                                                    ); ?>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; ?>
                                                        </table>
                                                    </td>
                                                    <td valign="top" align="right" width="50px">
                                                    <?= Html::a('<span class="fa fa-plus"></span>', ['business-files', 'method' => 'add', 'id' => $bp->id], ['title' => 'Tambah File pada Proses Bisnis ini', 'role'=>'modal-remote', 'class' => 'btn btn-success btn-xs']); ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <?php $i++;endforeach; ?>
                                </table>
                            <?php Pjax::end() ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label>Dokumen Standar Mutu (Quality Standard Documents) Human Initiative</label>
                            <?php if(Yii::$app->user->can('Admin')): ?>
                                <?= Html::a('<i class="fa fa-plus"></i> Tambah Dokumen Standar', ['create-document'],
                                ['role'=>'modal-remote','title'=> 'Tambah Dokumen Standar','class'=>'btn btn-success btn-xs pull-right']); ?>
                            <?php endif; ?>
                            <?php Pjax::begin(['id' => 'documents-pjax']) ?>
                                <table class="table table-bordered table-striped table-condensed kv-table-wrap">
                                    <tr>
                                        <th width="30px" class="text-center" rowspan="2" style="vertical-align: middle;">#</th>
                                        <th class="text-center" rowspan="2" style="vertical-align: middle;">Level</th>
                                        <th class="text-center" rowspan="2" style="vertical-align: middle;">Tipe Dokumen</th>
                                        <th class="text-center" colspan="2">Daftar Dokumen</th>
                                        <th class="text-center" rowspan="2" style="vertical-align: middle;">Unggahan Dokumen</th>
                                        <th class="text-center" rowspan="2" style="vertical-align: middle;">Aksi</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">Nomor Dokumen</th>
                                        <th class="text-center">Judul Dokumen</th>
                                    </tr>
                                    <?php $i=1; foreach($qualityDocuments as $qd): ?>
                                    <tr>
                                        <td style="vertical-align: middle;" class="text-center"><?= $i; ?></td>
                                        <td style="vertical-align: middle;"><?= 'Level ' . $qd->level; ?></td>
                                        <td style="vertical-align: middle;"><?= $qd->document_type_id ? $qd->type->document_type : '-'; ?></td>
                                        <td ><?= $qd->document_no ?: '-'; ?></td>
                                        <td ><?= $qd->title ?: '-'; ?></td>
                                        <td style="vertical-align: middle;"><?= Html::a($qd->file->name, $qd->file->storage === 0 ? [$qd->file->location] : $qd->file->location, ['target' => '_blank', 'data-pjax' => 0]); ?></td>
                                        <td style="vertical-align: middle;" align="center">
                                            <?= Html::a('<span class="fa fa-eye"></span>', ['view-document', 'id' => $qd->id], ['role' => 'modal-remote', 'style' => 'color: var(--primary) !important']); ?>
                                            &nbsp;&nbsp;
                                            <?= Html::a('<span class="fa fa-pencil"></span>', ['update-document', 'id' => $qd->id], ['role' => 'modal-remote', 'style' => 'color: var(--warning) !important']); ?>
                                            &nbsp;&nbsp;
                                            <?= Html::a(
                                                '<span class="fa fa-trash"></span>', 
                                                ['delete-document', 'id' => $qd->id], 
                                                [
                                                    'title' => 'Hapus Dokumen Standar Mutu ini',
                                                    'role'=>'modal-remote',
                                                    'data-confirm'=>false, 
                                                    'data-method'=>false,
                                                    'data-request-method'=>'post',
                                                    'data-confirm-title'=>'Konfirmasi',
                                                    'data-confirm-message'=>'Yakin ingin menghapus Dokumen Standar Mutu ini?'
                                                ]
                                            ); ?>
                                        </td>
                                    </tr>
                                    <?php $i++;endforeach; ?>
                                </table>
                            <?php Pjax::end() ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<script>
    function from(val) {
        var from = val;
        var to = $("input[name=year-to]").val();
        if(to){
            window.location.replace("/businessprocess/business-process/index?yearFrom=" + from + "&yearTo=" + to);
        }
    }

    function to(val) {
        var from = $("input[name=year-from]").val();
        var to = val;
        if(from){
            window.location.replace("/businessprocess/business-process/index?yearFrom=" + from + "&yearTo=" + to);
        }
    }
</script>