<?php

namespace app\modules\businessprocess\controllers;

use app\components\FilesHelper;
use Yii;
use app\models\BusinessProcess;
use app\models\BusinessProcessFiles;
use app\models\Files;
use app\models\QualityStandardDocuments;
use app\models\search\BusinessProcessSearch;
use app\models\search\QualityStandardDocumentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * BusinessProcessController implements the CRUD actions for BusinessProcess model.
 */
class BusinessProcessController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BusinessProcess models.
     * @return mixed
     */
    /* public function actionIndex($yearFrom = null, $yearTo = null)
    {

        $bp = BusinessProcess::find();
        if($yearFrom && $yearTo){
            $bp->andWhere('period_start >= ' . $yearFrom . ' and period_end <= ' . $yearTo);
        } else {
            $lastBp = BusinessProcess::find()->orderBy('id desc')->one();
            $filterYearFrom = $lastBp->period_start;
            $filterYearTo = $lastBp->period_end;

            $bp->andWhere(['period_start' => $filterYearFrom]);
            $bp->andWhere(['period_end' => $filterYearTo]);
        }
        $businessProcess = $bp->orderBy('id')->all();

        // echo $bp->createCommand()->getRawSql();

        $qd = QualityStandardDocuments::find();
        if($yearFrom && $yearTo){
            $qd->andWhere('period_start >= ' . $yearFrom . ' and period_end <= ' . $yearTo);
        } else {
            $lastQd = QualityStandardDocuments::find()->orderBy('id desc')->one();
            $filterYearFrom = $lastQd->period_start;
            $filterYearTo = $lastQd->period_end;

            $qd->andWhere(['period_start' => $filterYearFrom]);
            $qd->andWhere(['period_end' => $filterYearTo]);
        }
        $qualityDocuments = $qd->orderBy('level asc')->all();

        return $this->render('index', [
            'businessProcess' => $businessProcess,
            'qualityDocuments' => $qualityDocuments,
            'yearFrom' => $yearFrom,
            'yearTo' => $yearTo,
        ]);
    } */

    public function actionIndex($yearFrom = null, $yearTo = null)
    {

        $bp = BusinessProcess::find();
        if($yearFrom && $yearTo){
            $bp->andWhere('period_start >= ' . $yearFrom . ' and period_end <= ' . $yearTo);
        } else {
            $lastBp = BusinessProcess::find()->orderBy('id desc')->one();
            $filterYearFrom = $lastBp->period_start;
            $filterYearTo = $lastBp->period_end;

            $bp->andWhere(['period_start' => $filterYearFrom]);
            $bp->andWhere(['period_end' => $filterYearTo]);
        }
        $businessProcess = $bp->orderBy('id')->all();

        // echo $bp->createCommand()->getRawSql();

        $searchModel = new QualityStandardDocumentsSearch();
        if($yearFrom && $yearTo){
            $searchModel->period_start = $yearFrom;
            $searchModel->period_end = $yearTo;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'businessProcess' => $businessProcess,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'yearFrom' => $yearFrom,
            'yearTo' => $yearTo,
        ]);
    }

    /**
     * Displays a single BusinessProcess model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "BusinessProcess #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new BusinessProcess model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new BusinessProcess();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Proses Bisnis",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#business-process-pjax',
                    'title'=> "Tambah Proses Bisnis",
                    'content'=>'<span class="text-success">Berhasil menambah Proses Bisnis</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Lagi',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tambah Proses Bisnis",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    /**
     * Updates an existing BusinessProcess model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Proses Bisnis',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#business-process-pjax',
                    'title'=> "Ubah Proses Bisnis",
                    'content'=>'<span class="text-success">Berhasil mengubah Proses Bisnis</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                    ];
            }else{
                 return [
                    'title'=> 'Ubah Proses Bisnis',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete an existing BusinessProcess model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#business-process-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionBusinessFiles($method, $id)
    {
        $request = Yii::$app->request;

        if($method == 'add'){
            $model = new BusinessProcessFiles();
            
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON; 
                if($method == 'del'){
                    $model = BusinessProcessFiles::findOne($id);
                    $model->delete();
        
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['forceClose'=>true,'forceReload'=>'#business-process-pjax'];
                }
            if($request->isGet){
                return [
                    'title'=> "File Proses Bisnis",
                    'content'=>$this->renderAjax('_form-file', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                if($model->storages == 0){
                    $model->files = UploadedFile::getInstances($model, 'files');
                    FilesHelper::upload($model->files, $id, 'business_process');
                } else {

                    $file = new Files();
                    $file->name = $model->links;
                    $file->storage = 1;
                    $file->location = $model->links;
                    $file->created_by = Yii::$app->user->identity->id;
                    $file->created_ip = \app\components\UserIP::getRealIpAddr();
                    if($file->save()){
                        $businessProcess = new BusinessProcessFiles();
                        $businessProcess->business_process_id = $id;
                        $businessProcess->files_id = $file->id;
                        $businessProcess->save();
                    }

                }
                
                return [
                    'forceReload'=>'#business-process-pjax',
                    'title'=> "File Proses Bisnis",
                    'content'=>'<span class="text-success">Berhasil menambah File Proses Bisnis</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Lagi',['business-files', 'method' => 'add', 'id' => $id],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionCreateDocument()
    {
        $request = Yii::$app->request;
        $model = new QualityStandardDocuments();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Dokumen Mutu",
                    'content'=>$this->renderAjax('create-document', [
                        'model' => $model,
                        'isPdfOnly' => true,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                if($model->save()){
                    if($model->storages == 0){
                        $model->files = UploadedFile::getInstances($model, 'files');
                        FilesHelper::upload($model->files, $model->id, 'quality_documents', $model, 'files_id');
                    } else {
    
                        $file = new Files();
                        $file->name = parse_url($model->links)['host'];
                        $file->storage = 1;
                        $file->location = $model->links;
                        $file->created_by = Yii::$app->user->identity->id;
                        $file->created_ip = \app\components\UserIP::getRealIpAddr();
                        if($file->save()){
                            $model->files_id = $file->id;
                            $model->save();
                        }
    
                    }

                    $msg = '<span class="text-success">Berhasil menambah Dokumen Mutu</span>';
                } else {
                    $msg = '<span class="text-danger">Gagal menambah Dokumen Mutu <br /> ' . json_encode($model->errors) . ' </span>';
                }


                return [
                    'forceReload'=>'#documents-pjax',
                    'title'=> "Tambah Dokumen Mutu",
                    'content'=>$msg,
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Lagi',['create-document'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tambah Dokumen Mutu",
                    'content'=>$this->renderAjax('create-document', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionViewDocument($id)
    {
        $request = Yii::$app->request;
        $model = QualityStandardDocuments::findOne($id);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'Lihat Dokumen Standar Mutu',
                    'content'=>$this->renderAjax('view-document', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Edit',['update-document','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionUpdateDocument($id)
    {
        $request = Yii::$app->request;
        $model = QualityStandardDocuments::findOne($id);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Dokumen Mutu",
                    'content'=>$this->renderAjax('create-document', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $model->save();

                return [
                    'forceReload'=>'#documents-pjax',
                    'title'=> "Ubah Dokumen Mutu",
                    'content'=>'<span class="text-success">Berhasil mengubah Dokumen Mutu</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            }else{
                return [
                    'title'=> "Ubah Dokumen Mutu",
                    'content'=>$this->renderAjax('create-document', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionDeleteDocument($id)
    {
        $request = Yii::$app->request;
        QualityStandardDocuments::findOne($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#documents-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionDocument($id, $column, $method)
    {
        $request = Yii::$app->request;

        $model = QualityStandardDocuments::findOne($id);

        if($column == 'files_id'){
            $title = 'Non-editable';
            $isPdfOnly = true;
        } else {
            $title = 'Editable';
            $isPdfOnly = false;
        }

        if($method == 'add'){
            $methodTitle = 'Upload';
        } else {
            $methodTitle = 'Edit';
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON; 
                if($method == 'del'){
                    $model->$column = null;
                    $model->save();
        
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['forceClose'=>true,'forceReload'=>'#documents-pjax'];
                }
            if($request->isGet){
                return [
                    'title'=> $methodTitle . ' ' . $title . ' Dokumen',
                    'content'=>$this->renderAjax('_form-file', [
                        'model' => $model,
                        'isPdfOnly' => $isPdfOnly,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                if($model->storages == 0){
                    $model->files = UploadedFile::getInstances($model, 'files');
                    FilesHelper::upload($model->files, $id, 'quality_documents', $model, $column);
                } else {

                    $file = new Files();
                    $file->name = $model->links;
                    $file->storage = 1;
                    $file->location = $model->links;
                    $file->created_by = Yii::$app->user->identity->id;
                    $file->created_ip = \app\components\UserIP::getRealIpAddr();
                    if($file->save()){

                        if($method == 'edit'){
                            if($f = Files::findOne($model->$column)){
                                $f->delete();
                            }
                        }

                        $model->$column = $file->id;
                        $model->save();
                    }

                }
                
                return [
                    'forceReload'=>'#documents-pjax',
                    'title'=> $methodTitle . ' ' . $title . ' Dokumen',
                    'content'=>'<span class="text-success">Berhasil mengupdate dokumen</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the BusinessProcess model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BusinessProcess the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BusinessProcess::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
