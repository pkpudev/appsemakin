<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StakeholderInfulence */

?>
<div class="stakeholder-infulence-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
