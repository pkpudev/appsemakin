<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StakeholderInfulence */
?>
<div class="stakeholder-infulence-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'influence_grade',
            'influence_point',
            [
                'attribute'=>'created_by',
                'value' => function($model){
                    return $model->created_by ? $model->createdBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'created_at',
                'value' => function($model){
                    return $model->created_at ?: '-';
                },
            ],
            [
                'attribute'=>'updated_by',
                'value' => function($model){
                    return $model->updated_by ? $model->updatedBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($model){
                    return $model->updated_at ?: '-';
                },
            ],
        ],
    ]) ?>

</div>
