<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluationValue */
?>
<div class="activity-evaluation-value-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'evaluation',
            'evaluation_value',
        ],
    ]) ?>

</div>
