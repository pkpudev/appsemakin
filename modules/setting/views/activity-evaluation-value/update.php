<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluationValue */
?>
<div class="activity-evaluation-value-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
