<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluationValue */

?>
<div class="activity-evaluation-value-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
