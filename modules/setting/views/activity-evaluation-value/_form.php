<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityEvaluationValue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-evaluation-value-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'evaluation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'evaluation_value')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
