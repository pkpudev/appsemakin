<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\IssueType */
?>
<div class="issue-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'issue_type',
            'is_active:boolean',
        ],
    ]) ?>

</div>
