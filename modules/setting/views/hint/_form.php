<?php

use app\models\Attributes;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Hint */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hint-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'attributes_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Attributes::find()->all(), 'id', function($model){
				return $model->feature . ' - ' . $model->label;
			}),
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['prompt' => '-- Pilih Atribut --'],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                ]
        ]); ?>

    <?= $form->field($model, 'hint')->textarea(['rows' => 6]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
