<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Hint */

?>
<div class="hint-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
