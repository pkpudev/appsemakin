<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Hint */
?>
<div class="hint-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'attributes_id',
                'value' => function($model){
                    return $model->atr->feature . ' - ' . $model->atr->label;
                }
            ],
            'hint:ntext',
        ],
    ]) ?>

</div>
