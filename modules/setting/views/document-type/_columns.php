<?php

use kartik\grid\GridView;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'document_type',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'is_active',
        'value' => function($model){
            return $model->is_active ? 'Aktif' : 'Tidak Aktif';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>[1 => 'Aktif', 0 => 'Tidak Aktif'], 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
        'hAlign' => 'center',
        'contentOptions' => ['style' => 'width:100px;'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_by',
        'value' => function($model){
            return $model->created_by ? $model->createdBy0->full_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'value' => function($model){
            return $model->created_at ?: '-';
        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'updated_by',
        'value' => function($model){
            return $model->updated_by ? $model->updatedBy0->full_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'updated_at',
        'value' => function($model){
            return $model->updated_at ?: '-';
        },
        'filter' => false
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip', 'style' => 'color: var(--warning) !important'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item',
                          'style' => 'color: var(--danger) !important'],
    ],

];
