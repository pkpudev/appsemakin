<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SourceOfFunds */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="source-of-funds-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'source_of_funds')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([1 => 'Bebas', 2 => 'Terikat'], ['class' => 'form-control']) ?>

    <?= $form->field($model, 'is_active')->dropDownList([1 => 'Aktif', 0 => 'Tidak Aktif'], ['class' => 'form-control']) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
