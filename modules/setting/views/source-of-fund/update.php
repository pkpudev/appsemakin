<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SourceOfFunds */
?>
<div class="source-of-funds-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
