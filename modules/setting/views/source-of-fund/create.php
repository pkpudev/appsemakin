<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SourceOfFunds */

?>
<div class="source-of-funds-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
