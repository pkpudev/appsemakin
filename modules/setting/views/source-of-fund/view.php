<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SourceOfFunds */
?>
<div class="source-of-funds-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'source_of_funds',
            ],
            [
                'attribute'=>'type',
                'value' => function($model){
                    return $model->type==1 ? 'Bebas' : 'Terikat';
                },
            ],
            [
                'attribute'=>'is_active',
                'value' => function($model){
                    return $model->is_active ? 'Aktif' : 'Tidak Aktif';
                },
            ],
        ],
    ]) ?>

</div>
