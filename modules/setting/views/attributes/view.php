<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Attributes */
?>
<div class="attributes-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'table',
            'column',
            'feature',
            'label',
        ],
    ]) ?>

</div>
