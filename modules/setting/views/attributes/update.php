<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Attributes */
?>
<div class="attributes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
