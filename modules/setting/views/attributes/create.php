<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Attributes */

?>
<div class="attributes-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
