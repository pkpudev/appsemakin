<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StakeholderSupport */
?>
<div class="stakeholder-support-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
