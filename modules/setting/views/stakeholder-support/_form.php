<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StakeholderSupport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stakeholder-support-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'support_grade')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'support_point')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
