<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CascadingType */
?>
<div class="cascading-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cascading_type',
            [
                'attribute'=>'is_active',
                'value' => function($model){
                    return $model->is_active ? 'Aktif' : 'Tidak Aktif';
                },
            ],
        ],
    ]) ?>

</div>
