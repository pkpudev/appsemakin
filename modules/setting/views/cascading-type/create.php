<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CascadingType */

?>
<div class="cascading-type-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
