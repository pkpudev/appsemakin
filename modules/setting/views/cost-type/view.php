<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CostType */
?>
<div class="cost-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cost_type',
            'description:ntext',
            [
                'attribute'=>'is_active',
                'value' => function($model){
                    return $model->is_active ? 'Aktif' : 'Tidak Aktif';
                },
            ],
        ],
    ]) ?>

</div>
