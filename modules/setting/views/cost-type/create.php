<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CostType */

?>
<div class="cost-type-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
