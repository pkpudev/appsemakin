<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TargetType */
?>
<div class="target-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'target_type',
            [
                'attribute' => 'is_active',
                'value' => function($model){
                    return $model->is_active ? 'Aktif' : 'Tidak Aktif';
                },
            ]
        ],
    ]) ?>

</div>
