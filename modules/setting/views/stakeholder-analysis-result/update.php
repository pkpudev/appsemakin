<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StakeholderAnalysisResult */
?>
<div class="stakeholder-analysis-result-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
