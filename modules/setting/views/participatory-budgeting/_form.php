<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ParticipatoryBudgeting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="participatory-budgeting-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'participatory_budgeting')->textarea(['rows' => 1]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_active')->dropDownList([1 => 'Aktif', 0 => 'Tidak Aktif'], ['class' => 'form-control']) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
