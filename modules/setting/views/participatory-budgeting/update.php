<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ParticipatoryBudgeting */
?>
<div class="participatory-budgeting-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
