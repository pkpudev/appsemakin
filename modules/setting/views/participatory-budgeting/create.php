<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ParticipatoryBudgeting */

?>
<div class="participatory-budgeting-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
