<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ParticipatoryBudgeting */
?>
<div class="participatory-budgeting-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'participatory_budgeting:ntext',
            'description:ntext',
            [
                'attribute'=>'is_active',
                'value' => function($model){
                    return $model->is_active ? 'Aktif' : 'Tidak Aktif';
                },
            ],
        ],
    ]) ?>

</div>
