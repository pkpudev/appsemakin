<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RiskCategory */

?>
<div class="risk-category-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
