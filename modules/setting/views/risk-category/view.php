<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RiskCategory */
?>
<div class="risk-category-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'risk_category',
            'description',
            'is_active:boolean',
        ],
    ]) ?>

</div>
