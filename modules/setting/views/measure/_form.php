<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Measure */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="measure-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'measure')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'used_on')
			->dropDownList(
							[
								1 => 'Volume', 
								2 => 'Frekuensi'
							], ['class' => 'form-control']) ?>
    
	<?= $form->field($model, 'is_active')->dropDownList([1 => 'Aktif', 0 => 'Tidak Aktif'], ['class' => 'form-control']) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
