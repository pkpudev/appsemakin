<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Measure */

?>
<div class="measure-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
