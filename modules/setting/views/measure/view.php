<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Measure */
?>
<div class="measure-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'measure',
            [
                'attribute' => 'used_on',
                'value' => function($model){
                    if($model->used_on == 1){
                        $on = 'Volume';
                    } else if($model->used_on == 2){
                        $on = 'Frekuensi';
                    } else {
                        $on = '-';
                    }
                    return $on;
                },
            ],
            [
                'attribute' => 'is_active',
                'value' => function($model){
                    return $model->is_active ? 'Aktif' : 'Tidak Aktif';
                },
            ]
        ],
    ]) ?>

</div>
