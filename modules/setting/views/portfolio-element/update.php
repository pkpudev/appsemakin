<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PortfolioElement */
?>
<div class="portfolio-element-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
