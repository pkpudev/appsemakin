<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PortfolioElement */
?>
<div class="portfolio-element-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'element',
            [
                'attribute'=>'is_active',
                'value' => function($model){
                    return $model->is_active ? 'Aktif' : 'Tidak Aktif';
                },
            ]
        ],
    ]) ?>

</div>
