<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PortfolioType */
?>
<div class="portfolio-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'portfolio_type',
            [
                'attribute' => 'is_active',
                'value' => function($model){
                    return $model->is_active ? 'Aktif' : 'Tidak Aktif';
                },
            ]
        ],
    ]) ?>

</div>
