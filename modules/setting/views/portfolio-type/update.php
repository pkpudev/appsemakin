<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PortfolioType */
?>
<div class="portfolio-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
