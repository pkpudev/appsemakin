<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Perspective */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="perspective-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'perspective')->textInput(['maxlength' => true]) ?>

	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'start_year')->widget(DatePicker::classname(), [
				'options' => ['placeholder' => 'Silahkan Pilih Tahun Mulai'],
				'pluginOptions' => [
					'autoclose'=>true,
					'todayHighlight' => true,
					'startView'=>'year',
					'minViewMode'=>'years',
					'format' => 'yyyy'
				]
			]) ?>
		</div>
		<div class="col-md-6">
			<?= $form->field($model, 'end_year')->widget(DatePicker::classname(), [
				'options' => ['placeholder' => 'Silahkan Pilih Tahun AKhir'],
				'pluginOptions' => [
					'autoclose'=>true,
					'todayHighlight' => true,
					'startView'=>'year',
					'minViewMode'=>'years',
					'format' => 'yyyy'
				]
			]) ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-6">
			<?= $form->field($model, 'is_active')->dropDownList([1 => 'Aktif', 0 => 'Tidak Aktif'], ['class' => 'form-control']) ?>
		</div>
	</div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
