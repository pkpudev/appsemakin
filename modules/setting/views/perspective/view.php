<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Perspective */
?>
<div class="perspective-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'perspective',
            [
                'label' => 'Period',
                'value' => function($model){
                    return $model->start_year . ' - ' . $model->end_year;
                }
            ],
            'weight',
            [
                'attribute' => 'is_active',
                'value' => function($model){
                    return $model->is_active ? 'Aktif' : 'Tidak Aktif';
                },
            ]
        ],
    ]) ?>

</div>
