<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Perspective */
?>
<div class="perspective-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
