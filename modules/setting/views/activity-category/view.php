<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ActivityCategory */
?>
<div class="activity-category-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'activity_category',
            'is_active:boolean',
        ],
    ]) ?>

</div>
