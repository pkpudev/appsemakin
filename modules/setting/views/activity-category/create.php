<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ActivityCategory */

?>
<div class="activity-category-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
