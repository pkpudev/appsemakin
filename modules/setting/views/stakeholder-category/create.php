<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StakeholderCategory */

?>
<div class="stakeholder-category-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
