<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StakeholderCategory */
?>
<div class="stakeholder-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
