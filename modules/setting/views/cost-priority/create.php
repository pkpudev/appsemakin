<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CostPriority */

?>
<div class="cost-priority-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
