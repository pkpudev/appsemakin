<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CostPriority */
?>
<div class="cost-priority-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cost_priority',
            'description:ntext',
            [
                'attribute'=>'is_active',
                'value' => function($model){
                    return $model->is_active ? 'Aktif' : 'Tidak Aktif';
                },
            ],
        ],
    ]) ?>

</div>
