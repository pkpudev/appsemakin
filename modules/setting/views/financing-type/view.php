<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinancingType */
?>
<div class="financing-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'financing_type',
            'description:ntext',
            'is_active:boolean',
        ],
    ]) ?>

</div>
