<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinancingType */
?>
<div class="financing-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
