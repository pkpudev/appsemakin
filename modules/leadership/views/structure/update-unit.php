<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UnitStructure */

?>
<div class="unit-structure-create">
    <?= $this->render('_form_unit', [
        'model' => $model,
    ]) ?>
</div>
