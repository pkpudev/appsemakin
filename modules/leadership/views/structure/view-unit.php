<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PositionStructure */
?>
<div class="position-structure-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'year',
            ],
            [
                'attribute'=>'unit_code',
            ],
            [
                'attribute'=>'initiative_code',
                'value' => function($model){
                    return $model->initiative_code ?: '-';
                }
            ],
            [
                'attribute'=>'unit_name',
            ],
            [
                'attribute'=>'level',
                'value' => function($model){
                    return $model->level == 1 ? 'Direktorat' : 
                            ($model->level == 2 ? 'Departemen' : 
                            ($model->level == 3 ? 'Divisi':
                            ($model->level == 4 ? 'Bidang' : 
                            ($model->level == 5 ? 'Squad' : '-'))));
                },
            ],
            [
                'attribute'=>'superior_unit_id',
                'value' => function($model){
                    return $model->superior_unit_id ? $model->sup->unit_name : '-';
                }
            ],
            [
                'attribute'=>'unit_id',
                'value' => function($model){
                    return $model->unit_id ? $model->dept->deptname : '-';
                }
            ],
            [
                'attribute'=>'is_value_stream',
                'value' => function($model){
                    return $model->is_value_stream ? 'Ya' : 'Bukan';
                },
            ],
            [
                'attribute'=>'created_by',
                'value' => function($model){
                    return $model->created_by ? $model->createdBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'created_at',
                'value' => function($model){
                    return $model->created_at ?: '-';
                },
            ],
            [
                'attribute'=>'updated_by',
                'value' => function($model){
                    return $model->updated_by ? $model->updatedBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($model){
                    return $model->updated_at ?: '-';
                },
            ],
        ],
    ]) ?>

</div>
