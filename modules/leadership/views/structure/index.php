<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\widgets\DatePicker;
// use kongoon\orgchart\OrgChart;
use yii\widgets\Pjax;

$this->title = 'Struktur Jabatan';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>
    .google-visualization-orgchart-table {
        margin-top: 20px;
    }

    .google-visualization-orgchart-node-medium {
        width: 400px;
    }

    .google-visualization-orgchart-node,
    .google-visualization-orgchart-nodesel {
        border: none !important;
        background: none !important;
    }

    .google-visualization-orgchart-node {
        -moz-border-radius: none !important;
        -webkit-border-radius: none !important;
        -webkit-box-shadow: none !important;
        -moz-box-shadow: none !important;
    }

    .directorate-box {
        color: rgb(111, 68, 137) !important;
        background-color: rgb(255, 153, 153);
        font-weight: bold;
        box-shadow: var(--shadow);
        min-height: 50px;
        border-radius: 3px;
        width: 110px;
    }

    .department-box {
        color: rgb(111, 68, 137) !important;
        background-color: rgb(255, 204, 153);
        font-weight: bold;
        box-shadow: var(--shadow);
        min-height: 50px;
        border-radius: 3px;
        width: 110px;
    }

    .division-box {
        color: rgb(111, 68, 137) !important;
        background-color: rgb(204, 255, 153);
        font-weight: bold;
        box-shadow: var(--shadow);
        min-height: 50px;
        border-radius: 3px;
        width: 110px;
    }

    .sub-division-box {
        color: rgb(111, 68, 137) !important;
        background-color: rgb(153, 255, 230);
        font-weight: bold;
        box-shadow: var(--shadow);
        min-height: 50px;
        border-radius: 3px;
        width: 110px;
    }

    .squad-box {
        color: rgb(111, 68, 137) !important;
        background-color: rgb(153, 204, 255);
        font-weight: bold;
        box-shadow: var(--shadow);
        min-height: 50px;
        border-radius: 3px;
        width: 110px;
    }

    .default-box {
        color: rgb(111, 68, 137) !important;
        background-color: rgb(204, 153, 255);
        font-weight: bold;
        border: 2px solid var(--background) !important;
        box-shadow: var(--shadow);
        min-height: 50px;
        border-radius: 3px;
        width: 110px;
    }
</style>
<div class="structure-index">

    <div class="row">
        <div class="col-md-2">
            <div class="box box-default">
                <div class="box-body">
                    <label>Filter Tahun</label>
                    <?= DatePicker::widget([
                        'name' => 'year-filter',
                        'value' => $year,
                        'options' => [
                            'placeholder' => 'Filter Tahun',
                            'onchange' => 'refreshPage($(this).val())',
                        ],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'startView'=>'year',
                            'minViewMode'=>'years',
                            'format' => 'yyyy',
                            'allowClear' => false
                        ]
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <?php $this->beginBlock('position_structure'); ?>
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'export' => [
                   'label' => 'Export Excel atau PDF',
                   'header' => '',
                   'options' => [
                       'class' => 'btn btn-primary'
                   ], 
                   'menuOptions' => [ 
                           'style' => 'background: var(--background); width: 187px;'
                   ]
               ],
               'exportConfig' => [
                   GridView::PDF => true,
                   GridView::EXCEL => true,
               ],
                'columns' => require(__DIR__.'/_columns.php'),
                'toolbar'=> [
                    ['content'=>
                        Html::a('<i class="fa fa-plus"></i> Tambah Struktur Jabatan', ['create'],
                        ['role'=>'modal-remote','title'=> 'Tambah Struktur Jabatan','class'=>'btn btn-success']) . 
                        Html::a('<i class="fa fa-file"></i> Clone Struktur Jabatan', ['clone-position', 'from' => 'index'],
                        ['role'=>'modal-remote','title'=> 'Clone Struktur Jabatan','class'=>'btn btn-warning'])
                    ],
                    ['content'=>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                        '{toggleData}'
                    ],
                    ['content'=>
                        '{export}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="fa fa-list-alt"></i> Daftar Struktur Jabatan',
                    'before'=>'',
                    'after'=>'',
                ]
            ])?>
        </div>
    <?php $this->endBlock(); ?>

    <?php $this->beginBlock('position_structure_sitemap'); ?>
        <?= Html::a('<i class="fa fa-plus"></i> Tambah Struktur Jabatan', ['create', 'from' => 'structure'],
            ['role'=>'modal-remote','title'=> 'Tambah Struktur Jabatan','class'=>'btn btn-success pull-right btn-sm']); ?>
        <?php Pjax::begin(['id' => 'position-structure-sitemap-pjax']) ?>
            <?= /* OrgChart::widget([
                'id' => 'position-structure',
                'data' => $positionStructureData
            ]); */'' ?>

            <b>Keterangan:</b>
            <table>
                <tr>
                    <td><div style="width: 100px; height: 15px; border-radius: 3px; background-color: rgb(255, 153, 153);"></div></td>
                    <td><b> : Direktorat</b></td>
                </tr>
                <tr>
                    <td><div style="width: 100px; height: 15px; border-radius: 3px; background-color: rgb(255, 204, 153);"></div></td>
                    <td><b> : Departemen</b></td>
                </tr>
                <tr>
                    <td><div style="width: 100px; height: 15px; border-radius: 3px; background-color: rgb(204, 255, 153);"></div></td>
                    <td><b> : Divisi</b></td>
                </tr>
                <tr>
                    <td><div style="width: 100px; height: 15px; border-radius: 3px; background-color: rgb(153, 255, 230);"></div></td>
                    <td><b> : Bidang</b></td>
                </tr>
                <tr>
                    <td><div style="width: 100px; height: 15px; border-radius: 3px; background-color: rgb(153, 204, 255);"></div></td>
                    <td><b> : Squad</b></td>
                </tr>
            </table>
        <?php Pjax::end() ?>
    <?php $this->endBlock(); ?>

    <?php $this->beginBlock('employee'); ?>
            <?=GridView::widget([
                'id'=>'employee',
                'dataProvider' => $dataProviderEmployee,
                'filterModel' => $searchModelEmployee,
                'pjax'=>true,
                'export' => [
                   'label' => 'Export Excel atau PDF',
                   'header' => '',
                   'options' => [
                       'class' => 'btn btn-primary'
                   ], 
                   'menuOptions' => [ 
                           'style' => 'background: var(--background); width: 187px;'
                   ]
               ],
               'exportConfig' => [
                   GridView::PDF => true,
                   GridView::EXCEL => true,
               ],
                'columns' => require(__DIR__.'/_column_employee.php'),
                'toolbar'=> [
                    ['content'=>
                        Html::a('<i class="fa fa-plus"></i> Tambah Jabatan x Karyawan', ['create-position-employee'],
                        ['role'=>'modal-remote','title'=> 'Tambah Jabatan x Karyawan','class'=>'btn btn-success'])
                    ],
                    ['content'=>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                        '{toggleData}'
                    ],
                    ['content'=>
                        '{export}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="fa fa-list-alt"></i> Daftar Jabatan x Karyawan',
                    'before'=>'',
                    'after'=>'',
                ]
            ])?>
    <?php $this->endBlock(); ?>

    <div class="box box-default">
        <div class="box-body" style="overflow-x: auto;">
            <?= Tabs::widget([
                'id' => 'unit_structure-analysis-tabs',
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => "<span class='fa fa-users'></span>&nbsp;Jabatan",
                        'content' => $this->blocks['position_structure'],
                    ],
                    [
                        'label' => "<span class='fa fa-sitemap'></span>&nbsp;Bagan Struktur Jabatan",
                        'content' => $this->blocks['position_structure_sitemap'],
                    ],
                    [
                        'label' => "<span class='fa fa-user-circle'></span>&nbsp; Jabatan x Karyawan",
                        'content' => $this->blocks['employee'],
                    ],
                ]
            ]); ?>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
<script>
    function refreshPage(val) {
        window.location.replace("/leadership/structure/index?year=" + val);
    }
</script>