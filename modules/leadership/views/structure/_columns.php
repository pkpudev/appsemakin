<?php

use app\models\PositionStructure;
use app\models\SdmPositionLevel;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'position_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'level',
        'value' => function($model){
            return $model->level ? $model->level0->level_name : '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=> ArrayHelper::map(SdmPositionLevel::find()->all(), 'id', 'level_name'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'role',
        'value' => function($model){
            return $model->role ?: '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'responsibility',
        'value' => function($model){
            return $model->responsibility ?: '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'authority',
        'value' => function($model){
            return $model->authority ?: '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'responsible_to_id',
                'value' => function($model){
                    return $model->responsible_to_id ? $model->resp->position_name : '-';
                }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Bertanggung Jawab Atas',
        'value' => function($model){
            $post = [];
            $postStruct = PositionStructure::find()->where(['responsible_to_id' => $model->id])->all();
            foreach($postStruct as $ps){
                array_push($post, $ps->position_name);
            }
            return implode(', ', $post);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ucode',
        'label' => 'Kode Unit',
        'value' => function($model){
            return $model->unit_structure_id ? $model->unit->unit_code : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'f_directorate',
        'label'=>'Direktorat',
        'value' => function($model){
            return $model->getDirectorate();
            // return $model->unit->level == 1 ? $model->unit->unit_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'f_department',
        'label'=>'Departemen',
        'value' => function($model){
            return $model->getDepartment();
            // return $model->unit->level == 2 ? $model->unit->unit_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'f_division',
        'label'=>'Divisi',
        'value' => function($model){
            return $model->getDivision();
            // return $model->unit->level == 3 ? $model->unit->unit_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'f_squad',
        'label'=>'Squad',
        'value' => function($model){
            return $model->getSquad();
            // return $model->unit->level == 5 ? $model->unit->unit_name : '-';
        }
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'level',
    //     'value' => function($model){
    //         return $model->level ? $model->level0->level_name : '-';
    //     },
    //     'filterType'=>GridView::FILTER_SELECT2,
    //     'filter'=> ArrayHelper::map(SdmPositionLevel::find()->all(), 'id', 'level_name'), 
    //     'filterWidgetOptions'=>[
    //         'pluginOptions'=>['allowClear'=>true],
    //     ],
    //     'filterInputOptions'=>['placeholder'=>''],
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },   
        'template' => '{view}&nbsp;{update}&nbsp;{delete}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<span class="fa fa-eye"></span>',
                    ['view', 'id' => $model->id], 
                    [
                        'title' => 'Lihat Struktur Jabatan ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--primary) !important'
                    ]
                );
            },
            'update' => function ($url, $model) {
                return Html::a('<span class="fa fa-pencil"></span>',
                    ['update', 'id' => $model->id], 
                    [
                        'title' => 'Ubah Struktur Jabatan ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--warning) !important'
                    ]
                );
            },
            'delete' => function ($url, $model) {
                return Html::a('<span class="fa fa-trash"></span>',
                    ['delete', 'id' => $model->id], 
                    [
                        'title' => 'Hapus Struktur Jabatan ini',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Konfirmasi',
                        'data-confirm-message'=>'Yakin ingin menghapus Struktur Jabatan ini?',
                        'style' => 'color: var(--danger) !important'
                    ]
                );
            },
        ],
        'width' => '90px'
    ],

];
