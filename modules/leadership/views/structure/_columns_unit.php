<?php

use app\models\PositionStructure;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'year',
    //     'group' => true
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_code',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'initiative_code',
        'value' => function($model){
            return $model->initiative_code ?: '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'level',
        'value' => function($model){
            return $model->level == 1 ? 'Direktorat' : 
                    ($model->level == 2 ? 'Departemen' : 
                    ($model->level == 3 ? 'Divisi':
                    ($model->level == 4 ? 'Bidang' : 
                    ($model->level == 5 ? 'Squad' : '-'))));
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=> [1 => 'Direktorat', 2 => 'Departemen', 3 => 'Divisi', 4 => 'Bidang', 5 => 'Squad'], 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'superior_unit_id',
        'value' => function($model){
            return $model->superior_unit_id ? $model->sup->unit_name . ($model->sup->year != $model->year ? ' (' . $model->sup->year . ')' : '') : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'is_value_stream',
        'value' => function($model){
            return $model->is_value_stream ? 'Ya' : 'Bukan';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>[1 => 'Ya', 0 => 'Bukan'], 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
        'hAlign' => 'center',
        'contentOptions' => ['style' => 'width:100px;'],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },   
        'template' => '{view}&nbsp;{update}&nbsp;{delete}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<span class="fa fa-eye"></span>',
                    ['view-unit', 'id' => $model->id], 
                    [
                        'title' => 'Lihat Struktur Unit ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--primary) !important'
                    ]
                );
            },
            'update' => function ($url, $model) {
                return Html::a('<span class="fa fa-pencil"></span>',
                    ['update-unit', 'id' => $model->id], 
                    [
                        'title' => 'Ubah Struktur Unit ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--warning) !important'
                    ]
                );
            },
            'delete' => function ($url, $model) {
                $check = PositionStructure::findOne(['unit_structure_id' => $model->id]);
                if(!$check)
                return Html::a('<span class="fa fa-trash"></span>',
                    ['delete-unit', 'id' => $model->id], 
                    [
                        'title' => 'Hapus Struktur Unit ini',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Konfirmasi',
                        'data-confirm-message'=>'Yakin ingin menghapus Struktur Unit ini?',
                        'style' => 'color: var(--danger) !important'
                    ]
                );
            },
        ],
        'width' => '90px'
    ],

];
