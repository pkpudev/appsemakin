<?php

use app\models\Branch;
use app\models\Departments;
use app\models\UnitStructure;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\UnitStructure */
/* @var $form yii\widgets\ActiveForm */

$listUnit = ArrayHelper::map(Departments::find()->where('level is not null and level != 0')->orderBy('deptname asc')->all(), 'deptid', 'deptname');
$listUnit = ArrayHelper::merge([0 => '+ Input Unit Baru'], $listUnit);
?>

<div class="unit-structure-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'year')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'unit_id')->widget(Select2::classname(), [
                'data' => $listUnit,
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Unit --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'level')->widget(Select2::classname(), [
                'data' => [1 => 'Direktorat', 2 => 'Departemen', 3 => 'Divisi', 4 => 'Bidang', 5 => 'Squad'],
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Level --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
        <div class="col-md-9">

            <?php if($model->isNewRecord): ?>
                <?= $form->field($model, 'superior_unit_id')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'options' => [
                                    'placeholder' => 'Pilih Unit'
                                ],
                    'select2Options' => [
                        'pluginOptions' => [
                                                'allowClear' => true,
                                                'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                            ]
                    ],
                    'pluginOptions' => [
                        'depends' => ['unitstructure-year'],
                        'url' => Url::to(['/leadership/structure/get-unit']),
                    ]
                ]); ?>
            <?php else: ?>
                <?= $form->field($model, 'superior_unit_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(UnitStructure::find()->where(['year' => $model->year])->all(), 'id', 'unit_name'),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => ['prompt' => '-- Pilih Unit --'],
                    'pluginOptions' => [
                        'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                        ]
                ]); ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'unit_code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'initiative_code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'unit_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'is_value_stream')->dropDownList([0 => 'Bukan', 1 => 'Ya'], ['class' => 'form-control']) ?>
        </div>
        <div class="col-md-4" id="branch-place" <?= $model->isNewRecord ? 'style="display: none;"' : '' ?>>
            <?= $form->field($model, 'branch_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Branch::find()->where(['is_active' => true])->all(), 'id', 'branch_name'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Kantor --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ])->label('Kantor'); ?>
        </div>
    </div>

    <?php if (!Yii::$app->request->isAjax){ ?>
          <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php
$script = <<<JS

    jQuery('#unitstructure-unit_id').on('change', function(e){
        unitId = this.value;

        if(unitId == 0){
            $('#branch-place').show()
        } else {
            $('#branch-place').hide()
        }
        
        $.ajax(
        {
            url: '/leadership/structure/get-dept?id=' + unitId,
            dataType: "json",
            async: false,
            success: function(d)
            {
                console.log(d);
                $('label[for=unitstructure-superior_unit_id]').html(d.sup_unit_label);
                $("#unitstructure-level").select2('data', { id: d.level, text: d.level_label});
                $("#unitstructure-level").val(d.level).trigger('change');
                $("#unitstructure-unit_name").val(d.deptname);
            }
        });
    })

JS;
$this->registerJs($script);