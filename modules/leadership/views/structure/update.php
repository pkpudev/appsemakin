<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PositionStructure */
?>
<div class="position-structure-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
