<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PositionStructure */
?>
<div class="position-structure-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'year',
            'position_code',
            'position_name',
            [
                'attribute'=>'level',
                'value' => function($model){
                    return $model->level ? $model->level0->level_name : '-';
                }
            ],
            [
                'attribute'=>'role',
                'format' => 'raw',
                'value' => function($model){
                    return $model->role ?: '-';
                }
            ],
            [
                'attribute'=>'responsibility',
                'format' => 'raw',
                'value' => function($model){
                    return $model->responsibility ?: '-';
                }
            ],
            [
                'attribute'=>'authority',
                'format' => 'raw',
                'value' => function($model){
                    return $model->authority ?: '-';
                }
            ],
            [
                'attribute'=>'responsible_to_id',
                'value' => function($model){
                    return $model->responsible_to_id ? $model->resp->position_name : '-';
                }
            ],
            [
                'attribute'=>'unit_structure_id',
                'value' => function($model){
                    return $model->unit_structure_id ? $model->unit->unit_name : '-';
                }
            ],
            [
                'attribute'=>'created_by',
                'value' => function($model){
                    return $model->created_by ? $model->createdBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'created_at',
                'value' => function($model){
                    return $model->created_at ?: '-';
                },
            ],
            [
                'attribute'=>'updated_by',
                'value' => function($model){
                    return $model->updated_by ? $model->updatedBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($model){
                    return $model->updated_at ?: '-';
                },
            ],
        ],
    ]) ?>

</div>
