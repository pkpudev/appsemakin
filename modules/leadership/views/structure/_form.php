<?php

use app\models\Company;
use app\models\PositionStructure;
use app\models\SdmPosition;
use app\models\SdmPositionLevel;
use app\models\UnitStructure;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PositionStructure */
/* @var $form yii\widgets\ActiveForm */

$listPosition = ArrayHelper::map(SdmPosition::find()->joinWith(['departments d'])->where(['d.company_id'=>Company::HI])->orderBy('position_name asc')->all(), 'id', 'position_name');
$listPosition = ArrayHelper::merge([0 => '+ Input Jabatan Baru'], $listPosition);

?>

<div class="position-structure-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'year')->widget(DatePicker::classname(), [
                'options' => ['id', 'placeholder' => 'Silahkan Pilih Tahun'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'position_id')->widget(Select2::classname(), [
                'data' => $listPosition,
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Jabatan --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'level')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(SdmPositionLevel::find()->all(), 'id', 'level_name'),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Level --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'responsible_to_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(PositionStructure::find()->all(), 'id', /* 'position_name' */ function($model){
                    return $model->year . ' - ' . $model->position_name;
                }),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['prompt' => '-- Pilih Jabatan --'],
                'pluginOptions' => [
                    'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                    ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'position_code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'position_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'unit_structure_id')->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => [
                                'placeholder' => 'Pilih Unit'
                            ],
                'select2Options' => [
                    'initValueText' => $model->unit->unit_name,
                    'options' => [
                                    'value' => $model->unit_structure_id
                                ],
                    'pluginOptions' => [
                                            'allowClear' => true,
                                            'dropdownParent' => new \yii\web\JsExpression("$('#ajaxCrudModal')"),
                                        ]
                ],
                'pluginOptions' => [
                    'depends' => ['positionstructure-year'],
                    'url' => Url::to(['/leadership/structure/get-unit']),
                ]
            ]); ?>
        </div>
    </div>

    <?= $form->field($model, 'role')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'responsibility')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'authority')->textarea(['rows' => 3]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php
$script = <<<JS

    jQuery('#positionstructure-position_id').on('change', function(e){
        postId = this.value;
        
        $.ajax(
        {
            url: '/leadership/structure/get-position?id=' + postId,
            dataType: "json",
            async: false,
            success: function(d)
            {
                console.log(d);
                // $('label[for=positionstructure-responsible_to_id]').html(d.resp_label);
                $("#positionstructure-level").select2('data', { id: d.level, text: d.level_label});
                $("#positionstructure-level").val(d.level).trigger('change');
                $("#positionstructure-position_name").val(d.postname);
            }
        });
    })

JS;
$this->registerJs($script);
