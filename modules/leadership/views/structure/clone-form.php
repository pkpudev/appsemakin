<?php

use kartik\widgets\ActiveForm;
use kartik\date\DatePicker;

?>
<div class="clone">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'clone_from')->widget(DatePicker::classname(), [
                'options' => ['id', 'placeholder' => 'Silahkan Pilih Tahun Yg ingin di clone'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ])->label('Tahun Dari') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'clone_to')->widget(DatePicker::classname(), [
                'options' => ['id', 'placeholder' => 'Silahkan Pilih Tahun Tujuan'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'startView'=>'year',
                    'minViewMode'=>'years',
                    'format' => 'yyyy'
                ]
            ])->label('Tahun Tujuan') ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>