<?php

use app\models\Employee;
use app\models\PositionStructure;
use app\models\SdmPositionLevel;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'unit_id',
        'label'=>'Unit',
        'value' => function($model){
            return $model->position->unit->unit_name;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(app\models\UnitStructure::find()->where(['year' => $year])->orderBy('unit_code')->asArray()->all(), 'id', 'unit_name'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'position_structure_id',
        'value' => function($model){
            return $model->position->position_name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'level',
        'value' => function($model){
            return $model->position->level ? $model->position->level0->level_name : '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=> ArrayHelper::map(SdmPositionLevel::find()->all(), 'id', 'level_name'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'attribute'=>'employee_id',
        'value'=>function ($model) {
            return $model->employee->full_name;
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>ArrayHelper::map(Employee::find()->where(['and', ['user_status'=>1], ['is_employee'=>1], ['<>', 'employee_type_id', 4]])->orderBy('full_name')->all(), 'id', 'full_name'),
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
        'group'=>true,
        'width' => '280px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Lokasi Bekerja',
        'attribute'=>'branch_id',
        'value' => function($model){
            return $model->employee->branch->branch_name;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(\app\models\Branch::find()->where(['is_active' => true])->orderBy('id asc')->all(), 'id', 'branch_name'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Bobot',
        'attribute'=>'weight',
        'value' => function($model){
            return $model->weight ? round($model->weight, 2) . '%' : '-';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Status Karyawan',
        'attribute'=>'employee_type_id',
        'value' => function($model){
            return $model->employee->type->employee_type;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(\app\models\SdmEmployeeType::find()->where(['<>', 'id', 4])->asArray()->all(), 'id', 'employee_type'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => ''],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },   
        'template' => '{update}&nbsp;&nbsp;{delete}',
        'buttons' => [
            'update' => function ($url, $model) {
                return Html::a('<span class="fa fa-pencil"></span>',
                    ['update-position-employee', 'id' => $model->id], 
                    [
                        'title' => 'Ubah Jabatan x Karyawan ini',
                        'role'=>'modal-remote',
                        'style' => 'color: var(--warning) !important'
                    ]
                );
            },
            'delete' => function ($url, $model) {
                return Html::a('<span class="fa fa-trash"></span>',
                    ['delete-position-employee', 'id' => $model->id], 
                    [
                        'title' => 'Hapus Jabatan x Karyawan ini',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Konfirmasi',
                        'data-confirm-message'=>'Yakin ingin menghapus Struktur Jabatan ini?',
                        'style' => 'color: var(--danger) !important'
                    ]
                );
            },
        ],
        'visible' => Yii::$app->user->can('Admin'),
        'width' => '90px'
    ],

];
