<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PositionStructure */

?>
<div class="position-structure-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
