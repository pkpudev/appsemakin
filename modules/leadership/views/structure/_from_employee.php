<?php

use app\models\Employee;
use app\models\PositionStructure;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PositionStructureEmployee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="position-structure-employee-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'position_structure_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(PositionStructure::find()->all(), 'id', function($model){
            return $model->year . ' - ' . $model->position_name;
        }),
        'initValueText' => $model->position_structure_id ? $model->position->year . ' - ' . $model->position->position_name : '',
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
                        'prompt' => '-- Pilih Jabatan --',
                        'value' => $model->position_structure_id
                    ],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]); ?>

    <?= $form->field($model, 'employee_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Employee::find()
                                        ->where(['company_id' => 1])
                                        ->andWhere(['user_status' => 1])
                                        ->andWhere(['<>', 'employee_type_id', 4])
                                        ->orderBy('full_name asc')->all(), 'id', function($model){
                                            return $model->full_name . ' (' . $model->branch->branch_name . ')';
                                        }),
        'initValueText' => $model->employee_id ? $model->employee->full_name : '',
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
                        'prompt' => '-- Pilih Karyawan --',
                        'value' => $model->employee_id
                    ],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]); ?>
  
    <?php if (!Yii::$app->request->isAjax){ ?>
          <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
