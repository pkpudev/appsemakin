<?php

namespace app\modules\leadership\controllers;

use app\models\Departments;
use Yii;
use app\models\PositionStructure;
use app\models\PositionStructureEmployee;
use app\models\SdmPosition;
use app\models\SdmPositionLevel;
use app\models\search\PositionStructureEmployeeSearch;
use app\models\search\PositionStructureSearch;
use app\models\search\UnitStructureSearch;
use app\models\UnitStructure;
use app\models\VwEmployeeManager;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * StructureController implements the CRUD actions for PositionStructure model.
 */
class StructureController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PositionStructure models.
     * @return mixed
     */
    public function actionIndex($year = null)
    {

        if(!$year){
            $year = date('Y');
            // $year = '2022';
        }

        $searchModel = new PositionStructureSearch();
        $searchModel->year = $year;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModelEmployee = new PositionStructureEmployeeSearch();
        $searchModelEmployee->year = $year;
        $dataProviderEmployee = $searchModelEmployee->search(Yii::$app->request->queryParams);

        $positionStructureData = [];
        if($postLv1 = PositionStructure::find()->where(['year' => $year])->andWhere('level = 9')->andWhere('responsible_to_id is null')->one()){
            $idLv1 = $postLv1->id;

            $positionEmployee1 = PositionStructureEmployee::find()->where(['position_structure_id' => $postLv1->id])->all();
            $lv1Names = [];
            foreach($positionEmployee1 as $pe1){
                array_push($lv1Names, $pe1->employee->full_name);
            }
            array_push($positionStructureData, [
                                                    [
                                                        'v' => '"' . $idLv1 . '"', 
                                                        'f' => '<div class="' . $this->getLevelNamePosition($postLv1->unit->level) . '">' . $this->replaceLongString($postLv1->position_name) . '<hr />' . implode('<br />', $lv1Names) . '</div>'
                                                    ], 
                                                    '', 
                                                    $postLv1->position_name . ' (' . implode(', ', $lv1Names) . ')'
                                                ]);
            $postLv2 = PositionStructure::find()->where(['responsible_to_id' => $idLv1])->all();
            foreach($postLv2 as $ul2){

                $positionEmployee2 = PositionStructureEmployee::find()->where(['position_structure_id' => $ul2->id])->all();
                $lv2Names = [];
                foreach($positionEmployee2 as $pe2){
                    array_push($lv2Names, $pe2->employee->full_name);
                }

                array_push($positionStructureData, [
                                                        [
                                                            'v' => '"' . $ul2->id . '"', 
                                                            'f' => '<div class="' . $this->getLevelNamePosition($ul2->unit->level) . '">' . $this->replaceLongString($ul2->position_name) . '<hr />' . implode('<br />', $lv2Names) . '</div>'
                                                        ], 
                                                        '"' . $idLv1 . '"', 
                                                        $ul2->position_name . ($lv2Names ? ' (' . implode(', ', $lv2Names) . ')' :'')
                                                    ]);
    
                $postLv3 = PositionStructure::find()->where(['responsible_to_id' => $ul2->id])->all();
                foreach($postLv3 as $ul3){

                    $positionEmployee3 = PositionStructureEmployee::find()->where(['position_structure_id' => $ul3->id])->all();
                    $lv3Names = [];
                    foreach($positionEmployee3 as $pe3){
                        array_push($lv3Names, $pe3->employee->full_name);
                    }

                    array_push($positionStructureData, [
                                                            [
                                                                'v' => '"' . $ul3->id . '"', 
                                                                'f' => '<div class="' . $this->getLevelNamePosition($ul3->unit->level) . '">' . $this->replaceLongString($ul3->position_name) . '<hr />' . implode('<br />', $lv3Names) . '</div>'
                                                            ], 
                                                            '"' . $ul2->id . '"', 
                                                            $ul3->position_name . ($lv3Names ? ' (' . implode(', ', $lv3Names) . ')' :'')
                                                        ]);
    
                    $postLv4 = PositionStructure::find()->where(['responsible_to_id' => $ul3->id])->all();
                    foreach($postLv4 as $ul4){

                        $positionEmployee4 = PositionStructureEmployee::find()->where(['position_structure_id' => $ul4->id])->all();
                        $lv4Names = [];
                        foreach($positionEmployee4 as $pe4){
                            array_push($lv4Names, $pe4->employee->full_name);
                        }

                        array_push($positionStructureData, [
                                                                [
                                                                    'v' => '"' . $ul4->id . '"', 
                                                                    'f' => '<div class="' . $this->getLevelNamePosition($ul4->unit->level) . '">' . $this->replaceLongString($ul4->position_name) . '<hr />' . implode('<br />', $lv4Names)  . '</div>'
                                                                ], 
                                                                '"' . $ul3->id . '"', 
                                                                $ul4->position_name . ($lv4Names ? ' (' . implode(', ', $lv4Names) . ')' :'')
                                                            ]);
        
                    }
                }
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModelEmployee' => $searchModelEmployee,
            'dataProviderEmployee' => $dataProviderEmployee,
            'positionStructureData' => $positionStructureData,
            'year' => $year,
        ]);
    }

    public function actionUnit($year = null)
    {

        if(!$year){
            // $year = date('Y');
            $year = '2022';
        }

        $searchModelUnit = new UnitStructureSearch();
        $searchModelUnit->year = $year;
        $dataProviderUnit = $searchModelUnit->search(Yii::$app->request->queryParams);

        $unitStructureData = [];
        if($unitLv1 = UnitStructure::find()->where(['year' => $year])->andWhere('superior_unit_id is null and unit_id <> 329')->one()){
            $idLv1 = $unitLv1->id;
            array_push($unitStructureData, [
                                                [
                                                    'v' => '"' . $idLv1 . '"', 
                                                    'f' => '<div class="' . $this->getLevelNameUnit($unitLv1->level) . '">' .$unitLv1->unit_name  . '</div>'
                                                ], 
                                                '', 
                                                $unitLv1->unit_name
                                            ]);
            $unitLv2 = UnitStructure::find()->where(['superior_unit_id' => $idLv1])->all();
            foreach($unitLv2 as $ul2){
                array_push($unitStructureData, [
                                                    [
                                                        'v' => '"' . $ul2->id . '"', 
                                                        'f' => '<div class="' . $this->getLevelNameUnit($ul2->level) . '">' .$ul2->unit_name  . '</div>'
                                                    ], 
                                                    '"' . $idLv1 . '"', 
                                                    $ul2->unit_name
                                                ]);
    
                $unitLv3 = UnitStructure::find()->where(['superior_unit_id' => $ul2->id])->all();
                foreach($unitLv3 as $ul3){
                    array_push($unitStructureData, [
                                                        [
                                                            'v' => '"' . $ul3->id . '"',
                                                            'f' => '<div class="' . $this->getLevelNameUnit($ul3->level) . '">' .$ul3->unit_name  . '</div>'
                                                        ], 
                                                        '"' . $ul2->id . '"', 
                                                        $ul2->unit_name
                                                    ]);
    
                    $unitLv4 = UnitStructure::find()->where(['superior_unit_id' => $ul3->id])->all();
                    foreach($unitLv4 as $ul4){
                        array_push($unitStructureData, [
                                                            [
                                                                'v' => '"' . $ul4->id . '"',
                                                                'f' => '<div class="' . $this->getLevelNameUnit($ul4->level) . '">' .$ul4->unit_name  . '</div>'
                                                            ], 
                                                            '"' . $ul3->id . '"', 
                                                            $ul3->unit_name
                                                        ]);
        
                    }
                }
            }
        }

        return $this->render('unit', [
            'searchModelUnit' => $searchModelUnit,
            'dataProviderUnit' => $dataProviderUnit,
            'unitStructureData' => $unitStructureData,
            'year' => $year,
        ]);
    }

    /**
     * Displays a single PositionStructure model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'Lihat Struktur Jabatan',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new PositionStructure model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new PositionStructure();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Struktur Jabatan",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $newPositionStructure = false;
                if($model->position_id == 0){
                    $newPositionStructure = true;
                    $model->position_id = 9999;
                }
                $model->save();
                
                if($newPositionStructure){
                    $newPosition = new SdmPosition();
                    $newPosition->position_name = $model->position_name;
                    $newPosition->position_code = $model->position_code;
                    $newPosition->level = $model->level;
                    $newPosition->division_id = $model->unit->unit_id;
                    $newPosition->supposition_id = $model->resp->position_id;
                    if($newPosition->save()){
                        $model->position_id = $newPosition->id;
                        $model->save(false);
                    } else {
                        var_dump($newPosition->errors);die;
                    }
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Struktur Jabatan",
                    'content'=>'<span class="text-success">Berhasil menambahkan Struktur Jabatan</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Lagi',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tambah Struktur Jabatan",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    /**
     * Updates an existing PositionStructure model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Struktur Jabatan',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post())){

                $newPositionStructure = false;
                if($model->position_id == 0){
                    $newPositionStructure = true;
                    $model->position_id = 9999;
                }
                $model->save();
                
                if($newPositionStructure){
                    $newPosition = new SdmPosition();
                    $newPosition->position_name = $model->position_name;
                    $newPosition->position_code = $model->position_code;
                    $newPosition->level = $model->level;
                    $newPosition->division_id = $model->unit->unit_id;
                    $newPosition->supposition_id = $model->resp->position_id;
                    if($newPosition->save()){
                        $model->position_id = $newPosition->id;
                        $model->save(false);
                    } else {
                        var_dump($newPosition->errors);die;
                    }
                }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Lihat Struktur Jabatan',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                 return [
                    'title'=> 'Ubah Struktur Jabatan',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete an existing PositionStructure model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionCreateUnit($from = null)
    {
        $request = Yii::$app->request;
        $model = new UnitStructure();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Unit",
                    'content'=>$this->renderAjax('create-unit', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $newUnitStructure = false;
                if($model->unit_id == 0){
                    $newUnitStructure = true;
                    $model->unit_id = 9999;
                } else {
                    $dept = Departments::findOne($model->unit_id);
                    $dept->deptname = $model->unit_name;
                    $dept->save(false);
                }
                if($model->save(false)){
                    if($newUnitStructure){
                        $newUnit = new Departments();
                        $newUnit->company_id = 1;
                        $newUnit->branch_id = $model->branch_id;
                        $newUnit->deptname = $model->unit_name;
                        $newUnit->supdeptid = $model->sup->unit_id;
                        $newUnit->level = $model->level;
                        if($newUnit->save()){
                            $model->unit_id = $newUnit->deptid;
                            $model->save(false);
                        } else {
                            var_dump($newUnit->errors);die;
                        }
                    }
    
                    if($from == 'structure'){
                        $reload = '#structure-sitemap-pjax';
                    } else {
                        $reload = '#unit-pjax';
                    }
                } else {
                    return [
                        'title'=> "Tambah Unit",
                        'content'=>'<span class="text-danger">Gagal menambahkan Unit.<br />Error: ' . json_encode($model->errors) . '</span>',
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) 
    
                    ];
                }


                return [
                    'forceReload'=>$reload,
                    'title'=> "Tambah Unit",
                    'content'=>'<span class="text-success">Berhasil menambahkan Unit</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Lagi',['create-unit'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tambah Unit",
                    'content'=>$this->renderAjax('create-unit', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionViewUnit($id)
    {
        $request = Yii::$app->request;
        $model = UnitStructure::findOne($id);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Lihat Unit",
                    'content'=>$this->renderAjax('view-unit', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Ubah',['update-unit','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionUpdateUnit($id)
    {
        $request = Yii::$app->request;
        $model = UnitStructure::findOne($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Struktur Unit',
                    'content'=>$this->renderAjax('update-unit', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post())){

                $newUnitStructure = false;
                if($model->unit_id == 0){
                    $newUnitStructure = true;
                    $model->unit_id = 9999;
                }
                if($model->save(false)){
                    if($newUnitStructure){
                        $newUnit = new Departments();
                        $newUnit->company_id = 1;
                        $newUnit->branch_id = $model->branch_id;
                        $newUnit->deptname = $model->unit_name;
                        $newUnit->supdeptid = $model->sup->unit_id;
                        if($newUnit->save()){
                            $model->unit_id = $newUnit->deptid;
                            $model->save(false);
                        } else {
                            var_dump($newUnit->errors);die;
                        }
                    }
    
                } else {
                    return [
                        'title'=> "Tambah Unit",
                        'content'=>'<span class="text-danger">Gagal menambahkan Unit.<br />Error: ' . json_encode($model->errors) . '</span>',
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) 
    
                    ];
                }

                return [
                    'forceReload'=>'#unit-pjax',
                    'title'=> 'Lihat Struktur Unit',
                    'content'=>$this->renderAjax('view-unit', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                 return [
                    'title'=> 'Ubah Struktur Unit',
                    'content'=>$this->renderAjax('update-unit', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionDeleteUnit($id)
    {
        $request = Yii::$app->request;
        UnitStructure::findOne($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#unit-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionGetPosition($id)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = SdmPosition::findOne($id);
        
		$out = [
				'id' => $post->id,
				'postname' => $post->position_name,
				'level' => $post->level,
				'level_label' => $post->level0->level_name,
                'resp_label' => SdmPositionLevel::findOne($post->level+1)->level_name ?: '-'                
            ];

		return $out;
	}

    public function actionGetDept($id)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $dept = Departments::findOne($id);
        
		$out = [
				'id' => $dept->deptid,
				'deptname' => $dept->deptname,
				'level' => $dept->level,
				'level_label' => $dept->level == 1 ? 'Direktorat' : 
                                ($dept->level == 2 ? 'Departemen' : 
                                ($dept->level == 3 ? 'Divisi':
                                ($dept->level == 4 ? 'Bidang' : 'Squad'))),
                'sup_unit_label' => $dept->level == 2 ? 'Direktorat' : 
                                    ($dept->level == 3 ? 'Departemen' : 
                                    ($dept->level == 4 ? 'Divisi':
                                    ($dept->level == 5 ? 'Bidang' : '-'))),
                
            ];

		return $out;
	}

    public function actionCreatePositionEmployee()
    {
        $request = Yii::$app->request;
        $model = new PositionStructureEmployee();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Jabatan x Karyawan",
                    'content'=>$this->renderAjax('_from_employee', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#employee-pjax',
                    'title'=> "Tambah Jabatan x Karyawan",
                    'content'=>'<span class="text-success">Berhasil menambahkan Jabatan x Karyawan</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Lagi',['create-position-employee'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Tambah Jabatan x Karyawan",
                    'content'=>$this->renderAjax('_from_employee', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionUpdatePositionEmployee($id)
    {
        $request = Yii::$app->request;
        $model = PositionStructureEmployee::findOne($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Jabatan x Karyawan",
                    'content'=>$this->renderAjax('_from_employee', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#employee-pjax',
                    'title'=> "Ubah Jabatan x Karyawan",
                    'content'=>'<span class="text-success">Berhasil mengubah Jabatan x Karyawan</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            }else{
                return [
                    'title'=> "Ubah Jabatan x Karyawan",
                    'content'=>$this->renderAjax('_from_employee', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionDeletePositionEmployee($id)
    {
        $request = Yii::$app->request;
        PositionStructureEmployee::findOne($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#employee-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionCloneUnit()
    {
        $request = Yii::$app->request;
        $model = new UnitStructure();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Clone Struktur Unit",
                    'content'=>$this->renderAjax('clone-form', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $units = UnitStructure::find()->where(['year' => $model->clone_from])->orderBy('level')->all();
                foreach($units as $unit){
                    $check = UnitStructure::findOne(['year' => $model->clone_to, 'unit_id' => $unit->unit_id]);
                    
                    if(!$check){

                        $newUnitStructure = new UnitStructure();
                        $newUnitStructure->year = $model->clone_to;
                        $newUnitStructure->unit_id = $unit->unit_id;
                        $newUnitStructure->unit_name = $unit->unit_name;
                        $newUnitStructure->unit_code = $unit->unit_code;
                        $newUnitStructure->level = $unit->level;
                        $newUnitStructure->initiative_code = $unit->initiative_code;
                        if($unit->superior_unit_id){
                            $sup = UnitStructure::findOne(['year' => $model->clone_to, 'unit_id' => $unit->sup->unit_id]);
                            $newUnitStructure->superior_unit_id = $sup->id;
                        }
                        $newUnitStructure->save();
                    }
                }

                return [
                    'forceReload'=>'#unit-pjax',
                    'title'=> "Clone Struktur Unit",
                    'content'=>'<span class="text-success">Berhasil clone Struktur Unit</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            }else{
                return [
                    'title'=> "Clone Struktur Unit",
                    'content'=>$this->renderAjax('clone-form', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionClonePosition()
    {
        $request = Yii::$app->request;
        $model = new UnitStructure();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Clone Struktur Unit",
                    'content'=>$this->renderAjax('clone-form', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $positions = PositionStructure::find()->where(['year' => $model->clone_from])->orderBy('level desc')->all();
                foreach($positions as $position){
                    // $check = PositionStructure::findOne(['year' => $model->clone_to, 'unit_id' => $unit->unit_id]);
                    
                    // if(!$check){
                        if($unitStructure = UnitStructure::findOne(['unit_id' => $position->unit->unit_id, 'year' => $model->clone_to])){
                            $newPositionStructure = new PositionStructure();
                            $newPositionStructure->year = $model->clone_to;
                            $newPositionStructure->position_id = $position->position_id;
                            $newPositionStructure->position_name = $position->position_name;
                            $newPositionStructure->position_code = $position->position_code;
                            $newPositionStructure->level = $position->level;
                            $newPositionStructure->role = $position->role;
                            $newPositionStructure->responsibility = $position->responsibility;
                            $newPositionStructure->authority = $position->authority;

                            if($position->responsible_to_id){
                                $resp = PositionStructure::findOne(['year' => $model->clone_to, 'position_id' => $position->resp->position_id]);
                                $newPositionStructure->responsible_to_id = $resp->id;
                            }
                            $newPositionStructure->unit_structure_id = $unitStructure->id;
                            
                            $newPositionStructure->save();
                        }
                    // }
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Clone Struktur Jabatan",
                    'content'=>'<span class="text-success">Berhasil clone Struktur Jabatan</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            }else{
                return [
                    'title'=> "Clone Struktur Unit",
                    'content'=>$this->renderAjax('clone-form', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionCloneEmployeePosition()
    {
        $request = Yii::$app->request;
        $model = new UnitStructure();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Clone Struktur Unit",
                    'content'=>$this->renderAjax('clone-form', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $positions = PositionStructure::find()->where(['year' => $model->clone_from])->orderBy('level')->all();
                foreach($positions as $position){
                    // $check = PositionStructure::findOne(['year' => $model->clone_to, 'unit_id' => $unit->unit_id]);
                    
                    // if(!$check){

                        $newPositionStructure = new PositionStructure();
                        $newPositionStructure->year = $model->clone_to;
                        $newPositionStructure->position_id = $position->position_id;
                        $newPositionStructure->position_name = $position->position_name;
                        $newPositionStructure->position_code = $position->position_code;
                        $newPositionStructure->level = $position->level;
                        $newPositionStructure->role = $position->role;
                        $newPositionStructure->responsibility = $position->responsibility;
                        $newPositionStructure->authority = $position->authority;

                        if($position->responsible_to_id){
                            $resp = PositionStructure::findOne(['year' => $model->clone_to, 'position_id' => $position->resp->position_id]);
                            $newPositionStructure->responsible_to_id = $resp->id;
                        }

                        if($unitStructure = UnitStructure::findOne(['unit_id' => $position->unit->unit_id, 'year' => $model->clone_to])){
                            $newPositionStructure->unit_structure_id = $unitStructure->id;
                        }

                        $newPositionStructure->save();
                    // }
                }

                return [
                    'forceReload'=>'#unit-pjax',
                    'title'=> "Clone Struktur Unit",
                    'content'=>'<span class="text-success">Berhasil clone Struktur Unit</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            }else{
                return [
                    'title'=> "Clone Struktur Unit",
                    'content'=>$this->renderAjax('clone-form', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            return $this->redirect(['index']);
        }

    }

    public function actionGetUnit() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $year = $_POST['depdrop_parents'];

            $out = [];
            $units = UnitStructure::find()->where(['year' => $year])->orderBy('unit_code')->all();
            foreach($units as $unit){
                array_push($out, ['id' => $unit->id, 'name' => $unit->unit_code . ' - ' . $unit->unit_name]);
            }
        }
        return ['output'=>$out, 'selected'=>''];
    }

    public function actionGetPositions() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $unitId = $_POST['depdrop_parents'];

            $out = [];
            $positions = PositionStructure::find()->where(['unit_structure_id' => $unitId])->orderBy('level desc')->all();
            foreach($positions as $position){
                array_push($out, ['id' => $position->id, 'name' => $position->position_name]);
            }
        }
        return ['output'=>$out, 'selected'=>''];
    }

    public function actionGetPositions2() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $unitId = $_POST['depdrop_parents'];

            
            $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['is_active' => true])->andWhere(['unit_id' => $unitId])->one();
            $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['is_active' => true])->andWhere(['unit_id' => $unitId])->one();

            if($isGeneralManager){
                $level = "level <> 6";
            } else if($isManager){
                $level = "level <> 4";
            }

            $out = [];
            $positions = PositionStructure::find()->where(['unit_structure_id' => $unitId])->andWhere($level)->orderBy('level desc')->all();
            foreach($positions as $position){
                array_push($out, ['id' => $position->id, 'name' => $position->position_name]);
            }
        }
        return ['output'=>$out, 'selected'=>''];
    }

    public function actionGetUnitsByYear() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $year = $_POST['depdrop_parents'];

            $unitId = [];
            $isManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['year' => $year])->andWhere(['is_active' => true])->one();
            $isGeneralManager = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['year' => $year])->andWhere(['is_active' => true])->one();
            $isVicePrecident = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 8])->andWhere(['year' => $year])->andWhere(['is_active' => true])->one();
            if($isManager){
                $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 4])->andWhere(['year' => $year])->all();
                foreach($vwEmployeeManagers as $vwEmployeeManager){
                    array_push($unitId, $vwEmployeeManager->unit_id);
                    $units = UnitStructure::find()->where(['superior_unit_id' => $vwEmployeeManager->unit_id])->all();
                    foreach($units as $u){
                        array_push($unitId, $u->id);
                    }
                }


            } 
            if($isGeneralManager){
                $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 6])->andWhere(['year' => $year])->all();
                foreach($vwEmployeeManagers as $vwEmployeeManager){
                    $units = UnitStructure::find()->where(['superior_unit_id' => $vwEmployeeManager->unit_id])->all();
                    foreach($units as $u){
                        array_push($unitId, $u->id);
                    }
                }

                if(Yii::$app->user->id == 38080){
                    $employeePositions = PositionStructureEmployee::find()->where(['employee_id' => Yii::$app->user->id])->all();
                    foreach($employeePositions as $ep){
                        // array_push($unitId, $ep->position->unit_structure_id);

                        if($ep->position->level == 6){
                            $gms = PositionStructure::find()->where(['responsible_to_id' => $ep->position_structure_id])->all();
                            foreach($gms as $gm){
                                array_push($unitId, $gm->unit_structure_id);
                            }
                        }

                    }
                }

            }
            if($isVicePrecident){
                $vwEmployeeManagers = VwEmployeeManager::find()->where(['employee_id' => Yii::$app->user->id])->andWhere(['level' => 8])->andWhere(['year' => $year])->all();
                foreach($vwEmployeeManagers as $vwEmployeeManager){
                    $units = UnitStructure::find()->where(['superior_unit_id' => $vwEmployeeManager->unit_id])->all();
                    foreach($units as $u){
                        array_push($unitId, $u->id);
                    }
                }

            }

            $out = [];
            $units = UnitStructure::find()
                                            ->where(['id' => $unitId])
                                            ->all();

            foreach($units as $unit){
                array_push($out, ['id' => $unit->id, 'name' => $unit->unit_name]);
            }
        }
        return ['output'=>$out, 'selected'=>''];
    }

    public function actionGetPositionsByYear() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $year = $_POST['depdrop_parents'];

            $out = [];
            $positions = PositionStructureEmployee::find()
                                                    ->alias('pse')
                                                    ->joinWith('position p')
                                                    ->where(['p.year' => $year])
                                                    ->andWhere(['employee_id' => Yii::$app->user->id])->all();
            foreach($positions as $position){
                array_push($out, ['id' => $position->position_structure_id, 'name' => $position->position->position_name]);
            }
        }
        return ['output'=>$out, 'selected'=>''];
    }

    public function actionGetPersons() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $positionId = $_POST['depdrop_parents'];

            $out = [];
            $positionEmployees = PositionStructureEmployee::find()->where(['position_structure_id' => $positionId])->all();
            foreach($positionEmployees as $positionEmployee){
                array_push($out, ['id' => $positionEmployee->employee_id, 'name' => $positionEmployee->employee->full_name]);
            }
        }
        return ['output'=>$out, 'selected'=>''];
    }

    /**
     * Finds the PositionStructure model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PositionStructure the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PositionStructure::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getLevelNamePosition($level){
        if($level == 1){
            return 'directorate-box';
        } else if($level == 2){
            return 'department-box';
        } else if($level == 3){
            return 'division-box';
        } else if($level == 4){
            return 'sub-division-box';
        } else if($level == 5){
            return 'squad-box';
        } else {
            return 'default-box';
        }
    }


    protected function getLevelNameUnit($level){
        if($level == 1){
            return 'directorate-box';
        } else if($level == 2){
            return 'department-box';
        } else if($level == 3){
            return 'division-box';
        } else if($level == 4){
            return 'bidang-box';
        } else if($level == 5){
            return 'squad-box';
        } else {
            return 'default-box';
        }
    }

    protected function replaceLongString($string){
        // if(strlen($string) > 15){
        //     return substr($string, 0, 12) . '...';
        // } else {
            return $string;
        // }
    }
}
