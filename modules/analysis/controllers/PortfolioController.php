<?php

namespace app\modules\analysis\controllers;

use Yii;
use app\models\Portfolio;
use app\models\PortfolioCanvas;
use app\models\PortfolioElement;
use app\models\search\PortfolioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * PortfolioController implements the CRUD actions for Portfolio model.
 */
class PortfolioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Portfolio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PortfolioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Portfolio model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'Model Bisnis',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $model
            ]);
        }
    }

    /**
     * Creates a new Portfolio model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Portfolio();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah Model Bisnis',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['view', 'id' => $model->id]));
                return [
                    // 'forceReload'=>'#crud-datatable-pjax',
                    'title'=> 'Tambah Model Bisnis',
                    'content'=>'<span class="text-success">Berhasil membuat Model Bisnis</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            }else{
                return [
                    'title'=> 'Tambah Model Bisnis',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Portfolio model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $from = null)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Model Bisnis',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){

                if($from == 'view'){
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['view', 'id' => $model->id]));
                    return [
                        'title'=> 'Model Bisnis',
                        'content'=>'<span class="text-success">Berhasil mengubah Model Bisnis</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                    ];
                } else {
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> 'Model Bisnis',
                        'content'=>$this->renderAjax('view', [
                            'model' => $model,
                        ]),
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                    ];
                }

            }else{
                 return [
                    'title'=> 'Ubah Model Bisnis',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Portfolio model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $from = null)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($from == 'view'){
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index']));
            return [
                'title' => 'Info',
                'content' => '<span class="text-success">Berhasil menghapus Model Bisnis!</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function actionCreateCanvas($id, $element_id)
    {
        $request = Yii::$app->request;
        $model = new PortfolioCanvas();
        $model->portfolio_id = $id;
        $model->element_id = $element_id;
        $element = PortfolioElement::findOne($element_id)->element;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah ' . $element,
                    'content'=>$this->renderAjax('_form_canvas', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#' . str_replace('/', '_', strtolower(str_replace(' ', '_', $element))) . '-pjax',
                    'title'=> 'Tambah ' . $element . '',
                    'content'=>'<span class="text-success">Berhasil menambah ' . $element . '</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Tambah Lagi',['create-canvas', 'id' => $id, 'element_id' => $element_id],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            } else {
                var_dump($model->errors);die;
            }
        }else{
            return $this->redirect(['view', 'id' => $id]);
        }
    }

    public function actionViewCanvas($id){
        $request = Yii::$app->request;
        $model = PortfolioCanvas::findOne($id);
        $portfolioId = $model->portfolio_id;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'Lihat',
                    'content'=>$this->renderAjax('view-canvas', [
                        'model' => $model
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            '<div class="btn-group">' .
                            Html::a(
                                'Hapus', 
                                ['delete-canvas', 'id' => $model->id, 'from' => 'view'], 
                                [
                                    'class' => 'btn btn-danger',
                                    'title' => 'Hapus data ini',
                                    'role'=>'modal-remote',
                                    'data-confirm'=>false, 
                                    'data-method'=>false,
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Konfirmasi',
                                    'data-confirm-message'=>'Yakin ingin menghapus data ini?'
                                ]
                            ) .
                            Html::a('Ubah',['update-canvas','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']) .
                            '</div>'
                ];
        }else{
            return $this->redirect(['view', 'id' => $portfolioId]);
        }
    }

    public function actionUpdateCanvas($id)
    {
        $request = Yii::$app->request;
        $model = PortfolioCanvas::findOne($id);
        $element = PortfolioElement::findOne($model->element_id)->element;
        $portfolioId = $model->portfolio_id;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah ' . $element,
                    'content'=>$this->renderAjax('_form_canvas', [
                        'model' => $model,
                    ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#' . str_replace('/', '_', strtolower(str_replace(' ', '_', $element))) . '-pjax',
                    'title'=> 'Ubah ' . $element . '',
                    'content'=>'<span class="text-success">Berhasil mengubah ' . $element . '</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            } else {
                var_dump($model->errors);die;
            }
        }else{
            return $this->redirect(['view', 'id' => $portfolioId]);
        }
    }

    public function actionDeleteCanvas($id)
    {
        $request = Yii::$app->request;
        $model = PortfolioCanvas::findOne($id);
        $element = PortfolioElement::findOne($model->element_id)->element;
        $portfolioId = $model->portfolio_id;
        $model->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'forceClose'=>true,
                'forceReload'=>'#' . str_replace('/', '_', strtolower(str_replace(' ', '_', $element))) . '-pjax',
                'title'=> 'Ubah ' . $element . '',
                'content'=>'<span class="text-success">Berhasil menghapus ' . $element . '</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

            ];
        }else{
            return $this->redirect(['view', 'id' => $portfolioId]);
        }
    }

    /**
     * Finds the Portfolio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Portfolio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Portfolio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
