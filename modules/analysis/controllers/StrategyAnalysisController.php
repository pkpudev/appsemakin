<?php

namespace app\modules\analysis\controllers;

use app\models\Opportunities;
use Yii;
use app\models\StrategyAnalysis;
use app\models\search\StrategyAnalysisSearch;
use app\models\SituationAnalysis;
use app\models\Strategies;
use app\models\Strengths;
use app\models\Threats;
use app\models\Weaknesses;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * StrategyAnalysisController implements the CRUD actions for StrategyAnalysis model.
 */
class StrategyAnalysisController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all StrategyAnalysis models.
     * @return mixed
     */
    public function actionIndex($situationAnalysisId = null)
    {

        if(!$situationAnalysisId){
            if($lastSituationAnalysisId = SituationAnalysis::find()->orderBy('created_at desc')->one()){
                $situationAnalysisId = $lastSituationAnalysisId->id;
            }
        }

        $searchModelSo = new StrategyAnalysisSearch();
        $searchModelSo->strategy_type = 'SO';
        $searchModelSo->situation_analysis_id = $situationAnalysisId;
        $dataProviderSo = $searchModelSo->search(Yii::$app->request->queryParams);

        $searchModelWo = new StrategyAnalysisSearch();
        $searchModelWo->strategy_type = 'WO';
        $searchModelWo->situation_analysis_id = $situationAnalysisId;
        $dataProviderWo = $searchModelWo->search(Yii::$app->request->queryParams);

        $searchModelSt = new StrategyAnalysisSearch();
        $searchModelSt->strategy_type = 'ST';
        $searchModelSt->situation_analysis_id = $situationAnalysisId;
        $dataProviderSt = $searchModelSt->search(Yii::$app->request->queryParams);

        $searchModelWt = new StrategyAnalysisSearch();
        $searchModelWt->strategy_type = 'WT';
        $searchModelWt->situation_analysis_id = $situationAnalysisId;
        $dataProviderWt = $searchModelWt->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModelSo' => $searchModelSo,
            'dataProviderSo' => $dataProviderSo,
            'searchModelWo' => $searchModelWo,
            'dataProviderWo' => $dataProviderWo,
            'searchModelSt' => $searchModelSt,
            'dataProviderSt' => $dataProviderSt,
            'searchModelWt' => $searchModelWt,
            'dataProviderWt' => $dataProviderWt,
            'situationAnalysisId' => $situationAnalysisId,
        ]);
    }


    /**
     * Displays a single StrategyAnalysis model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'Lihat Strategi Analisis (' . $model->strategy_type . ')',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new StrategyAnalysis model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /* public function actionCreate($type, $strategyId = null)
    {
        $request = Yii::$app->request;

        $model = new StrategyAnalysis();

        $internalFactor = $type[0];
        $externalFactor = $type[1];

        if($internalFactor == 'S'){
            $internalFactorLabel = 'Faktor Internal (Strength)';
        } else if($internalFactor == 'W'){
            $internalFactorLabel = 'Faktor Internal (Weakness)';
        }

        if($externalFactor == 'O'){
            $externalFactorLabel = 'Faktor Eksternal (Opportunity)';
        } else if($externalFactor == 'T'){
            $externalFactorLabel = 'Faktor Eksternal (Threat)';
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah Analisis Strategi (' . $type . ')',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'type' => $type,
                        'strategyId' => $strategyId,
                        'internalFactor' => $internalFactor,
                        'externalFactor' => $externalFactor,
                        'internalFactorLabel' => $internalFactorLabel,
                        'externalFactorLabel' => $externalFactorLabel,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['id' => 'btn-submit', 'class'=>'btn btn-primary','type'=>"submit", 'disabled' => !$strategyId])

                ];
            }else if($model->load($request->post())){

                $internalFactors = $request->post('StrategyAnalysis')['internal_factor_id'];
                $externalFactors = $request->post('StrategyAnalysis')['external_factor_id'];

                foreach($internalFactors as $if){
                    foreach($externalFactors as $ef){
                        if(!$check = StrategyAnalysis::findOne(['strategy_type' => $type, 'internal_factor_id' => $if, 'external_factor_id' => $ef, 'strategy_id' => $model->strategy_id])){
                            $newStrategyAnalysis = new StrategyAnalysis();
                            $newStrategyAnalysis->strategy_type = $type;
                            $newStrategyAnalysis->internal_factor_id = $if;
                            $newStrategyAnalysis->external_factor_id = $ef;
                            $newStrategyAnalysis->strategy_id = $model->strategy_id;
                            $newStrategyAnalysis->save();
                        }
                    }
                }

                return [
                    'forceReload'=>'#' . strtolower($type) . '-pjax',
                    'title'=> 'Tambah Analisis Strategi (' . $type . ')',
                    'content'=>'<span class="text-success">Berhasil menambah Analisis Strategi (' . $type . ')</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    } */

    public function actionCreate($type, $situationAnalysisId = null)
    {
        $request = Yii::$app->request;

        if(!$situationAnalysisId){
            if($lastSituationAnalysisId = SituationAnalysis::find()->orderBy('created_at desc')->one()){
                $situationAnalysisId = $lastSituationAnalysisId->id;
            }
        }

        $model = new StrategyAnalysis();

        $internalFactor = $type[0];
        $externalFactor = $type[1];

        if($internalFactor == 'S'){
            $internalFactorLabel = 'Faktor Internal (Strength)';
        } else if($internalFactor == 'W'){
            $internalFactorLabel = 'Faktor Internal (Weakness)';
        }

        if($externalFactor == 'O'){
            $externalFactorLabel = 'Faktor Eksternal (Opportunity)';
        } else if($externalFactor == 'T'){
            $externalFactorLabel = 'Faktor Eksternal (Threat)';
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah Analisis Strategi (' . $type . ')',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'type' => $type,
                        'situationAnalysisId' => $situationAnalysisId,
                        'internalFactor' => $internalFactor,
                        'externalFactor' => $externalFactor,
                        'internalFactorLabel' => $internalFactorLabel,
                        'externalFactorLabel' => $externalFactorLabel,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['id' => 'btn-submit', 'class'=>'btn btn-primary','type'=>"submit", 'disabled' => !$situationAnalysisId])

                ];
            }else if($model->load($request->post())){

                $internalFactors = $request->post('StrategyAnalysis')['internal_factor_id'];
                $externalFactors = $request->post('StrategyAnalysis')['external_factor_id'];

                $strategyId = $model->strategy_id;

                if($strategyId == '-' && $model->strategyText){
                    $newStrategy = new Strategies();
                    $newStrategy->situation_analysis_id = $situationAnalysisId;
                    $newStrategy->strategy = $model->strategyText;
                    if($newStrategy->save()){
                        $strategyId = $newStrategy->id;
                    }
                } else if($strategyId == '-' && !$model->strategyText){
                    return [
                        'title'=> 'Info',
                        'content'=>'<span class="text-danger">Jika anda memilih Tambah Strategi Baru, harap isi field text Strategi Baru!</span>',
                        'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                }

                foreach($internalFactors as $if){
                    foreach($externalFactors as $ef){
                        if(!$check = StrategyAnalysis::findOne(['strategy_type' => $type, 'internal_factor_id' => $if, 'external_factor_id' => $ef, 'strategy_id' => $strategyId])){
                            $newStrategyAnalysis = new StrategyAnalysis();
                            $newStrategyAnalysis->strategy_type = $type;
                            $newStrategyAnalysis->internal_factor_id = $if;
                            $newStrategyAnalysis->external_factor_id = $ef;
                            $newStrategyAnalysis->strategy_id = $strategyId;
                            $newStrategyAnalysis->save();
                        }
                    }
                }

                return [
                    'forceReload'=>'#' . strtolower($type) . '-pjax',
                    'title'=> 'Tambah Analisis Strategi (' . $type . ')',
                    'content'=>'<span class="text-success">Berhasil menambah Analisis Strategi (' . $type . ')</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                ];
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    public function actionCreateStrategy($type)
    {
        $request = Yii::$app->request;
        $model = new Strategies();

        $internalFactor = $type[0];
        $externalFactor = $type[1];

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Tambah Strategi (' . $type . ')',
                    'content'=>$this->renderAjax('_form-strategy', [
                        'model' => $model,
                        'type' => $type,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            } else if($model->load($request->post())){

                if($model->save()){
                    return [
                        'title'=> 'Tambah Strategi',
                        'content'=>'Berhasil menambahkan Strategi',
                        'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('<i class="fa fa-plus"></i> Tambah Analisis Strategi (' . $type .')', ['create', 'type' => $type, 'strategyId' => $model->id],
                        ['role'=>'modal-remote','title'=> 'Tambah Analisis Strategi (' . $type .')','class'=>'btn btn-success'])
    
                    ];
                } else {
                    return [
                        'title'=> 'Tambah Analisis Strategi (' . $type . ')',
                        'content'=>'<span class="text-danger">Gagal menambah Strategi (' . json_encode($model->errors) . ')</span>',
                        'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
    
                    ];
                }

            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Updates an existing StrategyAnalysis model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $situationAnalysisId = $model->strategy->situation_analysis_id;
        $type = $model->strategy_type;

        $internalFactor = $type[0];
        $externalFactor = $type[1];

        if($type[0] == 'S'){
            $initValueInternal = Strengths::findOne($model->internal_factor_id)->strength;
        } else if($type[0] == 'W'){
            $initValueInternal = Weaknesses::findOne($model->internal_factor_id)->weakness;
        } 

        if($type[1] == 'O'){
            $initValueExternal = Opportunities::findOne($model->external_factor_id)->opportunity;
        } else if($type[0] == 'T'){
            $initValueExternal = Threats::findOne($model->external_factor_id)->threat;
        } 

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Analisis Strategi',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'internalFactor' => $internalFactor,
                        'externalFactor' => $externalFactor,
                        'initValueInternal' => $initValueInternal,
                        'initValueExternal' => $initValueExternal,
                        'situationAnalysisId' => $situationAnalysisId,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post())){

                if(!$check = StrategyAnalysis::findOne(['strategy_type' => $type, 'internal_factor_id' => $model->internal_factor_id, 'external_factor_id' => $model->external_factor_id, 'strategy_id' => $model->strategy_id])){
                    $model->save();

                    return [
                        'forceReload'=>'#' . strtolower($type) . '-pjax',
                        'title'=> 'Ubah Analisis Strategi',
                        'content'=>'<span class="text-success">Berhasil mengubah Analisis Strategi (' . $type . ')</span>',
                        'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                } else {
                    return [
                        'title'=> 'Ubah Analisis Strategi',
                        'content'=>'<span class="text-danger">Gagal mengubah Analisis Strategi (Data sudah ada)</span>',
                        'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
                }
            }
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete an existing StrategyAnalysis model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $type = $model->strategy_type;
        $model->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#' . strtolower($type) . '-pjax',];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    

    public function actionGetSituationAnalysis($q = null)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $filter = "title ilike '%{$q}%'";
        $select = "id, title As text";
        $query = SituationAnalysis::find()
            ->select($select)
            ->offset(0)
            ->limit(50);
        
		$out = [
			'items' => [
				'id' => '',
				'text' => '',
			]
		];

		if (!is_null($q)) {
			$query->andWhere($filter);
			$situtation = $query->asArray()->all();
			$out['items'] = $situtation;
		}

		return $out;
	}

    public function actionGetFactors($q = null, $id, $type)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $strategy = Strategies::findOne($id);
        $situationAnalysisId = $strategy->situation_analysis_id;

        if($type == 'S'){
            $filter = "strength ilike '%{$q}%'";
            $select = "id, strength As text";
            $query = Strengths::find()
                ->select($select)
                ->where(['situation_analysis_id' => $situationAnalysisId])
                ->offset(0)
                ->limit(50)
                ->orderBy(['strength' => SORT_ASC]);
        } else if($type == 'O'){
            $filter = "opportunity ilike '%{$q}%'";
            $select = "id, opportunity As text";
            $query = Opportunities::find()
                ->select($select)
                ->where(['situation_analysis_id' => $situationAnalysisId])
                ->offset(0)
                ->limit(50)
                ->orderBy(['opportunity' => SORT_ASC]);
        } else if($type == 'W'){
            $filter = "weakness ilike '%{$q}%'";
            $select = "id, weakness As text";
            $query = Weaknesses::find()
                ->select($select)
                ->where(['situation_analysis_id' => $situationAnalysisId])
                ->offset(0)
                ->limit(50)
                ->orderBy(['weakness' => SORT_ASC]);
        } else if($type == 'T'){
            $filter = "threat ilike '%{$q}%'";
            $select = "id, threat As text";
            $query = Threats::find()
                ->select($select)
                ->where(['situation_analysis_id' => $situationAnalysisId])
                ->offset(0)
                ->limit(50)
                ->orderBy(['threat' => SORT_ASC]);
        }

		$out = [
			'items' => [
				'id' => '',
				'text' => '',
			]
		];

		if (!is_null($q)) {
			$query->andWhere($filter);
			$factors = $query->asArray()->all();
			$out['items'] = $factors;
		} /* else if ($id > 0) {
			$out['items'] = [
				'id' => $id,
				'text' => SdmEmployee::find($id)->full_name,
			];
		} */
		return $out;
	}

    /**
     * Finds the StrategyAnalysis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StrategyAnalysis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StrategyAnalysis::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
