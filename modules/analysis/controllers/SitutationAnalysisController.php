<?php

namespace app\modules\analysis\controllers;

use app\components\FilesHelper;
use app\models\ExternalVariable;
use app\models\Files;
use app\models\InternalVariable;
use app\models\Opportunities;
use app\models\search\ExternalVariableSearch;
use app\models\search\InternalVariableSearch;
use app\models\search\OpportunitiesSearch;
use Yii;
use app\models\SituationAnalysis;
use app\models\search\SituationAnalysisSearch;
use app\models\search\StrengthSearch;
use app\models\search\ThreatsSearch;
use app\models\search\WeaknessesSearch;
use app\models\SituationAnalysisFiles;
use app\models\Strengths;
use app\models\Threats;
use app\models\Weaknesses;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * SitutationAnalysisController implements the CRUD actions for SituationAnalysis model.
 */
class SitutationAnalysisController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'delete-swot' => ['post'],
                    'delete-internal-variable' => ['post'],
                    'delete-external-variable' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SituationAnalysis models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SituationAnalysisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SituationAnalysis model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $strengthSearchModel = new StrengthSearch();
        $strengthSearchModel->situation_analysis_id = $id;
        $dataProviderStrength = $strengthSearchModel->search(Yii::$app->request->queryParams);

        $weaknessSearchModel = new WeaknessesSearch();
        $weaknessSearchModel->situation_analysis_id = $id;
        $dataProviderWeakness = $weaknessSearchModel->search(Yii::$app->request->queryParams);

        $opportunitySearchModel = new OpportunitiesSearch();
        $opportunitySearchModel->situation_analysis_id = $id;
        $dataProviderOpportunity = $opportunitySearchModel->search(Yii::$app->request->queryParams);

        $threatSearchModel = new ThreatsSearch();
        $threatSearchModel->situation_analysis_id = $id;
        $dataProviderThreat = $threatSearchModel->search(Yii::$app->request->queryParams);
        
        /* $dataProviderStrength = new ActiveDataProvider([
            'query' => Strengths::find()->where(['situation_analysis_id' => $id]),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['internal_variable_id' => SORT_ASC]]
        ]);

        $dataProviderWeakness = new ActiveDataProvider([
            'query' => Weaknesses::find()->where(['situation_analysis_id' => $id]),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['internal_variable_id' => SORT_ASC]]
        ]);

        $dataProviderOpportunity = new ActiveDataProvider([
            'query' => Opportunities::find()->where(['situation_analysis_id' => $id]),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['external_variable_id' => SORT_ASC]]
        ]);

        $dataProviderThreat = new ActiveDataProvider([
            'query' => Threats::find()->where(['situation_analysis_id' => $id]),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['external_variable_id' => SORT_ASC]]
        ]); */

        $dataProviderFile = new ActiveDataProvider([
            'query' => SituationAnalysisFiles::find()->where(['situation_analysis_id' => $id]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render('view', [
            'model' => $this->findModel($id),
            'strengthSearchModel' => $strengthSearchModel,
            'weaknessSearchModel' => $weaknessSearchModel,
            'opportunitySearchModel' => $opportunitySearchModel,
            'threatSearchModel' => $threatSearchModel,
            'dataProviderStrength' => $dataProviderStrength,
            'dataProviderWeakness' => $dataProviderWeakness,
            'dataProviderOpportunity' => $dataProviderOpportunity,
            'dataProviderThreat' => $dataProviderThreat,
            'dataProviderFile' => $dataProviderFile,
        ]);
    }

    /**
     * Creates a new SituationAnalysis model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new SituationAnalysis();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Buat Analisis Situasi",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-success','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Buat Analisis Situasi",
                    'content'=>'<span class="text-success">Berhasil menambahkan Analisis Situasi</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])

                ];
            }else{
                return [
                    'title'=> "Buat Analisis Situasi",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-success','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing SituationAnalysis model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Ubah Analisis Situasi',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-success','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['view', 'id' => $model->id]));
                return [
                    'title' => "Info",
                    'content' => '<span class="text-success">Berhasil mengubah Situasi Analisis</span>',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            }else{
                 return [
                    'title'=> 'Ubah Analisis Situasi',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-success','type'=>"submit"])
                ];
            }
        }else{
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    /**
     * Delete an existing SituationAnalysis model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $from = null)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($from == 'view'){
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index']));
            return [
                'title' => 'Info',
                'content' => '<span class="text-success">Berhasil menghapus Analisis Situasi!</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /** SWOT */
    public function actionViewSwot($id, $column)
    {
        $request = Yii::$app->request;

        $field1 = $column;
        if($column == 'strength'){
            $model = Strengths::findOne($id);
            $field2 = 'internal_variable_id';
        } else if($column == 'weakness'){
            $model = Weaknesses::findOne($id);
            $field2 = 'internal_variable_id';
        } else if($column == 'opportunity'){
            $model = Opportunities::findOne($id);
            $field2 = 'external_variable_id';
        } else if($column == 'threat'){
            $model = Threats::findOne($id);
            $field2 = 'external_variable_id';
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Lihat " . ucfirst($column),
                    'content'=>$this->renderAjax('_view_swot', [
                        'model' => $model,
                        'field1' => $field1,
                        'field2' => $field2,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Ubah ' . ucfirst($column),
                                ['update-swot', 'id' => $model->situation_analysis_id, 'swotId' => $model->id, 'column' => $column], 
                                [
                                    'role' => 'modal-remote',
                                    'class' => 'btn btn-warning'
                                ]
                            )

                ];
            }
        }else{
            return $this->redirect(['view', 'id' => $id]);
        }
    }

    public function actionAddSwot($id, $column)
    {
        $request = Yii::$app->request;

        $field1 = $column;
        if($column == 'strength'){
            $model = new Strengths();
            $field2 = 'internal_variable_id';
        } else if($column == 'weakness'){
            $model = new Weaknesses();
            $field2 = 'internal_variable_id';
        } else if($column == 'opportunity'){
            $model = new Opportunities();
            $field2 = 'external_variable_id';
        } else if($column == 'threat'){
            $model = new Threats();
            $field2 = 'external_variable_id';
        }

        $model->situation_analysis_id = $id;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah " . ucfirst($column),
                    'content'=>$this->renderAjax('_form_swot', [
                        'model' => $model,
                        'field1' => $field1,
                        'field2' => $field2,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-success','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#' . $column . '-table-pjax',
                    'title'=> "Tambah " . ucfirst($column),
                    'content'=>'<span class="text-success">Berhasil menambahkan ' . ucfirst($column) . '</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]) .
                                Html::a('Tambah ' . ucfirst($column) . ' Lagi',
                                ['add-swot', 'id' => $id, 'column' => $column], 
                                [
                                    'role' => 'modal-remote',
                                    'class' => 'btn btn-warning'
                                ])

                ];
            }
        }else{
            return $this->redirect(['view', 'id' => $id]);
        }
    }

    public function actionUpdateSwot($id, $swotId, $column)
    {
        $request = Yii::$app->request;

        $field1 = $column;
        if($column == 'strength'){
            $model = Strengths::findOne($swotId);
            $field2 = 'internal_variable_id';
        } else if($column == 'weakness'){
            $model = Weaknesses::findOne($swotId);
            $field2 = 'internal_variable_id';
        } else if($column == 'opportunity'){
            $model = Opportunities::findOne($swotId);
            $field2 = 'external_variable_id';
        } else if($column == 'threat'){
            $model = Threats::findOne($swotId);
            $field2 = 'external_variable_id';
        }

        $model->situation_analysis_id = $id;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah " . ucfirst($column),
                    'content'=>$this->renderAjax('_form_swot', [
                        'model' => $model,
                        'field1' => $field1,
                        'field2' => $field2,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-success','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#' . $column . '-table-pjax',
                    'title'=> "Ubah " . ucfirst($column),
                    'content'=>'<span class="text-success">Berhasil mengubah ' . ucfirst($column) . '</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])

                ];
            }
        }else{
            return $this->redirect(['view', 'id' => $id]);
        }
    }

    public function actionDeleteSwot($id, $swotId, $column)
    {
        $request = Yii::$app->request;

        if($column == 'strength'){
            $model = Strengths::findOne($swotId);
        } else if($column == 'weakness'){
            $model = Weaknesses::findOne($swotId);
        } else if($column == 'opportunity'){
            $model = Opportunities::findOne($swotId);
        } else if($column == 'threat'){
            $model = Threats::findOne($swotId);
        }

        $model->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#' . $column . '-table-pjax'];
        }else{
            return $this->redirect(['view', 'id' => $id]);
        }
    }

    /** SETTING */
    public function actionInternalVariable()
    {
        $searchModel = new InternalVariableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-setting-internal', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddInternalVariable()
    {
        $request = Yii::$app->request;
        $model = new InternalVariable();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Variabel Internal",
                    'content'=>$this->renderAjax('_form_variable', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-success','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Variabel Internal",
                    'content'=>'<span class="text-success">Berhasil menambahkan Variabel Internal</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])

                ];
            }
        }else{
            return $this->redirect(['index-internal-variable']);
        }
    }

    public function actionUpdateInternalVariable($id)
    {
        $request = Yii::$app->request;
        $model = InternalVariable::findOne($id);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Variabel Internal",
                    'content'=>$this->renderAjax('_form_variable', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-success','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Variabel Internal",
                    'content'=>'<span class="text-success">Berhasil mengubah Variabel Internal</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])

                ];
            }
        }else{
            return $this->redirect(['index-internal-variable']);
        }
    }

    public function actionDeleteInternalVariable($id)
    {
        $request = Yii::$app->request;
        InternalVariable::findOne($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }


    }

    public function actionExternalVariable()
    {
        $searchModel = new ExternalVariableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-setting-external', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddExternalVariable()
    {
        $request = Yii::$app->request;
        $model = new ExternalVariable();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Tambah Variabel Eksternal",
                    'content'=>$this->renderAjax('_form_variable', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-success','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Tambah Variabel Eksternal",
                    'content'=>'<span class="text-success">Berhasil menambahkan Variabel Eksternal</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])

                ];
            }
        }else{
            return $this->redirect(['index-external-variable']);
        }
    }

    public function actionUpdateExternalVariable($id)
    {
        $request = Yii::$app->request;
        $model = ExternalVariable::findOne($id);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Ubah Variabel Eksternal",
                    'content'=>$this->renderAjax('_form_variable', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-success','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ubah Variabel Eksternal",
                    'content'=>'<span class="text-success">Berhasil mengubah Variabel Eksternal</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])

                ];
            }
        }else{
            return $this->redirect(['index-external-variable']);
        }
    }

    public function actionDeleteExternalVariable($id)
    {
        $request = Yii::$app->request;
        ExternalVariable::findOne($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }


    }

    public function actionAddFiles($id){
        $request = Yii::$app->request;

        $model = new SituationAnalysisFiles();
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isAjax) {
            if ($request->isGet) {
                return [
                    'title' => "Tambah Lampiran",
                    'content' => $this->renderAjax('_form-file', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Tambah', ['class' => 'btn btn-primary pull-right', 'type' => 'submit'])
                ];
            } else if($model->load($request->post())){

                $model->files = UploadedFile::getInstances($model, 'files');
                FilesHelper::upload($model->files, $id, 'situation_analysis');

                return [
                    'forceReload'=>'#situtation-files-pjax',
                    'title' => "<b>Info</b>",
                    'content' => 'Berhasil menambah Lampiran Analisis Situasi!',
                    'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }
        } else  {
            return $this->redirect(['view', 'id' => $id]);
        }
    }

    public function actionDeleteFile($id)
    {
        $request = Yii::$app->request;
        $model = SituationAnalysisFiles::findOne($id);
        unlink(Yii::getAlias('@app/web') . $model->file->location);
        $model->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#situtation-files-pjax'];
        }else{
            return $this->redirect(['index']);
        }


    }
     
    

    /**
     * Finds the SituationAnalysis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SituationAnalysis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SituationAnalysis::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
