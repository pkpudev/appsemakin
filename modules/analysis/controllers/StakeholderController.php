<?php

namespace app\modules\analysis\controllers;

use app\components\HistoryHelper;
use Yii;
use app\models\Stakeholder;
use app\models\search\StakeholderSearch;
use app\models\StakeholderAnalysisResult;
use app\models\StakeholderContact;
use app\models\StakeholderHistory;
use app\models\StakeholderInfluence;
use app\models\StakeholderSupport;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * StakeholderController implements the CRUD actions for Stakeholder model.
 */
class StakeholderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Stakeholder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StakeholderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Stakeholder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        
        $dataProviderContact = new ActiveDataProvider([
            'query' => StakeholderContact::find()->where(['stakeholder_id' => $id]),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['name' => SORT_ASC]]
        ]);
        
        $dataProviderHistory = new ActiveDataProvider([
            'query' => StakeholderHistory::find()->where(['stakeholder_id' => $id]),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
        ]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> 'Lihat Pemangku Kepentingan',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                        'dataProviderContact' => $dataProviderContact,
                        'dataProviderHistory' => $dataProviderHistory
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'dataProviderContact' => $dataProviderContact,
                'dataProviderHistory' => $dataProviderHistory
            ]);
        }
    }

    /**
     * Creates a new Stakeholder model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Stakeholder();

        if ($model->load($request->post()) && $model->save()) {
            HistoryHelper::create(new StakeholderHistory(), 'stakeholder_id', $model->id, 'Membuat Pemangku Kepentingan.');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Updates an existing Stakeholder model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        
        if ($model->load($request->post()) && $model->save()) {
            HistoryHelper::create(new StakeholderHistory(), 'stakeholder_id', $model->id, 'Mengubah Pemangku Kepentingan.');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Delete an existing Stakeholder model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $from=null)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($from == 'view'){
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->headers->set('X-Redirect', \yii\helpers\Url::to(['index']));
            return [
                'title' => 'Info',
                'content' => '<span class="text-success">Berhasil menghapus Pemangku Kepentingan!</span>',
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }


    public function actionContact($method, $stake_id, $id=null)
    {
        $request = Yii::$app->request;

        if($method == 'ins'){
            $model = new StakeholderContact();
            $model->stakeholder_id = $stake_id;
            $msg = 'menambah';
        } else if($method == 'upd'){
            $model = StakeholderContact::findOne($id);
            $msg = 'mengubah';
        } else if($method == 'del'){
            $model = StakeholderContact::findOne($id);
            if($model->delete()){
                if($request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['forceClose'=>true,'forceReload'=>'#contact-pjax'];
                } else {
                    return $this->redirect(['view', 'id' => $stake_id]);
                }
            }
        } else if($method == 'view'){
            $model = StakeholderContact::findOne($id);
            if($request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title'=> 'Lihat Kontak',
                    'content'=>$this->renderAjax('_view_contact', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Ubah',['contact', 'method' => 'upd', 'stake_id' => $model->stakeholder_id, 'id' => $id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            } else {
                return $this->redirect(['view', 'id' => $stake_id]);
            }
        } else {
            return $this->redirect(['view', 'id' => $stake_id]);
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> 'Kontak Pemangku Kepentingan',
                    'content'=>$this->renderAjax('_form_contact', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Simpan',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                if($method == 'ins'){
                    HistoryHelper::create(new StakeholderHistory(), 'stakeholder_id', $model->stakeholder_id, 'Menambah Kontak.');
                } else if($method == 'upd') {
                    HistoryHelper::create(new StakeholderHistory(), 'stakeholder_id', $model->stakeholder_id, 'Mengubah salah satu Kontak.');
                }

                return [
                    'forceReload'=>'#contact-pjax',
                    'title'=> 'Kontak Pemangku Kepentingan',
                    'content'=>'<span class="text-success">Berhasil  ' . $msg . ' Kontak Pemangku Kepentingan</span>',
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Tambah Lagi',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }
        } else {
            return $this->redirect(['view', 'id' => $stake_id]);
        }

    }

    public function actionGetResult($influence_id, $support_id){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $influencePoint = StakeholderInfluence::findOne($influence_id)->influence_point;
        $supportPoint = StakeholderSupport::findOne($support_id)->support_point;

        $resultPoint = $influencePoint * $supportPoint;

        $analysisResult = StakeholderAnalysisResult::find()
                            ->where('min <= ' . $resultPoint . ' and max >= ' . $resultPoint)
                            ->one();

        return [
                    'id' => $analysisResult->id, 
                    'text' => $analysisResult->result_grade, 
                    'influencePoint' => $influencePoint,
                    'supportPoint' => $supportPoint,
                    'resultPoint' => $resultPoint
                ];
    }

    /**
     * Finds the Stakeholder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stakeholder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stakeholder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
