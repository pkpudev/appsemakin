<?php

use yii\widgets\DetailView;

?>
<div class="stakeholder-contact-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'name',
                'value' => function ($model) {
                    return ($model->name) ?: '-';
                },
            ],
            [
                'attribute' => 'position',
                'value' => function ($model) {
                    return ($model->position) ?: '-';
                },
            ],
            [
                'attribute' => 'hp_no',
                'value' => function ($model) {
                    return ($model->hp_no) ?: '-';
                },
            ],
            [
                'attribute' => 'address',
                'value' => function ($model) {
                    return ($model->address) ?: '-';
                },
            ],
            [
                'attribute'=>'created_by',
                'value' => function($model){
                    return $model->created_by ? $model->createdBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'created_at',
                'value' => function($model){
                    return $model->created_at ?: '-';
                },
            ],
            [
                'attribute'=>'updated_by',
                'value' => function($model){
                    return $model->updated_by ? $model->updatedBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($model){
                    return $model->updated_at ?: '-';
                },
            ],
        ],
    ]) ?>

</div>
