<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

$this->title = 'Analisis Pelanggan';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="situation-analysis-index">

    <?php $this->beginBlock('customers_analysis'); ?>
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'export' => [
                   'label' => 'Export Excel atau PDF',
                   'header' => '',
                   'options' => [
                       'class' => 'btn btn-primary'
                   ], 
                   'menuOptions' => [ 
                           'style' => 'background: var(--background); width: 187px;'
                   ]
               ],
               'exportConfig' => [
                   GridView::PDF => true,
                   GridView::EXCEL => true,
               ],
                'columns' => require(__DIR__.'/_columns.php'),
                'toolbar'=> [
                    ['content'=>
                        Html::a('<i class="fa fa-plus"></i> Tambah Analisis Pelanggan', ['create'],
                        ['role'=>'modal-remote','title'=> 'Tambah Analisis Pelanggan','class'=>'btn btn-success'])
                    ],
                    ['content'=>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                        '{toggleData}'
                    ],
                    ['content'=>
                        '{export}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="fa fa-list-alt"></i> Daftar Analisis Pelanggan',
                    'before'=>'',
                    'after'=>'',
                ]
            ])?>
        </div>
    <?php $this->endBlock(); ?>


    <?php $this->beginBlock('customers'); ?>
        <?=GridView::widget([
            'id'=>'customer',
            'dataProvider' => $dataProviderCustomer,
            'filterModel' => $searchModelCustomer,
            'pjax'=>true,
            'export' => [
               'label' => 'Export Excel atau PDF',
               'header' => '',
               'options' => [
                   'class' => 'btn btn-primary'
               ], 
               'menuOptions' => [ 
                       'style' => 'background: var(--background); width: 187px;'
               ]
           ],
           'exportConfig' => [
               GridView::PDF => true,
               GridView::EXCEL => true,
           ],
            'columns' => require(__DIR__.'/_columns_customer.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-plus"></i> Tambah Pelanggan', ['create-customer'],
                    ['role'=>'modal-remote','title'=> 'Tambah Pelanggan','class'=>'btn btn-success'])
                ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Daftar Pelanggan',
                'before'=>'',
                'after'=>'',
            ]
        ])?>
    <?php $this->endBlock(); ?>


    <div class="box box-default">
        <div class="box-body">
            <?= Tabs::widget([
                'id' => 'customers-analysis-tabs',
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => "<span class='fa fa-shopping-cart'></span>&nbsp;Analisis Pelanggan",
                        'content' => $this->blocks['customers_analysis'],
                    ],
                    [
                        'label' => "<span class='fa fa-users'></span>&nbsp;Pelanggan",
                        'content' => $this->blocks['customers'],
                    ],
                ]
            ]); ?>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>