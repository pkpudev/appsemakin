<?php

use app\components\AttributeHintHelper;
use app\models\Customers;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SituationAnalysis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-analysis-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'customers_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Customers::find()->where(['is_active' => true])->all(), 'id', 'customer'),
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['prompt' => '-- Pilih Pelanggan --'],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                ]
        ])->label(AttributeHintHelper::getHint($model, 'customers_id')); ?>

        <?= $form->field($model, 'needs')->textarea(['rows' => 3])->label(AttributeHintHelper::getHint($model, 'needs')) ?>

        <?= $form->field($model, 'expectation')->textarea(['rows' => 3])->label(AttributeHintHelper::getHint($model, 'expectation')) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
