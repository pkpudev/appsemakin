<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomersAnalysis */

?>
<div class="customer-analysis-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
