<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'customer')->textInput() ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>

		<?= $form->field($model, 'category')->dropDownList([1 => 'Internal', 2 => 'Eksternal'], ['class' => 'form-control']) ?>
		
		<?= $form->field($model, 'is_active')->dropDownList([1 => 'Aktif', 0 => 'Tidak Aktif'], ['class' => 'form-control']) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
