<?php

use app\models\CustomersAnalysis;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_by',
        'value' => function($model){
            return $model->created_by ? $model->createdBy0->full_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'value' => function($model){
            return $model->created_at ?: '-';
        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'updated_by',
        'value' => function($model){
            return $model->updated_by ? $model->updatedBy0->full_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'updated_at',
        'value' => function($model){
            return $model->updated_at ?: '-';
        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'is_active',
        'value' => function($model){
            return $model->is_active ? 'Aktif' : 'Tidak Aktif';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>[1 => 'Aktif', 0 => 'Tidak Aktif'], 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
        'hAlign' => 'center',
        'contentOptions' => ['style' => 'width:100px;'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'category',
        'value' => function($model){
            return $model->category == 1 ? 'Internal' : 'Eksternal';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=>[1 => 'Internal', 2 => 'Eksternal'], 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
        'hAlign' => 'center',
        'contentOptions' => ['style' => 'width:100px;'],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },   
        'template' => '{edit}&nbsp;&nbsp;{delete}',
        'buttons' => [
            'edit' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                        ['update-customer', 'id' => $model->id], 
                        [
                            'title' => 'Ubah Pelanggan ini',
                            'role' => 'modal-remote',
                            'style' => 'color: var(--warning) !important'
                        ]
                    );
            },
            'delete' => function ($url, $model) {
                $check = CustomersAnalysis::findOne(['customers_id' => $model->id]);

                if(!$check)
                return Html::a('<span class="fa fa-trash"></span>',
                    ['delete-customer', 'id' => $model->id], 
                    [
                        'title' => 'Hapus Pelanggan ini',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Konfirmasi',
                        'data-confirm-message'=>'Yakin ingin menghapus Pelanggan ini?',
                        'style' => 'color: var(--danger) !important'
                    ]
                );
            },
        ],
        'width' => '60px'
    ],

];
