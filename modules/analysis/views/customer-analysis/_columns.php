<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customers_id',
        'value' => function($model){
            return $model->customers_id ? $model->customers->customer : '-';
        },
        'group' => true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'needs',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'expectation',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_by',
        'value' => function($model){
            return $model->created_by ? $model->createdBy0->full_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'value' => function($model){
            return $model->created_at ?: '-';
        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'updated_by',
        'value' => function($model){
            return $model->updated_by ? $model->updatedBy0->full_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'updated_at',
        'value' => function($model){
            return $model->updated_at ?: '-';
        },
        'filter' => false
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'visibleButtons' => [
            'view' => false,
            'update' => $model->created_by == Yii::$app->user->id || Yii::$app->user->can('Admin'),
            'delete' => $model->created_by == Yii::$app->user->id || Yii::$app->user->can('Admin'),
        ],
        'viewOptions'=>['data-pjax'=>0,'title'=>'Lihat Analisis Situasi ini','data-toggle'=>'tooltip', 'style' => 'color: var(--primary) !important'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip', 'style' => 'color: var(--warning) !important'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Konfirmasi',
                          'data-confirm-message'=>'Yakin ingin menghapus Analisis Pelanggan ini?', 
                          'style' => 'color: var(--danger) !important'],
    ],

];
