<?php

use yii\widgets\DetailView;

?>
<div class="portfolio-canvas-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'portfolio_id',
                'value' => function($model){
                    return $model->portfolio->portfolio_name;
                }
            ],
            [
                'attribute'=>'element_id',
                'value' => function($model){
                    return $model->element->element;
                }
            ],
            [
                'attribute'=>'description',
                'value' => function($model){
                    return $model->description ?: '-';
                }
            ],
            [
                'attribute'=>'created_by',
                'value' => function($model){
                    return $model->created_by ? $model->createdBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'created_at',
                'value' => function($model){
                    return $model->created_at ?: '-';
                },
            ],
            [
                'attribute'=>'updated_by',
                'value' => function($model){
                    return $model->updated_by ? $model->updatedBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($model){
                    return $model->updated_at ?: '-';
                },
            ],
        ],
    ]) ?>
</div>