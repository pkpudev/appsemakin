<?php

use app\models\Portfolio;
use app\models\PortfolioCanvas;
use app\models\PortfolioElement;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */

$this->title = 'Lihat Model Bisnis';

CrudAsset::register($this);
?>
<style>
    .portfolio-view {
        overflow-x: auto;
        padding-bottom: 20px;
    }
</style>
<div class="portfolio-view">

    <div class="row" style="display: none;">
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <div class="box box-default">
                <div class="box-header">
                    <label>Detail Model Bisnis</label>
                    <?php if($model->created_by == Yii::$app->user->id || Yii::$app->user->can('Admin')): ?>
                    <div class="btn-group pull-right">
                        <?= Html::a('Ubah Model Bisnis', ['update', 'id' => $model->id, 'from' => 'view'], ['role'=>'modal-remote', 'class' => 'btn btn-warning btn-xs']); ?>
                        <?= Html::a(
                            'Hapus Model Bisnis', 
                            ['delete', 'id' => $model->id, 'from' => 'view'], 
                            [
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Hapus Kontak ini',
                                'role'=>'modal-remote',
                                'data-confirm'=>false, 
                                'data-method'=>false,
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Konfirmasi',
                                'data-confirm-message'=>'Yakin ingin menghapus Model Bisnis ini?'
                            ]
                                    ); ?>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'attribute'=>'portfolio_name',
                            ],
                            [
                                'attribute'=>'version',
                            ],
                            [
                                'attribute'=>'portfolio_date',
                            ],
                            [
                                'attribute'=>'created_by',
                                'value' => function($model){
                                    return $model->created_by ? $model->createdBy0->full_name : '-';
                                }
                            ],
                            [
                                'attribute'=>'created_at',
                                'value' => function($model){
                                    return $model->created_at ?: '-';
                                },
                            ],
                            [
                                'attribute'=>'updated_by',
                                'value' => function($model){
                                    return $model->updated_by ? $model->updatedBy0->full_name : '-';
                                }
                            ],
                            [
                                'attribute'=>'updated_at',
                                'value' => function($model){
                                    return $model->updated_at ?: '-';
                                },
                            ],
                        ],
                    ]) ?>
                </div>
            </div>

        </div>
        <div class="col-md-2"></div>
    </div>

    <div class="box box-default" style="width: 1225px; margin: 0 auto;">
        <div class="box-body">
            <table width="100%">
                <tr>
                    <td>
                        <h3 style="margin-top: 10px;"><?= $model->portfolio_name; ?></h3>
                    </td>
                    <td width="170px" valign="middle" align="right" style="padding-right: 10px;">
                        <div class="btn-group">
                            <?= Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model->id, 'from' => 'view'], ['title' => 'Ubah Model Bisnis ini', 'role'=>'modal-remote', 'class' => 'btn btn-warning btn-xs']); ?>
                            <?= Html::a(
                                        '<span class="fa fa-trash"></span>', 
                                        ['delete', 'id' => $model->id, 'from' => 'view'], 
                                        [
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Hapus Model Bisnis ini',
                                            'role'=>'modal-remote',
                                            'data-confirm'=>false, 
                                            'data-method'=>false,
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Konfirmasi',
                                            'data-confirm-message'=>'Yakin ingin menghapus Model Bisnis ini?'
                                        ]
                                    ); ?>
                        </div>
                    </td>
                    <td width="180px" style="background: var(--background); color: #fff;padding: 5px;border-top-left-radius: 10px;border-bottom-left-radius: 10px;">
                        <b>Tanggal:</b><br />
                        <div class="pull-right">
                            <?= $model->portfolio_date; ?>
                        </div>
                    </td>
                    <td width="180px" style="background: var(--background); color: #fff;padding: 5px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
                        <b>Versi:</b><br />
                        <div class="pull-right">
                            <?= $model->version; ?>
                        </div>
                    </td>
                </tr>
            </table>

            <table width="100%" class="table table-bordered" style="margin-top: 10px;">
                <tr>
                    <td width="172px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::VALUE_STREAMS)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::VALUE_STREAMS)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::VALUE_STREAMS], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::VALUE_STREAMS)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::VALUE_STREAMS)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::VALUE_STREAMS])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                    <td width="172px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::SOLUSTIONS)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::SOLUSTIONS)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::SOLUSTIONS], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::SOLUSTIONS)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::SOLUSTIONS)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::SOLUSTIONS])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                    <td width="172px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::CUSTOMERS)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::CUSTOMERS)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::CUSTOMERS], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::CUSTOMERS)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::CUSTOMERS)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::CUSTOMERS])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                    <td width="172px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::CHANNELS)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::CHANNELS)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::CHANNELS], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::CHANNELS)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::CHANNELS)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::CHANNELS])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                    <td width="172px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::CUSTOMER_RELATIONSHIPS)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::CUSTOMER_RELATIONSHIPS)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::CUSTOMER_RELATIONSHIPS], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::CUSTOMER_RELATIONSHIPS)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::CUSTOMER_RELATIONSHIPS)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::CUSTOMER_RELATIONSHIPS])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                    <td width="172px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::BUDGET)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::BUDGET)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::BUDGET], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::BUDGET)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::BUDGET)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::BUDGET])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                    <td width="172px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::KPI_REVENUE)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::KPI_REVENUE)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::KPI_REVENUE], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::KPI_REVENUE)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::KPI_REVENUE)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::KPI_REVENUE])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                </tr>
            </table>
            <table width="100%" class="table table-bordered" style="margin-top: -1px;">
                <tr>
                    <td width="401px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::KEY_PARTNERS)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::KEY_PARTNERS)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::KEY_PARTNERS], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::KEY_PARTNERS)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::KEY_PARTNERS)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::KEY_PARTNERS])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                    <td width="401px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::KEY_ACTIVITIES)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::KEY_ACTIVITIES)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::KEY_ACTIVITIES], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::KEY_ACTIVITIES)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::KEY_ACTIVITIES)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::KEY_ACTIVITIES])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                    <td width="401px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::KEY_RESOURCES)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::KEY_RESOURCES)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::KEY_RESOURCES], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::KEY_RESOURCES)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::KEY_RESOURCES)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::KEY_RESOURCES])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                </tr>
            </table>
            <table width="100%" class="table table-bordered" style="margin-top: -1px;">
                <tr>
                    <td width="602px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::COST_STRUCTURE)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::COST_STRUCTURE)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::COST_STRUCTURE], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::COST_STRUCTURE)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::COST_STRUCTURE)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::COST_STRUCTURE])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                    <td width="602px">
                        <b style="font-size: 13px;"><?= PortfolioElement::findOne(PortfolioElement::REVENUE_STREAMS)->element; ?></b><br />
                        <p style="font-size: 10px;"><i><?= PortfolioElement::findOne(PortfolioElement::REVENUE_STREAMS)->description ?: '-'; ?></i></p>
                        <?= Html::a('<span class="fa fa-plus"></span>', 
                                    ['create-canvas', 'id' => $model->id, 'element_id' => PortfolioElement::REVENUE_STREAMS], 
                                    [
                                        'title' => 'Tambah ' . PortfolioElement::findOne(PortfolioElement::REVENUE_STREAMS)->element, 
                                        'role'=>'modal-remote', 
                                        'class' => 'btn btn-success btn-xs', 
                                        'style' => 'margin-top: -45px;float: right;'
                                    ]); ?>
                        <?php Pjax::begin(['id' => strtolower(str_replace('/', '_', str_replace(' ', '_', PortfolioElement::findOne(PortfolioElement::REVENUE_STREAMS)->element)))  . '-pjax']) ?>
                            <div style="font-size: 12px;">
                                <?php
                                    $canvas = PortfolioCanvas::find()->where(['element_id' => PortfolioElement::REVENUE_STREAMS])->andWhere(['portfolio_id' => $model->id])->orderBy('id asc')->all();
                                    foreach($canvas as $cnv){
                                        echo '&#8226; ' . Html::a($cnv->description, ['view-canvas', 'id' => $cnv->id], ['role' => 'modal-remote']) . '<br />';
                                    }
                                ?>
                            </div>
                        <?php Pjax::end() ?>
                    </td>
                </tr>
            </table>

            <table width="100%" style="margin-top: 10px;">
                <tr>
                    <td valign="top">
                        <i>*Silahkan klik datanya untuk melihat/mengubah/menghapus.</i>
                    </td>
                    <td width="180px" style="background: var(--background); color: #fff;padding: 5px;border-top-left-radius: 10px;border-bottom-left-radius: 10px;">
                        <b style="width: 100%;display: inline-block;">Dibuat Oleh:</b><br />
                        <p class="pull-right">
                            <?= $model->created_by ? $model->createdBy0->full_name : '-' ?>
                        </p>
                    </td>
                    <td width="180px" style="background: var(--background); color: #fff;padding: 5px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
                        <b style="width: 100%;display: inline-block;">Dibuat Pada:</b><br />
                        <p class="pull-right">
                            <?= $model->created_at ?: '-' ?>
                        </p>
                    </td>
                </tr>
            </table>

            <table width="100%" style="margin-top: 5px;">
                <tr>
                    <td width="180px" style="background: var(--background); color: #fff;padding: 5px;border-top-left-radius: 10px;border-bottom-left-radius: 10px;">
                        <b style="width: 100%;display: inline-block;">Periode:</b><br />
                        <p class="pull-right">
                            <?= $model->period_start ?: '' ?> - <?= $model->period_end ?: '' ?>
                        </p>
                    </td>
                    <td width="180px" style="background: var(--background); color: #fff;padding: 5px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
                        <b style="width: 100%;display: inline-block;">Status:</b><br />
                        <p class="pull-right">
                            <?= $model->is_active ? 'Aktif' : 'Tidak Aktif' ?>
                        </p>
                    </td>
                    <td></td>
                    <td width="180px" style="background: var(--background); color: #fff;padding: 5px;border-top-left-radius: 10px;border-bottom-left-radius: 10px;">
                        <b style="width: 100%;display: inline-block;">Diubah Oleh:</b><br />
                        <p class="pull-right">
                            <?= $model->updated_by ? $model->updatedBy0->full_name : '-' ?>
                        </p>
                    </td>
                    <td width="180px" style="background: var(--background); color: #fff;padding: 5px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
                        <b style="width: 100%;display: inline-block;">Diubah Pada:</b><br />
                        <p class="pull-right">
                            <?= $model->updated_at ?: '-' ?>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
 

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>