<?php

use kartik\grid\GridView;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'portfolio_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'version',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'portfolio_date',
        'filterType'=>GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => ['format' => 'yyyy-mm-dd', 'autoclose' => true, 'todayHighlight' => true,],
        ],
        'contentOptions' => ['style' => 'width:150px;'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_by',
        'value' => function($model){
            return $model->created_by ? $model->createdBy0->full_name : '-';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'value' => function($model){
            return $model->created_at ?: '-';
        },
        'filter' => false
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'is_active',
        'value' => function($model){
            return $model->is_active ? 'Aktif' : 'Tidak Atif';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=> [1 => 'Aktif', 0 => 'Tidak Atif'], 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
        'width' => '120px'
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'visibleButtons' => [
            'update' => false,
            'delete' => false,
        ],
        'viewOptions'=>['data-pjax'=>0,'title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'],
    ],

];
