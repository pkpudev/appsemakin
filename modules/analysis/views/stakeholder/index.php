<?php

use app\models\Stakeholder;
use app\models\StakeholderAnalysisResult;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Tabs;

$this->title = 'Analisis Pemangku Kepentingan';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="stakeholder-analysis-index">

    <?php $this->beginBlock('stakeholder'); ?>
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'export' => [
                    'label' => 'Export Excel atau PDF',
                    'header' => '',
                    'options' => [
                        'class' => 'btn btn-primary'
                    ], 
                    'menuOptions' => [ 
                            'style' => 'background: var(--background); width: 187px;'
                    ]
                ],
                'exportConfig' => [
                    GridView::PDF => true,
                    GridView::EXCEL => true,
                ],
                'columns' => require(__DIR__.'/_columns.php'),
                'toolbar'=> [
                    ['content'=>
                        Html::a('<i class="fa fa-plus"></i> Tambah Analisis Pemangku Kepentingan', ['create'],
                        ['data-pjax'=>0,'title'=> 'Tambah Analisis Pemangku Kepentingan','class'=>'btn btn-success'])
                    ],
                    ['content'=>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                        '{toggleData}'
                    ],
                    ['content'=>
                        '{export}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="fa fa-list-alt"></i> Daftar Analisis Pemangku Kepentingan',
                    'before'=>'',
                    'after'=>'',
                ]
            ])?>
        </div>
    <?php $this->endBlock(); ?>


    <?php $this->beginBlock('matriks'); ?>
        <div style="width: 800px;margin: 10px auto;text-align: center;">
            <h4>Stakeholder Matrix</h4>
            <table style="width: 100%; border: 1px solid #eee;">
                <tr>
                    <td valign="top" width="50%" style="border: 1px solid #999;">
                        <center><b><?= StakeholderAnalysisResult::findOne(StakeholderAnalysisResult::SATISFIED)->result_grade ?></b></center>
                        <div style="min-height: 200px;">
                            <?php
                                $stakeholders = Stakeholder::find()->where(['analysis_result_id' => StakeholderAnalysisResult::SATISFIED])->orderBy('result_point desc')->all();
                                foreach($stakeholders as $stakeholder):
                            ?>
                                <?= Html::a($stakeholder->stakeholder . ' (' . $stakeholder->result_point . ' point)', ['view', 'id' => $stakeholder->id], ['target' => '_blank', 'style' => "border: 1px solid #eee;padding: 2px;margin-top: 5px;display: inline-block;"]); ?><br />
                            <?php endforeach; ?>
                        </div>
                    </td>
                    <td valign="top" style="border: 1px solid #999;">
                        <center><b><?= StakeholderAnalysisResult::findOne(StakeholderAnalysisResult::CLOSELY)->result_grade ?></b></center>
                        <div style="min-height: 200px;">
                            <?php
                                $stakeholders = Stakeholder::find()->where(['analysis_result_id' => StakeholderAnalysisResult::CLOSELY])->orderBy('result_point desc')->all();
                                foreach($stakeholders as $stakeholder):
                            ?>
                                <?= Html::a($stakeholder->stakeholder . ' (' . $stakeholder->result_point . ' point)', ['view', 'id' => $stakeholder->id], ['target' => '_blank', 'style' => "border: 1px solid #eee;padding: 2px;margin-top: 5px;display: inline-block;"]); ?><br />
                            <?php endforeach; ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="border: 1px solid #999;">
                        <center><b><?= StakeholderAnalysisResult::findOne(StakeholderAnalysisResult::MONITOR)->result_grade ?></b></center>
                        <div style="min-height: 200px;">
                            <?php
                                $stakeholders = Stakeholder::find()->where(['analysis_result_id' => StakeholderAnalysisResult::MONITOR])->orderBy('result_point desc')->all();
                                foreach($stakeholders as $stakeholder):
                            ?>
                                <?= Html::a($stakeholder->stakeholder . ' (' . $stakeholder->result_point . ' point)', ['view', 'id' => $stakeholder->id], ['target' => '_blank', 'style' => "border: 1px solid #eee;padding: 2px;margin-top: 5px;display: inline-block;"]); ?><br />
                            <?php endforeach; ?>
                        </div>
                    </td>
                    <td valign="top" style="border: 1px solid #999;">
                        <center><b><?= StakeholderAnalysisResult::findOne(StakeholderAnalysisResult::INFORMED)->result_grade ?></b></center>
                        <div style="min-height: 200px;">
                            <?php
                                $stakeholders = Stakeholder::find()->where(['analysis_result_id' => StakeholderAnalysisResult::INFORMED])->orderBy('result_point desc')->all();
                                foreach($stakeholders as $stakeholder):
                            ?>
                                <?= Html::a($stakeholder->stakeholder . ' (' . $stakeholder->result_point . ' point)', ['view', 'id' => $stakeholder->id], ['target' => '_blank', 'style' => "border: 1px solid #eee;padding: 2px;margin-top: 5px;display: inline-block;"]); ?><br />
                            <?php endforeach; ?>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

    <?php $this->endBlock(); ?>

    <div class="box box-default">
        <div class="box-body">
            <?= Tabs::widget([
                'id' => 'stakeholder-tabs',
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => "<span class='fa fa-users'></span>&nbsp;Analisis Pemangku Kepentingan",
                        'content' => $this->blocks['stakeholder'],
                    ],
                    [
                        'label' => "<span class='fa fa-braille'></span>&nbsp;Matrix",
                        'content' => $this->blocks['matriks'],
                    ],
                ]
            ]); ?>
        </div>
    </div>
    
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>