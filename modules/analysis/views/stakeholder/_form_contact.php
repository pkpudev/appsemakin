<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

?>

<div class="contact-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput() ?>
        
        <?= $form->field($model, 'position')->textInput() ?>
        
        <?= $form->field($model, 'hp_no')->textInput() ?>

        <?= $form->field($model, 'address')->textarea(['rows' => 3]) ?>
        
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
