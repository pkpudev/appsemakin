<?php

use app\models\StakeholderAnalysisResult;
use app\models\StakeholderCategory;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'stakeholder',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'stakeholder_category_id',
        'value' => function($model){
            return $model->stakeholder_category_id ? $model->category->stakeholder_category: '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=> ArrayHelper::map(StakeholderCategory::find()->all(), 'id', 'stakeholder_category'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'role',
        'value' => function($model){
            return $model->role ?: '-';
        },
        'format' => 'raw'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'responsibility',
        'value' => function($model){
            return $model->responsibility ?: '-';
        },
        'format' => 'raw'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'relationship_owner',
        'value' => function($model){
            return $model->relationship_owner ?: '-';
        },
        'format' => 'raw'
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'analysis_result_id',
        'value' => function($model){
            return $model->analysis_result_id ? $model->result->result_grade . ' (' . $model->result_point . ' point)': '-';
        },
        'filterType'=>GridView::FILTER_SELECT2,
        'filter'=> ArrayHelper::map(StakeholderAnalysisResult::find()->all(), 'id', 'result_grade'), 
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>''],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'visibleButtons' => [
            'update' => false,
            'delete' => false,
        ],
        'viewOptions'=>['data-pjax'=>0,'title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'],
    ],

];
