<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Stakeholder */

?>
<div class="stakeholder-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
