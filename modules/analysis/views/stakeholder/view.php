<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\widgets\DetailView;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Stakeholder */

$this->title = 'Lihat Pemangku Kepentingan';
CrudAsset::register($this);

?>
<div class="stakeholder-view">
 
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <div class="box box-default">
                <div class="box-header with-border">
                    <label>Detail Pemangku Kepentingan</label>
                    <div class="btn-group pull-right">
                        <?= Html::a('Ubah Pemangku Kepentingan', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-xs']); ?>
                        <?= Html::a(
                                        'Hapus Pemangku Kepentingan', 
                                        ['delete', 'id' => $model->id, 'from' => 'view'], 
                                        [
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Hapus Kontak ini',
                                            'role'=>'modal-remote',
                                            'data-confirm'=>false, 
                                            'data-method'=>false,
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Konfirmasi',
                                            'data-confirm-message'=>'Yakin ingin menghapus Pemangku Kepentingan ini?'
                                        ]
                                    ); ?>
                    </div>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'template' => '<tr><th width="250px">{label}</th><td>{value}</td></tr>',
                        'attributes' => [
                            'stakeholder',
                            [
                                'attribute' => 'stakeholder_category_id',
                                'value' => function($model){
                                    return $model->stakeholder_category_id ? $model->category->stakeholder_category: '-';
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'role',
                                'value' => function($model){
                                    return $model->role ?: '-';
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'responsibility',
                                'value' => function($model){
                                    return $model->responsibility ?: '-';
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'relationship_owner',
                                'value' => function($model){
                                    return $model->relationship_owner ?: '-';
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'expectation',
                                'value' => function($model){
                                    return $model->expectation ?: '-';
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'issue',
                                'value' => function($model){
                                    return $model->issue ?: '-';
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'engagement',
                                'value' => function($model){
                                    return $model->engagement ?: '-';
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'influence_id',
                                'value' => function($model){
                                    return $model->influence_id ? $model->influence->influence_grade . ' (' . $model->influence_point . ' point)': '-';
                                },
                            ],
                            [
                                'attribute' => 'support_id',
                                'value' => function($model){
                                    return $model->support_id ? $model->support->support_grade . ' (' . $model->support_point . ' point)': '-';
                                },
                            ],
                            [
                                'attribute' => 'analysis_result_id',
                                'value' => function($model){
                                    return $model->analysis_result_id ? $model->result->result_grade . ' (' . $model->result_point . ' point)': '-';
                                },
                            ],
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <label>Kontak Pemangku Kepentingan</label>
                    <?= Html::a('Tambah Kontak', ['contact', 'method' => 'ins', 'stake_id' => $model->id], ['class' => 'btn btn-success btn-xs pull-right', 'role' => 'modal-remote']); ?>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'id' => 'contact',
                        'dataProvider' => $dataProviderContact,
                        'layout' => '{items}{pager}',
                        'pjax' => true,
                        'emptyText' => 'Tidak ada data Kontak.',
                        'tableOptions' => [
                            'class' => 'table table-responsive table-striped table-bordered'
                        ],
                        'columns' =>  [
                            [
                                'class' => 'kartik\grid\SerialColumn'
                            ],
                            [
                                'attribute' => 'name',
                                'value' => function ($model) {
                                    return ($model->name) ?: '-';
                                },
                            ],
                            [
                                'attribute' => 'position',
                                'value' => function ($model) {
                                    return ($model->position) ?: '-';
                                },
                            ],
                            [
                                'attribute' => 'hp_no',
                                'value' => function ($model) {
                                    return ($model->hp_no) ?: '-';
                                },
                            ],
                            [
                                'attribute' => 'address',
                                'value' => function ($model) {
                                    return ($model->address) ?: '-';
                                },
                            ],
                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'dropdown' => false,
                                'vAlign'=>'middle',
                                'urlCreator' => function($action, $model, $key, $index) { 
                                        return Url::to([$action,'id'=>$key]);
                                },   
                                'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',
                                'buttons' => [
                                    'view' => function ($url, $model) {
                                            return Html::a('<span class="fa fa-eye"></span>',
                                                ['contact', 'method' => 'view', 'stake_id' => $model->stakeholder_id,  'id' => $model->id], 
                                                [
                                                    'title' => 'Lihat Pelanggan ini',
                                                    'role' => 'modal-remote',
                                                    'style' => 'color: var(--primary) !important'
                                                ]
                                            );
                                    },
                                    'update' => function ($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                                ['contact', 'method' => 'upd', 'stake_id' => $model->stakeholder_id, 'id' => $model->id], 
                                                [
                                                    'title' => 'Ubah Pelanggan ini',
                                                    'role' => 'modal-remote',
                                                    'style' => 'color: var(--warning) !important'
                                                ]
                                            );
                                    },
                                    'delete' => function ($url, $model) {
                                        return Html::a('<span class="fa fa-trash"></span>',
                                            ['contact', 'method' => 'del', 'stake_id' => $model->stakeholder_id, 'id' => $model->id], 
                                            [
                                                'title' => 'Hapus Kontak ini',
                                                'role'=>'modal-remote',
                                                'data-confirm'=>false, 
                                                'data-method'=>false,
                                                'data-request-method'=>'post',
                                                'data-confirm-title'=>'Konfirmasi',
                                                'data-confirm-message'=>'Yakin ingin menghapus Kontak ini?',
                                                'style' => 'color: var(--danger) !important'
                                            ]
                                        );
                                    },
                                ],
                                'width' => '90px'
                            ],
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-header with-border">
                    <label>Histori Pemangku Kepentingan</label>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'id' => 'history-table',
                        'dataProvider' => $dataProviderHistory,
                        'layout' => '{items}{pager}',
                        'pjax' => true,
                        'emptyText' => 'Tidak ada data Histori.',
                        'columns' =>  [
                            [
                                'class' => 'kartik\grid\SerialColumn'
                            ],
                            [
                                'attribute' => 'created_stamp',
                                'value' => function($model){
                                    return date('Y-m-d H:i:s', strtotime($model->created_stamp));
                                }
                            ],
                            'action',
                            [
                                'attribute' => 'created_by',
                                'value' => function ($model) {
                                    return ($model->created_by) ? $model->createdBy0->full_name : '-';
                                },
                            ],
                            [
                                'attribute' => 'comment',
                                'value' => function ($model) {
                                    return ($model->comment) ? $model->comment : '-';
                                },
                            ]
                        ],
                    ]) ?>
                </div>
            </div>

        </div>
        <div class="col-md-2"></div>
    </div>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
