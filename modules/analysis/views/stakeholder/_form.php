<?php

use app\components\AttributeHintHelper;
use app\models\StakeholderCategory;
use app\models\StakeholderInfluence;
use app\models\StakeholderSupport;
use dosamigos\ckeditor\CKEditor;
use kartik\editors\Summernote;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Stakeholder */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Tambah Analisis Pemangku Kepentingan'
?>

<div class="stakeholder-form">

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <div class="box box-default">
                <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'stakeholder')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'stakeholder_category_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(StakeholderCategory::find()->all(), 'id', 'stakeholder_category'),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['prompt' => '-- Pilih Kategori --'],
                            ]); ?>
                        </div>
                    </div>

                    <hr style="margin: 0 0 5px 0;" />

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'role')->widget(Summernote::class, [
                                'useKrajeePresets' => true,
                                'pluginOptions' => [
                                    'height' => 100,
                                    'dialogsFade' => true,
                                    'toolbar' => [
                                        ['style1', ['style']],
                                        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
                                        ['para', ['ul', 'ol', 'paragraph', 'height']],
                                        ['insert', ['table', 'hr']],
                                    ],
                                ]
                            ]); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'responsibility')->widget(Summernote::class, [
                                'useKrajeePresets' => true,
                                'pluginOptions' => [
                                    'height' => 100,
                                    'dialogsFade' => true,
                                    'toolbar' => [
                                        ['style1', ['style']],
                                        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
                                        ['para', ['ul', 'ol', 'paragraph', 'height']],
                                        ['insert', ['table', 'hr']],
                                    ],
                                ]
                            ]); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'relationship_owner')->widget(Summernote::class, [
                                'useKrajeePresets' => true,
                                'pluginOptions' => [
                                    'height' => 100,
                                    'dialogsFade' => true,
                                    'toolbar' => [
                                        ['style1', ['style']],
                                        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
                                        ['para', ['ul', 'ol', 'paragraph', 'height']],
                                        ['insert', ['table', 'hr']],
                                    ],
                                ]
                            ]); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'expectation')->widget(Summernote::class, [
                                'useKrajeePresets' => true,
                                'pluginOptions' => [
                                    'height' => 100,
                                    'dialogsFade' => true,
                                    'toolbar' => [
                                        ['style1', ['style']],
                                        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
                                        ['para', ['ul', 'ol', 'paragraph', 'height']],
                                        ['insert', ['table', 'hr']],
                                    ],
                                ]
                            ]); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'issue')->widget(Summernote::class, [
                                'useKrajeePresets' => true,
                                'pluginOptions' => [
                                    'height' => 100,
                                    'dialogsFade' => true,
                                    'toolbar' => [
                                        ['style1', ['style']],
                                        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
                                        ['para', ['ul', 'ol', 'paragraph', 'height']],
                                        ['insert', ['table', 'hr']],
                                    ],
                                ]
                            ]); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'engagement')->widget(Summernote::class, [
                                'useKrajeePresets' => true,
                                'pluginOptions' => [
                                    'height' => 100,
                                    'dialogsFade' => true,
                                    'toolbar' => [
                                        ['style1', ['style']],
                                        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
                                        ['para', ['ul', 'ol', 'paragraph', 'height']],
                                        ['insert', ['table', 'hr']],
                                    ],
                                ]
                            ]); ?>
                        </div>
                    </div>

                    <hr style="margin: 0 0 5px 0;" />

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'influence_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(StakeholderInfluence::find()->all(), 'id', 'influence_grade'),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['prompt' => '-- Pilih Pengaruh --'],
                            ])->label(AttributeHintHelper::getHint($model, 'influence_id')); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'support_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(StakeholderSupport::find()->all(), 'id', 'support_grade'),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['prompt' => '-- Pilih Dukungan --'],
                            ])->label(AttributeHintHelper::getHint($model, 'support_id')); ?>
                        </div>
                    </div>

                    <?= $form->field($model, 'analysis_result_id')->hiddenInput()->label(false); ?>
                    <?= $form->field($model, 'influence_point')->hiddenInput()->label(false); ?>
                    <?= $form->field($model, 'support_point')->hiddenInput()->label(false); ?>
                    <?= $form->field($model, 'result_point')->hiddenInput()->label(false); ?>

                    <div>
                        <label>Hasil Analisis</label>
                        <div id="resultText" class="form-control" style="margin-bottom: 10px;background: #eee;">
                            <?= $model->isNewRecord ? '-' : $model->result->result_grade; ?>
                        </div>
                    </div>

                    <?php if (!Yii::$app->request->isAjax){ ?>
                        <div class="form-group">
                            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                        </div>
                    <?php } ?>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        </div>
        <div class="col-md-2"></div>
    </div>
    
</div>

<?php
$script = <<<JS

    jQuery('#stakeholder-influence_id').on('change', function(e){
        influenceId = this.value;
        supportId = $('#stakeholder-support_id').val()

        if(supportId > 0){
            $.ajax(
            {
                url: '/analysis/stakeholder/get-result?influence_id=' + influenceId + '&support_id=' + supportId,
                dataType: "json",
                async: false,
                success: function(d)
                {
                    console.log(d);
                    $("#resultText").html(d.text);
                    $('#stakeholder-analysis_result_id').val(d.id);
                    $('#stakeholder-influence_point').val(d.influencePoint);
                    $('#stakeholder-support_point').val(d.supportPoint);
                    $('#stakeholder-result_point').val(d.resultPoint);
                }
            });
        }
    })

    jQuery('#stakeholder-support_id').on('change', function(e){
        supportId = this.value;
        influenceId = $('#stakeholder-influence_id').val()

        if(influenceId > 0){
            $.ajax(
            {
                url: '/analysis/stakeholder/get-result?influence_id=' + influenceId + '&support_id=' + supportId,
                dataType: "json",
                async: false,
                success: function(d)
                {
                    console.log(d);
                    $("#resultText").html(d.text);
                    $('#stakeholder-analysis_result_id').val(d.id);
                    $('#stakeholder-influence_point').val(d.influencePoint);
                    $('#stakeholder-support_point').val(d.supportPoint);
                    $('#stakeholder-result_point').val(d.resultPoint);
                }
            });
        }
    })

JS;
$this->registerJs($script);