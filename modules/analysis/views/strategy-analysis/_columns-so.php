<?php

use app\models\Opportunities;
use app\models\Strengths;
use app\models\Threats;
use app\models\Weaknesses;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    /* [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Analisis Situasi',
        'value' => function($model){
            return $model->situation->title;
        },
        'group' => true
    ], */
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'internal_factor_id',
        'label' => 'Strength',
        'value' => function($model){
            $type = $model->strategy_type;
            if($type[0] == 'S'){
                return Strengths::findOne($model->internal_factor_id)->strength;
            } else if($type[0] == 'W'){
                return Weaknesses::findOne($model->internal_factor_id)->weakness;
            } 
        },
        'group' => true,
        'width' => '33%'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'external_factor_id',
        'label' => 'Opportunity',
        'value' => function($model){
            $type = $model->strategy_type;
            if($type[1] == 'O'){
                return Opportunities::findOne($model->external_factor_id)->opportunity;
            } else if($type[1] == 'T'){
                return Threats::findOne($model->external_factor_id)->threat;
            }
        },
        'group' => true,
        'width' => '33%'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'strategy',
        'label' => 'Strategi',
        'value' => function($model){
            return $model->strategy->strategy;
        },
        'group' => true
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'visibleButtons' => [
            'update' => $model->created_by == Yii::$app->user->id || Yii::$app->user->can('Admin'),
            'delete' => $model->created_by == Yii::$app->user->id || Yii::$app->user->can('Admin'),
        ],
        'viewOptions'=>['role'=>'modal-remote','title'=>'Lihat Analisis Strategi ini','data-toggle'=>'tooltip', 'style' => 'color: var(--primary) !important'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Ubah Analisis Strategi ini', 'data-toggle'=>'tooltip', 'style' => 'color: var(--warning) !important'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Hapus Analisis Strategi ini',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Konfirmasi',
                          'data-confirm-message'=>'Anda yakin ingin menghapus Analisis Strategi ini?', 
                          'style' => 'color: var(--danger) !important'],
    ],

];
