<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StrategyAnalysis */

?>
<div class="strategy-analysis-create">
    <?= $this->render('_form', [
        'model' => $model,
        'type' => $type,
        'situationAnalysisId' => $situationAnalysisId,
        'internalFactor' => $internalFactor,
        'externalFactor' => $externalFactor,
        'internalFactorLabel' => $internalFactorLabel,
        'externalFactorLabel' => $externalFactorLabel,
    ]) ?>
</div>
