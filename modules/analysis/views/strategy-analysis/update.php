<?php

use app\models\Opportunities;
use app\models\Strategies;
use app\models\StrategyAnalysis;
use app\models\Strengths;
use app\models\Threats;
use app\models\Weaknesses;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StrategyAnalysis */
/* @var $form yii\widgets\ActiveForm */


if($internalFactor == 'S'){
    $arrayInternalFactor = Strengths::find()->where(['situation_analysis_id' => $situationAnalysisId])->all();
    $columnInternal = 'strength';
} else if($internalFactor == 'W'){
    $arrayInternalFactor = Weaknesses::find()->where(['situation_analysis_id' => $situationAnalysisId])->all();
    $columnInternal = 'weakness';
}

if($externalFactor == 'O'){
    $arrayExternalFactor = Opportunities::find()->where(['situation_analysis_id' => $situationAnalysisId])->all();
    $columnExternal = 'opportunity';
} else if($externalFactor == 'T'){
    $arrayExternalFactor = Threats::find()->where(['situation_analysis_id' => $situationAnalysisId])->all();
    $columnExternal = 'threat';
}

?>

<div class="strategy-analysis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'internal_factor_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map($arrayInternalFactor, 'id', $columnInternal),
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['prompt' => '-- Pilih ' . $internalFactorLabel . ' --'],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")')
                ]
        ])->label($internalFactorLabel); ?>

        <?= $form->field($model, 'external_factor_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map($arrayExternalFactor, 'id', $columnExternal),
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['prompt' => '-- Pilih ' . $externalFactorLabel . ' --'],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")')
                ]
        ])->label($externalFactorLabel); ?>
    
    <?= $form->field($model, 'strategy_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Strategies::find()->where(['situation_analysis_id' => $model->strategy->situation_analysis_id])->all(), 'id', 'strategy'),
        'initValueText' => $strategyId ? Strategies::findOne($strategyId)->strategy : '',
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
                        'prompt' => '-- Pilih Strategi --',
                        'value' => $strategyId,
                    ],
        'pluginOptions' => [
            'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ]
    ]); ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>