<?php

use app\components\AttributeHintHelper;
use app\models\Opportunities;
use app\models\Strategies;
use app\models\StrategyAnalysis;
use app\models\Strengths;
use app\models\Threats;
use app\models\Weaknesses;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StrategyAnalysis */
/* @var $form yii\widgets\ActiveForm */

if($internalFactor == 'S'){
    $arrayInternalFactor = Strengths::find()->where(['situation_analysis_id' => $situationAnalysisId])->all();
    $columnInternal = 'strength';
} else if($internalFactor == 'W'){
    $arrayInternalFactor = Weaknesses::find()->where(['situation_analysis_id' => $situationAnalysisId])->all();
    $columnInternal = 'weakness';
}

if($externalFactor == 'O'){
    $arrayExternalFactor = Opportunities::find()->where(['situation_analysis_id' => $situationAnalysisId])->all();
    $columnExternal = 'opportunity';
} else if($externalFactor == 'T'){
    $arrayExternalFactor = Threats::find()->where(['situation_analysis_id' => $situationAnalysisId])->all();
    $columnExternal = 'threat';
}

$strategies = ArrayHelper::map(Strategies::find()->where(['situation_analysis_id' => $situationAnalysisId])->all(), 'id', 'strategy');
$strategies = ArrayHelper::merge(['-' => '+ Input  Strategi Baru'], $strategies);
?>

<div class="strategy-analysis-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'internal_factor_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map($arrayInternalFactor, 'id', $columnInternal),
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['prompt' => '-- Pilih ' . $internalFactorLabel . ' --'],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                'multiple' => true,
                ]
        ])->label($internalFactorLabel); ?>

        <?= $form->field($model, 'external_factor_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map($arrayExternalFactor, 'id', $columnExternal),
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['prompt' => '-- Pilih ' . $externalFactorLabel . ' --'],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                'multiple' => true,
                ]
        ])->label($externalFactorLabel); ?>

        <?= $form->field($model, 'strategy_id')->widget(Select2::classname(), [
            'data' => $strategies,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                            'prompt' => '-- Pilih Strategi --',
                            'value' => $strategyId,
                        ],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                ]
        ]); ?>

        <div id="strategyText" style="display: none;">
            <?= $form->field($model, 'strategyText')->textarea(['rows' => 3])->label('Input Strategi Baru') ?>
        </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php
$script = <<< JS

    jQuery('#strategyanalysis-strategy_id').on('change', function(e){
        // alert(this.value)
        if(this.value == '-'){
            $('#strategyText').show()
        } else {
            $('#strategyText').hide()
        }
    })

JS;

$this->registerJs($script);
?>