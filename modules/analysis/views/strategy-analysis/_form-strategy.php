<?php

use app\models\Strategies;
use app\models\StrategyAnalysis;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StrategyAnalysis */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="strategy-analysis-form">

    <?php $form = ActiveForm::begin(); ?>
    
        <?= $form->field($model, 'situation_analysis_id')->widget(Select2::classname(), [
            'options' => [
                'placeholder' => '-- Pilih Analisis Situasi --',
            ],
            'pluginOptions' => [
                'allowClear' => false,
                'multiple' => false,
                'ajax' => [
                    'delay' => 1000,
                    'url' => new JsExpression("function(data) { 
                        return '/analysis/strategy-analysis/get-situation-analysis'
                    }"),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term} }'),
                    'processResults' => new JsExpression('function(data) {
                            var results = [];
                            $.each(data.items, function (index, item) {
                                results.push({
                                    id: item.id,
                                    text: item.text
                                })
                            })
                            return { results: results }
                        }'),
                ],
                'minimumInputLength' => 3,
                'templateResult' => new JsExpression("function(d) { 
                        if (!d.id) { return d.text }
                        var \$situation = d.text
                        return \$situation
                    }"),
                'templateSelection' => new JsExpression("function(d) {
                        if (!d.id) { return d.text }
                        var \$situation = d.text
                        return \$situation
                    }"),
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
            ],
        ]); ?>

        <?= $form->field($model, 'strategy')->textarea(['rows' => 3]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
