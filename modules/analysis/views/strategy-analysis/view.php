<?php

use app\models\Opportunities;
use app\models\Strengths;
use app\models\Threats;
use app\models\Weaknesses;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StrategyAnalysis */
?>
<div class="strategy-analysis-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'internal_factor_id',
                'value' => function($model){
                    $type = $model->strategy_type;
                    if($type[0] == 'S'){
                        return Strengths::findOne($model->internal_factor_id)->strength;
                    } else if($type[0] == 'W'){
                        return Weaknesses::findOne($model->internal_factor_id)->weakness;
                    } 
                },
                'group' => true
            ],
            [
                'attribute'=>'external_factor_id',
                'value' => function($model){
                    $type = $model->strategy_type;
                    if($type[1] == 'O'){
                        return Opportunities::findOne($model->external_factor_id)->opportunity;
                    } else if($type[0] == 'T'){
                        return Threats::findOne($model->external_factor_id)->threat;
                    } 
                },
                'group' => true
            ],
            [
                'attribute'=>'strategy_id',
                'value' => function($model){
                    return $model->strategy->strategy;
                },
                'group' => true
            ],
            [
                'attribute'=>'created_by',
                'value' => function($model){
                    return $model->created_by ? $model->createdBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'created_at',
                'value' => function($model){
                    return $model->created_at ?: '-';
                }
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'updated_by',
                'value' => function($model){
                    return $model->updated_by ? $model->updatedBy0->full_name : '-';
                }
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($model){
                    return $model->updated_at ?: '-';
                }
            ],
        ],
    ]) ?>

</div>
