<?php

use app\models\SituationAnalysis;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;

$this->title = 'Analisis Strategi';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="situation-analysis-index">
    
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default" style="width: 406px;">
                <div class="box-body">
                    <label>Pilih Analisis Situasi untuk menampilkan data Analisis Strategi</label>
                    <?= Select2::widget([
                        'model' => $searchModelSo,
                        'attribute' => 'situation_analysis_id',
                        'pjaxContainerId' => 'crud-datatable-pjax',
                        'options' => [
                            'id' => 'situation_analysis_id-filter',
                            'class' => 'form-control additional-filter',
                            'prompt' => '-- Pilih Analisis Situasi --',
                            'onchange' => 'refreshPage($(this).val())',
                            'value' => $situationAnalysisId
                        ],
                        'pluginOptions' => ['allowClear' => false, 'prompt' => '--'],
                        'data' => ArrayHelper::map(SituationAnalysis::find()->all(), 'id', 'title'),
                    ]); ?>
                </div>
            </div>
        </div>
    </div>


    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'so',
            'dataProvider' => $dataProviderSo,
            // 'filterModel' => $searchModelSo,
            // 'filterSelector' => '.additional-filter',
            'pjax'=>true,
             'export' => [
                'label' => 'Export Excel atau PDF',
                'header' => '',
                'options' => [
                    'class' => 'btn btn-primary'
                ], 
                'menuOptions' => [ 
                        'style' => 'background: var(--background); width: 187px;'
                ]
            ],
            'exportConfig' => [
                GridView::PDF => true,
                GridView::EXCEL => true,
            ],
            'columns' => require(__DIR__.'/_columns-so.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-plus"></i> Tambah Analisis Strategi (SO)', ['create', 'type' => 'SO', 'situationAnalysisId' => $situationAnalysisId],
                    ['role'=>'modal-remote','title'=> 'Tambah Analisis Strategi (SO)','class'=>'btn btn-success'])
                ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Strength - Opportunity (SO)',
                'before'=>'',
                'after'=>'',
            ]
        ])?>

        <?=GridView::widget([
            'id'=>'wo',
            'dataProvider' => $dataProviderWo,
            // 'filterModel' => $searchModelWo,
            // 'filterSelector' => '.additional-filter',
            'pjax'=>true,
             'export' => [
                'label' => 'Export Excel atau PDF',
                'header' => '',
                'options' => [
                    'class' => 'btn btn-primary'
                ], 
                'menuOptions' => [ 
                        'style' => 'background: var(--background); width: 187px;'
                ]
            ],
            'exportConfig' => [
                GridView::PDF => true,
                GridView::EXCEL => true,
            ],
            'columns' => require(__DIR__.'/_columns-wo.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-plus"></i> Tambah Analisis Strategi (WO)', ['create', 'type' => 'WO', 'situationAnalysisId' => $situationAnalysisId],
                    ['role'=>'modal-remote','title'=> 'Tambah Analisis Strategi (WO)','class'=>'btn btn-success'])
                ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Weakness - Opportunity (WO)',
                'before'=>'',
                'after'=>'',
            ]
        ])?>

        <?=GridView::widget([
            'id'=>'st',
            'dataProvider' => $dataProviderSt,
            // 'filterModel' => $searchModelSt,
            // 'filterSelector' => '.additional-filter',
            'pjax'=>true,
             'export' => [
                'label' => 'Export Excel atau PDF',
                'header' => '',
                'options' => [
                    'class' => 'btn btn-primary'
                ], 
                'menuOptions' => [ 
                        'style' => 'background: var(--background); width: 187px;'
                ]
            ],
            'exportConfig' => [
                GridView::PDF => true,
                GridView::EXCEL => true,
            ],
            'columns' => require(__DIR__.'/_columns-st.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-plus"></i> Tambah Analisis Strategi (ST)', ['create', 'type' => 'ST', 'situationAnalysisId' => $situationAnalysisId],
                    ['role'=>'modal-remote','title'=> 'Tambah Analisis Strategi (ST)','class'=>'btn btn-success'])
                ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Strength - Threat (ST)',
                'before'=>'',
                'after'=>'',
            ]
        ])?>

        <?=GridView::widget([
            'id'=>'wt',
            'dataProvider' => $dataProviderWt,
            // 'filterModel' => $searchModelWt,
            // 'filterSelector' => '.additional-filter',
            'pjax'=>true,
             'export' => [
                'label' => 'Export Excel atau PDF',
                'header' => '',
                'options' => [
                    'class' => 'btn btn-primary'
                ], 
                'menuOptions' => [ 
                        'style' => 'background: var(--background); width: 187px;'
                ]
            ],
            'exportConfig' => [
                GridView::PDF => true,
                GridView::EXCEL => true,
            ],
            'columns' => require(__DIR__.'/_columns-wt.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-plus"></i> Tambah Analisis Strategi (WT)', ['create', 'type' => 'WT', 'situationAnalysisId' => $situationAnalysisId],
                    ['role'=>'modal-remote','title'=> 'Tambah Analisis Strategi (WT)','class'=>'btn btn-success'])
                ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Weakness - Threat (WT)',
                'before'=>'',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<script>
    function refreshPage(val) {
        window.location.replace("/analysis/strategy-analysis/index?situationAnalysisId=" + val);
    }
</script>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>