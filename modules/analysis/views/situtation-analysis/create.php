<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SituationAnalysis */

?>
<div class="situation-analysis-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
