<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SituationAnalysis */
?>
<div class="situation-analysis-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
