<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use app\models\Strengths;
use app\models\Weaknesses;
use johnitvn\ajaxcrud\CrudAsset;


$this->title = 'Setting Variabel Internal';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="situation-analysis-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'variable',
                ],[
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'created_by',
                    'value' => function($model){
                        return $model->created_by ? $model->createdBy0->full_name : '-';
                    }
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'created_at',
                    'value' => function($model){
                        return $model->created_at ?: '-';
                    },
                    'filter' => false
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'updated_by',
                    'value' => function($model){
                        return $model->updated_by ? $model->updatedBy0->full_name : '-';
                    }
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'updated_at',
                    'value' => function($model){
                        return $model->updated_at ?: '-';
                    },
                    'filter' => false
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'is_active',
                    'value' => function($model){
                        return $model->is_active ? 'Aktif' : 'Tidak Aktif';
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>[1 => 'Aktif', 0 => 'Tidak Aktif'], 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>''],
                    'hAlign' => 'center',
                    'contentOptions' => ['style' => 'width:100px;'],
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'urlCreator' => function($action, $model, $key, $index) { 
                            return Url::to([$action,'id'=>$key]);
                    },   
                    'template' => '{edit}&nbsp;&nbsp;{delete}',
                    'buttons' => [
                        'edit' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    ['update-internal-variable', 'id' => $model->id], 
                                    [
                                        'title' => 'Ubah Variabel Internal ini',
                                        'role' => 'modal-remote',
                                        'style' => 'color: var(--warning) !important'
                                    ]
                                );
                        },
                        'delete' => function ($url, $model) {
                            $cek1 = Strengths::findOne(['internal_variable_id' => $model->id]);
                            $cek2 = Weaknesses::findOne(['internal_variable_id' => $model->id]);

                            if(!$cek1 && !$cek2){
                                return Html::a('<span class="fa fa-trash"></span>',
                                    ['delete-internal-variable', 'id' => $model->id], 
                                    [
                                        'title' => 'Hapus Variabel Internal ini',
                                        'role'=>'modal-remote',
                                        'data-confirm'=>false, 
                                        'data-method'=>false,
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Konfirmasi',
                                        'data-confirm-message'=>'Yakin ingin menghapus Variabel Internal ini?',
                                        'style' => 'color: var(--danger) !important'
                                    ]
                                );
                            }
                        },
                    ]
                ],
            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="fa fa-plus"></i> Tambah Setting Variabel Internal', ['add-internal-variable'],
                    ['role'=>'modal-remote','title'=> 'Tambah Setting Variabel Internal','class'=>'btn btn-success'])
                ],
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="fa fa-list-alt"></i> Daftar Variabel Internal',
                'before'=>'',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>