<?php

use app\models\AllowanceCategory;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;

?>
<div class="add-file">

    <?php $form = ActiveForm::begin(); ?>

    <i style="color: var(--danger) !important"><b>Pastikan nama file tidak ada karater pagar (#), petik (', "), atau karakter unik lainya.</b></i>

    <?= $form->field($model, 'files[]')->widget(FileInput::classname(), [
        'options' => ['multiple' => true],    
        'pluginOptions' => [
            'showPreview' => true,
            'showCaption' => true,
            'showRemove' => false,
            'showUpload' => false
        ]
    ]); ?>

    <?php ActiveForm::end(); ?>

</div>

