<?php

use app\models\ExternalVariable;
use app\models\InternalVariable;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

if($field2 == 'internal_variable_id'){
	$listVariable = ArrayHelper::map(InternalVariable::find()->where(['is_active' => true])->all(), 'id', 'variable');
} else {
	$listVariable = ArrayHelper::map(ExternalVariable::find()->where(['is_active' => true])->all(), 'id', 'variable');
}

?>

<div class="internal-variable-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, $field2)->widget(Select2::classname(), [
            'data' => $listVariable,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['prompt' => '-- Pilih Variabel --'],
            'pluginOptions' => [
                'dropdownParent' => new yii\web\JsExpression('$("#ajaxCrudModal")'),
                ]
	]); ?>

    <?= $form->field($model, $field1)->textarea(['rows' => 3]) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
