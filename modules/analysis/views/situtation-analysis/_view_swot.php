<?php

use yii\widgets\DetailView;

?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute'=>$field2,
            'value' => function($model){
                return $model->variable->variable;
            }
        ],
        [
            'attribute'=>$field1,
        ],
        [
            'attribute'=>'created_by',
            'value' => function($model){
                return $model->created_by ? $model->createdBy0->full_name : '-';
            }
        ],
        [
            'attribute'=>'created_at',
            'value' => function($model){
                return $model->created_at ?: '-';
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'updated_by',
            'value' => function($model){
                return $model->updated_by ? $model->updatedBy0->full_name : '-';
            }
        ],
        [
            'attribute'=>'updated_at',
            'value' => function($model){
                return $model->updated_at ?: '-';
            }
        ],
    ],
]) ?>