<?php

use app\models\ExternalVariable;
use app\models\InternalVariable;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\SituationAnalysis */

$this->title = "Lihat Analisis Situasi";

$internalVariable = InternalVariable::find()->all();
$externalVariable = ExternalVariable::find()->all();

CrudAsset::register($this);
?>
<div class="situation-analysis-view">
 
    <div class="row">
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-8">
                    <div class="box box-default">
                        <div class="box-header">
                            <label>Detail Analisis Situasi</label>
                        </div>
                        <div class="box-body">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    [
                                        'attribute'=>'title',
                                    ],
                                    [
                                        'attribute'=>'description',
                                    ],
                                    [
                                        'attribute'=>'created_by',
                                        'value' => function($model){
                                            return $model->created_by ? $model->createdBy0->full_name : '-';
                                        }
                                    ],
                                    [
                                        'attribute'=>'created_at',
                                        'value' => function($model){
                                            return $model->created_at ?: '-';
                                        }
                                    ],
                                    [
                                        'class'=>'\kartik\grid\DataColumn',
                                        'attribute'=>'updated_by',
                                        'value' => function($model){
                                            return $model->updated_by ? $model->updatedBy0->full_name : '-';
                                        }
                                    ],
                                    [
                                        'attribute'=>'updated_at',
                                        'value' => function($model){
                                            return $model->updated_at ?: '-';
                                        }
                                    ],
                                    [
                                        'attribute'=>'ref_link',
                                        'format' => 'raw',
                                        'value' => function($model){
                                            return $model->ref_link ?: '-';
                                        }
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-default" style="min-height: 325px;">
                        <div class="box-header">
                            <label>Lampiran</label>
                            <?= Html::a('Tambah Lampiran', ['add-files', 'id' => $model->id], ['class' => 'btn btn-warning btn-xs pull-right', 'role' => 'modal-remote']); ?>
                        </div>
                        <div class="box-body">
                        <?= GridView::widget([
                                'id' => 'situtation-files',
                                'dataProvider' => $dataProviderFile,
                                'layout' => '{items}{pager}',
                                'pjax' => true,
                                'emptyText' => 'Tidak ada data File.',
                                'tableOptions' => [
                                    'class' => 'table table-responsive table-striped table-bordered'
                                ],
                                'columns' =>  [
                                    [
                                        'class' => 'kartik\grid\SerialColumn'
                                    ],
                                    [
                                        'attribute' => 'files_id',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            return ($model->files_id) ? Html::a($model->file->name, [$model->file->location], ['target' => '_blank', 'data-pjax' => 0]) : '-';
                                        },
                                        'width' => '200px',
                                        'group' => true
                                    ],
                                    [
                                        'class' => 'kartik\grid\ActionColumn',
                                        'dropdown' => false,
                                        'vAlign'=>'middle',
                                        'urlCreator' => function($action, $model, $key, $index) { 
                                                return Url::to([$action,'id'=>$key]);
                                        },   
                                        'template' => '{delete}',
                                        'buttons' => [
                                            'delete' => function ($url, $model) {
                                                return Html::a('<span class="fa fa-trash"></span>',
                                                    ['delete-file', 'id' => $model->id], 
                                                    [
                                                        'title' => 'Hapus File ini',
                                                        'role'=>'modal-remote',
                                                        'data-confirm'=>false, 
                                                        'data-method'=>false,
                                                        'data-request-method'=>'post',
                                                        'data-confirm-title'=>'Konfirmasi',
                                                        'data-confirm-message'=>'Yakin ingin menghapus File ini?',
                                                        'style' => 'color: var(--danger) !important'
                                                    ]
                                                );
                                            },
                                        ],
                                        'width' => '90px'
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="box box-default">
                        <div class="box-body text-center">
                            <?php if($model->created_by == Yii::$app->user->id || Yii::$app->user->can('Admin')): ?>
                            <div class="btn-group">
                                <?= Html::a('Ubah Analisis Situasi', ['update', 'id' => $model->id], ['class' => 'btn btn-warning', 'role' => 'modal-remote']); ?>
                                <?= Html::a(
                                        'Hapus Analisis Situasi', 
                                        ['delete', 'id' => $model->id, 'from' => 'view'], 
                                        [
                                            'class' => 'btn btn-danger',
                                            'title' => 'Hapus Kontak ini',
                                            'role'=>'modal-remote',
                                            'data-confirm'=>false, 
                                            'data-method'=>false,
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Konfirmasi',
                                            'data-confirm-message'=>'Yakin ingin menghapus Analisis Situasi ini?'
                                        ]
                                    ); ?>
                            </div>
                            <?php endif; ?>

                            <?= Html::a('Analisis Strategi', ['/analysis/strategy-analysis/index', 'situationAnalysisId' => $model->id], ['class' => 'btn btn-primary', 'target' => '_blank']); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>

            <div class="row equal firstequal">
                <div class="col-md-6">
                    <div class="box box-default" style="height: 100%;">
                        <div class="box-body">
                            <label>Strength</label>
                            <?= Html::a('Tambah Strength', ['add-swot', 'id' => $model->id, 'column' => 'strength'], ['class' => 'btn btn-success btn-xs pull-right', 'role' => 'modal-remote']); ?>
                            <?= GridView::widget([
                                'id' => 'strength-table',
                                'dataProvider' => $dataProviderStrength,
                                'filterModel' => $strengthSearchModel,
                                'layout' => '{items}{pager}',
                                'pjax' => true,
                                'emptyText' => 'Tidak ada data Strength.',
                                'tableOptions' => [
                                    'class' => 'table table-responsive table-striped table-bordered'
                                ],
                                'columns' =>  [
                                    [
                                        'class' => 'kartik\grid\SerialColumn'
                                    ],
                                    [
                                        'attribute' => 'internal_variable_id',
                                        'value' => function ($model) {
                                            return ($model->internal_variable_id) ? $model->variable->variable : '-';
                                        },
                                        'width' => '200px',
                                        'filterType'=>GridView::FILTER_SELECT2,
                                        'filter'=> ArrayHelper::map($internalVariable, 'id', 'variable'), 
                                        'filterWidgetOptions'=>[
                                            'pluginOptions'=>['allowClear'=>true],
                                        ],
                                        'filterInputOptions'=>['placeholder'=>''],
                                        'group' => true
                                    ],
                                    [
                                        'attribute' => 'strength',
                                        'value' => function ($model) {
                                            return ($model->strength) ?: '-';
                                        },
                                    ],
                                    [
                                        'class' => 'kartik\grid\ActionColumn',
                                        'dropdown' => false,
                                        'vAlign'=>'middle',
                                        'urlCreator' => function($action, $model, $key, $index) { 
                                                return Url::to([$action,'id'=>$key]);
                                        },   
                                        'template' => '{view}&nbsp;&nbsp;{edit}&nbsp;&nbsp;{delete}',
                                        'buttons' => [
                                            'view' => function ($url, $model) {
                                                    return Html::a('<span class="fa fa-eye"></span>',
                                                        ['view-swot', 'id' => $model->id, 'column' => 'strength'], 
                                                        [
                                                            'title' => 'Lihat Strength ini',
                                                            'role' => 'modal-remote',
                                                            'style' => 'color: var(--primary) !important'
                                                        ]
                                                    );
                                            },
                                            'edit' => function ($url, $model) {
                                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                                        ['update-swot', 'id' => $model->situation_analysis_id, 'swotId' => $model->id, 'column' => 'strength'], 
                                                        [
                                                            'title' => 'Ubah Strength ini',
                                                            'role' => 'modal-remote',
                                                            'style' => 'color: var(--warning) !important'
                                                        ]
                                                    );
                                            },
                                            'delete' => function ($url, $model) {
                                                return Html::a('<span class="fa fa-trash"></span>',
                                                    ['delete-swot', 'id' => $model->situation_analysis_id, 'swotId' => $model->id, 'column' => 'strength'], 
                                                    [
                                                        'title' => 'Hapus Strength ini',
                                                        'role'=>'modal-remote',
                                                        'data-confirm'=>false, 
                                                        'data-method'=>false,
                                                        'data-request-method'=>'post',
                                                        'data-confirm-title'=>'Konfirmasi',
                                                        'data-confirm-message'=>'Yakin ingin menghapus Strength ini?',
                                                        'style' => 'color: var(--danger) !important'
                                                    ]
                                                );
                                            },
                                        ],
                                        'width' => '90px'
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-default" style="height: 100%;">
                        <div class="box-body">
                            <label>Weakness</label>
                            <?= Html::a('Tambah Weakness', ['add-swot', 'id' => $model->id, 'column' => 'weakness'], ['class' => 'btn btn-success btn-xs pull-right', 'role' => 'modal-remote']); ?>
                            <?= GridView::widget([
                                'id' => 'weakness-table',
                                'dataProvider' => $dataProviderWeakness,
                                'filterModel' => $weaknessSearchModel,
                                'layout' => '{items}{pager}',
                                'pjax' => true,
                                'emptyText' => 'Tidak ada data Weakness.',
                                'tableOptions' => [
                                    'class' => 'table table-responsive table-striped table-bordered'
                                ],
                                'columns' =>  [
                                    [
                                        'class' => 'kartik\grid\SerialColumn'
                                    ],
                                    [
                                        'attribute' => 'internal_variable_id',
                                        'value' => function ($model) {
                                            return ($model->internal_variable_id) ? $model->variable->variable : '-';
                                        },
                                        'width' => '200px',
                                        'filterType'=>GridView::FILTER_SELECT2,
                                        'filter'=> ArrayHelper::map($internalVariable, 'id', 'variable'), 
                                        'filterWidgetOptions'=>[
                                            'pluginOptions'=>['allowClear'=>true],
                                        ],
                                        'filterInputOptions'=>['placeholder'=>''],
                                        'group' => true
                                    ],
                                    [
                                        'attribute' => 'weakness',
                                        'value' => function ($model) {
                                            return ($model->weakness) ?: '-';
                                        },
                                    ],
                                    [
                                        'class' => 'kartik\grid\ActionColumn',
                                        'dropdown' => false,
                                        'vAlign'=>'middle',
                                        'urlCreator' => function($action, $model, $key, $index) { 
                                                return Url::to([$action,'id'=>$key]);
                                        },   
                                        'template' => '{view}&nbsp;&nbsp;{edit}&nbsp;&nbsp;{delete}',
                                        'buttons' => [
                                            'view' => function ($url, $model) {
                                                    return Html::a('<span class="fa fa-eye"></span>',
                                                        ['view-swot', 'id' => $model->id, 'column' => 'weakness'], 
                                                        [
                                                            'title' => 'Lihat Weakness ini',
                                                            'role' => 'modal-remote',
                                                            'style' => 'color: var(--primary) !important'
                                                        ]
                                                    );
                                            },
                                            'edit' => function ($url, $model) {
                                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                                        ['update-swot', 'id' => $model->situation_analysis_id, 'swotId' => $model->id, 'column' => 'weakness'], 
                                                        [
                                                            'title' => 'Ubah Weakness ini',
                                                            'role' => 'modal-remote',
                                                            'style' => 'color: var(--warning) !important'
                                                        ]
                                                    );
                                            },
                                            'delete' => function ($url, $model) {
                                                return Html::a('<span class="fa fa-trash"></span>',
                                                    ['delete-swot', 'id' => $model->situation_analysis_id, 'swotId' => $model->id, 'column' => 'weakness'], 
                                                    [
                                                        'title' => 'Hapus Weakness ini',
                                                        'role'=>'modal-remote',
                                                        'data-confirm'=>false, 
                                                        'data-method'=>false,
                                                        'data-request-method'=>'post',
                                                        'data-confirm-title'=>'Konfirmasi',
                                                        'data-confirm-message'=>'Yakin ingin menghapus Weakness ini?',
                                                        'style' => 'color: var(--danger) !important'
                                                    ]
                                                );
                                            },
                                        ],
                                        'width' => '90px'
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row equal">
                <div class="col-md-6">
                    <div class="box box-default" style="height: 100%;">
                        <div class="box-body">
                            <label>Opportunity</label>
                            <?= Html::a('Tambah Opportunity', ['add-swot', 'id' => $model->id, 'column' => 'opportunity'], ['class' => 'btn btn-success btn-xs pull-right', 'role' => 'modal-remote']); ?>
                            <?= GridView::widget([
                                'id' => 'opportunity-table',
                                'dataProvider' => $dataProviderOpportunity,
                                'filterModel' => $opportunitySearchModel,
                                'layout' => '{items}{pager}',
                                'pjax' => true,
                                'emptyText' => 'Tidak ada data Opportunity.',
                                'tableOptions' => [
                                    'class' => 'table table-responsive table-striped table-bordered'
                                ],
                                'columns' =>  [
                                    [
                                        'class' => 'kartik\grid\SerialColumn'
                                    ],
                                    [
                                        'attribute' => 'external_variable_id',
                                        'value' => function ($model) {
                                            return ($model->external_variable_id) ? $model->variable->variable : '-';
                                        },
                                        'width' => '200px',
                                        'filterType'=>GridView::FILTER_SELECT2,
                                        'filter'=> ArrayHelper::map($externalVariable, 'id', 'variable'), 
                                        'filterWidgetOptions'=>[
                                            'pluginOptions'=>['allowClear'=>true],
                                        ],
                                        'filterInputOptions'=>['placeholder'=>''],
                                        'group' => true
                                    ],
                                    [
                                        'attribute' => 'opportunity',
                                        'value' => function ($model) {
                                            return ($model->opportunity) ?: '-';
                                        },
                                    ],
                                    [
                                        'class' => 'kartik\grid\ActionColumn',
                                        'dropdown' => false,
                                        'vAlign'=>'middle',
                                        'urlCreator' => function($action, $model, $key, $index) { 
                                                return Url::to([$action,'id'=>$key]);
                                        },   
                                        'template' => '{view}&nbsp;&nbsp;{edit}&nbsp;&nbsp;{delete}',
                                        'buttons' => [
                                            'view' => function ($url, $model) {
                                                    return Html::a('<span class="fa fa-eye"></span>',
                                                        ['view-swot', 'id' => $model->id, 'column' => 'opportunity'], 
                                                        [
                                                            'title' => 'Lihat Opportunity ini',
                                                            'role' => 'modal-remote',
                                                            'style' => 'color: var(--primary) !important'
                                                        ]
                                                    );
                                            },
                                            'edit' => function ($url, $model) {
                                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                                        ['update-swot', 'id' => $model->situation_analysis_id, 'swotId' => $model->id, 'column' => 'opportunity'], 
                                                        [
                                                            'title' => 'Ubah Opportunity ini',
                                                            'role' => 'modal-remote',
                                                            'style' => 'color: var(--warning) !important'
                                                        ]
                                                    );
                                            },
                                            'delete' => function ($url, $model) {
                                                return Html::a('<span class="fa fa-trash"></span>',
                                                    ['delete-swot', 'id' => $model->situation_analysis_id, 'swotId' => $model->id, 'column' => 'opportunity'], 
                                                    [
                                                        'title' => 'Hapus Opportunity ini',
                                                        'role'=>'modal-remote',
                                                        'data-confirm'=>false, 
                                                        'data-method'=>false,
                                                        'data-request-method'=>'post',
                                                        'data-confirm-title'=>'Konfirmasi',
                                                        'data-confirm-message'=>'Yakin ingin menghapus Opportunity ini?',
                                                        'style' => 'color: var(--danger) !important'
                                                    ]
                                                );
                                            },
                                        ],
                                        'width' => '90px'
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-default" style="height: 100%;">
                        <div class="box-body">
                            <label>Threat</label>
                            <?= Html::a('Tambah Threat', ['add-swot', 'id' => $model->id, 'column' => 'threat'], ['class' => 'btn btn-success btn-xs pull-right', 'role' => 'modal-remote']); ?>
                            <?= GridView::widget([
                                'id' => 'threat-table',
                                'dataProvider' => $dataProviderThreat,
                                'filterModel' => $threatSearchModel,
                                'layout' => '{items}{pager}',
                                'pjax' => true,
                                'emptyText' => 'Tidak ada data Threat.',
                                'tableOptions' => [
                                    'class' => 'table table-responsive table-striped table-bordered'
                                ],
                                'columns' =>  [
                                    [
                                        'class' => 'kartik\grid\SerialColumn'
                                    ],
                                    [
                                        'attribute' => 'external_variable_id',
                                        'value' => function ($model) {
                                            return ($model->external_variable_id) ? $model->variable->variable : '-';
                                        },
                                        'width' => '200px',
                                        'filterType'=>GridView::FILTER_SELECT2,
                                        'filter'=> ArrayHelper::map($externalVariable, 'id', 'variable'), 
                                        'filterWidgetOptions'=>[
                                            'pluginOptions'=>['allowClear'=>true],
                                        ],
                                        'filterInputOptions'=>['placeholder'=>''],
                                        'group' => true
                                    ],
                                    [
                                        'attribute' => 'threat',
                                        'value' => function ($model) {
                                            return ($model->threat) ?: '-';
                                        },
                                    ],
                                    [
                                        'class' => 'kartik\grid\ActionColumn',
                                        'dropdown' => false,
                                        'vAlign'=>'middle',
                                        'urlCreator' => function($action, $model, $key, $index) { 
                                                return Url::to([$action,'id'=>$key]);
                                        },   
                                        'template' => '{view}&nbsp;&nbsp;{edit}&nbsp;&nbsp;{delete}',
                                        'buttons' => [
                                            'view' => function ($url, $model) {
                                                    return Html::a('<span class="fa fa-eye"></span>',
                                                        ['view-swot', 'id' => $model->id, 'column' => 'threat'], 
                                                        [
                                                            'title' => 'Lihat Threat ini',
                                                            'role' => 'modal-remote',
                                                            'style' => 'color: var(--primary) !important'
                                                        ]
                                                    );
                                            },
                                            'edit' => function ($url, $model) {
                                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                                        ['update-swot', 'id' => $model->situation_analysis_id, 'swotId' => $model->id, 'column' => 'threat'], 
                                                        [
                                                            'title' => 'Ubah Threat ini',
                                                            'role' => 'modal-remote',
                                                            'style' => 'color: var(--warning) !important'
                                                        ]
                                                    );
                                            },
                                            'delete' => function ($url, $model) {
                                                return Html::a('<span class="fa fa-trash"></span>',
                                                    ['delete-swot', 'id' => $model->situation_analysis_id, 'swotId' => $model->id, 'column' => 'threat'], 
                                                    [
                                                        'title' => 'Hapus Threat ini',
                                                        'role'=>'modal-remote',
                                                        'data-confirm'=>false, 
                                                        'data-method'=>false,
                                                        'data-request-method'=>'post',
                                                        'data-confirm-title'=>'Konfirmasi',
                                                        'data-confirm-message'=>'Yakin ingin menghapus Threat ini?',
                                                        'style' => 'color: var(--danger) !important'
                                                    ]
                                                );
                                            },
                                        ],
                                        'width' => '90px'
                                    ],
                                ],
                            ]) ?>
                        </div>
                </div>
            </div>

        </div>
    </div>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => 'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
