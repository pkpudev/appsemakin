<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css?p=230820211059',
        'css/custom.css?v=1.0.20',
        'css/custom-login.css',
        // 'css/step.css',
        // 'css/animate.min.css',
    ];
    public $js = [
        'js/custom.js?v=0.0.3',
        'js/jquery.ez-plus.js',
        // 'js/jquery.countdown.js',
        // 'js/jquery.nicescroll.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
